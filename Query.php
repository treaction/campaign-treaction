<?php
namespace Packages\Core\Persistence\Generic;

use Packages\Core\Persistence\Generic\AbstractQuery;


/**
 * Description of Query
 *
 * @author Cristian.Reus
 */
class Query extends AbstractQuery {
	
	/**
	 * init
	 * 
	 * @return void
	 * @throws \InvalidArgumentException
	 */
	public function init() {
		if (\is_null($this->databaseHandle)) {
			throw new \InvalidArgumentException('no databaseHandle initialized', 1445854440);
		}
	}
	
	/**
	 * executeSelectQuery
	 * ^
	 * @param string $query
	 * @return boolean|array
	 */
	public function executeSelectQuery($query) {
		return $this->databaseHandle->processSelectQuery(
			$query,
			array()
		);
	}
	
	/**
	 * executeQuery
	 * 
	 * @param string $query
	 * @return mixed
	 */
	public function executeQuery($query) {
		return $this->databaseHandle->executeQuery($query);
	}
	
	/**
	 * getRowByQueryPartsDataArray
	 * 
	 * @param array $queryPartsDataArray
	 * @param string $fetchClass
	 * @param string $addWhere
	 * @return \Packages\Core\DomainObject\AbstractEntity
	 * @throws \DomainException
	 */
	public function getRowByQueryPartsDataArray(array $queryPartsDataArray, $fetchClass, $addWhere = '') {
		#var_dump(debug_backtrace()); 
		$query = $this->createSelectQuery(
			$queryPartsDataArray,
			$addWhere
		);
		
		$object = $this->databaseHandle->processFetchQueryByClass(
			$query,
			$queryPartsDataArray,
			$fetchClass
		);
		if (!($object instanceof \Packages\Core\DomainObject\AbstractEntity)) {
			throw new \DomainException('invalid Entity: ' . $fetchClass, 1447056666);
		}
		
		$object->_memorizeCleanState();
		
		return $object;
	}
	
	/**
	 * getRowsByQueryPartsDataArray
	 * 
	 * @param array $queryPartsDataArray
	 * @param string $fetchClass
	 * @param string $addWhere
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	public function getRowsByQueryPartsDataArray(array $queryPartsDataArray, $fetchClass, $addWhere = '') {
		$query = $this->createSelectQuery(
			$queryPartsDataArray,
			$addWhere
		);
		
		return $this->databaseHandle->processFetchAllQueryByClass(
			$query,
			$queryPartsDataArray,
			$fetchClass
		);
	}
	
	/**
	 * getCountByQueryPartsDataArray
	 * 
	 * @param array $queryPartsDataArray
	 * @return integer
	 */
	public function getCountByQueryPartsDataArray(array $queryPartsDataArray) {
		$queryPartsDataArray['SELECT'] = array(
			'count' => 'COUNT(1)'
		);
		unset(
			$queryPartsDataArray['ORDER_BY'],
			$queryPartsDataArray['LIMIT']
		);
		
		$query = $this->createSelectQuery(
			$queryPartsDataArray,
			' AND ' . Query::masketField($this->primaryField) . ' > 1'
		);
		
		return $this->databaseHandle->processFetchColumn(
			$query,
			$queryPartsDataArray
		);
	}
	
	/**
	 * add
	 * 
	 * @param array $dataArray
	 * @param null|string $table
	 * @return integer
	 */
	public function add(array $dataArray, $table = NULL) {
		$query = $this->createInsertQuery(
			$dataArray,
			$table
		);
		
		return $this->databaseHandle->insertQuery(
			$query,
			$dataArray
		);
	}
	
	/**
	 * update
	 * 
	 * @param integer $primaryFieldValue
	 * @param array $dataArray
	 * @param null|string $primaryField
	 * @param null|string $table
	 * @return boolean
	 * @throws \InvalidArgumentException
	 * @throws \DomainException
	 */
	public function update($primaryFieldValue, array $dataArray, $primaryField = NULL, $table = NULL) {
		if (!\is_int($primaryFieldValue)) {
			throw new \InvalidArgumentException('Invalid field value', 1446133625);
		}
		
		if ((int) $primaryFieldValue <= 0) {
			throw new \DomainException('Field value must not be zero');
		}
		
		if (\is_null($primaryField) 
			|| \strlen($primaryField) === 0
		) {
			$primaryField = $this->primaryField;
		}
		
		$query = $this->createUpdateQuery(
			$primaryField,
			$dataArray,
			$table
		);
		
		$dataArray['_' . $primaryField] = array(
			'value' => (int) $primaryFieldValue,
			'dataType' => \PDO::PARAM_INT
		);
		
		return $this->databaseHandle->updateQuery(
			$query,
			$dataArray
		);
	}
	
	/**
	 * delete
	 * 
	 * @param integer $primaryFieldValue
	 * @param null|string $primaryField
	 * @param null|string $table
	 * @return boolean
	 * @throws \InvalidArgumentException
	 * @throws \DomainException
	 */
	public function delete($primaryFieldValue, $primaryField = NULL, $table = NULL) {
		if (!\is_int($primaryFieldValue)) {
			throw new \InvalidArgumentException('Invalid field value', 1446133625);
		}
		
		if ((int) $primaryFieldValue <= 0) {
			throw new \DomainException('Field value must not be zero');
		}
		
		if (\is_null($primaryField) 
			|| \strlen($primaryField) === 0
		) {
			$primaryField = $this->primaryField;
		}
		
		$query = $this->createDeleteQuery(
			$primaryField,
			$table
		);
		
		return $this->databaseHandle->deleteQuery(
			$query,
			array(
				'_' . $primaryField => array(
					'value' => $primaryFieldValue,
					'dataType' => \PDO::PARAM_INT
				)
			)
		);
	}
}
