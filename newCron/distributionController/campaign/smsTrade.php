<?php
namespace newCron\distributionController\campaign;

class smsTrade
  {
    
    static public function buildMessageXml($recipient, $message) {
      $xml = new \SimpleXMLElement('<MESSAGES/>');

      $authentication = $xml->addChild('AUTHENTICATION');
      $authentication->addChild('PRODUCTTOKEN', 'EF1B1798-C53E-44A8-BA79-87499DF4160F');

      $msg = $xml->addChild('MSG');
      $msg->addChild('FROM', 'Company');
      $msg->addChild('TO', $recipient);
      $msg->addChild('BODY', $message);

      return $xml->asXML();
    }

    public function sendMessage($recipient, $message) {
      $xml = self::buildMessageXml($recipient, $message);

      $ch = curl_init(); // cURL v7.18.1+ and OpenSSL 0.9.8j+ are required
      curl_setopt_array($ch, array(
          CURLOPT_URL            => 'https://sgw01.cm.nl/gateway.ashx',
          CURLOPT_HTTPHEADER     => array('Content-Type: application/xml'),
          CURLOPT_POST           => true,
          CURLOPT_POSTFIELDS     => $xml,
          CURLOPT_RETURNTRANSFER => true
        )
      );

      $response = curl_exec($ch);

      curl_close($ch);

      return $response;
    }
  }
