<?php


/**
 * getServerPath
 * 
 * @return string
 */
function getServerPath() {
	return \substr(__DIR__, 0, \stripos(__DIR__, 'newCron' . \DIRECTORY_SEPARATOR));
}

/**
 * createCampaignQueryDataArray
 * 
 * @param \DateTime $dateFrom
 * @param \DateTime $dateTo
 * @param \integer $campaignDsdId
 * @return array
 */
function createCampaignQueryDataArray(\DateTime $dateFrom, \DateTime $dateTo) {
	return array(
		'WHERE' => array(
			'mailId' => array(
				'sql' => '`mail_id`',
				'value' => null,
				'comparison' => '<>'
			),
			'dateBegin' => array(
				'sql' => '`datum`',
				'value' => $dateFrom->format('Y-m-d H:i:s'),
				'comparison' => \PdoDbManager::$fieldGreaterThan
			),
			'dateEnd' => array(
				'sql' => '`datum`',
				'value' => $dateTo->format('Y-m-d H:i:s'),
				'comparison' => \PdoDbManager::$fieldLessThan
			),
		),
		'ORDER_BY' => '`datum` DESC'
	);
}

/**
 * processUpdateSendEmailCampaign
 * @param \CampaignManager $campaignManager
 * @return void
 */
function processUpdateSendBounceEmailCampaign(\CampaignManager $campaignManager, $campaignId) {
	
	
	$dataArray = array(
		'send_bounce_mail' => array(
			'value' => 1,
			'dataType' => \PDO::PARAM_INT
		)
	);
	$campaignManager->updateCampaignDataRowByKid($campaignId, $dataArray);
}

/**
 * mailContent
 * 
 * @param \CampaignEntity $campaignEntity
 * @param type $mandant
 * @return string
 */
function mailContent(\CampaignEntity $campaignEntity, $mandant, $hsBouncesQuote){
   
    switch ($campaignEntity->getVersandsystem()){
        case 'M' : 
            $versansystem = 'Xqueue';
            break;
        case 'SE' : 
            $versansystem = 'Sendeffect';
            break;
        default :
            $versansystem = 'Xqueue';        
    }
    
    $messageBody = '**********************************************************************' . \chr(13);
    $messageBody .= 'Versansystem : ' . $versansystem. \chr(13);
    $messageBody .= 'Mandant : ' . $mandant . \chr(13);
    $messageBody .= 'Kampagne ID : ' . $campaignEntity->getK_id() . \chr(13);
    $messageBody .= 'Kampagne Name : ' . $campaignEntity->getK_name() .  \chr(13);
    $messageBody .= 'Versandmenge : ' . $campaignEntity->getVersendet() . \chr(13);
    $messageBody .= 'Versanddatum : ' . $campaignEntity->getDatum()->format('d.m.Y') . ' um ' . $campaignEntity->getDatum()->format('H:i'). \chr(13);
    $messageBody .= 'Bouneces Gesamt : ' . ($campaignEntity->getHbounces() + $campaignEntity->getSbounces()) . \chr(13);
    $messageBody .= 'Soft Bouneces : ' . $campaignEntity->getSbounces() . \chr(13);
    $messageBody .= 'Hard Bouneces: ' . $campaignEntity->getHbounces() . \chr(13);
    $messageBody .= 'Bouneces Quote: ' . $hsBouncesQuote . ' % ' . \chr(13);
    $messageBody .= '**********************************************************************' . \chr(13);   
    return $messageBody;   
}
    
// getServerPath
$serverPath = \getServerPath();
\define('DIR_Cron', $serverPath . 'newCron' . \DIRECTORY_SEPARATOR);
\define('DIR_Cron_config', DIR_Cron . 'config' . \DIRECTORY_SEPARATOR);
\define('DIR_Cron_library', DIR_Cron . 'library' . \DIRECTORY_SEPARATOR);
\define('DIR_Cron_dist', DIR_Cron . 'distributionController' . \DIRECTORY_SEPARATOR);
\define('DIR_Cron_campaign', DIR_Cron_dist . 'campaign' . \DIRECTORY_SEPARATOR);
\define('DIR_Core', $serverPath. 'Core' . \DIRECTORY_SEPARATOR);
\define('DIR_Packages', DIR_Core. 'Packages' . \DIRECTORY_SEPARATOR);
\define('DIR_Core2', DIR_Packages. 'Core' . \DIRECTORY_SEPARATOR);
\define('DIR_Utiliy', DIR_Core2. 'Utility' . \DIRECTORY_SEPARATOR);

/**
 * init
 * 
 * loadAndInitCronEmsAutoloader
 * 
 * loadAndInitCronEmsAutoloader
 * $debugLogManager
 * $clientManager
 */
require_once(DIR_Cron . 'loadAndInitCronEmsAutoloader.php');



require_once(DIR_Cron_config . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

require_once(DIR_Factory . 'DeliverySystemFactory.php');

require_once (DIR_Utiliy . 'MailUtility.php');


$benutzerVertriebEntity = $clientManager->getActiveUsersDataItemsByDepartment('v');
/* @var $benutzerVertriebEntity \UserEntity  */

foreach ($benutzerVertriebEntity as $benutzerVertrieb){ 
    
       $vertriebAdressArray[$benutzerVertrieb->getBenutzer_id()] = $benutzerVertrieb->getEmail();  
   }
 
try {
  
   #\DebugAndExceptionUtils::sendDebugData($empfaengerArray, __FILE__ . ': ' . $empfaengerArray);


	$timeStart = time();
	
	$dateNow = new \DateTime('now');
	$dateNow->setTime(0, 0, 0);
	$dateFrom = clone $dateNow;
	$dateTo = clone $dateNow;

	$dateFrom->sub(new \DateInterval('P1D'));
	$dateFrom->setTime(23, 59, 59);
	
	$dateTo->add(new \DateInterval('P1D'));
	$dateTo->setTime(00, 00, 00);
	
	$clientEntity = $clientManager->getClientDataById($clientId);
		if (!($clientEntity instanceof \ClientEntity)) {
		throw new \DomainException('invalid ClientEntity!', 1424262859);
	}
		
	/**
	 * initCampaignManager
	 */
	$campaignManager = \initCampaignManager(
		$clientEntity,
		$pdoDebug,
		$pdoStatmentDebug
	);
	
        $campaignQueryPartsDataArray = \createCampaignQueryDataArray(
		$dateFrom, 
		$dateTo
		);
         
	/**
	* getCampaignDataItemByQueryParts
	*/
	$campaignsDataArray = $campaignManager->getCampaignsDataItemsByQueryParts(
		$campaignQueryPartsDataArray,
		\PDO::FETCH_ASSOC
	);
        foreach ($campaignsDataArray as $campaignDataArray){                   
               $campaignEntity = $campaignManager->getCampaignDataItemById(
                                $campaignDataArray['k_id']
                                );
               
               $bounces = $campaignEntity->getHbounces() + $campaignEntity->getSbounces();
	       $send = $campaignEntity->getVersendet();
               $hsBouncesQuote = \FormatUtils::rateCalculation(
                       $bounces, 
                       $send
                       ); 
               if((\intval($bounces)) > 1000
                   ||
                   \floatval($hsBouncesQuote) > 0.50
                   ){                   
               $messageBody = \mailContent(
                               $campaignEntity,
                               $mandant,
                               $hsBouncesQuote
                           );
                                        
                  if(\intval($campaignEntity->getSend_bounce_mail()) !== 1){
                    $subject = 'Alarm hoher Bounce';
                    $mailToTcm = new \Packages\Core\Utility\MailUtility();
                    
                          $mailToTcm::sendMailToTcm(
                                  $subject,
                                  $messageBody
                          );
                          /*foreach ($vertriebAdressArray as $key => $vertriebAdress){
                              $mailToTcm::sendMailToSingle(
                                      $vertriebAdress,
                                     $subject,
                                      $messageBody
                                      );
                          }*/
                       \processUpdateSendBounceEmailCampaign(
                               $campaignManager,
                               $campaignEntity->getK_id()
                               );                                                         
                       }
                  }
        }

} catch (Exception $exc) {
	echo $exc->getTraceAsString();
 }


