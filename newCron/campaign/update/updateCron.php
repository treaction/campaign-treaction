<?php
/**
 * getServerPath
 * 
 * @return string
 */
function getServerPath() {
	return \substr(__DIR__, 0, \stripos(__DIR__, 'newCron' . \DIRECTORY_SEPARATOR));
}

/**
 * getParantsClientsIds
 * 
 * @param ClientEntity $clientEntity
 * @param \ClientManager $clientManager
 * @return array
 */
function getParantsClientsIds(\ClientEntity $clientEntity, \ClientManager $clientManager) {
	$resultDataArray = array();
	
	if ($clientEntity->getHas_multiple_distributors()) {
		/**
		 * getParentClientsDataItems
		 */
		$parentClients = $clientManager->getParentClientsDataItems($clientEntity->getId());
		if (\count($parentClients) > 0) {
			$resultDataArray[] = $clientEntity->getId();
			
			foreach ($parentClients as $clientEntity) {
				/* @var $clientEntity \ClientEntity */
				$resultDataArray[] = $clientEntity->getId();
			}
		}
	}
	
	return $resultDataArray;
}

/**
 * createCampaignQueryDataArray
 * 
 * @param \DateTime $dateFrom
 * @param \DateTime $dateTo
 * @param \integer $campaignDsdId
 * @return array
 */
function createCampaignQueryDataArray(\DateTime $dateFrom, \DateTime $dateTo, $campaignDsdId) {
	return array(
		'WHERE' => array(
			'mailId' => array(
				'sql' => '`mail_id`',
				'value' => null,
				'comparison' => '<>'
			),
			'dateBegin' => array(
				'sql' => '`datum`',
				'value' => $dateFrom->format('Y-m-d H:i:s'),
				'comparison' => \PdoDbManager::$fieldLessOrEqualThan
			),
			'dateEnd' => array(
				'sql' => '`datum`',
				'value' => $dateTo->format('Y-m-d H:i:s'),
				'comparison' => \PdoDbManager::$fieldGreaterOrEqualThan
			),
			'dsdId' => array(
				'sql' => '`dsd_id`',
				'value' => $campaignDsdId,
				'comparison' => 'integerEqual'
			),
		),
		'ORDER_BY' => '`datum` DESC'
	);
}

/**
 * processCronDeliverySystemWebserviceAndUpdateCampaignData
 * 
 * @param \array $campaignsDataArray
 * @param \CampaignManager $campaignManager
 * @param \ClientDeliverySystemWidthDeliverySystemEntity $clientDeliveryEntity
 * @param \debugLogManager $debugLogManager
 * @return void
 */
function processCronDeliverySystemWebserviceAndUpdateCampaignData(array $campaignsDataArray, \CampaignManager $campaignManager, \ClientDeliverySystemWidthDeliverySystemEntity $clientDeliveryEntity, \debugLogManager $debugLogManager) {
	/**
     * DeliverySystemFactory::getAndInitDeliverySystem
	 * 
	 * debug
     */
	$deliverySystemWebservice = \DeliverySystemFactory::getAndInitDeliverySystem($clientDeliveryEntity);
	
	
	/**
     * connect to deliveryWebservice
     */
    $deliverySystemWebservice->login();
	$asps = $clientDeliveryEntity->getDeliverySystem()->getAsp();
          switch ($asps) {
                            case '4Wave 2018':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Marketing Sandbox':
                            case 'Stellar 2018':
                            case 'Stellar Basis':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Basis':
                            case '4Wave Black':
                            case 'Stellar Black':
                            case 'BlueLeads2019':
                            case 'BlueLeads Basis':  
                            case 'fulle.media Basis':
                            case 'fulle.media 2019':
                            case 'BlueLeads Extra':
                            case 'fulle.media Extra':    
                            case 'BlueLeads Black':
                            case 'fulle.media Black':      
                            case 'Maileon':
                            case 'CEOO':
                            case 'CEOO Black':    
                            case 'Mail-In-One':    
                              $asp = 'Maileon';
                                break;
                            case 'Sendeffect':
                            case 'Stellar SE':
                            case '4Wave SE':
                            case 'CEOO SE':    
                              $asp = 'Sendeffect';
                                break; 
                            case 'Promio':
                                $asp = 'Promio';
                                break;
                            case 'Broadmail';
                                $asp = 'Broadmail';
                                break;
                            default:
                             $asp = 'Maileon';
                                break;
                        }
	if ((\file_exists(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'updateSyncCampaign.php')) === true) {
		$actionId = $_SESSION['campaign']['actionId'];
		foreach ($campaignsDataArray as $campaignEntity) {
			/* @var $campaignEntity \CampaignEntity */
		
			include(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'updateSyncCampaign.php');
		}
	} else {
		\DebugAndExceptionUtils::sendDebugData(
			array(
				'clientDeliveryEntity' => $clientDeliveryEntity,
				'campaignsDataArray' => $campaignsDataArray
			),
			__FUNCTION__ . ': ' . $clientDeliveryEntity->getDeliverySystem()->getAsp()
		);
		
		$file = __FILE__;
		require(DIR_deliverySystems . 'noDeliverySystemImplemented.php');
	}
	
	/**
     * disconnect from deliveryWebservice
     */
    $deliverySystemWebservice->logout();
}

/**
 * processUpdateFirstCampaign
 * @param \CampaignManager $campaignManager
 * @return void
 */
function processUpdateFirstCampaign(\CampaignManager $campaignManager) {
	$dateNow = new \DateTime('now');
	
	$dataArray = array(
		'gestartet' => array(
			'value' => $dateNow->format('Y-m-d H:i:s'),
			'dataType' => \PDO::PARAM_STR
		)
	);
	$campaignManager->updateCampaignDataRowByKid(1, $dataArray);
}

/**
 * timeDifferent
 * 
 * @param integer $fromTime
 * @return string
 */
function timeDifferent($fromTime) {
	$seconds = \time() - $fromTime;
	
	return $seconds . ' seconds';
}


// getServerPath
$serverPath = \getServerPath();
\define('DIR_Cron', $serverPath . 'newCron' . \DIRECTORY_SEPARATOR);
\define('DIR_Cron_config', DIR_Cron . 'config' . \DIRECTORY_SEPARATOR);
\define('DIR_Cron_library', DIR_Cron . 'library' . \DIRECTORY_SEPARATOR);



/**
 * init
 * 
 * loadAndInitCronEmsAutoloader
 * 
 * loadAndInitCronEmsAutoloader
 * $debugLogManager
 * $clientManager
 */
require_once(DIR_Cron . 'loadAndInitCronEmsAutoloader.php');



require_once(DIR_Cron_config . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

require_once(DIR_Factory . 'DeliverySystemFactory.php');



$action = isset($argruments['action']) ? $argruments['action'] : null;
$parentClients = $underClientIds = $debugDataArray = array();

try {
	$timeStart = time();
	
	$dateTo = new \DateTime('now');
	
	$dateFrom = clone $dateTo;
	$dateFrom->setTime(0, 0, 0);
	
	switch ($action) {
		case '2Days':
			// Alle 15 Minuten, Kampagnen innerhalb 48h
			$dateFrom->sub(new \DateInterval('P2D'));
			break;

		case '1Week':
			// Alle 60 Minuten, Kampagnen > 48h und <= 1 Woche
			$dateFrom->sub(new \DateInterval('P7D'));
			
			$dateTo->sub(new \DateInterval('P2D'));
			$dateTo->setTime(23, 59, 59);
			break;

		case '2Weeks':
			// T�glich kurz vor Mitternacht, Kampagnen, > 1 Woche und <= 2 Wochen
			$dateFrom->sub(new \DateInterval('P14D'));
			
			$dateTo->sub(new \DateInterval('P7D'));
			$dateTo->setTime(23, 59, 59);
			break;
		
		default:
			throw new \InvalidArgumentException('no valid action given!', 1424262832);
	}
	
	// debug Settings
	$debugDataArray['dateFrom'] = $dateFrom;
	$debugDataArray['dateTo'] = $dateTo;
	
	$clientEntity = $clientManager->getClientDataById($clientId);
	if (!($clientEntity instanceof \ClientEntity)) {
		throw new \DomainException('invalid ClientEntity!', 1424262859);
	}

	// debug Settings
	$debugDataArray['clientEntity'] = $clientEntity->getId() . ' ' . $clientEntity->getMandant();
	
	$underClientIds = \getParantsClientsIds(
		$clientEntity,
		$clientManager
	);
	// debug Settings
	$debugDataArray['getParantsClientsIds'] = $underClientIds;
	
	/**
	 * getDeliverySystemDistributorsAndClientDeliveryDataItemsByClientId
	 * 
	 * get active delivery Systems for select ClientEntity
	 */
	$deliverySystemDistributorsWidthClientDeliveryDataArray = $clientManager->getDeliverySystemDistributorsAndClientDeliveryDataItemsByClientId(
		$clientEntity->getId(),
		$underClientIds
	);
       
	if (\count($deliverySystemDistributorsWidthClientDeliveryDataArray)) {
		/**
		 * initCampaignManager
		 */
		$campaignManager = \initCampaignManager(
			$clientEntity,
			$pdoDebug,
			$pdoStatmentDebug
		);
		
		foreach ($deliverySystemDistributorsWidthClientDeliveryDataArray as $key => $deliverySystemDistributorsWidthClientDeliveryEntity) {
			/* @var $deliverySystemDistributorsWidthClientDeliveryEntity \DeliverySystemDistributorWidthClientDeliverySystemEntity */
			
			/**
			 * createCampaignQueryDataArray
			 */
			$campaignQueryPartsDataArray = \createCampaignQueryDataArray(
				$dateTo,
				$dateFrom,
				$deliverySystemDistributorsWidthClientDeliveryEntity->getId()
			);
                       
			// debug Settings
			$debugDataArray['deliverySystemDistributor'][$key]['title'] = $deliverySystemDistributorsWidthClientDeliveryEntity->getTitle();
			$debugDataArray['deliverySystemDistributor'][$key]['campaignQueryPartsDataArray'] = $campaignQueryPartsDataArray;
			
			/**
			* getCampaignDataItemByQueryParts
			*/
			$campaignsDataArray = $campaignManager->getCampaignsDataItemsByQueryParts(
				$campaignQueryPartsDataArray,
				\PDO::FETCH_CLASS
			);
                        
			if (\count($campaignsDataArray) > 0) {
				if (\CommandLine::getBoolean('processDeliveryWebservice')) {
					\processCronDeliverySystemWebserviceAndUpdateCampaignData(
						$campaignsDataArray,
						$campaignManager,
						$deliverySystemDistributorsWidthClientDeliveryEntity->getClientDeliveryEntity(),
						$debugLogManager
					);
				}
				
				// debug Settings
				$debugDataArray['deliverySystemDistributor'][$key]['campaignsCount'] = \count($campaignsDataArray);
			} else {
				// debug Settings
				$debugDataArray['deliverySystemDistributor'][$key]['campaignsCount'] = 'no campaigns found';
			}
		}
		
		/**
		* processUpdateFirstCampaign
		*/
		if (\CommandLine::getBoolean('processDeliveryWebservice')) {
			\processUpdateFirstCampaign($campaignManager);
		}
	}

	if (\CommandLine::getBoolean('sendMail')) {
		\DebugAndExceptionUtils::sendDebugData(
			$debugDataArray,
			'MaaS API Debug (' . $clientId . ', action: ' . $action . ') execution time: ' . \timeDifferent($timeStart)
		);
	}
} catch (\Exception $e) {
	\DebugAndExceptionUtils::sendDebugData(
		$e,
		'MaaS API St�rung:' . __FILE__
	);
}

unset(
	$parentClients,
	$underClientIds,
	$debugDataArray
);