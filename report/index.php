<?php
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();

require_once('../system/db_optdb_connect.inc.php');

$report = $_GET['report'];
$monat_selected = $_GET['monat'];
$jahr_selected = $_GET['jahr'];
$empfaenger = substr($_GET['empfaenger'], 0, -1);

$monat_name = array('', 'Januar', 'Februar', 'M&auml;rz', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember');

for ($i=1; $i<=12; $i++) {
	if ($i<=9) {$j = "0".$i;} else {$j=$i;}
	$monat_option .= "<option value='".$j."' ";
		if ($j==$monat_selected) {$monat_option .= "selected";}
	$monat_option .= ">".$monat_name[$i]."</option>";
}

$yNow = date("Y",time());
$jahr_sql = mysql_query("SELECT YEAR(Anmeldung) AS Jahr FROM `Users` WHERE YEAR(Anmeldung) <= '$yNow' AND YEAR(Anmeldung) > 0 GROUP BY Jahr ORDER BY Jahr desc");
while ($zeile = mysql_fetch_array($jahr_sql)) {
	$jahr = $zeile['Jahr'];
	$jahr_option .= "<option value='".$jahr."' ";
		if ($jahr==$jahr_selected) {$jahr_option .= "selected";}
	$jahr_option .= ">".$jahr."</option>";
}

if ($report && $empfaenger) {
	$email_confirm = "<span style='color:#0000FF'>Das Reporting <strong>'".$report."'</strong> wurde an <strong>".$empfaenger."</strong> versendet.</span><br /><br />";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporting - &Uuml;berblick</title>
<style type="text/css">

</style>
<script>
var TimeToFade = 1000.0;

function fade(eid){

var element = document.getElementById(eid);
if(element == null)
return;

if(element.FadeState == null){
element.FadeState = -2;
}

if(element.FadeState == 1 || element.FadeState == -1){
element.FadeState = element.FadeState == 1 ? -1 : 1;
element.FadeTimeLeft = TimeToFade - element.FadeTimeLeft;

}else{
element.FadeState = element.FadeState == 2 ? -1 : 1;
element.FadeTimeLeft = TimeToFade;
setTimeout("animateFade(" + new Date().getTime()
+ ",'" + eid + "')", 33);
}
}

function animateFade(lastTick, eid){

var curTick = new Date().getTime();
var elapsedTicks = curTick - lastTick;

var element = document.getElementById(eid);

if(element.FadeTimeLeft <= elapsedTicks){

element.style.opacity = element.FadeState == 1 ? '1' : '0';
element.style.filter = 'alpha(opacity = '
+ (element.FadeState == 1 ? '100' : '0') + ')';
element.FadeState = element.FadeState == 1 ? 2 : -2;

return;
}

element.FadeTimeLeft -= elapsedTicks;
var newOpVal = element.FadeTimeLeft/TimeToFade;
if(element.FadeState == 1)
newOpVal = 1 - newOpVal;

element.style.opacity = newOpVal;
element.style.filter =
'alpha(opacity = ' + (newOpVal*100) + ')';

setTimeout("animateFade(" + curTick
+ ",'" + eid + "')", 33);
}

function hover(id) {
document.getElementById(id).style.display = '';
 }
 
function feld_anz(m)
{	
	for (i=1;i<=m;i++) {
		var anzahl = anzahl+1;
		var feld = 'details'+i;
		 if (document.getElementById)
		{
			vista = (document.getElementById(feld).style.display == 'none') ? '' : 'none';
			document.getElementById(feld).style.display = vista;
		}
	}
}


// Request senden
	function setRequest() 
	{
		// Request erzeugen
		if (window.XMLHttpRequest) 
		{
			request = new XMLHttpRequest(); // Mozilla, Safari, Opera
		} 
		else if (window.ActiveXObject) 
		{
			try 
			{
				request = new ActiveXObject('Msxml2.XMLHTTP'); // IE 5
			} 
			catch (e) 
			{
				try 
				{
					request = new ActiveXObject('Microsoft.XMLHTTP'); // IE 6
				} 
				catch (e) {}
			}
		}
		
		// �berpr�fen, ob Request erzeugt wurde
		if (!request) 
		{
			alert("Kann keine XMLHTTP-Instanz erzeugen");
			return false;
		} 
		else 
		{
			var monat = document.getElementById('monat').value;
			var jahr = document.getElementById('jahr').value;
			var prod = document.getElementById('prod').value;
			var url = "ergebnis.php";
			
			document.getElementById("content").innerHTML = '<table width="350" cellpadding="0" cellspacing="0" style="border: 1px solid #44AAFF" align="left"><tr><td height="150" style="vertical-align:middle" align="center"><span style="vertical-align:middle;color: #0055C6">Erstelle Reporting<br /><br /><img src="ajax-loader.gif"></span></td></tr></table>';

			request.open('post', url, true);
			// Requestheader senden
			request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			// Request senden
			request.send('monat='+monat+'&jahr='+jahr+'&prod='+prod);
			// Request auswerten
			request.onreadystatechange = interpretRequest;


		}
	}

	// Request auswerten
	function interpretRequest() 
	{
		switch (request.readyState) 
		{
			// wenn der readyState 4 und der request.status 200 ist, dann ist alles korrekt gelaufen
			case 4:
				if (request.status != 200) 
				{
					document.getElementById('content').innerHTML = "Fehler: " + request.status + " " + request.statusText;
				} 
				else 
				{
					var content = request.responseText;
					// den Inhalt des Requests in das <div> schreiben
					document.getElementById('content').innerHTML = content;
				}
				break;
			default:
				break;
		}
	}
</script>
<style type="text/css">
body, html {font-family:Arial, Helvetica, sans-serif; font-size:11px;color:#000000;}
a {text-decoration:none;color:#333333;}
a:hover {text-decoration:none;color:#0000FF;}
a:visited {text-decoration:none;color:#333333;}
#home {background-color:#666666;color:#FFFFFF;width:100px;padding:3px;}
#home:hover {background-color:#44aaff;color:#FFFFFF;width:100px;padding:3px;}
#email_option {background-color:#E1E1E1;width:500px;padding:5px}
#email_option form{margin-bottom: 10px; margin-top: 10px;}
#blau_oben {background-color:#44aaff;color:#FFFFFF;background-image:url('bg.gif');background-repeat:repeat-x}
.blau {background-color:#44aaff;color:#FFFFFF;}
table {border-collapse:collapse;border: 1px solid #0055C6;}
table tr td{border: 1px solid #0055C6;}
</style>
</head>

<body style="font-family:Arial, Helvetica, sans-serif;color:#333333;font-size:12px" <?php if ($email_confirm) {print "onload=\"fade('fadeBlock');\"";} ?>>
<span style="font-size:16px;font-weight:bold">Interactive One - Realtime Lieferungen</span><hr />

<div id="fadeBlock" style="display:in-line;background-color:white;width:550px;
text-align:left;-moz-opacity:0.0;filter:alpha(opacity=1); ">
<?php
	print $email_confirm;
?>
</div>

Bitte Monat / Jahr ausw�hlen und auf das gew�nschte Reporting klicken:<br /><br />
	<select name="monat" id="monat"><?php print $monat_option;?></select>&nbsp;
	<select name="jahr" id="jahr"><?php print $jahr_option;?></select>&nbsp;&nbsp;&nbsp;
	<select name="prod" id="prod"><option value="Realtime">Realtime</option></select>
	<input type="button" onclick="javascript:setRequest()" value="anzeigen"/>
 <br /><br />
 <div id="content"></div>
</body>
</html>
