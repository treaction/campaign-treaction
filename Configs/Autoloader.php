<?php
class Autoloader {
	
	/**
	 * prodSystemUrl
	 * 
	 * @var string
	 */
	public static $prodSystemUrl = 'campaign-treaction.de';
	
	/**
	 * testSystemUrl
	 * 
	 * @var string
	 */
	public static $testSystemUrl = 'test.campaign-treaction.de';
	
	/**
	 * devSystemUrl
	 * 
	 * @var string
	 */
	public static $devSystemUrl = 'dev.campaign-treaction.de';
	
	
	
	/**
	 * init
	 * 
	 * @return	void
	 */
	public static function init() {
		try {
			\spl_autoload_register(
				array(
					__CLASS__,
					'loadSystemClass'
				)
			);
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
	
	/**
	 * loadModules
	 * 
	 * @return void
	 */
	public static function loadModules() {
		try {
			\spl_autoload_register(
				array(
					__CLASS__,
					'loadModulesClass'
				)
			);
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
	
	/**
	 * initErrorReporting
	 * 
	 * @return void
	 */
	public static function initErrorReporting() {
		\error_reporting(null);
		\ini_set('display_errors', false);
		
		if (\Packages\Core\Utility\GeneralUtility::getUserIp() == \Packages\Core\Utility\GeneralUtility::getDevIpAddress()) {
			if (\Packages\Core\Utility\GeneralUtility::isProdSystem()) {
				// TODO: besser l�sen
				if (\Packages\Core\Utility\GeneralUtility::$loggedUserId === 2) {
					\error_reporting(E_ALL);
					\ini_set('display_errors', true);
				}
			} else {
				\error_reporting(E_ALL);
				\ini_set('display_errors', true);
			}
		}
	}
	
	/**
	 * startAndCheckClientSession
	 * 
	 * @return void
	 * @throws \RuntimeException
	 */
	public static function startAndCheckClientSession() {
		if (\Packages\Core\Utility\GeneralUtility::isProdSystem()) {
			$locationUrl = self::$prodSystemUrl;
		} elseif (\Packages\Core\Utility\GeneralUtility::isTestSystem()) {
			$locationUrl = self::$testSystemUrl;
		} elseif (\Packages\Core\Utility\GeneralUtility::isDevSystem()) {
			$locationUrl = self::$devSystemUrl;
		} else {
			throw new \RuntimeException('invalid system configuration', 1446814100);
		}
		
		\session_start();
		
		if (\strlen($_SESSION['mandant']) === 0) {
			\session_destroy();
			unset($_SESSION);
			\header('Location: http://' . $locationUrl . '/meta/login.php');
            
			exit();
		}
	}
        /*
         * Test
         */
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              load - functions
	 *
	 ****************************************************************************************** */
	/**
	 * loadSystemClass
	 * 
	 * @param string $class
	 * return void
	 */
	protected static function loadSystemClass($class) {
		$correctClassName = \str_replace('\\', \DIRECTORY_SEPARATOR, $class);
		
		if ((\file_exists(\DIR_CorePath . $correctClassName . '.php')) === true) {
			require_once(\DIR_CorePath . $correctClassName . '.php');
		}
	}
	
	/**
	 * loadModulesClass
	 * 
	 * @param string $class
	 * @return void
	 */
	protected static function loadModulesClass($class) {
		if (\strpos($class, '\\Modules') === 0 
			|| \strpos($class, '\Modules') === 0 
			|| \strpos($class, 'Modules') === 0
		) {
			$correctClassName = \str_replace('Modules\\', '', $class);
			$correctClassName = \str_replace('\\', \DIRECTORY_SEPARATOR, $correctClassName);

			if ((\file_exists(\DIR_Modules . $correctClassName . '.php')) === true) {
				require_once(\DIR_Modules . $correctClassName . '.php');
			} else {
				throw new \RuntimeException('no class found', 1446825278);
				
			}
		}
	}

}