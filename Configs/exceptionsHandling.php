<?php
$exceptionDataArray = \Packages\Core\Utility\ExceptionUtility::getSimpleExceptionResultDataArray($e);
$exceptionDataArray['user'] = $_SESSION['u_vorname'] . ' ' . $_SESSION['u_nachname'];

// sendEmailToDevEmailAddress
\Packages\Core\Utility\ExceptionUtility::sendDebugData(
	$exceptionDataArray,
	$_SESSION['mandant'] . ': ' . __FILE__ . ' -> ' . __LINE__
);