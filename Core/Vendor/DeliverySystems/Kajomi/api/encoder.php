<?php
require_once 'api/classes.php';
require_once \DIR_Contrib_Pear . 'HTTP/Request2.php';

class encoder
{
	private $con;
	private $path=NULL;
	/**
	 * 
	 * constructor
	 * @param iconnection $icon
	 */
	public function __construct($icon)
	{
		$this->con=$icon;
	}
		
	/**
	 * 
	 * uploads file into storage
	 * @param string $fname
	 * full path to video
	 * @param string $description
	 * description for video
	 * @return
	 * returns storagekey
	 */
	public function uploadFile($fname,$description=NULL) 
	{
		return $this->con->uploadFile($fname,$description);
	}
	
	/**
	 * 
	 * uploads file into storage
	 * @param string $fname
	 * full path to video
	 * @param string $description
	 * description for video
	 * @return
	 * returns storagekey
	 */
	public function purgeFile($fname) 
	{
		return $this->con->purgeFile($fname);
	}	
	/**
	 * returns all files in storage
	 * 
	 */
	public function readStorage()
	{
		$cc=call::create("api","basic/encoder/readStorage");
		return $this->con->invoke($cc);		
	}
	/**
	 * 
	 * encodes file in storage
	 * @param string $storagekey
	 * storagekey
	 * @return
	 * array of jobs
	 */
	public function encode($storagekey)
	{
		$cc=call::create("api","basic/encoder/encode")->addParam("storagekey", $storagekey);
		return $this->con->invoke($cc);
	}
	/**
	 * 
	 * "prepares" file for encode
	 * @param string $storagekey
	 * storagekey
	 * @return
	 * array of string containing recommendations
	 */
	public function prepare($storagekey)
	{
		$cc=call::create("api","basic/encoder/prepare")->addParam("storagekey", $storagekey);
		return $this->con->invoke($cc);		
	}
	/**
	 * 
	 * creates link for thumbnail/slideshow
	 * @param string $storagekey
	 * @param string $type
	 * jpeg or gif
	 * @param int $n
	 * for jpeg only: retrieve image number 1 to 10
	 * @return
	 * string with url to image
	 */
	public function resolve($storagekey,$type,$n=-1) 
	{
		$cc=call::create("api","basic/encoder/resolve")->addParam("storagekey", $storagekey)->addParam("type", $type)->addParam("n", $n);
		return $this->con->invoke($cc);
	}
	/**
	 * 
	 * retrieves html template snippets for video for manual insertion
	 * @param string $storagekey
	 * @return
	 * array of string
	 * string[0] : <meta></meta> content
	 * string[1] : <div></div> content for player
	 */
	public function getTemplate($storagekey,$width="100%",$height="100%",$imagetype="gif",$imagenum="1")
	{
		$cc=call::create("api","basic/encoder/getTemplate")->addParam("storagekey", $storagekey)->addParam("width", $width)->addParam("height", $height)->addParam("imagetype",$imagetype)->addParam("imagenum",$imagenum);
		return $this->con->invoke($cc);				
	}	
	/**
	 * 
	 * insert video into html template
	 * requires:
	 * valid <meta/> tag
	 * <div id="video"/> tag
	 * @param string $template
	 * valid html template
	 * @param string $storagekey
	 * storagekey
	 * @return
	 * modified html template
	 */
	public function insertTemplate($template,$storagekey)
	{
		$cc=call::create("api","basic/encoder/getTemplate")->addParam("template", $template)->addParam("storagekey", $storagekey);
		return $this->con->invoke($cc);				
	}		
	/**
	 * 
	 * retrieves jobstate
	 * @param string $jobid
	 * jobid
	 * @return 
	 * jobstate
	 */
	public function getState($jobid)
	{
		$cc=call::create("api","basic/encoder/getState")->addParam("jobid", $jobid);
		return $this->con->invoke($cc);				
	}
	
	/**
	 * 
	 * retrieves file format
	 * @param string $storagekey
	 * storagekey
	 * @param string $extension
	 * extension	 
	 * @return 
	 * format object
	 */
	public function getFormat($storagekey,$extension)
	{
		$cc=call::create("api","basic/encoder/getFormat")->addParam("storagekey", $storagekey)->addParam("extension", $extension);
		return $this->con->invoke($cc);				
	}	
	/*  overlay(string overlay, string background, int sourcenr, int targetnr)*/ 
	public function picoverlay($overlay,$background,$sourcenr,$targetnr)
	{
		$cc=call::create("api","basic/encoder/overlay")->addParam("overlay", $overlay)->addParam("background", $background)->addParam("sourcenr", $sourcenr)->addParam("targetnr", $targetnr);
		return $this->con->invoke($cc);	
	}
	public function getExtensions($storagekey)
	{
		$cc=call::create("api","basic/encoder/getExtensions")->addParam("storagekey", $storagekey);
		return $this->con->invoke($cc);
	}
}