<?php

/**
 * 
 * standard api exception thrown 
 * @author winand appelhoff
 */
	class wexception extends Exception
	{
		/**
		 * @var string $src
		 * source of exception
		 */
		public $src; 
		/**
		 * \cond 
		 */
		public function __construct($message, $code,$_src="") 
		{
			parent::__construct($message, $code);
			$this->src=$_src;
		}
		/**
		 * \endcond
		 */
	}
/**
 * \cond
 */
	class call
	{
		private $module;
		private $path;
		private $sess_required=true;
		public $_params=array();
		
		public function __construct($mod,$func=null)
		{
			$this->module=$mod;$this->path=$func;
		}
		
		public static function create($mod,$func=null)
		{
			return new call($mod,$func);
		}
		
		public function  addParam($name, $o) { $this->_params[$name] = $o; return $this; }
		
		public function serialize($protocol=null)
		{
			return "/" . $this->module . "/"  . ($protocol != null ? $protocol . "/" : "") . ($this->path != null ? $this->path : "");
		}			
	}
	/**
	 * 
	 * \endcond
	 */
	
	/**
	 * basic connector interface
	 * @author winand appelhoff
	 */
	interface iconnector
    {
    	/**
    	 * 
    	 * connects to specified host/port
    	 * @param string $host 
    	 * path to host
    	 * @param string $port 
    	 * port (usually 80)
    	 */
        function connect($host,$port);  
        /**
         * \cond
         */      
        function &invoke(call $c);
        /*function uploadFile($fname);
        function purgeFile($storagekey);*/
        /**
         * \endcond
         */
    }    

?>