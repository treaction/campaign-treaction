<?php
require_once 'iconnector.php';
require_once \DIR_Contrib_Pear . 'HTTP/Request2.php';

/**
 * 
 * basic httpconnector
 * @author winand appelhoff
 *
 */
	class httpconnector implements iconnector
	{
		private $h;
		private $p;
		private $sessionid=null;
		
		private $shared;
		private $secret;
		private $target=null;
		/**
		 * 
		 * constructor
		 * @param string $shared_key
		 * @param string $secret_key
		 */
		public function __construct($shared_key,$secret_key,$t=NULL)
		{			
			$this->shared=$shared_key;$this->secret=$secret_key;
			$this->readSession();
		}
		/**
		 * (non-PHPdoc)
		 * @see iconnector::connect()
		 */
		
		/**
		 * 
		 * impersonisation for administrative users only : don't even bother to try ;-)
		 * @param string $t
		 */
		public function setTarget($t)
		{
			$this->target=$t;
			$this->readSession();
		}
		
        public function connect($host,$port,$sessid=NULL)
        {
        	$this->h=$host;$this->p= ( $port!=0? ':' . $port : '');
        	$this->readSession();
        }
        private function readSession()
        {
       		$f=sys_get_temp_dir() . '/' . $this->shared . ($this->target!=NULL? '_' . $this->target . '_ssid.tmp': '_ssid.tmp');
       		if(file_exists($f))
       		{
       			$this->sessionid=file_get_contents($f);
       		}
       		else 
       			$this->sessionid=NULL;
        }        
        /**
         *  \cond
         */
        private function session()
        {
        	if($this->sessionid==NULL)
        	{
        		$auth='http://' . $this->h . $this->p . '/auth/' . $this->shared;
        		if($this->target!=NULL)
        			$auth = $auth . '/' . $this->target;
        		$areq=new HTTP_Request2($auth);
				$areq->setAdapter('curl');
        		$rnd=$areq->send();
        		$num=$rnd->getBody();
        		if($num=="unknown user")
        			throw new wexception("authorization failed", 0xf001);
        		$this->sessionid=base64_encode(sha1($this->secret . $num,true));
        		$f=sys_get_temp_dir() . '/' . $this->shared . ($this->target? '_' . $this->target . '_ssid.tmp': '_ssid.tmp');
        		file_put_contents($f,$this->sessionid);
        	}        	
        } 
        public function &invoke(call $c)
        {
        	try {        		
        		$this->session();	
        	
	        	$url='http://' . $this->h . $this->p . $c->serialize('json');
	        	$req = new HTTP_Request2($url);
				$req->setAdapter('curl');
	        	$vars=array();
	        	foreach ($c->_params as $k=>$v)
	        	{
	        		$vars[$k]= json_encode($v );
	        	}	        	
	        	
	        	$req->setHeader("sessid",$this->sessionid);
	        	$req->setMethod(HTTP_Request2::METHOD_POST)->addPostParameter($vars);
	        	$req->setConfig("timeout",20000);
	        	$req->setConfig("buffer_size",128*1024*1024);
	        	$resp=$req->send();
				
	        	$respstring=$resp->getBody();
				
	        	$ret= json_decode($respstring);
	        	if($ret instanceof stdClass && property_exists($ret,"code"))
	        	{
	        		if($ret->code==0xf000)  // sessionid unknown; reset & auth
	        		{
	        			$this->sessionid=NULL;
	        			return $this->invoke($c);
	        		}
	        		else 
	        			throw new wexception($ret->msg, $ret->code,$ret->src);
	        	}
	        	else
	        		return $ret;
        	}
        	catch(Exception $e)
        	{
        		throw new wexception($e->getMessage(), $e->getCode()? $e->getCode() : 0x0001);
        	}
        }
        public function uploadFile($fname,$description=NULL)
        {
        	try 
        	{
        		$this->sessionid=NULL;
	        	$this->session();
	        	$url='http://' . $this->h . $this->p . "/fs/push".($description!=NULL?"/".$description:"");
	        	$req = new HTTP_Request2($url);
	        	$req->setMethod(HTTP_Request2::METHOD_POST);
	        	$req->setHeader("sessid",$this->sessionid);
	        	$req->setConfig("timeout",1800);
	        	$req->addUpload("data", $fname);
	        	$resp=$req->send();
	        	return $resp->getBody();
        	}
        	catch (Exception $e)
        	{
        		throw new wexception($e->getMessage(), $e->getCode()? $e->getCode() : 0x0001);
        	}	    	
        }
	        public function purgeFile($storagekey)
        {
        	try 
        	{
        		$this->sessionid=NULL;
	        	$this->session();
	        	$url='http://' . $this->h . $this->p . "/fs/purge/".$storagekey;
	        	$req = new HTTP_Request2($url);
	        	$req->setMethod(HTTP_Request2::METHOD_POST);
	        	$req->setHeader("sessid",$this->sessionid);
	        	$resp=$req->send();
	        	return $resp->getBody();
        	}
        	catch (Exception $e)
        	{
        		throw new wexception($e->getMessage(), $e->getCode()? $e->getCode() : 0x0001);
        	}	    	
        }        
        /**
         * \endcond
         */		
	}

?>