<?php
$userRepository = new \Packages\Core\Domain\Repository\UserRepository();
$userRepository->setDatabaseHandle($systemConnectionHandle);
$userRepository->setTable('benutzer');
$userRepository->setPrimaryField('benutzer_id');
$userRepository->setFetchClass('\\Packages\\Core\\Domain\\Model\\UserEntity');
$userRepository->init();