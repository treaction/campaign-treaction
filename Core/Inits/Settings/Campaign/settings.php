<?php
$repositoryClassDataArray['campaign'] = '\\Modules\\Campaign\\Domain\\Repository\\CampaignRepository';

$moduleSettingsDataArray['campaign'] = array(
	'repositorySettings' => array(
		'campaign' => array(
			'table' => 'kampagnen',
			'primaryField' => 'k_id',
			'fetchClass' => '\\Modules\\Campaign\\Domain\\Model\\CampaignEntity'
		),
		'contactPerson' => array(
			'contactPersonTable' => 'kunde_ap',
			'contactPersonPrimaryField' => 'ap_id',
			'contactPersonFetchClass' => '\\Modules\\Customer\\Domain\\Model\\ContactPersonEntity'
		),
		'customer' => array(
			'customerTable' => 'kunde',
			'customerPrimaryField' => 'kunde_id',
			'customerFetchClass' => '\\Modules\\Customer\\Domain\\Model\\CustomerEntity'
		),
		'campaignAdvertising' => array(
			'campaignAdvertisingTable' => 'campaign_advertising',
			'campaignAdvertisingPrimaryField' => 'id',
			'campaignAdvertisingFetchClass' => '\\Modules\\Campaign\\Domain\\Model\\CampaignAdvertisingEntity'
		),
		'log' => array(
			'logTable' => 'log',
			'logPrimaryField' => 'logid',
			'logFetchClass' => '\\Modules\\Campaign\\Domain\\Model\\LogEntity'
		),
	),
	'moduleSettings' => array(
		'cmVorgabeO' => $cmVorgabeO, // $cm_vorgabe_o
		'cmVorgabeK' => $cmVorgabeK, // $cm_vorgabe_k
		'cmVorgabeUsq' => $cmVorgabeUsq // $cm_vorgabe_usq
	)
);