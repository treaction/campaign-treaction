<?php
namespace Packages\Core\Utility;


/**
 * Description of FileUtility
 *
 * @author Cristian.Reus
 */
final class FileUtility {
	
	/**
	 *mappingFileTypeDataArray
	 * 
	 * @var array 
	 */
		public static $mappingFileTypeDataArray = array(
		'html' => array(
			'html',
			'htm'
		),
		'text' => array(
			'txt'
		),
		'image' => array(
			'jpeg',
			'gif',
			'png',
			'bmp',
			'tiff',
			'jpg',
		),
		'blacklist' => array(
			'csv',
			'xls',
			'xlsx'
		),
		'others' => array()
	);
	
	/**
	 * isDir
	 * 
	 * @param string $filename
	 * @return boolean
	 * @throws \RuntimeException
	 */
	public static function isFile($filename) {
		if (!\is_file($filename)) {
			throw new \RuntimeException('no valid file given: ' . \htmlspecialchars($filename), 1446046402);
		}
		
		return true;
	}
	
	/**
	 * isReadable
	 * 
	 * @param string $filename
	 * @return boolean
	 * @throws \RuntimeException
	 */
	public static function isReadable($filename) {
		self::isFile($filename);
		
		if (!\is_readable($filename)) {
			throw new \RuntimeException('file is not readable: ' . \htmlspecialchars($filename), 1446046412);
		}
		
		return true;
	}
	
	/**
	 * isWritable
	 * 
	 * @param string $filename
	 * @return boolean
	 * @throws \RuntimeException
	 */
	public static function isWritable($filename) {
		self::isFile($filename);
		
		if (!\is_writable($filename)) {
			throw new \RuntimeException('file/dir is not writable: ' . \htmlspecialchars($filename), 1446046422);
		}
		
		return true;
	}
	
	/**
	 * moveFile
	 * 
	 * @param string $source
	 * @param string $destination
	 * @return boolean
	 * @throws \RuntimeException
	 */
	public static function moveFile($source, $destination) {
		self::isFile($source);
		
		if (!\rename($source, $destination)) {
			throw new \RuntimeException('file can not be moved: ' . \htmlspecialchars($source), 1447923454);
		}
		
		return true;
	}
       /**
	 * copyFile
	 * 
	 * @param string $source
	 * @param string $destination
	 * @return boolean
	 * @throws \RuntimeException
	 */
	public static function copyFile($source, $destination) {
		self::isFile($source);
		
		if (!\copy($source, $destination)) {
			throw new \RuntimeException('file can not be moved: ' . \htmlspecialchars($source), 1447923454);
		}
		
		return true;
	}
	
	/**
	 * createDirectory
	 * 
	 * @param string $pathname
	 * @return boolean
	 * @throws \RuntimeException
	 */
	public static function createDirectory($pathname) {
		if (!\file_exists($pathname)) {
			if (!\mkdir($pathname)) {
				throw new \RuntimeException('directory can not be created: ' . \htmlspecialchars($pathname), 1447925600);
			}
		}
		
		return true;
	}
	
	/**
	 * readFileIntoString
	 * 
	 * @param string $filename
	 * @return string
	 */
	public static function readFileIntoString($filename) {
		self::isReadable($filename);
		
		return \file_get_contents($filename);
	}
        
                
        /**
         * putFileContent
         * 
         * @param string $filename
             * @param string $data
         * @return string
         */
        public static function putFileContent($filename, $data) {
            self::isReadable($filename);
            if (!\file_put_contents($filename, $data)) {
                throw new \RuntimeException('file can not be put: ' . \htmlspecialchars($filename), 144792323454);
            }

            return true;
        } 

	
	/**
	 * createDownloadHeader
	 * 
	 * @param string $fileWidthPath
	 * @param string $filename
	 * @param boolean $removeFile
	 * @return mixed
	 */
	public static function createDownloadHeader($fileWidthPath, $filename, $removeFile = FALSE) {
		\header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		\header('Cache-Control: no-store, no-cache, must-revalidate');
		\header('Cache-Control: post-check=0, pre-check=0', false);
		\header('Content-Description: File Transfer');
		\header('Content-Type: application/octet-stream');
		\header('Content-Disposition: attachment; filename="' . $filename . '"');
		\header('Content-Length: ' . \filesize($fileWidthPath));

		\readfile($fileWidthPath);
		if ($removeFile) {
			\unlink($fileWidthPath);
		}
		exit;
	}
}