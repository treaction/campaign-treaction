<?php
namespace Packages\Core\Utility;

use Packages\Core\Utility\MailUtility;


/**
 * Description of ExceptionUtility
 *
 * @author Cristian.Reus
 */
class ExceptionUtility {
	
	/**
	 * getSimpleExceptionResultDataArray
	 * 
	 * @param \Exception $e
	 * @return array
	 */
	public static function getSimpleExceptionResultDataArray(\Exception $e) {
		$resultDataArray = array();
		do {
			$resultDataArray[\get_class($e)] = array(
				'message' => $e->getMessage(),
				'code' => $e->getCode(),
				'line' => $e->getLine(),
				'file' => $e->getFile()
			);
		} while($e = $e->getPrevious());
		
		return $resultDataArray;
	}
	
	/**
	 * sendDebugData
	 * 
	 * @param mixed $data
	 * @param string $title
	 * @return void
	 */
	public static function sendDebugData($exceptionData, $title = '') {
		if (\strlen($title) > 0) {
			MailUtility::$subject = $title;
		}

		if ($exceptionData instanceof \Exception) {
			$messageDataArray = self::getSimpleExceptionResultDataArray($exceptionData);
		} else {
			$messageDataArray = $exceptionData;
		}
		
		/**
		 * MailUtils::sendMail
		 */
		MailUtility::sendMail(self::createMessageData($messageDataArray));
		unset($messageDataArray);
	}
	

	/**
	 * createMessageData
	 * 
	 * @param array|object $data
	 * @param string|integer $key
	 * @return string
	 */
	private static function createMessageData($data, $key = '') {
		$retArr = array();
		foreach ((array) $data as $k => $v) {
			if ((!empty($key)) 
				|| ($key === 0)
			) {
				$k = $key . ' - ' . $k;
			}

			if (\is_array($v) 
				|| \is_object($v)
			) {
				\array_push(
					$retArr,
					self::createMessageData($v, $k)
				);
			} else {
				\array_push(
					$retArr,
					$k . ': ' . $v
				);
			}
		}

		return \implode(chr(11), $retArr);
	}
}
