<?php
namespace Packages\Core\Utility\Format;


/**
 * Description of NumberUtility
 *
 * @author Cristian.Reus
 */
final class NumberUtility {
	
	/**
	 * euroSignInUtf8Unicode
	 * 
	 * @param string
	 */
	const euroSignInUtf8Unicode = ' €';
	
	/**
	 * euroSignHtmlCode
	 * 
	 * @param string
	 */
	const euroSignHtmlCode = ' &euro;';
	
	
	
	
	
	/**
	 * sumFormat
	 * 
	 * @param float $number
	 * @param integer $decimal
	 * @return numeric
	 */
	public static function sumFormat($number, $decimal = 2) {
		return \number_format($number, $decimal, ',', '.');
	}

	/**
	 * numberFormat
	 * 
	 * @param float $number
	 * @return float
	 */
	public static function numberFormat($number) {
		return \number_format($number, 0, '', '.');
	}

	/**
	 * rateCalculation
	 * 
	 * @param integer $value1
	 * @param integer $value2
	 * @return float
	 */
	public static function rateCalculation($value1, $value2) {
		$result = 0;
		if ((int) $value1 > 0 
			&& (int) $value2 > 0
		) {
			$result = \round((($value1 / $value2) * 100), 2);
		}
		
		return $result;
	}
	
	/**
	 * averageCalculation
	 * 
	 * @param integer $value1
	 * @param integer $value2
	 * @return float
	 */
	public static function averageCalculation($value1, $value2) {
		$result = 0;
		if ((int) $value1 > 0 
			&& (int) $value2 > 0
		) {
			$result = \round(($value1 / ($value2 / 1000)), 2);
		}
		
		return $result;
	}
	
	
	/**
	* cleanPriceContent
	*
	* @param string $productNetPrice
	* @return float
	*/
	public static function cleanPriceContent($productNetPrice) {
		return \str_replace(\trim(self::euroSignHtmlCode), '', \strip_tags($productNetPrice));
	}

}