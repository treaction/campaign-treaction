<?php
namespace Packages\Core\Utility;


/**
 * Description of EncodingUtility
 *
 * @author Cristian.Reus
 */
class EncodingUtility {
	
	/**
	 * encodeToUtf8
	 * 
	 * @param string $string
	 * @return string
	 */
	public static function encodeToUtf8($string) {
		return \mb_convert_encoding($string, 'UTF-8', \mb_detect_encoding($string, 'UTF-8, ISO-8859-1, ISO-8859-15', true));
	}
	
	/**
	 * encodeToIso
	 * 
	 * @param string $string
	 * @return string
	 */
	public static function encodeToIso($string) {
		return \mb_convert_encoding($string, 'ISO-8859-1', \mb_detect_encoding($string, 'UTF-8, ISO-8859-1, ISO-8859-15', true));
	}
}
