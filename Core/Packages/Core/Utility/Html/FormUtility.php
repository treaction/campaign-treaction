<?php
namespace Packages\Core\Utility\Html;

use Packages\Core\Utility\Html\AbstractUtility;


/**
 * Description of FormUtility
 *
 * @author Cristian.Reus
 */
final class FormUtility extends AbstractUtility {
	
	/**
	 * createOptionListItemData
	 * 
	 * @param array $dataArray
	 * @param string|integer $actualValue
	 * @return string
	 */
	public static function createOptionListItemData(array $dataArray, $actualValue) {
		$result = '';

		foreach ($dataArray as $key => $value) {
			$selected = '';
			if ((string) $actualValue === (string) $key) {
				$selected = 'selected';
			}

			$result .= self::createOptionItem(
				array(
					'value' => $key,
					'selected' => $selected
				),
				$value
			);
		}

		return $result;
	}

	/**
	 * createOptionListItemDataWithoutKey
	 * 
	 * @param array $dataArray
	 * @param mixed $actualValue
	 * @return string
	 */
	public static function createOptionListItemDataWithoutKey(array $dataArray, $actualValue) {
		$result = '';

		foreach ($dataArray as $value) {
			$selected = '';
			if ((string) $actualValue === (string) $value) {
				$selected = 'selected';
			}

			$result .= self::createOptionItem(
				array(
					'value' => $value,
					'selected' => $selected
				),
				$value
			);
		}

		return $result;
	}

	/**
	 * createSelectItem
	 * 
	 * @param array $configDataArray
	 * @param string $optionsItems
	 * @param boolean $addDefaultItem
	 * @param string $defaultTitle
	 * @return string
	 */
	public static function createSelectItem(array $configDataArray, $optionsItems, $addDefaultItem = false, $defaultTitle = '- Bitte ausw&auml;hlen -') {
		$result = '<select ';
		foreach ($configDataArray as $key => $value) {
			$result .= $key . '="' . $value . '" ';
		}
		$result .= '/>' . \chr(13);

		if ((boolean) $addDefaultItem === true) {
			$result .= self::createOptionItem(
				array(
					'value' => ''
				),
				$defaultTitle
			);
		}

		$result .= $optionsItems . \chr(13) . '</select>';

		return $result;
	}

	/**
	 * createOptgroupDataArray
	 * 
	 * @param string $labelTitle
	 * @return array
	 */
	public static function createOptgroupDataArray($labelTitle) {
		return array(
			'begin' => '<optgroup label="' . $labelTitle . '">',
			'end' => '</optgroup>'
		);
	}
	
	/**
	 * openSelectOptgroup
	 * 
	 * @param integer $c
	 * @param array $optgroupDataArray
	 * @return string
	 */
	public static function openSelectOptgroup($c, array $optgroupDataArray) {
		$result = '';
		
		if ($c > 1) {
			/**
			 * closeSelectOptgroup
			 */
			$result .= self::closeSelectOptgroup($optgroupDataArray);
		}
		
		// openSelectOptgroup
		$result .= $optgroupDataArray['begin'];
		
		return $result;
	}
	
	/**
	 * closeSelectOptgroup
	 * 
	 * @param array $optgroupDataArray
	 * @return string
	 */
	public static function closeSelectOptgroup(array $optgroupDataArray) {
		return $optgroupDataArray['end'];
	}
	
	/**
	 * createOptionItem
	 * 
	 * @param array $configDataArray
	 * @param string $title
	 * @return string
	 */
	public static function createOptionItem(array $configDataArray, $title) {
		$result = '<option ';
		foreach ($configDataArray as $key => $value) {
			if ($key == 'value') {
				if (\strlen($value) > 0) {
					$result .= $key . '="' . $value . '" ';
				} else {
					$result .= $key . '="" ';
				}
			} elseif (\strlen($value) > 0) {
				$result .= $key . '="' . $value . '" ';
			}
		}
		$result .= '>' . $title . '</option>' . \chr(13);

		return $result;
	}

	/**
	 * createHiddenField
	 * 
	 * @param string $name
	 * @param string $value
	 * @return string
	 */
	public static function createHiddenField($name, $value) {
		return '<input type="hidden" name="' . $name . '" value="' . $value . '" />';
	}

	/**
	 * createCheckbox
	 * 
	 * @param array $configDataArray
	 * @return string
	 */
	public static function createCheckbox(array $configDataArray) {
		return self::createInputField(
			$configDataArray,
			'checkbox'
		);
	}

	/**
	 * createCheckboxItem
	 * 
	 * @param array $configDataArray
	 * @param string $title
	 * @return string
	 */
	public static function createCheckboxItem(array $configDataArray, $title) {
		return self::createCheckbox($configDataArray) . ' ' . $title . \chr(13);
	}

	/**
	 * createCheckboxItemWidthLabel
	 * 
	 * @param array $checkboxConfigDataArray
	 * @param string $labelTitle
	 * @param array $labelConfigDataArray
	 * @return string
	 */
	public static function createCheckboxItemWidthLabel(array $checkboxConfigDataArray, $labelTitle, array $labelConfigDataArray = array()) {
		return self::createCheckbox($checkboxConfigDataArray)
			. self::createLabelFieldItem(
				$labelTitle,
				$labelConfigDataArray
			) . \chr(13);
	}

	/**
	 * createLabelFieldItem
	 * 
	 * @param string $title
	 * @param array $configDataArray
	 * @return string
	 */
	public static function createLabelFieldItem($title, array $configDataArray = array()) {
		$result = '<label ';

		if (\count($configDataArray) > 0) {
			$result .= self::processConfigDataArray($configDataArray);
		}
		$result .= '>' . $title . '</label>' . \chr(13);

		return $result;
	}

	/**
	 * createRadioboxItemWidthLabel
	 * 
	 * @param array $radioboxConfigDataArray
	 * @param string $labelTitle
	 * @param array $labelConfigDataArray
	 * @return string
	 */
	public static function createRadioboxItemWidthLabel(array $radioboxConfigDataArray, $labelTitle, array $labelConfigDataArray = array()) {
		return self::createRadiobox($radioboxConfigDataArray)
			. ' ' . self::createLabelFieldItem(
				$labelTitle,
				$labelConfigDataArray
			) . \chr(13);
	}

	/**
	 * createRadiobox
	 * 
	 * @param array $configDataArray
	 * @return string
	 */
	public static function createRadiobox(array $configDataArray) {
		return self::createInputField(
			$configDataArray,
			'radio'
		);
	}

	/**
	 * createButton
	 * 
	 * @param array $configDataArray
	 * @param string $title
	 * @param string $type
	 * @return string
	 */
	public static function createButton(array $configDataArray, $title, $type = 'button') {
		return '<button type="' . $type . '" ' 
			. self::processConfigDataArray($configDataArray) 
		. '>' . $title . '</button>';
	}
	
	/**
	 * createInputField
	 * 
	 * @param array $configDataArray
	 * @param string $type
	 * @return string
	 */
	public static function createInputField(array $configDataArray, $type = 'text') {
		return '<input type="' . $type . '" ' 
			. self::processConfigDataArray($configDataArray) 
		. '/>';
	}

}