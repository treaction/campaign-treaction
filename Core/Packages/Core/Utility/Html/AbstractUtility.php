<?php
namespace Packages\Core\Utility\Html;


/**
 * Description of AbstractUtility
 *
 * @author Cristian.Reus
 */
abstract class AbstractUtility {

	/**
	 * processConfigDataArray
	 * 
	 * @param array $configDataArray
	 * @return string
	 */
	static protected function processConfigDataArray(array $configDataArray) {
		$content = '';

		foreach ($configDataArray as $key => $value) {
			if (\strlen($value) > 0) {
				$content .= $key . '="' . $value . '" ';
			}
		}

		return \rtrim($content);
	}

}