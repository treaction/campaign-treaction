<?php
namespace Packages\Core\Utility;

/**
 * Description of FilterUtility
 *
 * @author Cristian.Reus
 */
final class FilterUtility {
	
	/**
	 * validateFilterDataArray
	 * 
	 * @var array
	 */
	public static $validateFilterDataArray = array(
		'email' => \FILTER_SANITIZE_EMAIL,
		'encoded' => \FILTER_SANITIZE_ENCODED,
		'number_float' => \FILTER_SANITIZE_NUMBER_FLOAT,
		'number_int' => \FILTER_SANITIZE_NUMBER_INT,
		'special_chars' => \FILTER_SANITIZE_SPECIAL_CHARS,
		'full_special_chars' => \FILTER_SANITIZE_FULL_SPECIAL_CHARS,
		'string' => \FILTER_SANITIZE_STRING,
		'url' => \FILTER_SANITIZE_URL
	);
	
	
	
	/**
	 * filterData
	 * 
	 * @param string $string
	 * @param integer $filter
	 * @param null|array  $options
	 * @return boolean|string
	 */
	public static function filterData($string, $filter = \FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options = NULL) {
		return \filter_var(\trim($string), $filter, $options);
	}
	
	/**
	 * filterArrayData
	 * 
	 * @param array $dataArray
	 * @return array|boolean
	 */
	public static function filterArrayData(array $dataArray) {
		return \filter_var_array($dataArray, self::$validateFilterDataArray['string']);
	}

	/**
	 * utf8DecodeAndFilterData
	 * 
	 * @param string $string
	 * @param integer $filter
	 * @param null|array $options
	 * @return boolean|string
	 */
	public static function utf8DecodeAndFilterData($string, $filter = \FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options = NULL) {
		return self::filterData(
			\utf8_decode($string),
			$filter,
			$options
		);
	}

	/**
	 * utf8EncodeAndFilterData
	 * 
	 * @param string $string
	 * @param integer $filter
	 * @param null|array $options
	 * @return boolean|string
	 */
	public static function utf8EncodeAndFilterData($string, $filter = \FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options = NULL) {
		return self::filterData(
			\utf8_encode($string),
			$filter,
			$options
		);
	}

	/**
	 * convertDataToFloatvalue
	 * 
	 * @param string $string
	 * @return float
	 */
	public static function convertDataToFloatvalue($string) {
		$string = \trim($string);

		if (\strstr($string, ',')) {
			// replace dots (thousand seps) with blancs 
			$string = \str_replace('.', '', $string);

			// replace ',' with '.' 
			$string = \str_replace(',', '.', $string);
		}

		// search for number that may contain '.'
		if (\preg_match('#([0-9\.]+)#', $string, $match)) {
			$result = \floatval($match[0]);
		} else {
			// take some last chances with floatval
			$result = \floatval($string);
		}

		return $result;
	}
	
	
	/**
	 * isInteger
	 * 
	 * @param mixed $input
	 * @return boolean
	 */
	public static function isInteger($input) {
		return(\ctype_digit(\strval($input)));
	}
}