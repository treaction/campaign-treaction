<?php
namespace Packages\Core\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * Description of StaticCountryEntity
 *
 * @author Cristian.Reus
 */
class StaticCountryEntity extends AbstractEntity {
	
	/**
	 * uid
	 * 
	 * @var integer
	 */
	protected $uid;
	
	/**
	 * cn_iso_2
	 * 
	 * @var string
	 */
	protected $cn_iso_2;
	
	/**
	 * cn_iso_3
	 * 
	 * @var string
	 */
	protected $cn_iso_3;
	
	/**
	 * cn_tldomain
	 * 
	 * @var string
	 */
	protected $cn_tldomain;
	
	/**
	 * cn_phone
	 * 
	 * @var integer
	 */
	protected $cn_phone;
	
	/**
	 * cn_short_de
	 * 
	 * @var string
	 */
	protected $cn_short_de;
	
	
	
	public function __construct($primaryFieldName = 'uid') {
		parent::__construct($primaryFieldName);
	}
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter
     *
     *******************************************************************************************/
	public function getUid() {
		return (int) $this->uid;
	}
	public function setUid($uid) {
		$this->uid = \intval($uid);
	}
	
	public function getCn_iso_2() {
		return $this->cn_iso_2;
	}
	public function setCn_iso_2($cn_iso_2) {
		$this->cn_iso_2 = $cn_iso_2;
	}

	public function getCn_iso_3() {
		return $this->cn_iso_3;
	}
	public function setCn_iso_3($cn_iso_3) {
		$this->cn_iso_3 = $cn_iso_3;
	}

	public function getCn_tldomain() {
		return $this->cn_tldomain;
	}
	public function setCn_tldomain($cn_tldomain) {
		$this->cn_tldomain = $cn_tldomain;
	}

	public function getCn_phone() {
		return (int) $this->cn_phone;
	}
	public function setCn_phone($cn_phone) {
		$this->cn_phone = \intval($cn_phone);
	}

	public function getCn_short_de() {
		return $this->cn_short_de;
	}
	public function setCn_short_de($cn_short_de) {
		$this->cn_short_de = $cn_short_de;
	}

}