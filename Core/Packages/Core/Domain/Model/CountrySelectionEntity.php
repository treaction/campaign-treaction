<?php
namespace Packages\Core\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * Description of CountrySelectionEntity
 *
 * @author Cristian.Reus
 */
class CountrySelectionEntity extends AbstractEntity {
	
	/**
	 * id
	 * 
	 * @var integer
	 */
	protected $id;
	
	/**
	 * title
	 * 
	 * @var string
	 */
	protected $title;
	
	/**
	 * active
	 * 
	 * @var boolean
	 */
	protected $active;
	
	/**
	 * set_as_default
	 * 
	 * @var boolean
	 */
	protected $set_as_default;
	
	/**
	 * short_title
	 * 
	 * @var string
	 */
	protected $short_title;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter
     *
     *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}

	public function getTitle() {
		return $this->title;
	}
	public function setTitle($title) {
		$this->title = $title;
	}

	public function getActive() {
		return (boolean) $this->active;
	}
	public function setActive($active) {
		if ((int) $active === 1) {
			$active = true;
		} else {
			$active = false;
		}
		
		$this->active = $active;
	}

	public function getSet_as_default() {
		return (boolean) $this->set_as_default;
	}
	public function setSet_as_default($setAsDefault) {
		if ((int) $setAsDefault === 1) {
			$setAsDefault = true;
		} else {
			$setAsDefault = false;
		}
		
		$this->set_as_default = $setAsDefault;
	}

	public function getShort_title() {
		return $this->short_title;
	}
	public function setShort_title($shortTitle) {
		$this->short_title = $shortTitle;
	}

}