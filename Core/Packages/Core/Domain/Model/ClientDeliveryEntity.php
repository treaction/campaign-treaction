<?php
namespace Packages\Core\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * Description of ClientDeliveryEntity
 *
 * @author Cristian.Reus
 */
class ClientDeliveryEntity extends AbstractEntity {
	
	/**
	 * masp_id
	 * 
	 * @var integer
	 */
	protected $masp_id;
	
	/**
	 * m_id
	 * 
	 * @var integer
	 */
	protected $m_id;
	
	/**
	 * asp_id
	 * 
	 * @var integer
	 */
	protected $asp_id;
	
	/**
	 * m_login
	 * 
	 * @var string
	 */
	protected $m_login;
	
	/**
	 * api_user
	 * 
	 * @var string
	 */
	protected $api_user;
	
	/**
	 * pw
	 * 
	 * @var string
	 */
	protected $pw;
	
	/**
	 * active
	 * 
	 * @var boolean
	 */
	protected $active;
	
	/**
	 * shared_key
	 * 
	 * @var string
	 */
	protected $shared_key;
	
	/**
	 * secret_key
	 * 
	 * @var string
	 */
	protected $secret_key;
	
	/**
	 * deliverySystemEntity
	 * 
	 * @var \Packages\Core\Domain\Model\DeliverySystemEntity
	 * @relations 
	 */
	protected $deliverySystemEntity = NULL;
	
	
	
	/**
	 * @deprecated
	 */
	protected $profileID;
	protected $AID;
	protected $red;
	protected $verteiler;
	protected $blacklist_verteiler;
	
	
	
	public function __construct($primaryFieldName = 'masp_id') {
		parent::__construct($primaryFieldName);
	}
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getMasp_id() {
		return (int) $this->masp_id;
	}
	public function setMasp_id($masp_id) {
		$this->masp_id = \intval($masp_id);
	}

	public function getM_id() {
		return (int) $this->m_id;
	}
	public function setM_id($m_id) {
		$this->m_id = \intval($m_id);
	}

	public function getAsp_id() {
		return (int) $this->asp_id;
	}
	public function setAsp_id($asp_id) {
		$this->asp_id = \intval($asp_id);
	}

	public function getM_login() {
		return $this->m_login;
	}
	public function setM_login($m_login) {
		$this->m_login = $m_login;
	}

	public function getApi_user() {
		return $this->api_user;
	}
	public function setApi_user($api_user) {
		$this->api_user = $api_user;
	}

	public function getPw() {
		return $this->pw;
	}
	public function setPw($pw) {
		$this->pw = $pw;
	}

	public function getActive() {
		return (boolean) $this->active;
	}
	public function setActive($active) {
		if ((int) $active === 1) {
			$active = true;
		} else {
			$active = false;
		}
		
		$this->active = $active;
	}
	
	public function getShared_key() {
		return $this->shared_key;
	}
	public function setShared_key($shared_key) {
		$this->shared_key = $shared_key;
	}

	public function getSecret_key() {
		return $this->secret_key;
	}
	public function setSecret_key($secret_key) {
		$this->secret_key = $secret_key;
	}
	
	
	public function getDeliverySystemEntity() {
		return $this->deliverySystemEntity;
	}
	public function setDeliverySystemEntity(\Packages\Core\Domain\Model\DeliverySystemEntity $deliverySystemEntity) {
		$this->deliverySystemEntity = $deliverySystemEntity;
	}

		
	
	
	/**
	 * @deprecated
	 */
	public function getProfileID() {
		return $this->profileID;
	}
	public function setProfileID($profileID) {
		$this->profileID = $profileID;
	}

	public function getAID() {
		return $this->AID;
	}
	public function setAID($AID) {
		$this->AID = $AID;
	}

	public function getRed() {
		return $this->red;
	}
	public function setRed($red) {
		$this->red = $red;
	}

	public function getVerteiler() {
		return $this->verteiler;
	}
	public function setVerteiler($verteiler) {
		$this->verteiler = $verteiler;
	}

	public function getBlacklist_verteiler() {
		return $this->blacklist_verteiler;
	}
	public function setBlacklist_verteiler($blacklist_verteiler) {
		$this->blacklist_verteiler = $blacklist_verteiler;
	}

}