<?php

namespace Packages\Core\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;

/**
 * Description of HeaderFooter
 *
 * @author Sami Jarmoud
 */
class HeaderFooterEntity extends AbstractEntity {

    /**
     * id
     * 
     * @var integer
     */
    protected $id;

    /**
     * header 
     * 
     * @var string
     */
    protected $header ;
    
    /**
     * footer 
     * 
     * @var string
     */
    protected $footer;
    
    /**
     * versandsystem
     * 
     * @var string
     */
    protected $versandsystem;

    /**
     * m_id
     * 
     * @var integer
     */
    protected $m_id;
    
    /**
     * active
     * 
     * @var integer
     */
    protected $active;

    /**
     * user_id
     * 
     * @var integer
     */
    protected $user_id;
    
    /**
    * create_at
    * 
    * @var \DateTime
    */
   protected $create_at;

   /**
    * update_at
    * 
    * @var \DateTime
    */
   protected $update_at;
    
    
   function getId() {
       return $this->id;
   }

   function getHeader() {
       return $this->header;
   }

   function getFooter() {
       return $this->footer;
   }

   function getVersandsystem() {
       return $this->versandsystem;
   }

   function getMId() {
       return $this->m_id;
   }

   function getActive() {
       return $this->active;
   }

   function getUser_id() {
       return $this->user_id;
   }

   function getCreate_at() {
       return $this->create_at;
   }

   function getUpdate_at() {
       return $this->update_at;
   }

   function setId($id) {
       $this->id = $id;
   }

   function setHeader($header) {
       $this->header = $header;
   }

   function setFooter($footer) {
       $this->footer = $footer;
   }

   function setVersandsystem($versandsystem) {
       $this->versandsystem = $versandsystem;
   }

   function setMId($m_id) {
       $this->m_id = $m_id;
   }

   function setActive($active) {
       $this->active = $active;
   }

   function setUser_id($user_id) {
       $this->user_id = $user_id;
   }

   function setCreate_at($create_at) {
       $this->create_at = $create_at;
   }

   function setUpdate_at($update_at) {
       $this->update_at = $update_at;
   }





}
