<?php
namespace Packages\Core\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * Description of ClientEntity
 *
 * @author Cristian.Reus
 */
class ClientEntity extends AbstractEntity {
	
	/**
	 * id
	 * 
	 * @var integer
	 */
	protected $id = NULL;
	
	/**
	 * mandant
	 * 
	 * @var string
	 */
	protected $mandant;
	
	/**
	 * package
	 * 
	 * @var string 
	 */
	protected $package;
	
	/**
	 * abkz
	 * 
	 * @var string 
	 */
	protected $abkz;
	
	/**
	 * unsubkey
	 * 
	 * @var string
	 */
	protected $unsubkey;
	
	/**
	 * dm_rep_mail
	 * 
	 * @var string
	 */
	protected $dm_rep_mail;
	
	/**
	 * dm_rep_betreff
	 * 
	 * @var string
	 */
	protected $dm_rep_betreff;
	
	/**
	 * untermandant
	 * 
	 * @var string
	 */
	protected $untermandant;
	
	/**
	 * cm_rep_mail
	 * 
	 * @var string
	 */
	protected $cm_rep_mail;
	
	/**
	 * cm_rep_betreff
	 * 
	 * @var string
	 */
	protected $cm_rep_betreff;
	
	/**
	 * cm_zustellung_betreff
	 * 
	 * @var string
	 */
	protected $cm_zustellung_betreff;
	
	/**
	 * cm_zustellung_mail
	 * 
	 * @var string
	 */
	protected $cm_zustellung_mail;
	
	/**
	 * cm_rechnung_betreff
	 * 
	 * @var string
	 */
	protected $cm_rechnung_betreff;
	
	/**
	 * cm_rechnung_mail
	 * 
	 * @var string
	 */
	protected $cm_rechnung_mail;
	
	/**
	 * has_multiple_distributors
	 * 
	 * @var boolean
	 */
	protected $has_multiple_distributors;
	
	/**
	 * parent_id
	 * 
	 * @var integer
	 */
	protected $parent_id;
	
	/**
	 * active
	 * 
	 * @var boolean
	 */
	protected $active;
	
	
	/**
	 * clientDeliveries
	 * 
	 * @var \Packages\Core\Persistence\ObjectStorage<\Packages\Core\Domain\Model\ClientDeliveryEntity> 
	 * @relations
	 */
	protected $clientDeliveries;
	
	/**
	 * clientClickProfiles
	 * 
	 * @var \Packages\Core\Persistence\ObjectStorage<\Packages\Core\Domain\Model\ClientClickProfileEntity> 
	 * @relations
	 */
	protected $clientClickProfiles;
	
	
	/**
	 * @deprecated
	 */
	/**
	 * crm_hash
	 * 
	 * @var string
	 * @deprecated
	 */
	protected $crm_hash;
	
	/**
	 * intone_group
	 * 
	 * @var boolean
	 * @deprecated
	 */
	protected $intone_group;
	
	/**
	 * broadmail_import
	 * 
	 * @var boolean
	 * @deprecated
	 */
	protected $broadmail_import;
	
	/**
	 * client_base_path
	 * 
	 * @var string
	 * @deprecated
	 */
	protected $client_base_path;
	
	
	
	public function __construct($primaryFieldName = 'id') {
		parent::__construct($primaryFieldName);
		
		$this->clientDeliveries = new \Packages\Core\Persistence\ObjectStorage();
		$this->clientClickProfiles = new \Packages\Core\Persistence\ObjectStorage();
	}
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter
     *
     *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}

	public function getMandant() {
		return $this->mandant;
	}
	public function setMandant($mandant) {
		$this->mandant = $mandant;
	}

	public function getPackage() {
		return $this->package;
	}
	public function setPackage($package) {
		$this->package = $package;
	}

	public function getAbkz() {
		return $this->abkz;
	}
	public function setAbkz($abkz) {
		$this->abkz = $abkz;
	}

	public function getUnsubkey() {
		return $this->unsubkey;
	}
	public function setUnsubkey($unsubkey) {
		$this->unsubkey = $unsubkey;
	}

	public function getDm_rep_mail() {
		return $this->dm_rep_mail;
	}
	public function setDm_rep_mail($dm_rep_mail) {
		$this->dm_rep_mail = $dm_rep_mail;
	}

	public function getDm_rep_betreff() {
		return $this->dm_rep_betreff;
	}
	public function setDm_rep_betreff($dm_rep_betreff) {
		$this->dm_rep_betreff = $dm_rep_betreff;
	}

	public function getUntermandant() {
		return $this->untermandant;
	}
	public function setUntermandant($untermandant) {
		$this->untermandant = $untermandant;
	}

	public function getCm_rep_mail() {
		return $this->cm_rep_mail;
	}
	public function setCm_rep_mail($cm_rep_mail) {
		$this->cm_rep_mail = $cm_rep_mail;
	}

	public function getCm_rep_betreff() {
		return $this->cm_rep_betreff;
	}
	public function setCm_rep_betreff($cm_rep_betreff) {
		$this->cm_rep_betreff = $cm_rep_betreff;
	}

	public function getCm_zustellung_betreff() {
		return $this->cm_zustellung_betreff;
	}
	public function setCm_zustellung_betreff($cm_zustellung_betreff) {
		$this->cm_zustellung_betreff = $cm_zustellung_betreff;
	}

	public function getCm_zustellung_mail() {
		return $this->cm_zustellung_mail;
	}
	public function setCm_zustellung_mail($cm_zustellung_mail) {
		$this->cm_zustellung_mail = $cm_zustellung_mail;
	}

	public function getCm_rechnung_betreff() {
		return $this->cm_rechnung_betreff;
	}
	public function setCm_rechnung_betreff($cm_rechnung_betreff) {
		$this->cm_rechnung_betreff = $cm_rechnung_betreff;
	}

	public function getCm_rechnung_mail() {
		return $this->cm_rechnung_mail;
	}
	public function setCm_rechnung_mail($cm_rechnung_mail) {
		$this->cm_rechnung_mail = $cm_rechnung_mail;
	}

	public function getHas_multiple_distributors() {
		return (boolean) $this->has_multiple_distributors;
	}
	public function setHas_multiple_distributors($has_multiple_distributors) {
		$this->has_multiple_distributors = (boolean) \intval($has_multiple_distributors);
	}

	public function getParent_id() {
		return (int) $this->parent_id;
	}
	public function setParent_id($parent_id) {
		$this->parent_id = \intval($parent_id);
	}
	
	public function getActive() {
		return (boolean) $this->active;
	}
	public function setActive($active) {
		if ((int) $active === 1) {
			$active = true;
		} else {
			$active = false;
		}
		
		$this->active = $active;
	}
	
	
	public function getClientDeliveries() {
		return $this->clientDeliveries;
	}
	public function setClientDeliveries(\Packages\Core\Persistence\ObjectStorage $clientDeliveries) {
		$this->clientDeliveries = $clientDeliveries;
	}
	public function addClientDeliveryEntity(\Packages\Core\Domain\Model\ClientDeliveryEntity $clientDeliveryEntity, $data = NULL) {
		$this->clientDeliveries->attach($clientDeliveryEntity, $data);
	}
	public function removeClientDeliveryEntity(\Packages\Core\Domain\Model\ClientDeliveryEntity $clientDeliveryEntity) {
		$this->clientDeliveries->detach($clientDeliveryEntity);
	}
	
	
	public function getClientClickProfiles() {
		return $this->clientClickProfiles;
	}
	public function setClientClickProfiles(\Packages\Core\Persistence\ObjectStorage $clientClickProfiles) {
		$this->clientClickProfiles = $clientClickProfiles;
	}
	public function addClientClickProfilesEntity(\Packages\Core\Domain\Model\ClientClickProfileEntity $clientClickProfilesEntity, $data = NULL) {
		$this->clientClickProfiles->attach($clientClickProfilesEntity, $data);
	}
	public function removeClientClickProfilesEntity(\Packages\Core\Domain\Model\ClientClickProfileEntity $clientClickProfilesEntity) {
		$this->clientClickProfiles->detach($clientClickProfilesEntity);
	}
		
		
	
	/**
	 * @deprecated
	 */
	public function getCrm_hash() {
		return $this->crm_hash;
	}
	public function setCrm_hash($crm_hash) {
		$this->crm_hash = $crm_hash;
	}

	public function getIntone_group() {
		return (boolean) $this->intone_group;
	}
	public function setIntone_group($intone_group) {
		$this->intone_group = (boolean) \intval($intone_group);
	}

	public function getBroadmail_import() {
		return (boolean) $this->broadmail_import;
	}
	public function setBroadmail_import($broadmail_import) {
		$this->broadmail_import = (boolean) \intval($broadmail_import);
	}

	public function getClient_base_path() {
		return $this->client_base_path;
	}
	public function setClient_base_path($client_base_path) {
		$this->client_base_path = $client_base_path;
	}

}
