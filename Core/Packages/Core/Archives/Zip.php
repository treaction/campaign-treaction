<?php
namespace Packages\Core\Archives;

/**
 * Description of Zip
 *
 * @author Cristian.Reus
 */
class Zip {
	
	/**
	 * zipArchive
	 * 
	 * @var \ZipArchive
	 */
	public $zipArchive = NULL;
	
	
	
	public function __construct() {
		$this->zipArchive = new \ZipArchive();
	}
	
	/**
	 * createArchive
	 * 
	 * @param string $filename
	 * @return \Packages\Core\Archives\Zip
	 * @throws \UnexpectedValueException
	 */
	public function createArchive($filename) {
		$result = $this->zipArchive->open(
			$filename,
			\ZipArchive::CREATE)
		;
		if ($result !== TRUE) {
			throw new \UnexpectedValueException($result, 1453802904);
		}
		
		return $this;
	}
	
	/**
	 * readArchive
	 * 
	 * @param string $filename
	 * @return \Packages\Core\Archives\Zip
	 * @throws \UnexpectedValueException
	 */
	public function readArchive($filename) {
		$result = $this->zipArchive->open($filename);
		if ($result !== TRUE) {
			throw new \UnexpectedValueException($result, 1453809647);
		}
		
		return $this;
	}
	
	/**
	 * listArchiveItems
	 * 
	 * @return array
	 */
	public function listArchiveItems() {
		$resultDataArray = array(); // TODO: auf array itterator umstellen
		for ($i = 0; $i < $this->zipArchive->numFiles; $i++) {
			$resultDataArray[] = $this->zipArchive->statIndex($i);
		}
		
		return $resultDataArray;
	}
	
	/**
	 * addFile
	 * 
	 * @param string $filename
	 * @param string $localFilename
	 * @return void
	 * @throws \DomainException
	 * @throws \UnexpectedValueException
	 */
	public function addFile($filename, $localFilename = NULL) {
		if (\is_null($this->zipArchive)) {
			throw new \DomainException('no archive created!', 1453807981);
		}
		
		$result = $this->zipArchive->addFile(
			$filename,
			$localFilename
		);
		if ($result !== TRUE) {
			throw new \UnexpectedValueException($this->zipArchive->getStatusString(), 1453805989);
		}
	}
	
	/**
	 * addFiles
	 * 
	 * @param array $filesDataArray
	 * @return \Packages\Core\Archives\Zip
	 */
	public function addFiles(array $filesDataArray) {
		foreach ($filesDataArray as $localFilename => $filename) {
			$this->addFile(
				$filename,
				$localFilename
			);
		}
		
		return $this;
	}
	
	/**
	 * close
	 * 
	 * @return void
	 */
	public function close() {
		if (!\is_null($this->zipArchive)) {
			$this->zipArchive->close();
		}
	}
}
