<?php
namespace Packages\Core;

use Packages\Core\InterfaceSingleton;


/**
 * Description of ClassAliasMap
 *
 * @author Cristian.Reus
 */
class ClassAliasMap implements InterfaceSingleton {
	
	/**
	 * aliasToClassNameMapping

	 * @var array
	 */
	protected $aliasToClassNameMapping = array();
	
	
	
	/**
	 * getClassNameForAlias
	 * 
	 * @param string $alias
	 * @return string
	 */
	public function getClassNameForAlias($alias) {
		$lookUpClassName = \strtolower($alias);
		
		return isset($this->aliasToClassNameMapping[$lookUpClassName]) ? $this->aliasToClassNameMapping[$lookUpClassName] : $alias;
	}
}