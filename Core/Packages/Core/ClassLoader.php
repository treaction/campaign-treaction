<?php
namespace Packages\Core;

use Packages\Core\Utility\GeneralUtility;


/**
 * Description of ClassLoader
 *
 * @author Cristian.Reus
 */
class ClassLoader {
	
	/**
	 * getClassNameForAlias
	 * 
	 * @param string $alias
	 * @return string
	 */
	public static function getClassNameForAlias($alias) {
		$staticAliasMap = new \Packages\Core\ClassAliasMap();
		
		return $staticAliasMap->getClassNameForAlias($alias);
	}
	
	/**
	 * getClassNameWithoutNamespace
	 * 
	 * @param string $className
	 * @return string
	 */
	public static function getClassNameWithoutNamespace($className) {
		$pos = \strrpos($className, GeneralUtility::$namespaceDivider);
		if ($pos) {
			$result = \substr($className, $pos + 1);
		} else {
			$result = $className;
		}
		
		return \lcfirst($result);
	}
}