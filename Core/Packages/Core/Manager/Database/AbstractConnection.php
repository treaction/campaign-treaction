<?php
namespace Packages\Core\Manager\Database;

use Packages\Core\Manager\Database\InterfaceConnection;


/**
 * Description of Connection
 *
 * @author Cristian.Reus
 */
abstract class AbstractConnection implements InterfaceConnection {
	
	/**
	 * host
	 * 
	 * @var string
	 */
	protected $host = NULL;
	
	/**
	 * dbName
	 * 
	 * @var string
	 */
	protected $dbName = NULL;
	
	/**
	 * dbUser
	 * 
	 * @var string
	 */
	protected $dbUser = NULL;
	
	/**
	 * dbPassword
	 * 
	 * @var string
	 */
	protected $dbPassword = NULL;
	
	/**
	 * connection
	 * 
	 * @var mixed
	 */
	protected $connection = NULL;
	
	/**
	 * lastInsertId
	 * 
	 * @var integer
	 */
	protected $lastInsertId = 0;
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function setHost($host) {
		$this->host = $host;
	}

	public function setDbName($dbName) {
		$this->dbName = $dbName;
	}

	public function setDbUser($dbUser) {
		$this->dbUser = $dbUser;
	}

	public function setDbPassword($dbPassword) {
		$this->dbPassword = $dbPassword;
	}
	
	public function getLastInsertId() {
		return $this->lastInsertId;
	}
}
