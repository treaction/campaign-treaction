<?php
namespace Packages\Core\Log;

use Packages\Core\Log\InterfaceLog;


/**
 * Description of AbstractLog
 *
 * @author Cristian.Reus
 */
abstract class AbstractLog implements InterfaceLog {
	/**
	 * userId
	 * 
	 * @var integer
	 */
	protected $userId = 0;
	
	
	
	/**
	 * init
	 * 
	 * @return void
	 */
	abstract public function init();
	
	
	
	
	
	/* *******************************************************************************************
	 *
	 *              setter and getter
	 *
	 * ***************************************************************************************** */
	public function setUserId($userId) {
		$this->userId = \intval($userId);
	}
}