<?php
namespace Packages\Core\Log;

use Packages\Core\Log\AbstractLog;
use Packages\Core\Utility\GeneralUtility;


/**
 * Description of FirePhpLog
 *
 * @author Cristian.Reus
 */
class FirePhpLog extends AbstractLog {

	/**
	 * @var \FirePHP
	 */
	protected $firePhp = NULL;
	
	
	
	
	
	/**
	 * init
	 * 
	 * @return void
	 */
	public function init() {
		if ($this->userId === GeneralUtility::getDebugUserId()) {
			require_once(\DIR_Vendor . 'FirePHPCore/FirePHP.class.php');
			
			$this->firePhp = \FirePHP::getInstance(true);
			$this->firePhp->setOption('maxObjectDepth', 10);
			$this->firePhp->setOption('maxArrayDepth', 10);
		}
	}

	/**
	 * log
	 * 
	 * @param mixed $data
	 * @param string $header
	 * @return void
	 */
	public function log($data, $header) {
		if ($this->firePhp instanceof \FirePHP) {
			$this->firePhp->log($data, $header . ':');
		}
	}
	
	/**
	 * info
	 * 
	 * @param mixed $data
	 * @param string $header
	 * @return void
	 */
	public function info($data, $header) {
		if ($this->firePhp instanceof \FirePHP) {
			$this->firePhp->info($data, $header . ':');
		}
	}
	
	/**
	 * warning
	 * 
	 * @param mixed $data
	 * @param string $header
	 * @return void
	 */
	public function warning($data, $header) {
		if ($this->firePhp instanceof \FirePHP) {
			$this->firePhp->warn($data, $header . ':');
		}
	}
	
	/**
	 * error
	 * 
	 * @param mixed $data
	 * @param string $header
	 * @return void
	 */
	public function error($data, $header) {
		if ($this->firePhp instanceof \FirePHP) {
			$this->firePhp->error($data, $header . ':');
		}
	}

	/**
	 * beginGroup
	 * 
	 * @param string $title
	 * @return void
	 */
	public function beginGroup($title) {
		if ($this->firePhp instanceof \FirePHP) {
			$this->firePhp->group($title);
		}
	}

	/**
	 * endGroup
	 * 
	 * @return void
	 */
	public function endGroup() {
		if ($this->firePhp instanceof \FirePHP) {
			$this->firePhp->groupEnd();
		}
	}

}