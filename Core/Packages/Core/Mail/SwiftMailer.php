<?php
namespace Packages\Core\Mail;

use Packages\Core\Mail\AbstractMail;


/**
 * Description of SwiftMailer
 *
 * @author Cristian.Reus
 */
class SwiftMailer extends AbstractMail {
	
	/**
	 * transportTyp
	 * 
	 * @var string
	 */
	protected $transportTyp = 'smtp';

	/**
	 * mailer
	 * 
	 * @var Swift_Mailer
	 */
	protected $mailer = NULL;
	
	/**
	 * transport
	 * 
	 * @var mixed
	 */
	protected $transport = NULL;
	
	/**
	 * failedRecipients
	 * 
	 * @var array
	 */
	protected $failedRecipients = array();
	
	
	
	/**
	 * init
	 * 
	 * @return void
	 */
	public function init() {
		// set global charset
		\Swift_Preferences::getInstance()->setCharset($this->charset);
		
		// createTransport
		$this->createTransport();
		
		// Create the Mailer using your created Transport
		$this->mailer = \Swift_Mailer::newInstance($this->transport);
	}

	/**
	 * sendMessage
	 * 
	 * @param \Swift_Message $message
	 * @return integer
	 */
	public function sendMessage($message) {
		$failedRecipients = array();
		
		$result = $this->mailer->send(
			$message,
			$failedRecipients
		);
		
		$this->failedRecipients = $failedRecipients;
		
		return $result;
	}
	
	/**
	 * createMessageObject
	 * 
	 * @return \Swift_Message
	 */
	public function createMessageObject() {
		$message = \Swift_Message::newInstance();
		/* @var $message \Swift_Message */

		// set Message ID
		if (\strlen($this->serverName) > 0) {
			$message->setId(\time() . '.' . \uniqid() . '@' . $this->serverName);
		}

		// setFrom
		$message->setFrom(
			$this->fromEmail,
			$this->fromName
		);

		// setSender
		$message->setSender(
			$this->fromEmail,
			$this->fromName
		);

		return $message;
	}
	
	/**
	 * resetFailedRecipients
	 * 
	 * @return void
	 */
	public function resetFailedRecipients() {
		$this->failedRecipients = array();
	}
	
	
	/**
	 * createTransport
	 * 
	 * @return void
	 */
	protected function createTransport() {
		if (\is_null($this->transport)) {
			try {
				switch ($this->transportTyp) {
					case 'smtp':
						$this->transport = \Swift_SmtpTransport::newInstance(
								$this->smtpHost,
								$this->smtpPort,
								$this->encryption
							)
							->setUsername($this->smtpUsername)
							->setPassword($this->smtpPassword)
						;
						break;

					case 'sendmail':
						$this->transport = \Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -t');
						break;

					default:
						$this->transport = \Swift_MailTransport::newInstance();
						break;
				}

				if (!($this->transport instanceof \Swift_MailTransport) 
					&& \strlen($this->serverName) > 0
				) {
					$this->transport->setLocalDomain($this->serverName);
				}
			} catch (\Swift_TransportException $e) {
				throw new \RuntimeException(__METHOD__ . ' failured', 1447410634, $e);
			}
		}
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function getFailedRecipients() {
		return $this->failedRecipients;
	}

}
