<?php
namespace Packages\Core\Mail;

use Packages\Core\Mail\InterfaceMail;


/**
 * Description of AbstractMail
 *
 * @author Cristian.Reus
 */
abstract class AbstractMail implements InterfaceMail {
	
	/**
	 * smtpHost
	 * 
	 * @var string
	 */
	protected $smtpHost;
	
	/**
	 * smtpUsername
	 * 
	 * @var string
	 */
	protected $smtpUsername;
	
	/**
	 * smtpPassword
	 * 
	 * @var string
	 */
	protected $smtpPassword;
	
	/**
	 * smtpPort
	 * 
	 * @var integer
	 */
	protected $smtpPort = 25;
	
	/**
	 * encryption
	 * 
	 * @var string|null
	 */
	protected $encryption = NULL;
	
	/**
	 * serverName
	 * 
	 * @var string
	 */
	protected $serverName = '';
	
	/**
	 * fromEmail
	 * 
	 * @var string
	 */
	protected $fromEmail;
	
	/**
	 * fromName
	 * 
	 * @var string
	 */
	protected $fromName;
	
	/**
	 * contentType
	 * 
	 * @var string
	 */
	protected $contentType = 'text/plain';
	
	/**
	 * charset
	 * 
	 * @var string
	 */
	protected $charset = 'utf-8';
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function setSmtpHost($smtpHost) {
		$this->smtpHost = $smtpHost;
	}

	public function setSmtpUsername($smtpUsername) {
		$this->smtpUsername = $smtpUsername;
	}

	public function setSmtpPassword($smtpPassword) {
		$this->smtpPassword = $smtpPassword;
	}

	public function setSmtpPort($smtpPort) {
		$this->smtpPort = $smtpPort;
	}

	public function setEncryption($encryption) {
		$this->encryption = $encryption;
	}

	public function setServerName($serverName) {
		$this->serverName = $serverName;
	}

	public function setFromEmail($fromEmail) {
		$this->fromEmail = $fromEmail;
	}

	public function setFromName($fromName) {
		$this->fromName = $fromName;
	}

	public function setContentType($contentType) {
		$this->contentType = $contentType;
	}

	public function setCharset($charset) {
		$this->charset = $charset;
	}

}
