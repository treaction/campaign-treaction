<?php
namespace Packages\Core;

use Packages\Core\Utility\FilterUtility;
use Packages\Core\Utility\FileUtility;
use Packages\Core\Utility\GeneralUtility;


/**
 * Description of Dispatch
 *
 * @author Cristian.Reus
 */
class Dispatch {
	
	/**
	 * moduleRequiredData
	 * 
	 * @var array
	 */
	protected static $moduleRequiredData = array(
		'_module',
		'_controller',
		'_action'
	);
	
	
	
	/**
	 * checkAndInitModule
	 * 
	 * @param \Packages\Core\Domain\Model\ClientEntity $clientEntity
	 * @param \Packages\Core\Domain\Model\UserEntity $loggedUserEntity
	 * @param \Packages\Core\Domain\Repository\SystemRepository $systemRepository
	 * @param \Packages\Core\Domain\Repository\UserRepository $userRepository
	 * @param array $dataArray
	 * @return void
	 * @throws \InvalidArgumentException
	 */
	public static function checkAndInitModule(\Packages\Core\Domain\Model\ClientEntity $clientEntity, \Packages\Core\Domain\Model\UserEntity $loggedUserEntity, \Packages\Core\Domain\Repository\SystemRepository $systemRepository, \Packages\Core\Domain\Repository\UserRepository $userRepository, array $dataArray) {
		$foundRequiredFields = array();
		$requestDataArray = array();
		
		foreach ($dataArray as $key => $item) {
			if (\in_array($key, self::$moduleRequiredData)) {
				$value = FilterUtility::filterData($item);
				
				if (\strlen($value) > 0) {
					$foundRequiredFields[$key] = $value;
				}
			} else {
				if (\is_array($item)) {
					$requestDataArray[$key] = FilterUtility::filterArrayData($item);
				} else {
					$requestDataArray[$key] = FilterUtility::filterData($item);
				}
			}
		}
		
		if (\count($foundRequiredFields) !== \count(self::$moduleRequiredData)) {
			throw new \InvalidArgumentException('moduleRequiredData are not valid!', 1446816392);
		}
		
		self::createAndInitMvcModuleObject(
			$clientEntity,
			$loggedUserEntity,
			$systemRepository,
			$userRepository,
			$foundRequiredFields,
			$requestDataArray
		);
	}
	
	
	/**
	 * createAndInitModuleObject
	 * 
	 * @param \Packages\Core\Domain\Model\ClientEntity $clientEntity
	 * @param \Packages\Core\Domain\Model\UserEntity $loggedUserEntity
	 * @param \Packages\Core\Domain\Repository\SystemRepository $systemRepository
	 * @param \Packages\Core\Domain\Repository\UserRepository $userRepository
	 * @param array $foundRequiredFields
	 * @param array $requestDataArray
	 * @return void
	 * @throws \DomainException
	 */
	protected static function createAndInitMvcModuleObject(\Packages\Core\Domain\Model\ClientEntity $clientEntity, $loggedUserEntity, \Packages\Core\Domain\Repository\SystemRepository $systemRepository, \Packages\Core\Domain\Repository\UserRepository $userRepository, array $foundRequiredFields, array $requestDataArray) {
		try {
			\Autoloader::loadModules();
			
			$className = GeneralUtility::$namespaceDivider . 'Modules' 
				. GeneralUtility::$namespaceDivider . \ucfirst($foundRequiredFields['_module']) 
				. GeneralUtility::$namespaceDivider . 'Controller' 
				. GeneralUtility::$namespaceDivider . \ucfirst($foundRequiredFields['_controller']) . 'Controller'
			;
			
			require_once(\DIR_Inits . 'Connections' . \DIRECTORY_SEPARATOR . 'clientConnection.php');
			/* @var $clientConnectionHandle \Packages\Core\Manager\Database\AbstractConnection */
			
			require_once(\DIR_Vendor . 'swiftmailer-5.4.1/lib/swift_required.php');
			
			$mailEngine = new \Packages\Core\Mail\SwiftMailer();
			$mailEngine->setSmtpHost($clientSettingsDataArray['mailSettings']['smtpHost']);
			$mailEngine->setSmtpUsername($clientSettingsDataArray['mailSettings']['smtpUser']);
			$mailEngine->setSmtpPassword($clientSettingsDataArray['mailSettings']['smtpPassword']);
			$mailEngine->setSmtpPort($clientSettingsDataArray['mailSettings']['smtpPort']);
			$mailEngine->setEncryption($clientSettingsDataArray['mailSettings']['smtpEncryption']);
			$mailEngine->setServerName($clientSettingsDataArray['mailSettings']['serverName']);
			$mailEngine->setFromEmail($clientSettingsDataArray['mailSettings']['fromEmail']);
			$mailEngine->setFromName($clientSettingsDataArray['mailSettings']['fromName']);
			$mailEngine->init();
			
			
			/**
			 * load moduleSettings Configs
			 */
			$moduleSettingsFile = \DIR_Inits . 'Settings' . \DIRECTORY_SEPARATOR . \ucfirst($foundRequiredFields['_module']) . \DIRECTORY_SEPARATOR . 'settings.php';
			$moduleInitFile = \DIR_Inits . 'Settings' . \DIRECTORY_SEPARATOR . \ucfirst($foundRequiredFields['_module']) . \DIRECTORY_SEPARATOR . 'init.php';
			if (FileUtility::isReadable($moduleSettingsFile)) {
				require_once($moduleSettingsFile);
			}
			
			$controller = GeneralUtility::makeInstance(
				$className,
				$moduleSettingsDataArray[\strtolower($foundRequiredFields['_module'])],
				$foundRequiredFields
			);
			/* @var $controller \Packages\Core\Mvc\Controller\AbstractController */
			
			$controller->init(
				$repositoryClassDataArray[\strtolower($foundRequiredFields['_module'])],
				$clientConnectionHandle,
				$mailEngine,
				$systemRepository,
				$clientEntity,
				$loggedUserEntity
			);
			if (\is_readable($moduleInitFile)) {
				require_once($moduleInitFile);
			}
			$controller->{$foundRequiredFields['_action'] . 'Action'}($requestDataArray);
		} catch (\Exception $e) {
			throw new \DomainException('failed to call controller!', 1447055705, $e);
		}
	}
}