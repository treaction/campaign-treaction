<?php
namespace Packages\Core\Reflection;


/**
 * Description of ClassReflection
 *
 * @author Cristian.Reus
 */
class ClassReflection extends \ReflectionClass {

	/**
	 * docCommentParser
	 * 
	 * @var \Packages\Core\Reflection\DocCommentParser
	 */
	protected $docCommentParser;
	
	
	
	/**
	 * getMethods
	 *
	 * @param integer|NULL $filter
	 * @return array
	 */
	public function getMethods($filter = NULL) {
		$extendedMethods = array();
		$methods = $filter === NULL ? parent::getMethods() : parent::getMethods($filter);
		
		foreach ($methods as $method) {
			$extendedMethods[] = new \Packages\Core\Reflection\MethodReflection(
				$this->getName(),
				$method->getName()
			);
		}
		return $extendedMethods;
	}

	/**
	 * getMethod
	 *
	 * @param string $name
	 * @return \Packages\Core\Reflection\MethodReflection
	 */
	public function getMethod($name) {
		$parentMethod = parent::getMethod($name);
		
		if (!\is_object($parentMethod)) {
			return $parentMethod;
		}
		
		return new \Packages\Core\Reflection\MethodReflection(
			$this->getName(),
			$parentMethod->getName()
		);
	}

	/**
	 * getConstructor
	 *
	 * @return \Packages\Core\Reflection\MethodReflection
	 */
	public function getConstructor() {
		$parentConstructor = parent::getConstructor();
		
		if (!\is_object($parentConstructor)) {
			return $parentConstructor;
		}
		
		return new \Packages\Core\Reflection\MethodReflection(
			$this->getName(),
			$parentConstructor->getName()
		);
	}

	/**
	 * getProperties
	 *
	 * @param integer|NULL $filter
	 * @return array
	 */
	public function getProperties($filter = NULL) {
		$extendedProperties = array();
		$properties = $filter === NULL ? parent::getProperties() : parent::getProperties($filter);
		
		foreach ($properties as $property) {
			$extendedProperties[] = new \Packages\Core\Reflection\PropertyReflection(
				$this->getName(),
				$property->getName()
			);
		}
		
		return $extendedProperties;
	}

	/**
	 * getProperty
	 *
	 * @param string $name
	 * @return \Packages\Core\Reflection\PropertyReflection
	 */
	public function getProperty($name) {
		return new \Packages\Core\Reflection\PropertyReflection(
			$this->getName(),
			$name
		);
	}

	/**
	 * getInterfaces
	 *
	 * @return array
	 */
	public function getInterfaces() {
		$extendedInterfaces = array();
		$interfaces = parent::getInterfaces();
		
		foreach ($interfaces as $interface) {
			$extendedInterfaces[] = new \Packages\Core\Reflection\ClassReflection($interface->getName());
		}
		
		return $extendedInterfaces;
	}

	/**
	 * getParentClass
	 *
	 * @return \Packages\Core\Reflection\ClassReflection
	 */
	public function getParentClass() {
		$parentClass = parent::getParentClass();
		
		return $parentClass === FALSE ? FALSE : new \Packages\Core\Reflection\ClassReflection($parentClass->getName());
	}

	/**
	 * isTaggedWith
	 *
	 * @param string $tag
	 * @return boolean
	 */
	public function isTaggedWith($tag) {
		$result = $this->getDocCommentParser()->isTaggedWith($tag);
		
		return $result;
	}

	/**
	 * getTagsValues
	 *
	 * @return array
	 */
	public function getTagsValues() {
		return $this->getDocCommentParser()->getTagsValues();
	}

	/**
	 * getTagValues
	 *
	 * @param string $tag
	 * @return array
	 */
	public function getTagValues($tag) {
		return $this->getDocCommentParser()->getTagValues($tag);
	}

	/**
	 * getDocCommentParser
	 *
	 * @return \Packages\Core\Reflection\DocCommentParser
	 */
	protected function getDocCommentParser() {
		if (!\is_object($this->docCommentParser)) {
			$this->docCommentParser = new \Packages\Core\Reflection\DocCommentParser();
			$this->docCommentParser->parseDocComment($this->getDocComment());
		}
		
		return $this->docCommentParser;
	}
}
