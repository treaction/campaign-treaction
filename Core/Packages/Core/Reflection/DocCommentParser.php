<?php
namespace Packages\Core\Reflection;


/**
 * Description of DocCommentParser
 *
 * @author Cristian.Reus
 */
class DocCommentParser {

	/**
	 * description
	 * 
	 * @var string
	 */
	protected $description = '';

	/**
	 * tags
	 * 
	 * @var array
	 */
	protected $tags = array();
	
	
	
	/**
	 * parseDocComment
	 *
	 * @param string $docComment
	 * @return void
	 */
	public function parseDocComment($docComment) {
		$this->description = '';
		$this->tags = array();
		
		$lines = \explode(\chr(10), $docComment);
		foreach ($lines as $line) {
			if (\strlen($line) > 0 
				&& \strpos($line, '@') !== FALSE
			) {
				$this->parseTag(\substr($line, \strpos($line, '@')));
			} elseif (\count($this->tags) === 0) {
				$this->description .= \preg_replace('/\\s*\\/?[\\\\*]*(.*)$/', '$1', $line) . \chr(10);
			}
		}
		
		$this->description = \trim($this->description);
	}

	/**
	 * getTagsValues
	 *
	 * @return array
	 */
	public function getTagsValues() {
		return $this->tags;
	}

	/**
	 * getTagValues
	 *
	 * @param string $tagName
	 * @return array
	 * @throws \RuntimeException
	 */
	public function getTagValues($tagName) {
		if (!$this->isTaggedWith($tagName)) {
			throw new \RuntimeException('Tag "' . $tagName . '" does not exist.', 1446196933);
		}
		
		return $this->tags[$tagName];
	}

	/**
	 * isTaggedWith
	 *
	 * @param string $tagName
	 * @return boolean
	 */
	public function isTaggedWith($tagName) {
		return isset($this->tags[$tagName]);
	}

	/**
	 * getDescription
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * parseTag
	 *
	 * @param string $line
	 * @return void
	 */
	protected function parseTag($line) {
		$tagAndValue = \preg_split('/\\s/', $line, 2);
		$tag = \substr($tagAndValue[0], 1);
		
		if (\count($tagAndValue) > 1) {
			$this->tags[$tag][] = \trim($tagAndValue[1]);
		} else {
			$this->tags[$tag] = array();
		}
	}
}