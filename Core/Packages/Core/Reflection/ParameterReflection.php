<?php
namespace Packages\Core\Reflection;


/**
 * Description of ParameterReflection
 *
 * @author Cristian.Reus
 */
class ParameterReflection extends \ReflectionParameter {

	/**
	 * getDeclaringClass
	 *
	 * @return \Packages\Core\Reflection\ClassReflection
	 */
	public function getDeclaringClass() {
		return new \Packages\Core\Reflection\ClassReflection(parent::getDeclaringClass()->getName());
	}

	/**
	 * getClass
	 *
	 * @return \Packages\Core\Reflection\ClassReflection
	 */
	public function getClass() {
		try {
			$class = parent::getClass();
		} catch (\Exception $e) {
			return NULL;
		}
		
		return \is_object($class) ? new \Packages\Core\Reflection\ClassReflection($class->getName()) : NULL;
	}
}
