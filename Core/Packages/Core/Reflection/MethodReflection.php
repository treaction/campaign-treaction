<?php
namespace Packages\Core\Reflection;


/**
 * Description of MethodReflection
 *
 * @author Cristian.Reus
 */
class MethodReflection extends \ReflectionMethod {

	/**
	 * docCommentParser
	 * 
	 * @var \Packages\Core\Reflection\DocCommentParser
	 */
	protected $docCommentParser;
	
	
	
	/**
	 * getDeclaringClass
	 *
	 * @return \Packages\Core\Reflection\ClassReflection
	 */
	public function getDeclaringClass() {
		return new \Packages\Core\Reflection\ClassReflection(parent::getDeclaringClass()->getName());
	}

	/**
	 * getParameters
	 *
	 * @return array
	 */
	public function getParameters() {
		$extendedParameters = array();
		foreach (parent::getParameters() as $parameter) {
			$extendedParameters[] = new \Packages\Core\Reflection\ParameterReflection(
				array(
					$this->getDeclaringClass()->getName(),
					$this->getName()
				),
				$parameter->getName()
			);
		}
		
		return $extendedParameters;
	}

	/**
	 * isTaggedWith
	 *
	 * @param string $tag
	 * @return boolean
	 */
	public function isTaggedWith($tag) {
		$result = $this->getDocCommentParser()->isTaggedWith($tag);
		
		return $result;
	}

	/**
	 * getTagsValues
	 *
	 * @return array
	 */
	public function getTagsValues() {
		return $this->getDocCommentParser()->getTagsValues();
	}

	/**
	 * getTagValues
	 *
	 * @param string $tag
	 * @return array
	 */
	public function getTagValues($tag) {
		return $this->getDocCommentParser()->getTagValues($tag);
	}

	/**
	 * getDescription
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->getDocCommentParser()->getDescription();
	}

	/**
	 * getDocCommentParser
	 *
	 * @return \Packages\Core\Reflection\DocCommentParser
	 */
	protected function getDocCommentParser() {
		if (!\is_object($this->docCommentParser)) {
			$this->docCommentParser = new \Packages\Core\Reflection\DocCommentParser();
			$this->docCommentParser->parseDocComment($this->getDocComment());
		}
		return $this->docCommentParser;
	}
}
