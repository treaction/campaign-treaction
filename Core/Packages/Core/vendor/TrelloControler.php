<?php

namespace Packages\Core\vendor;

require_once 'autoload.php';

use Trello\Client;

/**
 * Description of TrelloControler
 *
 * @author Sami Jarmoud
 */
class TrelloControler {

    public static function create($kampagneName, $kampagneId, $mId) {

        $client = new Client();

       /* $client->authenticate('347358a6a57b6a84eb1c9d16559c165f', 'bfd6dbccb817b18dee8d38890c1565bb88a84d32585145b351e637d0012894c7', Client::AUTH_URL_CLIENT_ID);
        $params = array(
            'name' => 'ID:'.$kampagneId. '-'.$kampagneName,
            'idBoard' => '5ad0addbcb173674eb741557'
        );
        $boards = $client->api('list')->create($params);
        */
        if($mId == 14){
        $client->authenticate('347358a6a57b6a84eb1c9d16559c165f', 'bfd6dbccb817b18dee8d38890c1565bb88a84d32585145b351e637d0012894c7', Client::AUTH_URL_CLIENT_ID);
        
         $params = array(
            'name' => 'ID:'.$kampagneId. '-'.$kampagneName,
            'idList' => '5e9d9d350bba8d8106155494'
        );
        $boards = $client->api('card')->create($params);
        }else{
        $client->authenticate('3fd3bace2dfca517beb4b1858979e412', 'be83c665da041eca55f0320b4197ac5789736d898c76f224281af688568977d5', Client::AUTH_URL_CLIENT_ID);
        
        $params = array(
            'name' => 'ID:'.$kampagneId. '-'.$kampagneName,
            'idList' => '5aeadfcf1f362103f6f045f2'
        );
        $boards = $client->api('card')->create($params);
        }

        return $boards;
    }

}
