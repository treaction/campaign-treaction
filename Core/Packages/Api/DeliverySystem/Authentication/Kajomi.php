<?php
namespace Packages\Api\DeliverySystem\Authentication;

use Packages\Api\DeliverySystem\Authentication\AbstractAuthentication;

/**
 * Description of Kajomi
 *
 * @author Cristian.Reus
 */
class Kajomi extends AbstractAuthentication {
	
	/**
	 * authentification
	 * 
	 * @var \httpconnector|null
	 */
	protected $authentification = NULL;
	
	
	
	/**
	 * login
	 * 
	 * @return void
	 */
	public function login() {
		if (\is_null($this->authentification)) {
			try {
				require_once(\DIR_Vendor . 'DeliverySystems' . \DIRECTORY_SEPARATOR . 'Kajomi' . \DIRECTORY_SEPARATOR . 'connectors' . \DIRECTORY_SEPARATOR . 'httpconnector.php');

				// create new httpconnector instance
				$this->authentification = new \httpconnector(
					$this->sharedKey,
					$this->secretKey
				);

				// initialize connection
				$this->authentification->connect(
					'85.10.252.31/srv', // api.kajomimail.de/srv
					0
				);
			} catch (\Exception $e) {
				// TODO: log message
				throw $e;
			}
		}
	}

	/**
	 * logout
	 * 
	 * @return void
	 */
	public function logout() {
		// remove session
		$sessionFile = \sys_get_temp_dir() . \DIRECTORY_SEPARATOR . $this->sharedKey . '_ssid.tmp';
		\unlink($sessionFile);
		
		$this->authentification = NULL;
	}

}