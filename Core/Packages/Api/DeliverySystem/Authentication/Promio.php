<?php
namespace Packages\Api\DeliverySystem\Authentication;

use Packages\Api\DeliverySystem\Authentication\AbstractAuthentication;

/**
 * Description of Promio
 *
 * @author Sami Jarmoud
 */
class Promio extends AbstractAuthentication {
	
	/**
	 * client
	 * 
	 * @var \SoapClient|NULL
	 */
	protected $client = NULL;
	
	
	
	/**
	 * login
	 * 
	 * @return void
	 */
	public function login(){
            if (\is_null($this->authentification)) {
               
                try {
                     $authentification = new \stdClass();
                     $authentification->requestId               = "1359476991234";
                     $authentification->checksum                = md5( "1359476991234". $this->password );
                     $authentification->requestIdBlockedMinutes = 0;
                    $this->authentification = array(              
                        'clientId' => $this->clientId,
                        'authentification' => $authentification,
                        'pw' => $this->password,
                        'secretKey' => $this->secretKey
                    );

                } catch (Exception $ex) {
                    throw $ex;
                }
            }
        }

	/**
	 * logout
	 * 
	 * @return void
	 */
	public function logout() {
		$this->authentification = NULL;
	}

}