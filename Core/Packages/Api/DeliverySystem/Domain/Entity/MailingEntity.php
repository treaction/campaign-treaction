<?php
namespace Packages\Api\DeliverySystem\Domain\Entity;

use Packages\Core\DomainObject\AbstractDomainObject;


/**
 * Description of MailingEntity
 *
 * @author Cristian.Reus
 */
class MailingEntity extends AbstractDomainObject {
	
	/**
	 * title
	 * 
	 * @var string
	 */
	protected $title;
	
	/**
	 * subject
	 * 
	 * @var string
	 */
	protected $subject;
	
	/**
	 * htmlContent
	 * 
	 * @var string
	 */
	protected $htmlContent = NULL;
	
	/**
	 * txtContent
	 * 
	 * @var string
	 */
	protected $txtContent = NULL;
	
	/**
	 * fromName
	 * 
	 * @var string
	 */
	protected $fromName;
	
	/**
	 * fromEmail
	 * 
	 * @var string
	 */
	protected $fromEmail;
	
	/**
	 * mimeType
	 * 
	 * @var string
	 */
	protected $mimeType;
	
	/**
	 * charset
	 * 
	 * @var NULL|string
	 */
	protected $charset = NULL;
	
	/**
	 * createdDate
	 * 
	 * @var \DateTime|NULL
	 */
	protected $createdDate = NULL;
	
	/**
	 * maxRecipients
	 * 
	 * @var float|integer
	 */
	protected $maxRecipients;
	
	/**
	 * recipientFilterIds
	 * 
	 * @var array
	 */
	protected $recipientFilterIds = array();
	
	/**
	 * recipientListIds
	 * 
	 * @var array
	 */
	protected $recipientListIds = array();
	
	/**
	 * scheduleDate
	 * 
	 * @var \DateTime
	 */
	protected $scheduleDate = NULL;
	
	/**
	 * sendingFinishedDate
	 * 
	 * @var \DateTime
	 */
	protected $sendingFinishedDate = NULL;
	
	/**
	 * sendingStartedDate
	 * 
	 * @var \DateTime
	 */
	protected $sendingStartedDate = NULL;
	
	/**
	 * status
	 * 
	 * @var string
	 */
	protected $status;
	
	/**
	 * useScheduleDate
	 * 
	 * @var boolean
	 */
	protected $useScheduleDate = FALSE;
	
	/**
	 * sendername
	 * 
	 * @var string
	 */
	protected $sendername;
        
        /**
	 * replytomail
	 * 
	 * @var string
	 */
	protected $replytomail;
        
         /**
	 * domain
	 * 
	 * @var string
	 */
	protected $domain;
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function getTitle() {
		return $this->title;
	}
	public function setTitle($title) {
		$this->title = $title;
	}

	public function getSubject() {
		return $this->subject;
	}
	public function setSubject($subject) {
		$this->subject = $subject;
	}

	public function getHtmlContent() {
		return $this->htmlContent;
	}
	public function setHtmlContent($htmlContent) {
		$this->htmlContent = $htmlContent;
	}

	public function getTxtContent() {
		return $this->txtContent;
	}
	public function setTxtContent($txtContent) {
		$this->txtContent = $txtContent;
	}

	public function getFromName() {
		return $this->fromName;
	}
	public function setFromName($fromName) {
		$this->fromName = $fromName;
	}

	public function getFromEmail() {
		return $this->fromEmail;
	}
	public function setFromEmail($fromEmail) {
		$this->fromEmail = $fromEmail;
	}
	
	public function getMimeType() {
		return $this->mimeType;
	}
	public function setMimeType($mimeType) {
		$this->mimeType = $mimeType;
	}
	
	public function getCharset() {
		return $this->charset;
	}
	public function setCharset($charset) {
		$this->charset = $charset;
	}

	public function getCreatedDate() {
		if (!($this->createdDate instanceof \DateTime)) {
			$this->setCreatedDate($this->createdDate);
		}
		
		return $this->createdDate;
	}
	public function setCreatedDate($createdDate = NULL) {
		$this->createDateTimeFromValueForField(
			'createdDate',
			$createdDate
		);
	}

	public function getMaxRecipients() {
		return (int) $this->maxRecipients;
	}
	public function setMaxRecipients($maxRecipients) {
		$this->maxRecipients = \intval($maxRecipients);
	}
	
	public function getRecipientFilterIds() {
		return $this->recipientFilterIds;
	}
	public function setRecipientFilterIds($recipientFilterIds) {
		$this->recipientFilterIds = $recipientFilterIds;
	}
	
	public function getRecipientListIds() {
		return $this->recipientListIds;
	}
	public function setRecipientListIds($recipientListIds) {
		$this->recipientListIds = $recipientListIds;
	}

        public function getSendermail() {
		return $this->sendermail;
	}
	public function setSendermail($sendermail) {
		$this->sendermail = $sendermail;
	}
        
        public function getReplytomail() {
		return $this->replytomail;
	}
	public function setReplytomail($replytomail) {
		$this->replytomail = $replytomail;
	}
        function getDomain() {
            return $this->domain;
        }

        function setDomain($domain) {
            $this->domain = $domain;
        }

        	public function getScheduleDate() {
		if (!($this->scheduleDate instanceof \DateTime)) {
			$this->setScheduleDate($this->scheduleDate);
		}
		
		return $this->scheduleDate;
	}
	public function setScheduleDate($scheduleDate = NULL) {
		$this->createDateTimeFromValueForField(
			'scheduleDate',
			$scheduleDate
		);
	}
	
	public function getSendingFinishedDate() {
		if (!($this->sendingFinishedDate instanceof \DateTime)) {
			$this->setScheduleDate($this->sendingFinishedDate);
		}
		
		return $this->sendingFinishedDate;
	}
	public function setSendingFinishedDate($sendingFinishedDate = NULL) {
		$this->createDateTimeFromValueForField(
			'sendingFinishedDate',
			$sendingFinishedDate
		);
	}

	public function getSendingStartedDate() {
		if (!($this->sendingStartedDate instanceof \DateTime)) {
			$this->setSendingStartedDate($this->sendingStartedDate);
		}
		
		return $this->sendingStartedDate;
	}
	public function setSendingStartedDate($sendingStartedDate = NULL) {
		$this->createDateTimeFromValueForField(
			'sendingStartedDate',
			$sendingStartedDate
		);
	}

	public function getStatus() {
		return $this->status;
	}
	public function setStatus($status) {
		$this->status = $status;
	}
	
	public function getUseScheduleDate() {
		return (boolean) $this->useScheduleDate;
	}
	public function setUseScheduleDate($useScheduleDate) {
		if ((int) $useScheduleDate === 1) {
			$useScheduleDate = TRUE;
		} else {
			$useScheduleDate = FALSE;
		}
		
		$this->useScheduleDate = $useScheduleDate;
	}

}
