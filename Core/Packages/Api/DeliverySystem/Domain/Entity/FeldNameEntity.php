<?php
namespace Packages\Api\DeliverySystem\Domain\Entity;


/**
 * Description of FeldNameEntity
 * 
 * @author MohamedSamiJarmoud
 *
 */
class FeldNameEntity extends AbstractDomainObject{
	
	/**
	 * geschlecht
	 * 
	 * @var string
	 */
	protected $geschlechtFeldName = 'Anrde';
	/**
	 * verteiler
	 * 
	 * @var string
	 */
	protected $verteilerFeldName = 'verteiler';
	/**
	 * selektionsProfil
	 * 
	 * @var string
	 */
	protected $selektionsFeldName = 'selektion';
	/**
	 * alter
	 * 
	 * @var string
	 */
	protected $alterFeldName = 'alter';
	/**
	 * plz
	 * 
	 * @var string
	 */
	protected $plzFeldName = 'plz';
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	
	public function setGeschlechtFeldName($geschlechtFeldName){
		$this->geschlechtFeldName = $geschlechtFeldName;
 	}
    public function getGeschlechtFeldName(){
    	return $this->geschlechtFeldName;
    }
    public function setVerteilerFeldName($verteilerFeldName){
    	$this->verteilerFeldName = $verteilerFeldName;
    }
    public function getVerteilerFeldName(){
    	return $this->verteilerFeldName;
    }
    public function setSelektionsFeldName($selektionsFeldName){
    	$this->selektionsFeldName = $selektionsFeldName;
    }
    public function getSelektionsFeldName(){
    	return $this->selektionsFeldName;
    }
    public function setAlterFeldName($alterFeldName){
    	$this->alterFeldName = $alterFeldName;
    }
    public function getAlterFeldName(){
    	return $this->alterFeldName;
    }
    public function setPlzFeldName($PlzFeldName){
    	$this->plzFeldName = $plzFeldName;
    }
    public function getPlzFeldName(){
    	return $this->PlzFeldName;
    }
    
}