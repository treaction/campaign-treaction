<?php
namespace Packages\Api\DeliverySystem\Statistic;

use Packages\Api\DeliverySystem\Domain\Entity\StatisticEntity;
use Packages\Api\DeliverySystem\Domain\Entity\FeldNameEntity;


/**
 * 
 * @author MohamedSamiJarmoud
 *
 */
Interface InterfaceStatistic{
	
	/**
	 * processStatisticAuszaelung
	 * 
	 * @param StatisticEntity $statisticEntity
	 * @return array
	 */
	public function processStatisticAuszaelung(array $auszaelung , $distributor_id);
	public function processStatisticAnlegen(array $auszaelung , $distributor_id, $client_id);
}