<?php
namespace Packages\Api\DeliverySystem\Campaign;

use Packages\Api\DeliverySystem\Factory\Broadmail as BroadmailFactory;
use Packages\Api\DeliverySystem\Campaign\AbstractCampaign;
use Packages\Api\DeliverySystem\Domain\Entity\MailingListEntity;
use Packages\Api\DeliverySystem\Domain\Entity\MailingEntity;

use Packages\Core\Persistence\ObjectStorage;
use Packages\Core\Utility\EncodingUtility;
use Packages\Api\DeliverySystem\Utility\DeliverySystemUtiltiy;

/**
 * Description of Broadmail
 *
 * @author Cristian.Reus
 */
class Broadmail extends AbstractCampaign {
	
	/**
	 * mailingStatusDataArray
	 * 
	 * @var array
	 */
	public static $mailingStatusDataArray = array(
		'NEW' => 'NEW',
		'SENDING' => 'SENDING',
		'DONE' => 'DONE',
		'CANCELED' => 'CANCELED',
		'ALL' => 'ALL'
	);
	
	/**
	 * createMailingPropertiesMapping
	 * 
	 * @var array
	 */
	protected $createMailingPropertiesMapping = array(
		'subject' => 'Subject',
	);
	
	/**
	 * readMailingPropertiesMapping
	 * 
	 * @var array
	 */
	protected $readMailingPropertiesMapping = array(
		'title' => 'Name',
		'subject' => 'Subject',
		'fromName' => 'FromName',
		'fromEmail' => 'FromEmailPrefix',
		'mimeType' => 'MimeType',
		'charset' => 'Charset',
		'createdDate' => 'CreatedDate',
		'maxRecipients' => 'MaxRecipients',
		'recipientFilterIds' => 'RecipientFilterIds',
		'recipientListIds' => 'RecipientListIds',
		'scheduleDate' => 'ScheduleDate',
		'status' => 'Status'
	);
	
	/**
	 * extendedReadMailingPropertiesMapping
	 * 
	 * @var array
	 */
	protected $extendedReadMailingPropertiesMapping = array(
		'sendingFinishedDate' => 'SendingFinishedDate',
		'sendingStartedDate' => 'SendingStartedDate',
	);
	
	/**
	 * updateMailingPropertiesMapping
	 * 
	 * @var array
	 */
	protected $updateMailingPropertiesMapping = array(
		'charset' => 'Charset',
		'mimeType' => 'MimeType',
		'title' => 'Name',
		'subject' => 'Subject',
	);
	
	
	
	/**
	 * getList
	 * 
	 * @param string $status
	 * @param integer $limit
	 * @param integer $offset
	 * @return ObjectStorage
	 */
	public function getList($status, $limit = 20, $offset = 0) {
		$mailingWebservice = BroadmailFactory::getInstance('MailingWebservice');
		
		$mailingsDataArray = $this->getMailingsIds(
			$mailingWebservice,
			$status
		);
		
		$result = new ObjectStorage();
		if (\count($mailingsDataArray) > 0) {
			for ($offset; $offset < $limit; $offset++) {
				if (!isset($mailingsDataArray[$offset])) {
					break;
				}
				
				$mailingId = (int) $mailingsDataArray[$offset];
				
				$mailingListEntity = new MailingListEntity();
				$mailingListEntity->_setProperty('uid', $mailingId);
				$mailingListEntity->setStatus($status);
				$mailingListEntity->setTitle(
					$this->processGetFieldDataByMailingId(
						$mailingWebservice,
						$mailingId,
						$this->readMailingPropertiesMapping['title']
					)
				);

				if ($status !== self::$mailingStatusDataArray['NEW']) {
					$mailingListEntity->setDate(
						$this->processGetFieldDataByMailingId(
							$mailingWebservice,
							$mailingId,
							$this->extendedReadMailingPropertiesMapping['sendingStartedDate']
						)
					);
				}

				$result->attach($mailingListEntity);
			}
		}
		
		return $result;
	}
	
	/**
	 * create
	 * 
	 * @param MailingEntity $mailingEntity
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return integer
	 * @throws \Exception
	 */
	public function create(MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
		try {
			$mailingWebservice = BroadmailFactory::getInstance('MailingWebservice');
			
			$mailingId = $mailingWebservice->create(
				$this->authentication->getAuthentification(),
				'regular', 
				$mailingEntity->getTitle(),
				$mailingEntity->getMimeType(), 
				$mailingEntity->getRecipientListIds(),
				$mailingEntity->getFromEmail(),
				$mailingEntity->getFromName(),
				$mailingEntity->getCharset()
			);

			try {
				$this->processUpdateMailingWebserviceData(
					$this->createMailingPropertiesMapping,
					$mailingWebservice,
					(int) $mailingId,
					$mailingEntity
				);

				$mailingWebservice->setOpenTrackingEnabled(
					$this->authentication->getAuthentification(),
					$mailingId,
					TRUE
				);

				switch ($mailingEntity->getMimeType()){
					case 'text/html':
						$mailingWebservice->setClickTrackingEnabled(
						$this->authentication->getAuthentification(),
						$mailingId,
						'text/html',
						TRUE		
					 );
						break;
					case 'text/plain':
						$mailingWebservice->setClickTrackingEnabled(
						$this->authentication->getAuthentification(),
						$mailingId,
						'text/plain',
						TRUE		
					 );
						break;
					
					default :
						$mailingWebservice->setClickTrackingEnabled(
						$this->authentication->getAuthentification(),
						$mailingId,
						'text/html',
						TRUE	
					);
						$mailingWebservice->setClickTrackingEnabled(
						$this->authentication->getAuthentification(),
						$mailingId,
						'text/plain',
						TRUE		
					 );
				}
				
			} catch (\Exception $ex) {
				$this->delete($mailingId);
				throw $ex;		
			}
			
			// TODO: scheduleDate
			#$this->processScheduleMailing(
			#	$mailingWebservice,
			#	$mailingEntity
			#);
		} catch (\Exception $e) {
			throw $e;
		}
		
		return (int) $mailingId;
	}
	
	/**
	 * read
	 * 
	 * @param integer $mailingId
	 * @param boolean $getContent
	 * @return MailingEntity
	 * @throws \InvalidArgumentException
	 * @throws \Exception
	 */
	public function read($mailingId, $getContent = FALSE) {
		try {
			if ((int) $mailingId <= 0) {
				throw new \InvalidArgumentException('invalid mailingId', 1445508683);
			}
			
			$mailingWebservice = BroadmailFactory::getInstance('MailingWebservice');

			$mailingEntity = new MailingEntity();
			$mailingEntity->_setProperty('uid', (int) $mailingId);

			$this->processSetMailingEntityData(
				$this->readMailingPropertiesMapping,
				$mailingWebservice,
				(int) $mailingId,
				$mailingEntity
			);
			
			$mailingStatus = $this->processGetFieldDataByMailingId(
				$mailingWebservice,
				(int) $mailingId,
				$this->readMailingPropertiesMapping['status']
			);
			if ($mailingStatus !== self::$mailingStatusDataArray['NEW']) {
				$this->processSetMailingEntityData(
					$this->extendedReadMailingPropertiesMapping,
					$mailingWebservice,
					(int) $mailingId,
					$mailingEntity
				);
			}

			if ($getContent === TRUE) {
				$mimeType = $this->processGetFieldDataByMailingId(
					$mailingWebservice,
					(int) $mailingId,
					$this->readMailingPropertiesMapping['mimeType']
				);

				switch ($mimeType) {
					case 'text/plain':
						$mailingEntity->setTxtContent(
							$this->getMailingContentByIdAndMimeType(
								$mailingWebservice,
								(int) $mailingId,
								$mimeType
							)
						);
						break;

					case 'text/html':
						$mailingEntity->setHtmlContent(
							$this->getMailingContentByIdAndMimeType(
								$mailingWebservice,
								(int) $mailingId,
								$mimeType
							)
						);
						break;

					default:
						// multipart
						$mailingEntity->setTxtContent(
							$this->getMailingContentByIdAndMimeType(
								$mailingWebservice,
								(int) $mailingId,
								'text/plain'
							)
						);

						$mailingEntity->setHtmlContent(
							$this->getMailingContentByIdAndMimeType(
								$mailingWebservice,
								(int) $mailingId,
								'text/html'
							)
						);
						break;
				}
			}
		} catch (\Exception $e) {
			throw $e;
		}
		
		return $mailingEntity;
	}
	
	/**
	 * $result
	 * 
	 * @param MailingEntity $mailingEntity
	 * @param MailingEntity $newMailingEntity
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return boolean
	 * @throws \Exception
	 * @throws \InvalidArgumentException
	 */
	public function update(MailingEntity $mailingEntity, MailingEntity $newMailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
		try {
			if ($mailingEntity->_isNew()) {
				throw new \InvalidArgumentException('The domainEntity "(' . \get_class($mailingEntity) . ')" is new.', 1446713194);
			}
			
			$mailingWebservice = BroadmailFactory::getInstance('MailingWebservice');
			
			$this->processUpdateMailingWebserviceData(
				$this->updateMailingPropertiesMapping,
				$mailingWebservice,
				$mailingEntity->getUid(),
				$mailingEntity
			);
			
			if (
				(
					$mailingEntity->_isDirty('fromName') 
					|| $mailingEntity->_isDirty('fromEmail')
				) && (
					\strlen($mailingEntity->getFromName()) > 0 
					&& \strlen($mailingEntity->getFromEmail()) > 0
				)
			) {
				$mailingWebservice->__soapCall(
					'setFrom',
					array(
						$this->authentication->getAuthentification(),
						$mailingEntity->getUid(),
						$mailingEntity->getFromEmail(),
						$mailingEntity->getFromName()
					)
				);
			}		
			
			// processScheduleMailing
			$this->processScheduleMailing(
				$mailingWebservice,
				$mailingEntity
			);
			
			$result = TRUE;
		} catch (\Exception $e) {
			throw $e;
		}
		
		return $result;
	}

	/**
	 * delete
	 * 
	 * @param integer $mailingId
	 * @return boolean
	 * @throws \InvalidArgumentException
	 * @throws Exception
	 */
	public function delete($mailingId) {
		$result = FALSE;
		
		try {
			if ((int) $mailingId <= 0) {
				throw new \InvalidArgumentException('invalid mailingId', 1446713342);
			}
			
			$mailingWebservice = BroadmailFactory::getInstance('MailingWebservice');
			
			$mailingStatus = $this->processGetFieldDataByMailingId(
				$mailingWebservice,
				(int) $mailingId,
				$this->readMailingPropertiesMapping['status']
			);
			switch ($mailingStatus) {
				case 'NEW':
					$mailingWebservice->remove(
						$this->authentication->getAuthentification(),
						(int) $mailingId
					);

					$result = TRUE;
					break;
			}
		} catch (\Exception $e) {
			throw $e;
		}
		
		return $result;
	}
	
	/**
	 * autoNvMailings
	 * 
	 * @param integer $oldMailingId
	 * @return integer
	 * @throws \Packages\Api\DeliverySystem\Campaign\Exception
	 */
	public function autoNvMailings($oldMailingId, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
		try {
			$mailingWebservice = BroadmailFactory::getInstance('MailingWebservice');
			
			$newMailingId = $mailingWebservice->copy(
				$this->authentication->getAuthentification(),
				$oldMailingId
			);
			if (\is_numeric($newMailingId)) {
				/**
				 * clear targetGroups
				 */
				$oldRecipientFilterIds = $mailingWebservice->getRecipientFilterIds(
					$this->authentication->getAuthentification(),
					$newMailingId
				);
				$recipientFilterIds = $this->processClearMailingRecipientFilterWebservices($oldRecipientFilterIds);
				$mailingWebservice->setRecipientFilterIds(
					$this->authentication->getAuthentification(),
					$newMailingId,
					$recipientFilterIds
				);
				unset($oldRecipientFilterIds, $recipientFilterIds);
			}
		} catch (\Exception $e) {
			throw $e;
		}
		
		return (int) $newMailingId;
	}
	
	/**
	 * resetShippingDate
	 * 
	 * @param integer $mailingId
	 * @return integer
	 */
	public function resetShippingDate($mailingId) {
		$newMailingId = $this->copyMailing($mailingId);
		
		$this->delete($mailingId);
		
		return $newMailingId;
	}
	
	/**
	 * sendTestMail
	 * 
	 * @param long $mailingId
	 * @param long $recipientListId
	 * @param array $recipient
	 * @return integer
	 */
	public function sendTestMail($mailingId, $recipientListId, array $recipient){
			
		$resultTestMail['status'] = $this->setUserMailToRecipientList(
						                     $recipient,
						                     $recipientListId
						                    );
		try {
			$mailingWebservice = BroadmailFactory::getInstance('MailingWebservice');
			
			$resultTestMail['senden'] = $mailingWebservice->sendTestMail(
					$this->authentication->getAuthentification(),
					$mailingId,
					$recipientListId,
					$recipient['email']
					);
		 } catch (Exception $ex) {
			 throw $ex;
		 }
		return $resultTestMail;
	}
	
        /**
        * send
        * 
        * @param MailingEntity $MailingEntity
        * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
        * @return boolean
        */
        public function send(MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity, $sendermail, $replytomail) {
            return TRUE;
        }
         /*
         * getVerteilerZustand
         * @param integer $clientId
         * @return array
         */
        public function getVerteilerZustand($clientId){
            
        }
	
	/**
	 * getMailingsIds
	 * 
	 * @param \SoapClient $mailingWebservice
	 * @param string $status
	 * @return array
	 */
	protected function getMailingsIds(\SoapClient $mailingWebservice, $status) {
		if ($status === self::$mailingStatusDataArray['ALL']) {
			$mailingsDataArray = $mailingWebservice->__soapCall(
				'getIds',
				array(
					$this->authentication->getAuthentification(),
					'regular'
				)
			);
		} else {
			$mailingsDataArray = $mailingWebservice->__soapCall(
				'getIdsInStatus',
				array(
					$this->authentication->getAuthentification(),
					'regular',
					$status
				)
			);
		}
		
		return $mailingsDataArray;
	}
	
	/**
	 * processSetMailingEntityData
	 * 
	 * @param array $processDataArray
	 * @param \SoapClient $mailingWebservice
	 * @param integer $mailingId
	 * @param MailingEntity $mailingEntity
	 * @return void
	 */
	protected function processSetMailingEntityData(array $processDataArray, \SoapClient $mailingWebservice, $mailingId, MailingEntity &$mailingEntity) {
		foreach ($processDataArray as $key => $item) {
			$mailingEntity->_setProperty(
				$key,
				$this->processGetFieldDataByMailingId(
					$mailingWebservice,
					$mailingId,
					$item
				)
			);
		}
	}
	
	/**
	 * processUpdateMailingWebserviceData
	 * 
	 * @param array $processDataArray
	 * @param \SoapClient $mailingWebservice
	 * @param type $mailingId
	 * @param MailingEntity $mailingEntity
	 * @return void
	 */
	protected function processUpdateMailingWebserviceData(array $processDataArray, \SoapClient $mailingWebservice, $mailingId, MailingEntity $mailingEntity) {
		foreach ($processDataArray as $key => $item) {
			if (
				(
					\is_array($mailingEntity->_getProperty($key)) 
					&& \count($mailingEntity->_getProperty($key)) > 0
				) 
				|| (
					!\is_array($mailingEntity->_getProperty($key))
					&& \strlen($mailingEntity->_getProperty($key)) > 0
				)
			) {
				if ($mailingEntity->_isDirty($key)) {
					$this->processSetFieldDataByMailingId(
						$mailingWebservice,
						$mailingId,
						$item,
						$mailingEntity->_getProperty($key)
					);
				}
			}
		}
		
		if ($mailingEntity->_isDirty('maxRecipients') 
			&& $mailingEntity->getMaxRecipients() > 0
		) {
			$mailingWebservice->__soapCall(
				'setMaxRecipients',
				array(
					$this->authentication->getAuthentification(),
					(int) $mailingId,
					$mailingEntity->getMaxRecipients(),
					TRUE
				)
			);
		}
		
		if (\count($mailingEntity->getRecipientListIds()) > 0) {
			$updateRecipientListIds = FALSE;
			
			$cleanRecipientListIds = $mailingEntity->_getCleanProperty('recipientListIds');
			if (!\is_null($cleanRecipientListIds)) {
				$comparisonResult = $mailingEntity->getRecipientListIds() === $cleanRecipientListIds;
				if (!$comparisonResult) {
					$updateRecipientListIds = TRUE;
				}
			} else {
				$updateRecipientListIds = TRUE;
			}
			
			if ($updateRecipientListIds) {
				$this->processSetFieldDataByMailingId(
					$mailingWebservice,
					$mailingId,
					'recipientListIds',
					$mailingEntity->_getProperty('recipientListIds')
				);
			}
		}
		
		if ($mailingEntity->_isDirty('htmlContent') 
			&& \strlen($mailingEntity->getHtmlContent()) > 0
		) {
			$this->processSetOrUpdateContentAndTracking(
				$mailingWebservice,
				(int) $mailingId,
				'text/html',
				$mailingEntity->getHtmlContent()
			);
		}
		
		if ($mailingEntity->_isDirty('txtContent') 
			&& \strlen($mailingEntity->getTxtContent()) > 0
		) {
			$this->processSetOrUpdateContentAndTracking(
				$mailingWebservice,
				(int) $mailingId,
				'text/plain',
				$mailingEntity->getTxtContent()
			);
		}
	}
	
	/**
	 * processSetOrUpdateContentAndTracking
	 * 
	 * @param \SoapClient $mailingWebservice
	 * @param integer $mailingId
	 * @param string $mimeType
	 * @param string $content
	 * @return void
	 */
	protected function processSetOrUpdateContentAndTracking(\SoapClient $mailingWebservice, $mailingId, $mimeType, $content) {
		switch ($this->charset) {
			case 'UTF-8':
				$content = EncodingUtility::encodeToUtf8($content);
				break;
			
			default:
				$content = EncodingUtility::encodeToIso($content);
				break;
		}
		
		$mailingWebservice->__soapCall(
			'setContent',
			array(
				$this->authentication->getAuthentification(),
				(int) $mailingId,
				$mimeType,
				$content
			)
		);
		
		$mailingWebservice->__soapCall(
			'setClickTrackingEnabled',
			array(
				$this->authentication->getAuthentification(),
				(int) $mailingId,
				$mimeType,
				TRUE
			)
		);		
		
	}
	
	/**
	 * getMailingContentByIdAndMimeType
	 * 
	 * @param \SoapClient $mailingWebservice
	 * @param integer $mailingId
	 * @param string $mimeType
	 * @return string
	 */
	protected function getMailingContentByIdAndMimeType(\SoapClient $mailingWebservice, $mailingId, $mimeType) {
		return $mailingWebservice->__soapCall(
			'getContent',
			array(
				$this->authentication->getAuthentification(),
				(int) $mailingId,
				$mimeType
			)
		);
	}
	
	/**
	 * processGetFieldDataByMailingId
	 * 
	 * @param \SoapClient $mailingWebservice
	 * @param integer $mailingId
	 * @param string $fieldName
	 * @return mixed
	 */
	protected function processGetFieldDataByMailingId(\SoapClient $mailingWebservice, $mailingId, $fieldName) {
		return $mailingWebservice->__soapCall(
			'get' . \ucfirst($fieldName),
			array(
				$this->authentication->getAuthentification(),
				(int) $mailingId
			)
		);
	}
	
	/**
	 * processSetFieldDataByMailingId
	 * 
	 * @param \SoapClient $mailingWebservice
	 * @param integer $mailingId
	 * @param string $key
	 * @param mixed $value
	 */
	protected function processSetFieldDataByMailingId(\SoapClient $mailingWebservice, $mailingId, $key, $value) {
         if($key != 'Subject'){
            $mailingWebservice->__soapCall(
			'set' . \ucfirst($key),
			array(
				$this->authentication->getAuthentification(),
				(int) $mailingId,
				$value
			)
		);
            }
        if($key === 'Subject'){
            $mailingWebservice->__soapCall(
			'setSubject',
			array(
				$this->authentication->getAuthentification(),
				(int) $mailingId,
				htmlspecialchars_decode($value,ENT_QUOTES)
			)
		); 
            }
	}
	
	/**
	 * processClearMailingRecipientFilterWebservices
	 * 
	 * @param array $recipientFilterIds
	 * @return array
	 */
	protected function processClearMailingRecipientFilterWebservices(array $recipientFilterIds) {
		$resultDataArray = array();
		
		if (\count($recipientFilterIds) > 0) {
			$recipientFilterWebservice = BroadmailFactory::getInstance('RecipientFilterWebservice');
			
			foreach ($recipientFilterIds as $recipientFilterId) {
				if (\strpos($recipientFilterWebservice->getName($this->authentication->getAuthentification(), $recipientFilterId), 'Exclude Mailing') === FALSE) {
					$resultDataArray[] = $recipientFilterId;
				}
			}
			unset($recipientFilterWebservice);
		}
		
		return $resultDataArray;
	}
	
	/**
	 * processScheduleMailing
	 * 
	 * @param \SoapClient $mailingWebservice
	 * @param MailingEntity $mailingEntity
	 * @return void
	 */
	protected function processScheduleMailing(\SoapClient $mailingWebservice, MailingEntity $mailingEntity) {
		if ($mailingEntity->getScheduleDate() instanceof \DateTime 
			&& $mailingEntity->getUseScheduleDate()
		) {
			$mailingWebservice->__soapCall(
				'start',
				array(
					$this->authentication->getAuthentification(),
					$mailingEntity->getUid(),
					$mailingEntity->getScheduleDate()->format('c')
				)
			);
		}
	}
	
	/**
	 * copyMailing
	 * 
	 * @param integer $oldMailingId
	 * @return integer
	 * @throws \Packages\Api\DeliverySystem\Campaign\Exception
	 */
	protected function copyMailing($oldMailingId) {
		try {
			$mailingWebservice = BroadmailFactory::getInstance('MailingWebservice');
			
			$newMailingId = $mailingWebservice->copy(
				$this->authentication->getAuthentification(),
				$oldMailingId
			);
		} catch (\Exception $e) {
			throw $e;
		}
		
		return (int) $newMailingId;
	}
	
	/**
	 * 
	 * @param array $recipient
	 * @param integer $recipientListId
	 * @return string
	 * @throws \Packages\Api\DeliverySystem\Campaign\Exception
	 */
	protected function setUserMailToRecipientList(array $recipient, $recipientListId){
		try{
			$recipientWebservice = BroadmailFactory::getInstance('RecipientWebservice');
			// Status 8 => schon vorhanden
			$status = '8';
			$isExist = $recipientWebservice->contains(
			      $this->authentication->getAuthentification(),
				  $recipientListId,
					$recipient['email']
					);
			if(!$isExist){
			 $status = $recipientWebservice->add2(
						$this->authentication->getAuthentification(),
						$recipientListId,
						'0',
						$recipient['email'],
						$recipient['email'],
						array(
							'emailaddress',
					        'Anrede',
					        'Vorname',
					        'Nachname'
					      ),
						array(
							$recipient['email'],
							$recipient['anrede'],
							$recipient['vorname'],
							$recipient['nachname']
						  )
						);
			}
			
			return $status;
			
		} catch (Exception $e) {
			throw $e;
		}
		
	}

}
