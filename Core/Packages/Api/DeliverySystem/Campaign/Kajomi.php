<?php
namespace Packages\Api\DeliverySystem\Campaign;

use Packages\Api\DeliverySystem\Campaign\AbstractCampaign;
use Packages\Api\DeliverySystem\Domain\Entity\MailingListEntity;
use Packages\Api\DeliverySystem\Domain\Entity\MailingEntity;

use Packages\Core\Persistence\ObjectStorage;
use Packages\Core\Utility\EncodingUtility;

use Modules\Campaign\Utility\CampaignAndCustomerUtility;

/**
 * Description of Kajomi
 *
 * @author Cristian.Reus
 */
class Kajomi extends AbstractCampaign {
	
	/**
	 * mailingStatusDataArray
	 * 
	 * @var array
	 */
	public static $mailingStatusDataArray = array(
		'ALL' => 'ALL'
	);
	
	/**
	 * readMailingPropertiesMapping
	 * 
	 * @var array
	 */
	protected $readMailingPropertiesMapping = array(
		'title' => 'description',
		'subject' => 'subject',
		'fromName' => 'sendername',
		'fromEmail' => 'senderemail'
	);
	
	/**
	 * messageObjectPropertiesMapping
	 * 
	 * @var array
	 */
	protected $messageObjectPropertiesMapping = array(
		'fromName' => 'sendername',
		'fromEmail' => 'senderemail',
		'title' => 'description',
		'subject' => 'subject',
		'txtContent' => 'content',
		'htmlContent' => 'htcontent',
	);
	
	/**
	 * kajomiClient
	 * 
	 * @var \kajomiclient|NULL
	 */
	protected $kajomiClient = NULL;
	
	
	
	/**
	 * getList
	 * 
	 * @param string $status
	 * @param integer $limit
	 * @param integer $offset
	 * @return ObjectStorage
	 */
	public function getList($status, $limit = 20, $offset = 0) {
		$this->sortingList = 'desc';
		
		$mailingsDataArray = $this->getMailingsList(FALSE);
		
		$result = new ObjectStorage();
		if (\count($mailingsDataArray) > 0) {
			for ($offset; $offset < $limit; $offset++) {
				if (!isset($mailingsDataArray[$offset])) {
					break;
				}
				
				$mailingData = $mailingsDataArray[$offset];
				
				/**
				 * queuenum < 1 wird ignorieren,
				 * solche kampagnen wurden noch nicht versendet und haben somit noch keine eindeutige id (laut aussage von kajomi)
				 */
				if (\intval($mailingData->queuenum) <= 1) {
					continue;
				}
				
				// get mailing details
				$mailingEntity = $this->read((int) $mailingData->queuenum);
				
				$mailingListEntity = new MailingListEntity();
				$mailingListEntity->_setProperty('uid', (int) $mailingData->queuenum);
				$mailingListEntity->setStatus($status);
				$mailingListEntity->setTitle($mailingEntity->getTitle());
				
				$result->attach($mailingListEntity);
			}
		}
		
		return $result;
	}
	
	/**
	 * create
	 * 
	 * @param MailingEntity $mailingEntity
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return integer
	 * @throws Exception
	 */
	public function create(MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
		// in die description die ekm id mit speichern damit man später über einen cronjob die daten sync kann, mom ist id und queuenum 2 paar schuhe. (queuenum) wird erst nach versendung zugewissen
		try {
			$this->initApiClient();
			
			/**
			 * messageObject
			 */
			$messageObject = new \message();
			foreach ($this->messageObjectPropertiesMapping as $key => $item) {
				if ($key == 'title') {
					$messageObject->{$item} = $campaignEntity->getK_id() . ': ' . $mailingEntity->_getProperty($key);
				} elseif ($key == 'htmlContent' 
					|| $key == 'txtContent'
				) {
					$messageObject->{$item} = EncodingUtility::encodeToUtf8($mailingEntity->_getProperty($key));
				} else {
					$messageObject->{$item} = $mailingEntity->_getProperty($key);
				}
			}
			
			/**
			 * checkHtml
			 * 
			 * TODO: für später
			 */
			#$result = $this->kajomiClient->checkHTML($messageObject->htcontent);
			#\Packages\Core\Utility\DebugUtility::debug($result, 'checkHTML');

			/**
			 * distributorObject
			 */
			$listObject = $this->getListById((int) \current($mailingEntity->getRecipientListIds()));

			/**
			 * selectionObject
			 * 
			 * TODO: email an kajomi schicken
			 */
			$selection = new \selection();
			$selection->mlimit = $mailingEntity->getMaxRecipients();
			$selection->msex = CampaignAndCustomerUtility::findGenderSelection($campaignEntity->getZielgruppe());

			/**
			 * array of rules to apply to list
			 * 
			 * siehe email von: Do 19.11.2015 11:23
			 */
			$rulesDataArray = array();

			/**
			 * array of queues to exclude from receiving message
			 */
			$excludeDataArray = array();
			
			// send data to kajomi
			$allQueueDataArray = $this->kajomiClient->send(
				$messageObject,
				$listObject,
				$selection,
				$rulesDataArray,
				$excludeDataArray,
				$mailingEntity->getScheduleDate()->format('Y-m-d H:i')
			);
			\usort($allQueueDataArray, array($this, 'sortAscByQueuenum'));
			
			$newMailingQueueData = \end($allQueueDataArray);
			/* @var $newMailingQueueData \queue */
			
			// gilt nicht für autoNV kampagnen
			if ($mailingEntity->getUseScheduleDate() !== TRUE) {
				// kampagne erstmal auf inaktiv setzen
				$this->kajomiClient->setQueueStatus(
					$newMailingQueueData,
					FALSE
				);
			}

			$mailingId = (int) $newMailingQueueData->id;
		} catch (\Exception $e) {
			throw new \Exception(__METHOD__ . ': ' . $campaignEntity->getK_id(), 1454485851, $e);
		}
		
		return $mailingId;
	}
	
	/**
	 * read
	 * 
	 * @param integer $mailingId
	 * @param boolean $getContent
	 * @return MailingEntity
	 * @throws \InvalidArgumentException
	 * @throws Exception
	 */
	public function read($mailingId, $getContent = FALSE) {
		try {
			if ((int) $mailingId <= 0) {
				throw new \InvalidArgumentException('invalid mailingId', 1445508683);
			}
			
			$this->initApiClient();

			$mailingData = $this->kajomiClient->getQueue((int) $mailingId);

			$mailingEntity = new MailingEntity();
			$mailingEntity->_setProperty('uid', $mailingId);

			foreach ($this->readMailingPropertiesMapping as $key => $item) {
				$mailingEntity->_setProperty(
					$key,
					$mailingData->m->{$item}
				);
			}

			if ($getContent === TRUE) {
				$mailingEntity->setHtmlContent((string) $mailingData->m->htcontent);
				$mailingEntity->setTxtContent((string) $mailingData->m->content);
			}
		} catch (\Exception $e) {
			throw new \Exception(__METHOD__ . ': ' . $mailingId, 1454485882, $e);
		}
		
		return $mailingEntity;
	}
	
	/**
	 * update
	 * 
	 * @param MailingEntity $mailingEntity
	 * @param MailingEntity $newMailingEntity
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return boolean
	 * @throws \Exception
	 * @throws \InvalidArgumentException
	 */
	public function update(MailingEntity $mailingEntity, MailingEntity $newMailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
		// Workaround Auslesen der alten Kampagne GetQueues, Cancel benutzen und dann neu einstellen mit Send
		try {
			if ($mailingEntity->_isNew()) {
				throw new \InvalidArgumentException('The domainEntity "(' . \get_class($mailingEntity) . ')" is new.', 1446813134);
			}
			
			if ($mailingEntity->getUid() > 0) {
				// gilt nicht für autoNV kampagnen
				if ($campaignEntity->getK_id() !== $campaignEntity->getNv_id()) {
					if ($campaignEntity->getUse_campaign_start_date() !== (boolean) $campaignEntity->_getCleanProperty('use_campaign_start_date') 
						|| $campaignEntity->getDatum()->format('U') <> $dbDatum->format('U')
					) {
						$cancelQueueData = $this->getPendingMailingById($mailingEntity->getUid());
						
						// status auf inaktiv setzen
						$this->kajomiClient->setQueueStatus(
							$cancelQueueData,
							FALSE
						);
					}
				}
				
				// altes mailing löschen
				$this->delete($mailingEntity->getUid());
			}
			
			// neues mailing hinzufügen
			$newMailingId = $this->create(
				$newMailingEntity,
				$campaignEntity
			);
			$campaignEntity->setMail_id($newMailingId);
			
			$result = TRUE;
		} catch (\Exception $e) {
			throw new \Exception(__METHOD__ . ': ' . $mailingEntity->getUid(), 1454485793, $e);
		}
		
		return $result;
	}

	/**
	 * delete
	 * 
	 * @param integer $mailingId
	 * @return boolean
	 * @throws \InvalidArgumentException
	 * @throws Exception
	 */
	public function delete($mailingId) {
		$result = FALSE;
		
		try {
			if ((int) $mailingId <= 0) {
				throw new \InvalidArgumentException('invalid mailingId', 1446813112);
			}
			
			$cancelQueueData = $this->getPendingMailingById($mailingId);
			if (!\is_null($cancelQueueData)) {
				$this->kajomiClient->cancelQueue($cancelQueueData);
				$result = TRUE;
			}
		} catch (\Exception $e) {
			throw new \Exception(__METHOD__ . ': ' . $mailingId, 1454485609, $e);
		}
		
		return $result;
	}
	
	/**
	 * autoNvMailings
	 * 
	 * @param integer $oldMailingId
	 * @return integer
	 */
	public function autoNvMailings($oldMailingId, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
		return -1;
	}
	
	/**
	 * resetShippingDate
	 * 
	 * @param integer $mailingId
	 * @return integer
	 */
	public function resetShippingDate($mailingId) {
		// TODO: ->setQueueStatus(true/false)
//		try {
//			if ((int) $mailingId <= 0) {
//				throw new \InvalidArgumentException('invalid mailingId', 1454434042);
//			}
//			
//			$cancelQueueData = $this->getPendingMailingById($mailingId);
//			\Packages\Core\Utility\DebugUtility::debug($cancelQueueData, '$cancelQueueData');
//		} catch (\Exception $e) {
//			throw new \Exception(__METHOD__ . ': ' . $mailingId, 1454485906, $e);
//		}
//		
//		return $result;
		
		return $mailingId;
	}
	
	/**
	 * sendTestMail
	 * @param integer $mailingId
	 * @param long $recipientListId
	 * @param array $recipient
	 * @return integer
	 */
	public function sendTestMail($mailingId, $recipientListId, array $recipient){
		
		return $resultSendTestMail;
	}
	
       /**
        * send
        * 
        * @param MailingEntity $MailingEntity
        * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
        * @return boolean
        */
        public function send(MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
           return TRUE;
       }
        /*
         * getVerteilerZustand
         * @param integer $clientId
         * @return array
         */
        public function getVerteilerZustand($clientId);
	/**
	 * initApiClient
	 * 
	 * @return void
	 */
	protected function initApiClient() {
		if (\is_null($this->kajomiClient)) {
			require_once(\DIR_Vendor . 'DeliverySystems' . \DIRECTORY_SEPARATOR . 'Kajomi' . \DIRECTORY_SEPARATOR . 'api' . \DIRECTORY_SEPARATOR . 'kjmservice.php');
			
			// create client instance
			$this->kajomiClient = new \kajomiclient($this->authentication->getAuthentification());
		}
	}
	
	/**
	 * getMailingsList
	 * 
	 * @param boolean $pending
	 * @return array
	 */
	protected function getMailingsList($pending = FALSE) {
		$this->initApiClient();
		
		$result = $this->kajomiClient->getQueues($pending);
		if ($this->sortingList === 'asc') {
			\usort($result, array($this, 'sortAscByQueuenum'));
		} else {
			\usort($result, array($this, 'sortDescByQueuenum'));
		}
		
		return $result;
	}
	
	/**
	 * getListById
	 * 
	 * @param integer $listId
	 * @return lists
	 * @throws \DomainException
	 * 
	 * TODO: später in die vertreiler object auslagern
	 */
	protected function getListById($listId) {
		$this->initApiClient();
		
		$listObject = null;
		$listsDataArray = $this->kajomiClient->getLists();
		foreach ($listsDataArray as $item) {
			if ((int) $listId === (int) $item->listnum) {
				// create lists object from result data
				$listObject = new \lists();
				foreach ($item as $key => $value) {
					$listObject->{$key} = $value;
				}
				break;
			}
		}
		
		if (!($listObject instanceof \lists)) {
			throw new \DomainException('invalid list data', 1446724279);
		}
		
		return $listObject;
	}
	
	/**
	 * sortAscByQueuenum
	 * 
	 * @param \stdClass $a
	 * @param \stdClass $b
	 * @return int
	 */
	protected function sortAscByQueuenum(\stdClass $a, \stdClass $b) {
		$result = 0;
		
		if ($a->id < $b->id) {
			$result = -1;
		} elseif ($a->id > $b->id) {
			$result = 1;
		}
		
		return $result;
	}

	/**
	 * sortDescByQueuenum
	 * 
	 * @param \stdClass $a
	 * @param \stdClass $b
	 * @return type
	 */
	protected function sortDescByQueuenum(\stdClass $a, \stdClass $b) {
		$result = 0;

		if ($a->id < $b->id) {
			$result = 1;
		} elseif ($a->id > $b->id) {
			$result = -1;
		}

		return $result;
	}
	
	/**
	 * getPendingMailingById
	 * 
	 * @param integer $mailingId
	 * @return NULL|queue
	 */
	protected function getPendingMailingById($mailingId) {
		$this->initApiClient();
			
		$resultQueueData = NULL;
		
		// holle nur neue mailings, evtl. unnötig, mit kajomi klären
		$mailingsDataArray = $this->getMailingsList(TRUE);
		if (\count($mailingsDataArray) > 0) {
			foreach ($mailingsDataArray as $mailingObject) {
				if ($mailingId === (int) $mailingObject->id) {
					$resultQueueData = $mailingObject;
					break;
				}
			}
		}
		
		return $resultQueueData;
	}

}
