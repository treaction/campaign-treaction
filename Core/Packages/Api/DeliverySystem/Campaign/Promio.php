<?php

namespace Packages\Api\DeliverySystem\Campaign;

use Packages\Api\DeliverySystem\Campaign\AbstractCampaign;
use Packages\Api\DeliverySystem\Domain\Entity\MailingListEntity;
use Packages\Api\DeliverySystem\Domain\Entity\MailingEntity;
use Packages\Core\Persistence\ObjectStorage;
use Packages\Core\Utility\EncodingUtility;
use Packages\Api\DeliverySystem\Utility\DeliverySystemUtiltiy;

/**
 * Description of Promio
 * 
 * @author Sami.Jamroud
 */
Class Promio extends AbstractCampaign {


    /**
     * updateMailingPropertiesMapping
     * 
     * @var array
     */
    protected $updateMailingPropertiesMapping = array(
        'title' => 'name',
        'subject' => 'subject',
        'htmlContent' => 'html',
        'txtContent' => 'text',
    );

    /**
     * getList
     * 
     * @param string $status
     * @param integer $limit
     * @param integer $offset
     * @return \Packages\Core\Persistence\ObjectStorage
     */
    public function getList($status, $limit = 20, $offset = 0) {
        
    }

    /**
     * create
     * 
     * @param MailingEntity $mailingEntity
     * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
     * @return array $mailingIDs
     * @throws \Exception
     */
    public function create(MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
        try {
            $ch = curl_init();
            
            $this->setCurl( $ch,
                           'https://api.promio-connect.com/3.0/api/message/create',
                           $this->authentication->getAuthentification()['secretKey'],
                           (int)$this->authentication->getAuthentification()['clientId']
                    );
                   
               switch ($campaignEntity->getCharset()) {
                case 'ISO-8859-1':
                    $htmlContent = EncodingUtility::encodeToUtf8($mailingEntity->getHtmlContent());
                    break;

                case 'UTF-8':
                    $htmlContent = $mailingEntity->getHtmlContent();
                    break;
                default :
                    $htmlContent = $mailingEntity->getHtmlContent();
            } 
            if(strlen($mailingEntity->getTxtContent()) > 0){
                $text = $mailingEntity->getTxtContent();
            }else{
                $text ='-';
            } 
            
            $data['name'] = $campaignEntity->getK_id().'-'.$mailingEntity->getTitle();
            $data['clientId'] = (int)$this->authentication->getAuthentification()['clientId'];
            $data['campaignCode'] = DeliverySystemUtiltiy::getShortcutClickProfiles($campaignEntity->getCampaign_click_profiles_id());
            $data['sendFromId'] = $this->getSendFromId($mailingEntity->getDomain());
            $data['sendFromName'] = $mailingEntity->getFromName();
            $data['content']['html'] = $htmlContent;
            $data['content']['text'] = utf8_encode($text);
            $data['content']['subject'] =  $mailingEntity->getSubject();
            
         $jsonData = json_encode( $data);
         curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonData );
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         
         $result = curl_exec($ch);
        
           if($result){
               $mailingIDs = json_decode($result, true);
               $campaignEntity->setExternal_job_id($mailingIDs['messageId']);            
               //TargetGroup           
               $this->setTargetGroup((int)$mailingIDs['messageId'], (int)$mailingEntity->getRecipientListIds()[0]);
            }else{
                 throw new \Exception(curl_error($ch));
           }
        // close curl resource to free up system resources
        curl_close($ch);   
         
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $mailingIDs['campaignId'];
    }

    /**
     * read
     * 
     * @param integer $mailingId
     * @param boolean $getContent
     * @return \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity
     */
    public function read($mailingId, $getContent = false) {
        
    }

    /**
     * update
     * 
     * @param MailingEntity $mailingEntity
     * @param MailingEntity $newMailingEntity
     * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
     * @return boolean
     */
    public function update(MailingEntity $mailingEntity, MailingEntity $newMailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
     
            try {
            $ch = curl_init();
            $messageId = (int)$campaignEntity->getExternal_job_id();

            if(!empty($messageId)){
            $this->setCurl( $ch,
                           'https://api.promio-connect.com/3.0/api/message/'.$messageId.'/update',
                           $this->authentication->getAuthentification()['secretKey'],
                           (int)$this->authentication->getAuthentification()['clientId']
                    );          
               if ($mailingEntity->_isDirty('htmlContent') && \strlen($mailingEntity->getHtmlContent()) > 0
                  ) {

                      $htmlContent = $mailingEntity->getHtmlContent();
             }

                if ($mailingEntity->_isDirty('txtContent') && \strlen($mailingEntity->getTxtContent()) > 0
                  ) {
                       $text = $mailingEntity->getTxtContent();
             }else{
                $text ='-';
            } 
            if(!is_null($mailingEntity->getTitle())){
                 $data['name'] = $campaignEntity->getK_id().'-'.$mailingEntity->getTitle();
            }
           
            $data['clientId'] = (int)$this->authentication->getAuthentification()['clientId'];
            $data['campaignCode'] = DeliverySystemUtiltiy::getShortcutClickProfiles($campaignEntity->getCampaign_click_profiles_id());
            $data['sendFromId'] = $this->getSendFromId($newMailingEntity->getDomain());
            if(!is_null($mailingEntity->getFromName())){
                $data['sendFromName'] = $mailingEntity->getFromName();
            }
             if(!is_null($htmlContent)){
                $data['content']['html'] = $htmlContent;
            }
             if(!is_null($mailingEntity->getSubject())){
               $data['content']['subject'] =  $mailingEntity->getSubject(); 
            }           
            $data['content']['text'] = $text;
            
         
         $jsonData = json_encode( $data);
         curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonData );
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         
         $result = curl_exec($ch);
         
           if($result){
               $res = TRUE;  
            }else{
                 throw new \Exception(curl_error($ch));
           }
        // close curl resource to free up system resources
        curl_close($ch);   
            }
        } catch (\Exception $ex) {
            throw $ex;
        
        }
        return $res;
    }

    /**
     * delete
     * 
     * @param integer $mailingId mailingId
     * @return mixed|boolean
     */
    public function delete($mailingId) {
                $result = FALSE;
		try {
			if (\strlen($mailingId) <= 0) {
				throw new \InvalidArgumentException('invalid mailingId', 1446813112);
			}			
                        $campaignId = new \stdClass();
                        $campaignId->id       = $mailingId;
                        $campaignId->checksum = md5(  $campaignId->id. $this->authentication->getAuthentification()['pw'] );
                     $result = TRUE;    
		} catch (\Exception $e) {
			throw new \Exception(__METHOD__ . ': ' . $mailingId, 1454485609, $e);
		}
		
		return $result;
        
    }
    
    /**
     * send
     * 
     * @param MailingEntity $MailingEntity
     * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
     * @param boolean $updateAutoNv
     * @return boolean
     */
    public function send(MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity , $sendermail, $replytomail) {
      
      try {
             $messageId = (int)$campaignEntity->getExternal_job_id(); 
             $resultTarget = $this->updateTargetGroup($messageId, $mailingEntity);
                if($campaignEntity->getUse_campaign_start_date() === TRUE
                               ){
                //schedule
                 $chSchedule = curl_init();
                  $this->setCurl( $chSchedule,
                           'https://api.promio-connect.com/3.0/api/message/'.$messageId.'/schedule',
                           $this->authentication->getAuthentification()['secretKey'],
                           (int)$this->authentication->getAuthentification()['clientId']
                    );
                   $dataSchedule['scheduledForDeliveryAt'] = $campaignEntity->getDatum()->format('Y-m-d H:i');
                   $jsonDataSchedule = json_encode( $dataSchedule);
                   curl_setopt( $chSchedule, CURLOPT_POSTFIELDS, $jsonDataSchedule );
                   curl_setopt($chSchedule, CURLOPT_RETURNTRANSFER, 1);
         
                  $resultSchedule = curl_exec($chSchedule);
                  var_dump($resultSchedule);    
                //send  
                  $ch = curl_init();
                  $this->setCurl( $ch,
                           'https://api.promio-connect.com/3.0/api/message/'.$messageId.'/send',
                           $this->authentication->getAuthentification()['secretKey'],
                           (int)$this->authentication->getAuthentification()['clientId']
                    );
                   $data['scheduledForDeliveryAt'] = $campaignEntity->getDatum()->format('Y-m-d H:i');
                   $jsonData = json_encode( $data);
                   curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonData );
                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         
                  $result = curl_exec($ch);
                  var_dump($result);
                }

            if($result){
               $res = TRUE;  
            }else{
                 throw new \Exception(curl_error($ch));
           }
        // close curl resource to free up system resources
        curl_close($ch);
        } catch (\Exception $e) {
            throw $e;
        }

        return $res;
    }    
        

    /**
     * autoNvMailings
     * 
     * @param integer $oldMailingId mailingId
     * @return integer
     */
    public function autoNvMailings($oldMailingId, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
     
        }

    /**
     * resetShippingDate
     * 
     * @param integer $mailingId
     * @return integer
     */
    public function resetShippingDate($mailingId) {
        
    }

     /**
     * sendTestMail
     * @param integer $mailingId
     * @param long $recipientListId
     * @param array $recipient
     * @return integer
     */
    public function sendTestMail($mailingId, $recipientListId, array $recipient) {
        

       /*  
         try{
              $ch = curl_init();
        
              #$messageId = (int)$mailingId;
              $messageId = 1078298;
              $this->setCurl( $ch,                
                           'https://api.promio-connect.com/3.0/api/message/'.$messageId.'/send-test',
                             $this->authentication->getAuthentification()['secretKey'],
                           (int)$this->authentication->getAuthentification()['clientId']
                    );
            
          $data['recipients']['mail'] = $recipient['email'];
          $data['recipients']['firstName'] = 'Sami';
          $data['recipients']['lastName'] = 'Jarmoud';
          $data['recipients']['gender'] = 1;
          
         $jsonData = json_encode( $data);
         curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonData );
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         
         $result = curl_exec($ch);
         var_dump($result);
                
         } catch (\Exception $ex) {
                throw $ex;
         }
         */
        
    }
    
        /**
     * getVerteilerZustand
     * @param type $clientId
     * @return array
     * @throws \Exception
     */
    public function getVerteilerZustand($clientId){
       
    }
    
    public function updateTargetGroup($messageId, MailingEntity $mailingEntity){
 
          if (\count($mailingEntity->getRecipientFilterIds()) > 0) {
          $res = $this->setTargetGroup($messageId,  $mailingEntity->getRecipientFilterIds());
        }
        return $res;
    }
    public function setTargetGroup($messageId, $targetGroupId) {
        try {
            $ch = curl_init();
            $this->setCurl( $ch,
                           'https://api.promio-connect.com/3.0/api/message/'.$messageId.'/assign-target-group',
                           $this->authentication->getAuthentification()['secretKey'],
                           (int)$this->authentication->getAuthentification()['clientId']
                    );
         
         $data['targetGroupId'] = $targetGroupId;   
         $jsonData = json_encode( $data);
         curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonData );
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         $result = curl_exec($ch);
           return $result;  
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }      
    }

    protected function deliveryReceiver($targetGroupId,$sendFromId) {
        
        $deliveryReceiver = new \stdClass();
        #$deliveryReceiver->newsletterId = 82026;
        $deliveryReceiver->targetGroupAndSendFromId = new \stdClass();
        $deliveryReceiver->targetGroupAndSendFromId->targetGroupId  = $targetGroupId;
        $deliveryReceiver->targetGroupAndSendFromId->sendFromId = $sendFromId;
        if(is_object($deliveryReceiver)){
            return $deliveryReceiver;
        }else{
            return FALSE;
        }
    }
    
    protected function deliverySendTime($sendTime) {
        
      $deliverySendTime = new \stdClass();
      $deliverySendTime->sendDateTime = $sendTime;
        if(is_object($deliverySendTime)){
            return $deliverySendTime;
        }else{
            return FALSE;
        }
    }
    
    protected function getSendFromId($domain) {
         switch ($domain) {
                        case 'blueleads-aktuell.de':
                            $sendFromId = 14982;
                            break;
                        case 'blueleads-deals.de':
                            $sendFromId = 14983;
                            break;
                        case 'blueleadsonline.de':
                            $sendFromId = 14984;
                            break;
                        case 'bl-deals.online':
                            $sendFromId = 14981;
                            break;
                        
                        case 'dealsfuerdich.de':
                            $sendFromId = 14980;
                            break;
                        case 'dealsfuerdich.online':
                            $sendFromId = 14985;
                            break;
                        case 'deals-fuer-dich.de':
                            $sendFromId = 14986;
                            break;
                        case 'deals-fuer-dich.online':
                            $sendFromId = 14987;
                            break;
                        case 'info.deals-shopping.de':
                            $sendFromId = 17208;
                            break;
                         
                        default:
                            $sendFromId = 14757;
                            break;
                    }
                    return $sendFromId;
    }
    
    private function setCurl($ch,$endpoint, $apiKey, $sendId){
         
          
         
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
           'API-Key:'. $apiKey,
           'Sender-ID:'. $sendId,
            'Content-Type: application/json'
           ));
          
          curl_setopt($ch, CURLOPT_URL, $endpoint);
     }
    
}
