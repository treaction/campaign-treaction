<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<HTML>
<HEAD>
<TITLE>POP3- Zugriff - Test</TITLE>
<style>
PRE { font-family:courier; font-size:11pt; color:black; background-color:white }
</style>
</HEAD>
<BODY bgcolor="#eeeeee">
<h1>Abfrage eines POP3-Servers</h1>
<div style="font-size:12pt; font-family:Arial, Helvetica, Univers; font-weight:bold; color:black">Anmeldedaten</div>
<p>
<div class =text>
Geben Sie hier die folgenden Daten zur Abfrage eines beliebigen POP3-Servers im Internet an.
Diese Daten werden nicht gespeichert!
<p>
<form method="post" action="pop3test.php">
<table border=0>
	<tr>
		<td valign="top">Anmeldename:</td><td><input type="Text" name="_user" value="testmail_intone"></td>
	</tr>
	<tr>
		<td valign="top">Kennwort:</td><td><input type="Text" name="_pass" value="intone123"></td>
	</tr>
	<tr>
		<td valign="top">Server:</td><td><input type="Text" name="_server" value="pop.mail.yahoo.de"></td>
	</tr>
	<tr>
		<td valign="top">Aktion</td><td><input type="Checkbox" name="_delete" value="On"> Nachrichten l&ouml;schen?</td>
	</tr>
	<tr>
		<td>&nbsp;</td><td><input type="Submit" name="submit" value="Server abfragen"></td>
	</tr>
</table>
</form>

<p>
<div style="font-size:12pt;  font-family:Arial, Helvetica, Univers; font-weight:bold; color:black">Ergebnis der Abfrage</div>
<?php
require("pop3class.php");
$user=trim($_POST['_user']);		// Aus dem Formular
$password=trim($_POST['_pass']);	// Aus dem Formular
$apop=0;
$pop3_connection=new pop3_class;
$pop3_connection->hostname=trim($_POST['_server']); // Aus dem Formular

if(($error=$pop3_connection->Open())=="")  {
   echo "<PRE>Verbunden mit POP3-Server ".$pop3_connection->hostname."</PRE>\n";
   if(($error = $pop3_connection->Login($user, $password, $apop))=="") {
       echo "<PRE>Nutzer ".$user." angemeldet.</PRE>\n";
       if(($error=$pop3_connection->Statistics(&$messages, &$size))=="") {
            echo "<PRE>Es sind $messages Nachrichten in der Mailbox mit insgesamt $size Bytes.</PRE>\n";
            $result=$pop3_connection->ListMessages("",0);
            if(GetType($result)=="array") {
			   echo "<ul>";
               for(Reset($result), $message=0; $message<count($result); Next($result), $message++) {
			   	   echo "<li>Nachricht ", Key($result)," - ",$result[Key($result)]," Bytes.</li>\n";
			   }
			   echo "</ul>";
			   $result=$pop3_connection->ListMessages("",1);
               if(GetType($result)=="array") {
			      echo "<ul>";
                  for(Reset($result),$message=0; $message<count($result); Next($result),$message++) {
				      echo "<li>Nachricht ",Key($result),", Unique ID - \"",$result[Key($result)], "\"</li>\n";
				  }
				  echo "</ul>";
				  if($messages>0) {
				  	 for($current = 0; $current<=10; $current++) {
					    if(($error=$pop3_connection->RetrieveMessage($current, &$headers, &$body, 2000))=="")  {
					        echo "<PRE>Nachricht $current: ---Nachrichten-Header startet hier---</PRE>\n";
							for($line=0;$line<count($headers);$line++) echo HtmlSpecialChars($headers[$line]), "<br>\n";
					        echo "<PRE>---Nachrichten-Header endet hier---</PRE><PRE>---Nachrichten-Body startet hier--</PRE>\n";
							for($line=0;$line<count($body);$line++) echo HtmlSpecialChars($body[$line]), "<br>\n";
 						    echo "<PRE>---Nachrichten Body endet hier---</PRE>\n";
							if ($_delete == "On") {
								if(($error = $pop3_connection->DeleteMessage($current)) == "") { 
								     echo "<PRE>Nachricht $current wurde zum L&ouml;schen markiert.</PRE>\n";
								     if(($error=$pop3_connection->ResetDeletedMessages())=="") {
									     echo "<PRE>Gel&ouml;schte Nachrichten wurden entfernt.</PRE>\n";
									 } /* end if */
								} /* end if */
							} /* end if */
					 	} /* end if */
					  } /* end for */
				  } /* end if */
				  if($error=="" && ($error=$pop3_connection->Close())=="")
				      echo "<PRE>Verbindung zu POP3-Server &quot;$pop3_connection->hostname&quot; beendet.</PRE>\n";
			  } else $error=$result;
		   } else $error=$result;
  		} /* end if */
 	} /* end if */
} /* end if */
if($error!="") echo "<p><div style=\"color:red; font-weight:bold; font-size:12pt\">Fehler: $error</div>";
?>
</div>
</body>
</html>
