<?php
class BlacklistPo {
    private $id;
    private $email;
    private $datum;
    private $grund;
    private $firma;
    private $bearbeiter;
    private $notizen;


    function initFromPdo($row) {
        $this->id = $row['id'];
        $this->email = $row['email'];
        $this->datum = $row['datum'];
        $this->grund = $row['grund'];
        $this->firma = $row['firma'];
        $this->bearbeiter = $row['bearbeiter'];
        $this->notizen = $row['notizen'];
    }




    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

    public function getEmail() {
        return $this->email;
    }
    public function setEmail($email) {
        $this->email = $email;
    }

    public function getDatum() {
        return $this->datum;
    }
    public function setDatum($datum) {
        $this->datum = $datum;
    }

    public function getGrund() {
        return $this->grund;
    }
    public function setGrund($grund) {
        $this->grund = $grund;
    }

    public function getFirma() {
        return $this->firma;
    }
    public function setFirma($firma) {
        $this->firma = $firma;
    }

    public function getBearbeiter() {
        return $this->bearbeiter;
    }
    public function setBearbeiter($bearbeiter) {
        $this->bearbeiter = $bearbeiter;
    }

    public function getNotizen() {
        return $this->notizen;
    }
    public function setNotizen($notizen) {
        $this->notizen = $notizen;
    }
}
?>