<?php
final class ExcelUtil {
    const SEPERATOR = ';';
    const ENCLOSER = '"';

    private $stringBuffer;


    public function __clone() {}
    public function __construct(){
        $this->stringBuffer = '';
    }

    /**
     * @param $rowArr
     */
    public function addCsvHeader($rowArr) {
        $line = '';
        $headerRow = array();

        foreach($rowArr as $value) {
            $headerRow[] = self::ENCLOSER . $value . self::ENCLOSER;
        }
        $line = implode(self::SEPERATOR, $headerRow) . "\n";

        $this->stringBuffer .= trim($line) . "\n";
    }



    /**
     * @param array $csvHeaderArr
     * @param array $csvItemsArr
     * @param array $specialOptionsArr
     */
    public function addCsvLineItem($csvHeaderArr, $csvItemsArr, $specialOptionsArr) {
        $line = '';
        $csvDaten = array();

        foreach($csvItemsArr as $item) {
            // id
            if(array_key_exists('id', $csvHeaderArr)) {
                $csvDaten['id'] = self::ENCLOSER . $item->getId() . self::ENCLOSER;
            }

            // email feld
            if(array_key_exists('email', $csvHeaderArr)) {
                if(!empty($specialOptionsArr) && array_search('email', $specialOptionsArr)) {
                    // spezielle zeichen aus email entfernen (bei zmail sind es z.b. *@)
                    $tmp = str_replace($specialOptionsArr['searchOption'], $specialOptionsArr['replaceOption'], $item->getEmail());
                    $csvDaten['email'] = self::ENCLOSER . $tmp . self::ENCLOSER;
                } else {
                    $csvDaten['email'] = self::ENCLOSER . $item->getEmail() . self::ENCLOSER;
                }
            }

            // datum
            if(array_key_exists('datum', $csvHeaderArr)) {
                $csvDaten['datum'] = self::ENCLOSER . $item->getDatum() . self::ENCLOSER;
            }

            // grund
            if(array_key_exists('grund', $csvHeaderArr)) {
                $csvDaten['grund'] = self::ENCLOSER . $item->getGrund() . self::ENCLOSER;
            }

            // firma
            if(array_key_exists('firma', $csvHeaderArr)) {
                $csvDaten['firma'] = self::ENCLOSER . $item->getFirma() . self::ENCLOSER;
            }

            // bearbeiter
            if(array_key_exists('bearbeiter', $csvHeaderArr)) {
                $csvDaten['bearbeiter'] = self::ENCLOSER . $item->getBearbeiter() . self::ENCLOSER;
            }

            // notizen
            if(array_key_exists('notizen', $csvHeaderArr)) {
                $csvDaten['notizen'] = self::ENCLOSER . $item->getNotizen() . self::ENCLOSER;
            }

            $line .= implode(self::SEPERATOR, $csvDaten) . "\n";
        }

        $this->stringBuffer .= trim($line) . "\n";
    }


    public function toString(){
        $this->stringBuffer = str_replace("\r", '', $this->stringBuffer);

        if($this->stringBuffer == '') {
            $this->stringBuffer = "\n(0) Records Found!\n";
        }

        return utf8_decode($this->stringBuffer);
    }


    public function initDownload($fileName = 'blacklist.csv') {
        HttpUtil::setDownloadHeader($fileName, HttpUtil::MIME_TYPE_CSV, null);
        print $this->toString();
        die;
    }
}
?>