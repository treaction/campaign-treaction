<?php
class CheckEmailUtil {
    public static function CheckEmail($email) {
        $status = false;
        $email = trim($email);

        // email = name@domain.tdl
        if(preg_match('/^[a-zA-Z0-9]+([-_\.]?[a-zA-Z0-9\-\_])+@[a-z0-9]+([-_\.]?[a-z0-9])+\.[a-z]{2,4}$/', $email)) {
            $status = true;
        }

        // email = *@domain.tdl
        if(preg_match('/^[\*\]+@[a-z0-9]+([-_\.]?[a-z0-9\-\_])+\.[a-z]{2,4}$/', $email)) {
            $status = true;
        }

        return $status;
    }
}
?>