function show_div(id) {
	document.getElementById(id).style.visibility = 'visible';
	document.getElementById(id).style.display = 'block';
}
function hidden_div(id) {
	document.getElementById(id).style.visibility = 'hidden';
	document.getElementById(id).style.display = 'none';
}

function trim(str) {
	if(typeof(str)== 'string') {
		return str.replace(/^\s+|\s+$/g, '');
	 } else {
		 return str;
	 }
}

// multiblacklist ein bzw. ausblenden
function auswahlBlacklist(id1, id2) {
	if(document.getElementById(id2).style.display == 'none') {
		hidden_div(id1);
		show_div(id2);
	} else {
		show_div(id1);
		hidden_div(id2);
	}
}


function formCheckBlacklist(select, limit) {
	var form = document.forms['blacklist'];
	var val = form.elements['blacklist[multi_email]'].value.replace(/\r/g, '').split('\n');
	var error = '';
	
	
	// anzahl der zeilen überprüfen, beim multi email
	if(val.length > limit) {
		error += '<li>Sie haben mehr als ' + limit + ' Email Adressen eingefügt.</li>';
	}
	
	// Mandanten auswahl
	if(trim(form.elements['blacklist[mandant]']).value == '---') {
		error += '<li>Bitte wählen Sie einen Mandaten aus!</li>';
	}
	
	// Grund auswahl
	if(trim(form.elements['blacklist[grund]']).value == 'manuell') {
		show_div('grund_optional');
	} else {
		hidden_div('grund_optional');
	}
	if(trim(form.elements['blacklist[grund]']).value == '---') {
		error += '<li>Bitte wählen Sie einen Grund aus!</li>';
	}
	else if (trim(form.elements['blacklist[grund]']).value == 'manuell'  && form.elements['blacklist[grund_optional]'].value == '') {
		error += '<li>Bitte geben Sie einen Grund ein!</li>';
	}
	
	// bearbeiter checken
	if(trim(form.elements['mitarbeiter']).value == '') {
		error += '<li>Sie haben keine rechte dazu!</li>';
	}
	
	if(error != '' ) {
		// submit ausblenden
		hidden_div('submit_blacklist');
		document.getElementById('submit_blacklist').disabled = true;
		
		errormsg = '<p><b>Bitte korrigieren Sie diese Felder aus:</b></p>'; 
		errormsg += '<ul>';
		errormsg += error;
		errormsg += '</ul>';
		
		show_div('error_blacklist');
		nachricht = document.getElementById('error_blacklist');
		nachricht.style.display = 'block';
		nachricht.innerHTML = errormsg;
	} else {
		// submit einblenden
		show_div('submit_blacklist');
		document.getElementById('submit_blacklist').disabled = false;
		
		// fehler meldung ausblenden
		hidden_div('error_blacklist');
	}

}