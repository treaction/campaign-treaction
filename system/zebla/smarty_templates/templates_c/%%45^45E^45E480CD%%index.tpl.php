<?php /* Smarty version 2.6.26, created on 2010-03-10 15:09:43
         compiled from index.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'core/header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="start">
	<h1>ZEBLA - Zentrales Blacklist und Abmelde-Tool</h1>
	<div id="content">
		<div id="content_left">
			<h2>Herzlich Willkommen <?php echo $this->_tpl_vars['mandant']; ?>
!</h2>
			<!-- such formular -->
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'core/search_form.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<!-- such formular - ende -->
		</div>
		
		<!-- mandanten logo -->
		<img src="imgs/<?php echo $this->_tpl_vars['mandant']; ?>
.gif" id="mandanten_logo" border="0" alt="<?php echo $this->_tpl_vars['mandant']; ?>
" />
		
		<div id="trennlinie">&nbsp;</div>
		
		<div id="inhalt">
						<?php echo $this->_tpl_vars['action_content']; ?>

		</div>
		
		<div id="letzten_eintraegen">
			<h3>Die letzten 10 Einträge:</h3>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'core/items_liste.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</div>
	</div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'core/footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>