<?php /* Smarty version 2.6.26, created on 2010-03-10 15:09:43
         compiled from start.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'lower', 'start.tpl', 19, false),)), $this); ?>
<ul class="ui-tabs-nav">
	<li><a href="#blacklist_start">Blacklist Eintrag hinzufügen:</a></li>
</ul>

<div id="blacklist_start">
	<div id="blacklist_item">
		<form id="blacklist" name="blacklist" method="post" action="" onsubmit="return formCheckBlacklist('', 50);">
			<fieldset class="edit">
				<label for="email" style="text-align:right;"><span>Email:</span> <img src="imgs/info.gif" alt="email info" title="Um eine gesamte Domain auf die Blacklist zu setzen, geben Sie diese bitte folgendermaße ein: - *@domain.de" /></label>
				<input type="text" name="blacklist[email]" id="email" style="float:left;" />
				<textarea name="blacklist[multi_email]" id="multi_email" cols="100" rows="20" style="display: none; float: left;" onkeyup="formCheckBlacklist(this, '50')" onkeydown="formCheckBlacklist(this, '50')" onchange="formCheckBlacklist(this, '50')" onfocus="formCheckBlacklist(this, '50')" ></textarea>
				<a href="#" onclick="auswahlBlacklist('email', 'multi_email'); return false;" style="font-size: 11px; color: #3A83B6; font-weight: bold; margin: 0 0 0 5px;" > + Mehrere Emails</a><br class="clear-float" />

				<label for="mandant">Mandant:</label>
				<?php if ($this->_tpl_vars['isAdmin']): ?>
					<select name="blacklist[mandant]" id="mandant" onchange="formCheckBlacklist(this);" onkeydown="formCheckBlacklist(this);"  onkeyup="formCheckBlacklist(this);" >
						<option value="---" >Bitte auswählen</option>
						<?php $_from = $this->_tpl_vars['mandantenArrListe']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['mandantenArrListe'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['mandantenArrListe']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['mandantenItem']):
        $this->_foreach['mandantenArrListe']['iteration']++;
?>
							<option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['mandantenItem'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)); ?>
" <?php if ($this->_tpl_vars['mandant'] == ((is_array($_tmp=$this->_tpl_vars['mandantenItem'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp))): ?>selected="selected" <?php endif; ?> ><?php echo ((is_array($_tmp=$this->_tpl_vars['mandantenItem'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)); ?>
</option>
						<?php endforeach; endif; unset($_from); ?>
					</select>
				<?php else: ?>
					<span><?php echo $this->_tpl_vars['mandant']; ?>
</span> <input type="hidden" name="blacklist[mandant]"  value="<?php echo $this->_tpl_vars['mandant']; ?>
" />
				<?php endif; ?>
				<br class="clear-float" />
                
				<label for="grund">Grund:</label>
				<select name="blacklist[grund]" id="grund" onchange="formCheckBlacklist(this);" onkeydown="formCheckBlacklist(this);"  onkeyup="formCheckBlacklist(this);" >
					<option value="---">Bitte auswählen</option>
					<?php $_from = $this->_tpl_vars['grundArrListe']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['grundArrListe'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['grundArrListe']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['grundItem']):
        $this->_foreach['grundArrListe']['iteration']++;
?>
						<option value="<?php echo $this->_tpl_vars['grundItem']->getGrund(); ?>
" ><?php echo $this->_tpl_vars['grundItem']->getGrund(); ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
					<option value="Telefonische Beschwerde">Telefonische Beschwerde</option>
					<option value="manuell">-&gt; Sonstiges</option>
    			</select>
				<div style="display:none; visibility: hidden; margin: 10px 0 0 75px;" id="grund_optional">
					<b>Sonstiges:</b><br />
					<input type="text" name="blacklist[grund_optional]" value="" onchange="formCheckBlacklist(this);" />
				</div><br class="clear-float" />
    		        
				<label for="notizen">Notizen:</label><textarea name="blacklist[notizen]" id="notizen" cols="40" rows="3"></textarea>
			</fieldset>

			<fieldset class="edit">
				<label>&nbsp;</label>
				<input type="hidden" name="mitarbeiter"  value="<?php echo $this->_tpl_vars['mitarbeiter']; ?>
" />
				<input type="hidden" name="action"  value="submit_blacklist" />

				<input type="submit" name="submit_blacklist" id="submit_blacklist" style="display:none;" value="JETZT EINTRAGEN!" disabled="disabled" />
			</fieldset>
		</form>
		<div id="error_blacklist">  </div>
	</div>
	
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'core/download_box.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>