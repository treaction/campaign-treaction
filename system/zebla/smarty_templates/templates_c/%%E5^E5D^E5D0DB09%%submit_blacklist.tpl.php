<?php /* Smarty version 2.6.26, created on 2010-03-10 15:11:54
         compiled from submit_blacklist.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'submit_blacklist.tpl', 8, false),)), $this); ?>
<ul class="ui-tabs-nav">
	<li><a href="#blacklist_start">Blacklist Eintrag - Statistik:</a></li>
</ul>

<div id="blacklist_start">
	<div id="blacklist_item">
				<?php if (count($this->_tpl_vars['emailOkArr']) > 0): ?>
			<h3>Blacklisteintrag erfolgreich durchgeführt.</h3>
			<p style="font-weight: bold;">
				<?php if ($this->_tpl_vars['gesamtCount'] > 1): ?>
					<?php echo count($this->_tpl_vars['emailOkArr']); ?>
 von <?php echo $this->_tpl_vars['gesamtCount']; ?>
 Emails wurden erfolgreich in die Blacklist eingetragen:
				<?php else: ?>
					Folgende Email wurde erfolgreich in die Blacklist eingetragen:
				<?php endif; ?>
			</p>
			<dl>
				<dt>Email(s):</dt>
				<dd>
					<?php $_from = $this->_tpl_vars['emailOkArr']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['emailOkItem']):
?>
						<?php echo $this->_tpl_vars['emailOkItem']; ?>
<br />
					<?php endforeach; endif; unset($_from); ?>
				</dd><br class="clear-float" />
				<dt>Mandant:</dt><dd><?php echo $this->_tpl_vars['blacklistMandant']; ?>
</dd><br class="clear-float" />
				<dt>Grund:</dt><dd><?php echo $this->_tpl_vars['blacklistGrund']; ?>
</dd><br class="clear-float" />
				<dt>Zusätzliche Notizen:</dt><dd><?php echo $this->_tpl_vars['blacklistNotizen']; ?>
</dd><br class="clear-float" />
			</dl><br class="clear-float" />
			<div style="height:25px;"> </div>
		<?php endif; ?>
		
				<?php if (count($this->_tpl_vars['emailBlacklistArr']) > 0): ?>
			<p style="font-weight: bold;">Folgenden Email(s) bzw. Domain waren bereits auf die Blacklist:</p>
			<dl>
				<dt>Email(s):</dt>
				<dd>
					<?php $_from = $this->_tpl_vars['emailBlacklistArr']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['emailBlacklistItem']):
?>
						<?php echo $this->_tpl_vars['emailBlacklistItem']; ?>
<br />
					<?php endforeach; endif; unset($_from); ?>
				</dd>
			</dl><br class="clear-float" />
			<div style="height:25px;"> </div>
		<?php endif; ?>
		
				<?php if (count($this->_tpl_vars['emailFalschArr']) > 0): ?>
			<p style="font-weight: bold;">Folgenden Emails sind falsch:</p>
			<dl>
				<dt>Email(s):</dt>
				<dd>
					<?php $_from = $this->_tpl_vars['emailFalschArr']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['emailFalschItem']):
?>
						<?php echo $this->_tpl_vars['emailFalschItem']; ?>
<br />
					<?php endforeach; endif; unset($_from); ?>
				</dd>
			</dl><br class="clear-float" />
		<?php endif; ?>
		
				<div style="margin: 15px 0 0 0;">
			<?php if (count($this->_tpl_vars['emailOkArr']) > 0): ?>
				<a href="index.php?mitarbeiter=<?php echo $this->_tpl_vars['mitarbeiter']; ?>
">weiterer Blacklisteintrag...</a>
			<?php else: ?>
				<a href="index.php?mitarbeiter=<?php echo $this->_tpl_vars['mitarbeiter']; ?>
">Zurück zur Startseite.</a>
			<?php endif; ?>
		</div>
	</div>
	
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'core/download_box.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>