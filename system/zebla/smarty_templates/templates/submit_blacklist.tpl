<ul class="ui-tabs-nav">
	<li><a href="#blacklist_start">Blacklist Eintrag - Statistik:</a></li>
</ul>

<div id="blacklist_start">
	<div id="blacklist_item">
		{* zeige erfolgreich eingetragene daten an *}
		{if $emailOkArr|@count > 0}
			<h3>Blacklisteintrag erfolgreich durchgeführt.</h3>
			<p style="font-weight: bold;">
				{if $gesamtCount > 1}
					{$emailOkArr|@count} von {$gesamtCount} Emails wurden erfolgreich in die Blacklist eingetragen:
				{else}
					Folgende Email wurde erfolgreich in die Blacklist eingetragen:
				{/if}
			</p>
			<dl>
				<dt>Email(s):</dt>
				<dd>
					{foreach from=$emailOkArr item=emailOkItem}
						{$emailOkItem}<br />
					{/foreach}
				</dd><br class="clear-float" />
				<dt>Mandant:</dt><dd>{$blacklistMandant}</dd><br class="clear-float" />
				<dt>Grund:</dt><dd>{$blacklistGrund}</dd><br class="clear-float" />
				<dt>Zusätzliche Notizen:</dt><dd>{$blacklistNotizen}</dd><br class="clear-float" />
			</dl><br class="clear-float" />
			<div style="height:25px;"> </div>
		{/if}
		
		{* zeige vorhandene einträge *}
		{if $emailBlacklistArr|@count > 0}
			<p style="font-weight: bold;">Folgenden Email(s) bzw. Domain waren bereits auf die Blacklist:</p>
			<dl>
				<dt>Email(s):</dt>
				<dd>
					{foreach from=$emailBlacklistArr item=emailBlacklistItem}
						{$emailBlacklistItem}<br />
					{/foreach}
				</dd>
			</dl><br class="clear-float" />
			<div style="height:25px;"> </div>
		{/if}
		
		{* zeige falsche einträge *}
		{if $emailFalschArr|@count > 0}
			<p style="font-weight: bold;">Folgenden Emails sind falsch:</p>
			<dl>
				<dt>Email(s):</dt>
				<dd>
					{foreach from=$emailFalschArr item=emailFalschItem}
						{$emailFalschItem}<br />
					{/foreach}
				</dd>
			</dl><br class="clear-float" />
		{/if}
		
		{* 
			infotext je nach email status anzeigen
			wenn emails eingetragen wurden zeige einen anderen text als wenn keine emails in die db eingetragen wurden
		*}
		<div style="margin: 15px 0 0 0;">
			{if $emailOkArr|@count > 0}
				<a href="index.php?mitarbeiter={$mitarbeiter}">weiterer Blacklisteintrag...</a>
			{else}
				<a href="index.php?mitarbeiter={$mitarbeiter}">Zurück zur Startseite.</a>
			{/if}
		</div>
	</div>
	
	{* download box *}
	{include file='core/download_box.tpl'}
</div>