<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>ZEBLA - Zentrales Blacklist und Abmelde-Tool</title>
	
	<!-- cachen deaktivieren -->
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="PRAGMA" content="no-cache" />
	
	<link href="css/screen.css" rel="stylesheet" type="text/css" />
	<link href="css/jquery/tooltip.css" rel="stylesheet" type="text/css" />
	<link href="css/jquery/ui/tabs.css" rel="stylesheet" type="text/css" />
	
	<!-- jquery plugins -->
	<script type="text/javascript" src="js/jquery/jquery.js"></script>
	<script type="text/javascript" src="js/jquery/jquery_dimensions.js"></script>
	<script type="text/javascript" src="js/jquery/jquery_tooltip.js"></script>
	<!-- jquery ui -->
	<script type="text/javascript" src="js/jquery/ui/ui_core.js"></script>
	<script type="text/javascript" src="js/jquery/ui/ui_tabs.js"></script>
	
	<script type="text/javascript" src="js/zebla.js"></script>
	<script type="text/javascript" src="js/jquery_zebla.js"></script>
</head>

<body>