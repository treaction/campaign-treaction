<div id="suche">
	<h3>Suche innerhalb der Blacklist:</h3>
	<form name="blacklist_suche" method="post" action="">
		<input type="hidden" name="action"  value="suchergebnisse" />
		<fieldset>
			<label for="email">E-Mail:</label>
				<input type="text" id="search-email" name="search[email]" value="{$searchEmail}"  />
				<input type="submit" name="submit_suche"  value="suche starten" />
			<div>
				<input type="radio" name="search[option]" id="einfache_suche" value="einfache_suche" {if $searchOption == 'einfache_suche'} checked="checked" {/if} /> <label for="einfache_suche">Exakte suche</label>
				<input type="radio" name="search[option]" id="erweiterte_suche" value="erweiterte_suche" {if $searchOption == 'erweiterte_suche'} checked="checked" {/if} /> <label for="erweiterte_suche">Ähnliche anzeigen</label>
			</div>
		</fieldset>
	</form>
</div>