<div id="blacklist_start">
	{if $blacklistItemsArr|@count > 0}
		<h3>{$blacklistItemsArr|@count} Blacklisteinträge gefunden</h3>
		{include file='core/items_liste.tpl'}
		<a href="index.php?mitarbeiter={$mitarbeiter}" id="backurl">Zurück zur Startseite</a>
	{else}
		<h3 class="error">Es wurden keine passende Einträgen gefunden.</h3>
		<a href="index.php?mitarbeiter={$mitarbeiter}" id="backurl">Zurück zur Startseite</a>
	{/if}
</div>