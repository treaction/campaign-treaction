<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', TRUE);
ini_set('memory_limit', '256M');

session_start();

function __autoload($class) {
	$path = 'class/';
	
	if(preg_match('/Manager$/', $class)) {
		$path .= 'Manager/';
	} else if (preg_match('/Po$/', $class)) {
		$path .= 'Po/';
	} else if (preg_match('/Util$/', $class)) {
		$path .= 'Util/';
	} else {
		throw new RuntimeException('unknown class: ' . $class);
	}
	
	require_once ($path . $class . '.php');
}

require_once('class/Database.php');
require_once('class/SrvgsDatabase.php');
require_once('class/SmartyFactory.php');


// allgemeine einstellungen
#####################################################
// Mandanten Level (gilt nur f�r intone, pepperos)
$adminUsers = array(
	'intone',
    'magolino',
    'mailrevolution',
    'silveraddress'
);

// mandant festlegen anhand der eingeloggten user (username = mandant)
$mandant = $_SESSION['mandant'];

$specialOptionsArr = array();
if($mandant == 'zmail') {
	$specialOptionsArr['searchField'] = 'email';
	$specialOptionsArr['searchOption'] = '*@';
	$specialOptionsArr['replaceOption'] = '';
}

$isAdmin = false;
if(in_array($mandant, $adminUsers)) {
	$isAdmin = true;
}

// mitarbeiter festlegen (wenn kein mitarbeiter mit�bergeben wurde nimm den mandant als mitarbeiter)
$mitarbeiter = isset($_REQUEST['mitarbeiter']) ? $_REQUEST['mitarbeiter'] : $mandant;

$searchEmail = isset($_POST['search']['email']) ? trim($_POST['search']['email']) : null;
$searchOption = isset($_POST['search']['option']) ? $_POST['search']['option'] : 'einfache_suche';

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
###################################################

$action_content = '';
switch($action) {
	case 'suchergebnisse':
		if(empty($searchEmail)) {
			$zeblaSmartyAction = new SmartyFactory();
			$zeblaSmartyAction->setTemplateName('core/no_email');
			$zeblaSmartyAction->assign('mitarbeiter', $mitarbeiter);
		} else {
			$resultSearchItemArr = BlacklistManager::searchBlacklistEmail($searchEmail, $searchOption);
			
			$zeblaSmartyAction = new SmartyFactory();
			$zeblaSmartyAction->setTemplateName('suchergebnisse');
			$zeblaSmartyAction->assign('blacklistItemsArr', $resultSearchItemArr);
			$zeblaSmartyAction->assign('mitarbeiter', $mitarbeiter);
		}
		
		$action_content = $zeblaSmartyAction->display();
		break;
	
	case 'submit_blacklist':
		// wenn keinen passenden grund in der liste gefunden wurde dann nehme den optionalen grund und speichere es in die db
		if($_POST['blacklist']['grund'] == 'manuell') {
			$blacklistGrund = $_POST['blacklist']['grund_optional'];
		} else {
			$blacklistGrund = $_POST['blacklist']['grund'];
		}
		
		$blacklistNotizen = $_POST['blacklist']['notizen'];
		$blacklistMandant = $_POST['blacklist']['mandant'];
		
		// eine oder mehrere emails in die blacklist table einf�gen
		if($_POST['blacklist']['multi_email'] != '') {
			// mehrere emails zu einzelne aufsplittern
			$emailTmpArr = explode("\n", $_POST['blacklist']['multi_email']);
			
			// leere zeichen entfernen
			$resultArr = array();
			foreach($emailTmpArr as $emailItem) {
				$emailItem = trim($emailItem);
				$tmpArr = BlacklistManager::insertBlacklistItems($emailItem, $blacklistGrund, $blacklistMandant, $mitarbeiter, $blacklistNotizen);
				
				// eintrag in die srvgsBlacklist Table
				SrvgsBlacklistManager::insertBlacklistItems($emailItem, $blacklistGrund, $blacklistMandant, $mitarbeiter, $blacklistNotizen);
				
				if(isset($tmpArr['ok'])) {
					$resultArr['ok'][] = $tmpArr['ok'];
				}
				
				if(isset($tmpArr['blacklist'])) {
					$resultArr['blacklist'][] = $tmpArr['blacklist'];
				}
				
				if(isset($tmpArr['falsch'])) {
					$resultArr['falsch'][] = $tmpArr['falsch'];
				}
			}
			
			$gesamtCount = count($emailTmpArr);
		} else {
			$emailItem = trim($_POST['blacklist']['email']);
			
			$gesamtCount = 1;
			$resultArr = BlacklistManager::insertBlacklistItems($emailItem, $blacklistGrund, $blacklistMandant, $mitarbeiter, $blacklistNotizen);
			
			// eintrag in die srvgsBlacklist Table
			SrvgsBlacklistManager::insertBlacklistItems($emailItem, $blacklistGrund, $blacklistMandant, $mitarbeiter, $blacklistNotizen);
		}
		
		$emailOkArr = isset($resultArr['ok']) ? $resultArr['ok'] : null;
		$emailBlacklistArr = isset($resultArr['blacklist']) ? $resultArr['blacklist'] : null;
		$emailFalschArr = isset($resultArr['falsch']) ? $resultArr['falsch'] : null;
		
		// download zahlen
		$countBlacklistItems = BlacklistManager::countBlacklistItem();
		
		
		$zeblaSmartyAction = new SmartyFactory();
		$zeblaSmartyAction->setTemplateName('submit_blacklist');
		$zeblaSmartyAction->assign('mitarbeiter', $mitarbeiter);
		$zeblaSmartyAction->assign('gesamtCount', $gesamtCount);
		$zeblaSmartyAction->assign('blacklistGrund', $blacklistGrund);
		$zeblaSmartyAction->assign('blacklistNotizen', $blacklistNotizen);
		$zeblaSmartyAction->assign('blacklistMandant', $blacklistMandant);
		$zeblaSmartyAction->assign('emailOkArr', $emailOkArr);
		$zeblaSmartyAction->assign('emailBlacklistArr', $emailBlacklistArr);
		$zeblaSmartyAction->assign('emailFalschArr', $emailFalschArr);
		
		// download counts
		$zeblaSmartyAction->assign('countBlacklistItems', $countBlacklistItems);
		
		$action_content = $zeblaSmartyAction->display();
		break;
		
	case 'download':
		// es besteht die m�glichkeit mehrere Felder zu exportieren
		$csvHeaderArr = array(
            'email' => 'Email'
		);
		
		$csvItemArr = BlacklistManager::selectBlacklistItems('`email` ASC');
		
		$excelUtil = new ExcelUtil();
		$excelUtil->addCsvHeader($csvHeaderArr);
		$excelUtil->addCsvLineItem($csvHeaderArr, $csvItemArr, $specialOptionsArr);
		$excelUtil->initDownload('blacklist.csv');
		break;
		
	default:
		$mandantenArrListe = BlacklistManager::selectMandantenListe();
		
		// beim mandant magolino
		if($mandant == 'magolino') {
			// Zmail und Silver entfernen
			unset($mandantenArrListe[array_search('Zmail', $mandantenArrListe)]);
			unset($mandantenArrListe[array_search('Silver', $mandantenArrListe)]);
		}
		
		$grundArrListe = BlacklistManager::selectTop10Beschwerden();
		$countBlacklistItems = BlacklistManager::countBlacklistItem();
		
		$zeblaSmartyAction = new SmartyFactory();
		$zeblaSmartyAction->setTemplateName('start');
		$zeblaSmartyAction->assign('isAdmin', $isAdmin);
		$zeblaSmartyAction->assign('mandantenArrListe', $mandantenArrListe);
		$zeblaSmartyAction->assign('grundArrListe', $grundArrListe);
		$zeblaSmartyAction->assign('mitarbeiter', $mitarbeiter);
		$zeblaSmartyAction->assign('mandant', $mandant);
		
		// download counts
		$zeblaSmartyAction->assign('countBlacklistItems', $countBlacklistItems);
		
		$action_content = $zeblaSmartyAction->display();
		break;
}


// letzten 10 eintr�gen
$latestOrderBy = '`id` DESC';
$latestItemsArr = BlacklistManager::selectBlacklistItems($latestOrderBy, 10);

// smarty template initialisieren
$zeblaSmarty = new SmartyFactory();
$zeblaSmarty->setTemplateName('index');
$zeblaSmarty->assign('mandant', $mandant);
$zeblaSmarty->assign('adminUsers', $adminUsers);

$zeblaSmarty->assign('searchEmail', $searchEmail);
$zeblaSmarty->assign('searchOption', $searchOption);

$zeblaSmarty->assign('action_content', $action_content);
$zeblaSmarty->assign('blacklistItemsArr', $latestItemsArr);

echo $zeblaSmarty->display();
?>