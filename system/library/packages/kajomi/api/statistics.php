<?php
require_once 'api/classes.php';
require_once DIR_Pear . 'HTTP/Request2.php';

		/**
		 * \cond 
		 */
		/**
		 * \endcond 
		 */

class statistics
{
	private $con;
	
	/**
	 * constructor
	 * @param iconnection $icon
	 */
	public function __construct($icon)
	{
		$this->con=$icon;
	}
	
	/**
	 * 
	 * executes statistics
	 * @param string $name
	 * statistics name
	 * @param array $_parameters
	 * associative array containing parameters
	 * @return
	 * table[] array of results
	 */
	public function execute($name,$_parameters)
	{
		$_p=array();
		foreach ($_parameters as $n => $v)
			array_push($_p,new parameter('?'.$n, $_parameters[$n]));
		
		$cc = call::create("api", "basic/statistics/execute")->addParam("name", $name)->addParam("_p", $_p);
		return $this->con->invoke($cc);
	}
	
	public function describe($name)
	{
		$cc = call::create("api", "basic/statistics/describe")->addParam("name", $name);
		return $this->con->invoke($cc);
	}	
}
	
?>