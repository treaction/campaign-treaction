<?php
require_once 'api/classes.php';
require_once DIR_Pear . 'HTTP/Request2.php';

		/**
		 * \cond 
		 */
		/**
		 * \endcond 
		 */

class categorization
{
	private $con;
	
	/**
	 * constructor
	 * @param iconnection $icon
	 */
	public function __construct($icon)
	{
		$this->con=$icon;
	}	
	
	
	/**
	 * adds new category
	 * @param category $c
	 * @return category
	 */
	public function add($c){
		$cc=call::create("api","basic/categorization/add")->addParam("c", $c);
		return $this->con->invoke($cc);
	}	
	
	/**
	 * updates existing category
	 * @param category $c
	 */
	public function update($c){
		$cc=call::create("api","basic/categorization/update")->addParam("c", $c);
		return $this->con->invoke($cc);
	}
	
	/**
	 * removes existing category
	 * @param unknown_type $c
	 */
	public function remove($c){
		$cc=call::create("api","basic/categorization/remove")->addParam("c", $c);
		return $this->con->invoke($cc);
	}

	//
	
	/**
	 * retrieves category by value
	 * @param string value
	 * @return array of category
	 */
	public function byValue($value){
		$cc=call::create("api","basic/categorization/byValue")->addParam("value", $value);
		return $this->con->invoke($cc);
	}
	/**
	 * retrieves category by identifier
	 * @param string ident
	 * @return array of category
	 */
	public function byIdent($ident){
		$cc=call::create("api","basic/categorization/byIdent")->addParam("ident", $ident);
		return $this->con->invoke($cc);
	}	
	/**
	 * retrieves category by id; only a single category will be returned!
	 * @param integer id
	 * @return category
	 */
	public function byId($id){
		$cc=call::create("api","basic/categorization/byId")->addParam("id", $id);
		return $this->con->invoke($cc);
	}	
	

	/**
	 * retrieves all root nodes (categories without a parent)
	 * @return array of category
	 */	
	public function getRootNodes(){
		$cc=call::create("api","basic/categorization/getRootNodes");
		return $this->con->invoke($cc);
	}
	/**
	 * retrieves all leaf nodes (categories tagged with isLeaf=1)
	 * @return array of category
	 */		
	public function getLeafNodes(){
		$cc=call::create("api","basic/categorization/getLeafNodes");
		return $this->con->invoke($cc);
	}
	
	/**
	 * retrieves child categories
	 * @param category c
	 * @return array of category
	 */	
	public function getChildren($c){
		if($c->isleaf==1)
			return array();
		$cc=call::create("api","basic/categorization/getChildren")->addParam("c", $c);
		return $this->con->invoke($cc);
	}
	
	public function getAllChildren($c){
		$c->_children=array();
		foreach($this->getChildren($c) as $cc)
			array_push($c->_children,$this->getAllChildren($cc));
		return $c;	
	}
	
	/**
	 * retrieves parent categories
	 * @param category c
	 * @return array of category
	 */		
	public function getParents($c){
		$cc=call::create("api","basic/categorization/getParents")->addParam("c", $c);
		return $this->con->invoke($cc);
	}	
	
	
	/**
	 * links 2 categories
	 * @param category parent
	 * @param category child
	 */		
	public function linkChild($parent,$child){
		$cc=call::create("api","basic/categorization/linkChild")->addParam("parent", $parent)->addParam("child", $child);
		return $this->con->invoke($cc);
	}
	/**
	 * unlinks 2 categories
	 * @param category parent
	 * @param category child
	 */		
	public function unlinkChild($parent,$child){
		$cc=call::create("api","basic/categorization/unlinkChild")->addParam("parent", $parent)->addParam("child", $child);
		return $this->con->invoke($cc);
	}	
	
	//
	
	public function linkUser($c,$userid,$score){
		$cc=call::create("api","basic/categorization/linkUser")->addParam("c", $c)->addParam("userid", $userid)->addParam("score", $score);
		return $this->con->invoke($cc);
	}		
	public function unlinkUser($c,$userid){
		$cc=call::create("api","basic/categorization/linkUser")->addParam("c", $c)->addParam("userid", $userid);
		return $this->con->invoke($cc);
	}		
	
	public function linkTrackinglink($c,$l,$score){
		$cc=call::create("api","basic/categorization/linkTrackinglink")->addParam("c", $c)->addParam("l", $l)->addParam("score", $score);
		return $this->con->invoke($cc);
	}		
	public function unlinkTrackingLink($c,$l){
		$cc=call::create("api","basic/categorization/unlinkTrackingLink")->addParam("c", $c)->addParam("l", $l);
		return $this->con->invoke($cc);
	}	
	
	public function getUserCategories($userid){
		$cc=call::create("api","basic/categorization/getUserCategories")->addParam("userid", $userid);
		return $this->con->invoke($cc);
	}		
	
	public function getLinkCategories($l){
		$cc=call::create("api","basic/categorization/getLinkCategories")->addParam("l", $l);
		return $this->con->invoke($cc);
	}		
	
	

	public function getCategoryUserCount($_categories){
		$cc=call::create("api","basic/categorization/getCategoryUserCount")->addParam("_categories", $_categories);
		return $this->con->invoke($cc);
	}	

	public function getCategoryUsers($c){
		$cc=call::create("api","basic/categorization/getCategoryUsers")->addParam("c", $c);
		return $this->con->invoke($cc);
	}		
	
	public function getCategoriesUsers($_c){
		$ret=NULL;
		foreach ($_c as $c)
		{
			$cc=call::create("api","basic/categorization/getCategoryUsers")->addParam("c", $c);
			if($ret==NULL)
			 	$ret=$this->con->invoke($cc);
			 else 
			 	$ret->_rows=array_merge($ret->_rows,$this->con->invoke($cc));
		}
		return $ret;
	}	
	
	
	public function compareCategories($userid,$field,$_categories){
		$ret=array();		
		$_usrcategories=$this->getUserCategories($userid);
		foreach($_usrcategories as $c)
			foreach($_categories as $s)
				if($s==$c->Value->{$field})
					array_push($ret, $c);
		return $ret;
	}
	
	public function recurseUp($c,$userid,$maxdepth,$score,$sc_multiplier=0.5,$depth=0)
	{
		$_ret=array();
		$this->linkUser($c,$userid,$score);		
		$_c=$this->getParents($c);
		if($_c==null)
			return array();		
		$_ret=array_merge($_ret,$_c);
		$score*=$sc_multiplier;
		if(++$depth>=$maxdepth || !$score>0.0)
			return $_c;
		foreach($_c as $cc){
			$tmp=array_merge($_ret,$this->recurseUp($cc, $userid, $maxdepth, $score,$sc_multiplier,$depth));
			$_ret=$tmp;			
		}			
		return $_ret;
	}
	
	public function recurseLinkUp($c,$l,$maxdepth,$score,$sc_multiplier=0.5,$depth=0)
	{
		$_ret=array();
		$this->linkUser($c,$l,$score);		
		$_c=$this->getParents($c);
		if($_c==null)
			return array();		
		$_ret=array_merge($_ret,$_c);
		$score*=$sc_multiplier;
		if(++$depth>=$maxdepth || !$score>0.0)
			return $_c;
		foreach($_c as $cc){
			$tmp=array_merge($_ret,$this->recurseLinkUp($cc, $l, $maxdepth, $score,$sc_multiplier,$depth));
			$_ret=$tmp;			
		}			
		return $_ret;
	}	

	
	// temp
	

}	
?>