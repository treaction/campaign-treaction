<?php
    /**
     * (C)opyright 2002-2007 optivo GmbH, Stralauer Allee 2, 10247 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Main entry point for accessing all webservices.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Oct 08 15:26:20 CEST 2008
     */

    require_once('rpc_attachment.php');
    require_once('rpc_basicreporting.php');
    require_once('rpc_blacklist.php');
    require_once('rpc_mailing.php');
    require_once('rpc_mailid.php');
    require_once('rpc_mailingreporting.php');
    require_once('rpc_optinprocess.php');
    require_once('rpc_recipientfilter.php');
    require_once('rpc_recipientlist.php');
    require_once('rpc_recipient.php');
    require_once('rpc_response.php');
    require_once('rpc_session.php');
    require_once('rpc_system.php');
    require_once('rpc_unsubscribe.php');

    class BroadmailRpcFactory {

        var $sessionId;
        var $fault;
        var $error;

        // NuSOAP variables
        var $endPoint;
        var $proxyHost;
        var $proxyPort;
        var $proxyUsername;
        var $proxyPassword;
        var $timeout;
        var $responseTimeout;

        function BroadmailRpcFactory($mandatorId, $username, $password, $endPoint = 'http://api.broadmail.de/soap11',
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 120) {

            $this->endPoint = $endPoint;
            $this->proxyHost = $proxyHost;
            $this->proxyPort = $proxyPort;
            $this->proxyUsername = $proxyUsername;
            $this->proxyPassword = $proxyPassword;
            $this->timeout = $timeout;
            $this->responseTimeout = $responseTimeout;

            $sessionWs = $this->newSessionWebservice();
            $this->sessionId = $sessionWs->login($mandatorId, $username, $password);
            $this->fault = $sessionWs->fault;
            $this->error = $sessionWs->getError();
        }

        function logout() {
            $sessionWs = $this->newSessionWebservice();
            $sessionWs->logout();
        }

        function getError() {
            return $this->error;
        }

        function newAttachmentWebservice() {
            return new BroadmailRpcAttachmentWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }
        function newBasicReportingWebservice() {
            return new BroadmailRpcBasicReportingWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }
        function newBlacklistWebservice() {
            return new BroadmailRpcBlacklistWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }
        function newMailingWebservice() {
            return new BroadmailRpcMailingWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }
        function newMailIdWebservice() {
            return new BroadmailRpcMailIdWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }
        function newMailingReportingWebservice() {
            return new BroadmailRpcMailingReportingWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }
        function newOptinProcessWebservice() {
            return new BroadmailRpcOptinProcessWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }
        function newRecipientFilterWebservice() {
            return new BroadmailRpcRecipientFilterWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }
        function newRecipientListWebservice() {
            return new BroadmailRpcRecipientListWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }
        function newRecipientWebservice() {
            return new BroadmailRpcRecipientWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }
        function newResponseWebservice() {
            return new BroadmailRpcResponseWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }
        function newSessionWebservice() {
            return new BroadmailRpcSessionWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }
        function newSystemWebservice() {
            return new BroadmailRpcSystemWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }
        function newUnsubscribeWebservice() {
            return new BroadmailRpcUnsubscribeWebserviceImpl($this->sessionId, $this->endPoint,
                $this->proxyHost, $this->proxyPort, $this->proxyUsername, $this->proxyPassword,
                $this->timeout, $this->responseTimeout);
        }

    }

?>