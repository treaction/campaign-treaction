<?
    /**
     * (C)opyright 2002-2007 optivo GmbH, Stralauer Allee 2, 10247 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * Example illustrating the use of the BroadmailRpcFactory to access the SOAP 1.1 interface.
     *
     * @author Peter Romianowski
     * @version 1.0 2005-05-06
     */
?>

<html>
    <head>
        <title>optivo&reg; broadmail SOAP1.1 PHP Library Demo 3</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
    </head>
    <body>
    
        <h1>optivo&reg; broadmail SOAP1.1 PHP Library Demo 3</h1>
        
        This is a small demonstration on how to use this library to access the optivo&reg; broadmail functionality.<br>
        It illustrates the process of adding recipients to a recipient list. Note that for most recipient lists the
        recipient id equals the recipient's email address.
        

<?

    // Innclude the library
    require_once('../broadmail_rpc.php');
    
    $submitted = $_GET['mandatorId'];
    if ($submitted) {
    
        // The form has been submitted.
        // Create a new factory and login.
        $factory = new BroadmailRpcFactory($_GET['mandatorId'], $_GET['username'], $_GET['password'], $_GET['endPoint']);
        
        // This is how error handling works. You should check the result of the getError() method
        // after each(!) call to a webservice.
        if ($factory->getError()) {
            die ('<h2>Error during login</h2><code>'.$factory->getError().'</code>');
        }
        
        // Now create a RecipientWebservice and insert the given recipient.
        $recipientWs = $factory->newRecipientWebservice();
        
        $addSuccess = $recipientWs->add(
            $_GET['recipientListId'],       // recipientListId
            $_GET['optinProcessId'],        // optinProcessId
            $_GET['recipientId'],           // recipientId
            $_GET['recipientEmail'],        // emailAddress
            array(),                        // attributeNames (Give an array with the names of the attributes as specified in your recipient list.)
            array()                         // attributeValues (Give an array with the corresponding values. Note the conversion rules!)
        );
        
        if ($recipientWs->getError()) {
            die ('<h2>Error in RecipientWebservice</h2><code>'.$recipientWs->getError().'</code>');
        }

        // Finally: Logout
        $factory->logout();

    } 

?>
        
        <h2>Login Information</h2>
        <form method="GET">
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td>Endpoint</td>
                <td><input type="text" name="endPoint" value="http://api.broadmail.de/soap11" size="50"></td>
            </tr>
            <tr>
                <td>Mandator ID</td>
                <td><input type="text" name="mandatorId" size="50"></td>
            </tr>
            <tr>
                <td>Username</td>
                <td><input type="text" name="username" size="50"></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="text" name="password" size="50"></td>
            </tr>
        </table>

        <h2>Recipient Insertion</h2>
        The following values are used to insert a recipient.<br><br>
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td>Recipient List Id</td>
                <td><input type="text" name="recipientListId" size="50"></td>
            </tr>
            <tr>
                <td>Optin Process Id</td>
                <td><input type="text" name="optinProcessId" size="50"></td>
            </tr>
            <tr>
                <td>Recipient Id</td>
                <td><input type="text" name="recipientId" size="50"></td>
            </tr>
            <tr>
                <td>Recipient Email Address</td>
                <td><input type="text" name="recipientEmail" size="50"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit"></td>
            </tr>
        </table>

        </form>

        <? if ($submitted) { ?>
        
            <h2>Results</h2>
            <? if ($addSuccess == "true") { ?>
                The recipient has been inserted.
            <? } else { ?>
                There is already a recipient with the given id.
            <? } ?>
        
        <? } ?>

    </body>
</html>
