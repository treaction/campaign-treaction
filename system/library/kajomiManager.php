<?php
class kajomiManager {
    public function __construct() {
        $this->dbh = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
    }

    public function getMailingData($mID, $msgid) {
        $firePhp = $this->firePhp();
        
        $qry_login = 'SELECT `m_login`, `api_user`, `pw`' . 
            ' FROM `mandant_asp`' . 
            ' WHERE `m_id` = :mID ' . 
                ' AND `asp_id` = "4"'
        ;
        $stmt_login = $this->dbh->prepare($qry_login);
        $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt_login->execute();
        $row = $stmt_login->fetch();

        $curl_url = 'http://' . $row['m_login'] . '/toolz/getdata.php?mod=' . $row['api_user'] . '&hash=' . md5($row['pw'] . $msgid) . '&msgid=' . $msgid . '&typ=mailinfo';

        $c = curl_init($curl_url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($c);
        curl_close($c);

        $xml = simplexml_load_string($result);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($msgid, 'msgid');
            $firePhp->log($xml, 'xml');
        }

        $dataArr = array(
            'versanddatum' => $xml->versandDatum,
            'versandzeit' => $xml->versandZeit,
            'versendet' => $xml->gesamt->menge->brutto,
            'softbounces' => $xml->gesamt->menge->softbounces,
            'hardbounces' => $xml->gesamt->menge->hardbounces,
            'openings' => $xml->gesamt->oeffner->gesamt,
            'openings_u' => $xml->gesamt->oeffner->unique,
            'klicks' => $xml->gesamt->klicker->gesamt,
            'klicks_u' => $xml->gesamt->klicker->unique
        );

        return $dataArr;
    }
    
    protected function firePhp() {
        $firePhp = null;
        if (intval($_SESSION['benutzer_id']) === 24) {
            require_once($_SERVER['DOCUMENT_ROOT'] . '/system/library/FirePHPCore/FirePHP.class.php');
            $firePhp = FirePHP::getInstance(true);
        }

        return $firePhp;
    }
}
?>