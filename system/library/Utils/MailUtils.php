<?php
final class MailUtils {
	const EMAIL_EMPFAENGER = 'sami.jarmoud@treaction.de'; // sami.jarmoud@treaction.onmicrosoft.com
	static public $EMAIL_BETREFF = 'MaaS API Error - campaign-treaction.de';
	static protected $headerDataArray = array(
		'MIME-Version: 1.0',
		'Content-type: text/plain; charset=iso-8859-1',
		'From: Treaction MaaS Mailer <api@campaign-treaction.de>'
	);
	
	
	
	
	
	/**
	 * sendMail
	 * 
	 * @param string $message
	 * @return void
	 */
	public static function sendMail($message) {
		\mail(
			self::EMAIL_EMPFAENGER,
			static::$EMAIL_BETREFF,
			$message,
			self::createHeaderData()
		);
	}
	
	/**
	 * sendEmailToUser
	 * 
	 * @param string $userEmail
	 * @param string $message
	 * @return void
	 */
	public static function sendEmailToUser($userEmail, $message) {
		\mail(
			$userEmail,
			static::$EMAIL_BETREFF,
			$message,
			self::createHeaderData()
		);
	}
	
	
	
	/**
	 * createHeaderData
	 * 
	 * @return string
	 */
	protected static function createHeaderData() {
		$headerDataArray = \array_merge(
			self::$headerDataArray,
			array(
				'X-Mailer: PHP/' . \phpversion()
			)
		);
		
		return \implode(\chr(10), $headerDataArray);
	}

}