<?php
final class ClientUtils {
	/**
	 * getClientsSelectItems
	 * 
	 * @param integer $clientId
	 * @param boolean $onlyLoggedClient
	 * @return string
	 */
	static public function getClientsSelectItems($clientId, $onlyLoggedClient = false) {
		$result = '';
		
		$allClientsIdsDataArray = self::getClientsAccessDataArrayByLoggedUser($onlyLoggedClient);
		foreach ($allClientsIdsDataArray as $tmpClientId => $tmpClientName) {
			$selected = '';
			
			if ((int) $clientId === (int) $tmpClientId) {
				$selected = 'selected';
			}
			
			$result .= self::getClientSelectOption(
				$_SESSION['mandant_all'][$tmpClientName],
				$tmpClientName,
				$selected
			);
		}
		unset($allClientsIdsDataArray);
		
		return $result;
	}
	
	/**
	 * getClientsAccessDataArrayByLoggedUser
	 * 
	 * @param boolean $onlyLoggedClient
	 * @return array
	 */
	static public function getClientsAccessDataArrayByLoggedUser($onlyLoggedClient = false) {
		if (\count($_SESSION['mandant_all_id_Arr']) > 0 
			&& $_SESSION['mID'] > 0
		) {
			$tmpAllClientsIdsDataArray = \array_flip($_SESSION['mandant_all_id_Arr']);
			$accessClientsDataArray = array(
				$_SESSION['mID'] => $tmpAllClientsIdsDataArray[$_SESSION['mID']]
			);

			if ((boolean) $onlyLoggedClient !== true) {
				if (\count($_SESSION['zugang_mandant']) > 0) {
					foreach ($_SESSION['zugang_mandant'] as $zmKey) {
						if ((int) $zmKey) {
							$accessClientsDataArray[$zmKey] = $tmpAllClientsIdsDataArray[$zmKey];
						}
					}
				}

				if (!\array_key_exists($_SESSION['hauptmandantID'], $accessClientsDataArray)) {
					$accessClientsDataArray[$_SESSION['hauptmandantID']] = $tmpAllClientsIdsDataArray[$_SESSION['hauptmandantID']];
				}
				\ksort($accessClientsDataArray);
			}
			unset($tmpAllClientsIdsDataArray);
		} else {
			throw new \Exception('You don`t have permission.');
		}

		return $accessClientsDataArray;
	}
	
	/**
	 * getClientNameByClientId
	 * 
	 * @param integer $clientId
	 * @return string
	 */
	static public function getClientNameByClientId($clientId) {
		$allClientsIdsDataArray = \array_flip($_SESSION['mandant_all_id_Arr']);
		
		return $allClientsIdsDataArray[(int) $clientId];
	}
	
	/**
	 * getClientSelectOption
	 * 
	 * @param string $clientValue
	 * @param string $clientName
	 * @param string $selected
	 * @return string
	 */
	static protected function getClientSelectOption($clientValue, $clientName, $selected = '') {
		return \HtmlFormUtils::createOptionItem(
			array(
				'style' => 'background-color:#EAEAEA;color:#333333;',
				'value' => $clientValue,
				'selected' => $selected
			),
			$clientName
		);
	}
}
