<?php
final class DirectoryUtils {
	/**
	 * isDir
	 * 
	 * @param string $path
	 * @throws \RuntimeException
	 * @return void
	 */
	public static function isDir($path) {
		if (!\is_dir($path)) {
			throw new \RuntimeException('no valid path given: ' . \htmlspecialchars($path), 1432634568);
		}
	}
	
	/**
	 * isReadable
	 * 
	 * @param string $path
	 * @throws \RuntimeException
	 * @return void
	 */
	public static function isReadable($path) {
		self::isDir($path);
		
		if (!\is_readable($path)) {
			throw new \RuntimeException('path is not readable: ' . \htmlspecialchars($path), 1432635339);
		}
	}
	
	/**
	 * isWritable
	 * 
	 * @param string $path
	 * @throws \RuntimeException
	 * @return void
	 */
	public static function isWritable($path) {
		self::isDir($path);
		
		if (!\is_writable($path)) {
			throw new \RuntimeException('path is not writable: ' . \htmlspecialchars($path), 1432641186);
		}
	}
}