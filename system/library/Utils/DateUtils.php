<?php
final class DateUtils {
	public static $monthDataArray = array(
		1 => 'Januar',
		2 => 'Februar',
		3 => 'M&auml;rz',
		4 => 'April',
		5 => 'Mai',
		6 => 'Juni',
		7 => 'Juli',
		8 => 'August',
		9 => 'September',
		10 => 'Oktober',
		11 => 'November',
		12 => 'Dezember'
	);
	public static $abkzMonthDataArray = array(
		1 => 'Jan.',
		2 => 'Feb.',
		3 => 'M&auml;rz',
		4 => 'Apr.',
		5 => 'Mai',
		6 => 'Juni',
		7 => 'Juli',
		8 => 'Aug.',
		9 => 'Sep.',
		10 => 'Okt.',
		11 => 'Nov.',
		12 => 'Dez.'
	);
	
	
	
	
	
	/**
	 * getWeekDay
	 * 
	 * @param \DateTime $dateTime
	 * @param boolean $shortType
	 * @return string
	 */
	public static function getWeekDay(\DateTime $dateTime, $shortType = false) {
		if ((boolean) $shortType === true) {
			$weekDayDataArray = array(
				'So.',
				'Mo.',
				'Di.',
				'Mi.',
				'Do.',
				'Fr.',
				'Sa.'
			);
		} else {
			$weekDayDataArray = array(
				'Sonntag',
				'Montag',
				'Dienstag',
				'Mittwoch',
				'Donnerstag',
				'Freitag',
				'Samstag'
			);
		}

		return $weekDayDataArray[$dateTime->format('w')];
	}

	/**
	 * isDateToday
	 * 
	 * @param \DateTime $dateTime
	 * @return boolean
	 */
	public static function isDateToday(\DateTime $dateTime) {
		$today = new \DateTime();

		$result = false;
		if ($today->format('dmY') === $dateTime->format('dmY')) {
			$result = true;
		}

		return $result;
	}

	/**
	 * getDayCountByMonthAndYear
	 * 
	 * @param integer $month
	 * @param integer $year
	 * @return integer
	 */
	public static function getDayCountByMonthAndYear($month, $year) {
		return \cal_days_in_month(\CAL_GREGORIAN, $month, $year);
	}

	/**
	 * getDayDateTimeFromWeek
	 * 
	 * @param integer $year
	 * @param integer $weekNum
	 * @return array
	 */
	public static function getDayDateTimeFromWeek($year, $weekNum) {
		$resultDataArray = array();

		$datetime = new \DateTime('00:00:00');
		$datetime->setISODate((int) $year, $weekNum, 1);

		$week = new \DatePeriod(
			$datetime,
			new \DateInterval('P1D'),
			6
		);

		foreach ($week as $day) {
			$resultDataArray[] = $day;
		}

		return $resultDataArray;
	}
	
	/**
	 * getCurrentDateTime
	 * 
	 * @param string $format
	 * @return mixed
	 */
	public static function getCurrentDateTime($format = 'd.m.Y, H:i') {
		$datetime = new \DateTime();
		
		return $datetime->format($format);
	}

}