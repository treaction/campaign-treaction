<?php
final class FormatUtils {
	const euroSignInUtf8Unicode = ' €';
	const euroSignHtmlCode = ' &euro;';
	
	
	
	
	
	/**
	 * sumFormat
	 * 
	 * @param float $number
	 * @param integer $decimal
	 * @return numeric
	 */
	static public function sumFormat($number, $decimal = 2) {
		return \number_format($number, $decimal, ',', '.');
	}

	/**
	 * numberFormat
	 * 
	 * @param float $number
	 * @return float
	 */
	public static function numberFormat($number) {
		return \number_format($number, 0, '', '.');
	}
	
	/**
	 * totalPriceFormat
	 * 
	 * @param mixed $price
	 * @return mixed
	 */
	public static function totalPriceFormat($price){
		$result = \str_replace('.','', $price);
		
		return str_replace(',','.', $result);
	}
	/**
	 * rateCalculation
	 * 
	 * @param integer $value1
	 * @param integer $value2
	 * @return float
	 */
	public static function rateCalculation($value1, $value2) {
		$result = 0;
		if ((int) $value1 > 0 
			&& (int) $value2 > 0
		) {
			$result = \round((($value1 / $value2) * 100), 2);
		}
		
		return $result;
	}
	
	/**
	 * averageCalculation
	 * 
	 * @param integer $value1
	 * @param integer $value2
	 * @return float
	 */
	public static function averageCalculation($value1, $value2) {
		$result = 0;
		if ((int) $value1 > 0 
			&& (int) $value2 > 0
		) {
			$result = \round(($value1 / ($value2 / 1000)), 2);
		}
		
		return $result;
	}

	/**
	 * priceCalculationByBillingType
	 * 
	 * @param \CampaignEntity $campaignEntity
	 * @param boolean $applyBroking
	 * @return float
	 */
	public static function priceCalculationByBillingType(CampaignEntity $campaignEntity, $applyBroking = FALSE) {
		$price = 0;
		switch ($campaignEntity->getAbrechnungsart()) {
			case 'TKP':
				$price = self::calculateTkpPrice(
					$campaignEntity,
					$applyBroking
				);
				break;

			case 'Hybri':
			case 'Hybrid':
				$price = self::calculateHybridPrice(
					$campaignEntity,
					TRUE,
					TRUE
				);
				break;

			case 'Festp':
			case 'Festpreis':
				$price = self::calculateFixedPrice(
                                        $campaignEntity,
                                        $applyBroking
                                        );
				break;

			case 'CPL':
			case 'CPO':
			case 'CPC':                          
				$price = self::calculateCpxPrice(
					$campaignEntity,
					$applyBroking
				);                            
				break;
		}

		return $price;
	}

	/**
	 * validPriceCalculationByBillingType
	 * 
	 * @param \CampaignEntity $campaignEntity
	 * @param boolean $applyBroking
	 * @return float
	 */
	public static function validPriceCalculationByBillingType(CampaignEntity $campaignEntity, $applyBroking = FALSE) {
		$price = 0;   
		switch ($campaignEntity->getAbrechnungsart()) {
			case 'TKP':
				$price = self::calculateTkpPrice(
					$campaignEntity,
					$applyBroking
				);
				break;

			case 'Hybri':
			case 'Hybrid':
				$price = self::calculateHybridPrice(
					$campaignEntity,
					TRUE,
					TRUE
				);
				break;

			case 'Festp':
			case 'Festpreis':
				$price = self::calculateFixedPrice( 
                                        $campaignEntity,
                                        TRUE
                                        );
				break;

			case 'CPL':
			case 'CPO':
			case 'CPC':
				$price = self::calculateCpxPrice(
					$campaignEntity,
					TRUE
				);
				break;
		}

		return $price;
	}

	/**
	 * totalPriceCalculationByBillingType
	 * 
	 * @param \CampaignEntity $campaignEntity
	 * @param boolean $applyBroking
	 * @return float
	 */
	public static function totalPriceCalculationByBillingType(CampaignEntity $campaignEntity, $applyBroking = FALSE) {
		$price = self::priceCalculationByBillingType(
			$campaignEntity,
			$applyBroking
		);

		switch ($campaignEntity->getAbrechnungsart()) {
			case 'Festp':
			case 'Festpreis':
				break;

			default:
				$price += $campaignEntity->getUnit_price();
				break;
		}

		return $price;
	}

	/**
	 * getAndCalculateETkpPrice
	 * 
	 * @param \CampaignEntity $campaignEntity
	 * @param float $price
	 * @return string
	 */
	public static function getAndCalculateETkpPrice(CampaignEntity $campaignEntity, $price = 0) {
		$result = 0;

		if ($campaignEntity->getVersendet() > 0) {
			if ($price === 0) {
				$price = self::totalPriceCalculationByBillingType($campaignEntity);
			}

			if ($price > 0) {
				$result = $price / ($campaignEntity->getVersendet() / 1000);
			}
		}

		return self::sumFormat($result) . self::euroSignHtmlCode;
	}

	/**
	 * cutString
	 * 
	 * @param string $text
	 * @param integer $length
	 * @return string
	 */
	public static function cutString($text, $length) {
		if (\strlen($text) > $length) {
			$cutText = \substr($text, 0, $length);
			$cutText = $cutText . '...';
		} else {
			$cutText = $text;
		}

		return $cutText;
	}

	/**
	 * getFormatetTotalPriceByBillingType
	 * 
	 * @param \CampaignEntity $campaignEntity
	 * @param float $price
	 * @return string
	 */
	public static function getFormatetTotalPriceByBillingType(CampaignEntity $campaignEntity, $price = 0) {
		if ($price === 0) {
			/**
			 * totalPriceCalculationByBillingType
			 */
			$price = self::totalPriceCalculationByBillingType($campaignEntity);
		}

		switch ($campaignEntity->getAbrechnungsart()) {
			case 'TKP':
			case 'Hybri':
			case 'Hybrid':
			case 'Festp':
			case 'Festpreis':
				$result = '<span style="color: #7D960B; font-weight: bold;">' . self::sumFormat($price) . self::euroSignHtmlCode . '</span>';
				break;

			default:
				// CPX
				if ($campaignEntity->getLeads_u() > 0) {
					$result = '<span style="color: #7D960B; font-weight: bold;">' . self::sumFormat($price) . self::euroSignHtmlCode . '</span>';
				} else {
					$result = '<span style="color: red;">' . self::sumFormat($price) . self::euroSignHtmlCode . '</span>';
				}
				break;
		}

		return $result;
	}

		/**
	 * getIntegerFormatetTotalPriceByBillingType
	 * 
	 * @param \CampaignEntity $campaignEntity
	 * @param float $price
	 * @return string
	 */
	public static function getIntegerFormatetTotalPriceByBillingType(CampaignEntity $campaignEntity, $price = 0) {
		if ($price === 0) {
			/**
			 * totalPriceCalculationByBillingType
			 */
			$price = self::totalPriceCalculationByBillingType($campaignEntity);
		}

		switch ($campaignEntity->getAbrechnungsart()) {
			case 'TKP':
			case 'Hybri':
			case 'Hybrid':
			case 'Festp':
			case 'Festpreis':
				$result = self::sumFormat($price);
				break;

			default:
				// CPX
				if ($campaignEntity->getLeads_u() > 0) {
					$result = self::sumFormat($price);
				} else {
					$result = self::sumFormat($price);
				}
				break;
		}

		return $result;
	}
	/**
	 * cleanUpNotes
	 * 
	 * @param string $string
	 * @return string
	 */
	public static function cleanUpNotes($string) {
		return \str_replace(
			array(
				'&#10;',
				'&#13;'
			),
			array(
				\chr(13),
				\chr(13)
			),
			$string
		);
	}

	/**
	 * nl2Br
	 * 
	 * @param string $string
	 * @return string
	 */
	public static function nl2Br($string) {
		return \str_replace(
			array(
				\chr(13),
				\chr(10)
			),
			array(
				'<br />',
				'<br />'
			),
			\trim($string)
		);
	}
	
	
	/**
	* cleanPriceContent
	*
	* @param string $productNetPrice
	* @return float
	*/
	public static function cleanPriceContent($productNetPrice) {
		return \str_replace(\trim(self::euroSignHtmlCode), '', \strip_tags($productNetPrice));
	}
	
	
	/**
	 * calculateTkpPrice
	 * 
	 * @param CampaignEntity $campaignEntity
	 * @param boolean $applyBroking
	 * @return float
	 */
	protected static function calculateTkpPrice(CampaignEntity $campaignEntity, $applyBroking) {
		if ((boolean) $applyBroking === TRUE
			&& (
				$campaignEntity->getBroking_volume() > 0 
				&& $campaignEntity->getBroking_price() > 0
			)
		) {
			$price = ($campaignEntity->getPreis() * ($campaignEntity->getGebucht() - $campaignEntity->getBroking_volume())) 
				+ (($campaignEntity->getPreis() - $campaignEntity->getBroking_price()) * $campaignEntity->getBroking_volume())
			;
		} else {
			$price = $campaignEntity->getPreis() * $campaignEntity->getGebucht();
		}
				
		return $price / 1000;
	}
        
        /**
	 * calculateTkpPriceForHybridCampaign
	 * 
	 * @param CampaignEntity $campaignEntity
	 * @param boolean $applyBroking
	 * @return float
	 */
	protected static function calculateTkpPriceForHybridCampaign(CampaignEntity $campaignEntity, $applyBroking) {
		if (((boolean) $applyBroking === TRUE
			&& (
				$campaignEntity->getBroking_volume() > 0 
				&& $campaignEntity->getHybrid_preis() > 0
                                && $campaignEntity->getPreis() > 0
			) )

		) {
			$price = ($campaignEntity->getPreis() * ($campaignEntity->getGebucht() - $campaignEntity->getBroking_volume())) 
				+ (($campaignEntity->getPreis() - $campaignEntity->getHybrid_preis()) * $campaignEntity->getBroking_volume())
			;
		} else {
			$price = $campaignEntity->getPreis() * $campaignEntity->getGebucht();
		}
				
		return $price / 1000;
	}
	
	/**
	 * calculateFixedPrice
	 * 
	 * @param CampaignEntity $campaignEntity
	 * @return float
	 */
	protected static function calculateFixedPrice(CampaignEntity $campaignEntity, $applyBroking) {
            if ((boolean) $applyBroking === TRUE
                    && 
                    ($campaignEntity->getBroking_price() > 0)
                    ){
                $price = $campaignEntity->getPreis_gesamt() - $campaignEntity->getBroking_price();
            }else{
                $price = $campaignEntity->getPreis_gesamt();
            }
		return $price;
	}
	
	/**
	 * calculateCpxPrice
	 * 
	 * @param CampaignEntity $campaignEntity
	 * @param boolean $calculateValidPrice
	 * @return float
	 */
	protected static function calculateCpxPrice(CampaignEntity $campaignEntity, $calculateValidPrice) {
		if ((boolean) $calculateValidPrice === TRUE) {
                        if ($campaignEntity->getLeads_u() > 0) {
				$price = ($campaignEntity->getPreis() * $campaignEntity->getLeads_u()) - ($campaignEntity->getBroking_price() * $campaignEntity->getLeads_u());
			} else {
				$price = ($campaignEntity->getPreis() * $campaignEntity->getLeads()) - ($campaignEntity->getBroking_price() * $campaignEntity->getLeads());
			}
			
		} else {
			if ($campaignEntity->getLeads_u() > 0) {
				$price = $campaignEntity->getPreis() * $campaignEntity->getLeads_u();
			} else {
				$price = $campaignEntity->getPreis() * $campaignEntity->getLeads();
			}
		}
		
		return $price;
	}
	
	/**
	 * calculateCpxPriceForHybridCampaign
	 * 
	 * @param CampaignEntity $campaignEntity
	 * @param boolean $calculateValidPrice
	 * @return float
	 */
	protected static function calculateCpxPriceForHybridCampaign(CampaignEntity $campaignEntity, $calculateValidPrice) {
		if ((boolean) $calculateValidPrice === TRUE) {
                     if ($campaignEntity->getLeads_u() > 0) {
				$price = ($campaignEntity->getPreis2() * $campaignEntity->getLeads_u()) - ($campaignEntity->getHybrid_preis2() * $campaignEntity->getLeads_u());
			} else {
				$price = ($campaignEntity->getPreis2() * $campaignEntity->getLeads()) - ($campaignEntity->getHybrid_preis2() * $campaignEntity->getLeads());
			}
		} else {
			if ($campaignEntity->getLeads_u() > 0) {
				$price = $campaignEntity->getPreis2() * $campaignEntity->getLeads_u();
			} else {
				$price = $campaignEntity->getPreis2() * $campaignEntity->getLeads();
			}
		}
		
		return $price;
	}
	
	/**
	 * calculateHybridPrice
	 * 
	 * @param CampaignEntity $campaignEntity
	 * @param boolean $calculateValidPrice
	 * @param boolean $applyBroking
	 * @return float
	 */
	protected static function calculateHybridPrice(CampaignEntity $campaignEntity, $calculateValidPrice, $applyBroking = FALSE) {
		$tkpPrice = self::calculateTkpPriceForHybridCampaign(
			$campaignEntity,
			$applyBroking
		);
		
		$cpxPrice = self::calculateCpxPriceForHybridCampaign(
			$campaignEntity,
			$calculateValidPrice
		);
		
		return $tkpPrice + $cpxPrice;
	}

}