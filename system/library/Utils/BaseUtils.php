<?php
final class BaseUtils {
	protected static $debugUserId = 2;
	protected static $devIpAddress = '87.138.67.216';
	
	
	
	/**
	 * getBasePath
	 * 
	 * @return string
	 */
	public static function getBasePath() {
		return \dirname($_SERVER['SCRIPT_NAME']); 
	}

	/**
	 * getFirstPath
	 * 
	 * @param string $basePath
	 * @return string
	 */
	public static function getFirstPath($basePath) {
		$tmpDataArray = \preg_split('/([[:upper:]][[:lower:]]+)/', $basePath, null, \PREG_SPLIT_DELIM_CAPTURE|\PREG_SPLIT_NO_EMPTY);

		return \current($tmpDataArray);
	}

	/**
	 * checkIfDirExists
	 * 
	 * @param string $beginPath
	 * @param string $dbPath
	 * @return boolean
	 */
	public static function checkIfDirExists($beginPath, $dbPath) {
		return \is_dir('..' . $beginPath . $dbPath);
	}

	/**
	 * getScriptFileForRequestData
	 * 
	 * @return string
	 */
	public static function getScriptFileForRequestData() {
		return \str_replace(\RegistryUtils::get('basePath') . \DIRECTORY_SEPARATOR, '', $_SERVER['PHP_SELF']);
	}
	
	/**
	 * setScriptFileforRequestData
	 * 
	 * @param string $script
	 * @return string
	 */
	public static function setScriptFileforRequestData($script) {
		return \dirname(self::getScriptFileForRequestData()) . \DIRECTORY_SEPARATOR . $script;
	}
	
	/**
	 * isProdSystem
	 * 
	 * @return boolean
	 */
	public static function isProdSystem() {
		return (self::getMaasEnvironmentType() === 'Production');
	}
	
	/**
	 * isTestSystem
	 * 
	 * @return boolean
	 */
	public static function isTestSystem() {
		return (self::getMaasEnvironmentType() === 'Testing');
	}
	
	/**
	 * isDevSystem
	 * 
	 * @return boolean
	 */
	public static function isDevSystem() {
		return (self::getMaasEnvironmentType() === 'Development');
	}
	
	/**
	 * getUserIp
	 * 
	 * @return string
	 */
	public static function getUserIp() {
		if (\getenv('HTTP_X_FORWARDED_FOR')) {
			$result = \getenv('HTTP_X_FORWARDED_FOR');
		} else {
			$result = \getenv('REMOTE_ADDR');
		}
		
		return $result;
    }
	
	/**
	 * getAjaxRequestUrl
	 * 
	 * @param string $actionMethod
	 * @return string
	 */
	public static function getAjaxRequestUrl() {
		return 'AjaxRequests/ajax.php';
	}
	
	/**
	 * setAjaxRequestUri
	 * 
	 * @param string $actionMethod
	 * @param string $actionType
	 * @param array $actionTypeDataArray
	 * @return string
	 */
	public static function setAjaxRequestUri($actionMethod, $actionType, array $actionTypeDataArray = array()) {
		$queryData = array(
			'actionMethod' => $actionMethod,
			'actionType' => $actionType,
			$actionTypeDataArray
		);
		
		return \http_build_query($queryData);
	}
	
	/**
	 * getMaasEnvironmentType
	 * 
	 * @return string
	 */
	protected static function getMaasEnvironmentType() {
		return \getenv('MAAS_CONTEXT');
	}
	
	
	/********************************************************************************************
	 *
	 *              @deprecated
	 *
	 ****************************************************************************************** */
	/**
	* checkClientBasePathRights
	* 
	* @return void
	*/
	public static function checkClientBasePathRights() {
		$locationRedirectPath = \BaseUtils::getLocationRedirect($_SESSION['client_base_path']);
		if (\strlen($locationRedirectPath) > 0) {
			\header('Location: ' . $locationRedirectPath . 'index.php');
			exit();
		}
	}
	
	/**
	 * getLocationRedirect
	 * 
	 * @param string $clientBasePath
	 * @return string
	 */
	public static function getLocationRedirect($clientBasePath) {
		$basePath = self::getBasePath();
		$beginPath = self::getFirstPath($basePath);

		$result = '..' . $beginPath . \DIRECTORY_SEPARATOR;
		if (\strlen($clientBasePath) > 0) {
			if ((string) $beginPath . $clientBasePath !== (string) $basePath) {
				if (self::checkIfDirExists($beginPath, $clientBasePath)) {
					$result = '..' . $beginPath . $clientBasePath . \DIRECTORY_SEPARATOR;
				}
			}
		}

		return $result;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              getter - Functions
	 *
	 ****************************************************************************************** */
	public static function getDebugUserId() {
		return (int) self::$debugUserId;
	}
	
	public static function getDevIpAddress() {
		return self::$devIpAddress;
	}

}