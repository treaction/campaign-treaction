<?php
/**
 * @deprecated use other utils
 */
final class CampaignAndOthersUtils {
	// TODO: move to dataManagerUtils
	static public $countriesDataArray = array(
		'DE' => 'Deutschland',
		'AT' => '&Ouml;sterreich',
		'CH' => 'Schweiz',
		'DACH' => 'DACH',
		'NL' => 'Niederlande',
		'FR' => 'Frankreich',
		'BE' => 'Belgien',
		'ES' => 'Spanien',
		'DK' => 'D&auml;nemark',
		'SE' => 'Schweden',
		'UK' => 'UK',
		'NO' => 'Norwegen',
		'FI' => 'Finnland',
		'IT' => 'Italien'
	);
	
	// TODO: move to dataManagerUtils
	static public $exportCountriesDataArray = array(
		'' => 'deutsch',
		'NL' => 'holl&auml;ndisch',
		'FR' => 'franz&ouml;sisch',
		'BE_FR' => 'belgisch -> FR',
		'BE_NL' => 'belgisch -> NL',
		'ES' => 'spanisch',
		'DK' => 'd&auml;nisch',
		'SE' => 'schwedisch',
		'UK' => 'englisch',
		'NO' => 'norwegisch',
		'FI' => 'finnisch',
		'IT' => 'italienisch'
	);
	
	// TODO: move to dataManagerUtils
	public static $genderCountryDataArray = array(
		'NL' => array(
			'm' => 'Heer',
			'w' => 'Vrouw'
		),
		'FR' => array(
			'm' => 'Monsieur',
			'w' => 'Madame'
		),
		'BE_FR' => array(
			'm' => 'Monsieur',
			'w' => 'Madame'
		),
		'BE_NL' => array(
			'm' => 'Heer',
			'w' => 'Vrouw'
		),
		'ES' => array(
			'm' => 'Se�or',
			'w' => 'Se�ora'
		),
		'DK' => array(
			'm' => 'Mr.',
			'w' => 'Woman'
		),
		'SE' => array(
			'm' => 'Herr',
			'w' => 'Fru'
		),
		'UK' => array(
			'm' => 'Mr.',
			'w' => 'Ms.'
		),
		'NO' => array(
			'm' => 'Mr.',
			'w' => 'Kvinne'
		),
		'FI' => array(
			'm' => 'Herra',
			'w' => 'Rouva'
		),
		'IT' => array(
			'm' => 'Mr.',
			'w' => 'Donna'
		)
	);
	
	static public $campaignsStatusDataArray = array(
		0 => array(
			'status' => 0,
			'label' => 'Neu',
			'sublabel' => 'Neue Kampagne',
			'campaignViewLabel' => 'Neu',
			'campaignStatusColor' => 'red',
		),
		11 => array(
			'status' => 11,
			'label' => 'Warte auf Best&auml;tigung',
			'sublabel' => 'Warte auf Buchungsbest&auml;tigung',
			'campaignViewLabel' => 'Warte auf<br />Best&auml;tigung',
			'campaignStatusColor' => '#E63C3C',
		),
		12 => array(
			'status' => 12,
			'label' => 'Warte auf Werbemittel',
			'sublabel' => 'Werbemittel wurden angefordert',
			'campaignViewLabel' => 'Warte auf<br />Werbemittel',
			'campaignStatusColor' => '#E63C3C',
		),
		1 => array(
			'status' => 1,
			'label' => 'Werbemittel erhalten',
			'sublabel' => 'Werbemittel wurden geliefert',
			'campaignViewLabel' => 'Werbemittel',
			'campaignStatusColor' => '#FF9900',
		),
		2 => array(
			'status' => 2,
			'label' => 'Optimiert',
			'sublabel' => 'Werbemittel wurden optimiert und sind versandfertig',
			'campaignViewLabel' => 'Optimiert',
			'campaignStatusColor' => '#FF9900',
		),
		3 => array(
			'status' => 3,
			'label' => 'Testmail',
			'sublabel' => 'Testmails wurden versendet und warten auf Freigabe',
			'campaignViewLabel' => 'Testmails',
			'campaignStatusColor' => '#FF9900',
		),
		4 => array(
			'status' => 4,
			'label' => 'Freigabe',
			'sublabel' => 'Freigabe wurde erteilt',
			'campaignViewLabel' => 'Freigabe',
			'campaignStatusColor' => '#5DB00D',
		),
		13 => array(
			'status' => 13,
			'label' => 'programiert',
			'sublabel' => 'programiert',
			'campaignViewLabel' => 'programiert',
			'campaignStatusColor' => '#FF9900',
		),
		5 => array(
			'status' => 5,
			'label' => 'Versendet',
			'sublabel' => 'Mailing wurde versendet',
			'campaignViewLabel' => 'versendet',
			'campaignStatusColor' => '#5DB00D',
		),
		
		// TODO: deprecated
		10 => array(
			'status' => 10,
			'label' => 'Anfrage',
			'sublabel' => 'Selektions-/Buchnungsanfrage wurde gestellt',
			'campaignViewLabel' => 'Anfrage',
			'campaignStatusColor' => '#E63C3C',
		),
	);
	
	static public $campaignsReportingStatusDataArray = array(
		23 => array(
			'status' => 23,
			'label' => 'Reportfertig',
			'sublabel' => 'Das Mailing ist abgeschlossen und kann reportet werden.',
			'campaignViewLabel' => 'Reportfertig',
			'campaignStatusColor' => '#FF9900',
		),
		20 => array(
			'status' => 20,
			'label' => 'Report intern verschickt',
			'sublabel' => 'Das Reporting wurde intern verschickt',
			'campaignViewLabel' => 'Report intern<br />verschickt',
			'campaignStatusColor' => '#FF9900',
		),
		21 => array(
			'status' => 21,
			'label' => 'Erstreporting abgeschlossen',
			'sublabel' => 'Das Erstreporting wurde an die Agentur verschickt',
			'campaignViewLabel' => 'Erstreporting<br />abgeschlossen',
			'campaignStatusColor' => '#FF9900',
		),
		22 => array(
			'status' => 22,
			'label' => 'Endreporting abgeschlossen',
			'sublabel' => 'Das finale Reporting wurde an die Agentur verschickt',
			'campaignViewLabel' => 'Endreporting<br />abgeschlossen',
			'campaignStatusColor' => '#5DB00D',
		)
	);
	
	static public $campaignsOrderStatusDataArray = array(
		30 => array(
			'status' => 30,
			'label' => 'Rechnungsstellung freigegeben',
			'sublabel' => 'Die Rechnungsstellungsmail wurde an Buchhaltung verschickt.',
			'campaignViewLabel' => 'Rechnungsstellung<br />freigegeben',
			'campaignStatusColor' => '#FF9900',
		),
		32 => array(
			'status' => 32,
			'label' => 'Rechnung gestellt',
			'sublabel' => 'Die Rechnung wurde an den Kunden verschickt.',
			'campaignViewLabel' => 'Rechnung<br />gestellt',
			'campaignStatusColor' => '#FF9900',
		),
		33 => array(
			'status' => 33,
			'label' => 'Rechnung bezahlt',
			'sublabel' => 'Die Rechung wurde bezahlt.',
			'campaignViewLabel' => 'Rechnung<br />bezahlt',
			'campaignStatusColor' => '#5DB00D',
		)
	);
	
	static public $campaignsOthersStatusDataArray = array(
		6 => array(
			'status' => 6,
			'label' => 'senden',
			'sublabel' => 'senden',
			'campaignViewLabel' => 'senden',
			'campaignStatusColor' => 'yellow',
		),
		7 => array(
			'status' => 7,
			'label' => 'abgebrochen',
			'sublabel' => 'Abgebrochen',
			'campaignViewLabel' => 'Abgebrochen',
			'campaignStatusColor' => 'yellow',
		),
		24 => array(
			'status' => 24,
			'label' => 'Report freigegeben',
			'sublabel' => 'Report freigegeben.',
			'campaignViewLabel' => 'Report freigegeben',
			'campaignStatusColor' => '#FF9900',
		),
		31 => array(
			'status' => 31,
			'label' => 'Rechnung wird erstellt',
			'sublabel' => 'Der Rechnungserhalt wurde best&auml;tigt',
			'campaignViewLabel' => 'Rechnung<br />wird erstellt',
			'campaignStatusColor' => '#FF9900',
		),
	);
	
	static public $campaignBillingsTypeDataArray = array(
		'TKP' => 'TKP',
		'Hybri' => 'Hybrid',
		'CPL' => 'CPL',
		'CPO' => 'CPO',
		'CPC' => 'CPC',
		'Festp' => 'Festpreis'
	);
	
	static public $mailtypDataArray = array(
		'm' => 'Multipart',
		'h' => 'Html',
		't' => 'Text'
	);
	
	static public $genderDataArray = array(
		1 => 'nur Frauen',
		2 => 'nur Herren',
		0 => 'beide'
	);
	
	static public $allTabFieldsDataArray = array(
		'k_id' => array(
			'label' => 'Kampagnen ID',
			'sublabel' => 'ID'
		),
		'datum' => array(
			'label' => 'Versanddatum',
			'sublabel' => 'Start'
		),
		'bearbeiter' => array(
			'label' => 'Bearbeiter',
			'sublabel' => 'Bearbeiter'
		),
		'betreff' => array(
			'label' => 'Betreff',
			'sublabel' => 'Betreff'
		),
		'absender' => array(
			'label' => 'Absendername',
			'sublabel' => 'Absendername'
		),
		'gebucht' => array(
			'label' => 'Gebucht',
			'sublabel' => 'Gebucht'
		),
		'versendet' => array(
			'label' => 'Versendet',
			'sublabel' => 'Versendet'
		),
		'beendet' => array(
			'label' => 'Versandendzeit',
			'sublabel' => 'Beendet'
		),
		'openings' => array(
			'label' => '&Ouml;ffnungen (unique)',
			'sublabel' => '&Ouml; (u)'
		),
		'openings_a' => array(
			'label' => '&Ouml;ffnungen (alle)',
			'sublabel' => '&Ouml;'
		),
		'open_quote_a' => array(
			'label' => '&Ouml;ffnungsrate (alle in %)',
			'sublabel' => '&Ouml;R'
		),
		'open_ziel' => array(
			'label' => '&Ouml;ffnungszielrate',
			'sublabel' => '&Ouml; Ziel'
		),
		'open_quote' => array(
			'label' => '&Ouml;ffnungsrate (unique in %)',
			'sublabel' => '&Ouml;R (u)'
		),
		'klicks' => array(
			'label' => 'Klicks (unique)',
			'sublabel' => 'K (u)'
		),
		'klicks_a' => array(
			'label' => 'Klicks (alle)',
			'sublabel' => 'K'
		),
		'klicks_quote' => array(
			'label' => 'Klickrate (unique in %)',
			'sublabel' => 'KR (u)'
		),
		'klicks_ziel' => array(
			'label' => 'Klickzielrate',
			'sublabel' => 'K Ziel'
		),
		'klicks_quote_a' => array(
			'label' => 'Klickrate (alle in %)',
			'sublabel' => 'KR'
		),
		'hbounces' => array(
			'label' => 'Hardbounces (abs. und %)',
			'sublabel' => 'Hardbounces'
		),
		'sbounces' => array(
			'label' => 'Softbounces (abs. und %)',
			'sublabel' => 'Softbounces'
		),
		'hsbounces' => array(
			'label' => 'Bounces gesamt (abs. und %)',
			'sublabel' => 'Bounces gesamt'
		),
		'zielgruppe' => array(
			'label' => 'Zielgruppe',
			'sublabel' => 'Zielgruppe'
		),
		'bl' => array(
			'label' => 'Blacklist',
			'sublabel' => 'Blacklist'
		),
		'mailformat' => array(
			'label' => 'Mailformat',
			'sublabel' => 'Mime'
		),
		'asp' => array(
			'label' => 'Versandsystem (ASP)',
			'sublabel' => 'ASP'
		),
		'notiz' => array(
			'label' => 'Notiz',
			'sublabel' => 'Notiz'
		),
		'ap' => array(
			'label' => 'Ansprechpartner',
			'sublabel' => 'Ansprechpartner'
		),
		'abrechnungsart' => array(
			'label' => 'Abrechnungsart',
			'sublabel' => 'Abr.rt'
		),
		'preis' => array(
			'label' => 'Preis',
			'sublabel' => 'Preis'
		),
		'preis_gesamt' => array(
			'label' => 'Einnahmen',
			'sublabel' => 'Einnahmen'
		),
               'preis_broking' => array(
			'label' => 'Einnahmen nach Broking',
			'sublabel' => 'E.Broking'
		 ),
		'avg_tkp' => array(
			'label' => 'eTKP',
			'sublabel' => 'eTKP'
		),
		'leads' => array(
			'label' => 'Leads',
			'sublabel' => 'Leads'
		),
		'leads_u' => array(
			'label' => 'Leads validiert',
			'sublabel' => 'Leads validiert'
		),
		'report' => array(
			'label' => 'Report intern',
			'sublabel' => 'Report intern'
		),
		'rechnung' => array(
			'label' => 'Rechnung',
			'sublabel' => 'Rechnung'
		),
		'erstreporting' => array(
			'label' => 'Erstreporting f&auml;llig am',
			'sublabel' => 'Erstrep. f&auml;llig am'
		),
		'endreporting' => array(
			'label' => 'Endreporting f&auml;llig am',
			'sublabel' => 'Endrep. f&auml;llig am'
		),
		'menge_ziel' => array(
			'label' => 'Vorgabe Menge',
			'sublabel' => 'Menge Ziel'
		),
		'abmelder' => array(
			'label' => 'Abmelder',
			'sublabel' => 'Abmelder'
		),
		'rechnungstellung' => array(
			'label' => 'Rechnungstellung',
			'sublabel' => 'Re-St'
		),
		'zustellreport' => array(
			'label' => 'Zustellreport',
			'sublabel' => 'Z-Rep'
		),
		'infomail' => array(
			'label' => 'Infomail - Neues Mailing',
			'sublabel' => 'Infomail'
		),
		'unit_price' => array(
			'label' => 'Preis pauschal',
			'sublabel' => 'Preis pauschal'
		),
		'anfrage' => array(
			'label' => 'Anfrage details',
			'sublabel' => 'Anfrage details'
		),
		'dsd_id' => array(
			'label' => 'Verteiler',
			'sublabel' => 'Verteiler'
		),
		'mail_id' => array(
			'label' => 'MailId',
			'sublabel' => 'MailId'
		),
		'use_campaign_start_date' => array(
			'label' => 'in Asp programiert',
			'sublabel' => 'in Asp prog.'
		),
		'actionLogInfo' => array(
			'label' => 'LogInfo',
			'sublabel' => 'LogInfo'
		),
		'campaign_click_profile_id' => array(
			'label' => 'Kampagnen-Klickprofil',
			'sublabel' => 'K.-Klickprofil'
		),
		'selection_click_profile_id' => array(
			'label' => 'Selektion-Klickprofil',
			'sublabel' => 'S.-Klickprofil'
		),
		'broking_volume' => array(
			'label' => 'Broking Volumen',
			'sublabel' => 'B.Volumen'
		),
		'broking_price' => array(
			'label' => 'Broking Preis',
			'sublabel' => 'B.Preis'
		),
               'ctr' => array(
			'label' => 'Click-Through-Rate',
			'sublabel' => 'B.Preis'
		),
               'monatsrechnung' => array(
			'label' => 'M.Rechnung',
			'sublabel' => 'M.Rechnung'
		),
		
		// TODO: deprecated, entfernen
		#'crm_order_id' => array(
		#	'label' => 'CRM - Id',
		#	'sublabel' => 'CRM - Id'
		#),
		'cs_id' => array(
			'label' => 'Selektion Land',
			'sublabel' => 'Selektion Land'
		),
	);
	
	static public $customerDataSelections = array(
		1 => 'DOI',
		2 => 'SOI',
		3 => 'DOI/SOI',
		0 => 'Unbekannt'
	);
	
	
	
	
	
	/**
	 * getClientsAccessDataArrayByLoggedUser
	 * 
	 * @param   boolean $onlyLoggedClient
	 * 
	 * @return  array
	 * @deprecated moved to ClientUtils::getClientsAccessDataArrayByLoggedUser
	 */
	static public function getClientsAccessDataArrayByLoggedUser($onlyLoggedClient = false) {
		if (\count($_SESSION['mandant_all_id_Arr']) > 0 
			&& $_SESSION['mID'] > 0 
		) {
			$tmpAllClientsIdsDataArray = \array_flip($_SESSION['mandant_all_id_Arr']);
			$accessClientsDataArray = array(
				$_SESSION['mID'] => $tmpAllClientsIdsDataArray[$_SESSION['mID']]
			);

			if ((boolean) $onlyLoggedClient !== true) {
				if (\count($_SESSION['zugang_mandant']) > 0) {
					foreach ($_SESSION['zugang_mandant'] as $zmKey) {
						if ((int) $zmKey) {
							$accessClientsDataArray[$zmKey] = $tmpAllClientsIdsDataArray[$zmKey];
						}
					}
				}

				if (!\array_key_exists($_SESSION['hauptmandantID'], $accessClientsDataArray)) {
					$accessClientsDataArray[$_SESSION['hauptmandantID']] = $tmpAllClientsIdsDataArray[$_SESSION['hauptmandantID']];
				}
				\ksort($accessClientsDataArray);
			}
			unset($tmpAllClientsIdsDataArray);
		} else {
			throw new \Exception('You don`t have permission.');
		}

		return $accessClientsDataArray;
	}
	
	
	/**
	 * getCountriesSelectItems
	 * 
	 * @param integer $countryId
	 * @return string
	 */
	static public function getCountriesSelectItems($countryId) {
		$result = '';
		foreach ($_SESSION['countrySelectionsDataArray'] as $countrySelectionEntity) {
			/* @var $countrySelectionEntity CountrySelectionEntity */
			
			$selected = '';
			if ($countrySelectionEntity->getId() === (int) $countryId) {
				$selected = 'selected';
			}
			
			$result .= \HtmlFormUtils::createOptionItem(
				array(
					'value' => $countrySelectionEntity->getId(),
					'selected' => $selected
				),
				$countrySelectionEntity->getTitle()
			);
		}
		
		return $result;
	}
	
	/**
	 * getDeliverySystemsItems
	 * 
	 * @param string $selectedItem
	 * @return string
	 */
	static public function getDeliverySystemsItems($selectedItem) {
		$result = '';
		
		foreach ($_SESSION['deliverySystemsDataArray'] as $deliverySystemItemEntity) {
			/* @var $deliverySystemItemEntity \DeliverySystemEntity */
			
			$selected = '';
			if ($deliverySystemItemEntity->getAsp_abkz() === $selectedItem) {
				$selected = 'selected';
			}
			
			$result .= \HtmlFormUtils::createOptionItem(
				array(
					'value' => $deliverySystemItemEntity->getAsp_abkz(),
					'selected' => $selected
				),
				$deliverySystemItemEntity->getAsp()
			);
		}
		
		return $result;
	}
	
	/**
	 * getDeliverySystemEntityByAspAbkz
	 * 
	 * @param string $deliverySystem
	 * @return null|DeliverySystemEntity
	 */
	static public function getDeliverySystemEntityByAspAbkz($deliverySystem) {
		$result = null;
		
		foreach ($_SESSION['deliverySystemsDataArray'] as $deliverySystemItemEntity) {
			/* @var $deliverySystemItemEntity \DeliverySystemEntity */
			
			if ($deliverySystemItemEntity->getAsp_abkz() === $deliverySystem) {
				$result = $deliverySystemItemEntity;
				break;
			}
		}
		
		return $result;
	}
	
	/**
	 * getDeliverySystemDistributorsItems
	 * 
	 * @param array $deliverySystemDistributorDataArray
	 * @param \DeliverySystemDistributorEntity $selectedDeliverySystemDistributorEntity
	 * @param boolean $disableNotSelectedClientId
	 * @param boolean $selectDefaultValue
	 * @return string
	 */
	static public function getDeliverySystemDistributorsItems(array $deliverySystemDistributorDataArray, \DeliverySystemDistributorEntity $selectedDeliverySystemDistributorEntity, $disableNotSelectedClientId = false, $selectDefaultValue = false) {
		$result = '';
		
		foreach ($deliverySystemDistributorDataArray as $key => $deliverySystemDistributorEntity) {
			/* @var $deliverySystemDistributorEntity \DeliverySystemDistributorEntity */
			
			$selected = $disabled = '';
			if ($deliverySystemDistributorEntity->getId() === $selectedDeliverySystemDistributorEntity->getId()) {
				$selected = 'selected';
			} elseif ($selectDefaultValue 
				&& $deliverySystemDistributorEntity->getSet_as_default()
			) {
				$selected = 'selected';
			}
			
			if ($disableNotSelectedClientId 
				&& $deliverySystemDistributorEntity->getM_asp_id() !== $selectedDeliverySystemDistributorEntity->getM_asp_id()
			) {
				$disabled = 'disabled';
			}
			
			if ($deliverySystemDistributorEntity->getId() === $deliverySystemDistributorEntity->getParent_id()) {
				$optgroupDataArray = \HtmlFormUtils::createOptgroupDataArray(
					self::getParentClientDeliverySystemDistributorTitle($deliverySystemDistributorEntity->getTitle())
				);
				
				$result .= \HtmlFormUtils::openSelectOptgroup(
					$key,
					$optgroupDataArray
				);
			}
		     
			$result .= \HtmlFormUtils::createOptionItem(
				array(
					'value' => $deliverySystemDistributorEntity->getId(),
					'selected' => $selected,
					'disabled' => $disabled
				),
				$deliverySystemDistributorEntity->getTitle()
			);
			
			if (\count($deliverySystemDistributorDataArray) === $key) {
				// closeSelectOptgroup
				$result .= \HtmlFormUtils::closeSelectOptgroup($optgroupDataArray);
				unset($optgroupDataArray);
			}
		}
		$result .= '<option value="" selected>- Bitte ausw&auml;hlen -</option>';
		return $result;
	}
	
        
        /**
	 * getDomainDeliverySystemItems
	 * 
	 * @param array $deliveryDomainDataArray
	 * @param \$deliveryDomainEntity $selectedDomainDeliverySystemEntity
	 * @param boolean $disableNotSelectedClientId
	 * @param boolean $selectDefaultValue
	 * @return string
	 */
	static public function getDomainDeliverySystemItems(array $deliveryDomainDataArray, \DomainDeliveryEntity $selectedDomainDeliverySystemEntity, $disableNotSelectedClientId = false, $selectDefaultValue = false) {
		$result = '';
		
		foreach ($deliveryDomainDataArray as $key => $deliveryDomainEntity) {
			/* @var $deliveryDomainEntity \DomainDeliveryEntity */
			
			$selected = $disabled = '';
			if ($deliveryDomainEntity->getDasp_id() === $selectedDomainDeliverySystemEntity->getDasp_id()) {
				$selected = 'selected';
			} 
			
		
			$result .= \HtmlFormUtils::createOptionItem(
				array(
					'value' => $deliveryDomainEntity->getDasp_id(),
					'selected' => $selected,
					'disabled' => $disabled
				),
				$deliveryDomainEntity->getDomain_name()
			);
		}

		return $result;
	}
        
        
	/**
	 * getDeliverySystemDistributorEntityByDsdId
	 * 
	 * @param integer $dsdId
	 * @return \DeliverySystemDistributorEntity
	 */
	static public function getDeliverySystemDistributorEntityByDsdId($dsdId) {
		return $_SESSION['deliverySystemDistributorEntityDataArray'][$dsdId];
	}
	
	/**
	* getParentClientDeliverySystemDistributorTitle
	* 
	* @param string $value
	* @return string
	*/
	public static function getParentClientDeliverySystemDistributorTitle($value) {
		$tmpDataArray = \explode('Gesamtverteiler', $value);
		$result = \trim($tmpDataArray[0]);
		if (\strlen($result) === 0) {
			$result = $value;
		}
		unset($tmpDataArray);
	
		return \rtrim($result, '- ');
	}
	
	
	/**
	 * getCustomerDataSelectionItems
	 * 
	 * @param integer $dataSelectionId
	 * @return string
	 */
	static public function getCustomerDataSelectionItems($dataSelectionId) {
		$result = '';
		foreach (self::$customerDataSelections as $key => $value) {
			$selected = '';
			if ($key === (int) $dataSelectionId) {
				$selected = 'selected';
			}
			
			$result .= \HtmlFormUtils::createOptionItem(
				array(
					'value' => $key,
					'selected' => $selected
				),
				$value
			);
		}
		
		return $result;
	}

}