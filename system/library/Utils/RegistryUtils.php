<?php
final class RegistryUtils {
	public static $arrayKeyDelimiter = '->';
	public static $systemKey = 'emsSuite';
	
	
	/**
	 * set
	 * 
	 * @param mixed $key
	 * @param mixed $valueData
	 * @return void
	 */
	public static function set($key, $valueData) {
		if (!isset($_SESSION[self::$systemKey][$key])) {
			$_SESSION[self::$systemKey][$key] = $valueData;
		} else {
			if (\is_array($valueData)) {
				$_SESSION[self::$systemKey][$key] += $valueData;
			}
		}
	}
	
	/**
	 * get
	 * 
	 * @param mixed $key
	 * @return mixed
	 */
	public static function get($key) {
		if ((boolean) \strpos($key, self::$arrayKeyDelimiter) !== false) {
			// mehr dimensionales array durchsuchen
			$tmpDataArray = \explode(self::$arrayKeyDelimiter, $key);
			$countTmpDataArray = \count($tmpDataArray);
			
			$dataArray = self::getAll();
			for ($i = 0; $i < $countTmpDataArray; $i++) {
				$dataArray = self::getDataByKey(
					$dataArray,
					$tmpDataArray[$i]
				);
			}
			
			return $dataArray;
		} elseif (isset($_SESSION[self::$systemKey][$key])) {
			return $_SESSION[self::$systemKey][$key];
		}
	}
	
	/**
	 * getAll
	 * 
	 * @return mixed
	 */
	public static function getAll() {
		return $_SESSION[self::$systemKey];
	}
	
	/**
	 * getArrayDataByKey
	 * 
	 * @param mixed $currentData
	 * @param mixed $key
	 * @return mixed
	 */
	protected static function getDataByKey($currentData, $key) {
		if (isset($currentData[$key])) {
			return $currentData[$key];
		}
	}
}
