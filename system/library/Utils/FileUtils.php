<?php
final class FileUtils {
	/**
	 * isDir
	 * 
	 * @param string $filename
	 * @throws \RuntimeException
	 * @return void
	 */
	public static function isFile($filename) {
		if (!\is_file($filename)) {
			throw new \RuntimeException('no valid file given: ' . \htmlspecialchars($filename), 1432800216);
		}
	}
	
	/**
	 * isReadable
	 * 
	 * @param string $filename
	 * @throws \RuntimeException
	 * @return void
	 */
	public static function isReadable($filename) {
		self::isFile($filename);
		
		if (!\is_readable($filename)) {
			throw new \RuntimeException('file is not readable: ' . \htmlspecialchars($filename), 1432800257);
		}
	}
	
	/**
	 * isWritable
	 * 
	 * @param string $filename
	 * @throws \RuntimeException
	 * @return void
	 */
	public static function isWritable($filename) {
		self::isFile($filename);
		
		if (!\is_writable($filename)) {
			throw new \RuntimeException('file/dir is not writable: ' . \htmlspecialchars($filename), 1432800671);
		}
	}
}