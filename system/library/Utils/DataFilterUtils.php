<?php
final class DataFilterUtils {
	static public $validateFilterDataArray = array(
		'email' => FILTER_SANITIZE_EMAIL,
		'encoded' => FILTER_SANITIZE_ENCODED,
		'number_float' => FILTER_SANITIZE_NUMBER_FLOAT,
		'number_int' => FILTER_SANITIZE_NUMBER_INT,
		'special_chars' => FILTER_SANITIZE_SPECIAL_CHARS,
		'full_special_chars' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
		'string' => FILTER_SANITIZE_STRING,
		'url' => FILTER_SANITIZE_URL
	);
	
	/**
	 *
	 * @deprecated use: $validateFilterDataArray
	 */
	static public $validateFilder = array(
		'email' => FILTER_SANITIZE_EMAIL,
		'encoded' => FILTER_SANITIZE_ENCODED,
		'number_float' => FILTER_SANITIZE_NUMBER_FLOAT,
		'number_int' => FILTER_SANITIZE_NUMBER_INT,
		'special_chars' => FILTER_SANITIZE_SPECIAL_CHARS,
		'full_special_chars' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
		'string' => FILTER_SANITIZE_STRING,
		'url' => FILTER_SANITIZE_URL
	);
	
	
	
	
	
	/**
	 * filterData
	 * 
	 * @param string $string
	 * @param integer $filter
	 * @param null|array  $options
	 * @return boolean|string
	 */
	static public function filterData($string, $filter = FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options = null) {
		return \filter_var(\trim($string), $filter, $options);
	}

	/**
	 * utf8DecodeAndFilterData
	 * 
	 * @param string $string
	 * @param integer $filter
	 * @param null|array $options
	 * @return boolean|string
	 */
	static public function utf8DecodeAndFilterData($string, $filter = FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options = null) {
		return self::filterData(
			\utf8_decode($string),
			$filter,
			$options
		);
	}

	/**
	 * utf8EncodeAndFilterData
	 * 
	 * @param string $string
	 * @param integer $filter
	 * @param null|array $options
	 * @return boolean|string
	 */
	static public function utf8EncodeAndFilterData($string, $filter = FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options = null) {
		return self::filterData(
			\utf8_encode($string),
			$filter,
			$options
		);
	}

	/**
	 * convertDataToFloatvalue
	 * 
	 * @param string $string
	 * @return float
	 */
	static public function convertDataToFloatvalue($string) {
		$string = \trim($string);

		if (\strstr($string, ',')) {
			// replace dots (thousand seps) with blancs 
			$string = \str_replace('.', '', $string);

			// replace ',' with '.' 
			$string = \str_replace(',', '.', $string);
		}

		// search for number that may contain '.'
		if (\preg_match('#([0-9\.]+)#', $string, $match)) {
			$result = \floatval($match[0]);
		} else {
			// take some last chances with floatval
			$result = \floatval($string);
		}

		return $result;
	}
	
	
	/**
	 * isInteger
	 * 
	 * @param mixed $input
	 * @return boolean
	 */
	static public function isInteger($input) {
		return(\ctype_digit(\strval($input)));
	}
}