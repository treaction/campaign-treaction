<?php
interface MailerWebserviceInterface {

	public function init();

	public function sendMessage($message);
}