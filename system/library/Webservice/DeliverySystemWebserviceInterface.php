<?php
interface DeliverySystemWebserviceInterface {
	
	public function login();

	public function logout();
}