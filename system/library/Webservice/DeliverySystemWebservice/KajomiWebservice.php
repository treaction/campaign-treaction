<?php
require_once(\DIR_Webservice . 'DeliverySystemWebserviceAbstract.php');

class KajomiWebservice extends \DeliverySystemWebserviceAbstract {
	protected static $mailingStatusDataArray = array(
		100 => 'Alle'
	);
	
	/**
	 * @var httpconnector
	 */
	protected $apiConnection = null;
	
	protected $curlHandle;
	
	
	
	/**
	 * login
	 * 
	 * @return void
	 */
	public function login() {
		try {
			$this->setCurlHandle(\curl_init());
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}

	/**
	 * logout
	 * 
	 * @return void
	 */
	public function logout() {
		\curl_close($this->getCurlHandle());
	}
	
	
	
	/**
	 * getMailingData
	 * 
	 * @param mixed $mailingId
	 * @return \SimpleXMLElement
	 * 
	 * @throws \DomainException
	 */
	public function getMailingData($mailingId) {
		\curl_setopt(
			$this->getCurlHandle(),
			CURLOPT_URL,
			'http://' . $this->getMLogin() . '/toolz/getdata.php?'
				. 'mod=' . $this->getApiUser()
				. '&hash=' . \md5($this->getPw() . $mailingId)
				. '&msgid=' . $mailingId
				. '&typ=mailinfo'
		);
		\curl_setopt($this->getCurlHandle(), \CURLOPT_RETURNTRANSFER, true);

		try {
			if (($curlResult = \curl_exec($this->getCurlHandle())) == false) {
				throw new \DomainException('curl Error: ' . \curl_error($this->getCurlHandle()));
			}

			return \simplexml_load_string($curlResult);
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
	
	
	/**
	 * getMailingsList
	 * @param boolean $pending
	 * @return array|mixed
	 */
	public function getMailingsList($pending = false) {
		require_once(DIR_packages . 'kajomi/api/kjmservice.php');
		
		$this->initApi();
		
		// create client instance
		$client = new kajomiclient($this->getApiConnection());
		
		return $client->getQueues($pending);
	}
	
	/**
	 * deleteMailing
	 * 
	 * @param integer $mailingId
	 * @return boolean
	 */
	public function deleteMailing($mailingId) {
		$result = FALSE;
		$cancelQueueData = $this->getPendingMailingById((int) $mailingId);
		if (!\is_null($cancelQueueData)) {
			try {
				require_once(DIR_packages . 'kajomi/api/kjmservice.php');

				$this->initApi();
				$client = new kajomiclient($this->getApiConnection());

				$client->cancelQueue($cancelQueueData);
				$result = TRUE;
			} catch (\Exception $e) {
				throw $e;
			}
		}
		
		return $result;
	}
	
	/**
	 * getMailingDataByQueuenumId
	 * 
	 * @param integer $queuenumId
	 * @return \stdClass
	 */
	public function getMailingDataByQueuenumId($queuenumId) {
		require_once(DIR_packages . 'kajomi/api/kjmservice.php');
		
		$this->initApi();
		
		// create client instance
		$client = new kajomiclient($this->getApiConnection());
		
		return $client->getQueue(\intval($queuenumId));
	}
	
	/**
	 * getMailingsListWidthMailingname
	 * 
	 * @param boolean $pending
	 * @return array|mixed
	 */
	public function getMailingsListWidthMailingname($pending = false) {
		$mailingsDataArray = $this->getMailingsList($pending);
		if (\count($mailingsDataArray) > 0) {
			foreach ($mailingsDataArray as $key => $item) {
				unset($mailingsDataArray[$key]->m);
				
				$mailingData = $this->getMailingDataByQueuenumId(\intval($item->queuenum));
				$mailingsDataArray[$key]->m = new \stdClass();
				$mailingsDataArray[$key]->m->description = $mailingData->m->description;
				unset($mailingData);
			}
		}
		
		return $mailingsDataArray;
	}
	
	
	/**
	 * initApi
	 * 
	 * @return void
	 */
	protected function initApi() {
		if (\is_null($this->getApiConnection())) {
			require_once(DIR_packages . 'kajomi/connectors/httpconnector.php');

			// create new httpconnector instance
			$this->setApiConnection(
				new httpconnector(
					$this->getSharedKey(),
					$this->getSecretKey()
				)
			);

			// initialize connection
			$this->getApiConnection()->connect(
				'85.10.252.31/srv', // api.kajomimail.de/srv
				0
			);
		}
	}
	
	/**
	 * getPendingMailingById
	 * 
	 * @param integer $mailingId
	 * @return NULL|queue
	 */
	protected function getPendingMailingById($mailingId) {
		$resultQueueData = NULL;
		$mailingsDataArray = $this->getMailingsList(TRUE);
		if (\count($mailingsDataArray) > 0) {
			foreach ($mailingsDataArray as $mailingObject) {
				if ($mailingId === (int) $mailingObject->id) {
					$resultQueueData = $mailingObject;
					break;
				}
			}
		}
		
		return $resultQueueData;
	}
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getApiConnection() {
		return $this->apiConnection;
	}
	public function setApiConnection(httpconnector $httpApiConnector) {
		$this->apiConnection = $httpApiConnector;
	}
	
	public function getCurlHandle() {
		return $this->curlHandle;
	}
	public function setCurlHandle($curlHandle) {
		$this->curlHandle = $curlHandle;
	}

}