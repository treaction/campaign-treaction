<?php
        
require_once(\DIR_Webservice . 'DeliverySystemWebserviceAbstract.php');
require_once(DIR_packages . 'Maileon'. \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');

class MaileonWebservice extends \DeliverySystemWebserviceAbstract {
	protected static $mailingStatusDataArray = array(
		1 => 'NEW',
		2 => 'SENDING',
		3 => 'DONE',
		4 => 'CANCELED',
		100 => 'Alle'
	);
	
	protected $recipientListId;
	protected $optinProcessId = 0;
	private $authentification = NULL;
	

	/**
	 * login
	 * 
	 * @return void
	 */
	public function login() {
		try {
			/**
			 * initClient
			 */
			$this->initClient();
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}

	/**
	 * logout
	 * 
	 * @return void
	 */
	public function logout() {
		$this->authentification = NULL;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              init - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * initClient
	 * 
	 * @return void
	 */
	protected function initClient() {
		try {
                    $this->authentification = array(
                        "BASE_URI" => "https://api.maileon.com/1.0",
                        "API_KEY" => $this->pw,
                        "PROXY_HOST" => "",
                        "PROXY_PORT" => "",
                        "THROW_EXCEPTION" => true,
                        "TIMEOUT" => 100, // 5 seconds
                        "DEBUG" => "false" // NEVER enable on production
                    );
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
	
	
	/********************************************************************************************
	 *
	 *              webservices - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * getWebserviceObjectByType
	 * 
	 * @param string $webserviceType
	 * @return string|objects
	 */
	public function getWebserviceObjectByType($webserviceType) {
		try {
                    $config = array(
                        "BASE_URI" => "https://api.maileon.com/1.0",
                        "API_KEY" => $this->pw,
                        "PROXY_HOST" => "",
                        "PROXY_PORT" => "",
                        "THROW_EXCEPTION" => true,
                        "TIMEOUT" => 100, // 5 seconds
                        "DEBUG" => "false" // NEVER enable on production
                    );
			switch ($webserviceType) {
				case 'MailingWebservice':
					$webservice = new \com_maileon_api_mailings_MailingsService($config); 
                                        #$webservice->setDebug(FALSE);
					break;
                                case 'ReportsWebservice':
                                        $webservice = new \com_maileon_api_reports_ReportsService($config);
                                        break;
                                case 'ContactsService':
                                        $webservice = new \com_maileon_api_contacts_ContactsService($config);
                                        break;
                                case 'ContactFiltersService':
                                        $webservice = new \com_maileon_api_contactfilters_ContactfiltersService($config);
                                        break;
				default:
					throw new \DomainException('Unknown webserviceObject: ' . $webserviceType);
			}

			return $webservice;
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	 ****************************************************************************************** */
	public function getRecipientListId() {
		return $this->recipientListId;
	}
	public function setRecipientListId($recipientListId) {
		$this->recipientListId = $recipientListId;
	}

	public function getOptinProcessId() {
		return $this->optinProcessId;
	}
	public function setOptinProcessId($optinProcessId) {
		$this->optinProcessId = $optinProcessId;
	}

	public function getAuthentification() {
		return $this->authentification;
	}

}