<?php
require_once(\DIR_Webservice . 'DeliverySystemWebserviceAbstract.php');

/**
 * Description of PromioWebservice
 *
 * @author SamiMohamedJarmoud
 */
 class PromioWebservice extends \DeliverySystemWebserviceAbstract{

	protected static $mailingStatusDataArray = array(
	        1 => 'NEW',
		2 => 'SENDING',
		3 => 'DONE',
		4 => 'CANCELED',
		100 => 'Alle'
	);
        
        /**
         * authentification
         * 
         * @var \authentification|NULL
         */
        protected $authentification = NULL;  
        
        
     	/**
	 * login
	 * 
	 * @return void
	 */
	public function login() {
		   if (\is_null($this->authentification)) {
               
                try {
                     $authentification = new \stdClass();
                     $authentification->requestId               = "1359476991234";
                     $authentification->checksum                = md5( "1359476991234". $this->pw );
                     $authentification->requestIdBlockedMinutes = 0;
                    $this->authentification = array(              
                        'clientId' => $this->mLogin,
                        'authentification' => $authentification,
                        'pw' => $this->pw,
                        'secretKey' => $this->secretKey
                    );
                     return $this->authentification;
                } catch (Exception $ex) {
                    throw $ex;
                }
            }
	}

	/**
	 * logout
	 * 
	 * @return void
	 */
	public function logout() {
		$this->authentification = NULL;
	}


	/**
	 * deleteMailing
	 * 
	 * @param integer $mailingId
	 * @return boolean
	 */
        public function deleteMailing($mailingId){
          
            $result = FALSE;
            if(\strlen($mailingId) > 0){
                try{
                   /*
                    $campaignId = new \stdClass();
                    $campaignId->id       = $mailingId;
                    $campaignId->checksum = md5(  $campaignId->id. $this->pw );
                    
                    $mySoapClient = new \SoapClient( "https://api.promio-mail.com/1.6/soap/15952.wsdl", array( 'trace' => true ));
                    $mySoapClient->deleteCampaign( $campaignId );*/
                    
                    $ch = curl_init();
                    $this->setCurl( $ch,     
                             
                           'https://api.promio-connect.com/3.0/api/message/'.$mailingId.'/delete',
                            $this->getSecretKey(),                           
                           (int)$this->getMLogin()
                    );
  
        $data['messageId'] = (int)$mailingId;
         $jsonData = json_encode( $data);
         curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonData );
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $resDelete =  curl_exec($ch);
        var_dump($resDelete);
                    $result = TRUE;
                } catch (Exception $ex) {
                    throw $ex;
                }
            }
            
           return $result; 
        }     
        
        /**
	 * getMailingData
	 * 
	 * @param mixed $mailingId
	 * @return mixed $result
	 * 
	 * @throws \DomainException
	 */
	public function getMailingData($mailingId) {
            $clientId = (int)$this->getMLogin();
            if(\strlen($mailingId) > 0){
		try {
                    $oChecksumId = new \stdClass();
                    $oChecksumId->id       = $mailingId;
                    $oChecksumId->checksum = md5(  $oChecksumId->id . $this->pw);
 
                   $oSoapClient = new \SoapClient( "https://api.promio-mail.com/1.6/soap/$clientId.wsdl");
                   $result = $oSoapClient->getCampaignResults(  $oChecksumId );   
                   return $result;
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
       }
        
      public function setCurl($ch,$endpoint, $apiKey, $sendId){   
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
           'API-Key:'. $apiKey,
           'Sender-ID:'. $sendId,
            'Content-Type: application/json'
           ));
          
          curl_setopt($ch, CURLOPT_URL, $endpoint);
     }
        
}
