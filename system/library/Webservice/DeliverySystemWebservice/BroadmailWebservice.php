<?php
require_once(\DIR_Webservice . 'DeliverySystemWebserviceAbstract.php');

class BroadmailWebservice extends \DeliverySystemWebserviceAbstract {
	protected static $mailingStatusDataArray = array(
		1 => 'NEW',
		2 => 'SENDING',
		3 => 'DONE',
		4 => 'CANCELED',
		100 => 'Alle'
	);
	
	protected $recipientListId;
	protected $optinProcessId = 0;
	private $client;
	private $session;
	
	
	
	
	
	/**
	 * login
	 * 
	 * @return void
	 */
	public function login() {
		try {
			/**
			 * initClient
			 */
			$this->initClient();

			/**
			 * initSession
			 */
			$this->initSession();
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}

	/**
	 * logout
	 * 
	 * @return void
	 */
	public function logout() {
		$this->setSession($this->getClient()->logout($this->getSession()));
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              init - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * initClient
	 * 
	 * @return void
	 */
	protected function initClient() {
		try {
			$this->setClient(
				new \SoapClient('http://api.broadmail.de/soap11/RpcSession?wsdl')
			);
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}

	/**
	 * initSession
	 * 
	 * @return void
	 */
	protected function initSession() {
		try {
			$this->setSession(
				$this->getClient()->login(
					$this->getMLogin(),
					$this->getApiUser(),
					$this->getPw()
				)
			);
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              webservices - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * getWebserviceObjectByType
	 * 
	 * @param string $webserviceType
	 * @return string|objects
	 */
	public function getWebserviceObjectByType($webserviceType) {
		try {
			switch ($webserviceType) {
				case 'BlacklistWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcBlacklist?wsdl');
					break;

				case 'MailIdWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcMailId?wsdl');
					break;

				case 'MailingReportingWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcMailingReporting?wsdl');
					break;

				case 'MailingWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcMailing?wsdl');
					break;

				case 'OptinProcessWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcOptinProcess?wsdl');
					break;

				case 'RecipientListWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcRecipientList?wsdl');
					break;

				case 'RecipientWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcRecipient?wsdl');
					break;

				case 'ResponseWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcResponse?wsdl');
					break;

				case 'UnsubscribeWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcUnsubscribe?wsdl');
					break;
				
				case 'RecipientFilterWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcRecipientFilter?wsdl');
					break;

				default:
					throw new \DomainException('Unknown webserviceObject: ' . $webserviceType);
			}

			return $webservice;
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	 ****************************************************************************************** */
	public function getRecipientListId() {
		return $this->recipientListId;
	}
	public function setRecipientListId($recipientListId) {
		$this->recipientListId = $recipientListId;
	}

	public function getOptinProcessId() {
		return $this->optinProcessId;
	}
	public function setOptinProcessId($optinProcessId) {
		$this->optinProcessId = $optinProcessId;
	}

	public function getClient() {
		return $this->client;
	}
	public function setClient($client) {
		$this->client = $client;
	}

	public function getSession() {
		return $this->session;
	}
	public function setSession($session) {
		$this->session = $session;
	}

}