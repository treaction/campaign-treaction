<?php
function fillVorlage($text, $kid) {
    // debug
    $firePhp = null;
    if (intval($_SESSION['benutzer_id']) === 24) {
        #require_once($_SERVER['DOCUMENT_ROOT'] . '/system/library/FirePHPCore/FirePHP.class.php');
        #$firePhp = FirePHP::getInstance(true);
    }
    
    $mandant = $_SESSION['mandant'];
    $mID = $_SESSION['mID'];
    
    $o_new = 0;
    $k_new = 0;
    $k_openings = 0;
    $k_klicks = 0;

    include($_SERVER['DOCUMENT_ROOT'] . '/system/db_connect.inc.php');
    $cm = new campaignManagerWebservice();
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->group('fillVorlage');
    }

    $sql = mysql_query(
        'SELECT *' . 
            ' FROM `' . $kampagne_db . '`' . 
            ' WHERE k_id = "' . $kid . '"'
        ,
        $verbindung
    );
    if ($sql) {
        $z = mysql_fetch_assoc($sql);
        mysql_free_result($sql);

        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($z, 'z');
        }
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: $sql');
        }
    }

    $k_name = $z['k_name'];
    $k_agentur_id = $z['agentur_id'];
    $k_ap_id = $z['ap_id'];

    $kArr = $cm->getKundeData(
        $kunde,
        $k_agentur_id
    );
    $k_agentur = $kArr['firma'];

    $apArr = $cm->getAP(
        $kunde_ap,
        $k_ap_id
    );
    if (count($apArr) > 0) {
        $ap_anrede = $apArr['anrede'];
        $ap_titel = $apArr['titel'];
        $ap_vorname = $apArr['vorname'];
        $ap_nachname = $apArr['nachname'];
        $ap_email = $apArr['email'];
        $ap_telefon = $apArr['telefon'];
        $ap_fax = $apArr['fax'];
        $ap_mobil = $apArr['mobil'];
        $k_ap = $ap_titel . ' ' . $ap_vorname . ' ' . $ap_nachname;
    }

    $k_datum = $z['datum'];
    $k_versandsystem = $z['versandsystem'];

    if ($k_versandsystem == 'BM') {
        $k_datum_raw = util::bmTime($k_datum);
    } else {
        $k_datum_raw = $k_datum;
    }

    $k_datum_raw_parts = explode(' ', $k_datum_raw);
    $k_versanddatum = util::datum_de(
        $k_datum_raw_parts[0],
        'date'
    );
    $k_versandzeit = $k_datum_raw_parts[1];
    $k_wochentag = util::getWochentag(
        $z['datum'],
        ''
    );

    switch ($k_versandsystem) {
        case 'BM':
            $k_versandsystem = 'Broadmail';
            break;
        
        case 'E':
            $k_versandsystem = 'Elaine';
            break;
        
        case 'BC':
            $k_versandsystem = 'Backclick';
            break;
        
        case 'K':
            $k_versandsystem = 'Kajomi';
            break;
    };

    $k_gebucht = $z['gebucht'];
    $k_zielgruppe = $z['zielgruppe'];
    $k_abrechnungsart = $z['abrechnungsart'];
    $k_preis = $z['preis'];
    $k_betreff = $z['betreff'];
    $k_absendername = $z['absendername'];
    $k_mime = $z['mailtyp'];
    
    switch ($k_mime) {
        case 't':
            $k_mime = 'text/plain';
            break;
        
        case 'h':
            $k_mime = 'text/html';
            break;
        
        case 'm':
            $k_mime = 'multipart/alternative';
            break;
    }

    $klickrate_extern = $z['klickrate_extern'];
    $openingrate_extern = $z['openingrate_extern'];

    $sql_all = mysql_query(
        'SELECT SUM(versendet) as v, SUM(openings_all) as o, SUM(klicks_all) as k, SUM(leads_u) as lu, SUM(leads) as l' . 
            ' FROM `' . $kampagne_db . '`' . 
            ' WHERE nv_id = "' . $kid . '"'
        ,
        $verbindung
    );
    if ($sql_all) {
        $za = mysql_fetch_assoc($sql_all);
        mysql_free_result($sql_all);

        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($za, 'za');
        }
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung_pep), 'sql error: $sql_all');
        }
    }

          switch ($_SESSION['mandant']) {
            case 'stellarPerformance':
            $mandant = 'fulle.media';

                break;
             case 'leadWorld':
            $mandant = 'Lead World';

                break;
             case 'taglichAngebote':
            $mandant = 'Taeglich Angebote';

                break;

            default:
            $mandant = $_SESSION['mandant'];
                break;
        }
    $versendet = $za['v'];
    $k_openings = $za['o'];
    $k_klicks = $za['k'];
    $k_leads_u = $za['lu'];
    $k_leads = $za['l'];
    
    $oRate = util::getQuote(
        $za['v'], // $k_gebucht
        $za['o']
    );
    $kRate = util::getQuote(
        $za['v'],
        $za['k']
    );
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($oRate, 'oRate');
        $firePhp->log($kRate, 'kRate');
    }

    if ($k_mime == 'text/plain') {
        $k_openings = $k_klicks;
    }

    if ($openingrate_extern != 0) {
        $o_new = (($k_openings * $openingrate_extern) / 100);
    }

    $o_new = $k_openings + $o_new;
    $oRate_ext = util::getQuote(
        $k_gebucht,
        $o_new
    );
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($o_new, 'o_new');
        $firePhp->log($oRate_ext, 'oRate_ext');
    }

    if ($klickrate_extern != 0) {
        $k_new = ($za['k'] * $klickrate_extern) / 100;
    }
    $k_new = $za['k'] + $k_new;
    $kRate_ext = util::getQuote(
        $k_gebucht,
        $k_new
    );
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($k_new, 'k_new');
        $firePhp->log($kRate_ext, 'kRate_ext');
    }

    $kRate_rel = util::getQuote(
        $za['o'],
        $za['k']
    );
    $kRate_rel_ext = util::getQuote(
        $o_new,
        $k_new
    );
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($kRate_rel, 'kRate_rel');
        $firePhp->log($kRate_rel_ext, 'kRate_rel_ext');
    }

    if ($ap_anrede == 'Herr') {
        $b_standard = 'Sehr geehrter Herr';
    } elseif ($ap_anrede == 'Frau') {
        $b_standard = 'Sehr geehrte Frau';
    } else {
        $b_standard = 'Sehr geehrte Damen und Herren';
    }

    $k_preis_gesamt = ($k_gebucht * $k_preis) / 1000;

    $text = str_replace("{b_standard}", $b_standard, $text);

    if ($k_abrechnungsart != 'TKP' && $k_abrechnungsart != 'Hybri') {
        $text = str_replace("Versandmenge", '', $text);
        #$text = str_replace("{k_gebucht}", '', $text);
    }

    $text = str_replace("{ap_anrede}", $ap_anrede, $text);
    $text = str_replace("{ap_titel}", $ap_titel, $text);
    $text = str_replace("{ap_vorname}", $ap_vorname, $text);
    $text = str_replace("{ap_nachname}", $ap_nachname, $text);
    $text = str_replace("{ap_email}", $ap_email, $text);
    $text = str_replace("{ap_telefon}", $ap_telefon, $text);
    $text = str_replace("{ap_fax}", $ap_fax, $text);
    $text = str_replace("{ap_mobil}", $ap_mobil, $text);

    $text = str_replace("{k_id}", $kid, $text);
    $text = str_replace("{k_name}", $k_name, $text);
    $text = str_replace("{k_ap}", $k_ap, $text);
    $text = str_replace("{k_agentur}", $k_agentur, $text);
    $text = str_replace("{k_versanddatum}", $k_versanddatum, $text);
    $text = str_replace("{k_versandzeit}", $k_versandzeit, $text);
    $text = str_replace("{k_wochentag}", $k_wochentag, $text);
    $text = str_replace("{k_versandsystem}", $k_versandsystem, $text);
    $text = str_replace("{k_gebucht}", util::zahl_format($k_gebucht), $text);
    $text = str_replace("{k_zielgruppe}", $k_zielgruppe, $text);
    $text = str_replace("{k_abrechnungsart}", $k_abrechnungsart, $text);
    $text = str_replace("{k_preis}", $k_preis, $text);
    $text = str_replace("{k_preis_gesamt}", util::betrag_format($k_preis_gesamt), $text);
    $text = str_replace("{k_betreff}", $k_betreff, $text);
    $text = str_replace("{k_absendername}", $k_absendername, $text);
    $text = str_replace("{k_mime}", $k_mime, $text);
    $text = str_replace("{k_openings}", util::zahl_format($k_openings), $text);
    $text = str_replace("{k_klicks}", util::zahl_format($k_klicks), $text);
    $text = str_replace("{k_versendet}", util::zahl_format($versendet), $text);
    $text = str_replace("{k_opening_rate}", $oRate, $text);
    $text = str_replace("{k_klick_rate}", $kRate, $text);
    $text = str_replace("{k_openings_ext}", util::zahl_format($o_new), $text);
    $text = str_replace("{k_klicks_ext}", util::zahl_format($k_new), $text);
    $text = str_replace("{k_opening_rate_ext}", $oRate_ext, $text);
    $text = str_replace("{k_klick_rate_ext}", $kRate_ext, $text);
    $text = str_replace("{k_klick_rate_rel}", $kRate_rel, $text);
    $text = str_replace("{k_klick_rate_rel_ext}", $kRate_rel_ext, $text);
    $text = str_replace("{k_leads_u}", util::zahl_format($k_leads_u), $text);

    $text = str_replace("{u_email}", $_SESSION['u_email'], $text);
    $text = str_replace("{u_anrede}", $_SESSION['u_anrede'], $text);
    $text = str_replace("{u_vorname}", $_SESSION['u_vorname'], $text);
    $text = str_replace("{u_nachname}", $_SESSION['u_nachname'], $text);
    
    $text = str_replace("{mandant}", $mandant, $text);
    
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->groupEnd();
    }

    return $text;
}
?>