<?php

class util {

    public static function validEmail($email) {
        $isValid = true;
        $atIndex = strrpos($email, "@");
        if (is_bool($atIndex) && !$atIndex) {
            $isValid = false;
        } else {
            $domain = substr($email, $atIndex + 1);
            $local = substr($email, 0, $atIndex);
            $localLen = strlen($local);
            $domainLen = strlen($domain);
            if ($localLen < 1 || $localLen > 64) {
                // local part length exceeded
                $isValid = false;
            } else if ($domainLen < 1 || $domainLen > 255) {
                // domain part length exceeded
                $isValid = false;
            } else if ($local[0] == '.' || $local[$localLen - 1] == '.') {
                // local part starts or ends with '.'
                $isValid = false;
            } else if (preg_match('/\\.\\./', $local)) {
                // local part has two consecutive dots
                $isValid = false;
            } else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
                // character not valid in domain part
                $isValid = false;
            } else if (preg_match('/\\.\\./', $domain)) {
                // domain part has two consecutive dots
                $isValid = false;
            } else if
            (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\", "", $local))) {
                // character not valid in local part unless 
                // local part is quoted
                if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\", "", $local))) {
                    $isValid = false;
                }
            }
            if ($isValid && !(checkdnsrr($domain, "MX") || checkdnsrr($domain, "A"))) {
                // domain not found in DNS
                $isValid = false;
            }
        }
        return $isValid;
    }

    public static function getActionName($actionID) {

        switch ($actionID) {
            case 1: $action = "Mailing angelegt";
                break;
            case 2: $action = "Mailing bearbeitet";
                break;
            case 3: $action = "NV hinzugef&uuml;gt";
                break;
            case 4: $action = "Status ge&auml;ndert";
                break;
            case 5: $action = "Reporting bearbeitet";
                break;
            case 6: $action = "Rechnungsstellungsmail verschickt";
                break;
            case 7: $action = "Zustellreport verschickt";
                break;
            case 8: $action = "Infomail 'Neues Mailing'";
                break;
            case 9: $action = "Mailing synchronisiert";
                break;
        }

        return $action;
    }

    public static function getAbteilungName($abt) {

        switch ($abt) {
            case 'g': $abteilung = "Gesch&auml;ftsf&uuml;hrung";
                break;
            case 'v': $abteilung = "Vertrieb";
                break;
            case 't': $abteilung = "Technik";
                break;
            case 'b': $abteilung = "Buchhaltung";
                break;
            case 's': $abteilung = "Support";
                break;
        }

        return $abteilung;
    }

    public static function fillTestmail($text) {
        switch ($_SESSION['mandant']) {
            case 'stellarPerformance':
            $mandant = 'fulle.media';

                break;
             case 'leadWorld':
            $mandant = 'Lead World';

                break;
             case 'taglichAngebote':
            $mandant = 'Taeglich Angebote';

                break;

            default:
                $mandant = $_SESSION['mandant'];
                break;
        }

        $text = str_replace("{b_standard}", "Sehr geehrte Frau", $text);

        $text = str_replace("{ap_anrede}", "Frau", $text);
        $text = str_replace("{ap_titel}", "Dr.", $text);
        $text = str_replace("{ap_vorname}", "Maxi", $text);
        $text = str_replace("{ap_nachname}", "Musterfrau", $text);
        $text = str_replace("{ap_email}", "muster@agentur.de", $text);
        $text = str_replace("{ap_telefon}", "01234-123456", $text);
        $text = str_replace("{ap_fax}", "01234-123457", $text);
        $text = str_replace("{ap_mobil}", "0151-123456", $text);

        $text = str_replace("{k_name}", "Newsletter [Testmail]", $text);
        $text = str_replace("{k_ap}", "Maxi Musterfrau", $text);
        $text = str_replace("{k_agentur}", "Musterfirma GmbH", $text);
        $text = str_replace("{k_versanddatum}", "01.01.2010", $text);
        $text = str_replace("{k_versandzeit}", "9:30", $text);
        $text = str_replace("{k_wochentag}", "Montag", $text);
        $text = str_replace("{k_versandsystem}", "Broadmail", $text);
        $text = str_replace("{k_gebucht}", "100.000", $text);
        $text = str_replace("{k_zielgruppe}", "DE, Frauen, 25+", $text);
        $text = str_replace("{k_abrechnungsart}", "TKP", $text);
        $text = str_replace("{k_preis}", "25", $text);
        $text = str_replace("{k_betreff}", "Ihr Gutschein liegt bereit", $text);
        $text = str_replace("{k_absendername}", "Testmail", $text);
        $text = str_replace("{k_mime}", "multipart/alternative", $text);
        $text = str_replace("{k_openings}", "12.345", $text);
        $text = str_replace("{k_klicks}", "1.234", $text);
        $text = str_replace("{k_versendet}", "125.000", $text);
        $text = str_replace("{k_opening_rate}", "12.34", $text);
        $text = str_replace("{k_klick_rate}", "1.23", $text);
        $text = str_replace("{k_openings_ext}", "14.567", $text);
        $text = str_replace("{k_klicks_ext}", "1.567", $text);
        $text = str_replace("{k_opening_rate_ext}", "14.56", $text);
        $text = str_replace("{k_klick_rate_ext}", "1.45", $text);
        $text = str_replace("{k_klick_rate_rel}", "11.33", $text);
        $text = str_replace("{k_klick_rate_rel_ext}", "13.55", $text);

        $text = str_replace("{u_email}", $_SESSION['u_email'], $text);
        $text = str_replace("{u_anrede}", $_SESSION['u_anrede'], $text);
        $text = str_replace("{u_vorname}", $_SESSION['u_vorname'], $text);
        $text = str_replace("{u_nachname}", $_SESSION['u_nachname'], $text);
         $text = str_replace("{mandant}", $mandant, $text);

        return $text;
    }

    public static function bmTime($date) {
        $dateParts = explode(' ', $date);
        $datePart = $dateParts[0];
        $timePart = $dateParts[1];
        $timeParts = explode(':', $timePart);
        $timePart = ($timeParts[0] + 0) . ":" . $timeParts[1];
        return $datePart . " " . $timePart;
    }

    public static function getWochentag($date, $type) {
        $dateParts = explode(' ', $date);
        $datePart = $dateParts[0];
        $dateParts = explode('-', $date);

        $d = mktime(0, 0, 0, $dateParts[1], $dateParts[2], $dateParts[0]);
        $wochentag = date("w", $d);
        $wochentage = array("Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag");
        if ($type == 'short') {
            $wochentage = array("So.", "Mo.", "Di.", "Mi.", "Do.", "Fr.", "Sa.");
        }

        $wochentagname = $wochentage[$wochentag];

        return $wochentagname;
    }

    public static function getTageByMonat($monat, $jahr) {

        $tage = date("t", mktime(0, 0, 0, $monat, 1, $jahr));

        return $tage;
    }

    public static function getMonat($date) {
        $dateParts = explode(' ', $date);
        $datePart = $dateParts[0];

        $datePart_t = explode('-', $datePart);
        $monat_nr = intval($datePart_t[1]);
        $monat_nr--;

        $monatArr = array("Januar", "Februar", "M&auml;rz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember");
        $monatname = $monatArr[$monat_nr] . " " . $datePart_t[0];

        return $monatname;
    }

    public static function getMonate() {

        $monatArr = array("1" => "Januar", "2" => "Februar", "3" => "M&auml;rz", "4" => "April", "5" => "Mai", "6" => "Juni", "7" => "Juli", "8" => "August", "9" => "September", "10" => "Oktober", "11" => "November", "12" => "Dezember");

        return $monatArr;
    }

    public static function zahl_format($zahl) {
        $zahl = number_format($zahl, 0, 0, '.');
        return $zahl;
    }

    public static function betrag_format($zahl) {
        $zahl = number_format($zahl, 2, ',', '.');
        return $zahl;
    }

    public static function cutString($text, $length) {

        if (strlen($text) > $length) {
            $cutText = substr($text, 0, $length);
            $cutText = $cutText . "...";
        } else {
            $cutText = $text;
        }

        return $cutText;
    }

    public static function datum_de($date_time, $typ) {
        $h = "";
        $m = "";

        $date_1 = explode(' ', $date_time);
        $datum = explode('-', $date_1[0]);
        $jahr = $datum[0];
        $monat = $datum[1];
        $tag = $datum[2];
        if (isset($date_1[1])) {
            $zeit = $date_1[1];
            $zeit_teile = explode(':', $date_1[1]);
            $h = $zeit_teile[0];
            $m = $zeit_teile[1];
        } else {
            $h = "00";
            $m = "00";
        }


        $zeit = $h . ":" . $m;
        $de = $tag . "." . $monat . "." . $jahr;

        if ($typ == 'date') {
            $dat_zeit = $de;
        } else {
            $dat_zeit = $de . " - " . $zeit;
        }

        return $dat_zeit;
    }

    public static function zeit_de($date_time) {
        $date_1 = explode(' ', $date_time);
        $datum = explode('-', $date_1[0]);
        $jahr = $datum[0];
        $monat = $datum[1];
        $tag = $datum[2];
        $zeit = $date_1[1];
        $zeit_teile = explode(':', $date_1[1]);
        $h = $zeit_teile[0];
        $m = $zeit_teile[1];
        $zeit = $h . ":" . $m;

        return $zeit;
    }

    public static function datum_vs($date_time) {
        $date_1 = explode(' ', $date_time);
        $datum = explode('-', $date_1[0]);
        $jahr = $datum[0];
        $monat = $datum[1];
        $tag = $datum[2];
        $de = $tag . "." . $monat . ".";

        return $de;
    }

    public static function getQuote($brutto, $netto) {
        $q = 0;
        if ($brutto > 0)
            $q = round((($netto / $brutto) * 100), 2);
        return $q;
    }

    public static function passedMin($date) {
        $date_1 = explode(' ', $date);
        $datum = explode('-', $date_1[0]);
        $jahr = $datum[0];
        $monat = $datum[1];
        $tag = $datum[2];

        $zeit = $date_1[1];
        $zeit_teile = explode(':', $date_1[1]);
        $h = $zeit_teile[0];
        $m = $zeit_teile[1];

        $heute = time();
        $v_datum = mktime($h, $m, 0, $monat, $tag, $jahr);

        $passedMin = floor(($heute - $v_datum) / 60);

        return $passedMin;
    }

    public static function getQuoteColor($value, $target) {
        if ($value < $target) {
            $q = "<span style='color:red'>" . $value . "%</span>";
        } else {
            $q = "<span style='color:green'>" . $value . "%</span>";
        }

        return $q;
    }

    public static function getPassedH($date) {
        $date_1 = explode(' ', $date);
        $datum = explode('-', $date_1[0]);
        $jahr = $datum[0];
        $monat = $datum[1];
        $tag = $datum[2];

        $zeit = $date_1[1];
        $zeit_teile = explode(':', $date_1[1]);
        $h = $zeit_teile[0];
        $m = $zeit_teile[1];

        $heute = time();
        $v_datum = mktime($h, $m, 0, $monat, $tag, $jahr);

        $passedHours = floor(($heute - $v_datum) / 3600);
        return $passedHours;
    }

    public static function getMonatName($m) {

        switch ($m) {
            case 1: return "Jan.";
                break;
            case 2: return "Feb.";
                break;
            case 3: return "M&auml;rz";
                break;
            case 4: return "Apr.";
                break;
            case 5: return "Mai";
                break;
            case 6: return "Juni";
                break;
            case 7: return "Juli";
                break;
            case 8: return "Aug.";
                break;
            case 9: return "Sep.";
                break;
            case 10: return "Okt.";
                break;
            case 11: return "Nov.";
                break;
            case 12: return "Dez.";
                break;
        }
    }

    public static function getCampaignStatusName($status) {

        switch ($status) {
            case 0: $status_bez = "Neu";
                $status_k = "red";
                break;
            case 1: $status_bez = "Werbemittel";
                $status_k = "#FF9900";
                break;
            case 2: $status_bez = "Optimiert";
                $status_k = "#FF9900";
                break;
            case 3: $status_bez = "Testmails";
                $status_k = "#FF9900";
                break;
            case 4: $status_bez = "Freigabe";
                $status_k = "#7D960B;";
                break;
            case 5: $status_bez = "versendet";
                $status_k = "#666666";
                break;
            case 6: $status_bez = "senden...";
                $status_k = "#9900FF";
                $bg_color = "yellow";
                break;
            case 7: $status_bez = "abgebr.";
                $status_k = "#E63C3C";
                break;
            case 10: $status_bez = "Anfrage";
                $status_k = "#666666";
                break;
            case 11: $status_bez = "Warte auf<br />Best&auml;tigung";
                $status_k = "#E63C3C";
                break;
            case 12: $status_bez = "Warte auf<br />Werbemittel";
                $status_k = "#E63C3C";
                break;
            case 20: $status_bez = "Warte auf<br />Report Freigabe";
                $status_k = "#FF9900";
                break;
            case 21: $status_bez = "Erstreporting";
                $status_k = "#FF9900";
                break;
            case 22: $status_bez = "Endreporting";
                $status_k = "#666666";
                break;
            case 23: $status_bez = "Reportfertig";
                $status_k = "#FF9900";
                break;
            case 24: $status_bez = "Report freigegeben";
                $status_k = "#7D960B";
                break;
            case 30: $status_bez = "Rechnungsstellung<br />freigegeben";
                $status_k = "#666666";
                break;
            case 31: $status_bez = "Rechnung<br />best&auml;tigt";
                $status_k = "#666666";
                $re_bez = "Bestätigt";
                break;
            case 32: $status_bez = "Rechnung<br />gestellt";
                $status_k = "#FF9900";
                $re_bez = "<img src='img/Tango/22/actions/dialog-apply.png' />";
                break;
            case 33: $status_bez = "Rechnung<br />bezahlt";
                $status_k = "#666666";
                break;
        }

        return '<span style=\'color:' . $status_k . ';\'>' . $status_bez . '</span>';
    }

    public static function getStatusName($status) {

        switch ($status) {
            case 0: $status_bez = "Neu";
                break;
            case 1: $status_bez = "Werbemittel";
                break;
            case 2: $status_bez = "Optimiert";
                break;
            case 3: $status_bez = "Testmails";
                break;
            case 4: $status_bez = "Freigabe";
                break;
            case 5: $status_bez = "versendet";
                break;
            case 6: $status_bez = "senden...";
                break;
            case 7: $status_bez = "abgebr.";
                break;
            case 10: $status_bez = "Anfrage";
                break;
            case 11: $status_bez = "Warte auf Bestätigung";
                break;
            case 12: $status_bez = "Warte auf Werbemittel";
                break;
            case 20: $status_bez = "Warte auf Report Freigabe";
                break;
            case 21: $status_bez = "Erstreporting";
                break;
            case 22: $status_bez = "Endreporting";
                break;
            case 23: $status_bez = "Reportfertig";
                break;
            case 24: $status_bez = "Report freigegeben";
                break;
            case 30: $status_bez = "Rechnungsstellung freigegeben";
                break;
            case 31: $status_bez = "Rechnung bestätigt";
                break;
            case 32: $status_bez = "Rechnung gestellt";
                break;
            case 33: $status_bez = "Rechnung bezahlt";
                break;
        }

        return $status_bez;
    }

    public static function getASPName($asp) {

        switch ($asp) {
            case 'BM': return "Broadmail";
                break;
            case 'BC': return "Backclick";
                break;
            case 'E': return "Elaine";
                break;
            case 'K': return "Kajomi";
                break;
            case 'MS': return "MailSolution";
                break;
        }
    }

    public static function getMimeName($mime) {

        switch ($mime) {
            case 'm': return "Multipart";
                break;
            case 'h': return "Html";
                break;
            case 't': return "Text";
                break;
        }
    }

    public static function isDateToday($date) {

        $today = date("Y-m-d");
        $dateArr = explode(" ", $date);

        if ($today == $dateArr[0]) {
            return true;
        } else {
            return false;
        }
    }

    public static function firstMonday($year = null) {
        if (is_null($year))
            $year = date("Y");
        $date = mktime(0, 0, 0, 1, 4, $year);
        return mktime(0, 0, 0, 1, date("d", $date) + (date("w", $date) == 0 ? -6 : (1 - date("w", $date))), $year);
    }

    public static function getKW($kw, $year = null) {
        if (is_null($year))
            $year = date("Y");
        return util::firstMonday($year) + 604800 * ($kw - 1);
    }

    #DATA MANAGER

    public static function getDataQuoteColor($value) {

        if ($value < 50) {
            $q = "<span style='color:red'>" . $value . "%</span>";
        } elseif ($value > 90) {
            $q = "<span style='color:green'>" . $value . "%</span>";
        } else {
            $q = $value . "%";
        }

        return $q;
    }

}

?>