<?php
require_once(DIR_Manager . 'PdoDbManager.php');

// Entity
require_once(DIR_Entity . 'DeliverySystemEntity.php');
require_once(DIR_Entity . 'ClientDeliveryEntity.php');
require_once(DIR_Entity . 'UserEntity.php');
require_once(DIR_Entity . 'ClientEntity.php');
require_once(DIR_Entity . 'DeliverySystemDistributorEntity.php');
require_once(DIR_Entity . 'ClientAndDeliverySystemEntity.php');
require_once(DIR_Entity . 'CountrySelectionEntity.php');
require_once(DIR_Entity . 'StaticCountryEntity.php');
require_once(DIR_Entity . 'EmsActionLogEntity.php');
require_once(DIR_Entity . 'DeliverySystemDistributorWidthClientDeliverySystemEntity.php');
require_once(DIR_Entity . 'MandantEntity.php');
require_once(DIR_Entity . 'DomainDeliveryEntity.php');
require_once(DIR_Entity . 'EspMappingEntity.php');
require_once(DIR_Entity . 'HeaderFooterEntity.php');

class ClientManager extends \PdoDbManager {
	protected $userTable = 'benutzer';
	protected $deliverySystemTable = 'versandsystem';
	protected $clientTable = 'mandant';
	protected $clientDeliveryTable = 'mandant_asp';
	protected $deliverySystemDistributorTable = 'delivery_system_distributor';
	protected $countrySelectionTable = 'country_selection';
	protected $staticCountriesTable = 'static_countries';
	protected $emsActionLogTable = 'ems_action_log';
	protected $clickProfilesTable = 'click_profiles';
	protected $clientClickProfilesTable = 'client_click_profiles';
        protected $mandantTable = 'mandant_parameters';
        protected $domainDeliveryTable = 'domain_asp';
        protected $espMappingTable = 'esp_Mapping_links';
        protected $headerFooterTable = 'header_footer';

	protected $deliverySystemFetchClass = 'DeliverySystemEntity';
	protected $deliverySystemDistributorFetchClass = 'DeliverySystemDistributorEntity';
	protected $clientDeliveryFetchClass = 'ClientDeliveryEntity';
	protected $clientFetchClass = 'ClientEntity';
	protected $userFetchClass = 'UserEntity';
	protected $countrySelectionFetchClass = 'CountrySelectionEntity';
	protected $staticCountryFetchClass = 'StaticCountryEntity';
	protected $clickProfileFetchClass = 'ClickProfileEntity';
	protected $clientClickProfileFetchClass = 'ClientClickProfileEntity';
        protected $mandantFetchClass = 'MandantEntity';
        protected $domainDeliveryFetchClass = 'DomainDeliveryEntity';
        protected $espMappingFetchClass = 'EspMappingEntity';
        protected $headerFooterFetchClass = 'HeaderFooterEntity';
	
	/**
	 * @deprecated use ems_action_log
	 */
	protected $actionLogTable = 'action_log';
	
	
	
	
	
	/**
	 * init
	 * 
	 * @return  void
	 */
	public function init() {
		$this->setHost('localhost');
		$this->setDbName('peppmt'); // TestSystem
		$this->setDbUser('dbo49323410');
		$this->setDbPassword('5C4i7E9v');

		parent::init();
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              versandsystem - functions
	 *
	 ****************************************************************************************** */
        
        /**
         * getMandantParameter
         * @return AdminMandantEntity
         */
        public function getMandantParameter(){
           
                   $qry = 'SELECT * FROM '. $this->getMandantTables() .
                           ' ORDER BY id DESC LIMIT 1 ';
                   
                   $stmt = $this->getDbh()->prepare($qry);
                   $stmt->execute();

                   $stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getMandantFetchClass());
                   
                   $result = $stmt->fetch();
                   return $result;
        }
	/**
	 * getClientDeliverySystemDataItemByAspAbk
	 * 
	 * @param integer $clientId
	 * @param string $aspAbkz
	 * @return false|DeliverySystemEntity
	 */
	public function getClientDeliverySystemDataItemByAspAbk($clientId, $aspAbkz) {
		$qry = 'SELECT ' . $this->getDeliverySystemTable() . '.*'
			. ' FROM ' . $this->getDeliverySystemTable()
				. ' JOIN ' . $this->getClientDeliveryTable() . ' ON ' . $this->getDeliverySystemTable() . '.`id` = `mandant_asp`.`asp_id`'
			. ' WHERE 1 = 1'
				. ' AND ' . $this->getClientDeliveryTable() . '.`active` = 1'
				. ' AND ' . $this->getClientDeliveryTable() . '.`m_id` = :mID' 
				. ' AND ' . $this->getDeliverySystemTable() . '.`asp_abkz` = :aspAbkz'
		;

		$debugDataArray = array(
			'm_id' => $clientId,
			'asp_abkz' => $aspAbkz
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':mID', $clientId, \PDO::PARAM_INT);
			$stmt->bindParam(':aspAbkz', $aspAbkz, \PDO::PARAM_STR);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getDeliverySystemFetchClass());

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $stmt->fetch();
	}
	
	/**
	 * getDeliverySystemDataItemsByClientId
	 * 
	 * @param integer $clientId
	 * @return false|array
	 */
	public function getDeliverySystemDataItemsByClientId($clientId) {
        #var_dump($clientId);
		$qry = 'SELECT ' . $this->getDeliverySystemTable() . '.*'
			. ' FROM ' . $this->getDeliverySystemTable()
				. ' JOIN ' . $this->getClientDeliveryTable() . ' ON ' . $this->getDeliverySystemTable() . '.`id` = `mandant_asp`.`asp_id`'
			. ' WHERE 1 = 1'
				. ' AND ' . $this->getClientDeliveryTable() . '.`active` = 1'
                . ' AND ' . $this->getDeliverySystemTable() . '.`active` = 1'
				. ' AND ' . $this->getClientDeliveryTable() . '.`m_id` = :mID'
		;

		$debugDataArray = array(
			'm_id' => $clientId
		);
		$resultDataArray = array();
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':mID', $clientId, \PDO::PARAM_INT);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getDeliverySystemFetchClass());
			while (($row = $stmt->fetch()) !== false) {
				/* @var $row \DeliverySystemEntity */
				$resultDataArray[$row->getId()] = $row;
			}

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $resultDataArray;
	}
	
	/**
	 * getDeliverySystemEntityById
	 * 
	 * @param integer $id
	 * @return false\DeliverySystemEntity
	 */
	public function getDeliverySystemEntityById($id) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'id' => array(
						'sql' => '`id`',
						'value' => $id,
						'comparison' => 'integerEqual'
					),
				),
			),
			$this->getDeliverySystemTable(),
			''
		);
		
		return $this->fetchStmt(
			$stmt,
			$this->getDeliverySystemFetchClass()
		);
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              versandsystem, delivery_system_distributor - functions
	 *
	 ****************************************************************************************** */
	/**
	 * getDeliverySystemDistributorDataById
	 * 
	 * @param integer $id
	 * @return false|\DeliverySystemDistributorEntity
	 */
	public function getDeliverySystemDistributorDataById($id) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'id' => array(
						'sql' => '`id`',
						'value' => $id,
						'comparison' => 'integerEqual'
					),
					'active' => array(
						'sql' => '`active`',
						'value' => 1,
						'comparison' => 'integerEqual'
					),
				),
			),
			$this->getDeliverySystemDistributorTable(),
			''
		);
		
		return $this->fetchStmt(
			$stmt,
			$this->getDeliverySystemDistributorFetchClass()
		);
	}
	
	/**
	 * getDeliverySystemDistributorWidthDeliverySystemEntityById
	 * 
	 * @param integer $id
	 * @return \DeliverySystemDistributorWidthClientDeliverySystemEntity
	 */
	public function getDeliverySystemDistributorWidthDeliverySystemEntityById($id) {
		// TODO: mandanten id checken!!!
		$tmpDeliverySystemDistributorEntityClass = $this->getDeliverySystemDistributorFetchClass();
		$this->setDeliverySystemDistributorFetchClass('DeliverySystemDistributorWidthClientDeliverySystemEntity');
		
		$deliverySystemDistributorEntity = $this->getDeliverySystemDistributorDataById($id);
		/* @var $clientDeliveryEntity \DeliverySystemDistributorWidthClientDeliverySystemEntity */
		$deliverySystemDistributorEntity->setClientDeliveryEntity(
			$this->getClientDeliveryWidthDeliverySystemEntityByMaspId($deliverySystemDistributorEntity->getM_asp_id())
		);
		
		$this->setDeliverySystemDistributorFetchClass($tmpDeliverySystemDistributorEntityClass);
		
		return $deliverySystemDistributorEntity;
	}

	/**
	 * getDeliverySystemDistributorsDataItemsByClientId
	 * 
	 * @param integer $clientId
	 * @param array $underClientIds
	 * @return array
	 */
	public function getDeliverySystemDistributorsDataItemsByClientId($clientId, $underClientIds = array()) {
		$qry = 'SELECT ' . $this->getDeliverySystemDistributorTable() . '.*'
			. ' FROM ' . $this->getClientDeliveryTable()
				. ' JOIN ' . $this->getDeliverySystemDistributorTable() . ' ON ' . $this->getClientDeliveryTable() . '.masp_id = ' . $this->getDeliverySystemDistributorTable() . '.m_asp_id'
			. ' WHERE 1 = 1'
				. ' AND ' . $this->getClientDeliveryTable() . '.`active` = 1' 
				. ' AND ' . $this->getDeliverySystemDistributorTable() . '.`active` = 1' 
				. $this->getAddWhereForUnderClientsIds($underClientIds)
		;

		$resultDataArray = array();
		$debugDataArray = array(
			'clientId' => $clientId
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			// updateStmpForUnderClientsIds
			$this->updateStmpForUnderClientsIds(
				$clientId,
				$underClientIds,
				$stmt
			);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getDeliverySystemDistributorFetchClass());

			while (($row = $stmt->fetch()) !== false) {
				/* @var $row \DeliverySystemDistributorEntity */
				$resultDataArray[$row->getId()] = $row;
			}

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $resultDataArray;
	}
	
	/**
	 * getDeliverySystemDistributorsAndClientDeliveryDataItemsByClientId
	 * 
	 * @param integer $clientId
	 * @param array $underClientIds
	 * @return array
	 */
	public function getDeliverySystemDistributorsAndClientDeliveryDataItemsByClientId($clientId, $underClientIds = array()) {
		$tmpDeliverySystemDistributorEntityClass = $this->getDeliverySystemDistributorFetchClass();
		$this->setDeliverySystemDistributorFetchClass('DeliverySystemDistributorWidthClientDeliverySystemEntity');
		
		$resultDataArray = $this->getDeliverySystemDistributorsDataItemsByClientId(
			$clientId,
			$underClientIds
		);
		if (\count($resultDataArray) > 0) {
			foreach ($resultDataArray as $key => $deliverySystemDistributorEntity) {
				/* @var $deliverySystemDistributorEntity \DeliverySystemDistributorWidthClientDeliverySystemEntity */
				$resultDataArray[$key]->setClientDeliveryEntity(
					$this->getClientDeliveryWidthDeliverySystemEntityByMaspId(
						$deliverySystemDistributorEntity->getM_asp_id()
					)
				);
			}
		}
		
		$this->setDeliverySystemDistributorFetchClass($tmpDeliverySystemDistributorEntityClass);
		
		return $resultDataArray;
	}
	
	/**
	 * DeliverySystemDistributorEntity
	 * 
	 * @param integer $clientId
	 * @param string $aspAbkz
	 * @return false|DeliverySystemDistributorEntity
	 * 
	 * @deprecated use: getDeliverySystemDistributorsDataItemsByClientIdAndDeliverySystemId
	 */
	public function getDeliverySystemDistributorsDataItemsByClientIdAndDeliverySystemAsp($clientId, $aspAbkz, $underClientIds = array()) {
		$qry = 'SELECT ' . $this->getDeliverySystemDistributorTable() . '.*'
			. ' FROM ' . $this->getDeliverySystemDistributorTable() 
				. ' JOIN ' . $this->getClientDeliveryTable() . ' ON ' . $this->getDeliverySystemDistributorTable() . '.m_asp_id = ' . $this->getClientDeliveryTable() . '.masp_id'
				. ' JOIN ' . $this->getDeliverySystemTable() . ' ON ' . $this->getClientDeliveryTable() . '.`asp_id` = ' . $this->getDeliverySystemTable() . '.`id`'
			. ' WHERE 1 = 1'
				. ' AND ' . $this->getClientDeliveryTable() . '.`active` = 1' 
				. ' AND ' . $this->getDeliverySystemTable() . '.`asp_abkz` = :aspAbkz'
				. ' AND ' . $this->getDeliverySystemDistributorTable() . '.`active` = 1' 
				. $this->getAddWhereForUnderClientsIds($underClientIds)
		;
		
		$debugDataArray = array(
			'mId' => $clientId,
			'aspAbkz' => $aspAbkz,
			'underClientsIds' => $underClientIds
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':aspAbkz', $aspAbkz, \PDO::PARAM_STR);
			// updateStmpForUnderClientsIds
			$this->updateStmpForUnderClientsIds(
				$clientId,
				$underClientIds,
				$stmt
			);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getDeliverySystemDistributorFetchClass());

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $stmt->fetchAll();
	}
	
        /**
	 * getDeliverySystemDistributorsDataItemsByClientIdAndDeliverySystemId
	 * 
	 * @param integer $clientId
	 * @param integer $deliverySystemId
	 * @param array $underClientIds
	 * @return false|array
	 */
	public function getDeliverySystemDistributorsDataItemsByClientIdAndDeliverySystemId($clientId, $deliverySystemId, $underClientIds = array()) {
		$qry = 'SELECT ' . $this->getDeliverySystemDistributorTable() . '.*'
			. ' FROM ' . $this->getDeliverySystemDistributorTable() 
				. ' JOIN ' . $this->getClientDeliveryTable() . ' ON ' . $this->getDeliverySystemDistributorTable() . '.m_asp_id = ' . $this->getClientDeliveryTable() . '.masp_id'
				. ' JOIN ' . $this->getDeliverySystemTable() . ' ON ' . $this->getClientDeliveryTable() . '.`asp_id` = ' . $this->getDeliverySystemTable() . '.`id`'
			. ' WHERE 1 = 1'
				. ' AND ' . $this->getClientDeliveryTable() . '.`active` = 1' 
				. ' AND ' . $this->getDeliverySystemTable() . '.`id` = :deliverySystemId'
				. ' AND ' . $this->getDeliverySystemDistributorTable() . '.`active` = 1' 
				. $this->getAddWhereForUnderClientsIds($underClientIds)
		;
		
		$debugDataArray = array(
			'mId' => $clientId,
			'deliverySystemId' => $deliverySystemId,
			'underClientsIds' => $underClientIds
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':deliverySystemId', $deliverySystemId, \PDO::PARAM_INT);
			// updateStmpForUnderClientsIds
			$this->updateStmpForUnderClientsIds(
				$clientId,
				$underClientIds,
				$stmt
			);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getDeliverySystemDistributorFetchClass());

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $stmt->fetchAll();
	}
        
	/**
	 * getDomainDeliverySystemsDataArrayByClientId
	 * 
	 * @param integer $clientId
	 * @param integer $deliverySystemId
	 * @param array $underClientIds
	 * @return false|array
	 */
	public function getDomainDeliverySystemsDataArrayByClientId($clientId, $deliverySystemmDistributorId, $underClientIds = array()) {
		$qry = 'SELECT *'
			. ' FROM ' . $this->getDomainDeliveryTable() 				
			. ' WHERE 1 = 1'
			. ' AND `active` = 1'
			. ' AND `dsd_id` = ' . $deliverySystemmDistributorId
			. ' AND `m_id` = ' . $clientId
                        
		;
		
		$debugDataArray = array(
			'mId' => $clientId,
			'deliverySystemId' => $deliverySystemmDistributorId,
			'underClientsIds' => $underClientIds
		);
		try {
			$stmt = $this->getDbh()->prepare($qry);                       
			#$stmt->bindParam(':deliverySystemmDistributorId', $deliverySystemmDistributorId, \PDO::PARAM_INT);
                       # $stmt->bindParam(':clientId', $clientId, \PDO::PARAM_INT);
                      
			// updateStmpForUnderClientsIds
			$this->updateStmpForUnderClientsIds(
				$clientId,
				$underClientIds,
				$stmt
			);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getDomainDeliveryFetchClass());

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $stmt->fetchAll();
	}
	
	/**
	 * getDeliverySystemDistributorsAndClientDeliveryDataItemsByClientIdAndDeliverySystemId
	 * 
	 * @param integer $clientId
	 * @param integer $deliverySystemId
	 * @param array $underClientIds
	 * @return false|array
	 */
	public function getDeliverySystemDistributorsAndClientDeliveryDataItemsByClientIdAndDeliverySystemId($clientId, $deliverySystemId, $underClientIds = array()) {
		$tmpDeliverySystemDistributorEntityClass = $this->getDeliverySystemDistributorFetchClass();
		$this->setDeliverySystemDistributorFetchClass('DeliverySystemDistributorWidthClientDeliverySystemEntity');
		
		$resultDataArray = $this->getDeliverySystemDistributorsDataItemsByClientIdAndDeliverySystemId(
			$clientId,
			$deliverySystemId,
			$underClientIds
		);
		if (\count($resultDataArray) > 0) {
			foreach ($resultDataArray as $key => $deliverySystemDistributorEntity) {
				/* @var $deliverySystemDistributorEntity \DeliverySystemDistributorWidthClientDeliverySystemEntity */
				$resultDataArray[$key]->setClientDeliveryEntity(
					$this->getClientDeliveryWidthDeliverySystemEntityByMaspId(
						$deliverySystemDistributorEntity->getM_asp_id()
					)
				);
			}
		}
		
		$this->setDeliverySystemDistributorFetchClass($tmpDeliverySystemDistributorEntityClass);
		
		return $resultDataArray;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              mandant, mandant_asp - functions
	 *
	 ****************************************************************************************** */
	/**
	 * getClientDeliveryDataByClientIdAndDeliverySystemAsp
	 * 
	 * @param integer $clientId
	 * @param string $aspAbkz
	 * @return false|\ClientAndDeliverySystemEntity
	 * 
	 * @deprecated, use: getClientDeliveryDataByIdAndDeliverySystemDistributorId
	 */
	public function getClientDeliveryDataByClientIdAndDeliverySystemAsp($clientId, $aspAbkz) {
		$qry = 'SELECT ' . $this->getClientDeliveryTable() . '.*'
				. ', ' . $this->getDeliverySystemTable() . '.asp' . ', ' . $this->getDeliverySystemTable() . '.asp_abkz'
			. ' FROM ' . $this->getClientDeliveryTable()
				. ' JOIN ' . $this->getDeliverySystemTable() . ' ON ' . $this->getClientDeliveryTable() . '.`asp_id` = ' . $this->getDeliverySystemTable() . '.`id`'
			. ' WHERE 1 = 1'
				. ' AND ' . $this->getClientDeliveryTable() . '.`active` = 1'
				. ' AND ' . $this->getClientDeliveryTable() . '.`m_id` = :mId'
				. ' AND ' . $this->getDeliverySystemTable() . '.`asp_abkz` = :aspAbkz'
		;

		$debugDataArray = array(
			'm_id' => $clientId,
			'asp_abkz' => $aspAbkz
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':mId', $clientId, \PDO::PARAM_INT);
			$stmt->bindParam(':aspAbkz', $aspAbkz, \PDO::PARAM_STR);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, 'ClientAndDeliverySystemEntity');

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $stmt->fetch();
	}
	
	/**
	 * getClientDeliveryDataByIdAndDeliverySystemDistributorId
	 * 
	 * @param integer $clientId
	 * @param integer $deliverySystemDistributorId
	 * @return false|\ClientDeliveryEntity
	 */
	public function getClientDeliveryDataByIdAndDeliverySystemDistributorId($clientId, $deliverySystemDistributorId, $underClientIds = array()) {
		$qry = 'SELECT ' . $this->getClientDeliveryTable() . '.*'
			. ' FROM ' . $this->getClientDeliveryTable()
				. ' JOIN ' . $this->getDeliverySystemDistributorTable() . ' ON ' . $this->getClientDeliveryTable() . '.`masp_id` = ' . $this->getDeliverySystemDistributorTable() . '.`m_asp_id`'
			. ' WHERE 1 = 1'
				. ' AND ' . $this->getClientDeliveryTable() . '.`active` = 1'
				. ' AND ' . $this->getDeliverySystemDistributorTable() . '.`id` = :deliverySystemDistributorId' 
				. $this->getAddWhereForUnderClientsIds($underClientIds)
		;

		$debugDataArray = array(
			'm_id' => $clientId,
			'deliverySystemDistributorId' => $deliverySystemDistributorId
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':deliverySystemDistributorId', $deliverySystemDistributorId, \PDO::PARAM_INT);
			// updateStmpForUnderClientsIds
			$this->updateStmpForUnderClientsIds(
				$clientId,
				$underClientIds,
				$stmt
			);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getClientDeliveryFetchClass());

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $stmt->fetch();
	}

	/**
	 * getClientDataById
	 * 
	 * @param integer $id
	 * @return false|\ClientEntity
	 */
	public function getClientDataById($id) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'id' => array(
						'sql' => '`id`',
						'value' => $id,
						'comparison' => 'integerEqual'
					),
				),
			),
			$this->getClientTable(),
			''
		);
		
		return $this->fetchStmt(
			$stmt,
			$this->getClientFetchClass()
		);
	}

	/**
	 * getClientDataByShortcut
	 * 
	 * @param string $clientShortcut
	 * @return false|\ClientEntity
	 */
	public function getClientDataByShortcut($clientShortcut) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'abkz' => array(
						'sql' => '`abkz`',
						'value' => $clientShortcut,
						'comparison' => \PdoDbManager::$fieldEqual
					),
				),
			),
			$this->getClientTable(),
			''
		);
		
		return $this->fetchStmt(
			$stmt,
			$this->getClientFetchClass()
		);
	}
	
	/**
	 * getParentClientsDataItems
	 * 
	 * @param integer $clientId
	 * @return false|array
	 */
	public function getParentClientsDataItems($clientId) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'parentId' => array(
						'sql' => '`parent_id`',
						'value' => $clientId,
						'comparison' => 'integerEqual'
					),
				),
			),
			$this->getClientTable(),
			''
		);
		
		return $this->fetchAllStmt(
			$stmt,
			$this->getClientFetchClass()
		);
	}
	
	/**
	 * getUnderClientIdsByClientId
	 * 
	 * @param integer $clientId
	 * @return array
	 */
	public function getUnderClientIdsByClientId($clientId) {
		// getParentClientsDataItems
		$parentClients = $this->getParentClientsDataItems($clientId);
		
		$resultDataArray = array();
		if (\count($parentClients) > 0) {
			$resultDataArray[] = $clientId;
			
			foreach ($parentClients as $clientEntity) {
				/* @var $clientEntity \ClientEntity */
				$resultDataArray[] = $clientEntity->getId();
			}
		}
		
		return $resultDataArray;
	}
	
	/**
	 * getClientDeliveryEntityByMaspId
	 * 
	 * @param integer $maspId
	 * @return false|\ClientDeliveryEntity
	 */
	public function getClientDeliveryEntityByMaspId($maspId) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'id' => array(
						'sql' => '`masp_id`',
						'value' => $maspId,
						'comparison' => 'integerEqual'
					),
					'status' => array(
						'sql' => '`active`',
						'value' => 1,
						'comparison' => 'integerEqual'
					),
				),
			),
			$this->getClientDeliveryTable(),
			''
		);
		
		return $this->fetchStmt(
			$stmt,
			$this->getClientDeliveryFetchClass()
		);
	}
	
	/**
	 * getClientDeliveryWidthDeliverySystemEntityByMaspId
	 * 
	 * @param integer $maspId
	 * @return \ClientDeliverySystemWidthDeliverySystemEntity
	 */
	public function getClientDeliveryWidthDeliverySystemEntityByMaspId($maspId) {
		$tmpClientDeliveryEntityClass = $this->getClientDeliveryFetchClass();
		$this->setClientDeliveryFetchClass('ClientDeliverySystemWidthDeliverySystemEntity');
		
		$clientDeliveryEntity = $this->getClientDeliveryEntityByMaspId($maspId);
		/* @var $clientDeliveryEntity \ClientDeliverySystemWidthDeliverySystemEntity */
		$clientDeliveryEntity->setDeliverySystem($this->getDeliverySystemEntityById($clientDeliveryEntity->getAsp_id()));
		
		$this->setClientDeliveryFetchClass($tmpClientDeliveryEntityClass);
		
		return $clientDeliveryEntity;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              benutzer - functions
	 *
	 ****************************************************************************************** */
	/**
	 * getUserDataItemById
	 * 
	 * @param integer $kid
	 * @return false|\UserEntity
	 */
	public function getUserDataItemById($id) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'benutzerId' => array(
						'sql' => '`benutzer_id`',
						'value' => $id,
						'comparison' => 'integerEqual'
					),
				),
			),
			$this->getUserTable(),
			''
		);

		return $this->fetchStmt(
			$stmt,
			$this->getUserFetchClass(),
			false
		);
	}

	/**
	 * getActiveUsersDataItemsByClientId
	 * 
	 * @param integer $mId
	 * @param boolean $useUnderMandant
	 * @return array
	 */
	public function getActiveUsersDataItemsByClientId($mId, $useUnderMandant = false) {
		$qry = 'SELECT *'
			. ' FROM ' . $this->getUserTable()
			. ' WHERE `active` = 1'
				. ' AND `mid` = :mid'
			. ' ORDER BY vorname ASC, nachname ASC'
		;

		$resultDataArray = array();
		$debugDataArray = array(
			'mid' => $mId,
			'useUnderMandant' => $useUnderMandant
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':mid', $mId, \PDO::PARAM_INT);
			$stmt->execute();

			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getUserFetchClass());

			while (($row = $stmt->fetch()) !== false) {
				$row->setPw('');

				/* @var $row \UserEntity */
				$resultDataArray[$row->getBenutzer_id()] = $row;
			}

			if ((boolean) $useUnderMandant === true) {
				$tmpDataArray = $this->getUnderMandantUsersDataItemsByMandant($mId);
				if (\is_array($tmpDataArray) 
					&& \count($tmpDataArray) > 0
				) {
					$resultDataArray += $tmpDataArray;
				}
				unset($tmpDataArray);
			}

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $resultDataArray;
	}

	/**
	 * getActiveUsersDataItemsByDepartment
	 * 
	 * @param integer $mId
	 * @param boolean $useUnderMandant
	 * @return array
	 */
	public function getActiveUsersDataItemsByDepartment($abteilung) {
		$qry = 'SELECT *'
			. ' FROM ' . $this->getUserTable()
			. ' WHERE `active` = 1'
				. ' AND `abteilung` = :abteilung'
			. ' ORDER BY vorname ASC, nachname ASC'
		;

		$resultDataArray = array();
		$debugDataArray = array(
			'abteilung' => $abteilung,
			'useUnderMandant' => $useUnderMandant
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':abteilung', $abteilung, \PDO::PARAM_STR);
			$stmt->execute();

			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getUserFetchClass());

			while (($row = $stmt->fetch()) !== false) {
				$row->setPw('');

				/* @var $row \UserEntity */
				$resultDataArray[$row->getBenutzer_id()] = $row;
			}

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $resultDataArray;
	}
        
	/**
	 * getDepartmentsUsersDataItemsByMandantId
	 * 
	 * @param integer $mId
	 * @param string $departments
	 * @param boolean $useUnderMandant
	 * @return false|array
	 */
	public function getDepartmentsUsersDataItemsByMandantId($mId, $departments, $useUnderMandant = false) {
		$departmentsDataArray = \explode(',', $departments);

		$qry = 'SELECT *'
			. ' FROM ' . $this->getUserTable()
			. ' WHERE `active` = 1'
				. ' AND `mid` = :mid'
				. ' AND `abteilung` IN (' . $this->createPdoInString($departmentsDataArray) . ')'
			. ' ORDER BY vorname ASC, nachname ASC'
		;

		$debugDataArray = array(
			'mid' => $mId,
			'departmentsDataArray' => $departmentsDataArray,
			'departmentsIn' => $this->createPdoInString($departmentsDataArray),
			'useUnderMandant' => $useUnderMandant
		);
		
		try {
			$resultDataArray = array();
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':mid', $mId, \PDO::PARAM_INT);

			foreach ($departmentsDataArray as $item) {
				$stmt->bindValue(':' . $item, $item, \PDO::PARAM_STR);
			}

			$stmt->execute();

			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getUserFetchClass());

			while (($row = $stmt->fetch()) !== false) {
				/* @var $row \UserEntity */
				$resultDataArray[$row->getBenutzer_id()] = $row;
			}

			if ((boolean) $useUnderMandant === true) {
				$tmpDataArray = $this->getUnderMandantUsersDataItemsByMandant(
					$mId,
					$departments
				);
				if (\is_array($tmpDataArray) 
					&& \count($tmpDataArray) > 0
				) {
					$resultDataArray += $tmpDataArray;
				}
				unset($tmpDataArray);
			}

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $resultDataArray;
	}

	/**
	 * getUnderMandantUsersDataItemsByMandant
	 * 
	 * @param integer $mId
	 * @param string|null $departments
	 * @return array
	 */
	protected function getUnderMandantUsersDataItemsByMandant($mId, $departments = null) {
		$qry = 'SELECT *'
			. ' FROM ' . $this->getUserTable()
			. ' WHERE `active` = 1'
		;

		$departmentsDataArray = array();
		if (!\is_null($departments) 
			&& \strlen($departments) > 0
		) {
			$departmentsDataArray = \explode(',', $departments);

			$qry .= ' AND `abteilung` IN (' . $this->createPdoInString($departmentsDataArray) . ')';
		}

		$resultDataArray = array();
		$debugDataArray = array(
			'mid' => $mId,
			'departmentsDataArray' => $departmentsDataArray,
			'departmentsIn' => $this->createPdoInString($departmentsDataArray)
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);

			if (\count($departmentsDataArray) > 0) {
				foreach ($departmentsDataArray as $item) {
					$stmt->bindValue(':' . $item, $item, \PDO::PARAM_STR);
				}
			}

			$stmt->execute();

			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getUserFetchClass());

			while (($row = $stmt->fetch()) !== false) {
				/* @var $row \UserEntity */
				$zugangMandantDataArray = \explode(',', $row->getZugang_mandant());

				if (\in_array((int) $mId, $zugangMandantDataArray)) {
					$row->setPw('');

					$resultDataArray[$row->getBenutzer_id()] = $row;
					$resultDataArray[$row->getBenutzer_id()]->setZugang_mandant('');
				}
			}

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $resultDataArray;
	}

	/**
	 * getTabFieldsDataArrayByIdUser
	 * 
	 * @param integer $userId
	 * @return array
	 * 
	 * @throws \DomainException
	 */
	public function getTabFieldsDataArrayByIdUser($userId) {
		$userEntity = $this->getUserDataItemById($userId);
		if ($userEntity instanceof \UserEntity) {
			if (\strlen($userEntity->getTab_config()) > 0) {
				$explodeDataArray = \explode(',', $userEntity->getTab_config());

				$tabFieldsDataArray = array();
				for ($x = 0; $x < \count($explodeDataArray); $x++) {
					$tabFieldsDataArray[] = $explodeDataArray[$x];
				}
			} else {
				$tabFieldsDataArray = array('datum', 'asp', 'gebucht', 'versendet', 'openings', 'open_quote', 'klicks', 'klicks_quote', 'zielgruppe');
			}
		} else {
			throw new \DomainException('no UserEntity');
		}

		return $tabFieldsDataArray;
	}

	/**
	 * updateUserDataRowById
	 * 
	 * @param integer $userId
	 * @param array $dataArray
	 * @return boolean
	 */
	public function updateUserDataRowById($userId, array $dataArray) {
		$qry = 'UPDATE ' . $this->getUserTable()
			. ' SET ' . $this->addUpdateQueryFields($dataArray)
			. ' WHERE `benutzer_id` = :benutzerId'
		;
		
		$dataArray['benutzerId'] = array(
			'value' => (int) $userId,
			'dataType' => \PDO::PARAM_INT
		);

		return $this->processUpdateItemByDataArray(
			$qry,
			$dataArray
		);
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              action_log - functions
	 *
	 ****************************************************************************************** */
	/**
	 * addEmsActionLogItem
	 * 
	 * @param \EmsActionLogEntity $emsActionLogEntity
	 * @return boolean|integer
	 */
	public function addEmsActionLogItem(\EmsActionLogEntity $emsActionLogEntity) {
		// createEmsActionLogDataFromEntity
		$dataArray = $this->createEmsActionLogDataFromEntity($emsActionLogEntity);
		
		$qry = 'INSERT INTO ' . $this->getEmsActionLogTable()
			. $this->processInsertQueryFieldsAndValues($dataArray)
		;

		return $this->processInsertItemByDataArray(
			$qry,
			$dataArray
		);
	}
	
	
	/**
	 * createEmsActionLogDataFromEntity
	 * 
	 * @param \EmsActionLogEntity $emsActionLogEntity
	 * @return array
	 */
	protected function createEmsActionLogDataFromEntity(\EmsActionLogEntity $emsActionLogEntity) {
		return array(
			'client_id' => array(
				'value' => $emsActionLogEntity->getClient_id(),
				'dataType' => \PDO::PARAM_INT
			),
			'user_id' => array(
				'value' => $emsActionLogEntity->getUser_id(),
				'dataType' => \PDO::PARAM_INT
			),
			'action_id' => array(
				'value' => $emsActionLogEntity->getAction_id(),
				'dataType' => \PDO::PARAM_INT
			),
			'record_id' => array(
				'value' => $emsActionLogEntity->getRecord_id(),
				'dataType' => \PDO::PARAM_INT
			),
			'record_table' => array(
				'value' => $emsActionLogEntity->getRecord_table(),
				'dataType' => \PDO::PARAM_STR
			),
			'crdate' => array(
				'value' => \date('Y-m-d H:i:s'),
				'dataType' => \PDO::PARAM_STR
			),
			'log_data' => array(
				'value' => (
					\count($emsActionLogEntity->getLog_data()) > 0 
						? \serialize($emsActionLogEntity->getLog_data())
						: ''
					),
				'dataType' => \PDO::PARAM_STR
			)
		);
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              country_selection, static_countries - functions
	 *
	 ****************************************************************************************** */
	/**
	 * getActiveCountrySelectionDataItems
	 * 
	 * @return array
	 */
	public function getActiveCountrySelectionDataItems() {
		$qry = 'SELECT *'
			. ' FROM ' . $this->getCountrySelectionTable()
			. ' WHERE 1 = 1'
				. ' AND `active` = 1'
		;

		$resultDataArray = array();
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->execute();

			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getCountrySelectionFetchClass());

			while (($row = $stmt->fetch()) !== false) {
				/* @var $row \CountrySelectionEntity */
				$resultDataArray[$row->getId()] = $row;
			}

			$this->debugPdoStatement($stmt);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry
			);
		}

		return $resultDataArray;
	}

	/**
	 * getCountrySelectionDataById
	 * 
	 * @param integer $id
	 * @return false|\CountrySelectionEntity
	 */
	public function getCountrySelectionDataById($id) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'id' => array(
						'sql' => '`id`',
						'value' => $id,
						'comparison' => 'integerEqual'
					),
				),
			),
			$this->getCountrySelectionTable(),
			''
		);
		
		return $this->fetchStmt(
			$stmt,
			$this->getCountrySelectionFetchClass()
		);
	}
	
	/**
	 * getAllStaticCountryDataItems
	 * 
	 * @return array
	 */
	public function getAllStaticCountryDataItems() {
		$qry = 'SELECT *'
			. ' FROM ' . $this->getStaticCountriesTable()
			. ' WHERE 1 = 1'
		;

		$resultDataArray = array();
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->execute();

			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getStaticCountryFetchClass());

			while (($row = $stmt->fetch()) !== false) {
				/* @var $row \StaticCountryEntity */
				$resultDataArray[$row->getUid()] = $row;
			}

			$this->debugPdoStatement($stmt);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry
			);
		}

		return $resultDataArray;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              click_profile - functions
	 *
	 ****************************************************************************************** */
	/**
	 * getActiveClientClickProfilesDataItems
	 * 
	 * @param integer $clientId
	 * @return array
	 */
	public function getActiveClientClickProfilesDataItems($clientId) {
		$qry = 'SELECT *' 
			. ' FROM ' . $this->getClientClickProfilesTable() 
			. ' WHERE 1 = 1'
				. ' AND ' . $this->getClientClickProfilesTable() . '.`active` = 1'
				. ' AND ' . $this->getClientClickProfilesTable() . '.`client_id` = :clientId'
		;

		$resultDataArray = array();
		$debugDataArray = array(
			'clientId' => $clientId,
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindValue(':clientId', $clientId, \PDO::PARAM_INT);
			$stmt->execute();
			
			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, 'ClientClickProfileWidthClickProfileEntity');
			
			while (($row = $stmt->fetch()) !== false) {
				/* @var $row \ClientClickProfileWidthClickProfileEntity */
				$resultDataArray[$row->getUid()] = $row;
				
				$clickProfileEntity = $this->getClientClickProfileDataByUid($row->getUid());
				if (!($clickProfileEntity instanceof \ClickProfileEntity)) {
					$clickProfileEntity = new \ClickProfileEntity();
				}
				$resultDataArray[$row->getUid()]->setClickProfile($clickProfileEntity);
				unset($clickProfileEntity);
			}

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry
			);
		}

		return $resultDataArray;
	}
	
        
        /**
	 * getActiveClientClickProfilesArray
	 * 
	 * @param integer $clientId
	 * @return array
	 */
	public function getActiveClientClickProfilesArray($clientId) {
		$qry = 'SELECT *' 
			. ' FROM ' . $this->getClientClickProfilesTable() 
			. ' WHERE 1 = 1'
				. ' AND ' . $this->getClientClickProfilesTable() . '.`active` = 1'
				. ' AND ' . $this->getClientClickProfilesTable() . '.`client_id` = :clientId'
		;

		$resultDataArray = array();
		$debugDataArray = array(
			'clientId' => $clientId,
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindValue(':clientId', $clientId, \PDO::PARAM_INT);
			$stmt->execute();
			
			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, 'ClientClickProfileWidthClickProfileEntity');
			
			while (($row = $stmt->fetch()) !== false) {
				/* @var $row \ClientClickProfileWidthClickProfileEntity */
							
				$clickProfileEntity = $this->getClientClickProfileDataByUid($row->getUid());
				if (!($clickProfileEntity instanceof \ClickProfileEntity)) {
					$clickProfileEntity = new \ClickProfileEntity();
				}
                                
                                $resultDataArray[$row->getUid()] = $clickProfileEntity->getTitle().'('.$clickProfileEntity->getShortcut().')';
				unset($clickProfileEntity);
			}

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry
			);
		}

		return $resultDataArray;
	}
	
	/**
	 * getAllClickProfilesDataItems
	 * 
	 * @return false|array
	 */
	public function getAllClickProfilesDataItems() {
		$stmt = $this->processDataArrayByQueryParts(
			array(),
			$this->getClickProfilesTable(),
			''
		);
		
		return $this->fetchAllStmt(
			$stmt,
			$this->getClickProfileFetchClass()
		);
	}
	
	/**
	 * getClientClickProfileDataByUid
	 * 
	 * @param integer $clickProfileId
	 * @return false|ClickProfileEntity
	 */
	public function getClientClickProfileDataByUid($clickProfileId) {
		$qry = 'SELECT ' . $this->getClickProfilesTable() . '.*' 
			. ' FROM ' . $this->getClickProfilesTable() 
				. ' JOIN ' . $this->getClientClickProfilesTable() . ' ON ' . $this->getClickProfilesTable() . '.id = ' . $this->getClientClickProfilesTable() . '.click_profile_id'
			. ' WHERE 1 = 1'
				. ' AND ' . $this->getClientClickProfilesTable() . '.`uid` = :clickProfileId'
		;
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindValue(':clickProfileId', $clickProfileId, \PDO::PARAM_INT);
			$stmt->execute();
			
			return $this->fetchStmt(
				$stmt,
				$this->getClickProfileFetchClass()
			);
		} catch (\Exception $e){
			$this->debugPdoException(
				$e,
				$qry
			);
		}
	}
	
	
	
	/**
	 * getAddWhereForUnderClientsIds
	 * 
	 * @param array $underClientIds
	 * @return string
	 */
	private function getAddWhereForUnderClientsIds($underClientIds) {
		$result = ' AND ' . $this->getClientDeliveryTable() . '.`m_id` = :mId';
		if (\count($underClientIds) > 0) {
			$result = ' AND ' . $this->getClientDeliveryTable() . '.`m_id` IN (' . $this->createPdoInString($underClientIds) . ')';
		}
		
		return $result;
	}
	
	/**
	 * updateStmpForUnderClientsIds
	 * 
	 * @param integer $mId
	 * @param array $underClientIds
	 * @param \PDOStatement $stmt
	 * @return void
	 */
	private function updateStmpForUnderClientsIds($mId, $underClientIds, \PDOStatement &$stmt) {
		if (\count($underClientIds) > 0) {
			foreach ($underClientIds as $underClientId) {
				$stmt->bindValue(':' . $underClientId, $underClientId, \PDO::PARAM_INT);
			}
		} else {
			$stmt->bindParam(':mId', $mId, \PDO::PARAM_INT);
		}
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	 ****************************************************************************************** */
	public function getUserTable() {
		return $this->userTable;
	}
	public function setUserTable($userTable) {
		$this->userTable = $userTable;
	}

	public function getDeliverySystemTable() {
		return $this->deliverySystemTable;
	}
	public function setDeliverySystemTable($deliverySystemTable) {
		$this->deliverySystemTable = $deliverySystemTable;
	}

	public function getClientTable() {
		return $this->clientTable;
	}
	public function setClientTable($clientTable) {
		$this->clientTable = $clientTable;
	}

	public function getClientDeliveryTable() {
		return $this->clientDeliveryTable;
	}
	public function setClientDeliveryTable($clientDeliveryTable) {
		$this->clientDeliveryTable = $clientDeliveryTable;
	}

	public function getDomainDeliveryTable() {
		return $this->domainDeliveryTable;
	}
	public function setDomainDeliveryTable($domainDeliveryTable) {
		$this->domainDeliveryTable = $domainDeliveryTable;
	}

        public function getDeliverySystemDistributorTable() {
		return $this->deliverySystemDistributorTable;
	}
	public function setDeliverySystemDistributorTable($deliverySystemDistributorTable) {
		$this->deliverySystemDistributorTable = $deliverySystemDistributorTable;
	}
        
	public function getCountrySelectionTable() {
		return $this->countrySelectionTable;
	}
	public function setCountrySelectionTable($countrySelectionTable) {
		$this->countrySelectionTable = $countrySelectionTable;
	}

	public function getStaticCountriesTable() {
		return $this->staticCountriesTable;
	}
	public function setStaticCountriesTable($staticCountriesTable) {
		$this->staticCountriesTable = $staticCountriesTable;
	}
	
	public function getEmsActionLogTable() {
		return $this->emsActionLogTable;
	}
	public function setEmsActionLogTable($emsActionLogTable) {
		$this->emsActionLogTable = $emsActionLogTable;
	}
	
	public function getClickProfilesTable() {
		return $this->clickProfilesTable;
	}
	public function setClickProfilesTable($clickProfilesTable) {
		$this->clickProfilesTable = $clickProfilesTable;
	}

	public function getClientClickProfilesTable() {
		return $this->clientClickProfilesTable;
	}
	public function setClientClickProfilesTable($clientClickProfilesTable) {
		$this->clientClickProfilesTable = $clientClickProfilesTable;
	}
        public function getMandantTables() {
		return $this->mandantTable;
	}
	public function setMandantTable($mandantTable) {
		$this->mandantTable = $mandantTable;
	}
        public function getEspMappingTable() {
          return $this->espMappingTable;
        }
        public function setEspMappingTable($espMappingTable) {
           $this->espMappingTable = $espMappingTable;
        }
        function getHeaderFooterTable() {
            return $this->headerFooterTable;
        }
        function setHeaderFooterTable($headerFooterTable) {
            $this->headerFooterTable = $headerFooterTable;
        }


	
	
	public function getDomainDeliveryFetchClass() {return $this->domainDeliveryFetchClass;
	}
	public function setDomainDeliveryFetchClass($domainDeliveryFetchClass) {
		$this->domainDeliveryFetchClass = $domainDeliveryFetchClass;
	}
        
        public function getDeliverySystemFetchClass() {
		return $this->deliverySystemFetchClass;
	}
	public function setDeliverySystemFetchClass($deliverySystemFetchClass) {
		$this->deliverySystemFetchClass = $deliverySystemFetchClass;
	}
		
	public function getDeliverySystemDistributorFetchClass() {
		return $this->deliverySystemDistributorFetchClass;
	}
	public function setDeliverySystemDistributorFetchClass($deliverySystemDistributorFetchClass) {
		$this->deliverySystemDistributorFetchClass = $deliverySystemDistributorFetchClass;
	}
	
	public function getClientDeliveryFetchClass() {
		return $this->clientDeliveryFetchClass;
	}
	public function setClientDeliveryFetchClass($clientDeliveryFetchClass) {
		$this->clientDeliveryFetchClass = $clientDeliveryFetchClass;
	}
	
	public function getClientFetchClass() {
		return $this->clientFetchClass;
	}
	public function setClientFetchClass($clientFetchClass) {
		$this->clientFetchClass = $clientFetchClass;
	}
	
	public function getUserFetchClass() {
		return $this->userFetchClass;
	}
	public function setUserFetchClass($userFetchClass) {
		$this->userFetchClass = $userFetchClass;
	}

	public function getCountrySelectionFetchClass() {
		return $this->countrySelectionFetchClass;
	}
	public function setCountrySelectionFetchClass($countrySelectionFetchClass) {
		$this->countrySelectionFetchClass = $countrySelectionFetchClass;
	}

	public function getStaticCountryFetchClass() {
		return $this->staticCountryFetchClass;
	}
	public function setStaticCountryFetchClass($staticCountryFetchClass) {
		$this->staticCountryFetchClass = $staticCountryFetchClass;
	}
	
	public function getClickProfileFetchClass() {
		return $this->clickProfileFetchClass;
	}
	public function setClickProfileFetchClass($clickProfileFetchClass) {
		$this->clickProfileFetchClass = $clickProfileFetchClass;
	}

	public function getClientClickProfileFetchClass() {
		return $this->clientClickProfileFetchClass;
	}
	public function setClientClickProfileFetchClass($clientClickProfileFetchClass) {
		$this->clientClickProfileFetchClass = $clientClickProfileFetchClass;
	}
        
        public function getMandantFetchClass() {
		return $this->mandantFetchClass;
	}
	public function setMandantFetchClass($mandantFetchClass) {
		$this->mandantFetchClass = $mandantFetchClass;
	}

	public function getEspMappingFetchClass() {
               return $this->espMappingFetchClass;
        }
        public function setEspMappingFetchClass($espMappingFetchClass) {
        $this->espMappingFetchClass = $espMappingFetchClass;
       }
        function getHeaderFooterFetchClass() {
            return $this->headerFooterFetchClass;
        }
        function setHeaderFooterFetchClass($headerFooterFetchClass) {
            $this->headerFooterFetchClass = $headerFooterFetchClass;
        }
	
	
	
	/**
	 * TODO: to be remove
	 * @deprecated
	 */
	public function getActionLogTable() {
		return $this->actionLogTable;
	}
	public function setActionLogTable($actionLogTable) {
		$this->actionLogTable = $actionLogTable;
	}

}