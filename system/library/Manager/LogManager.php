<?php
require_once(DIR_Manager . 'PdoDbManager.php');

// Entity
require_once(DIR_Entity . 'ActionLogEntity.php');

class LogManager extends \PdoDbManager {
	protected $logTable;
	
	
	
	
	
	/**
	 * addLogItem
	 * 
	 * @param array $dataArray
	 * @return boolean|integer
	 * 
	 * @deprecated moved into: CampaignManager->addLogItem
	 */
	public function addLogItem(array $dataArray) {
		$qry = 'INSERT INTO ' . $this->getLogTable()
			. $this->processInsertQueryFieldsAndValues($dataArray)
		;

		return $this->processInsertItemByDataArray(
			$qry,
			$dataArray
		);
	}

	/**
	 * deleteAllCampaignsLogByKid
	 * 
	 * @param integer $kId
	 * @return  boolean
	 * 
	 * @throws PDOException
	 * 
	 * @deprecated moved into: CampaignManager->deleteAllCampaignsLogByKid
	 */
	public function deleteAllCampaignsLogByKid($kId) {
		$qry = 'DELETE '
			. ' FROM ' . $this->getLogTable()
			. ' WHERE `kid` = :kid'
		;

		$debugDataArray = array(
			'kId' => $kId
		);
		
		try {
			$this->getDbh()->beginTransaction();

			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindValue(':kid', $kId, \PDO::PARAM_INT);
			$stmt->execute();

			$this->getDbh()->commit();

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);

			$result = true;
		} catch (\PDOException $e) {
			$result = false;

			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $result;
	}

	/**
	 * getLastLogByAction
	 * 
	 * @param integer $actionId
	 * @param integer $campaignId
	 * @return null|ActionLogEntity
	 * 
	 * @throws PDOException|DomainException
	 * 
	 * @deprecated moved into: CampaignManager->getLastLogByAction
	 */
	public function getLastLogByAction($actionId, $campaignId) {
		$qry = 'SELECT * '
			. ' FROM ' . $this->getLogTable()
			. ' WHERE action = :actionId'
				. ' AND kid = :kId'
			. ' ORDER BY timestamp DESC'
		;

		$debugDataArray = array(
			'actionId' => $actionId,
			'kId' => $campaignId
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':actionId', $actionId, \PDO::PARAM_INT);
			$stmt->bindParam(':kId', $campaignId, \PDO::PARAM_INT);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, 'ActionLogEntity');

			if (($row = $stmt->fetch()) !== false) {
				/* @var $row ActionLogEntity */
				$row->setTimestamp($row->getTimestamp());
			} else {
				$row = null;
			}

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $row;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	 ****************************************************************************************** */
	public function getLogTable() {
		return $this->logTable;
	}
	public function setLogTable($logTable) {
		$this->logTable = $logTable;
	}

}