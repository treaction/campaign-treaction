<?php
require_once(DIR_Manager . 'PdoDbManager.php');

// Entity
require_once(DIR_Entity . 'ClientEmsEntity.php');

class BlacklistManager extends \PdoDbManager {
	protected $blacklistTable = 'Blacklist';
	protected $daisTable = 'Datenauskunft';
	protected $clientEmsTable = 'mandant_ems';
	
	
	
	
	
	/**
	 * init
	 * 
	 * @return void
	 */
	public function init() {
		$this->setHost('localhost');
		$this->setDbName('blacklist');
		$this->setDbUser('blackuser');
		$this->setDbPassword('ati21coyu09t');

		parent::init();
	}

	/**
	 * getActiveClientsDataArray
	 * 
	 * @return false|array
	 */
	public function getActiveClientsDataArray() {
		$qry = 'SELECT *'
			. ' FROM ' . $this->getClientEmsTable()
			. ' WHERE 1 = 1'
				. ' AND `active` = 1'
		;

		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, 'ClientEmsEntity');

			$this->debugPdoStatement($stmt);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry
			);
		}


		return $stmt->fetchAll();
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              Blacklist - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * getBlacklistCountDataByClientAndDate
	 * 
	 * @param integer $clientId
	 * @param \DateTime $dataTime
	 * @return false|integer
	 */
	public function getBlacklistCountDataByClientAndDate($clientId, \DateTime $dataTime) {
		$qry = 'SELECT COUNT(1) as `count`'
			. ' FROM ' . $this->getBlacklistTable()
			. ' WHERE 1 = 1'
				. ' AND `mandant` = :clientId'
				. ' AND MONTH(`Datum`) = :month'
				. ' AND YEAR(`Datum`) = :year'
		;

		$debugDataArray = array(
			'clientId' > $clientId,
			'date' => $dataTime
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindValue(':clientId', $clientId, \PDO::PARAM_INT);
			$stmt->bindValue(':month', $dataTime->format('m'), \PDO::PARAM_INT);
			$stmt->bindValue(':year', $dataTime->format('Y'), \PDO::PARAM_INT);
			$stmt->execute();

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $stmt->fetchColumn();
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              Datenauskunft - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * getDaisCountDataByClientIdAndDateAndType
	 * 
	 * @param integer $clientId
	 * @param \DateTime $dataTime
	 * @param string $type
	 * @return false|integer
	 */
	public function getDaisCountDataByClientIdAndDateAndType($clientId, \DateTime $dataTime, $type = 'Datenauskunft') {
		$qry = 'SELECT COUNT(1) as `count`'
			. ' FROM ' . $this->getDaisTable()
			. ' WHERE 1 = 1'
				. ' AND `Mandant` = :clientId'
				. ' AND MONTH(`Versanddatum_Normal`) = :month'
				. ' AND YEAR(`Versanddatum_Normal`) = :year'
		;

		switch ($type) {
			case 'Datenauskunft':
				$qry .= ' AND `Typ` = "Datenauskunft"';
				break;

			case 'Datenauskunft_Extern':
				$qry .= ' AND `Typ` = "Datenauskunft_Extern"';
				break;

			case 'AIS':
				$qry .= ' AND `Typ` = "AIS"';
				break;

			default:
				throw new \InvalidArgumentException('invalid type: ' . $type);
		}
		
		$debugDataArray = array(
			'clientId' > $clientId,
			'date' => $dataTime
		);

		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindValue(':clientId', $clientId, \PDO::PARAM_INT);
			$stmt->bindValue(':month', $dataTime->format('m'), \PDO::PARAM_INT);
			$stmt->bindValue(':year', $dataTime->format('Y'), \PDO::PARAM_INT);
			$stmt->execute();

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $stmt->fetchColumn();
	}
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getBlacklistTable() {
		return $this->blacklistTable;
	}
	public function setBlacklistTable($blacklistTable) {
		$this->blacklistTable = $blacklistTable;
	}

	public function getDaisTable() {
		return $this->daisTable;
	}
	public function setDaisTable($daisTable) {
		$this->daisTable = $daisTable;
	}

	public function getClientEmsTable() {
		return $this->clientEmsTable;
	}
	public function setClientEmsTable($clientEmsTable) {
		$this->clientEmsTable = $clientEmsTable;
	}

}