<?php
class prognose {

	public static function getPassedTime($versanddatum,$time2) {
	
		$heuteY = date("Y");
		$heuteM = date("m");
		$heuteD = date("d");
		
		$datumParts = explode(" ",$versanddatum);
		
		$datumPartsDate = explode("-",$datumParts[0]);
		$datumPartsTime = explode(":",$datumParts[1]);
		
			$datum_timestamp = mktime($datumPartsTime[0],$datumPartsTime[1],$datumPartsTime[2],$datumPartsDate[1],$datumPartsDate[2],$datumPartsDate[0]);
			
			$passedHours = floor(($time2-$datum_timestamp)/3600);
			$passedDays = ($time2-$datum_timestamp)/86400;
			if ($passedDays >= 0.7 && $passedDays <= 1) {
				$passedDays = 1;
			} else {
				$passedDays = floor(($time2-$datum_timestamp)/86400);
			}
			
		$passedTime = array("passedHours" => $passedHours, "passedDays" => $passedDays);
		return $passedTime;

	}
	
	
	public static function getMailCat($versendet,$datum,$openings,$klicks) {
		
		$time = prognose::getPassedTime($datum,time());
		
		$passedHours = $time['passedHours'];
		
		$openingRate = round((($openings/$versendet)*100),2);
		$klickRate = round((($klicks/$versendet)*100),2);

		if ($passedHours>=0 && $passedHours<=5) {		
			
			if ($passedHours == 0) {
			
				if ($openingRate < 0.05) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 0.05 && $openingRate <= 0.1) {
					$openingCat = 2;
				}
				
				if ($openingRate > 0.1 && $openingRate <= 0.4) {
					$openingCat = 3;
				}
				
				if ($openingRate > 0.4 && $openingRate <= 0.8) {
					$openingCat = 4;
				}
				
				if ($openingRate > 0.8) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.02) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.02 && $klickRate <= 0.05) {
					$klickCat = 2;
				}
				
				if ($klickRate > 0.05 && $klickRate <= 0.08) {
					$klickCat = 3;
				}
				
				if ($klickRate > 0.08 && $klickRate <= 0.5) {
					$klickCat = 4;
				}
				
				if ($klickRate > 0.5) {
					$klickCat = 5;
				}
			
				
			}
			
			if ($passedHours == 1) {
			
				if ($openingRate <= 0.05) {
					$openingCat = 1;
				}
				
				if ($openingRate > 0.05 && $openingRate <= 0.4) {
					$openingCat = 2;
				}
				
				if ($openingRate > 0.4 && $openingRate <= 0.9) {
					$openingCat = 3;
				}
				
				if ($openingRate > 0.9 && $openingRate <= 1.9) {
					$openingCat = 4;
				}
				
				if ($openingRate > 1.9) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.02) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.02 && $klickRate <= 0.05) {
					$klickCat = 2;
				}
				
				if ($klickRate > 0.05 && $klickRate <= 0.08) {
					$klickCat = 3;
				}
				
				if ($klickRate > 0.08 && $klickRate <= 0.9) {
					$klickCat = 4;
				}
				
				if ($klickRate > 0.9) {
					$klickCat = 5;
				}
				
			}
			
			if ($passedHours == 2) {
			
				if ($openingRate < 0.5) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 0.5 && $openingRate <= 0.9) {
					$openingCat = 2;
				}
				
				if ($openingRate > 0.9 && $openingRate <= 1.9) {
					$openingCat = 3;
				}
				
				if ($openingRate > 1.9 && $openingRate <= 3.9) {
					$openingCat = 4;
				}
				
				if ($openingRate > 3.9) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.02) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.02 && $klickRate <= 0.09) {
					$klickCat = 2;
				}
				
				if ($klickRate > 0.09 && $klickRate <= 0.19) {
					$klickCat = 3;
				}
				
				if ($klickRate > 0.19 && $klickRate <= 1.4) {
					$klickCat = 4;
				}
				
				if ($klickRate > 1.4) {
					$klickCat = 5;
				}
			
			}
			
			
			if ($passedHours == 3) {
			
				if ($openingRate < 0.8) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 0.8 && $openingRate <= 1.9) {
					$openingCat = 2;
				}
				
				if ($openingRate > 1.9 && $openingRate <= 2.9) {
					$openingCat = 3;
				}
				
				if ($openingRate > 2.9 && $openingRate <= 6.9) {
					$openingCat = 4;
				}
				
				if ($openingRate > 6.9) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.04) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.04 && $klickRate <= 0.14) {
					$klickCat = 2;
				}
				
				if ($klickRate > 0.14 && $klickRate <= 0.25) {
					$klickCat = 3;
				}
				
				if ($klickRate > 0.25 && $klickRate <= 1.9) {
					$klickCat = 4;
				}
				
				if ($klickRate > 1.9) {
					$klickCat = 5;
				}
			
			}
			
			
			if ($passedHours == 4) {
			
				if ($openingRate < 1) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 1 && $openingRate <= 1.9) {
					$openingCat = 2;
				}
				
				if ($openingRate > 1.9 && $openingRate <= 3.9) {
					$openingCat = 3;
				}
				
				if ($openingRate > 3.9 && $openingRate <= 7.9) {
					$openingCat = 4;
				}
				
				if ($openingRate > 7.9) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.04) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.04 && $klickRate <= 0.19) {
					$klickCat = 2;
				}
				
				if ($klickRate > 0.19 && $klickRate <= 0.29) {
					$klickCat = 3;
				}
				
				if ($klickRate > 0.29 && $klickRate <= 2.4) {
					$klickCat = 4;
				}
				
				if ($klickRate > 2.4) {
					$klickCat = 5;
				}
			
			}
			
			
			if ($passedHours == 5) {
			
				if ($openingRate < 1.5) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 1.6 && $openingRate <= 3.9) {
					$openingCat = 2;
				}
				
				if ($openingRate > 3.9 && $openingRate <= 5.9) {
					$openingCat = 3;
				}
				
				if ($openingRate > 5.9 && $openingRate <= 8.9) {
					$openingCat = 4;
				}
				
				if ($openingRate > 8.9) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.09) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.09 && $klickRate <= 0.24) {
					$klickCat = 2;
				}
				
				if ($klickRate > 0.24 && $klickRate <= 0.49) {
					$klickCat = 3;
				}
				
				if ($klickRate > 0.49 && $klickRate <= 2.9) {
					$klickCat = 4;
				}
				
				if ($klickRate > 2.9) {
					$klickCat = 5;
				}
				
			}
			
			
				
		}
		
		if ($time['passedDays']>=1 && $time['passedDays']<=4) {
			
			if ($time['passedDays'] == 1) {
				
				if ($openingRate < 1.9) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 1.9 && $openingRate <= 5.9) {
					$openingCat = 2;
				}
				
				if ($openingRate > 5.9 && $openingRate <= 9.9) {
					$openingCat = 3;
				}
				
				if ($openingRate > 9.9 && $openingRate <= 17.9) {
					$openingCat = 4;
				}
				
				if ($openingRate > 17.9) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.1) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.1 && $klickRate <= 0.4) {
					$klickCat = 2;
				}
				
				if ($klickRate > 0.4 && $klickRate <= 0.7) {
					$klickCat = 3;
				}
				
				if ($klickRate > 0.7 && $klickRate <= 2.9) {
					$klickCat = 4;
				}
				
				if ($klickRate > 2.9) {
					$klickCat = 5;
				}
				
			}
			
			
			if ($time['passedDays']== 2) {
				
				if ($openingRate < 2.9) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 2.9 && $openingRate <= 6.9) {
					$openingCat = 2;
				}
				
				if ($openingRate > 6.9 && $openingRate <= 10.9) {
					$openingCat = 3;
				}
				
				if ($openingRate > 10.9 && $openingRate <= 19.9) {
					$openingCat = 4;
				}
				
				if ($openingRate > 19.9) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.3) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.3 && $klickRate <= 0.6) {
					$klickCat = 2;
				}
				
				if ($klickRate > 0.6 && $klickRate <= 1.1) {
					$klickCat = 3;
				}
				
				if ($klickRate > 1.1 && $klickRate <= 2.9) {
					$klickCat = 4;
				}
				
				if ($klickRate > 2.9) {
					$klickCat = 5;
				}
				
			}
			
			
			if ($time['passedDays'] >= 3) {
				
				if ($openingRate < 2.9) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 2.9 && $openingRate <= 7.9) {
					$openingCat = 2;
				}
				
				if ($openingRate > 7.9 && $openingRate <= 12.9) {
					$openingCat = 3;
				}
				
				if ($openingRate > 12.9 && $openingRate <= 19.9) {
					$openingCat = 4;
				}
				
				if ($openingRate > 19.9) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.3) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.3 && $klickRate <= 0.6) {
					$klickCat = 2;
				}
				
				if ($klickRate > 0.6 && $klickRate <= 1.2) {
					$klickCat = 3;
				}
				
				if ($klickRate > 1.2 && $klickRate <= 2.9) {
					$klickCat = 4;
				}
				
				if ($klickRate > 2.9) {
					$klickCat = 5;
				}
			}
			
				
			
		}
		
		$catArr = array("passedDays" => $time['passedDays'], "passedHours" => $passedHours, "oCat" => $openingCat, "kCat" => $klickCat);
		return $catArr;
	}
	
	
	
	public static function getPrognose($gebucht,$versendet,$klicks,$openings,$day,$hour,$oCat,$kCat) {

		if ($hour >= 0 && $hour <= 23 && $day == 0) {

			if ($hour == 0) {
				
				switch ($oCat) {
					case 1: $okoeff = 1.5; break;
					case 2: $okoeff = 1.7; break;
					case 3: $okoeff = 2; break;
					case 4: $okoeff = 2.5; break;
					case 5: $okoeff = 4; break;
				}
				
				switch ($kCat) {
					case 1: $kkoeff = 1.5; break;
					case 2: $kkoeff = 2; break;
					case 3: $kkoeff = 3; break;
					case 4: $kkoeff = 3.5; break;
					case 5: $kkoeff = 4; break;
				}
				
			}
			
			
			if ($hour == 1) {
				
				switch ($oCat) {
					case 1: $okoeff = 1.1; break;
					case 2: $okoeff = 1.2; break;
					case 3: $okoeff = 1.3; break;
					case 4: $okoeff = 1.5; break;
					case 5: $okoeff = 1.5; break;
				}
				
				switch ($kCat) {
					case 1: $kkoeff = 1.3; break;
					case 2: $kkoeff = 1.3; break;
					case 3: $kkoeff = 1.8; break;
					case 4: $kkoeff = 2; break;
					case 5: $kkoeff = 2.5; break;
				}
				
			}
			
			
			if ($hour == 2) {
				
				switch ($oCat) {
					case 1: $okoeff = 1.2; break;
					case 2: $okoeff = 1.2; break;
					case 3: $okoeff = 1.3; break;
					case 4: $okoeff = 1.4; break;
					case 5: $okoeff = 1.4; break;
				}
				
					$kkoeff = 1.2;
				
			}
			
			
			if ($hour > 2 && $hour <= 5) {
				
					$okoeff = 1.1;
					$kkoeff = 1.1;
				
			}
			
			if ($hour > 5) {
				
					$okoeff = 1.05;
					$kkoeff = 1.05;
				
			}
		
		}
		
		if ($day >= 1) {

			
			if ($day == 1) {
				$okoeff = 1.05;
				$kkoeff = 1.03;
			}
			
			if ($day == 2) { 
			
				$okoeff = 1.04;
				$kkoeff = 1.03;
				
			}
			
			if ($day == 3) { 
			
				$okoeff = 1.03;
				$kkoeff = 1.03;
				
			}
			
			if ($day == 4) { 
			
				$okoeff = 1.02;
				$kkoeff = 1.02;
				
			}
			
			if ($day >= 5) { 
			
				$okoeff = 1.01;
				$kkoeff = 1.01;
				
			}
			
		
		}
		
		$koeffArr = array("okoeff" => $okoeff, "kkoeff" => $kkoeff);
		return $koeffArr;
	
	}
	
	
	public static function getPrognoseData ($gebucht,$versendet,$versanddatum,$oeffner,$klicks) {
	
		global $rateHoursArr;
		global $rateLeftHoursArr;
		global $rateLeftDaysArr;
		global $lefth;
		global $passedDays_;

		$rateHoursArr = array();
		$rateLeftHoursArr = array();
		$rateLeftDaysArr = array();
		
		$t = prognose::getMailCat($versendet,$versanddatum,$oeffner,$klicks);
		
		$passedDays_ = $t['passedDays'];

		if ($t['passedHours'] >= 0 && $t['passedHours']<4 && $t['passedDays'] == 0) {

			$lefth = 4-$t['passedHours'];
			
			for($i=1; $i <= $lefth; $i++) {
				$t2 = prognose::getPrognose($gebucht,$versendet,$klicks,$oeffner,0,$lefth,$t['oCat'],$t['kCat']);
				
				$klicks = ceil($klicks*$t2["kkoeff"]);
				$oeffner = ceil($oeffner*$t2["okoeff"]);
				
				$krate = round((($klicks/$gebucht)*100),2);
				$orate = round((($oeffner/$gebucht)*100),2);
				
			}
			
			$rateHoursArr = array("klicks" => $klicks, "openings" => $oeffner, "kRate" => $krate, "oRate" => $orate);
		}
		
		if ($t['passedHours'] >= 4 && $t['passedHours']<23 && $t['passedDays'] == 0) {

			$d = date("Y-m-d 00:00:00");
			
			$time = prognose::getPassedTime($d,time());
			
			$j = 24-$time['passedHours'];
			
			for($i=1; $i < $j; $i++) {
			
				$t2 = prognose::getPrognose($gebucht,$versendet,$klicks,$oeffner,0,$j,"","");
				
				$klicks = ceil($klicks*$t2["kkoeff"]);
				$oeffner = ceil($oeffner*$t2["okoeff"]);
				
				$krate = round((($klicks/$gebucht)*100),2);
				$orate = round((($oeffner/$gebucht)*100),2);
				
				$h = $i;
				
				$rateLeftHoursArr = array("klicks" => $klicks, "openings" => $oeffner, "kRate" => $krate, "oRate" => $orate);
			}
			
		}
				
		if ($t['passedDays'] >= 0 && $t['passedDays']<5) {

			$d = $t['passedDays'];
			$pt = $t['passedHours'];
			if ($d == 0) {
				$d = 1;
				$j = 4;
			} else {
				$j = 4-$d;
			}
			
			
			for($i=1; $i <= $j; $i++) {
				$t2 = prognose::getPrognose($gebucht,$versendet,$klicks,$oeffner,$d,$t['oCat'],$t['kCat']);

				if ($t['passedDays'] == 0 && $i==1) {

					if ($pt == 0) {
						switch ($t['oCat']) {
							case 1: $newO = 4; break;
							case 2: $newO = 7; break;
							case 3: $newO = 10; break;
							case 4: $newO = 12; break;
							case 5: $newO = 15; break;
						}
						
						switch ($t['kCat']) {
							case 1: $newK = 2; break;
							case 2: $newK = 3; break;
							case 3: $newK = 5; break;
							case 4: $newK = 6; break;
							case 5: $newK = 10; break;
						}
					}
					
					if ($pt == 1) {
					
						switch ($t['oCat']) {
							case 1: $newO = 2; break;
							case 2: $newO = 2.5; break;
							case 3: $newO = 3; break;
							case 4: $newO = 3.5; break;
							case 5: $newO = 4; break;
						}
						
						switch ($t['kCat']) {
							case 1: $newK = 2; break;
							case 2: $newK = 2; break;
							case 3: $newK = 2.3; break;
							case 4: $newK = 3; break;
							case 5: $newK = 3; break;
						}
					}
					
					if ($pt == 2) {
					
						switch ($t['oCat']) {
							case 1: $newO = 2; break;
							case 2: $newO = 2.3; break;
							case 3: $newO = 2.7; break;
							case 4: $newO = 3.2; break;
							case 5: $newO = 3.5; break;
						}
						
						switch ($t['kCat']) {
							case 1: $newK = 2; break;
							case 2: $newK = 2; break;
							case 3: $newK = 2.5; break;
							case 4: $newK = 2.7; break;
							case 5: $newK = 3; break;
						}
					}
					
					
					if ($pt == 3) {
					
						switch ($t['oCat']) {
							case 1: $newO = 1.3; break;
							case 2: $newO = 1.5; break;
							case 3: $newO = 2; break;
							case 4: $newO = 2.2; break;
							case 5: $newO = 2.5; break;
						}
						
						switch ($t['kCat']) {
							case 1: $newK = 1.3; break;
							case 2: $newK = 1.5; break;
							case 3: $newK = 1.5; break;
							case 4: $newK = 1.5; break;
							case 5: $newK = 2; break;
						}
					}
					
					
					if ($pt == 4) {
					
						switch ($t['oCat']) {
							case 1: $newO = 1.2; break;
							case 2: $newO = 1.4; break;
							case 3: $newO = 1.6; break;
							case 4: $newO = 1.8; break;
							case 5: $newO = 2; break;
						}
						
						switch ($t['kCat']) {
							case 1: $newK = 1.2; break;
							case 2: $newK = 1.3; break;
							case 3: $newK = 2; break;
							case 4: $newK = 2.2; break;
							case 5: $newK = 2.4; break;
						}
					}
					
					
					$klicks = $klicks*$newK;
					$oeffner = $oeffner*$newO;
					
					if (count($rateLeftHoursArr) > 0) {
						$klicks = $rateLeftHoursArr['klicks'];
						$oeffner = $rateLeftHoursArr['openings'];
					}

				}
				
				
				$klicks = ceil($klicks*$t2["kkoeff"]);
				$oeffner = ceil($oeffner*$t2["okoeff"]);
				
				
				$krate = round((($klicks/$gebucht)*100),2);
				$orate = round((($oeffner/$gebucht)*100),2);
				
				$rateLeftDaysArr[] = array("klicks" => $klicks, "openings" => $oeffner, "kRate" => $krate, "oRate" => $orate);
				$d++;
				
				
			}
		}
		
		if ($t['passedDays']>=5) {

			$j = $t['passedDays'];
				$t2 = prognose::getPrognose($gebucht,$versendet,$klicks,$oeffner,$j,"","");
				$krate = round((($klicks/$gebucht)*100),2);
				$orate = round((($oeffner/$gebucht)*100),2);
				$rateLeftDaysArr[] = array("klicks" => $klicks, "openings" => $oeffner, "kRate" => $krate, "oRate" => $orate);
		}
		
	}
	
}

?>