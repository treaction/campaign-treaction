<?php
class emsPackage {
    public function __construct() {
        // TODO; $mandant, include und $this->dbh entfernen
        $mandant = $_SESSION['mandant'];
        include($_SERVER['DOCUMENT_ROOT'] . '/system/db_connect.inc.php');
        
        $this->dbh = new PDO('mysql:host=' . $dbHostEms . ';dbname=' . $db_name, $db_user, $db_pw);
        $this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
    }

    public function getUserModules($userID) {
        $qry = 'SELECT `menu`' . 
            ' FROM `benutzer`' . 
            ' WHERE `benutzer_id` = :userID'
        ;
        $stmt = $this->dbh2->prepare($qry);
        $stmt->bindParam(':userID', $userID, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $menuArr = explode(',', $row['menu']);

        return $menuArr;
    }

    public function getPackageID($mID) {
        $qry = 'SELECT `package`' . 
            ' FROM `mandant`' . 
            ' WHERE `id` = :mID'
        ;
        $stmt = $this->dbh2->prepare($qry);
        $stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $packageArr = explode(',', $row['package']);

        return $packageArr;
    }

    public function getUntermandantID($mID) {
        $qry = 'SELECT `untermandant`' . 
            ' FROM `mandant`' . 
            ' WHERE `id` = :mID'
        ;
        $stmt = $this->dbh2->prepare($qry);
        $stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $packageArr = '';
        if ($row['untermandant'] != '') {
            $packageArr = explode(',', $row['untermandant']);
        }

        return $packageArr;
    }

    public function getUserUntermandantID($userID) {
        $qry = 'SELECT `zugang_mandant`' . 
            ' FROM `benutzer`' . 
            ' WHERE `benutzer_id` = :userID'
        ;
        $stmt = $this->dbh2->prepare($qry);
        $stmt->bindParam(':userID', $userID, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $packageArr = array();
        if ($row['zugang_mandant']) {
            $packageArr = explode(',', $row['zugang_mandant']);
        }

        return $packageArr;
    }

    public function getUntermandantName($pID) {
        $qry = 'SELECT `mandant`' . 
            ' FROM `mandant`' . 
            ' WHERE `id` = :pID'
        ;
        $stmt = $this->dbh2->prepare($qry);
        $stmt->bindParam(':pID', $pID, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row['mandant'];
    }

    public function getUntermandantAbkz($pID) {
        $qry = 'SELECT `abkz`' . 
            ' FROM `mandant`' . 
            ' WHERE `id` = :pID'
        ;
        $stmt = $this->dbh2->prepare($qry);
        $stmt->bindParam(':pID', $pID, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row['abkz'];
    }

    public function getPackageName($pID) {
        $qry = 'SELECT `paket_name`' . 
            ' FROM `ems_paket`' . 
            ' WHERE `paketID` = :pID'
        ;
        $stmt = $this->dbh2->prepare($qry);
        $stmt->bindParam(':pID', $pID, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row['paket_name'];
    }
}
?>