<?php
class ClientClickProfileEntity {
	public $uid;
	public $client_id;
	public $click_profile_id;
	public $active;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getUid() {
		return (int) $this->uid;
	}
	public function setUid($uid) {
		$this->uid = \intval($uid);
	}
	
	public function getClient_id() {
		return (int) $this->client_id;
	}
	public function setClient_id($clientId) {
		$this->client_id = \intval($clientId);
	}

	public function getClick_profile_id() {
		return (int) $this->click_profile_id;
	}
	public function setClick_profile_id($clickProfileId) {
		$this->click_profile_id = \intval($clickProfileId);
	}
	
	public function getActive() {
		return (boolean) $this->active;
	}
	public function setActive($active) {
		$this->active = (boolean) \intval($active);
	}

}