<?php
class ClientClickProfileWidthClickProfileEntity extends \ClientClickProfileEntity {
	/**
	 * @var \ClickProfileEntity
	 */
	public $clickProfile;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getClickProfile() {
		return $this->clickProfile;
	}
	public function setClickProfile(\ClickProfileEntity $clickProfile) {
		$this->clickProfile = $clickProfile;
	}

}