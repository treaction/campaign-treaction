<?php


/**
 * Description of EspMappingEntity
 *
 * @author Sami Jarmoud
 */
class EspMappingEntity{

    /**
     * id
     * 
     * @var integer
     */
    protected $id;

    /**
     * Online_Version
     * 
     * @var string
     */
    protected $Online_Version;
    
     /**
     * Unsubscribe_link
     * 
     * @var string
     */
    protected $Unsubscribe_link;
    
     /**
     * Open_Pixel 
     * 
     * @var string
     */
    protected $Open_Pixel;
    
      /**
     * Anrede 
     * 
     * @var string
     */
    protected $Anrede;
    
    /**
     * Vorname
     * 
     * @var string
     */
    protected $Vorname;
    
    /**
     * Nachname 
     * 
     * @var string
     */
    
    protected $Nachname;
    
    /**
     * Email 
     * 
     * @var string
     */
    
    protected $Email;

    /**
     * Contact_formal 
     * 
     * @var string
     */
    protected $Contact_formal;
    
    /**
     * Contact_unformal 
     * 
     * @var string
     */
    protected $Contact_unformal;
    
    /**
     * Contact_standard 
     * 
     * @var string
     */
    protected $Contact_standard;

    /**
     * Versandsystem
     * 
     * @var string
     */
    protected $Versandsystem;

    /**
     * m_id
     * 
     * @var integer
     */
    protected $m_id;
    /**
     * active
     * 
     * @var integer
     */
    protected $active;

    /**
     * user_id
     * 
     * @var integer
     */
    protected $user_id;
    
    /**
    * create_at
    * 
    * @var \DateTime
    */
   protected $create_at;

   /**
    * update_at
    * 
    * @var \DateTime
    */
   protected $update_at;
   
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getOnlineVersion()
    {
        return $this->Online_Version;
    }

    /**
     * @param string $Online_Version
     */
    public function setOnlineVersion($Online_Version)
    {
        $this->Online_Version = $Online_Version;
    }

    /**
     * @return string
     */
    public function getUnsubscribeLink()
    {
        return $this->Unsubscribe_link;
    }

    /**
     * @param string $Unsubscribe_link
     */
    public function setUnsubscribeLink($Unsubscribe_link)
    {
        $this->Unsubscribe_link = $Unsubscribe_link;
    }

    /**
     * @return string
     */
    public function getOpenPixel()
    {
        return $this->Open_Pixel;
    }

    /**
     * @param string $Open_Pixel
     */
    public function setOpenPixel($Open_Pixel)
    {
        $this->Open_Pixel = $Open_Pixel;
    }

    function getContact_formal() {
        return $this->Contact_formal;
    }

    function getContact_unformal() {
        return $this->Contact_unformal;
    }

    function getContact_standard() {
        return $this->Contact_standard;
    }

    function setContact_formal($Contact_formal) {
        $this->Contact_formal = $Contact_formal;
    }

    function setContact_unformal($Contact_unformal) {
        $this->Contact_unformal = $Contact_unformal;
    }

    function setContact_standard($Contact_standard) {
        $this->Contact_standard = $Contact_standard;
    }

    
    /**
     * @return string
     */
    public function getVersandsystem()
    {
        return $this->Versandsystem;
    }

    /**
     * @param string $Versandsystem
     */
    public function setVersandsystem($Versandsystem)
    {
        $this->Versandsystem = $Versandsystem;
    }

    /**
     * @return int
     */
    public function getMId()
    {
        return $this->m_id;
    }

    /**
     * @param int $m_id
     */
    public function setMId($m_id)
    {
        $this->m_id = $m_id;
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    public function getAnrede() {
        return $this->Anrede;
    }

    public function getVorname() {
        return $this->Vorname;
    }

    public function getNachname() {
        return $this->Nachname;
    }

    public function setAnrede($Anrede) {
        $this->Anrede = $Anrede;
    }

    public function setVorname($Vorname) {
        $this->Vorname = $Vorname;
    }

    public function setNachname($Nachname) {
        $this->Nachname = $Nachname;
    }
    
    function getEmail() {
        return $this->Email;
    }

    function setEmail($Email) {
        $this->Email = $Email;
    }

    function getUser_id() {
        return $this->user_id;
    }

    function getCreate_at() {
        return $this->create_at;
    }

    function getUpdate_at() {
        return $this->update_at;
    }

    function setUser_id($user_id) {
        $this->user_id = $user_id;
    }

    function setCreate_at($create_at) {
        $this->create_at = $create_at;
    }

    function setUpdate_at($update_at) {
        $this->update_at = $update_at;
    }


}
