<?php
require_once(DIR_Entity . 'StaticCountryEntity.php');

class CustomerEntity {
	public $kunde_id;
	public $firma;
	public $firma_short;
	public $strasse;
	public $plz;
	public $ort;
	public $telefon;
	public $fax;
	public $website;
	public $email;
	public $geschaeftsfuehrer;
	public $registergericht;
	public $status;
	
	// newFields
	public $vat_number;
	public $payment_deadline;
	public $country_id;
	/**
	 * @var StaticCountryEntity
	 */
	public $staticCountry;
	public $data_selection;
	
	public $notes;
	
        public $monatsrechnung;
	/**
	 * @deprecated
	 */
	public $temp1;
	public $temp2;
	
	// TODO: remove
	public $crm_customer_id;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getKunde_id() {
		return (int) $this->kunde_id;
	}
	public function setKunde_id($kunde_id) {
		$this->kunde_id = \intval($kunde_id);
	}

	public function getFirma() {
		return $this->firma;
	}
	public function setFirma($firma) {
		$this->firma = $firma;
	}

	public function getFirma_short() {
		return $this->firma_short;
	}
	public function setFirma_short($firma_short) {
		$this->firma_short = $firma_short;
	}

	public function getStrasse() {
		return $this->strasse;
	}
	public function setStrasse($strasse) {
		$this->strasse = $strasse;
	}

	public function getPlz() {
		return $this->plz;
	}
	public function setPlz($plz) {
		$this->plz = $plz;
	}

	public function getOrt() {
		return $this->ort;
	}
	public function setOrt($ort) {
		$this->ort = $ort;
	}

	public function getTelefon() {
		return $this->telefon;
	}
	public function setTelefon($telefon) {
		$this->telefon = $telefon;
	}

	public function getFax() {
		return $this->fax;
	}
	public function setFax($fax) {
		$this->fax = $fax;
	}

	public function getWebsite() {
		return $this->website;
	}
	public function setWebsite($website) {
		$this->website = $website;
	}

	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
	}

	public function getGeschaeftsfuehrer() {
		return $this->geschaeftsfuehrer;
	}
	public function setGeschaeftsfuehrer($geschaeftsfuehrer) {
		$this->geschaeftsfuehrer = $geschaeftsfuehrer;
	}

	public function getRegistergericht() {
		return $this->registergericht;
	}
	public function setRegistergericht($registergericht) {
		$this->registergericht = $registergericht;
	}

	public function getStatus() {
		return (int) $this->status;
	}
	public function setStatus($status) {
		$this->status = \intval($status);
	}

	public function getVat_number() {
		return $this->vat_number;
	}
	public function setVat_number($vat_number) {
		$this->vat_number = $vat_number;
	}
	
	public function getPayment_deadline() {
		return (int) $this->payment_deadline;
	}
	public function setPayment_deadline($payment_deadline) {
		$this->payment_deadline = \intval($payment_deadline);
	}

	public function getCountry_id() {
		return (int) $this->country_id;
	}
	public function setCountry_id($country_id) {
		$this->country_id = \intval($country_id);
	}
	
	public function getStaticCountry() {
		return $this->staticCountry;
	}
	public function setStaticCountry(StaticCountryEntity $staticCountry) {
		$this->staticCountry = $staticCountry;
	}
	
	public function getData_selection() {
		return (int) $this->data_selection;
	}
	public function setData_selection($data_selection) {
		$this->data_selection = \intval($data_selection);
	}
	
	public function getNotes() {
		return $this->notes;
	}
	public function setNotes($notes) {
		$this->notes = $notes;
	}

	public function getMonatsrechnung() {
		return $this->monatsrechnung;
	}
	public function setMonatsrechnung($monatsrechnung) {
		$this->monatsrechnung = $monatsrechnung;
	}	
	// @deprecated
	public function getTemp1() {
		return $this->temp1;
	}
	public function setTemp1($temp1) {
		$this->temp1 = $temp1;
	}

	public function getTemp2() {
		return $this->temp2;
	}
	public function setTemp2($temp2) {
		$this->temp2 = $temp2;
	}
	
	// TODO: remove
	public function getCrm_customer_id() {
		return $this->crm_customer_id;
	}
	public function setCrm_customer_id($crm_customer_id) {
		$this->crm_customer_id = $crm_customer_id;
	}

}