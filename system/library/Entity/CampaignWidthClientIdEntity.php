<?php
require_once(DIR_Entity . 'CampaignEntity.php');

class CampaignWidthClientIdEntity extends CampaignEntity {
	public $client_id;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getClient_id() {
		return (int) $this->client_id;
	}
	public function setClient_id($client_id) {
		$this->client_id = \intval($client_id);
	}

}