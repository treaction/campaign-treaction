<?php
class ActionLogEntity {
	public $logid;
	public $userid;
	public $action;
	public $kid;
	public $nv_id;
	/**
	 * @var DateTime
	 */
	public $timestamp;
	public $status;
	public $import_nr;
	public $log_data;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getLogid() {
		return (int) $this->logid;
	}
	public function setLogid($logid) {
		$this->logid = \intval($logid);
	}

	public function getUserid() {
		return (int) $this->userid;
	}
	public function setUserid($userid) {
		$this->userid = \intval($userid);
	}

	public function getAction() {
		return (int) $this->action;
	}
	public function setAction($action) {
		$this->action = \intval($action);
	}

	public function getKid() {
		return (int) $this->kid;
	}
	public function setKid($kid) {
		$this->kid = \intval($kid);
	}

	public function getNv_id() {
		return (int) $this->nv_id;
	}
	public function setNv_id($nv_id) {
		$this->nv_id = \intval($nv_id);
	}

	public function getTimestamp() {
		return $this->timestamp;
	}
	public function setTimestamp($timestamp) {
		$this->timestamp = new \DateTime($timestamp);
	}

	public function getStatus() {
		return (int) $this->status;
	}
	public function setStatus($status) {
		$this->status = \intval($status);
	}

	public function getImport_nr() {
		return (int) $this->import_nr;
	}
	public function setImport_nr($import_nr) {
		$this->import_nr = \intval($import_nr);
	}
	
	public function getLog_data() {
		return $this->log_data;
	}
	public function setLog_data($log_data) {
		$this->log_data = $log_data;
	}

}