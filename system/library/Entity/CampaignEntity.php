<?php
class CampaignEntity {
	public $k_id;
	public $mail_id;
	public $nv_id;
	public $agentur_id;
	public $ap_id;
	public $datum;
	public $status;
	public $bearbeiter;
	public $k_name;
	public $betreff;
	public $absendername;
	public $gebucht;
	public $versendet;
	public $gestartet;
	public $beendet;
	public $openings;
	public $openings_all;
	public $vorgabe_o;
	public $klicks;
	public $klicks_all;
	public $sbounces;
	public $hbounces;
	public $vorgabe_k;
	public $zielgruppe;
	public $blacklist;
	public $mailtyp;
	public $notiz;
	public $tkp;
	public $abrechnungsart;
	public $preis;
	public $preis_gesamt;
	public $leads;
	public $report;
	public $rechnung;
	public $edit_lock;
	public $erstreporting;
	public $endreporting;
	public $preis2;
	public $vorgabe_m;
	public $hash;
	public $klickrate_extern;
	public $openingrate_extern;
	public $leads_u;
	public $abmelder;
	public $preis_gesamt_verify;
	public $vertriebler_id;
        public $kosten;
        public $send_mail_tcm;
        public $monatsrechnung;
        public $send_bounce_mail;
        public $broking_volume;
	public $broking_price;
        public $hybrid_preis;
        public $hybrid_preis2;
        public $externaljobid;
	/**
	 * @var DateTime
	 */
	public $last_update;
	public $dsd_id;
	public $unit_price;
	public $use_campaign_start_date;
	public $campaign_click_profiles_id;
	public $selection_click_profiles_id;
	public $premium_campaign;
	public $advertising_materials;
	
	
        

	
	/**
	 * @deprecated -> use dsdId
	 */
	public $versandsystem;
	public $agentur;
	public $ap;
	
	// TODO: entfernen
	public $crm_order_id;
	public $crm_guid;
	public $cs_id;
	public $anfrage;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getK_id() {
		return (int) $this->k_id;
	}
	public function setK_id($k_id) {
		$this->k_id = \intval($k_id);
	}

	public function getMail_id() {
		return $this->mail_id;
	}
	public function setMail_id($mail_id) {
		$this->mail_id = $mail_id;
	}

	public function getNv_id() {
		return (int) $this->nv_id;
	}
	public function setNv_id($nv_id) {
		$this->nv_id = \intval($nv_id);
	}

	public function getAgentur_id() {
		return (int) $this->agentur_id;
	}
	public function setAgentur_id($agentur_id) {
		$this->agentur_id = \intval($agentur_id);
	}

	public function getAp_id() {
		return $this->ap_id;
	}
	public function setAp_id($ap_id) {
		$this->ap_id = \intval($ap_id);
	}

	public function getDatum() {
		return $this->datum;
	}
	public function setDatum($datum) {
		$this->datum = new \DateTime($datum);
	}

	public function getStatus() {
		return (int) $this->status;
	}
	public function setStatus($status) {
		$this->status = \intval($status);
	}

	public function getBearbeiter() {
		return $this->bearbeiter;
	}
	public function setBearbeiter($bearbeiter) {
		$this->bearbeiter = $bearbeiter;
	}

	public function getK_name() {
		return $this->k_name;
	}
	public function setK_name($k_name) {
		$this->k_name = $k_name;
	}

	public function getBetreff() {
		return $this->betreff;
	}
	public function setBetreff($betreff) {
		$this->betreff = $betreff;
	}

	public function getAbsendername() {
		return $this->absendername;
	}
	public function setAbsendername($absendername) {
		$this->absendername = $absendername;
	}

	public function getGebucht() {
		return (int) $this->gebucht;
	}
	public function setGebucht($gebucht) {
		$this->gebucht = \intval($gebucht);
	}

	public function getVersendet() {
		return (int) $this->versendet;
	}
	public function setVersendet($versendet) {
		$this->versendet = \intval($versendet);
	}

	public function getGestartet() {
		return $this->gestartet;
	}
	public function setGestartet($gestartet) {
		if ($gestartet != '1970-01-01 02:00:00' 
			&& $gestartet != '0000-00-00 00:00:00'
		) {
			$this->gestartet = new \DateTime($gestartet);
		}
	}

	public function getBeendet() {
		return $this->beendet;
	}
	public function setBeendet($beendet) {
		if ($beendet != '1970-01-01 02:00:00' 
			&& $beendet != '0000-00-00 00:00:00'
		) {
			$this->beendet = new \DateTime($beendet);
		}
	}

	public function getOpenings() {
		return (int) $this->openings;
	}
	public function setOpenings($openings) {
		$this->openings = \intval($openings);
	}

	public function getOpenings_all() {
		return (int) $this->openings_all;
	}
	public function setOpenings_all($openings_all) {
		$this->openings_all = \intval($openings_all);
	}

	public function getVorgabe_o() {
		return (float) $this->vorgabe_o;
	}
	public function setVorgabe_o($vorgabe_o) {
		$this->vorgabe_o = \floatval($vorgabe_o);
	}

	public function getKlicks() {
		return (int) $this->klicks;
	}
	public function setKlicks($klicks) {
		$this->klicks = \intval($klicks);
	}

	public function getKlicks_all() {
		return (int) $this->klicks_all;
	}
	public function setKlicks_all($klicks_all) {
		$this->klicks_all = \intval($klicks_all);
	}

	public function getSbounces() {
		return (int) $this->sbounces;
	}
	public function setSbounces($sbounces) {
		$this->sbounces = \intval($sbounces);
	}

	public function getHbounces() {
		return (int) $this->hbounces;
	}
	public function setHbounces($hbounces) {
		$this->hbounces = \intval($hbounces);
	}

	public function getVorgabe_k() {
		return (float) $this->vorgabe_k;
	}
	public function setVorgabe_k($vorgabe_k) {
		$this->vorgabe_k = \floatval($vorgabe_k);
	}

	public function getZielgruppe() {
		return $this->zielgruppe;
	}
	public function setZielgruppe($zielgruppe) {
		$this->zielgruppe = $zielgruppe;
	}

	public function getBlacklist() {
		return $this->blacklist;
	}
	public function setBlacklist($blacklist) {
		$this->blacklist = $blacklist;
	}

	public function getMailtyp() {
		return $this->mailtyp;
	}
	public function setMailtyp($mailtyp) {
		$this->mailtyp = $mailtyp;
	}

	public function getNotiz() {
		return $this->notiz;
	}
	public function setNotiz($notiz) {
		$this->notiz = $notiz;
	}

	public function getTkp() {
		return $this->tkp;
	}
	public function setTkp($tkp) {
		$this->tkp = $tkp;
	}

	public function getAbrechnungsart() {
		return $this->abrechnungsart;
	}
	public function setAbrechnungsart($abrechnungsart) {
		$this->abrechnungsart = $abrechnungsart;
	}

	public function getPreis() {
		return (float) $this->preis;
	}
	public function setPreis($preis) {
		$this->preis = \floatval($preis);
	}

	public function getPreis_gesamt() {
		return (float) $this->preis_gesamt;
	}
	public function setPreis_gesamt($preis_gesamt) {
		$this->preis_gesamt = \floatval($preis_gesamt);
	}

	public function getLeads() {
		return (int) $this->leads;
	}
	public function setLeads($leads) {
		$this->leads = \intval($leads);
	}

	public function getReport() {
		return $this->report;
	}
	public function setReport($report) {
		$this->report = $report;
	}

	public function getRechnung() {
		return $this->rechnung;
	}
	public function setRechnung($rechnung) {
		$this->rechnung = $rechnung;
	}

	public function getEdit_lock() {
		return $this->edit_lock;
	}
	public function setEdit_lock($edit_lock) {
		$this->edit_lock = $edit_lock;
	}

	public function getErstreporting() {
		return $this->erstreporting;
	}
	public function setErstreporting($erstreporting) {
		if ($erstreporting != '0000-00-00 00:00:00') {
			$this->erstreporting = new \DateTime($erstreporting);
		}
	}

	public function getEndreporting() {
		return $this->endreporting;
	}
	public function setEndreporting($endreporting) {
		if ($endreporting != '0000-00-00 00:00:00') {
			$this->endreporting = new \DateTime($endreporting);
		}
	}

	public function getPreis2() {
		return (float) $this->preis2;
	}
	public function setPreis2($preis2) {
		$this->preis2 = \floatval($preis2);
	}

	public function getVorgabe_m() {
		return (int) $this->vorgabe_m;
	}
	public function setVorgabe_m($vorgabe_m) {
		$this->vorgabe_m = \intval($vorgabe_m);
	}

	public function getHash() {
		return $this->hash;
	}
	public function setHash($hash) {
		$this->hash = $hash;
	}

	public function getKlickrate_extern() {
		return (int) $this->klickrate_extern;
	}
	public function setKlickrate_extern($klickrate_extern) {
		$this->klickrate_extern = \intval($klickrate_extern);
	}

	public function getOpeningrate_extern() {
		return (int) $this->openingrate_extern;
	}
	public function setOpeningrate_extern($openingrate_extern) {
		$this->openingrate_extern = \intval($openingrate_extern);
	}

	public function getLeads_u() {
		return (int) $this->leads_u;
	}
	public function setLeads_u($leads_u) {
		$this->leads_u = \intval($leads_u);
	}

	public function getAbmelder() {
		return (int) $this->abmelder;
	}
	public function setAbmelder($abmelder) {
		$this->abmelder = \intval($abmelder);
	}

	public function getPreis_gesamt_verify() {
		return (int) $this->preis_gesamt_verify;
	}
	public function setPreis_gesamt_verify($preis_gesamt_verify) {
		$this->preis_gesamt_verify = \intval($preis_gesamt_verify);
	}

	public function getVertriebler_id() {
		return (int) $this->vertriebler_id;
	}
	public function setVertriebler_id($vertriebler_id) {
		$this->vertriebler_id = \intval($vertriebler_id);
	}

	public function getLast_update() {
		return $this->last_update;
	}
	public function setLast_update($last_update) {
		$this->last_update = new \DateTime($last_update);
	}

	public function getDsd_id() {
		return (int) $this->dsd_id;
	}
	public function setDsd_id($dsd_id) {
		$this->dsd_id = \intval($dsd_id);
	}

	public function getUnit_price() {
		return (float) $this->unit_price;
	}
	public function setUnit_price($unit_price) {
		$this->unit_price = \floatval($unit_price);
	}
	
	public function getUse_campaign_start_date() {
		return (int) $this->use_campaign_start_date;
	}
	public function setUse_campaign_start_date($use_campaign_start_date) {
		$this->use_campaign_start_date = \intval($use_campaign_start_date);
	}
	
	public function getCampaign_click_profiles_id() {
		return (int) $this->campaign_click_profiles_id;
	}
	public function setCampaign_click_profiles_id($campaign_click_profiles_id) {
		$this->campaign_click_profiles_id = \intval($campaign_click_profiles_id);
	}

	public function getSelection_click_profiles_id() {
		return (int) $this->selection_click_profiles_id;
	}
	public function setSelection_click_profiles_id($selection_click_profiles_id) {
		$this->selection_click_profiles_id = \intval($selection_click_profiles_id);
	}
	
	public function getPremium_campaign() {
		return (boolean) $this->premium_campaign;
	}
	public function setPremium_campaign($premium_campaign) {
		$this->premium_campaign = (boolean) \intval($premium_campaign);
	}

	public function getAdvertising_materials() {
		return (int) $this->advertising_materials;
	}
	public function setAdvertising_materials($advertising_materials) {
		$this->advertising_materials = \intval($advertising_materials);
	}
	
	public function getBroking_volume() {
		return (int) $this->broking_volume;
	}
	public function setBroking_volume($broking_volume) {
		$this->broking_volume = \intval($broking_volume);
	}
	
	public function getBroking_price() {
		return (float) $this->broking_price;
	}
	public function setBroking_price($broking_price) {
		$this->broking_price = \floatval($broking_price);
	}
        
        public function getHybrid_preis() {
		return (float) $this->hybrid_preis;
	}
	public function setHybrid_preis($hybrid_preis) {
		$this->hybrid_preis = \floatval($hybrid_preis);
	}
        
         public function getHybrid_preis2() {
		return (float) $this->hybrid_preis2;
	}
	public function setHybrid_preis2($hybrid_preis2) {
		$this->hybrid_preis2 = \floatval($hybrid_preis2);
	}
	
       public function getKosten() {
		return $this->kosten;
	}
	public function setKosten($kosten){
		$this->kosten = $kosten;
	} 
       public function getSend_mail_tcm() {
		return $this->send_mail_tcm;
	}
	public function setSend_mail_tcm($send_mail_tcm){
		$this->send_mail_tcm = $send_mail_tcm;
	}
        public function getSend_bounce_mail() {
		return $this->send_bounce_mail;
	}
	public function setSend_bounce_mail($send_bounce_mail){
		$this->send_bounce_mail= $send_bounce_mail;
	}
	public function getMonatsrechnung() {
		return $this->monatsrechnung;
	}
	public function setMonatsrechnung($monatsrechnung){
		$this->monatsrechnung = $monatsrechnung;
	} 
          public function getExternal_job_id() {
		return $this->externaljobid;
	}
	public function setExternal_job_id($externaljobid){
		$this->externaljobid = $externaljobid;
	}
	// TODO: deprecated
	public function getVersandsystem() {
		return $this->versandsystem;
	}
	public function setVersandsystem($versandsystem) {
		$this->versandsystem = $versandsystem;
	}
	
	public function getAgentur() {
		return $this->agentur;
	}
	public function setAgentur($agentur) {
		$this->agentur = $agentur;
	}
	
	public function getAp() {
		return $this->ap;
	}
	public function setAp($ap) {
		$this->ap = $ap;
	}
	
	// TODO: entfernen
	public function getCrm_order_id() {
		return $this->crm_order_id;
	}
	public function setCrm_order_id($crm_order_id) {
		$this->crm_order_id = $crm_order_id;
	}
	
	public function getCrm_guid() {
		return $this->crm_guid;
	}
	public function setCrm_guid($crm_guid) {
		$this->crm_guid = $crm_guid;
	}
	
	public function getCs_id() {
		return (int) $this->cs_id;
	}
	public function setCs_id($cs_id) {
		$this->cs_id = \intval($cs_id);
	}
	
	public function getAnfrage() {
		return $this->anfrage;
	}
	public function setAnfrage($anfrage) {
		$this->anfrage = $anfrage;
	}
}