<?php
class StaticCountryEntity {
	public $uid;
	public $cn_iso_2;
	public $cn_iso_3;
	public $cn_tldomain;
	public $cn_phone;
	public $cn_short_de;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getUid() {
		return (int) $this->uid;
	}
	public function setUid($uid) {
		$this->uid = \intval($uid);
	}
	
	public function getCn_iso_2() {
		return $this->cn_iso_2;
	}
	public function setCn_iso_2($cn_iso_2) {
		$this->cn_iso_2 = $cn_iso_2;
	}

	public function getCn_iso_3() {
		return $this->cn_iso_3;
	}
	public function setCn_iso_3($cn_iso_3) {
		$this->cn_iso_3 = $cn_iso_3;
	}

	public function getCn_tldomain() {
		return $this->cn_tldomain;
	}
	public function setCn_tldomain($cn_tldomain) {
		$this->cn_tldomain = $cn_tldomain;
	}

	public function getCn_phone() {
		return (int) $this->cn_phone;
	}
	public function setCn_phone($cn_phone) {
		$this->cn_phone = $cn_phone;
	}

	public function getCn_short_de() {
		return $this->cn_short_de;
	}
	public function setCn_short_de($cn_short_de) {
		$this->cn_short_de = $cn_short_de;
	}

}