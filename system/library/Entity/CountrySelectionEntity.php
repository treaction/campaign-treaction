<?php
class CountrySelectionEntity {
	public $id;
	public $title;
	public $active;
	public $set_as_default;
	public $short_title;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}

	public function getTitle() {
		return $this->title;
	}
	public function setTitle($title) {
		$this->title = $title;
	}

	public function getActive() {
		return (boolean) $this->active;
	}
	public function setActive($active) {
		$this->active = (boolean) \intval($active);
	}

	public function getSet_as_default() {
		return (boolean) $this->set_as_default;
	}
	public function setSet_as_default($set_as_default) {
		$this->set_as_default = (boolean) \intval($set_as_default);
	}

	public function getShort_title() {
		return $this->short_title;
	}
	public function setShort_title($short_title) {
		$this->short_title = $short_title;
	}

}