<?php
class DeliverySystemDistributorEntity {
	public $id;
	public $m_asp_id;
	public $title;
	public $distributor_id;
	public $active;
	public $set_as_default;
	public $parent_id;
	public $testlist_distributor_id;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}

	public function getM_asp_id() {
		return (int) $this->m_asp_id;
	}
	public function setM_asp_id($m_asp_id) {
		$this->m_asp_id = \intval($m_asp_id);
	}

	public function getTitle() {
		return $this->title;
	}
	public function setTitle($title) {
		$this->title = $title;
	}

	public function getDistributor_id() {
		return $this->distributor_id;
	}
	public function setDistributor_id($distributor_id) {
		$this->distributor_id = $distributor_id;
	}

	public function getActive() {
		return (boolean) $this->active;
	}
	public function setActive($active) {
		$this->active = (boolean) \intval($active);
	}

	public function getSet_as_default() {
		return (boolean) $this->set_as_default;
	}
	public function setSet_as_default($set_as_default) {
		$this->set_as_default = (boolean) \intval($set_as_default);
	}
	
	public function getParent_id() {
		return (int) $this->parent_id;
	}
	public function setParent_id($parent_id) {
		$this->parent_id = \intval($parent_id);
	}
	
	public function getTestlist_distributor_id() {
		return $this->testlist_distributor_id;
	}
	public function setTestlist_distributor_id($testlist_distributor_id) {
		$this->testlist_distributor_id = $testlist_distributor_id;
	}

}