<?php

class abmelderManager {

    public function __construct() {
        $this->dbh = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
    }

    public function cntAbmelder($hash, $mid) {
        
        $cnt = 0;
        $qry = ' SELECT COUNT(*) as anzahl '
                . ' FROM mandant_' . $mid
                . ' WHERE hash = :hash ';

        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':hash', $hash, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $cnt = $row['anzahl'];
        }

        return $cnt;
    }

    public function cntAbmelderAll($hashArr, $mid) {

        $i = 1;
        $cnt = 0;
        foreach ($hashArr as $h) {
            if ($i == 1) {
                $sql_h = " WHERE hash = '" . $h . "'";
            } else {
                $sql_h .= " OR hash = '" . $h . "'";
            }

            $i++;
        }

        $qry = ' SELECT COUNT(*) as anzahl '
                . ' FROM mandant_' . $mid
                . $sql_h;

        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $cnt = $row['anzahl'];
        }

        return $cnt;
    }

    public function getAbmelder($hash, $mid) {

        $qry = ' SELECT email '
                . ' FROM mandant_' . $mid
                . ' WHERE hash = :hash '
                . ' GROUP BY email ';

        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':hash', $hash, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();

        foreach ($result as $row) {
            $dataArr[] = $row['email'];
        }

        return $dataArr;
    }

    public function getAbmelderAll($hashArr, $mid) {

        $i = 1;
        foreach ($hashArr as $h) {
            if ($i == 1) {
                $sql_h = " WHERE hash = '" . $h . "'";
            } else {
                $sql_h .= " OR hash = '" . $h . "'";
            }

            $i++;
        }

        $qry = ' SELECT `email` '
                . ' FROM mandant_' . $mid
                . $sql_h;

        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();

        foreach ($result as $row) {
            $dataArr[] = $row['email'];
        }

        return $dataArr;
    }
    
    public function getAbmelderByDate($mid, $month, $year) {

        $dataArr = array();
        
        $qry = ' SELECT `email` '
                . ' FROM mandant_' . $mid
                . ' WHERE MONTH(`created_at`) = :month '
                . ' AND YEAR(`created_at`) = :year'
                . ' AND ( `ip` != "" OR `benutzer` > 0 ) '
                . ' GROUP BY `email` ';

        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':month', $month, PDO::PARAM_STR);
        $stmt->bindParam(':year', $year, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $dataArr[] = $row['email'];
        }

        return $dataArr;
    }

}

?>