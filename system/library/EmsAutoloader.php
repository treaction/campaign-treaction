<?php
class EmsAutoloader {
	/**
	 * init
	 * 
	 * @return	void
	 */
	public static function init() {
		try {
			\spl_autoload_register(
				array(
					__CLASS__, 'loadEntity'
				)
			);
			
			\spl_autoload_register(
				array(
					__CLASS__, 'loadUtils'
				)
			);
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
	
	/**
	 * startAndCheckClientSession
	 * 
	 * return void
	 */
	public static function startAndCheckClientSession() {
		\session_start();
		
		if (\strlen($_SESSION['mandant']) === 0) {
			\header('Location: https://campaign-treaction.de/meta/login.php');
			exit();
		}
	}
	
	/**
	 * initErrorReporting
	 * 
	 * @return void
	 */
	public static function initErrorReporting() {
		\error_reporting(null);
		\ini_set('display_errors', false);
		
		if (isset($_SESSION['benutzer_id']) 
			&& (int) $_SESSION['benutzer_id'] === \BaseUtils::getDebugUserId()
		) {
			\error_reporting(E_ALL);
			\ini_set('display_errors', true);
		}
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              load - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * loadEntity
	 * 
	 * @param string $class
	 * return void
	 */
	protected static function loadEntity($class) {
		if ((\preg_match('/Entity$/ui', $class))) {
			if ((\file_exists(\DIR_Entity . $class . '.php')) === true) {
				require_once(\DIR_Entity . $class . '.php');
			}
		}
	}

	/**
	 * loadUtils
	 * 
	 * @param string $class
	 * return void
	 */
	protected static function loadUtils($class) {
		if ((\preg_match('/Utils$/ui', $class))) {
			if ((\file_exists(\DIR_Utils . $class . '.php')) === true) {
				require_once(\DIR_Utils . $class . '.php');
			}
		}
	}
}