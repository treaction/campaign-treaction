<?php
class mandant {
    public function __construct() {
        $this->dbh = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
    }
    
    public function getMandantDataAll() {
        $qry = 'SELECT *' . 
            ' FROM `mandant_asp`'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $mArr = array();
        foreach ($result as $row) {
            $mArr[] = array(
                'm_id' => $row['m_id'],
                'asp_id' => $row['asp_id'],
                'm_login' => $row['m_login'],
                'api_user' => $row['api_user'],
                'pw' => $row['pw'],
                'verteiler' => $row['verteiler'],
                'blacklist_verteiler' => $row['blacklist_verteiler'],
                'active' => $row['active']
            );
        }

        return $mArr;
    }

    public function getMandantAbkz($mID) {
        $qry = 'SELECT `abkz`' . 
            ' FROM `mandant`' . 
            ' WHERE `id` = :mID'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row['abkz'];
    }

    public function getMandantASP($mID) {
        $qry = 'SELECT `asp_id`' . 
            ' FROM `mandant_asp`' . 
            ' WHERE `m_id` = :mID'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $mArr = array();
        foreach ($result as $row) {
            $mArr[] = $row['asp_id'];
        }

        return $mArr;
    }

    public function getMandantMASP($mID) {
        $qry = 'SELECT `masp_id`' . 
            ' FROM `mandant_asp`' . 
            ' WHERE `m_id` = :mID' . 
                ' AND `active` = "1"'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $mArr = array();
        foreach ($result as $row) {
            $mArr[] = $row['masp_id'];
        }

        return $mArr;
    }

    public function getASPData($mID, $aspID) {
        $qry = 'SELECT *' . 
            ' FROM `mandant_asp`' . 
            ' WHERE `m_id` = :mID' . 
                ' AND `asp_id` = :aspID'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt->bindParam(':aspID', $aspID, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $mArr = array(
            'm_login' => $row['m_login'],
            'api_user' => $row['api_user'],
            'pw' => $row['pw'],
            'verteiler' => $row['verteiler'],
            'blacklist_verteiler' => $row['blacklist_verteiler'],
            'active' => $row['active']
        );

        return $mArr;
    }

    public function getMandantASPname($aspID) {
        $qry = 'SELECT *' . 
            ' FROM `versandsystem`' . 
            ' WHERE `id` = :aspID'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':aspID', $aspID, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $mArr = array(
            'asp' => $row['asp'],
            'asp_abkz' => $row['asp_abkz']
        );

        return $mArr;
    }

    public function getMandantMASPname($maspID) {
        $qry = 'SELECT *' . 
            ' FROM `versandsystem` as `v`' . 
                ' INNER JOIN `mandant_asp` as `m` ON `v`.`id` = `m`.`asp_id`' . 
            ' WHERE `m`.`masp_id` = :maspID'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':maspID', $maspID, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $mArr = array(
            'asp' => $row['asp'],
            'asp_abkz' => $row['asp_abkz']
        );

        return $mArr;
    }

    public function getMandantID($abkz) {
        $qry = 'SELECT `id`' . 
            ' FROM `mandant`' . 
            ' WHERE `abkz` = :abkz'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':abkz', $abkz, PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row['id'];
    }

    public function getMandantData($mID) {
        $qry = 'SELECT * ' . 
            ' FROM `mandant`' . 
            ' WHERE `id` = :mID'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $m = array(
            'Mandant' => $row['mandant']
        );

        return $m;
    }

    public function getAllUsers($mID) {
        $qry = 'SELECT *' . 
            ' FROM `benutzer`' . 
            ' WHERE `mid` = :mID' . 
            ' ORDER BY `vorname` ASC'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = array(
                'benutzer_id' => $row['benutzer_id'],
                'benutzer_name' => $row['benutzer_name'],
                'anrede' => $row['anrede'],
                'vorname' => $row['vorname'],
                'nachname' => $row['nachname'],
                'email' => $row['email'],
                'tab_config' => $row['tab_config'],
                'abteilung' => $row['abteilung']
            );
        }

        return $dataArr;
    }

    public function getUserDataById($uID) {
        $qry = 'SELECT *' . 
            ' FROM `benutzer`' . 
            ' WHERE `benutzer_id` = :uID'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':uID', $uID, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $dataArr = array(
            'benutzer_id' => $row['benutzer_id'],
            'benutzer_name' => $row['benutzer_name'],
            'anrede' => $row['anrede'],
            'vorname' => $row['vorname'],
            'nachname' => $row['nachname'],
            'email' => $row['email'],
            'tab_config' => $row['tab_config'],
            'abteilung' => $row['abteilung']
        );

        return $dataArr;
    }

    public function getAllUsersByAbteilung($mID, $abteilung) {
        $qry = 'SELECT *' . 
            ' FROM `benutzer`' . 
            ' WHERE `active` = "1"'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $zugang_mandantArr = explode(',', $row['zugang_mandant']);
            $abt = $row['abteilung'];

            if (in_array($mID, $zugang_mandantArr) && $abt == $abteilung) {
                $dataArr[] = array(
                    'benutzer_id' => $row['benutzer_id'],
                    'benutzer_name' => utf8_decode($row['benutzer_name']),
                    'anrede' => $row['anrede'],
                    'vorname' => utf8_decode($row['vorname']),
                    'nachname' => utf8_decode($row['nachname']),
                    'email' => $row['email'],
                    'tab_config' => $row['tab_config'],
                    'mandant' => utf8_decode($row['mandant']),
                    'abteilung' => utf8_decode($row['abteilung'])
                );
            }
        }

        return $dataArr;
    }

    public function getAllUsersByMandant($mID) {
        $qry = 'SELECT *' . 
            ' FROM `benutzer`' . 
            ' WHERE `active` = "1"'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $zugang_mandantArr = explode(',', $row['zugang_mandant']);

            if (in_array($mID, $zugang_mandantArr)) {
                $dataArr[] = array(
                    'benutzer_id' => $row['benutzer_id'],
                    'benutzer_name' => utf8_decode($row['benutzer_name']),
                    'anrede' => $row['anrede'],
                    'vorname' => utf8_decode($row['vorname']),
                    'nachname' => utf8_decode($row['nachname']),
                    'email' => $row['email'],
                    'tab_config' => $row['tab_config'],
                    'abteilung' => utf8_decode($row['abteilung']),
                    'mandant' => utf8_decode($row['mandant'])
                );
            }
        }

        return $dataArr;
    }
}
?>