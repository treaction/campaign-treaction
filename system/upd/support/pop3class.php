<?php 

/* 
 * Copyright (C) 1998, Manuel Lemos (mlemos@acm.org) 
 *  
 * Permission to use and modify this software and its 
 * documentation for any purpose other than its incorporation 
 * into a commercial product is hereby granted without fee, 
 * as long as the author is notified that this piece of software 
 * is being used in other applications. 
 * Permission to copy and distribute this software and its 
 * documentation only for non-commercial use is also granted 
 * without fee, provided, however, that the above copyright 
 * notice appear in all copies, that both that copyright notice 
 * and this permission notice appear in supporting documentation. 
 * The author makes no representations about the suitability 
 * of this software for any purpose.  It is provided ``as is'', 
 * without express or implied warranty. 
 */ 

class pop3_class 
{ 
 var $hostname= ""; 
 var $port=110; 

  /* Private variables - DO NOT ACCESS */ 

 var $connection=0; 
 var $state= "DISCONNECTED"; 
 var $greeting= ""; 
 var $must_update=0; 


  /* Private methods - DO NOT CALL */ 

 Function GetLine() 
 { 
  for($line= "";;) 
  { 
   if(@feof($this->connection)) 
    return(0); 
   $line.=fgets($this->connection,100); 
   $length=strlen($line); 
   if($length>=2 && substr($line,$length-2,2)== "\r\n") 
    return(substr($line,0,$length-2)); 
  } 
 } 

 Function PutLine($line) 
 { 
  return(fputs($this->connection, "$line\r\n")); 
 } 

 Function OpenConnection() 
 { 
  if($this->hostname== "") 
   return( "2 (Es wurde kein g&uuml;ltiger Servername angegeben)"); 
  switch(($this->connection=fsockopen($this->hostname,$this->port))) 
  { 
   case -3: 
    return("3 (Socket konnte nicht erzeugt werden)"); 
   case -4: 
    return("4 (DNS-Lookup f&uuml;r Host \"$hostname\" fehlgeschlagen)"); 
   case -5: 
    return("5 (Verbindung unterbrochen oder wegen Zeit&uuml;berschreitung beendet)"); 
   case -6: 
    return("6 (Aufruf von fdopen() fehlgeschlagen)"); 
   case -7: 
    return("7 (Aufruf von setvbuf() fehlgeschlagen)"); 
   default: 
    return(""); 
  } 
 } 

 Function CloseConnection() 
 { 
  if($this->connection!=0) 
  { 
   fclose($this->connection); 
   $this->connection=0; 
  } 
 } 

  /* Public methods */ 

  /* Open method - set the object variable $hostname to the POP3 server address. */ 

 Function Open() 
 { 
  if($this->state!= "DISCONNECTED") 
   return( "1 (Die Verbindung wurde bereits erfolgreich ge&ouml;ffnet)"); 
  if(($error=$this->OpenConnection())!= "") 
   return($error); 
  $this->greeting=$this->GetLine(); 
  if(GetType($this->greeting)!= "string" 
  || strtok($this->greeting, " ")!= "+OK") 
  { 
   $this->CloseConnection(); 
   return( "3 (Der POP3 Server hat keine Begr&uuml;&szlig;ung &uuml;bermittelt)"); 
  } 
  $this->greeting=strtok( "\r\n"); 
  $this->must_update=0; 
  $this->state= "AUTHORIZATION"; 
  return( ""); 
 } 
  
  /* Close method - this method must be called at least if there are any 
     messages to be deleted */ 

 Function Close() 
 { 
  if($this->state== "DISCONNECTED") 
   return( "Es wurde keine Verbindung ge&ouml;ffnet"); 
  if($this->must_update) 
  { 
   if($this->PutLine( "QUIT")==0) 
    return( "Das <code>QUIT</code>-Kommando konnt nicht gesendet werden"); 
   $response=$this->GetLine(); 
   if(GetType($response)!= "string") 
    return( "Konnte die Quittung des letzten Kommandos nicht empfangen"); 
   if(strtok($response, " ")!= "+OK") 
    return( "Konnte die folgende Verbindung nicht quittieren: ".strtok("\r\n")); 
  } 
  $this->CloseConnection(); 
  $this->state= "DISCONNECTED"; 
  return( ""); 
 } 

  /* Login method - pass the user name and password of POP account.  Set 
     $apop to 1 or 0 wether you want to login using APOP method or not.  */ 

 Function Login($user,$password,$apop)  { 
  	if($this->state!= "AUTHORIZATION") return( "Verbindung ist nicht im <code>AUTHORIZATION</code>-Status"); 
  	if($apop) { 
		if($this->PutLine( "APOP $user ".md5($this->greeting.$password))==0) return( "Konnte das <code>APOP</code>-Kommando nicht senden"); 
	    $response=$this->GetLine(); 
		if(GetType($response)!= "string") return( "Habe keine Antwort auf <code>APOP</code>-Login empfangen"); 
		if(strtok($response, " ")!= "+OK") return( "<code>APOP</code>-Login fehlgeschlagen: ".strtok( "\r\n")); 
	}  else  { 
		if($this->PutLine( "USER $user")==0) return( "Konnte das <code>USER</code>-Kommando nicht senden"); 
		$response=$this->GetLine(); 
		if(GetType($response)!= "string") return( "Konnte keine Antwort auf User-Login empfangen"); 
		if(strtok($response, " ")!= "+OK") return( "<code>USER</code>-Fehler: ".strtok( "\r\n")); 
		if($this->PutLine( "PASS $password")==0) return( "Konnte das <code>PASS</code>-Kommando nicht senden"); 
		$response=$this->GetLine(); 
		if(GetType($response)!= "string") return( "Konnte auf das <code>PASS</code>-Kommando keine Antwort empfangen"); 
	    if(strtok($response, " ")!= "+OK") return( "Kennwort-Fehler: ".strtok( "\r\n")); 
	} 
	$this->state= "TRANSACTION"; 
  	return( ""); 
 } 

  /* Statistics method - pass references to variables to hold the number of 
     messages in the mail box and the size that they take in bytes.  */ 

 Function Statistics($messages,$size) 
 { 
  if($this->state!= "TRANSACTION") 
   return( "Die Verbindung ist nicht im <code>TRANSACTION</code>-Status"); 
  if($this->PutLine( "STAT")==0) 
   return( "Konnte das <code>STAT</code>-Kommando nicht l&ouml;schen"); 
  $response=$this->GetLine(); 
  if(GetType($response)!= "string") 
   return( "Habe auf das <code>STAT</code>-Kommando keine Antwort erhalten"); 
  if(strtok($response, " ")!= "+OK") 
   return( "Die statistischen Informationen wurden nicht empfangen: ".strtok( "\r\n")); 
  $messages=strtok( " "); 
  $size=strtok( " "); 
  return( ""); 
 } 

  /* ListMessages method - the $message argument indicates the number of a 
     message to be listed.  If you specify an empty string it will list all 
     messages in the mail box.  The $unique_id flag indicates if you want 
     to list the each message unique identifier, otherwise it will 
     return the size of each message listed.  If you list all messages the 
     result will be returned in an array. */ 

 Function ListMessages($message,$unique_id) 
 { 
  if($this->state!= "TRANSACTION") 
   return( "Die Verbindung ist nicht im <code>TRANSACTION</code>-Status"); 
  if($unique_id) 
   $list_command= "UIDL"; 
  else 
   $list_command= "LIST"; 
  if($this->PutLine( "$list_command $message")==0) 
   return( "Konnte das Kommando <code>$list_command</code> nicht ausf&uuml;hren"); 
  $response=$this->GetLine(); 
  if(GetType($response)!= "string") 
   return( "Konnte die Liste der Nachrichten nicht empfangen"); 
  if(strtok($response, " ")!= "+OK") 
   return( "Habe keine Antwort auf <code>LIST</code> empfangen: ".strtok( "\r\n")); 
  if($message== "") 
  { 
   for($messages=array();;) 
   { 
    $response=$this->GetLine(); 
    if(GetType($response)!= "string") 
     return( "Konnte die Liste der Nachrichten nicht empfangen");
    if($response== ".") 
     break; 
    $message=intval(strtok($response, " ")); 
    if($unique_id) 
     $messages[$message]=strtok( " "); 
    else 
     $messages[$message]=intval(strtok( " ")); 
   } 
   return($messages); 
  } 
  else 
  { 
   $message=intval(strtok( " ")); 
   return(intval(strtok( " "))); 
  } 
 } 

  /* RetrieveMessage method - the $message argument indicates the number of 
     a message to be listed.  Pass a reference variables that will hold the 
     arrays of the $header and $body lines.  The $lines argument tells how 
     many lines of the message are to be retrieved.  Pass a negative number 
     if you want to retrieve the whole message. */ 

 Function RetrieveMessage($message,$headers,$body,$lines) 
 { 
  if($this->state!= "TRANSACTION") 
   return( "Die Verbindung ist nicht im <code>TRANSACTION</code>-Status"); 
  if($lines<0) 
  { 
   $command= "RETR"; 
   $arguments= "$message"; 
  } 
  else 
  { 
   $command= "TOP"; 
   $arguments= "$message $lines"; 
  } 
  if($this->PutLine( "$command $arguments")==0) 
   return( "Konnte das Kommando <code>$command</code> nicht senden"); 
  $response=$this->GetLine(); 
  if(GetType($response)!= "string") 
   return( "Habe keine Antwort auf das letzte Kommando erhalten"); 
  if(strtok($response, " ")!= "+OK") 
   return( "Konnte die folgende Nachricht nicht abrufen: ".strtok( "\r\n")); 
  for($headers=$body=array(),$line=0;;$line++) 
  { 
   $response=$this->GetLine(); 
   if(GetType($response)!= "string") 
    return( "Konnte nicht Nachricht nicht abrufen"); 
   switch($response) 
   { 
    case  ".": 
     return( ""); 
    case  "": 
     break 2; 
    default: 
     if(substr($response,0,1)== ".") 
      $response=substr($response,1,strlen($response)-1); 
     break; 
   } 
   $headers[$line]=$response; 
  } 
  for($line=0;;$line++) 
  { 
   $response=$this->GetLine(); 
   if(GetType($response)!= "string") 
    return( "Konnte nicht Nachricht nicht abrufen"); 
   switch($response) 
   { 
    case  ".": 
     return( ""); 
    default: 
     if(substr($response,0,1)== ".") 
      $response=substr($response,1,strlen($response)-1); 
     break; 
   } 
   $body[$line]=$response; 
  } 
  return( ""); 
 } 

  /* DeleteMessage method - the $message argument indicates the number of 
     a message to be marked as deleted.  Messages will only be effectively 
     deleted upon a successful call to the Close method. */ 

 Function DeleteMessage($message) 
 { 
  if($this->state!= "TRANSACTION") 
     return( "Die Verbindung ist nicht im <code>TRANSACTION</code>-Status"); 
  if($this->PutLine( "DELE $message")==0) 
   return( "Konnte das <code>DELE</code>-Kommando nicht l&ouml;schen"); 
  $response=$this->GetLine(); 
  if(GetType($response)!= "string") 
   return( "Habe auf das L&ouml;schkommando keine Antwort erhalten"); 
  if(strtok($response, " ")!= "+OK") 
   return( "Die folgende Nachricht konnte nicht gel&ouml;scht werden: ".strtok( "\r\n")); 
  $this->must_update=1; 
  return( ""); 
 } 

  /* ResetDeletedMessages method - Reset the list of marked to be deleted 
     messages.  No messages will be marked to be deleted upon a successful 
     call to this method.  */ 

 Function ResetDeletedMessages() 
 { 
  if($this->state!= "TRANSACTION") 
     return( "Die Verbindung ist nicht im <code>TRANSACTION</code>-Status"); 
  if($this->PutLine( "RSET")==0) 
   return( "Konnte das <code>RSET</code>-Kommando nicht l&ouml;schen"); 
  $response=$this->GetLine(); 
  if(GetType($response)!= "string") 
   return( "Habe auf das R&uuml;cksetzkommando keine Antwort erhalten"); 
  if(strtok($response, " ")!= "+OK") 
   return( "Konnte die folgende Nachricht nicht zur&uuml;cksetzen: ".strtok( "\r\n")); 
  $this->must_update=0; 
  return( ""); 
 } 

  /* IssueNOOP method - Just pings the server to prevent it auto-close the 
     connection after an idle timeout (tipically 10 minutes).  Not very 
     useful for most likely uses of this class.  It's just here for 
     protocol support completeness.  */ 

 Function IssueNOOP() 
 { 
  if($this->state!= "TRANSACTION") 
	return( "Die Verbindung ist nicht im <code>TRANSACTION</code>-Status"); 
  if($this->PutLine( "NOOP")==0) 
   return( "Konnte das <code>NOOP</code>-Kommando nicht senden"); 
  $response=$this->GetLine(); 
  if(GetType($response)!= "string") 
   return( "Habe auf das <code>NOOP</code>-Kommando keine Antwort erhalten"); 
  if(strtok($response, " ")!= "+OK") 
   return( "Konnte das <code>NOOP</code>-Kommando nicht anwenden: ".strtok( "\r\n")); 
  return( ""); 
 } 
}; 

/* ---- pop3.php3 class file ends here. ---- */ 

?> 
