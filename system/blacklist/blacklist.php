<?php
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */


$mandant = $_SESSION['mandant'];
$benutzer_id = $_SESSION['benutzer_id'];
$mandantID = $_SESSION['mID'];

# Domains, die nicht blacklistbar sind
$domainArr = array(
    'gmx', 'web', 'yahoo', 't-online', 'hotmail', 'freenet', 'aol', 'arcor', 'live', 'bluewin', 'aon', 'alice', 'versanet', 'msn', 'ewetel', 'gmail', 'googlemail'
);

// debug
$debugLogManager->logData('domainArr', $domainArr);

require('../db_connect.inc.php');
require('../db_blacklist_connect.inc.php');

$listMails = explode("\n", $_REQUEST['list']);
$notizen = strip_tags($_REQUEST['notiz_blacklist']);

$grund = strip_tags($_REQUEST['grund']);
$grund_new = strip_tags($_REQUEST['grund_new']);

if ($grund_new != '') {
    $grund = $grund_new;
}

$found_domain = $found_syntax = $found_syntax = array();

foreach ($listMails as $email) {
    $found = false;
    $email = trim($email);
    
    // debug
    $debugLogManager->logData('email', $email);
    
    /**
     * keine validierung
     * 
     * util::validEmail($email),
     * 
     * @see https://iosm.pixsoftware.de/browse/EMS-196
     */
    if (strlen($email) > 0) {
        if (strpos($email, '*@') !== false) {
            $domain = explode('@', $email);
            foreach ($domainArr as $d) {
                if (strpos($domain[1], $d . '.') !== false) {
                    $found = true;
                    $found_domain[] = $email;
                }
            }
        }
        
        $checkEmail_sql = mysql_query(
            'SELECT Email' 
                . ' FROM Blacklist' 
                . ' WHERE Email = "' . mysql_real_escape_string($email) . '"',
            $verbindung_bl
        );
        $checkEmailCnt = mysql_num_rows($checkEmail_sql);
        // debug
        $debugLogManager->logData('checkEmailCnt', $checkEmailCnt);

        if (intval($checkEmailCnt) === 1) {
            $found_email_exist[] = $email;
            $found = true;
        }

        if ($found !== true) {
            mysql_query(
                'REPLACE INTO Blacklist' 
                    . ' (Email, Datum, Grund, Notizen, Bearbeiter, mandant)' 
                    . ' VALUES ("' . $email . '", NOW(), "' . $grund . '", "' . $notizen . '", "' . $benutzer_id . '", "' . $mandantID . '")'
                ,
                $verbindung_bl
            );
            
            // debug
            $debugLogManager->logData('REPLACE', true);
        }
    } else {
        if ($email != '') {
            $found_syntax[] = $email;
        }
    }
}

// debug
$debugLogManager->logData('found_domain', $found_domain);
$debugLogManager->logData('found_syntax', $found_syntax);
$debugLogManager->logData('found_email_exist', $found_email_exist);

if (count($found_domain) > 0 
    || count($found_syntax) > 0 
    || count($found_email_exist) > 0
) {
    $domains = $syntax = $emailExist = '';
    
    foreach ($found_domain as $dom) {
        $domains .= $dom . " : Domain nicht blacklistbar\\n";
    }

    foreach ($found_syntax as $syn) {
        $syntax .= $syn . " : Emailsyntax ungueltig\\n";
    }

    foreach ($found_email_exist as $ex) {
        $emailExist .= $ex . " : Bereits auf der Blacklist\\n";
    }

    $_SESSION['error'] = "<script type='text/javascript'>alert(\"Folgende Adressen konnten nicht geblacklistet werden:\\n\\n" . $domains . $syntax . $emailExist . "\");</script>";
}
?>