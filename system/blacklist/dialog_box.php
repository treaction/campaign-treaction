<?php
header('Content-Type: text/html; charset=ISO-8859-1');

include('../db_blacklist_connect.inc.php');

$lastMonthUnix = time() - 2678400;
$lastMonth = date('Y-m-d', $lastMonthUnix);

$sql = mysql_query(
    'SELECT Grund, COUNT(*) as a' 
        . ' FROM Blacklist' 
        . ' WHERE Grund != ""' 
        . ' AND Datum >= "' . $lastMonth . '"'
        . ' GROUP BY Grund' 
        . ' ORDER BY a DESC' 
        . ' LIMIT 0,20'
    ,
    $verbindung_bl
);

while ($z = mysql_fetch_array($sql)) {
    $grundArr[] = $z['Grund'];
}
asort($grundArr);

$opt = '';
foreach ($grundArr as $grund) {
    $opt .= '<option value="' . $grund . '">' . $grund . '</option>';
}
?>
<table border="0" cellpadding="2">
    <tr>
        <td colspan="2" style="color:#333333">
            Bitte f&uuml;gen Sie die Email-Adressen, die in die Blacklist eingetragen werden sollen einfach untereinander ein (max. 5000).<br />z.b. abc@domain.de, *@domain.de, @domain.de
        </td>
    </tr>
    <tr>
        <td><span style="color:#9900FF;font-weight:bold;">Grund:</span></td>
        <td height="40" align="right">
            <select name="grund" id="grund" style="width:275px;">
                <option value=''>-- Top 20 / letzten Monat --</option>
                <?php echo $opt;?>
            </select>
        </td>
    </tr>
    <tr>
        <td><span style="color:#9900FF;font-weight:bold;">Neuer Grund:</span></td>
        <td height="40" align="right"><input type="text" name="grund_new" id="grund_new" style="width:275px;margin-right:4px" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <span style="color:#9900FF;font-weight:bold;">Notiz (optional):</span><br />
            <textarea name="notiz_blacklist" id="notiz_blacklist" style="width:325px;height:30px;border:1px solid #999999" class="textfeld"></textarea>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <br /><span style="color:#8e8901;font-weight:bold;">Email / Domain:</span><textarea name="list" id="list_blacklist" style="width:325px;height:150px;border:1px solid #8e8901" class="textfeld"></textarea>
        </td>
    </tr>
</table>