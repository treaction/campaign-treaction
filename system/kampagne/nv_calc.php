<?php
session_start();
header('Content-Type: text/html; charset=ISO-8859-1');
$mandant = $_SESSION['mandant'];
include('../db_connect.inc.php');
include('../library/util.php');
$kid = $_GET['kid'];
$oRatePrognose = $_GET['oRatePrognose'];
$kRatePrognose = $_GET['kRatePrognose'];

function zahl_format($zahl) {
	$zahl = number_format($zahl, 0,0,'.');
	return $zahl;
}



function passedH($date) {
	$date_1 = explode(' ',$date);
	$datum = explode('-', $date_1[0]);
	$jahr = $datum[0];
	$monat = $datum[1];
	$tag = $datum[2];
	
	$zeit = $date_1[1];
	$zeit_teile = explode(':', $date_1[1]);
	$h = $zeit_teile[0];
	$m = $zeit_teile[1];
	
	$heute = time();
	$v_datum = mktime($h,$m,0,$monat,$tag,$jahr);
	
	$passedHours = floor(($heute-$v_datum)/3600);
	
	if ($passedHours > 72) {
		$passedHours = "<span style='font-weight:bold;color:red;'>".$passedHours."</span>";
	}
	return $passedHours;
}

function newTime($date) {
	$dateParts = explode(' ',$date);
	$datePart = $dateParts[0];
	$timePart =  $dateParts[1];
	$timeParts = explode(':', $timePart);
	$timePart = ($timeParts[0]+2).":".$timeParts[1].":00";
	return $datePart." ".$timePart;
}

function datum_de($date_time) {
	$date_1 = explode(' ',$date_time);
	$datum = explode('-', $date_1[0]);
	$jahr = $datum[0];
	$monat = $datum[1];
	$tag = $datum[2];
	
	$zeit = $date_1[1];
	$zeit_teile = explode(':', $date_1[1]);
	$h = $zeit_teile[0];
	$m = $zeit_teile[1];
	$zeit = $h.":".$m;
	$de = $tag.".".$monat.".".$jahr;
	
	$dat_zeit = $de." - ".$zeit;
	return $dat_zeit;
}

$k_daten = mysql_query("SELECT * FROM $kampagne_db WHERE k_id='$kid'",$verbindung);
$k_zeile = mysql_fetch_array($k_daten);
$gebucht = $k_zeile['gebucht'];
$mailtyp = $k_zeile['mailtyp'];
$vorgabe_openings = $k_zeile['vorgabe_o'];
$vorgabe_klicks = $k_zeile['vorgabe_k'];
if ($mailtyp == 't') {$vorgabe_openings = 0;}
if ($vorgabe_openings == '') {$vorgabe_openings = 0;}
if ($vorgabe_klicks == '') {$vorgabe_klicks = 0;}

$agentur = $k_zeile['agentur'];
$k_name = $k_zeile['k_name'];
$k_datum = $k_zeile['datum'];
$asp = $k_zeile['versandsystem'];
if ($asp == 'BM') {
			$k_datum = newTime($k_datum);
		}
		
$k_daten_global = mysql_query("SELECT 
									SUM(versendet) as v, 
									SUM(openings_all) as oa, 
									SUM(klicks_all) as ka,
									SUM(leads) as l 
								FROM $kampagne_db
								WHERE nv_id='$kid'",$verbindung);
								
$k_zeile_global = mysql_fetch_array($k_daten_global);
$versendet_global = $k_zeile_global['v'];
$openings_global = $k_zeile_global['oa'];
$klicks_global = $k_zeile_global['ka'];
$leads_global = $k_zeile_global['l'];

$oRateNow = round((($openings_global/$gebucht)*100),2);
$kRateNow = round((($klicks_global/$gebucht)*100),2);

$q1 = $versendet_global-$gebucht;
$usq = round((($q1/$gebucht)*100),1);

		if ($q1 > 0) {	
			$usqTxt = "<span style='color:red;'>".$usq."%</span>";
		} else {
			$usqTxt = "<span style='color:#7D960B;'>".$usq."%</span>";
		}
		

$h_vergangen = passedH($k_datum);

if ($h_vergangen < 0) {
	$h_vergangen = 0;
} else {
	$h_vergangen = passedH($k_datum);
}

$rest_ = $gebucht*3;
$rest_ = $rest_-$versendet_global;
if ($rest_ < 0) {
	$rest_ = 0;
}
?>
<form id="nv_calc" name="nv_calc">
<input type="hidden" id="kidnv" value="<?php print $kid; ?>" />
<input type="hidden" id="mime" value="<?php print $mailtyp; ?>" />
<input type="hidden" id="agentur" value="<?php print $agentur; ?>" />
<input type="hidden" id="kamp_name" value="<?php print $k_name; ?>" />
<input type="hidden" id="calc_gebucht" value="<?php print $gebucht; ?>" />
<input type="hidden" id="calc_versendet" value="<?php print $versendet_global; ?>" />
<input type="hidden" id="openings_global" value="<?php print $openings_global; ?>" />
<input type="hidden" id="klicks_global" value="<?php print $klicks_global; ?>" />
<input type="hidden" id="vorgabe_openings" value="<?php print $vorgabe_openings; ?>" />
<input type="hidden" id="vorgabe_klicks" value="<?php print $vorgabe_klicks; ?>" />
<input type="hidden" id="restMenge" value="<?php print $rest_; ?>" />

			<table class='transp'>
				<tr>
					<td class='details_hd_info'></td>
					<td class='details_hd_info' colspan="2" style="text-align:left">Mailingdaten</td>
				</tr>
				<tr>
					<td class='details_info' style="vertical-align:top">Kampagne</td>
					<td class='details2_info' id="k_name_td" colspan="2"><?php print "<b>".util::cutString($k_name,20)."</b>, ".util::cutString($agentur,10); ?></td>
				</tr>
				<tr>
					<td class='details_info2'>Versanddatum</td>
					<td class='details2_info2' colspan="2"><?php print datum_de($k_datum); ?></td>
				</tr>
                <tr>
					<td class='details_info'>h nach Versand</td>
					<td class='details2_info' colspan="2"><?php print $h_vergangen;?> Stunden vergangen</td>
				</tr>
				<tr>
					<td class='details_info2'>Gebucht</td>
					<td class='details2_info2' colspan="2"><?php print zahl_format($gebucht); ?></td>
				</tr>
				<tr>
					<td class='details_info'>Versendet</td>
					<td class='details2_info' colspan="2"><?php print zahl_format($versendet_global); ?></td>
				</tr>
				<tr>
					<td class='details_info2'>&Uuml;SQ</td>
					<td class='details2_info2' colspan="2"><?php print $usqTxt; ?></td>
				</tr>
                <tr>
					<td class='details_info' valign="top">Vorgaben</td>
					<td class='details2_info' colspan="2">&Ouml;-Rate: <?php print $vorgabe_openings; ?>%, K-Rate: <?php print $vorgabe_klicks; ?>%</td>
				</tr>
				<tr>
					<td class='details_hd_info'></td>
					<td class='details_hd_info' style="text-align:left;">&Ouml;ffnungsrate</td>
					<td class='details_hd_info' style="text-align:left;padding-right:10px;">Klickrate</td>
				</tr>
				<tr>
					<td class='details_info'>Aktuell</td>
					<td class='details2_info'><input type="text" name="ok" id="ok" style="width:30px;text-align:right;background-color:#F2F2F2;border:0px" value="<?php print $oRateNow; ?>" readonly="readonly"/><span id="okperc">%</span></td>
					<td class='details2_info'><input type="text" name="kk" id="kk" style="width:30px;text-align:right;background-color:#F2F2F2;border:0px" value="<?php print $kRateNow; ?>" readonly="readonly"/>%</td>
				</tr>
				<tr id="tr_prognose">
					<td class='details_info'>Prognose</td>
					<td class='details2_info'><input type="text" name="op" id="op" style="width:30px;text-align:right;background-color:#F2F2F2;border:0px" value="<?php print $oRatePrognose; ?>" readonly="readonly"/><span id="opperc">%</span></td>
					<td class='details2_info'><input type="text" name="kp" id="kp" style="width:30px;text-align:right;background-color:#F2F2F2;border:0px" value="<?php print $kRatePrognose; ?>" readonly="readonly" />%</td>
				</tr>
				<tr>
					<td class='details_info2'>Wunschrate</td>
					<td class='details2_info2'><input type="text" name="oz" id="oz" onclick="this.value=''" style="width:30px;text-align:right;background-color:#FFFFCC" value="" /> <span id="ozperc">%</span></td>
					<td class='details2_info2'><input type="text" name="kz" id="kz" onclick="this.value=''" style="width:30px;text-align:right;background-color:#FFFFCC" value=""  /> %</td>
				</tr>
				<tr>
					<td class='details_info'>Volumen</td>
					<td class='details2_info' colspan="2" style="text-align:center"><input type="text" name="vz" id="vz" onclick="this.value=''" value="" style="width:75px;text-align:right;background-color:#FFFFCC" /></td>
				</tr>
				<tr>
					<td class='details_info2'>&Uuml;SQ</td>
					<td class='details2_info2' colspan="2" style="text-align:center"><div id="calc_ousq" style="float:left;margin-left:80px;margin-top:5px;"><?php print $usqTxt; ?></div> <input type="button" style="float:right;" onclick="calc_rest();return false;" class="negative" value="200%" /></td>
				</tr>
				</tr>
				<tr>
					<td class='details_info'>Aktion</td>
					<td class='details2_info' colspan="2" style="text-align:center"><input type="button" id="oNVcalc" onclick="add_nv();" value="NV anlegen" /></td>
				</tr>
			</table>
</form>
<script type="text/javascript">
document.nv_calc.reset();

o_prognose = document.getElementById('op').value;
if (o_prognose == '') {
	document.getElementById('tr_prognose').style.display = "none";;
}

function add_nv() {
	YAHOO.example.container.calc.hide();
	YAHOO.example.container.dialog1.show();
	kamp_name = document.getElementById('kamp_name').value;
	agentur = document.getElementById('agentur').value;
	kidnv = document.getElementById('kidnv').value;
	menge = document.getElementById('vz').value;
	document.getElementById('k_hd').innerHTML = "Nachversand / Split hinzuf&uuml;gen";
	document.getElementById('hd_sublabel').innerHTML = "<span style='color:#333333;font-weight:bold'>"+agentur+"</span>, "+kamp_name;
	setRequestK('Module/Campaigns/View/Campaign/newOrEdit.php?kid='+kidnv+'&action=add_nv&mailing=NV&menge='+menge);
}
			
function Trenner(number) {
	number = '' + number;
	if (number.length > 3) {
	var mod = number.length % 3;
	var output = (mod > 0 ? (number.substring(0,mod)) : '');
	for (i=0 ; i < Math.floor(number.length / 3); i++) {
	if ((mod == 0) && (i == 0))
	output += number.substring(mod+ 3 * i, mod + 3 * i + 3);
	else
	// hier wird das Trennzeichen festgelegt mit '.'
	output+= '.' + number.substring(mod + 3 * i, mod + 3 * i + 3);
	}
	return (output);
}
	else return number;
}

gebucht = document.getElementById('calc_gebucht').value;
versendet = document.getElementById('calc_versendet').value;
o = document.getElementById('openings_global').value;
k = document.getElementById('klicks_global').value;
vorgabe_klicks = document.getElementById('vorgabe_klicks').value;
vorgabe_openings = document.getElementById('vorgabe_openings').value;
vz = document.getElementById('vz').value;
mime = document.getElementById('mime').value;

opRate = document.getElementById('op').value;
kpRate = document.getElementById('kp').value;

if (mime == 't') {
	document.getElementById('ok').style.display = "none";
	document.getElementById('op').style.display = "none";
	document.getElementById('oz').style.display = "none";
	document.getElementById('okperc').style.display = "none";
	document.getElementById('opperc').style.display = "none";
	document.getElementById('ozperc').style.display = "none";
}




vz = vz.replace(".","");
var newVZ = parseInt(vz);
			
		var ql;
		var gebucht;
		var oz_result;
		orate = (o/gebucht)*100;
		krate = (k/gebucht)*100;
		
		orate = (orate.toFixed(2));
		krate = (krate.toFixed(2));
		
		if (orate=='') {orate=0;}
		if (krate=='') {krate=0;}
		
		
		if (kpRate > 0) {
			orate = opRate;
			krate = kpRate;
			o = gebucht*(orate/100);
			o = (o.toFixed(0));
			k = gebucht*(krate/100);
			k = (k.toFixed(0));
		}
		
		document.getElementById('oz').onkeyup = oz_calc;
		document.getElementById('kz').onkeyup = kz_calc;
		document.getElementById('vz').onkeyup = vz_calc;
		
		function usq_calc(newVZ) {
			q1 = parseInt(versendet)-parseInt(gebucht)+parseInt(newVZ);
			
			if (q1 > 0) {
				usq = (q1/gebucht)*100;
				usq = (usq.toFixed(0));
			} else {
				usq = 0;
			}
			
			var	usqColor = "";
			if (usq >= 200) {
				usqColor = "red;font-weight:bold";	
			} 
			
			document.getElementById('calc_ousq').innerHTML = "<span style='color:"+usqColor+"'>"+usq+"%</span>";
		}
		
		
		function calc_rest() {
			rest_menge = document.getElementById('restMenge').value;
			rest_menge = rest_menge.replace(".","");
			document.getElementById('vz').value = rest_menge;
			vz_calc();
		}
		
		function vz_calc() {
			vz = document.getElementById('vz').value;
			vz = vz.replace(".","");
			newVZ = parseInt(vz);
			
			if (newVZ > 0) {
				kz_calc(newVZ);
				oz_calc(newVZ);
				usq_calc(newVZ);
			}
			
		}
		
		
		function oz_calc(newVZ) {

			var oz_result;
			oz = document.getElementById('oz').value;
			oz = oz.replace(/,/,"\.");
						
			ozmr = gebucht*(oz/100);
			ozm = ozmr-o;
			oz_faktor = versendet/o;
			oz_result = oz_faktor*ozm;
			oz_result = (oz_result.toFixed(2));
			
			if (oz_result<=0) {oz_result=0;}

			if (newVZ > 0) {
				vz_faktor = o/versendet;
				o_new = vz_faktor*(parseInt(newVZ)+parseInt(versendet));
				new_oz = (o_new/gebucht)*100;
				new_oz = (new_oz.toFixed(0));
			} else {
				newVZ = oz_result;
				newVZ = Math.round(newVZ/1000)*1000;
				document.getElementById('vz').value= Trenner(newVZ);
			}
			
			
			usq_calc(newVZ);
			reCalculate("o",newVZ);
			oz_result = Trenner(oz_result);
		}
		
		function kz_calc(newVZ) {
			var kz_result;
			kz = document.getElementById('kz').value;
			kz = kz.replace(/,/,"\.");

			kzmr = gebucht*(kz/100);
			kzm = kzmr-k;
			kz_faktor = versendet/k;
			kz_result = kz_faktor*kzm;
			kz_result = (kz_result.toFixed(0));
			
			if (kz_result<=0) {kz_result=0;}
			
			if (newVZ > 0) {
				vz_faktor = k/versendet;
				k_new = vz_faktor*(parseInt(newVZ)+parseInt(versendet));
				new_kz = (k_new/gebucht)*100;
				new_kz = (new_kz.toFixed(0));
			} else {
				newVZ = kz_result;
				newVZ = Math.round(newVZ/1000)*1000;
				document.getElementById('vz').value=Trenner(newVZ);
			}
			
			
			usq_calc(newVZ);
			reCalculate("k",newVZ);
			kz_result = Trenner(kz_result);
		}
		
		function reCalculate(typ,n) {
			if (typ == 'o') {
				vz_faktor = k/versendet;
				k_new = vz_faktor*(parseInt(n)+parseInt(versendet));
				new_kz = (k_new/gebucht)*100;
				new_kz = (new_kz.toFixed(2));
				document.getElementById('kz').value=new_kz;
			} else {
				vz_faktor = o/versendet;
				o_new = vz_faktor*(parseInt(n)+parseInt(versendet));
				new_oz = (o_new/gebucht)*100;
				new_oz = (new_oz.toFixed(2));
				document.getElementById('oz').value=new_oz;
			}
			
			usq_calc(n);
		}
		
</script>