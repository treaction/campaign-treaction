<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */
/* @var $swiftMailerWebservice \SwiftMailerWebservice */


$contactPersonId = isset($_POST['contactPerson']['ap_id']) ? \intval($_POST['contactPerson']['ap_id']) : 0;
if ($contactPersonId === 0) {
	throw new \InvalidArgumentException('no contactPersonId!');
}

/**
 * deleteContactPersonDataById
 * 
 * debug
 */
$deleteContactPersonResult = $campaignManager->deleteContactPersonDataById($contactPersonId);
$debugLogManager->logData('deleteContactPersonResult', $deleteContactPersonResult);

if ((boolean) $deleteContactPersonResult === true) {
	/**
	 * getCampaignsDataItemsByQueryParts
	 * 
	 * debug
	 */
	$campaignsDataArray = $campaignManager->getCampaignsDataItemsByQueryParts(
		array(
			'SELECT' => 'k_id, nv_id, k_name',
			'WHERE' => array(
				'apId' => array(
					'sql' => '`ap_id`',
					'value' => $contactPersonId,
					'comparison' => 'integerEqual'
				)
			)
		),
		\PDO::FETCH_ASSOC
	);
	$debugLogManager->logData('campaignsDataArray', $campaignsDataArray);
	if (\count($campaignsDataArray) > 0) {
		$campaignUpdateResultDataArray = $failedRecipients = array();
		
		$campaignUpdateDataArray = array(
			'ap_id' => array(
				'value' => 0,
				'dataType' => \PDO::PARAM_INT
			),
		);
		foreach ($campaignsDataArray as $campaignItem) {
			/**
			 * updateCampaignAndAddLogItem
			 */
			$campaignUpdateResultDataArray[$campaignItem['k_id']] = $campaignManager->updateCampaignAndAddLogItem(
				(int) $campaignItem['k_id'],
				(int) $campaignItem['nv_id'],
				(int) $_SESSION['benutzer_id'],
				15,
				0,
				$campaignUpdateDataArray
			);
		}
		$debugLogManager->logData('campaignUpdateResultDataArray', $campaignUpdateResultDataArray);
		
		/**
		 * sendUserInfoEmail
		 * 
		 * debug
		 */
		$emailMessage = 'Bei folgende Kampagnen wurde der Ansprechpartner gel�scht:' . \chr(13) . \chr(13);
		foreach ($campaignsDataArray as $campaignItem) {
			$emailMessage .= 'KampagneId: ' . (int) $campaignItem['k_id'] . ', KampagneName: ' . \htmlspecialchars($campaignItem['k_name']) . \chr(13);
		}
		
		/**
		 * sendUserInfoEmail
		 * 
		 * debug
		 */
		$mailerSendResults = \sendUserInfoEmail(
			$swiftMailerWebservice,
			$debugLogManager,
			\utf8_encode($emailMessage),
			\utf8_encode('Ems Kampagnen aktualisierungen!'),
			$failedRecipients
		);
		$debugLogManager->logData('mailerSendResults', $mailerSendResults);
		$debugLogManager->logData('failedRecipients', $failedRecipients);
	}
	
	/**
	 * addEmsActionLogItem
	 * 
	 * create EmsActionLogEntity
	 * 
	 * debug
	 */
	$emsActionLogEntity = new \EmsActionLogEntity();
	$emsActionLogEntity->setClient_id($_SESSION['mID']);
	$emsActionLogEntity->setUser_id($_SESSION['benutzer_id']);
	$emsActionLogEntity->setAction_id(15);
	$emsActionLogEntity->setRecord_id($contactPersonId);
	$emsActionLogEntity->setRecord_table($campaignManager->getContactPersonTable());
	$emsActionLogEntity->setLog_data(
		array(
			'campaignsDataArray' => $campaignsDataArray
		)
	);
	$debugLogManager->logData('emsActionLogEntity', $emsActionLogEntity);
	
	// addEmsActionLogItem
	$emsActionLogResult = $clientManager->addEmsActionLogItem($emsActionLogEntity);
	$debugLogManager->logData('emsActionLogResult', $emsActionLogResult);
	unset($emsActionLogEntity);
}