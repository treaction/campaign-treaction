<?php
/* @var $debugLogManager debugLogManager */
/* @var $campaignManager CampaignManager */



if (\count($_POST['contactPerson']) === 0) {
	throw new \InvalidArgumentException('no contactPersonData found!');
} else {
	$customerId = isset($_POST['customer']['kunde_id']) ? \intval($_POST['customer']['kunde_id']) : 0;
	
	/**
	 * getCustomerDataItemById (�berpr�fe ob kunde vorhanden ist)
	 * 
	 * debug
	 */
	$customerEntity = $campaignManager->getCustomerDataItemById($customerId);
	if (!($customerEntity instanceof \CustomerEntity)) {
		throw new \DomainException('no CustomerEntity!');
	}
	
	$contactPersonId = isset($_POST['contactPerson']['ap_id']) ? \intval($_POST['contactPerson']['ap_id']) : 0;
	if ($contactPersonId > 0) {
		throw new \BadMethodCallException('wrong method call!');
	}
}


/**
 * createContactPersonDataArray
 * 
 * debug
 */
$contactPersonDataArray = \createContactPersonDataArray($_POST['contactPerson']);
$contactPersonDataArray['status'] = array(
	'value' => 1,
	'dataType' => \PDO::PARAM_INT
);
$contactPersonDataArray['kunde_id'] = array(
	'value' => $customerId,
	'dataType' => \PDO::PARAM_INT
);
$debugLogManager->logData('contactPersonDataArray', $contactPersonDataArray);

/**
 * addContactPersonItem
 * 
 * debug
 */
$newContactPersonId = $campaignManager->addContactPersonItem($contactPersonDataArray);
$debugLogManager->logData('newContactPersonId', $newContactPersonId);