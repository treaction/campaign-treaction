<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */



if (\count($_POST['contactPerson']) === 0) {
	throw new \InvalidArgumentException('no contactPersonData found!');
} else {
	$contactPersonId = isset($_POST['contactPerson']['ap_id']) ? \intval($_POST['contactPerson']['ap_id']) : 0;
	if ($contactPersonId === 0) {
		throw new \InvalidArgumentException('no contactPersonId!');
	}
}


/**
 * getContactPersonDataItemById (�berpr�fe ob ansprechpartner existiert)
 * 
 * debug
 */
$contactPersonEntity = $campaignManager->getContactPersonDataItemById($contactPersonId);
if (!($contactPersonEntity instanceof \ContactPersonEntity)) {
	throw new \DomainException('invalid ContactPersonEntity!');
}
$debugLogManager->logData('contactPersonEntity', $contactPersonEntity);


/**
 * createContactPersonDataArray
 * 
 * debug
 */
$contactPersonDataArray = \createContactPersonDataArray($_POST['contactPerson']);
$debugLogManager->logData('contactPersonDataArray', $contactPersonDataArray);

/**
 * updateContactPersonDataById
 * 
 * debug
 */
$updateContactPersonResult = $campaignManager->updateContactPersonDataById(
	$contactPersonId,
	$contactPersonDataArray
);
$debugLogManager->logData('updateContactPersonResult', $updateContactPersonResult);

$emsActionLogEntity = new \EmsActionLogEntity();
$emsActionLogEntity->setClient_id($_SESSION['mID']);
$emsActionLogEntity->setUser_id($_SESSION['benutzer_id']);
$emsActionLogEntity->setAction_id(24);
$emsActionLogEntity->setRecord_id($contactPersonId);
$emsActionLogEntity->setRecord_table($campaignManager->getContactPersonTable());
$emsActionLogEntity->setLog_data(
	array(
		'contactPersonEntity' => $contactPersonEntity,
	)
);
$debugLogManager->logData('emsActionLogEntity', $emsActionLogEntity);

// addEmsActionLogItem
$emsActionLogResult = $clientManager->addEmsActionLogItem($emsActionLogEntity);
$debugLogManager->logData('emsActionLogResult', $emsActionLogResult);
unset($emsActionLogEntity);
