<?php
/* @var $debugLogManager debugLogManager */
/* @var $campaignManager CampaignManager */



if (\count($_POST['customer']) === 0) {
	throw new \InvalidArgumentException('no customerData found!');
} else {
	$customerId = isset($_POST['customer']['kunde_id']) ? \intval($_POST['customer']['kunde_id']) : 0;
	if ($customerId > 0) {
		throw new \BadMethodCallException('wrong method call!');
	}
}


/**
 * createCustomerDataArray
 * 
 * debug
 */
$customerDataArray = \createCustomerDataArray($_POST['customer']);
$customerDataArray['status'] = array(
	'value' => 1,
	'dataType' => \PDO::PARAM_INT
);
$debugLogManager->logData('customerDataArray', $customerDataArray);

/**
 * addCustomerItem
 * 
 * debug
 */
$newCustomerId = $campaignManager->addCustomerItem($customerDataArray);
$debugLogManager->logData('newCustomerId', $newCustomerId);