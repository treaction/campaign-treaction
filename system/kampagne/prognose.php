<?php
session_start();
header('Content-Type: text/html; charset=ISO-8859-1');
$mandant = $_SESSION['mandant'];
include('../db_connect.inc.php');
include('../library/prognose.test.php');
include('../library/chart.php');
include('../library/util.php');
$oRatePrognose = $_GET['oRatePrognose'];
$kRatePrognose = $_GET['kRatePrognose'];

function newTime($date) {
	$dateParts = explode(' ',$date);
	$datePart = $dateParts[0];
	$timePart =  $dateParts[1];
	$timeParts = explode(':', $timePart);
	$timePart = ($timeParts[0]+2).":".$timeParts[1].":00";
	return $datePart." ".$timePart;
}

function zahl_format($zahl) {
	$zahl = number_format($zahl, 0,0,'.');
	return $zahl;
}

function passedH($date) {
	$date_1 = explode(' ',$date);
	$datum = explode('-', $date_1[0]);
	$jahr = $datum[0];
	$monat = $datum[1];
	$tag = $datum[2];
	
	$zeit = $date_1[1];
	$zeit_teile = explode(':', $date_1[1]);
	$h = $zeit_teile[0];
	$m = $zeit_teile[1];
	
	$heute = time();
	$v_datum = mktime($h,$m,0,$monat,$tag,$jahr);
	
	$passedHours = floor(($heute-$v_datum)/3600);
	
	return $passedHours;
}

function datum_de($date_time) {
	$date_1 = explode(' ',$date_time);
	$datum = explode('-', $date_1[0]);
	$jahr = $datum[0];
	$monat = $datum[1];
	$tag = $datum[2];
	
	$zeit = $date_1[1];
	$zeit_teile = explode(':', $date_1[1]);
	$h = $zeit_teile[0];
	$m = $zeit_teile[1];
	$zeit = $h.":".$m;
	$de = $tag.".".$monat.".".$jahr;
	
	$dat_zeit = $de." - ".$zeit;
	return $dat_zeit;
}



$kid = $_GET['kid'];
$datasetnr = 1;
$datasetnr2 = 1;

$kDataArr = array();


	$k_daten = mysql_query("SELECT * FROM $kampagne_db WHERE k_id='$kid'",$verbindung);
	$k_zeile = mysql_fetch_array($k_daten);
	$gebucht = $k_zeile['gebucht'];
	$versendet = $k_zeile['versendet'];
	$openings = $k_zeile['openings_all'];
	$klicks = $k_zeile['klicks_all'];
	$vorgabe_openings = $k_zeile['vorgabe_o'];
	$vorgabe_klicks = $k_zeile['vorgabe_k'];
	$mailtyp = $k_zeile['mailtyp'];
	
	if ($vorgabe_openings == '') {$vorgabe_openings = 0;}
	if ($vorgabe_klicks == '') {$vorgabe_klicks = 0;}
	$agentur = $k_zeile['agentur'];
	$k_name = $k_zeile['k_name'];
	$hv_datum = $k_zeile['datum'];
	$v_system = $k_zeile['versandsystem'];
	
	$k_daten_nv = mysql_query("SELECT datum
								FROM $kampagne_db
								WHERE nv_id='$kid'
								AND status >= 5
								ORDER BY datum DESC",$verbindung);
	$k_zeile_nv = mysql_fetch_array($k_daten_nv);
	$nv_datum = $k_zeile_nv['datum'];
	
	if ($v_system == 'BM') {
		$hv_datum = newTime($hv_datum);
		$nv_datum = newTime($nv_datum);
	}
	
	$minH = passedH($hv_datum);
	$minHnv = passedH($nv_datum);
		
	if ($mailtyp == 't') {
		$vorgabe_openings = 0;
	}
		

if ($minH < 4 || $minHnv < 4) {

	$minH0 = $minH+1;
	$leftH = 5-$minH0;
	
	if ($leftH < 0) {
		$leftH = 5-$minHnv;
	}
	
	if ($leftH == 1) {
		$leftH = 240-(util::passedMin($nv_datum));
		$leftTimetxt = "<b>".$leftH." min</b>";
	} else {
		$leftTimetxt = "<b>".$leftH."h</b>";
	}
	
	$details = "<pre style='text-align:center;height:80px;margin-top:25px;color:#666666'>Die Prognose ist in ".$leftTimetxt." m�glich.</pre>";
	
} else {
	
	
	$k_daten_global = mysql_query("SELECT 
									SUM(versendet) as v, 
									SUM(openings_all) as oa, 
									SUM(klicks_all) as ka,
									SUM(leads) as l 
								FROM $kampagne_db
								WHERE nv_id='$kid'",$verbindung);
	$k_zeile_global = mysql_fetch_array($k_daten_global);
	$versendet_global = $k_zeile_global['v'];
	$openings_global = $k_zeile_global['oa'];
	$klicks_global = $k_zeile_global['ka'];
	$leads_global = $k_zeile_global['l'];
	
	$k_daten_all = mysql_query("SELECT * FROM $kampagne_db WHERE nv_id='$kid' AND status >= 5 ORDER BY datum DESC",$verbindung);
	
	$k_all_cnt = mysql_num_rows($k_daten_all);
	$j = 1;
	while ($k_zeile_all = mysql_fetch_array($k_daten_all)) {
		$kidPart = $k_zeile_all['k_id'];
		$datum_en = $k_zeile_all['datum'];
		$versendet = $k_zeile_all['versendet'];
		$openings_all = $k_zeile_all['openings_all'];
		$klicks_all = $k_zeile_all['klicks_all'];
		$leads = $k_zeile_all['leads'];
		$v_system = $k_zeile_all['versandsystem'];
		if ($v_system == 'BM') {
			$datum_en = newTime($datum_en);
		}
		
		
		$minHnv = passedH($datum_en);
		if ($minHnv >= 72) {
			$minHnv = "<span style='font-weight:bold;color:red;'>".$minHnv."</span>";
		} else {
			$minHnv = "<span style='font-weight:bold;color:green;'>".$minHnv."</span>";
		}
		
		$orate_nv = util::getQuote($gebucht,$openings_all)."%";
		$krate_nv = util::getQuote($gebucht,$klicks_all)."%";
		
		if ($mailtyp == 't') {
			$orate_nv = "-";
		}
		
		if ($j==1 || $j==3) {
			$bg_color = "FFFFFF";
			$j=1;
		} elseif ($j==2) {
			$bg_color = "E4E4E4";
		} 
		
		if ($datasetnr2 == $k_all_cnt) {
			$vTyp = "HV";
			$vTypTitle = "Hauptversand";
		} else {
			$vTyp = "NV";
			$vTypTitle = "Nachversand";
		}
		
		$dataHV .= '<tr style="background-color:#'.$bg_color.'">';
		$dataHV .= '<td title="'.$vTypTitle.'">'.$vTyp.'</td>';
		$dataHV .= '<td>'.util::datum_de($datum_en).'</td>';
		$dataHV .= '<td align="center">'.$minHnv.'</td>';
		$dataHV .= '<td align="right">'.util::zahl_format($versendet).'</td>';
		$dataHV .= '<td align="right">'.$orate_nv.'</td>';
		$dataHV .= '<td align="right">'.$krate_nv.'</td>';
		$dataHV .= '</tr>';
		$datasetnr2++;
		$j++;
	
		$prognose_start = prognose::getPrognoseData($gebucht,$versendet,$datum_en,$openings_all,$klicks_all);
		
		$cnt = count($rateLeftDaysArr);
		$c = 1;

		if ($datasetnr == 1 && $passedDays_<4) {

			$passedDays = 4-$cnt;
			$d = 5-$passedDays;
			
			for ($p = 1; $p <= $passedDays; $p++) {
			
				switch ($d) {
					case 4: $passedDay = "Gestern"; $oPassed = $openings_global/1.2; $kPassed = $klicks_global/1.2; break;
					case 3: $passedDay = "Vorgestern"; $oPassed = $openings_global/1.4; $kPassed = $klicks_global/1.4; break;
					case 2: $passedDay = "vor 3T"; $oPassed = $openings_global/4; $kPassed = $klicks_global/1.8; break;
					case 1: $passedDay = "vor 4T"; $oPassed = $openings_global/8; $kPassed = $klicks_global/2.2; break;
				}
				
				$oPassedRate = round((($oPassed/$gebucht)*100),1);
				$kPassedRate = round((($kPassed/$gebucht)*100),1);
				$prognoseChartDataPassed .= "<set value='".$oPassedRate."' label='".$passedDay."' color='CCCCCC' />";
				$prognoseChartDataPassed2 .= "<set value='".$kPassedRate."' label='".$passedDay."' color='CCCCCC' />";
			
				$d++;
			}
			
			$daysAll = count($rateLeftDaysArr);
		}
		
		if ($daysAll > 0) {
		
			if (count($rateHoursArr) > 0) {
					$oH = $openings_global+$rateHoursArr['openings'];
					$kH = $klicks_global+$rateHoursArr['klicks'];
					$kDataHArr = array("h" => $lefth, "o" => $oH, "k" => $kH);
			}
			
			
			foreach ($rateLeftDaysArr as $rd) {
			
				$oH = $rateHoursArr['openings']+$rd['openings'];
				$kH = $rateHoursArr['klicks']+$rd['klicks'];
				
				$kDataArr[] = array("d" => $c, "o" => $oH, "k" => $kH);
				$c++;
			}
			
			$g = $c-1;
			$loopToGo = $daysAll-$g;
			
			for ($l = 1; $l <= $loopToGo; $l++) {
				$kDataArr[] = array("d" => $c, "o" => $openings_all, "k" => $klicks_all);
				$c++;
			}
			
			$datasetnr++;
		}
	}

$oNowRate = round((($openings_global/$gebucht)*100),1);
$kNowRate = round((($klicks_global/$gebucht)*100),1);
			
if ($daysAll > 0) {		
		$prognoseChartData = "<chart canvasBorderColor='999999' valuePadding='0' numVDivLines='5' showBorder='0' borderColor='CCCCCC' borderThickness='0' bgColor='F2F2F2' chartTopMargin='5' chartBottomMargin='1' chartLeftMargin='5' chartRightMargin='15' yAxisMaxValue='".($vorgabe_openings+10)."' yAxisValuesStep='3'>";
			
		$prognoseChartData2 = "<chart canvasBorderColor='999999' valuePadding='0' numVDivLines='5' showBorder='0' borderColor='CCCCCC' borderThickness='0' bgColor='F2F2F2' chartTopMargin='5' chartBottomMargin='1' chartLeftMargin='5' chartRightMargin='15' yAxisMaxValue='".ceil($vorgabe_klicks+2)."' yAxisValuesStep='2'>";
		
		
		$klicksAllArr = array();
		$openingsAllArr = array();
		
		$hCnt = count($kDataHArr);
		
		/*
		print "<pre style='text-align:left'>";
		print_r($kDataHArr);
		print "</pre>";
		print "<hr/>";
		print "<pre style='text-align:left'>";
		print_r($kDataArr);
		print "</pre>";
		*/
		
		if ($hCnt > 0) {
			$lefth = $kDataHArr['h'];
			$openings_h = $kDataHArr['o'];
			$klicks_h = $kDataHArr['k'];
			
			$prognoseChartData .= "<set value='".$oNowRate."' label='Jetzt' color='267CE6' />";
			$prognoseChartData2 .= "<set value='".$kNowRate."' label='Jetzt' color='267CE6' />";
			
			if ($lefth > 0) {
				
				$openings_h = $openings_h;
				$klicks_h = $klicks_h;
								
				$rateHours = round((($openings_h/$gebucht)*100),1);
				$ratekHours = round((($klicks_h/$gebucht)*100),1);
				
				if ($rateHours > 100) {$rateHours = 100;}
				if ($ratekHours > 100) {$ratekHours = 100;}
				
				$prognoseChartData .= "<set value='".$rateHours."' label='".$lefth."h' color='267CE6' /><vLine label='Heute' color='267CE6'/>";
				$prognoseChartData2 .= "<set value='".$ratekHours."' label='".$lefth."h' color='267CE6' /><vLine label='Heute' color='267CE6'/>";
			}
			
		}
		
		$prognoseChartData .= $prognoseChartDataPassed;
		$prognoseChartData2 .= $prognoseChartDataPassed2;
		
		
		if ($hCnt == 0) {
				$prognoseChartData .= "<set value='".$oNowRate."' label='Heute' color='267CE6' /><vLine color='267CE6' linePosition='0'/>";
				$prognoseChartData2 .= "<set value='".$kNowRate."' label='Heute' color='267CE6' /><vLine color='267CE6' linePosition='0'/>";
		} 
		

		foreach ($kDataArr as $k) {
			
			$day = $k['d'];
			
				$klicksAll = $klicksAllArr[$day];
				$openingsAll = $openingsAllArr[$day];
				
				$kAll = $klicksAll+$k['k'];
				$oAll = $openingsAll+$k['o'];
				
				$klicksAllArr[$day] = $kAll;
				$openingsAllArr[$day] = $oAll;
				
		}
		
		$dayCnt = 1;
		$dayP = (count($klicksAllArr)-1);
		foreach ($klicksAllArr as $ka) {
		
			switch ($dayCnt) {
					case 1: $leftDay = "1T"; break;
					case 2: $leftDay = "2T"; break;
					case 3: $leftDay = "3T"; break;
					case 4: $leftDay = "4T"; break;
				}
				
				$kRate = round((($klicksAllArr[$dayCnt]/$gebucht)*100),1);
				if (($dayCnt-1) > 0) {
					$kOld = round((($klicksAllArr[($dayCnt-1)]/$gebucht)*100),1);
					if ($kRate  < $kOld) {$kRate = $kOld;}
				}	
				
				if ($kRate > 100) {$kRate = 100;}
				$prognoseChartData2 .= "<set value='".$kRate."' label='".$leftDay."' color='267CE6' />";
				
				if ($dayCnt == $dayP) {
					$dataPkrate = $kRate;
				}
			
			$dayCnt++;
		}
		
		$dayCnt = 1;
		foreach ($openingsAllArr as $ko) {
						
			switch ($dayCnt) {
					case 1: $leftDay = "1T"; break;
					case 2: $leftDay = "2T"; break;
					case 3: $leftDay = "3T"; break;
					case 4: $leftDay = "4T"; break;
				}
				
				$oRate = round((($openingsAllArr[$dayCnt]/$gebucht)*100),1);
				
				if (($dayCnt-1) > 0) { 
					$oOld = round((($openingsAllArr[($dayCnt-1)]/$gebucht)*100),1);
					if ($oRate  < $oOld) {$oRate = $oOld;}
				}	
				
				if ($oRate > 100) {$oRate = 100;}
				$prognoseChartData .= "<set value='".$oRate."' label='".$leftDay."' color='267CE6' />";
				
				if ($dayCnt == $dayP) {
					$dataPorate = $oRate;
				}
			
			$dayCnt++;
		}
		
		
		$prognoseChartData .= "<trendLines><line startValue='".$vorgabe_openings."' color='9900FF' displayvalue='Ziel' /></trendLines>";
			$prognoseChartData .= "</chart>";
			
			$prognoseChartData2 .= "<trendLines><line startValue='".$vorgabe_klicks."' color='9900FF' displayvalue='Ziel' /></trendLines>";
			$prognoseChartData2 .= "</chart>";
			
			$prognoseChartOpen = createChart::renderChart('p4h',$prognoseChartData,'Area2D','582','85');
			$prognoseChartKlick = createChart::renderChart('k4h',$prognoseChartData2,'Area2D','582','85');
			
			
		
		$q1 = $versendet_global-$gebucht;
		
		if ($q1 > 0) {
			$usq = round((($q1/$gebucht)*100),0);
		} else {
			$usq = 0;
		}
	
	$openingZielText = " [Ziel: ".$vorgabe_openings."% | Aktuell: ".$oNowRate."%]";
	
	if ($mailtyp == 't') {
		$prognoseChartOpen = "<span style='color:#666666;padding-left:30px'>Diagramm f&uuml;r Textmailings nicht verf&uuml;gbar.</span>";
		$openingZielText = "";
	}
	
	if (passedH($datum_en) >= 48) {
		$passedHours = "<span style='font-weight:bold;color:red;'>".$passedHours."</span>";
	} else {
		$passedHours = passedH($datum_en);
	}
	
	if ($usq >= 200) {
		$uColor = "red";
	} else {
		$uColor = "";
	}
	
	$details = "<table class='transp' style='width:550px'>
				<tr>
					<td class='details_hd_info'>Mailingdaten</td>
					<td class='details_hd_info'></td>
				</tr>
				<tr>
					<td class='details_info'>Kampagne</td>
					<td class='details2_info'>".util::cutString($agentur,10).", ".util::cutString($k_name,27).", Gebucht: ".zahl_format($gebucht).", Versendet: ".zahl_format($versendet_global).", <span style='color:".$uColor."'>�SQ: ".$usq."%</span></td>
				</tr>
				<tr>
					<td class='details2_info2' colspan='2'><table cellpadding='0' cellspacing='0' class='table_widget' style='width:500px;margin-left:10px'><tr class='trHeader'>
					<td></td>
					<td>Datum</td>
					<td align='center'>h vergangen</td>
					<td align='right'>Versendet</td>
					<td align='right'>&Ouml;-Rate (aktuell)</td>
					<td align='right'>K-Rate (aktuell)</td>
				</tr>".$dataHV."</table></td>
				</tr>
				<tr>
					<td class='details_hd_info'>�ffnungen</td>
					<td class='details_hd_info' style='text-align:left'>".$openingZielText."</td>
				</tr>
				<tr>
					<td class='details2_info' colspan='2' style='background-color:#FFFFFF'>".$prognoseChartOpen."</td>
				</tr>
				<tr>
					<td class='details_hd_info'>Klicks</td>
					<td class='details_hd_info' style='text-align:left'> [Ziel: ".$vorgabe_klicks."% | Aktuell: ".$kNowRate."%]</td>
				</tr>
				<tr>
					<td class='details2_info' colspan='2' style='background-color:#FFFFFF'>".$prognoseChartKlick."</td>
				</tr>
			</table>";
			
		
		
			
	} else {
		$details = "<pre style='text-align:center;height:80px;margin-top:25px;color:#666666'>Keine Prognose m�glich.<br />Der letzte Versand liegt mehr als 4 Tage zur�ck.</pre>";
	}

}

$k_name_ = explode(" ",$k_name);
		$k_name_ = $k_name_[0];
		$k_daten_last_sql = mysql_query("SELECT * FROM $kampagne_db WHERE nv_id=k_id AND status >= 5 AND agentur = '$agentur' AND k_name LIKE '%".$k_name_."%' ORDER BY datum DESC LIMIT 0,10",$verbindung);	
		
		
		$last_mailings = "<div style='padding:10px;'><table cellpadding='0' cellspacing='0' class='table_widget' style='width:575px;'><tr class='trHeader'>
							<td width='150'>Kampagne</td>
							<td>Datum</td>
							<td align='right'>Gebucht</td>
							<td align='right'>Versendet</td>
							<td align='right'>&Ouml;-Ziel</td>
							<td align='right'>&Ouml;-Rate</td>
							<td align='right'>K-Ziel</td>
							<td align='right'>K-Rate</td>
						</tr>";
		
		$m = 1;
		
		if (mysql_num_rows($k_daten_last_sql)>0) {
		
			while ($mz = mysql_fetch_array($k_daten_last_sql)) {
				
				if ($m==1 || $m==3) {
					$bg_color = "FFFFFF";
					$m=1;
				} elseif ($m==2) {
					$bg_color = "E4E4E4";
				} 
				
				$k_daten_last_sql2 = mysql_query("SELECT SUM(versendet) as v, SUM(openings_all) as o, SUM(klicks_all) as k FROM $kampagne_db WHERE nv_id=".$mz['k_id'],$verbindung);	
				$mz2 = mysql_fetch_array($k_daten_last_sql2);
				
			
				$last_mailings .= "<tr style='background-color:#".$bg_color."'>";
				$last_mailings .= "<td>".util::cutString($mz['k_name'],35)."</td>";
				$last_mailings .= "<td>".util::datum_de($mz['datum'],'date')."</td>";
				$last_mailings .= "<td align='right'>".util::zahl_format($mz['gebucht'])."</td>";
				$last_mailings .= "<td align='right'>".util::zahl_format($mz2['v'])."</td>";
				$last_mailings .= "<td align='right'>".$mz['vorgabe_o']."%</td>";
				$last_mailings .= "<td align='right'>".util::getQuote($mz['gebucht'],$mz2['o'])."%</td>";
				$last_mailings .= "<td align='right'>".$mz['vorgabe_k']."%</td>";
				$last_mailings .= "<td align='right'>".util::getQuote($mz['gebucht'],$mz2['k'])."%</td>";
				$last_mailings .= "</tr>";
				
				$m++;
			}
			
			$last_mailings .= "</table></div>";
		
		} else {
			$last_mailings .= "<tr><td height='300'>Keine Mailings gefunden</td></tr>";
		}
		

?>
<input type="hidden" id="kidP" value="<?php print $kid;?>" />
<input type="hidden" id="p_asp" value="<?php print $v_system;?>" />
<input type="hidden" id="oRatePrognose" value="<?php print $dataPorate;?>" />
<input type="hidden" id="kRatePrognose" value="<?php print $dataPkrate;?>" />
<div id="prognose_div" class="yui-navset" style="padding:5px;text-align:left;border:0px;width:570;">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class="selected"><a href="#tab1"><em>Prognose</em></a></li>
		<li><a href="#tab_l2"><em>Letzten Versendungen</em></a></li>
    </ul>

    <div id="bottom_content_info" class="yui-content" style="margin:0px;padding:0px">
        <div id="tab1"><?php print $details;?></div>
		<div id="tab_l2"><?php print $last_mailings;?></div>
    </div>
</div>

<script type="text/javascript">
var tabViewP = new YAHOO.widget.TabView('prognose_div');
</script>
