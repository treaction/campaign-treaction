<?php
\header('Content-Type: text/html; charset=utf-8');

/**
 * initCampaignEntityFromCampaignPostData
 * 
 * @param array $campaignPostData
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param \debugLogManager $debugLogManager
 * @return void
 */
function initCampaignEntityFromCampaignPostData(array $campaignPostData, \CampaignWidthCustomerAndContactPersonEntity &$campaignEntity, \debugLogManager $debugLogManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	$billingType = \DataFilterUtils::filterData(
		$campaignPostData['abrechnungsart'],
		\DataFilterUtils::$validateFilterDataArray['string']
	);
	
	$campaignEntity->setDatum($campaignPostData['date'] . ' ' . $campaignPostData['versand_hour'] . ':' . $campaignPostData['versand_min']);
	$campaignEntity->setErstreporting(\getReportingDateByType(
		'erstreporting',
		$campaignPostData['date'],
		$debugLogManager
	));
	$campaignEntity->setEndreporting(\getReportingDateByType(
		'endreporting',
		$campaignPostData['date'],
		$debugLogManager
	));
	$campaignEntity->setStatus(isset($campaignPostData['status']) ? \intval($campaignPostData['status']) : 0);

	$campaignEntity->setK_name(\DataFilterUtils::filterData(
		$campaignPostData['k_name'],
		\DataFilterUtils::$validateFilterDataArray['special_chars']
	));

	$campaignEntity->setBetreff(
		\str_replace(
			array(
				'&amp;'
			),
			array(
				'&'
			),
			\DataFilterUtils::filterData(
				$campaignPostData['betreff'],
				\DataFilterUtils::$validateFilterDataArray['special_chars']
			)
		)
	);

	$campaignEntity->setAbsendername(\DataFilterUtils::filterData(
		$campaignPostData['absendername'],
		\DataFilterUtils::$validateFilterDataArray['special_chars']
	));

	$campaignEntity->setAbrechnungsart(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray[$billingType]);

	$campaignEntity->setVersandsystem(\DataFilterUtils::filterData(
		$campaignPostData['versandsystem'],
		\DataFilterUtils::$validateFilterDataArray['string']
	));

	$campaignEntity->setMailtyp(\DataFilterUtils::filterData(
		$campaignPostData['mailtyp'],
		\DataFilterUtils::$validateFilterDataArray['string']
	));

	$campaignEntity->setNotiz(\DataFilterUtils::utf8DecodeAndFilterData(
		$campaignPostData['note'],
		\DataFilterUtils::$validateFilterDataArray['full_special_chars']
	));

	$campaignEntity->setMail_id(\DataFilterUtils::filterData(
		$campaignPostData['mail_id'],
		\DataFilterUtils::$validateFilterDataArray['string']
	));

	$campaignEntity->setBlacklist(isset($campaignPostData['bl']) ? $campaignPostData['bl'] : '');

	$campaignEntity->setBearbeiter(\DataFilterUtils::filterData(
		$campaignPostData['bearbeiter'],
		\DataFilterUtils::$validateFilterDataArray['special_chars']
	));

	$campaignEntity->setPreis(\DataFilterUtils::convertDataToFloatvalue($campaignPostData['preis']));
	$campaignEntity->setPreis2(\DataFilterUtils::convertDataToFloatvalue($campaignPostData['preis2']));
	$campaignEntity->setUnit_price(\DataFilterUtils::convertDataToFloatvalue($campaignPostData['unit_price']));
	$campaignEntity->setPreis_gesamt(\DataFilterUtils::convertDataToFloatvalue($campaignPostData['preis_gesamt']));
	$campaignEntity->setPreis_gesamt_verify(\DataFilterUtils::filterData(
		$campaignPostData['preis_gesamt_verify'],
		\DataFilterUtils::$validateFilterDataArray['number_int']
	));

	switch ($campaignEntity->getAbrechnungsart()) {
		case 'Hybrid':
			$campaignEntity->setPreis_gesamt(0);
			break;

		case 'Festpreis':
			$campaignEntity->setPreis(0);
			$campaignEntity->setPreis2(0);
			$campaignEntity->setUnit_price(0);
			break;

		default:
			$campaignEntity->setPreis2(0);
			$campaignEntity->setPreis_gesamt(0);
			break;
	}

	$campaignEntity->setVersendet($campaignPostData['versendet']);

	$campaignEntity->setLeads($campaignPostData['leads']);
	$campaignEntity->setLeads_u($campaignPostData['leads_u']);

	$campaignEntity->setKlicks($campaignPostData['klicks_u']);
	$campaignEntity->setKlicks_all($campaignPostData['klicks']);

	$campaignEntity->setOpenings($campaignPostData['openings_u']);
	$campaignEntity->setOpenings_all($campaignPostData['openings']);

	$campaignEntity->setVorgabe_o(\DataFilterUtils::convertDataToFloatvalue($campaignPostData['vorgabe_o']));
	$campaignEntity->setVorgabe_k(\DataFilterUtils::convertDataToFloatvalue($campaignPostData['vorgabe_k']));
	$campaignEntity->setVorgabe_m(\DataFilterUtils::filterData(
		$campaignPostData['vorgabe_m'],
		\DataFilterUtils::$validateFilterDataArray['number_int']
	));

	$campaignEntity->setGebucht(\DataFilterUtils::filterData(
		$campaignPostData['volumen'],
		\DataFilterUtils::$validateFilterDataArray['number_int']
	));
	
	$campaignEntity->setCampaign_click_profiles_id(\DataFilterUtils::filterData(
		$campaignPostData['campaign_click_profiles_id'],
		\DataFilterUtils::$validateFilterDataArray['number_int']
	));
	
	$campaignEntity->setSelection_click_profiles_id(\DataFilterUtils::filterData(
		$campaignPostData['selection_click_profiles_id'],
		\DataFilterUtils::$validateFilterDataArray['number_int']
	));
	
	$campaignEntity->setPremium_campaign(\DataFilterUtils::filterData(
		$campaignPostData['premium_campaign'],
		\DataFilterUtils::$validateFilterDataArray['number_int']
	));
	
	$campaignEntity->setAdvertising_materials(\DataFilterUtils::filterData(
		$campaignPostData['advertising_materials'],
		\DataFilterUtils::$validateFilterDataArray['number_int']
	));
	
	$debugLogManager->endGroup();
}

/**
 * getOrAddCustomerEntity
 * 
 * @param string $campaignAgentur
 * @param array $customerPostData
 * @param \debugLogManager $debugLogManager
 * @param \CampaignManager $campaignManager
 * @return \CustomerEntity
 * @throws \DomainException
 */
function getOrAddCustomerEntity($campaignAgentur, array $customerPostData, \debugLogManager $debugLogManager, \CampaignManager $campaignManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	$agentur = \DataFilterUtils::filterData(
		$campaignAgentur,
		\DataFilterUtils::$validateFilterDataArray['string']
	);
	$debugLogManager->logData('agentur', $agentur);
	
	if (\DataFilterUtils::isInteger($agentur)) {
		/**
		 * getCustomerDataItemById
		 */
		$customerEntity = $campaignManager->getCustomerDataItemById(\intval($agentur));
		if (!($customerEntity instanceof \CustomerEntity)) {
			throw new \DomainException('no CustomerEntity');
		}
		
		$debugLogManager->logData('getCustomerDataItemById', $customerEntity);
	} else {
		/**
		 * getCustomerDataItemByCompany
		 */
		$customerEntity = $campaignManager->getCustomerDataItemByCompany($agentur);
		if (!($customerEntity instanceof \CustomerEntity)) {
			/**
			 * company not found
			 */
			// initialize empty CustomerEntity
			$customerEntity = new \CustomerEntity();
			if (!($customerEntity instanceof \CustomerEntity)) {
				throw new \BadFunctionCallException('customerEntity not loaded');
			}

			$customerEntity->setFirma($agentur);

			/**
			 * updateCustomerEntityData
			 */
			\updateCustomerEntityData(
				$customerEntity,
				$customerPostData
			);


			/**
			 * addNewCustomerEntry
			 * 
			 * debug
			 */
			$idAddCustomerEntry = \addNewCustomerEntry(
				$campaignManager,
				$debugLogManager,
				$customerEntity
			);
			$debugLogManager->logData('idAddCustomerEntry', $idAddCustomerEntry);

			// setKunde_id
			$customerEntity->setKunde_id($idAddCustomerEntry);
			
			$debugLogManager->logData('customerEntity', $customerEntity);
		} else {
			$debugLogManager->logData('getCustomerDataItemByCompany', $customerEntity);
		}
	}
	
	$debugLogManager->endGroup();
	
	return $customerEntity;
}

/**
 * getOrAddContactPersonEntity
 * 
 * @param array $contactPersonPostData
 * @param \CustomerEntity $customerEntity
 * @param \debugLogManager $debugLogManager
 * @param \CampaignManager $campaignManager
 * @return \ContactPersonEntity
 * @throws \DomainException
 */
function getOrAddContactPersonEntity(array $contactPersonPostData, \CustomerEntity $customerEntity, \debugLogManager $debugLogManager, \CampaignManager $campaignManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	if (\strlen($contactPersonPostData['vorname']) > 0) {
		// initialize empty ContactPersonEntity
		$contactPersonEntity = new \ContactPersonEntity();
		if (!($contactPersonEntity instanceof \ContactPersonEntity)) {
			throw new \BadFunctionCallException('contactPersonEntity not loaded');
		}

		/**
		 * updateContactPersonEntityData
		 */
		\updateContactPersonEntityData(
			$contactPersonEntity,
			$contactPersonPostData,
			$customerEntity
		);


		/**
		 * addNewContactPersonEntity
		 * 
		 * debug
		 */
		$idAddContactPersonEntity = \addNewContactPersonEntity(
			$campaignManager,
			$debugLogManager,
			$contactPersonEntity
		);
		$debugLogManager->logData('idAddContactPersonEntity', $idAddContactPersonEntity);


		// setAp_id
		$contactPersonEntity->setAp_id($idAddContactPersonEntity);
		
		$debugLogManager->logData('contactPersonEntity', $contactPersonEntity);
	} else {
		/**
		 * getContactPersonDataItemById
		 * 
		 * debug
		 */
		$contactPersonEntity = $campaignManager->getContactPersonDataItemById(\intval($_POST['campaign']['ap']));
		if (!($contactPersonEntity instanceof \ContactPersonEntity)) {
			$contactPersonEntity = new \ContactPersonEntity();
			if (!($contactPersonEntity instanceof \ContactPersonEntity)) {
				throw new \BadFunctionCallException('contactPersonEntity not loaded');
			}
			
			$debugLogManager->logData('contactPersonEntity', $contactPersonEntity);
		} else {
			$debugLogManager->logData('getContactPersonDataItemById', $contactPersonEntity);
		}
	}
	
	$debugLogManager->endGroup();
	
	return $contactPersonEntity;
}

/**
 * getReportingDateByType
 * 
 * @param string $type
 * @param string $date
 * @param \debugLogManager $debugLogManager
 * @return string
 */
function getReportingDateByType($type, $date, \debugLogManager $debugLogManager) {
    // debug
	$debugLogManager->beginGroup(__FUNCTION__ . ': ' . $type);
	$debugLogManager->logData('date', $date);

	$reporteringDateObj = new \DateTime($date);

	switch ($type) {
		case 'erstreporting':
			$reporteringDateObj->modify('+1 day');
			break;

		case 'endreporting':
			$reporteringDateObj->modify('+6 day');
			break;
	}

	$weekNumber = $reporteringDateObj->format('w');
	if ((int) $weekNumber === 6) {
		$reporteringDateObj->modify('+2 day');
	} elseif ((int) $weekNumber === 0) {
		$reporteringDateObj->modify('+1 day');
	}

	// debug
	$debugLogManager->logData('reporteringDateObj', $reporteringDateObj);
	$debugLogManager->endGroup();

	return $reporteringDateObj->format('Y-m-d');
}

/**
 * addNewLogEntry
 * 
 * @param \CampaignManager $campaignManager
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param integer $actionId
 * @return boolean|integer
 */
function addNewLogEntry(\CampaignManager $campaignManager, \CampaignWidthCustomerAndContactPersonEntity $campaignEntity, $actionId) {
    return $campaignManager->addLogItem(
		array(
			'userid' => array(
				'value' => \intval($_SESSION['benutzer_id']),
				'dataType' => \PDO::PARAM_INT
			),
			'action' => array(
				'value' => $actionId,
				'dataType' => \PDO::PARAM_INT
			),
			'kid' => array(
				'value' => $campaignEntity->getK_id(),
				'dataType' => \PDO::PARAM_INT
			),
			'nv_id' => array(
				'value' => $campaignEntity->getNv_id(),
				'dataType' => \PDO::PARAM_INT
			),
			'timestamp' => array(
				'value' => \date('Y-m-d H:i:s'),
				'dataType' => \PDO::PARAM_STR
			)
		)
	);
}

/**
 * sendBenachrichtigungsMail
 * 
 * @param \CampaignManager $campaignManager
 * @param \debugLogManager $debugLogManager
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param \SwiftMailerWebservice $swiftMailerWebservice
 * @param array $benachrichtigungEmailDataArray
 * @param string $benachrichtigungNotiz
 * @return void
 */
function sendBenachrichtigungsMail(\CampaignManager $campaignManager, \debugLogManager $debugLogManager, \CampaignWidthCustomerAndContactPersonEntity $campaignEntity, \SwiftMailerWebservice $swiftMailerWebservice, array $benachrichtigungEmailDataArray, $benachrichtigungNotiz = '') {
    // debug
	$debugLogManager->beginGroup(__FUNCTION__);
	$debugLogManager->logData('campaignEntity', $campaignEntity);
	$debugLogManager->logData('benachrichtigungEmailDataArray', $benachrichtigungEmailDataArray);

	/**
	 * createMessage
	 */
	$swiftMessage = $swiftMailerWebservice->createMessage();
	/* @var $swiftMessage Swift_Message */

	$swiftMessage->setBody('Ein neues Mailing wurde im MaaS angelegt:' . \chr(13)
			. '========================================' . \chr(13) . \chr(13)
			. $benachrichtigungNotiz
			. 'Agentur: ' . $campaignEntity->getCustomerEntity()->getFirma() . \chr(13)
			. 'Kampagne: ' . $campaignEntity->getK_name() . \chr(13)
			. 'Versanddatum: ' . $campaignEntity->getDatum()->format('d.m.Y') . \chr(13)
			. 'Uhrzeit: ' . $campaignEntity->getDatum()->format('H:i') . ' Uhr' . \chr(13)
		)
		->setSubject('Neues Mailing [' . $campaignEntity->getK_name() . ']')
		->setTo($benachrichtigungEmailDataArray)
	;

	/**
	 * sendToMultipleAddress
	 * 
	 * debug
	 */
	$mailerSendResults = $swiftMailerWebservice->sendMessage($swiftMessage);
	$debugLogManager->logData('mailerSendResults', $mailerSendResults);

	/**
	 * getFailedRecipients
	 * 
	 * debug
	 */
	$failedRecipients = $swiftMailerWebservice->getFailedRecipients();
	$debugLogManager->logData('failedRecipients', $failedRecipients);
	$swiftMailerWebservice->setFailedRecipients(array());


	/**
	 * addNewLogEntry
	 * 
	 * debug
	 */
	$idAddLogEntry = addNewLogEntry(
		$campaignManager,
		$campaignEntity,
		8
	);
	$debugLogManager->logData('idAddLogEntry', $idAddLogEntry);

	$debugLogManager->endGroup();

	if (\count($failedRecipients) > 0) {
		echo '<script>alert("Die Email konnte an folgende Addressen nicht gesendet werden:' . \implode(\chr(13), $failedRecipients) . '");</script>';
	} else {
		echo '<script>alert("Die Email wurde versandt.");</script>';
	}
}

/**
 * updateCustomerEntityData
 * 
 * @param \CustomerEntity $customerEntity
 * @param array $postData
 * @return void
 */
function updateCustomerEntityData(\CustomerEntity &$customerEntity, array $postData) {
    $customerEntity->setStatus(1);
	$customerEntity->setFirma_short(\DataFilterUtils::filterData(
		$postData['firma_short'],
		\DataFilterUtils::$validateFilterDataArray['special_chars']
	));
	$customerEntity->setStrasse(\DataFilterUtils::filterData(
		$postData['strasse'],
		\DataFilterUtils::$validateFilterDataArray['special_chars']
	));
	$customerEntity->setPlz(\DataFilterUtils::filterData(
		$postData['plz'],
		\DataFilterUtils::$validateFilterDataArray['string']
	));
	$customerEntity->setOrt(\DataFilterUtils::filterData(
		$postData['ort'],
		\DataFilterUtils::$validateFilterDataArray['special_chars']
	));
	$customerEntity->setTelefon(\DataFilterUtils::filterData(
		$postData['telefon'],
		\DataFilterUtils::$validateFilterDataArray['string']
	));
	$customerEntity->setFax(\DataFilterUtils::filterData(
		$postData['fax'],
		\DataFilterUtils::$validateFilterDataArray['string']
	));
	$customerEntity->setWebsite(\DataFilterUtils::filterData(
		$postData['website'],
		\DataFilterUtils::$validateFilterDataArray['url']
	));
	$customerEntity->setEmail(\DataFilterUtils::filterData(
		$postData['email'],
		\DataFilterUtils::$validateFilterDataArray['email']
	));
	$customerEntity->setGeschaeftsfuehrer(\DataFilterUtils::filterData(
		$postData['geschaeftsfuehrer'],
		\DataFilterUtils::$validateFilterDataArray['special_chars']
	));
	$customerEntity->setRegistergericht(\DataFilterUtils::filterData(
		$postData['registergericht'],
		\DataFilterUtils::$validateFilterDataArray['string']
	));
	
	$customerEntity->setVat_number(\DataFilterUtils::filterData(
		$postData['vat_number'],
		\DataFilterUtils::$validateFilterDataArray['string']
	));
	$customerEntity->setPayment_deadline(\intval($postData['payment_deadline']));
	$customerEntity->setCountry_id(\intval($postData['country_id']));
	$customerEntity->setData_selection(\intval($postData['data_selection']));
}

/**
 * addNewCustomerEntry
 * 
 * @param \CampaignManager $campaignManager
 * @param \debugLogManager $debugLogManager
 * @param \CustomerEntity $customerEntity
 * @return boolean|integer
 */
function addNewCustomerEntry(\CampaignManager $campaignManager, \debugLogManager $debugLogManager, \CustomerEntity $customerEntity) {
    $customerDataArray = array(
		'firma' => array(
			'value' => $customerEntity->getFirma(),
			'dataType' => \PDO::PARAM_STR
		),
		'status' => array(
			'value' => $customerEntity->getStatus(),
			'dataType' => \PDO::PARAM_INT
		),
		'firma_short' => array(
			'value' => $customerEntity->getFirma_short(),
			'dataType' => \PDO::PARAM_STR
		),
		'strasse' => array(
			'value' => $customerEntity->getStrasse(),
			'dataType' => \PDO::PARAM_STR
		),
		'plz' => array(
			'value' => $customerEntity->getPlz(),
			'dataType' => \PDO::PARAM_STR
		),
		'ort' => array(
			'value' => $customerEntity->getOrt(),
			'dataType' => \PDO::PARAM_STR
		),
		'telefon' => array(
			'value' => $customerEntity->getTelefon(),
			'dataType' => \PDO::PARAM_STR
		),
		'fax' => array(
			'value' => $customerEntity->getFax(),
			'dataType' => \PDO::PARAM_STR
		),
		'website' => array(
			'value' => $customerEntity->getWebsite(),
			'dataType' => \PDO::PARAM_STR
		),
		'email' => array(
			'value' => $customerEntity->getEmail(),
			'dataType' => \PDO::PARAM_STR
		),
		'geschaeftsfuehrer' => array(
			'value' => $customerEntity->getGeschaeftsfuehrer(),
			'dataType' => \PDO::PARAM_STR
		),
		'registergericht' => array(
			'value' => $customerEntity->getRegistergericht(),
			'dataType' => \PDO::PARAM_STR
		),
		'vat_number' => array(
			'value' => $customerEntity->getVat_number(),
			'dataType' => \PDO::PARAM_STR
		),
		'payment_deadline' => array(
			'value' => $customerEntity->getPayment_deadline(),
			'dataType' => \PDO::PARAM_INT
		),
		'country_id' => array(
			'value' => $customerEntity->getCountry_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'data_selection' => array(
			'value' => $customerEntity->getData_selection(),
			'dataType' => \PDO::PARAM_INT
		),
	);
	$debugLogManager->logData(__FUNCTION__, $customerDataArray);

	return $campaignManager->addCustomerItem($customerDataArray);
}

/**
 * updateContactPersonEntityData
 * 
 * @param \ContactPersonEntity $contactPersonEntity
 * @param array $postData
 * @param \CustomerEntity $customerEntity
 * @return void
 */
function updateContactPersonEntityData(\ContactPersonEntity &$contactPersonEntity, array $postData, \CustomerEntity $customerEntity) {
    $contactPersonGender = \DataFilterUtils::filterData(
		$postData['anrede'],
		\DataFilterUtils::$validateFilterDataArray['string']
	);

	$contactPersonEntity->setStatus(1);
	$contactPersonEntity->setKunde_id($customerEntity->getKunde_id());
	$contactPersonEntity->setAnrede(($contactPersonGender == 'Herr' ? 'Herr' : 'Frau'));
	$contactPersonEntity->setTitel(\DataFilterUtils::filterData(
		$postData['titel'],
		\DataFilterUtils::$validateFilterDataArray['special_chars']
	));
	$contactPersonEntity->setVorname(\DataFilterUtils::filterData(
		$postData['vorname'],
		\DataFilterUtils::$validateFilterDataArray['special_chars']
	));
	$contactPersonEntity->setNachname(\DataFilterUtils::filterData(
		$postData['nachname'],
		\DataFilterUtils::$validateFilterDataArray['special_chars']
	));
	$contactPersonEntity->setEmail(\DataFilterUtils::filterData(
		$postData['email'],
		\DataFilterUtils::$validateFilterDataArray['email']
	));
	$contactPersonEntity->setVertriebler_id(\DataFilterUtils::filterData(
		$postData['vertriebler_id'],
		\DataFilterUtils::$validateFilterDataArray['number_int']
	));
	$contactPersonEntity->setTelefon(\DataFilterUtils::filterData(
		$postData['telefon'],
		\DataFilterUtils::$validateFilterDataArray['string']
	));
	$contactPersonEntity->setFax(\DataFilterUtils::filterData(
		$postData['fax'],
		\DataFilterUtils::$validateFilterDataArray['string']
	));
	$contactPersonEntity->setMobil(\DataFilterUtils::filterData(
		$postData['mobil'],
		\DataFilterUtils::$validateFilterDataArray['string']
	));
	$contactPersonEntity->setPosition(\DataFilterUtils::filterData(
		$postData['position'],
		\DataFilterUtils::$validateFilterDataArray['special_chars']
	));
}

/**
 * addNewContactPersonEntity
 * 
 * @param \CampaignManager $campaignManager
 * @param \debugLogManager $debugLogManager
 * @param \ContactPersonEntity $contactPersonEntity
 * @return boolean|integer
 */
function addNewContactPersonEntity(\CampaignManager $campaignManager, \debugLogManager $debugLogManager, \ContactPersonEntity $contactPersonEntity) {
    $contactPersonDataArray = array(
		'anrede' => array(
			'value' => $contactPersonEntity->getAnrede(),
			'dataType' => \PDO::PARAM_STR
		),
		'titel' => array(
			'value' => $contactPersonEntity->getTitel(),
			'dataType' => \PDO::PARAM_STR
		),
		'vorname' => array(
			'value' => $contactPersonEntity->getVorname(),
			'dataType' => \PDO::PARAM_STR
		),
		'nachname' => array(
			'value' => $contactPersonEntity->getNachname(),
			'dataType' => \PDO::PARAM_STR
		),
		'email' => array(
			'value' => $contactPersonEntity->getEmail(),
			'dataType' => \PDO::PARAM_STR
		),
		'vertriebler_id' => array(
			'value' => $contactPersonEntity->getVertriebler_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'kunde_id' => array(
			'value' => $contactPersonEntity->getKunde_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'status' => array(
			'value' => $contactPersonEntity->getStatus(),
			'dataType' => \PDO::PARAM_INT
		),
		'telefon' => array(
			'value' => $contactPersonEntity->getTelefon(),
			'dataType' => \PDO::PARAM_STR
		),
		'fax' => array(
			'value' => $contactPersonEntity->getFax(),
			'dataType' => \PDO::PARAM_STR
		),
		'mobil' => array(
			'value' => $contactPersonEntity->getMobil(),
			'dataType' => \PDO::PARAM_STR
		),
		'position' => array(
			'value' => $contactPersonEntity->getPosition(),
			'dataType' => \PDO::PARAM_STR
		),
	);
	$debugLogManager->logData(__FUNCTION__, $contactPersonDataArray);

	return $campaignManager->addContactPersonItem($contactPersonDataArray);
}

/**
 * createCampaignDataArray
 * 
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @return array
 */
function createCampaignDataArray(\CampaignWidthCustomerAndContactPersonEntity $campaignEntity) {
    $dataArray = array(
		'datum' => array(
			'value' => $campaignEntity->getDatum()->format('Y-m-d H:i:s'),
			'dataType' => \PDO::PARAM_STR
		),
		'gestartet' => array(
			'value' => $campaignEntity->getDatum()->format('Y-m-d H:i:s'),
			'dataType' => \PDO::PARAM_STR
		),
		'k_name' => array(
			'value' => $campaignEntity->getK_name(),
			'dataType' => \PDO::PARAM_STR
		),
		'betreff' => array(
			'value' => $campaignEntity->getBetreff(),
			'dataType' => \PDO::PARAM_STR
		),
		'absendername' => array(
			'value' => $campaignEntity->getAbsendername(),
			'dataType' => \PDO::PARAM_STR
		),
		'gebucht' => array(
			'value' => $campaignEntity->getGebucht(),
			'dataType' => \PDO::PARAM_INT
		),
		'mailtyp' => array(
			'value' => $campaignEntity->getMailtyp(),
			'dataType' => \PDO::PARAM_STR
		),
		'zielgruppe' => array(
			'value' => $campaignEntity->getZielgruppe(),
			'dataType' => \PDO::PARAM_STR
		),
		'blacklist' => array(
			'value' => $campaignEntity->getBlacklist(),
			'dataType' => \PDO::PARAM_STR
		),
		'versandsystem' => array(
			'value' => $campaignEntity->getVersandsystem(),
			'dataType' => \PDO::PARAM_STR
		),
		'notiz' => array(
			'value' => $campaignEntity->getNotiz(),
			'dataType' => \PDO::PARAM_STR
		),
		'vorgabe_o' => array(
			'value' => $campaignEntity->getVorgabe_o(),
			'dataType' => \PDO::PARAM_STR
		),
		'vorgabe_k' => array(
			'value' => $campaignEntity->getVorgabe_k(),
			'dataType' => \PDO::PARAM_STR
		),
		'vorgabe_m' => array(
			'value' => $campaignEntity->getVorgabe_m(),
			'dataType' => \PDO::PARAM_STR
		),
		'openings' => array(
			'value' => $campaignEntity->getOpenings(),
			'dataType' => \PDO::PARAM_INT
		),
		'openings_all' => array(
			'value' => $campaignEntity->getOpenings_all(),
			'dataType' => \PDO::PARAM_INT
		),
		'klicks' => array(
			'value' => $campaignEntity->getKlicks(),
			'dataType' => \PDO::PARAM_INT
		),
		'klicks_all' => array(
			'value' => $campaignEntity->getKlicks_all(),
			'dataType' => \PDO::PARAM_INT
		),
		'leads' => array(
			'value' => $campaignEntity->getLeads(),
			'dataType' => PDO::PARAM_INT
		),
		'leads_u' => array(
			'value' => $campaignEntity->getLeads_u(),
			'dataType' => \PDO::PARAM_INT
		),
		'erstreporting' => array(
			'value' => $campaignEntity->getErstreporting()->format('Y-m-d'),
			'dataType' => \PDO::PARAM_STR
		),
		'endreporting' => array(
			'value' => $campaignEntity->getEndreporting()->format('Y-m-d'),
			'dataType' => \PDO::PARAM_STR
		),
		'dsd_id' => array(
			'value' => $campaignEntity->getDsd_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'preis' => array(
			'value' => $campaignEntity->getPreis(),
			'dataType' => \PDO::PARAM_STR
		),
		'preis2' => array(
			'value' => $campaignEntity->getPreis2(),
			'dataType' => \PDO::PARAM_STR
		),
		'unit_price' => array(
			'value' => $campaignEntity->getUnit_price(),
			'dataType' => \PDO::PARAM_STR
		),
		'preis_gesamt' => array(
			'value' => $campaignEntity->getPreis_gesamt(),
			'dataType' => \PDO::PARAM_STR
		),
		'preis_gesamt_verify' => array(
			'value' => $campaignEntity->getPreis_gesamt_verify(),
			'dataType' => \PDO::PARAM_BOOL
		),
		'abrechnungsart' => array(
			'value' => $campaignEntity->getAbrechnungsart(),
			'dataType' => \PDO::PARAM_STR
		),
		'campaign_click_profiles_id' => array(
			'value' => $campaignEntity->getCampaign_click_profiles_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'selection_click_profiles_id' => array(
			'value' => $campaignEntity->getSelection_click_profiles_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'premium_campaign' => array(
			'value' => $campaignEntity->getPremium_campaign(),
			'dataType' => \PDO::PARAM_INT
		),
		'advertising_materials' => array(
			'value' => $campaignEntity->getAdvertising_materials(),
			'dataType' => \PDO::PARAM_INT
		),
		
		// TODO: deprecated, entfernen
		'cs_id' => array(
			'value' => $campaignEntity->getCs_id(),
			'dataType' => \PDO::PARAM_INT
		),

	);

	return $dataArray;
}

/**
 * processCampaignSelection
 * 
 * @param array $zielgruppeDataArray
 * @param array $tmpZgDataArray
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @throws \DomainException
 * @return void
 */
function processCampaignSelection(array &$zielgruppeDataArray, array $tmpZgDataArray, \CampaignWidthCustomerAndContactPersonEntity &$campaignEntity, \ClientManager $clientManager, \debugLogManager $debugLogManager) {
	$debugLogManager->beginGroup(__FUNCTION__);

	if ($campaignEntity->getSelection_click_profiles_id() > 0) {
		/**
		 * getClientClickProfileDataByUid
		 * 
		 * debug
		 */
		$clickProfileEntity = $clientManager->getClientClickProfileDataByUid($campaignEntity->getSelection_click_profiles_id());
		$debugLogManager->logData('clickProfileEntity', $clickProfileEntity);
		
		if (!($clickProfileEntity instanceof \ClickProfileEntity)) {
			throw new \DomainException('invalid ClickProfileEntity');
		}
		
		if ((\in_array($clickProfileEntity->getShortcut(), $tmpZgDataArray)) !== true) {
			$zielgruppeDataArray[] = $clickProfileEntity->getShortcut();
		}
	}
	
	$debugLogManager->endGroup();
}

/**
 * processTargetGroupDataArray
 * 
 * @param array $campaignPostData
 * @param array $zielgruppeDataArray
 * @param array $tmpZgDataArray
 * @param \debugLogManager $debugLogManager
 * @return void
 */
function processTargetGroupDataArray(array $campaignPostData, array &$zielgruppeDataArray, array $tmpZgDataArray, \debugLogManager $debugLogManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	// b2b
	if (isset($campaignPostData['b2b']) 
		&& \intval($campaignPostData['b2b']) === 1
	) {
		if ((\in_array('B2B', $tmpZgDataArray)) !== true) {
			$zielgruppeDataArray[] = 'B2B';
		}
	}
	
	// genderSelection
	if (isset($campaignPostData['genderSelection'])) {
		$genderSelection = \DataFilterUtils::filterData(
			$campaignPostData['genderSelection'],
			\DataFilterUtils::$validateFilterDataArray['number_int']
		);
		$debugLogManager->logData('genderSelection', $genderSelection);
		
		if ((int) $genderSelection === 0) {
			$genderSelectionValue = 'Damen und Herren';
		} else {
			$genderSelectionValue = \CampaignAndCustomerUtils::$genderDataArray[$genderSelection];
		}
		
		// debug
		$debugLogManager->logData('genderSelectionValue', $genderSelectionValue);
		
		if ((\in_array($genderSelectionValue, $tmpZgDataArray)) !== true) {
			$zielgruppeDataArray[] = $genderSelectionValue;
		}
	}
	
	// alter
	if (isset($campaignPostData['alter']) 
		&& \strlen($campaignPostData['alter']) > 0
	) {
		$alter = \DataFilterUtils::filterData(
			$campaignPostData['alter'],
			\DataFilterUtils::$validateFilterDataArray['special_chars']
		);
		
		if ((\in_array($alter, $tmpZgDataArray)) !== true) {
			$zielgruppeDataArray[] = $alter;
		}
	}
	
	$debugLogManager->endGroup();
}



/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * db_connect
 * 
 * $debugLogManager
 * $campaignManager
 * $clientManager
 * $swiftMailerWebservice
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = \RegistryUtils::get('clientEntity')->getAbkz();
require(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(\DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

require_once(DIR_configsInit . 'initSwiftMailerWebservice.php');
/* @var $swiftMailerWebservice \SwiftMailerWebservice */

// Factory
require_once(DIR_Factory . 'DeliverySystemFactory.php');



// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);

$postAction = isset($_POST['action']) ? \strip_tags($_POST['action']) : null;
$getAction = isset($_GET['action']) ? \strip_tags($_GET['action']) : null;

try {
	if (\strlen($postAction) > 0) {
		$imvs = isset($_POST['campaign']['imvs']) ? (boolean) \intval($_POST['campaign']['imvs']) : false;
		$debugLogManager->logData('imvs', $imvs);
		
		$mailingTyp = isset($_POST['mailingTyp']) ? \DataFilterUtils::filterData(
			$_POST['mailingTyp'],
			\DataFilterUtils::$validateFilterDataArray['special_chars']
		) : null;
		$debugLogManager->logData('mailingTyp', $mailingTyp);
		
		
		// initialize empty CampaignWidthCustomerAndContactPersonEntity
		$campaignEntity = new \CampaignWidthCustomerAndContactPersonEntity();
		if (!($campaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
			throw new \BadFunctionCallException('campaignWidthCustomerAndContactPersonEntity not loaded');
		}
		
		/**
		 * initCampaignEntityFromCampaignPostData
		 */
		\initCampaignEntityFromCampaignPostData(
			$_POST['campaign'],
			$campaignEntity,
			$debugLogManager
		);
        
        
        /**
         * getOrAddCustomerEntity
		 * 
		 * setCustomerData
         */
		$campaignEntity->setCustomerEntity(\getOrAddCustomerEntity(
			$_POST['campaign']['agentur'],
			$_POST['customer'],
			$debugLogManager,
			$campaignManager
		));
		
		
        /**
         * getOrAddContactPersonEntity
		 * 
		 * setContactData
         */
		$campaignEntity->setContactPersonEntity(\getOrAddContactPersonEntity(
			$_POST['contactPerson'],
			$campaignEntity->getCustomerEntity(),
			$debugLogManager,
			$campaignManager
		));
		
		
        // vertriebler
		$campaignVertriebler = isset($_POST['campaign']['vertriebler_id']) ? \intval($_POST['campaign']['vertriebler_id']) : 0;
		$contactPersonVertriebler = $campaignEntity->getContactPersonEntity()->getVertriebler_id() > 0 
			? $campaignEntity->getContactPersonEntity()->getVertriebler_id() 
			: \intval($_SESSION['benutzer_id'])
		;
		$debugLogManager->logData('campaignVertriebler', $campaignVertriebler);
		$debugLogManager->logData('contactPersonVertriebler', $contactPersonVertriebler);
		$campaignEntity->setVertriebler_id($campaignVertriebler ? $campaignVertriebler : $contactPersonVertriebler);

		$campaignEntity->setDsd_id(\DataFilterUtils::filterData(
			$_POST['campaign']['deliverySystemDistributor'],
			\DataFilterUtils::$validateFilterDataArray['number_int']
		));

		// zielgruppe
		$zielgruppeDataArray = $tmpZgDataArray = array();
		// zg
		if (\strlen($_POST['campaign']['zg']) > 0) {
			$zielgruppeDataArray[] = \DataFilterUtils::filterData(
				$_POST['campaign']['zg'],
				\DataFilterUtils::$validateFilterDataArray['special_chars']
			);
			
			$tmpZgDataArray = \explode(', ', \current($zielgruppeDataArray));
		}
		
		/**
		 * processCampaignSelection
		 * 
		 * targetGroup (campaignSelection)
		 */
		\processCampaignSelection(
			$zielgruppeDataArray,
			$tmpZgDataArray,
			$campaignEntity,
			$clientManager,
			$debugLogManager
		);
		
		/**
		 * processTargetGroupDataArray
		 * 
		 * debug
		 */
		\processTargetGroupDataArray(
			$_POST['campaign'],
			$zielgruppeDataArray,
			$tmpZgDataArray,
			$debugLogManager
		);
		$debugLogManager->logData('zielgruppeDataArray', $zielgruppeDataArray);
		
		$campaignEntity->setZielgruppe(\implode(', ', $zielgruppeDataArray));
		unset($tmpZgDataArray, $zielgruppeDataArray);
		
		if ($mailingTyp == 'NV') {
			$useCampaignStartDate = isset($_POST['campaign']['use_campaign_start_date']) ? (boolean) \intval($_POST['campaign']['use_campaign_start_date']) : 0;
			$debugLogManager->logData('useCampaignStartDate', $useCampaignStartDate);
			
			$campaignEntity->setUse_campaign_start_date($useCampaignStartDate);
		}

		// debug
		$debugLogManager->beginGroup($postAction . ' - Settings');

		switch ($postAction) {
			case 'new':
			case 'copy':
			case 'add_nv':
				require_once(DIR_Module_Campaigns . 'Actions/Campaign/addNewCampaign.php');
				break;

			case 'edit':
				require_once(DIR_Module_Campaigns . 'Actions/Campaign/editCampaign.php');
				break;
		}

		// debug
		$debugLogManager->logData('campaignEntity', $campaignEntity);

		if (isset($_POST['campaign']['benachrichtigungEmail']) 
			&& \count($_POST['campaign']['benachrichtigungEmail']) > 0
		) {
			$benachrichtigungNotiz = '';
			if (\strlen($_POST['campaign']['benachrichtigung_notiz']) > 0) {
				$benachrichtigungNotiz = \DataFilterUtils::filterData(
					$_POST['campaign']['benachrichtigung_notiz'],
					\DataFilterUtils::$validateFilterDataArray['special_chars']
				);

				$benachrichtigungNotiz = \FormatUtils::cleanUpNotes($benachrichtigungNotiz) . \chr(13) . \chr(13);
			}

			/**
			 * sendBenachrichtigungsMail
			 */
			\sendBenachrichtigungsMail(
				$campaignManager,
				$debugLogManager,
				$campaignEntity,
				$swiftMailerWebservice,
				\array_unique($_POST['campaign']['benachrichtigungEmail']),
				$benachrichtigungNotiz
			);
		}

		// debug
		$debugLogManager->endGroup();
	} elseif (\strlen($getAction) > 0) {
        switch ($getAction) {
			case 'edit_lock':
				require_once(DIR_Module_Campaigns . 'Actions/Campaign/editLockCampaign.php');
				break;

			default:
				break;
		}
	} else {
		throw new \InvalidArgumentException('no action method');
    }
} catch (\Exception $e) {
    require(DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());