<?php
session_start();
header('Content-Type: text/html; charset=ISO-8859-1');

$mandant = $_SESSION['mandant'];
$mid = $_SESSION['mID'];

include('../db_connect.inc.php');
include('../library/prognose.class.php');
include('../library/chart.php');
include('../library/util.php');
include('../library/log.php');
include('../library/mandant.class.php');
include('../library/campaignManagerWebservice.php');

$logManager = new logManager();
$mandantManager = new mandant();
$cmManager = new campaignManagerWebservice();

$vertriebler_fullname = "- nicht zugewiesen -";

function zahl_format($zahl) {
    $zahl = number_format($zahl, 0, 0, '.');
    return $zahl;
}

function datum_de($date_time, $form) {
    $date_1 = explode(' ', $date_time);
    $datum = explode('-', $date_1[0]);
    $jahr = $datum[0];
    $monat = $datum[1];
    $tag = $datum[2];

    $zeit = $date_1[1];
    $zeit_teile = explode(':', $date_1[1]);
    if ($form == "DE" || $zeit_teile[0] == "00" || $form == 'zeit') {
        $h = $zeit_teile[0];
    } else {
        $h = $zeit_teile[0];
    }
    $m = $zeit_teile[1];
    $zeit = $h . ":" . $m;
    $de = $tag . "." . $monat . "." . $jahr;

    $dat_zeit = $de . " " . $zeit;
    if ($form == 'dat') {
        return $de;
    } elseif ($form == '' || $form == "DE") {
        return $dat_zeit;
    } elseif ($form == 'zeit') {
        return $zeit;
    } elseif ($form == 'zeit_bm') {
        return $zeit;
    }
}

function newTime($date) {
    $dateParts = explode(' ', $date);
    $datePart = $dateParts[0];
    $timePart = $dateParts[1];
    $timeParts = explode(':', $timePart);
    $timePart = ($timeParts[0]) . ":" . $timeParts[1] . ":00";

    return $datePart . " " . $timePart;
}

$k_daten = mysql_query(
    "SELECT * FROM $kampagne_db WHERE k_id='" . $_GET['kid'] . "'",
    $verbindung
);
$k_zeile = mysql_fetch_array($k_daten);
$status = $k_zeile['status'];
$bearbeiter = $k_zeile['bearbeiter'];
$k_name = $k_zeile['k_name'];
$abrechnungsart = $k_zeile['abrechnungsart'];
$preis = $k_zeile['preis'];
$ap = $k_zeile['ap'];
$betreff = str_replace("'", "", $k_zeile['betreff']);
$betreff = str_replace('"', "", $betreff);
$betreff = str_replace('%20', " ", $betreff);
$mail_id = $k_zeile['mail_id'];

$vertriebler_id = $k_zeile['vertriebler_id'];
if ($vertriebler_id > 0) {
    require('../db_pep.inc.php');
    
    $v_daten = mysql_query(
        "SELECT `vorname`, `nachname` FROM `benutzer` WHERE `benutzer_id` = '$vertriebler_id'",
        $verbindung_pep
    );
    $vz = mysql_fetch_array($v_daten);
    $vertriebler_fullname = $vz['vorname'] . " " . $vz['nachname'];
}

$absendername = $k_zeile['absendername'];
$agentur = $k_zeile['agentur'];
$gebucht = $k_zeile['gebucht'];
$vorgabe_openings = $k_zeile['vorgabe_o'];
$vorgabe_klicks = $k_zeile['vorgabe_k'];
if ($vorgabe_klicks == '') {
    $vorgabe_klicks_info = '-';
} else {
    $vorgabe_klicks_info = $vorgabe_klicks . "%";
}

if ($vorgabe_openings == '') {
    $vorgabe_openings_info = "-";
} else {
    $vorgabe_openings_info = $vorgabe_openings . "%";
}

$zielgruppe = $k_zeile['zielgruppe'];
$blacklist = $k_zeile['blacklist'];
$v_system = $k_zeile['versandsystem'];
$hash = $k_zeile['hash'];

$abmelderCnt = intval($k_zeile['abmelder']);
$abmelderEmailArr = null;


$lastBenachrichtigungArr = $logManager->getLastLogByAction($log, 8, $_GET['kid']);
if (count($lastBenachrichtigungArr) > 0) {
    $lastBenachrichtigungDate = util::datum_de($lastBenachrichtigungArr['timestamp'], '');
    $lastBenachrichtigungUser = $lastBenachrichtigungArr['userid'];
    $userBenachrichtigungDataArr = $mandantManager->getUserDataById($lastBenachrichtigungUser);
    $lastBenachrichtigungData = $lastBenachrichtigungDate . " Uhr von " . utf8_decode($userBenachrichtigungDataArr['vorname']) . " " . utf8_decode($userBenachrichtigungDataArr['nachname']);
} else {
    $lastBenachrichtigungData = "offen";
}

$lastRechnungstellungArr = $logManager->getLastLogByAction($log, 6, $_GET['kid']);
if (count($lastRechnungstellungArr) > 0) {
    $lastRechnungstellungDate = util::datum_de($lastRechnungstellungArr['timestamp'], '');
    $lastRechnungstellungUser = $lastRechnungstellungArr['userid'];
    $userDataArr = $mandantManager->getUserDataById($lastRechnungstellungUser);
    $lastRechnungstellungData = $lastRechnungstellungDate . " Uhr von " . utf8_decode($userDataArr['vorname']) . " " . utf8_decode($userDataArr['nachname']);
} else {
    $lastRechnungstellungData = "offen";
}

$lastZustellungreportArr = $logManager->getLastLogByAction($log, 7, $_GET['kid']);
if (count($lastZustellungreportArr) > 0) {
    $lastZustellungreporDate = util::datum_de($lastZustellungreportArr['timestamp'], '');
    $lastZustellungreporUser = $lastZustellungreportArr['userid'];
    $userZustellungDataArr = $mandantManager->getUserDataById($lastZustellungreporUser);
    $lastZustellungreportData = $lastZustellungreporDate . " Uhr von " . utf8_decode($userZustellungDataArr['vorname']) . " " . utf8_decode($userZustellungDataArr['nachname']);
} else {
    $lastZustellungreportData = "offen";
}


$mailtyp = $k_zeile['mailtyp'];
switch ($mailtyp) {
    case 'm':
        $mailtyp_text = "Multipart";
        break;
    
    case 'h':
        $mailtyp_text = "HTML";
        break;
    
    case 't':
        $mailtyp_text = "TEXT";
        break;
    
    default:
        $mailtyp_text = "";
        break;
}

$note = $k_zeile['notiz'];
if ($note == '') {
    $note = "-";
}


$versendet = $k_zeile['versendet'];
$openings = $k_zeile['openings'];
$openings_all = $k_zeile['openings_all'];
$klicks = $k_zeile['klicks'];
$klicks_all = $k_zeile['klicks_all'];
$softbounces = $k_zeile['sbounces'];
$hardbounces = $k_zeile['hbounces'];

$datum_en = $k_zeile['datum'];
$k_datum = datum_de($datum_en, 'dat');
$k_zeit_teile = explode(' ', $datum_en);
$k_zeit = substr($k_zeit_teile[1], 0, 5);
$k_datum_teile = explode('.', $k_datum);
$timestamp2 = $timestamp;
$timestamp = mktime(0, 0, 0, $k_datum_teile[1], $k_datum_teile[0], $k_datum_teile[2]);
$wochentag = date("w", $timestamp);
switch ($wochentag) {
    case 1: $wochentag = "Montag";
        break;
    case 2: $wochentag = "Dienstag";
        break;
    case 3: $wochentag = "Mittwoch";
        break;
    case 4: $wochentag = "Donnerstag";
        break;
    case 5: $wochentag = "Freitag";
        break;
    case 6: $wochentag = "Samstag";
        break;
    case 0: $wochentag = "Sonntag";
}

if ($k_zeile['datum'] != 0) {
    $k_gestartet = $k_zeile['datum'];
    $gestartet = datum_de($k_gestartet);
    $gestartet_zeit = explode(" ", $gestartet);
    $gestartet = $gestartet_zeit[1];

    if ($k_zeile['beendet'] != 0) {
        $beendet = datum_de($k_zeile['beendet']);
        $beendet_zeit = explode(" ", $beendet);
        $beendet = $beendet_zeit[1];
    } else {
        $beendet = '';
    }

    $k_datum = datum_de($k_gestartet, 'dat');
    $k_zeit = datum_de($k_gestartet, 'zeit_bm');
} else {
    $gestartet = '';
    $beendet = '';
}

if ($_GET['hauptmail'] == 'HV') {
    $k_daten_sum = mysql_query(
		'SELECT SUM(versendet) as v' 
				. ', SUM(openings) as o' 
				. ', SUM(openings_all) as oa' 
				. ', SUM(klicks) as k' 
				. ', SUM(klicks_all) as ka' 
				. ', SUM(leads) as l' 
				. ', SUM(sbounces) as sb' 
				. ', SUM(hbounces) as hb' 
				. ', SUM(abmelder) as unsub' 
			. ' FROM ' . $kampagne_db 
			. ' WHERE nv_id = ' . intval($_GET['kid'])
		,
		$verbindung
	);
    $k_sum_zeile = mysql_fetch_array($k_daten_sum);
    
    $versendet = $k_sum_zeile['v'];
    $openings = $k_sum_zeile['o'];
    $openings_all = $k_sum_zeile['oa'];
    $klicks = $k_sum_zeile['k'];
    $klicks_all = $k_sum_zeile['ka'];
    $leads = $k_sum_zeile['l'];
    $softbounces = $k_sum_zeile['sb'];
    $hardbounces = $k_sum_zeile['hb'];

    if ($v_system == 'BM') {
        $datum_en = newTime($datum_en);
    }

    $q1 = $versendet - $gebucht;

    $usqMax = $gebucht * 3;
    $usqDiff = $usqMax - $versendet;

    $usq = round((($q1 / $gebucht) * 100), 1);
    if ($q1 > 0) {
        $usqTxt = "<span style='color:red;'>" . $usq . "%</span>";
    } else {
        $usqTxt = "<span style='color:#7D960B;'>" . $usq . "%</span>";
    }

    $usqContent = "
        <tr>
            <td class='details_hd_info'>&Uuml;SQ</td>
            <td class='details_hd_info' colspan='2' style='text-align:left'>" . $usqTxt . "</td>
        </tr>"
    ;

	$abmelderCnt = intval($k_sum_zeile['unsub']);
}

$abmelder_quote = round((($abmelderCnt / $versendet) * 100), 1);

if ($gebucht > 0) {
    $o_quote = round((($openings / $gebucht) * 100), 1);
    $k_quote = round((($klicks / $gebucht) * 100), 1);
    $o_quote_all = round((($openings_all / $gebucht) * 100), 1);
    $k_quote_all = round((($klicks_all / $gebucht) * 100), 1);
} else {
    $o_quote = 0;
    $k_quote = 0;
    $o_quote_all = 0;
    $k_quote_all = 0;
}

if ($softbounces > 0) {
    $bounces = $softbounces + $hardbounces;
    $b_quote = round((($bounces / $versendet) * 100), 1);
    $sb_quote = round((($softbounces / $bounces) * 100), 1);
    $hb_quote = round((($hardbounces / $bounces) * 100), 1);
} else {
    $bounces = 0;
    $b_quote = 0;
    $sb_quote = 0;
    $hb_quote = 0;
}

if ($status >= 5) {
    $versendet_ = "(versendet: " . zahl_format($versendet) . ")";
}

$avg_preis = ($gebucht / $versendet) * $preis;
$avg_preis = round($avg_preis, 2);

$details = "
    <table class='transp'>
        <tr>
            <td class='details_hd_info'>Buchungsdaten</td>
            <td class='details_hd_info'></td>
        </tr>
        <tr>
            <td class='details_info'>Agentur</td>
            <td class='details2_info'>" . $agentur . "</td>
        </tr>
        <tr>
            <td class='details_info2'>Kampagne</td>
            <td class='details2_info2'>" . $k_name . "</td>
        </tr>
        <tr>
            <td class='details_info'>Versanddatum</td>
            <td class='details2_info'>" . $wochentag . ", " . $k_datum . " - " . $k_zeit . "</td>
        </tr>
        <tr>
            <td class='details_info2'>Gebucht</td>
            <td class='details2_info2'>" . zahl_format($gebucht) . " " . $versendet_ . "</td>
        </tr>
        <tr>
            <td class='details_info'>Zielgruppe</td>
            <td class='details2_info'>" . $zielgruppe . "</td>
        </tr>
        <tr>
            <td class='details_info2'>Abrechnungsart:</td>
            <td class='details2_info2'>" . $abrechnungsart . "</td>
        </tr>
        <tr>
            <td class='details_info'>Preis</td>
            <td class='details2_info'>" . $preis . " Euro</td>
        </tr>
        <tr>
            <td class='details_info2'>&#216; Preis</td>
            <td class='details2_info2'>" . $avg_preis . " Euro</td>
        </tr>
        <tr>
            <td class='details_info'>Ansprechpartner</td>
            <td class='details2_info'>" . $ap . "</td>
        </tr>
        <tr>
            <td class='details_info2'>Vertriebler</td>
            <td class='details2_info2'>" . $vertriebler_fullname . "</td>
        </tr>
        <tr>
            <td class='details_hd_info'>Mailingdaten</td>
            <td class='details_hd_info'></td>
        </tr>
        <tr>
            <td class='details_info'>Betreff</td>
            <td class='details2_info'>" . $betreff . "</td>
        </tr>
        <tr>
            <td class='details_info2'>Absendername</td>
            <td class='details2_info2'>" . $absendername . "</td>
        </tr>
        <tr>
            <td class='details_info'>Versandsystem</td>
            <td class='details2_info'>" . $v_system . "</td>
        </tr>
        <tr>
            <td class='details_info2'>Mailformat</td>
            <td class='details2_info2'>" . $mailtyp_text . "</td>
        </tr>
        <tr>
            <td class='details_info'>Kategorie</td>
            <td class='details2_info'></td>
        </tr>
        <tr>
            <td class='details_info2'>Bearbeiter</td>
            <td class='details2_info2'>" . $bearbeiter . "</td>
        </tr>
        <tr>
            <td class='details_info'>Tracking Pixel</td>
            <td class='details2_info'>" . $tp . "</td>
        </tr>
        <tr>
            <td class='details_info2'>Blacklist</td>
            <td class='details2_info2'>" . $blacklist . "</td>
        </tr>
        <tr>
            <td class='details_info'>Status</td>
            <td class='details2_info'>" . $status_bez . "</td>
        </tr>
        <tr>
            <td class='details_info2'>Gestartet</td>
            <td class='details2_info2'>" . $gestartet . "</td>
        </tr>
        <tr>
            <td class='details_info'>Beendet</td>
            <td class='details2_info'>" . $beendet . "</td>
        </tr>
        <tr>
            <td class='details_hd_info'>Benachrichtigungen</td>
            <td class='details_hd_info'></td>
        </tr>
        <tr>
            <td class='details_info'>Infomail 'Neues Mailing'</td>
            <td class='details2_info'>" . $lastBenachrichtigungData . "</td>
        </tr>
        <tr>
            <td class='details_info2'>Rechnungstellung</td>
            <td class='details2_info2'>" . $lastRechnungstellungData . "</td>
        </tr>
        <tr>
            <td class='details_info'>Zustellreport</td>
            <td class='details2_info'>" . $lastZustellungreportData . "</td>
        </tr>
        <tr>
            <td class='details_hd_info'>Notiz</td>
            <td class='details_hd_info'></td>
        </tr>
        <tr>
            <td class='details2_info' colspan='2'>" . $note . "</td>
        </tr>
    </table>"
;

$technik = "
    <table class='transp'>
        <tr>
            <td class='details_info'>ID</td>
            <td class='details2_info'>" . $_GET['kid'] . "</td>
        </tr>
        <tr>
            <td class='details_info2'>Hash</td>
            <td class='details2_info2'>" . $hash . "</td>
        </tr>
        <tr>
            <td class='details_info2'>CRM - Id</td>
            <td class='details2_info2'>" . $k_zeile['crm_order_id'] . "</td>
        </tr>
    </table>"
;

if ($mailtyp == 't') {
    $o_unique = "-";
    $o_brutto = "-";
    $vorgabe_openings_info = "-";
} else {
    $o_unique = zahl_format($openings) . " (" . $o_quote . "%)";
    $o_brutto = zahl_format($openings_all) . " (" . $o_quote_all . "%)";
}

if ($abrechnungsart == 'TKP') {
    $display_leads = "style='display:none'";
}

$auswertung = "
    <table class='transp'>" . $usqContent . "
        <tr>
            <td class='details_hd_info'>&Ouml;ffnungen</td>
            <td class='details_hd_info'></td>
        </tr>
        <tr>
            <td class='details_info'>unique</td>
            <td class='details2_info'>" . $o_unique . "</td>
        </tr>
        <tr>
            <td class='details_info2'>brutto</td>
            <td class='details2_info2'>" . $o_brutto . "</td>
        </tr>
        <tr>
            <td class='details_info'>Vorgabe</td>
            <td class='details2_info'>" . $vorgabe_openings_info . "</td>
        </tr>
        <tr>
            <td class='details_hd_info'>Klicks</td>
            <td class='details_hd_info'></td>
        </tr>
        <tr>
            <td class='details_info'>unique</td>
            <td class='details2_info'>" . zahl_format($klicks) . " (" . $k_quote . "%)</td>
        </tr>
        <tr>
            <td class='details_info2'>brutto</td>
            <td class='details2_info2'>" . zahl_format($klicks_all) . " (" . $k_quote_all . "%)</td>
        </tr>
        <tr>
            <td class='details_info'>Vorgabe</td>
            <td class='details2_info'>" . $vorgabe_klicks_info . "</td>
        </tr>
        <tr " . $display_leads . ">
            <td class='details_hd_info'>Leads</td>
            <td class='details_hd_info'></td>
        </tr>
        <tr " . $display_leads . ">
            <td class='details_info'>Leads</td>
            <td class='details2_info'>" . zahl_format($leads) . "</td>
        </tr>
        <tr>
            <td class='details_hd_info'>Abmelder</td>
            <td class='details_hd_info'></td>
        </tr>
        <tr>
            <td class='details_info'>Gesamt</td>
            <td class='details2_info'>" . zahl_format($abmelderCnt) . " (" . $abmelder_quote . "%)</td>
        </tr>
        <tr>
            <td class='details_hd_info'>Bounces</td>
            <td class='details_hd_info'></td>
        </tr>
        <tr>
            <td class='details_info'>Gesamt</td>
            <td class='details2_info'>" . zahl_format($bounces) . " (" . $b_quote . "%)</td>
        </tr>
        <tr>
            <td class='details_info2'>Softbounces</td>
            <td class='details2_info2'>" . zahl_format($softbounces) . " (" . $sb_quote . "%)</td>
        </tr>
        <tr>
            <td class='details_info'>Hardbounces</td>
            <td class='details2_info'>" . zahl_format($hardbounces) . " (" . $hb_quote . "%)</td>
        </tr>
    </table>"
;

function historie() {
    global $mandant;
    include('../db_connect.inc.php');
    
    $kid = $_GET['kid'];

    $sql = mysql_query(
        "SELECT * FROM $log WHERE kid = '$kid' ORDER BY timestamp DESC",
        $verbindung
    );
    $cnt = mysql_num_rows($sql);

    if ($cnt > 0) {
        $details = "
            <table class='transp'>
                <tr>
                    <td class='details_hd_info' style='text-align:left;width:200px;'>Zeitstempel</td>
                    <td class='details_hd_info' style='text-align:left;width:250px;'>Bearbeiter</td>
                    <td class='details_hd_info' style='text-align:left;width:50%;'>Aktion</td>
                </tr>"
        ;

        while ($z = mysql_fetch_array($sql)) {
            $status = $z['status'];
            switch ($status) {
                case 1: $status_bez = "Werbemittel";
                    break;
                case 2: $status_bez = "Optimiert";
                    break;
                case 3: $status_bez = "Testmails";
                    break;
                case 4: $status_bez = "Freigabe";
                    break;
                case 5: $status_bez = "versendet";
                    break;
                case 6: $status_bez = "senden";
                    break;
                case 7: $status_bez = "abgebr.";
                    break;
                case 10: $status_bez = "Anfrage";
                    break;
                case 11: $status_bez = "Warte auf Best&auml;tigung";
                    break;
                case 12: $status_bez = "Warte auf Werbemittel";
                    break;
                case 20: $status_bez = "Report intern verschickt";
                    break;
                case 21: $status_bez = "Erstreporting abgeschlossen";
                    break;
                case 22: $status_bez = "Endreporting abgeschlossen";
                    break;
                case 23: $status_bez = "Reportfertig";
                    break;
                case 30: $status_bez = "Rechnung erhalten";
                    break;
                case 31: $status_bez = "Rechnung best&auml;tigt";
                    break;
                case 32: $status_bez = "Rechnung gecheckt";
                    break;
                case 33: $status_bez = "Rechnung bezahlt";
                    break;
                default: $status_bez = '';
            }

            if ($status_bez != '') {
                $status_neu = " auf: '" . $status_bez . "'";
            } else {
                $status_neu = '';
            }

            $mandantManager = new mandant();
            $userArr = $mandantManager->getUserDataById($z['userid']);
            
            $details .= "
                <tr>
                    <td class='details2_info' style='height:15px;width:300px'>" . util::datum_de($z['timestamp']) . " Uhr</td>
                    <td class='details2_info' style='height:15px;width:30%'>" . utf8_decode($userArr['vorname']) . " " . utf8_decode($userArr['nachname']) . "</td>
                    <td class='details2_info' style='height:15px;width:30%'>" . util::getActionName($z['action']) . $status_neu . "</td>
                </tr>"
            ;
        }

        $details .= "</table'>";
    } else {
        $details .= "<table><tr><td align='center' height='30' width='600' style='color:#666666'>Keine Eintr&auml;ge vorhanden</td></tr></table>";
    }

    return $details;
}

$cm_reportingArr = $cmManager->getCmReporting($cm_rep_table, $_GET['kid']);
if (count($cm_reportingArr) > 0) {
    $cm_report_content = "<table class='transp'>";
    foreach ($cm_reportingArr as $cmKey => $cmReportArr) {
        $cm_report_content .= "
                <tr>
                    <td colspan='2' class='details_hd_info' style='text-align:left;color:#9933FF' height='25'>" . $cmReportArr['typ'] . " vom: " . datum_de($cmReportArr['created'], '') . " Uhr</td>
		</tr>
                <tr>
                    <td class='details2_info' style='font-weight:bold;width:150px;'>Betreff: </td>
                    <td class='details2_info'>" . $cmReportArr['betreff'] . "</td>
		</tr>
                 <tr>
                    <td class='details2_info' style='font-weight:bold;width:150px;'>Empf&auml;nger: </td>
                    <td class='details2_info'>" . $cmReportArr['empfaenger'] . "</td>
		</tr>
                 <tr>
                    <td class='details2_info' style='font-weight:bold;width:150px;'>Absender: </td>
                    <td class='details2_info'>" . $cmReportArr['absender'] . "</td>
		</tr>
                 <tr>
                    <td colspan='2' style='background-color:#FFFFFF;padding:5px;'>" . $cmReportArr['report'] . "</td>
		</tr>";
    }
    $cm_report_content .= "</table>";
} else {
    $cm_report_content = 'Keine Reporting-Historie vorhanden';
}

$abmelderEmail = "Email\r\n";
if (count($abmelderEmailArr) > 0) {
    foreach ($abmelderEmailArr as $emailUns => $e) {
        $abmelderEmail .= $e . "\r\n";
    }
}
$_SESSION['datensaetze_unsub'] = $abmelderEmail;
?>

<div id="demo_div" class="yui-navset" style="padding:5px;text-align:left;border:0px;width:600">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class="selected"><a href="#tab1"><em>Allgemein</em></a></li>
        <li><a href="#tab2"><em>Auswertung</em></a></li>

        <?php
        if (($mailtyp == 'h' || $mailtyp == 'm') && $mail_id > 0 && ($v_system == 'M' || $v_system == 'SE')) {
            print '<li><a href="#tab3"><em>HTML - Vorschau</em></a></li>';
        }

       /* if (($mailtyp == 't' || $mailtyp == 'm') && $mail_id > 0 && ($v_system == 'M' || $v_system == 'SE')) {
            print '<li><a href="#tab4"><em>TEXT - Vorschau</em></a></li>';
        }*/
        ?>

        <li><a href="#tab6"><em>Technische Daten</em></a></li>
        <li><a href="#tab5"><em>Reporting</em></a></li>
        <li><a href="#tab7"><em>Historie</em></a></li>

    </ul>

    <div id="bottom_content_info" class="yui-content" style="height:350px;overflow:auto;margin:0px;padding:0px;">
        <div id="tab1"><?php print $details; ?></div>
        <div id="tab2"><?php print $auswertung; ?></div>
        <?php
        if (($mailtyp == 'h' || $mailtyp == 'm') && $mail_id > 0 && ($v_system == 'M' || $v_system == 'SE' || $v_system == 'PM')) {
            print '
                <div id="tab3" style="overflow:hidden;border:0px;">
                    <iframe id="iframe_html" style="width:100%;height:350px;border:0px;padding:0px;" src="Module/Campaigns/View/Campaign/preview.php?campaign[k_id]=' . $k_zeile['k_id'] . '&mailingType=html"></iframe>
                </div>'
            ;
        }

        /*if (($mailtyp == 't' || $mailtyp == 'm') && $mail_id > 0 && ($v_system == 'M' || $v_system == 'SE')) {
            print '
                <div id="tab4" style="overflow:hidden">
                    <iframe id="iframe_text" style="width:100%;height:350px;border:0px;padding:0px;" src="Module/Campaigns/View/Campaign/preview.php?campaign[k_id]=' . $k_zeile['k_id'] . '&mailingType=text"></iframe>
                </div>'
            ;
        }*/
        ?>
        <div id="tab6"><?php print $technik; ?></div>
        <div id="tab7"><?php print $cm_report_content; ?></div>
        <div id="tab5"><?php print historie(); ?></div>
    </div>
</div>

<script type="text/javascript">
    var tabView = new YAHOO.widget.TabView('demo_div');
</script>