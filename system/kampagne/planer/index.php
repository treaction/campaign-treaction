<?php
require_once('../../emsConfig.php');
require_once('../../EmsAutoloader.php');

\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();
\EmsAutoloader::initErrorReporting();

\header('Content-Type: text/html; charset=utf-8');


/**
 * createCsvAndUserAndEditTableContent
 * 
 * @param array $userTabFieldsDataArray
 * @param string $tableHeaderContent
 * @param string $tableEditHeaderContent
 * @param array $csvHeaderDataArray
 * @return void
 */
function createCsvAndUserAndEditTableContent(array $userTabFieldsDataArray, &$tableHeaderContent, &$tableEditHeaderContent, array &$csvHeaderDataArray) {
	// createTableRow - Tag
	$tableEditHeaderRowDataArray = \HtmlTableUtils::createTableRow(
		array(
			'id' => 'data_tab_felder_edit',
			'class' => 'hidden'
		)
	);
	
	// createTableRow - Tag
	$tableHeaderRowDataArray = \HtmlTableUtils::createTableRow();
	
	$tableEditHeaderContent = 
		$tableEditHeaderRowDataArray['begin'] 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				''
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				''
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				''
			)
	;
	
	$tableHeaderContent = 
		$tableHeaderRowDataArray['begin'] 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Datum'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Status'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Kampagne'
			)
	;
	
	foreach ($userTabFieldsDataArray as $userField) {
		foreach (\CampaignAndCustomerUtils::$allTabFieldsDataArray as $tabField => $tabArray) {
			if ($userField == $tabField) {
				switch ($tabArray['sublabel']) {
					case 'Re-St':
						$title = $tabArray['label'];
						break;
					
					case 'Z-Rep':
						$title = $tabArray['label'];
						break;
					
					case 'Infomail':
						$title = $tabArray['label'];
						break;
					
					default:
						$title = '';
						break;
				}
				
				$tableEditHeaderContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
					array(
						'class' => $tabField,
						'align' => 'center'
					),
					'<img class="delFeld" id="' . $tabField . '"  src="../img/Tango/16/actions/dialog-close.png" title="Spalte entfernen" />'
				);
				
				$tableHeaderContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
					array(
						'class' => $tabField,
						'title' => $title
					),
					$tabArray['sublabel']
				);
				
				$csvHeaderDataArray[$tabField] = \utf8_encode(\html_entity_decode($tabArray['sublabel']));
			}
		}
	}
	
	$tableEditHeaderContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
		array(
			'width' => '100%'
		),
		''
	) . $tableEditHeaderRowDataArray['end'];
	
	$tableHeaderContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
		array(
			'width' => '100%'
		),
		''
	) . $tableHeaderRowDataArray['end'];
}

/**
 * getSyncContentByDeliverySystem
 * 
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @return string
 */
function getSyncContentByDeliverySystem(\CampaignWidthCustomerAndContactPersonEntity $campaignEntity) {
	$result = '';

	if (\strlen($campaignEntity->getMail_id()) === 0 
		&& (
			$campaignEntity->getVersandsystem() == 'BM' 
			|| $campaignEntity->getVersandsystem() == 'K'
                        || $campaignEntity->getVersandsystem() == 'SE'
                        || $campaignEntity->getVersandsystem() == 'M'
                        || $campaignEntity->getVersandsystem() == '4W18'
                        || $campaignEntity->getVersandsystem() == '4WMS'
                        || $campaignEntity->getVersandsystem() == 'S18'
                        || $campaignEntity->getVersandsystem() == 'SB'
                        || $campaignEntity->getVersandsystem() == 'SS'
                        || $campaignEntity->getVersandsystem() == '4WB'
                        || $campaignEntity->getVersandsystem() == '4WBL'
                        || $campaignEntity->getVersandsystem() == 'SBL'
                        || $campaignEntity->getVersandsystem() == 'SSE'
                        || $campaignEntity->getVersandsystem() == '4WSE'                
                        || $campaignEntity->getVersandsystem() == 'SRES'
                        || $campaignEntity->getVersandsystem() == '4WE'  
                        || $campaignEntity->getVersandsystem() == 'LWB' 
                        || $campaignEntity->getVersandsystem() == 'PM'
                        || $campaignEntity->getVersandsystem() == 'BL19'
                        || $campaignEntity->getVersandsystem() == 'BLB' 
                        || $campaignEntity->getVersandsystem() == 'FM19'
                        || $campaignEntity->getVersandsystem() == 'FMB'               
                        || $campaignEntity->getVersandsystem() == 'BLE'
                        || $campaignEntity->getVersandsystem() == 'FME' 
                        || $campaignEntity->getVersandsystem() == 'BLBL'    
                        || $campaignEntity->getVersandsystem() == 'FMBL'
                        || $campaignEntity->getVersandsystem() == 'CEOO'
                        || $campaignEntity->getVersandsystem() == 'CEOOBL'                 
                        || $campaignEntity->getVersandsystem() == 'CSE'                
                )
	) {
		$result = ' <img src="img/icons/swap.png" alt="bitte synchronisieren" title="bitte synchronisieren" />';
	}

	return $result;
}

/**
 * getEmsUnsubscribeCountByDate
 * 
 * @param \UnsubscribeManager $unsubscribeManager
 * @param \debugLogManager $debugLogManager
 * @param integer $year
 * @param integer $month
 * @return false|integer
 */
function getEmsUnsubscribeCountByDate(\UnsubscribeManager $unsubscribeManager, \debugLogManager $debugLogManager, $year, $month) {
	// setClientTable
	$unsubscribeManager->setClientTable('mandant_' . $_SESSION['mID']);
	
	/**
	 * checkIfTableExist
	 */
	if ((boolean) ($unsubscribeManager->checkIfTableExist()) === true) {
		/**
		 * getClientCountDataForEmsByMonthAndYear
		 * 
		 * debug
		 */
		try {
			$result = $unsubscribeManager->getClientCountDataForEmsByMonthAndYear(
				$month,
				$year
			);
			$debugLogManager->logData(__FUNCTION__, $result);
		} catch (\Exception $e) {
			require(\DIR_configs . 'exceptions.php');
			
			die($exceptionMessage);
		}
	}
	
	return $result;
}



$mandant = $_SESSION['mandant'];
require_once('../../db_connect.inc.php');


/**
 * init $debugLogManager
 * init $campaignManager
 * init $logManager
 * init $clientManager
 */
require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(\DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */

require_once(\DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

require_once(\DIR_configsInit . 'initUnsubscribeManager.php');
/* @var $unsubscribeManager \UnsubscribeManager */


$umsatzView = $dayAnkerSet = false;
if ($_SESSION['abteilung'] == 'g' 
	|| $_SESSION['abteilung'] == 'v'
	|| $_SESSION['abteilung'] == 'b'
) {
	$umsatzView = true;
} elseif ((int) $_SESSION['benutzer_id'] === 1) {
	$umsatz_view = true;
}


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);
$debugLogManager->logData('umsatzView', $umsatzView);

$_SESSION['planerView'] = array(
	'textLenght' => 40,
	'bounceHThreshold' => $bounceHThreshold,
	'bounceSThreshold' => $bounceSThreshold
);

$year = isset($_GET['jahr']) ? \intval($_GET['jahr']) : \date('Y');
$debugLogManager->logData('year', $year);

$month = isset($_GET['monat']) ? \intval($_GET['monat']) : (int) \date('m');
if ($month < 10) {
	$month = 0 . $month;
}
$debugLogManager->logData('month', $month);

$sort = isset($_GET['sort']) 
	? \DataFilterUtils::filterData(
		$_GET['sort'],
		\DataFilterUtils::$validateFilder['full_special_chars']
	) 
	: 'ASC'
;
$debugLogManager->logData('sort', $sort);

$sort_ev = ($sort == 'DESC' ? 'ASC' : 'DESC');
$debugLogManager->logData('sort_ev', $sort_ev);


// debug
$debugLogManager->beginGroup('viewOptionsSettings');

$view = isset($_GET['view']) 
	? \DataFilterUtils::filterData(
		$_GET['view'],
		\DataFilterUtils::$validateFilder['full_special_chars']
	) 
	: 'month'
;
$debugLogManager->logData('view', $view);

$abrTyp = isset($_GET['abrTyp']) 
	? \DataFilterUtils::filterData(
		$_GET['abrTyp'],
		\DataFilterUtils::$validateFilder['full_special_chars']
	) 
	: 'all'
;
$debugLogManager->logData('abrTyp', $abrTyp);

$deliverySystem = isset($_GET['deliverySystem']) 
	? \DataFilterUtils::filterData(
		$_GET['deliverySystem'],
		\DataFilterUtils::$validateFilder['full_special_chars']
	) 
	: 'all'
;
$debugLogManager->logData('deliverySystem', $deliverySystem);

$deliverySystemDistributor = isset($_GET['deliverySystemDistributor']) ? \intval($_GET['deliverySystemDistributor']) : 'all';
$debugLogManager->logData('deliverySystemDistributor', $deliverySystemDistributor);

$campaignAnchor = isset($_GET['campaignAnchor']) ? '#' . \DataFilterUtils::filterData($_GET['campaignAnchor']) : '#HEUTE';
$debugLogManager->logData('campaignAnchor', $campaignAnchor);

$debugLogManager->endGroup();


$monthCountDay = \DateUtils::getDayCountByMonthAndYear(
	$month,
	$year
);
$debugLogManager->logData('monthCountDay', $monthCountDay);

$today = new \DateTime('now');
$debugLogManager->logData('today', $today);


require_once('include/viewManager.php');


// selection - begin
$campaignQueryPartsDataArray = array(
	'SELECT' => '*',
	'ORDER_BY' => '`datum` ' . $sort
);
$whereCampaignQueryPartsDataArray = array(
	'status' => array(
		'sql' => '`status`',
		'value' => 10,
		'comparison' => 'fieldNotEqual'
	),
	'year' => array(
		'sql' => 'YEAR(`datum`)',
		'value' => $year,
		'comparison' => 'integerEqual'
	),
	'month' => array(
		'sql' => 'MONTH(`datum`)',
		'value' => $month,
		'comparison' => 'integerEqual'
	)
);

if (\strlen($abrTypSql)) {
	$whereCampaignQueryPartsDataArray['other'] = array(
		'sql' => '1 = 1 ' . $abrTypSql,
		'value' => '',
		'comparison' => ''
	);
}

switch ($view) {
	case 'week':
		$whereCampaignQueryPartsDataArray['nvId'] = array(
			'sql' => 'nv_id',
			'value' => 'k_id',
			'comparison' => 'fieldEqual'
		);
		$whereCampaignQueryPartsDataArray['kName'] = array(
			'sql' => 'LEFT(`k_name`, 3) != "NV:"',
			'value' => '',
			'comparison' => ''
		);
		
		$abrTypSql .= ' AND `nv_id` = `k_id`' 
			. ' AND LEFT(`k_name`, 3) != "NV:"'
		;
		break;
	
	case 'monthSum':
		$whereCampaignQueryPartsDataArray['nvId'] = array(
			'sql' => 'nv_id',
			'value' => 'k_id',
			'comparison' => 'fieldEqual'
		);
		
		$abrTypSql .= ' AND `nv_id` = `k_id`';
		break;
}

$campaignQueryPartsDataArray['WHERE'] = $whereCampaignQueryPartsDataArray;
$debugLogManager->logData('campaignQueryPartsDataArray', $campaignQueryPartsDataArray);

$tableHeaderContent = $tableEditHeaderContent = $tableData = $timestamp = '';
$defaultCsvHeaderDataArray = array(
	'agentur' => 'Agentur',
	'status' => 'Status',
	'k_name' => 'Kampagne'
);
$csvHeaderDataArray = $defaultCsvHeaderDataArray;
$csvItemDataArray = array();

try {
	/**
	 * getMonthCampaignsCountDataArray
	 * 
	 * debug
	 */
	$monthCampaignsCountDataArray = $campaignManager->getMonthCampaignsCountDataArray(
		$year,
		$abrTypSql
	);
	$debugLogManager->logData('monthCampaignsCountDataArray', $monthCampaignsCountDataArray);
	
	require_once('include/monthButtons.php');
	
	
	/**
	 * getTabFieldsDataArrayByIdUser
	 * 
	 * debug
	 */
	$userTabFieldsDataArray = $clientManager->getTabFieldsDataArrayByIdUser($_SESSION['benutzer_id']);
	$debugLogManager->logData('userTabFieldsDataArray', $userTabFieldsDataArray);
	
	
	/**
	 * createCsvAndUserAndEditTableContent
	 */
	\createCsvAndUserAndEditTableContent(
		$userTabFieldsDataArray,
		$tableHeaderContent,
		$tableEditHeaderContent,
		$csvHeaderDataArray
	);
	
	$csvItemCount = $campaignCount = 0;
	if (\array_key_exists((int) $month, $monthCampaignsCountDataArray)) {
		$campaignCount = (int) $monthCampaignsCountDataArray[(int) $month];
	}
	$debugLogManager->logData('campaignCount', $campaignCount);
	
	$salesByBillingType = array();
	foreach (\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray as $billingType) {
		$salesByBillingType[$billingType] = 0;
	}
	$salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['Hybri'] . '_u'] = 0;
	$salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['CPL'] . '_u'] = 0;
	$salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['CPO'] . '_u'] = 0;
	$salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['CPC'] . '_u'] = 0;
	
	$recipientDataArray = array(
		'abmelder' => 0,
		'gebucht' => 0,
		'versendet' => 0
	);
	
	if ($campaignCount > 0) {
		\CampaignFieldsUtils::$textLenght = $_SESSION['planerView']['textLenght'];
		\CampaignFieldsUtils::$bounceHThreshold = $_SESSION['planerView']['bounceHThreshold'];
		\CampaignFieldsUtils::$bounceSThreshold = $_SESSION['planerView']['bounceSThreshold'];
		
		$hvAndNvCampaigEntity = new \CampaignEntity();
		if (!($hvAndNvCampaigEntity instanceof \CampaignEntity)) {
			throw new \DomainException('no CampaignEntity');
		}
		
		// debug
		$debugLogManager->beginGroup($view . 'View');
		
		switch ($view) {
			case 'week':
				require_once('pageView/weekView.php');
				break;
			
			case 'monthSum':
				require_once('pageView/monthSumView.php');
				break;
			
			default:
				// month
				require_once('pageView/monthView.php');
				break;
		}
		
		// debug
		$debugLogManager->endGroup();
	} else {
		require_once('viewData/viewData_noMailingsFound.php');
		$tableData .= $tableNoContent;
	}
	
	// debug
	$debugLogManager->logData('salesByBillingType', $salesByBillingType);
	
	
	/**
	 * getCampaignDataItemById
	 * 
	 * debug
	 */
	$syncLastTimeCampaignEntity = $campaignManager->getCampaignDataItemById(1);
	if (!($syncLastTimeCampaignEntity instanceof \CampaignEntity)) {
		throw new \DomainException('no CampaignEntity');
	}
	$debugLogManager->logData('syncLastTime', $syncLastTimeCampaignEntity);
	
	/**
	 * getEmsUnsubscribeCountByDate
	 * 
	 * debug
	 */
	$recipientDataArray['abmelder'] += \getEmsUnsubscribeCountByDate(
		$unsubscribeManager,
		$debugLogManager,
		$year,
		$month
	);
	$debugLogManager->logData('cntRecipientArr', $recipientDataArray);
} catch (\Exception $e) {
	require(\DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}

$_SESSION['csvExportData'] = array(
	'header' => $csvHeaderDataArray,
	'data' => $csvItemDataArray
);

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
?>
<input type="hidden" id="page_planer" value="1" />
<input type="hidden" id="planer_jahr" value="<?php echo $year; ?>" />
<input type="hidden" id="planer_monat" value="<?php echo $month; ?>" />
<input type="hidden" id="planer_sort" value="<?php echo $sort; ?>" />
<input type="hidden" id="planer_view" value="<?php echo $view; ?>" />
<input type="hidden" id="abrTyp" value="<?php echo $abrTyp; ?>" />
<input type="hidden" id="deliverySystem" value="<?php echo $deliverySystem; ?>" />
<input type="hidden" id="deliverySystemDistributor1" value="<?php echo $deliverySystemDistributor; ?>" />
<div style="background-color:#FFFFFF;">
<?php
	require_once('include/viewOption.php');
?>
</div>
<div id="fakeContainer">
    <table class="scrollTable" id="scrollTable" cellpadding="0" cellspacing="0" border="0" bgcolor="#F5F5F5">
        <?php
			echo $tableHeaderContent 
				. $tableData
			;
		?>
    </table>
</div>
<script type="text/javascript">
	function Resize() {
		setRequest(
			'kampagne/planer/index.php' 
				+ '?jahr=<?php echo $year; ?>' 
				+ '&monat=<?php echo $month; ?>' 
				+ '&view=<?php echo $view; ?>'
				+ '&abrTyp=<?php echo $abrTyp; ?>'
				+ '&sort=<?php echo $sort_ev; ?>',
			'p'
		);
	}
</script>
<?php 
	require_once('include/tablejs.php');