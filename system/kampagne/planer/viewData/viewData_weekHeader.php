<?php
$tableWeekHeaderRowDataArray = \HtmlTableUtils::createTableRow(
	array(
		'style' => 'background: #fff;'
	)
);

if ((int) $week === (int) $today->format('W')) {
	$weekHeaderStyle = 'height:35px;vertical-align:bottom;font-weight:bold;color:red;border-bottom:2px solid red;';
} else {
	$weekHeaderStyle = 'height:35px;vertical-align:bottom;font-weight:bold;color:#40547A;border-bottom:2px solid #40547A;';
}

$tableWeekHeader = 
	$tableWeekHeaderRowDataArray['begin']
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'style' => $weekHeaderStyle,
				'colspan' => \count($userTabFieldsDataArray),
			),
			\DateUtils::$monthDataArray[(int) $month] . ', KW ' . \intval($week)
		)
	. $tableWeekHeaderRowDataArray['end']
;
unset($tableWeekHeaderRowDataArray);