<?php
/* @var $campaignManager \CampaignManager */
/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $hvAndNvCampaigEntity \CampaignEntity */
/* @var $statusDataArray \CampaignAndCustomerUtils */


$onClick = 'doIt(event'
	. ', \'' . $campaignEntity->getMailtyp() . '\''
	. ', \'' . $campaignEntity->getMail_id() . '\''
	. ', \'' . \htmlspecialchars($campaignEntity->getCustomerEntity()->getFirma() . ', ' . $campaignEntity->getK_name()) . '\''
	. ' , ' . $campaignEntity->getK_id()
	. ', \'' . $campaignType . '\''
	. ', ' . $campaignEntity->getStatus()
	. ', \'' . \htmlspecialchars($campaignEntity->getCustomerEntity()->getFirma()) . '\''
	. ', \'' . \htmlspecialchars($campaignEntity->getK_name()) . '\''
	. ' ,\'' . $campaignEntity->getDatum()->format('Y-m-d') . '\''
	. ', \'' . $campaignEntity->getDatum()->format('H:i') . '\''
	. ', \'' . $campaignEntity->getGebucht() . '\''
	. ', \'' . $campaignEntity->getVorgabe_o() . '\''
	. ', \'' . $campaignEntity->getVorgabe_k() . '\''
	. ', \'' . $campaignEntity->getBlacklist() . '\''
	. ', \'' . $campaignEntity->getVersandsystem() . '\''
	. ', \'' . \htmlspecialchars($campaignEntity->getBearbeiter()) . '\''
	. ', \'' . $campaignEntity->getTkp() . '\''
	. ', \'' . \htmlspecialchars($campaignEntity->getContactPersonEntity()->getVorname() . ' ' . $campaignEntity->getContactPersonEntity()->getNachname()) . '\''
	. ', \'\''
	. ', \'' . $campaignEntity->getOpenings() . '\''
	. ', \'' . $campaignEntity->getOpenings_all() . '\''
	. ', \'' . $campaignEntity->getKlicks() . '\''
	. ', \'' . $campaignEntity->getKlicks_all() . '\''
	. ', \'' . $campaignEntity->getVersendet() . '\''
	. ', \'main\''
	. ', ' . \intval($_SESSION['rechte'])
	. ', \'' . $mandant . '\''
	. ');HighLightTR(\'' . $bgColorFest . '\', ' . $campaignEntity->getK_id() . ', \'HV\');'
;

if ($campaignType == 'HV') {
	$tableContentRowDataArray = \HtmlTableUtils::createTableRow(
		array(
			'id' => 's' . $campaignEntity->getK_id(),
			'class' => $campaignEntity->getK_id() . ', container',
			'onclick' => $onClick,
			'bgcolor' => $bgColorFest,
			'onMouseOver' => 'this.bgColor=\'#A6CBFF\'',
			'onMouseOut' => 'this.bgColor=\'' . $bgColor . '\''
		)
	);
} else {
	$tableContentRowDataArray = \HtmlTableUtils::createTableRow(
		array(
			'id' => 'snv' . $campaignEntity->getK_id(),
			'class' => $campaignEntity->getK_id() . ', container',
			'onclick' => $onClick,
			'bgcolor' => $bgColorFest,
			'onMouseOver' => 'this.bgColor=\'#A6CBFF\'',
			'onMouseOut' => 'this.bgColor=\'' . $bgColor . '\'',
			'data-hvCampaignId' => $campaignEntity->getNv_id(),
			'data-currentCampaignId' => $campaignEntity->getK_id(),
		)
	);
}

$v_zeichen = '';
if ($campaignEntity->getVorgabe_k() != $cm_vorgabe_k 
	|| $campaignEntity->getVorgabe_o() != $cm_vorgabe_o
) {
	$v_zeichen = '<a href="#" title="Ziel &Ouml;ffnung: ' . $campaignEntity->getVorgabe_o() . '%, Ziel Klicks: ' . $campaignEntity->getVorgabe_k() . '%" style="color: red;"><b>*</b></a>';
}


if ($campaignEntity->getStatus() === 5 
	|| $campaignEntity->getStatus() === 22 
	|| $campaignEntity->getStatus() >= 30
) {
	$campaignStatusColor = '#666666';
} else {
	$campaignStatusColor = $_SESSION['campaign']['statusDataArray'][$campaignEntity->getStatus()]['campaignStatusColor'];
}

$contentList = 
	$tableContentRowDataArray['begin'] 
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'style' => 'border-right:1px solid #DBB7FF;font-weight:bold;color:#565656;text-align:right;' . $bgTodayStyle
			),
			$dateTodayHeader
		) 
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'style' => 'border-right:1px solid #DBB7FF;'
			),
			'<span style="color: ' . $campaignStatusColor . ';">' 
				. $_SESSION['campaign']['statusDataArray'][$campaignEntity->getStatus()]['campaignViewLabel'] 
			. '</span>'
		) 
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'style' => 'border-right:1px solid #DBB7FF;',
				'title' => \htmlspecialchars($campaignEntity->getK_name() . ', ' . $campaignEntity->getCustomerEntity()->getFirma())
			),
			'<div style="font-weight: bold;">'
				. \FormatUtils::cutString(
					$campaignEntity->getK_name(),
					\CampaignFieldsUtils::$textLenght
				) 
				. $v_zeichen
				. \getSyncContentByDeliverySystem($campaignEntity) . '<br />'
				. '<span style="font-size: 10px; color: #9933FF;">'
					. \FormatUtils::cutString(
						$campaignEntity->getCustomerEntity()->getFirma(),
						\CampaignFieldsUtils::$textLenght
					)
				. '</span>'
			. '</div>'
		) 
;

foreach ($defaultCsvHeaderDataArray as $csvKey => $csvItem) {
	switch ($csvKey) {
		case 'agentur':
			// html_entity_decode
			$csvItemDataArray[$csvItemCount][$csvKey] = \trim($campaignEntity->getCustomerEntity()->getFirma());
			break;
		
		case 'status':
			$csvItemDataArray[$csvItemCount][$csvKey] = \str_replace('<br />', ' ', $_SESSION['campaign']['statusDataArray'][$campaignEntity->getStatus()]['label']);
			break;
		
		case 'k_name':
			// html_entity_decode
			$csvItemDataArray[$csvItemCount][$csvKey] = \trim($campaignEntity->getK_name());
			break;
	}
}


$contentList .= 
		\CampaignFieldsUtils::getCellContentByUserTabFieldsDataArray(
			$campaignManager,
			$campaignEntity,
			$hvAndNvCampaigEntity,
			$today,
			$userTabFieldsDataArray,
			false,
			$campaignType,
			$csvItemDataArray[$csvItemCount]
		) 
	. $tableContentRowDataArray['end']
;

switch ($campaignEntity->getAbrechnungsart()) {
	case 'Hybri':
	case 'Hybrid':
	case 'CPL':
	case 'CPO':
	case 'CPC':
		$salesByBillingType[$campaignEntity->getAbrechnungsart() . '_u'] += \FormatUtils::validPriceCalculationByBillingType(
			$campaignEntity,
			TRUE,
            TRUE
		) + $campaignEntity->getUnit_price();
		
		$salesByBillingType[$campaignEntity->getAbrechnungsart()] += \FormatUtils::totalPriceCalculationByBillingType(
			$campaignEntity,
			TRUE
		);
		break;
		
	default:
		$salesByBillingType[$campaignEntity->getAbrechnungsart()] += \FormatUtils::totalPriceCalculationByBillingType(
			$campaignEntity,
			TRUE
		);
		break;
}

$recipientDataArray['abmelder'] += $campaignEntity->getAbmelder();
$recipientDataArray['versendet'] += $campaignEntity->getVersendet();