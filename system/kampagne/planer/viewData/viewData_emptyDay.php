<?php
/* @var $debugLogManager \debugLogManager */

$emptyDate = new \DateTime($year . '-' . $month . '-' . $day);
$debugLogManager->logData('emptyDate', $emptyDate);

$actWeekDay = \DateUtils::getWeekDay(
	$emptyDate,
	true
);
if ($actWeekDay == 'Sa.' || $actWeekDay == 'So.') {
	$bgColorFest = $bgColor = '#C4DDFF';
}

$isToday = \DateUtils::isDateToday($emptyDate);
if ((boolean) $isToday === true) {
	$bgTodayStyle = 'background-color:#E63C3C; color:#FFFFFF';
	$dateTodayHeader = 'Heute';
	
	if (!$dayAnkerSet) {
		$dateTodayHeader .= '<a href="#" id="HEUTE"></a>';
		$dayAnkerSet = true;
	}
} else {
	$dateTodayHeader = $actWeekDay . ', ' . $emptyDate->format('d.m.Y');
	$bgTodayStyle = $bgColor;
}


$tableEmptyDayContentRowDataArray = \HtmlTableUtils::createTableRow(
	array(
		'bgcolor' => $bgColorFest,
		'class' => 'container'
	)
);

$tableEmptyDayContent = 
	$tableEmptyDayContentRowDataArray['begin']
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'style' => 'border-right:1px solid #DBB7FF;font-weight:bold;color:#565656;text-align:right;' . $bgTodayStyle,
			),
			'<a href="#" name="day"' . $day . '"></a>' . $dateTodayHeader
		) 
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'style' => 'color:#999999',
				'colspan' => \count($userTabFieldsDataArray) + 2,
			),
			'&nbsp;'
		)
	. $tableEmptyDayContentRowDataArray['end']
;
unset($tableEmptyDayContentRowDataArray);