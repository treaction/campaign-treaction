<?php
$tableNoContentRowDataArray = \HtmlTableUtils::createTableRow(
	array(
		'style' => 'background: #fff;'
	)
);

$tableNoContent = 
	$tableNoContentRowDataArray['begin']
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'style' => 'text-align:left;padding:10px;color:#666666',
				'colspan' => \count($userTabFieldsDataArray) + 3,
			),
			'Keine Mailings gefunden'
		)
	. $tableNoContentRowDataArray['end']
;
unset($tableNoContentRowDataArray);