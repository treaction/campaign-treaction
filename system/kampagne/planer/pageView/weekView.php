<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */

$weekBeginObj = new \DateTime();
$weekBeginObj->setDate($year, $month, 1);

$weekEndObj = new \DateTime();
$weekEndObj->setDate($year, $month, $monthCountDay);

switch ($month) {
    case 12:
        // dezember
        if (\intval($weekEndObj->format('W')) === 1) {
            $tmpDateTime = new \DateTime();
            $tmpDateTime->setDate($year, $month, \DateUtils::getDayCountByMonthAndYear($month, $year) - 2);
            
            $weekEnd = \intval($tmpDateTime->format('W')) + 1;
            unset($tmpDateTime);
        } else {
            $weekEnd = \intval($weekEndObj->format('W'));
        }
        
        $weekBegin = \intval($weekBeginObj->format('W'));
        break;
    
    case 1:
        // januar
        if (\intval($weekBeginObj->format('W')) >= 52) {
            $weekBegin = 0;
        } else {
            $weekBegin = \intval($weekBeginObj->format('W'));
        }
        
        $weekEnd = \intval($weekEndObj->format('W'));
        break;
    
    default:
        $weekBegin = \intval($weekBeginObj->format('W'));
        $weekEnd = \intval($weekEndObj->format('W'));
        break;
}

// debug
$debugLogManager->logData('weekBeginObj', $weekBeginObj);
$debugLogManager->logData('weekEndObj', $weekEndObj);
$debugLogManager->logData('weekBegin', $weekBegin);
$debugLogManager->logData('weekEnd', $weekEnd);

for ($week = $weekBegin; $week <= $weekEnd; $week++) {
	$debugLogManager->beginGroup('week - ' . $week);
	
	// viewData_weekHeader
	require('viewData/viewData_weekHeader.php');
	$tableData .= $tableWeekHeader;
	
	/**
	 * getDayDateTimeFromWeek
	 * 
	 * debug
	 */
	$weekDaysDataArray = \DateUtils::getDayDateTimeFromWeek(
		$year,
		$week
	);
	foreach ($weekDaysDataArray as $weekDay) {
		/* @var $weekDay DateTime */
		
		$day = $weekDay->format('d');
		
		// debug
		$debugLogManager->beginGroup('day - ' . $day);
		
		// check if day in act month
		if ($weekDay->format('Ymd') >= $weekBeginObj->format('Ymd') 
			&& $weekDay->format('Ymd') <= $weekEndObj->format('Ymd')
		) {
			$debugLogManager->logData('weekDay', $weekDay);
			
			$bgColorFest = $bgColor = ($day % 2 ? '#E4E4E4' : '#FFFFFF');
	
			$campaignQueryPartsDataArray['WHERE']['day'] = array(
				'sql' => 'DAY(`datum`)',
				'value' => $day,
				'comparison' => 'integerEqual'
			);
			
			/**
			 * getCampaignsAndCustomerDataItemsByQueryParts
			 */
			$campaignsDataArray = $campaignManager->getCampaignsAndCustomerDataItemsByQueryParts($campaignQueryPartsDataArray);
			#$debugLogManager->logData('campaignsDataArray', $campaignsDataArray);
			if (\count($campaignsDataArray) > 0) {
				foreach ($campaignsDataArray as $campaignEntity) {
					$campaignType = 'HV';
						
					$timestamp2 = $timestamp;
					$timestamp = $campaignEntity->getDatum()->format('d.m.Y');
					$actWeekDay = \DateUtils::getWeekDay(
						$campaignEntity->getDatum(),
						true
					);
					
					if ($actWeekDay == 'Sa.' || $actWeekDay == 'So.') {
						$bgColorFest = $bgColor = '#C4DDFF';
					}

					$dateTodayHeader = '';
					if ($timestamp != $timestamp2) {
						$dateTodayHeader = $actWeekDay . ', ' . $timestamp;
					}

					$isToday = \DateUtils::isDateToday($campaignEntity->getDatum());
					if ((boolean) $isToday === true) {
						$bgTodayStyle = 'background-color:#E63C3C; color:#FFFFFF';
						$dateTodayHeader = 'Heute';
						
						if (!$dayAnkerSet) {
							$dateTodayHeader .= '<a href="#" id="HEUTE"></a>';
							$dayAnkerSet = true;
						}
					} else {
						$bgTodayStyle = 'background-color:' . $bgColor;
					}
			
					
					$campaignEntity->setAbrechnungsart(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray[$campaignEntity->getAbrechnungsart()]);
					// debug
					$debugLogManager->logData('campaignEntity', $campaignEntity);
					
					require('viewData/viewData_list.php');
					$tableData .= $contentList;
					
					$recipientDataArray['gebucht'] += $campaignEntity->getGebucht();
					
					$csvItemCount++;
				}
			} else {
				require('viewData/viewData_emptyDay.php');
				$tableData .= $tableEmptyDayContent;
			}
		}
		
		// debug
		$debugLogManager->endGroup();
	}	
	
	// debug
	$debugLogManager->endGroup();
}