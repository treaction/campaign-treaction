<?php
$cpxSales = $salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['CPL']] 
	+ $salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['CPO']] 
	+ $salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['CPC']]
;

$grossSale = $salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['TKP']] 
	+ $cpxSales 
	+ $salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['Hybri']]
	+ $salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['Festp']]
;
$netSale = $salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['TKP']] 
	+ $salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['CPL'] . '_u'] 
	+ $salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['CPO'] . '_u'] 
	+ $salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['CPC'] . '_u']
	+ $salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['Hybri'] . '_u']
	+ $salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['Festp']]
;
?>
<table>
    <tr>
        <td width="480" align="left" style="padding:2px;">
            <select name="pJahr" style="font-size:12px;color:#565656;font-weight:bold;" onchange="setRequest('kampagne/planer/index.php?jahr=' + this.value + '&view=<?php echo $view; ?>', 'p');">
				<?php 
				echo \HtmlFormUtils::createOptionListItemDataWithoutKey(
					$_SESSION['campaign']['yearsDataArray'],
					$year
				);
				?>
            </select>
            <select name="pAbrTyp" style="font-size:12px;color:#565656;font-weight:bold;" onchange="setRequest(this.value + '&jahr=<?php echo $year; ?>&monat=<?php echo $month; ?>', 'p');">
				<?php echo $pAbrTypOption; ?>
            </select>
            <select name="pView" style="font-size:12px;color:#565656;font-weight:bold;" onchange="setRequest(this.value + '&jahr=<?php echo $year; ?>&monat=<?php echo $month; ?>', 'p');">
                <option value="kampagne/planer/index.php?<?php echo $viewParameter; ?>&abrTyp=all&<?php echo $deliverySystemParameter; ?>&<?php echo $deliverySystemDistributorParameter; ?>" <?php echo $abr_selected_def; ?>>Alle Abr.arten</option>
                <option value="kampagne/planer/index.php?<?php echo $viewParameter; ?>&abrTyp=cpx&<?php echo $deliverySystemParameter; ?>&<?php echo $deliverySystemDistributorParameter; ?>" <?php echo $abr_selected_cpx; ?>>CPx</option>
                <option value="kampagne/planer/index.php?<?php echo $viewParameter; ?>&abrTyp=cpx_o&<?php echo $deliverySystemParameter; ?>&<?php echo $deliverySystemDistributorParameter; ?>" <?php echo $abr_selected_cpx_o; ?>>CPx (offen)</option>
                <option value="kampagne/planer/index.php?<?php echo $viewParameter; ?>&abrTyp=tkp&<?php echo $deliverySystemParameter; ?>&<?php echo $deliverySystemDistributorParameter; ?>" <?php echo $abr_selected_tkp; ?>>TKP</option>
                <option value="kampagne/planer/index.php?<?php echo $viewParameter; ?>&abrTyp=hyb&<?php echo $deliverySystemParameter; ?>&<?php echo $deliverySystemDistributorParameter; ?>" <?php echo $abr_selected_hyb; ?>>Hybrid</option>
                <option value="kampagne/planer/index.php?<?php echo $viewParameter; ?>&abrTyp=hyb_o&<?php echo $deliverySystemParameter; ?>&<?php echo $deliverySystemDistributorParameter; ?>" <?php echo $abr_selected_hyb_o; ?>>Hybrid (offen)</option>
            </select>
			<br />
            <select name="pDeliverySystem" style="font-size:12px;color:#565656;font-weight:bold;" onchange="setRequest(this.value + '&jahr=<?php echo $year; ?>&monat=<?php echo $month; ?>', 'p');">
                <option value="kampagne/planer/index.php?<?php echo $viewParameter; ?>&<?php echo $abrTypParameter; ?>&deliverySystem=all" <?php echo $deliverySystem_selected; ?>>Alle ASP</option>
                <?php echo $deliverySystemOptionItems; ?>
            </select>
			<select name="pDeliverySystemDistributor" style="font-size:12px;color:#565656;font-weight:bold;" onchange="setRequest(this.value + '&jahr=<?php echo $year; ?>&monat=<?php echo $month; ?>', 'p');">
				<option value="kampagne/planer/index.php?<?php echo $viewParameter; ?>&<?php echo $abrTypParameter; ?><?php echo $deliverySystemParameter; ?>&deliverySystemDistributor=all" <?php echo $deliverySystemDistributor_selected; ?>>- Alle -</option>
				<?php echo $deliverySystemDistributorOptionItems; ?>
			</select>
        </td>
		<td id="umsatz_td">
			<?php if ((boolean) $umsatzView === true) {?>
			<table>
                <tr>
                    <td style="width:30px;border:0px;background-color:#FFFFFF;padding:2px"><img src="img/icon_euro_25.gif" title="Umsatz" /></td>
                    <td style="width:100px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">Umsatz <?php echo \DateUtils::$abkzMonthDataArray[(int) $month]; ?><br /><span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($grossSale); ?> Euro</span></td>
                    <td style="width:110px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">Umsatz (validiert)<br /><span style="color:#7D960B"><?php print \FormatUtils::sumFormat($netSale); ?> Euro</span></td>
                    <td style="width:100px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">TKP<br /><span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['TKP']]); ?> Euro</span></td>
                    <td style="width:100px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">CPL/CPO<br /><span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($cpxSales); ?> Euro</span></td>
                    <td style="width:100px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">Hybrid<br /><span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['Hybri']]); ?> Euro</span></td>
					<td style="width:100px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">Festpreis<br /><span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($salesByBillingType[\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['Festp']]); ?> Euro</span></td>
                </tr>
            </table>
			<?php } ?>
		</td>
    </tr>
</table>
<table class="button_table">
    <tr>
        <td style="padding:1px;margin:0px;height:23px;">
            <div style="white-space:nowrap"><?php echo $monthNavButtons; ?></div>
        </td>
    </tr>
</table>
<?php
$dataPrintVersion = '
    <p>Die Spalten, die nicht ausgedruckt werden sollen, k&ouml;nnen mit einem Klick auf <img src="../img/Tango/16/actions/dialog-close.png" /> entfernt werden.</p>
    <p><button onclick="PrintContent();">Drucken</button> <button onclick="location.reload();">Zur&uuml;cksetzen</button></p>
    <div id="tblData">
        <table class="scrollTable" id="scrollTable" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">' 
			. $tableEditHeaderContent .
			'<tr>' 
				. $tableHeaderContent 
			. '</tr>' 
			. \utf8_decode($tableData) .
		'</table>
    </div>
';
$_SESSION['data'] = $dataPrintVersion;