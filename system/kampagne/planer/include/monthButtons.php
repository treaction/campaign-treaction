<?php
$monthNavButtons = '';
for ($i = 1; $i <= 12; $i++) {
	$campaignMonth = \DateUtils::$abkzMonthDataArray[$i];
	
	$campaignCount = 0;
	if (\array_key_exists($i, $monthCampaignsCountDataArray)) {
		$campaignCount = (int) $monthCampaignsCountDataArray[$i];
	}

    $selectedClass = '';
    if ((int) $i === (int) $month) {
        $selectedClass = ' selected';
    }
	
	$monthNavButtons .= \HtmlFormUtils::createButton(
		array(
			'onclick' => 'setRequest(\'kampagne/planer/index.php?' 
					. 'jahr=' . $year 
					. '&monat=' . $i 
					. '&view=' . $view 
					. '&abrTyp=' . $abrTyp 
					. '&deliverySystem=' . $deliverySystem 
					. '&deliverySystemDistributor=' . $deliverySystemDistributor
				. '\', \'p\');'
			,
			'class' => 'b_planer' . $selectedClass,
			'value' => $campaignMonth . ' (' . $campaignCount . ')',
			'style' => 'width:70px'
		)
	);
}