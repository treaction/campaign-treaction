<?php
/* @var $syncLastTimeCampaignEntity \CampaignEntity */

$seperatorBegin = '
	<div style="float:left;margin-top:1px;">
        <img src="img/treaction_gruen_seperator.png" />
    </div>
	<div style="margin-left:5px;padding-top:5px;float:left;">'
;
?>
<div style="background-color:#8e8901;color:#FFFFFF;font-size:11px;height:23px;width:100%;">
    <div style="margin-left:5px;padding-top:5px;float:left;">
		<?php
		echo '&nbsp;&nbsp;Zuletzt aktualisiert: ' . $syncLastTimeCampaignEntity->getGestartet()->format('H:i') . ' Uhr';
		?>
    </div>
    <div style="float:left;margin-top:1px;"><img src='img/treaction_gruen_seperator.png' /></div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
		<?php
		if ($campaignCount > 0) {
			echo $campaignCount . ' Mailings &nbsp;';
		}
		?>
    </div>
	<?php
	if ($campaignCount > 0) {
		$unsubscribeQuote = \FormatUtils::rateCalculation(
			$recipientDataArray['abmelder'],
			$recipientDataArray['versendet']
		);
		$usqAll = \round(((($recipientDataArray['versendet'] - $recipientDataArray['gebucht']) / $recipientDataArray['gebucht']) * 100), 1);
		
		echo $seperatorBegin . ' Gebucht: ' . \FormatUtils::numberFormat($recipientDataArray['gebucht']) . '</div>' 
			. $seperatorBegin 
			. ' Versendet: ' . \FormatUtils::numberFormat($recipientDataArray['versendet']) . ' (&Uuml;SQ: ' . $usqAll . '%)</div>' 
			. $seperatorBegin . ' Abmelder: ' . \FormatUtils::numberFormat($recipientDataArray['abmelder']) . ' (' . $unsubscribeQuote . '%)</div>'
		;
	}
	?>
</div>
<script type="text/javascript">
	wi = document.documentElement.clientWidth;
	wi = wi - 205;

	hi = document.documentElement.clientHeight;
	if (uBrowser === 'Firefox') {
		hi = hi - 214;
	} else if (uBrowser === 'Chrome') {
		hi = hi - 221;
	} else {
		hi = hi - 206;
	}

	YAHOO.util.Dom.get('fakeContainer').style.width = wi + 'px';
	YAHOO.util.Dom.get('fakeContainer').style.height = hi + 'px';

	window.onresize = Resize;

	var mySt = new superTable('scrollTable', {
		cssSkin: 'sDefault',
		fixedCols: 0,
		headerRows: 1
	});
	
	location.href = '<?php echo $campaignAnchor; ?>';
    location.href = '#topBody';
</script>