<?php
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();
$data = $_SESSION['data'];
$data = str_replace("bitte synchronisieren","",$data);
$data = str_replace('height: "10em",',"",$data);
$data = str_replace('870',"750",$data);
$data = str_replace('#9900FF',"#666666",$data);
$data = str_replace('#9933FF',"#666666",$data);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<style type="text/css">
	body {font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;background-color:#FFFFFF;}
	table {font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;background-color:#FFFFFF;border-collapse:collapse;}
	table tr {background-color:#FFFFFF;}
	table td {border:1px solid #999999;padding:3px;white-space:nowrap;background-color:#FFFFFF;}
	table th {border:1px solid #999999;padding:3px;background-color:#FFFFFF;}
</style>
<link rel="stylesheet" type="text/css" href="../../yui/build/fonts/fonts-min.css" />
<link rel="stylesheet" type="text/css" href="../../yui/build/datatable/assets/skins/sam/datatable.css" />
<script type="text/javascript" src="../javascript/jquery/jquery.js"></script><script type="text/javascript">
$(document).ready(function() {
	$('.delFeld').click(function(){
		var feldID = $(this).attr('id');
		$('.'+feldID).fadeOut('fast');
	});
});

function PrintContent() {
	$('#data_tab_felder_edit').remove();
	var htmlHeader = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /><style type="text/css">body {font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;background-color:#FFFFFF;} table{font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;background-color:#FFFFFF;border-collapse:collapse;}	table tr {background-color:#FFFFFF;} table td {border:1px solid #999999;padding:3px;white-space:nowrap;background-color:#FFFFFF;} table th {border:1px solid #999999;padding:3px;background-color:#FFFFFF;}</style></head><body>';
	var htmlFooter = '</body></html>';
	var DocumentContainer = document.getElementById('tblData');
	var WindowObject = window.open('', 'PrintWindow',
	'width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes');
	WindowObject.document.writeln(htmlHeader+DocumentContainer.innerHTML+htmlFooter);
	WindowObject.document.close();
	this.close();
	WindowObject.focus();
	WindowObject.print();
	WindowObject.close();
}
</script>
</head>
<body class="yui-skin-sam" >
<?php print $data; ?>
</body>
</html>