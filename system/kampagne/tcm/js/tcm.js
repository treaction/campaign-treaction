function resetHandler() {
    var selDates = YAHOO.example.calendar.tcmCal.getSelectedDates();
    var resetDate;
    
    if (selDates.length > 0) {
        resetDate = selDates[0];
    } else {
        resetDate = selDates.today;
    }
    
    Dom.get('in_tcmList').value = '';
    Dom.get('out_tcmList').value = '';
    
    YAHOO.example.calendar.tcmCal.cfg.setProperty('selected', '', false);
    YAHOO.example.calendar.tcmCal.render();
}
function closeHandler() {
    this.hide();
}


var inTxt = YAHOO.util.Dom.get('in_tcmList'),
    outTxt = YAHOO.util.Dom.get('out_tcmList'),
    inDate, outDate, interval
;
inTxt.value = '';
outTxt.value = '';


/**
 * tcmDialog
 */
YAHOO.example.container.tcmDialog = new YAHOO.widget.Dialog('container_tcmDialog', {
    visible : false, 
    close: false,
    constraintoviewport : true,
    buttons: [
        {
            text: 'zur&uuml;cksetzen', 
            handler: resetHandler, 
            isDefault:true
        },
        {
            text: '&uuml;bernehmen', 
            handler: closeHandler
        }
    ]
});
YAHOO.example.container.tcmDialog.render();
YAHOO.util.Event.addListener(
    'showTcmCal',
    'click',
    YAHOO.example.container.tcmDialog.show,
    YAHOO.example.container.tcmDialog,
    true
);


/**
 * IntervalCalendar
 */
YAHOO.example.calendar.tcmCal = new YAHOO.example.calendar.IntervalCalendar(
    'tcm_CalCalendar',
    {
        START_WEEKDAY: 1,
        pages: 2
    }
);
YAHOO.example.calendar.tcmCal.cfg.setProperty(
    'WEEKDAYS_SHORT',
    ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
);
YAHOO.example.calendar.tcmCal.cfg.setProperty(
    'MONTHS_LONG',
    ['Januar', 'Februar', 'M\u00E4rz', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']
);
YAHOO.example.calendar.tcmCal.render();

YAHOO.example.calendar.tcmCal.selectEvent.subscribe(function() {
    interval = this.getInterval();
    
    if (interval.length == 2) {
        inDate = interval[0];
        inTxt.value = inDate.getFullYear() + '-' + (inDate.getMonth() + 1) + '-' + inDate.getDate();
        
        if (interval[0].getTime() != interval[1].getTime()) {
            outDate = interval[1];
            outTxt.value = outDate.getFullYear() + '-' +(outDate.getMonth() + 1) + '-' + outDate.getDate();
        } else {
            outTxt.value = '';
        }
    }
}, YAHOO.example.calendar.tcmCal, true);