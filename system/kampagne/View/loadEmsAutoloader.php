<?php
/**
 * @deprecated use: loadAndInitEmsAutoloader
 */
/**
 * getEmsRootPath
 * 
 * @param string $path
 * @return string
 */
function getEmsRootPath($path) {
	$resultDataArray = \array_filter(\explode(\DIRECTORY_SEPARATOR, $path));
	
	return $resultDataArray[1];
}

/**
 * getScriptFileForRequestData
 * 
 * @param string $emsWorkPath
 * @return string
 */
function getScriptFileForRequestData($emsWorkPath) {
	return \str_replace(\DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR, '', $_SERVER['PHP_SELF']);
}



$emsWorkPath = \getEmsRootPath(\dirname($_SERVER['PHP_SELF']));

#require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'emsConfig.php');
#require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'EmsAutoloader.php');
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'Configs' . \DIRECTORY_SEPARATOR . 'emsConfig.php');
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'EmsAutoloader.php');