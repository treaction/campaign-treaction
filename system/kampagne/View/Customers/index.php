<?php
require_once('../loadEmsAutoloader.php');

\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();
\EmsAutoloader::initErrorReporting();

\header('Content-Type: text/html; charset=utf-8');
?>
<div id="basic_layout" style="overflow:hidden;border:0px;">
    <div id="basic" style="overflow:hidden;background-color:#F2F2F2"></div>
    <div id="basic_content" style="background-color:#FFFFFF;height:100%">
		<div style="padding:25px">
			<span style="font-size:16px;">Kundenverwaltung</span><br /><br />
			Bitte w&auml;hlen Sie einen Kunden aus, um diesen zu bearbeiten.
		</div>
    </div>
</div>
<script type="text/javascript">
	var myContextMenu = false;

	var layoutb = new YAHOO.widget.Layout('basic_layout', {
		height: Dom.getClientHeight() - 119, //Height of the viewport
		width: Dom.get('basic_layout').offsetWidth, //Width of the outer element
		minHeight: 150, //So it doesn't get too small
		units: [
			{position: 'left', width: 302, body: 'basic', scroll: false, gutter: '-1 0 -1 -1'},
			{position: 'center', width: (Dom.get('basic_layout').offsetWidth - 368), body: 'basic_content', gutter: '0 -1 0 0'}
		]
	});
	
	layoutb.render();
	
	YAHOO.example.Basic = function() {
		var myColumnDefs = [
			{key: 'kunde_id', resizeable: true},
			{key: 'firma', label: 'Firma', resizeable: true, sortable: false}
		];
		
		var myDataSource = new YAHOO.util.DataSource('kampagne/View/Customers/jsonData.php?');
		myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSON;
		myDataSource.connXhrMode = 'queueRequests';
		myDataSource.responseSchema = {
			resultsList: 'Result',
			fields: ['kunde_id', 'firma']
		};

		var myDataTable = new YAHOO.widget.ScrollingDataTable(
			'basic',
			myColumnDefs,
			myDataSource,
			{
				height: (Dom.getClientHeight() - 144) + 'px',
				width: '301px',
				selectionMode: 'single'
			}
		);

		myDataTable.hideColumn('kunde_id');
		// Subscribe to events for row selection
		myDataTable.subscribe('rowMouseoverEvent', myDataTable.onEventHighlightRow);
		myDataTable.subscribe('rowMouseoutEvent', myDataTable.onEventUnhighlightRow);
		myDataTable.subscribe('rowClickEvent', myDataTable.onEventSelectRow);
		myDataTable.subscribe('rowSelectEvent', function() {
			var myDataTableData = this.getRecordSet().getRecord(this.getSelectedRows()[0])._oData;
			
			setRequest_default(
				'kampagne/View/Customers/details.php?customer[id]=' + parseInt(myDataTableData.kunde_id),
				'basic_content'
			);
		});

		layoutb.on('resize', function() {
			var height2 = Dom.getClientHeight() - 119;
			var width2 = Dom.getClientWidth() - 207;

			YAHOO.util.Dom.setStyle('basic_layout', 'width', width2 + 'px');
			YAHOO.util.Dom.setStyle('basic_layout', 'height', height2 + 'px');
			YAHOO.util.Dom.setStyle('basic_content', 'width', (width2 - 327) + 'px');
			YAHOO.util.Dom.setStyle('basic_content', 'height', height2 + 'px');

			myDataTable.set('height', ((height2 - 25) + 'px'));
		});
		
		return {
			oDS: myDataSource,
			oDT: myDataTable
		};

	}();

	Event.on(window, 'resize', layoutb.resize, layoutb, true);

	function Resize_() {
	}
	window.onresize = Resize_;
</script>