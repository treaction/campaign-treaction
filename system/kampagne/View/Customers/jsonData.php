<?php
require_once('../loadEmsAutoloader.php');

\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();
\EmsAutoloader::initErrorReporting();

\header('Content-Type: text/html; charset=utf-8');



$mandant = $_SESSION['mandant'];
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'db_connect.inc.php');



/**
 * init $debugLogManager
 * init $campaignManager
 */
require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager debugLogManager */

require(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager CampaignManager */



// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);
#$debugLogManager->logData('SESSION', $_SESSION);


try {
	/**
	 * getCustomersDataItemsByQueryParts
	 * 
	 * debug
	 */
	$customerDataArray = $campaignManager->getCustomersDataItemsByQueryParts(
		array(
			'SELECT' => '`kunde_id`, `firma`, `firma_short`, `status`',
			'ORDER_BY' => '`firma` ASC'
		),
		\PDO::FETCH_CLASS
	);
	$debugLogManager->logData('customerDataArray', $customerDataArray);
	
	if (\count($customerDataArray) === 0) {
		throw new \InvalidArgumentException('invalid customerDataArray');
	}
	
	echo '{"Result": ' . \json_encode($customerDataArray) . '}';
} catch (\Exception $e) {
	require(DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}