<?php
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */
/* @var $debugLogManager \debugLogManager */
/* @var $customerEntity \CustomerEntity */


// createTableRow - Tag
$tableHeaderRowDataArray = \HtmlTableUtils::createTableRow(
	array(
		'class' => 'performance_tr'
	)
);
$tableContentRowDataArray = \HtmlTableUtils::createTableRow(
	array(
		'class' => 'performance_tr2',
	)
);
$headerStyleDataArray = array(
);

$contactPersonsContent = 
	$tableHeaderRowDataArray['begin'] 
		. \HtmlTableUtils::createTableCellWidthContent(
			$headerStyleDataArray,
			'Email'
		) 
		. \HtmlTableUtils::createTableCellWidthContent(
			$headerStyleDataArray,
			'Anrede'
		) 
		. \HtmlTableUtils::createTableCellWidthContent(
			$headerStyleDataArray,
			'Vorname'
		) 
		. \HtmlTableUtils::createTableCellWidthContent(
			array(),
			'Nachname'
		) 
		. \HtmlTableUtils::createTableCellWidthContent(
			array(),
			'Vertriebler'
		)
		. \HtmlTableUtils::createTableCellWidthContent(
			array(),
			'Telefon'
		)
		. \HtmlTableUtils::createTableCellWidthContent(
			array(),
			''
		) 
	. $tableHeaderRowDataArray['end'] 
;

foreach ($contactPersonDataArray as $contactPersonEntity) {
	/* @var $contactPersonEntity ContactPersonEntity */
	$contactPersonsContent .= 
		$tableContentRowDataArray['begin'] 
			// Email
			. \HtmlTableUtils::createTableCellWidthContent(
				array(),
				'<a href="mailto:' . $contactPersonEntity->getEmail() . '" title="Email verfassen">' . $contactPersonEntity->getEmail() . '</a>'
			) 
			// Anrede
			. \HtmlTableUtils::createTableCellWidthContent(
				array(),
				$contactPersonEntity->getAnrede()
			) 
			// Vorname
			. \HtmlTableUtils::createTableCellWidthContent(
				array(),
				$contactPersonEntity->getTitel() . ' ' . $contactPersonEntity->getVorname()
			) 
			// Nachname
			. \HtmlTableUtils::createTableCellWidthContent(
				array(),
				$contactPersonEntity->getNachname()
			) 
			// Vertriebler
			. \HtmlTableUtils::createTableCellWidthContent(
				array(),
				\getSalesPersonDataById(
					$contactPersonEntity->getVertriebler_id(),
					$clientManager,
					$debugLogManager
				)
			) 
			// Telefon
			. \HtmlTableUtils::createTableCellWidthContent(
				array(),
				$contactPersonEntity->getTelefon()
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'align' => 'right'
				),
				'<input type="image" onclick="editOrCreateContactPersonData(' . $customerEntity->getKunde_id() . ', ' . $contactPersonEntity->getAp_id() . ');" src="img/Tango/16/apps/utilities-text-editor.png" title="bearbeiten" /> &nbsp;&nbsp;' 
					. '<input type="image" onclick="deleteContactPersonEntity(' . $contactPersonEntity->getAp_id() . ');" src="img/Tango/16/actions/dialog-close.png" title="l&ouml;schen" />'
					. '<input type="image" onclick="showContactPersonCampaigns(' . $customerEntity->getKunde_id() . ', ' . $contactPersonEntity->getAp_id() . ');" src="img/Tango/16/actions/edit-find.png" title="' 
						. \getContactPersonCampaignCount(
							$contactPersonEntity,
							$campaignManager,
							$debugLogManager
						)  . ' Kampagnen anzeigen" />'
			) 
		. $tableContentRowDataArray['end']
	;
}