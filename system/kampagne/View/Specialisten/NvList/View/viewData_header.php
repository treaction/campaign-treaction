<?php
/**
 * createTableHeaderRow - Tag
 */
$tableHeadDataArray = \HtmlTableUtils::createTableRow(
	array(
		'class' => 'headerStyleColumn trKzdgk'
	)
);
$tableSubheadDataArray = \HtmlTableUtils::createTableRow(
	array(
		'class' => 'headerStyleColumn headerColumn'
	)
);

$tableHeaderContent = 
	$tableHeadDataArray['begin']
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'colspan' => $_SESSION['tcmData']['beforeColspanGroup'],
				'class' => 'beforeKzdgk'
			), ''
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'colspan' => $_SESSION['tcmData']['colspanGroup'],
				'style' => 'text-align: center;',
				'class' => 'kzdgk'
			), 'Kennzahlen der gesamten Kampagne'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'colspan' => $_SESSION['tcmData']['afterColspanGroup'],
				'class' => 'afterKzdgk'
			), ''
		) 
	. $tableHeadDataArray['end']
	. $tableSubheadDataArray['begin']
;

foreach ($tableColumnsDataArray as $key => $itemDataArray) {
	$tableHeaderContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
		array(
			'title' => (isset($itemDataArray['title']) ? ' ' . $itemDataArray['title'] : ''),
			'class' => $key,
			'nowrap' => (isset($itemDataArray['nowrap']) ? $itemDataArray['nowrap'] : ''),
		),
		$itemDataArray['rowTitle']
	);
}
$tableHeaderContent .= $tableSubheadDataArray['end'];

unset($tableHeadDataArray, $tableSubheadDataArray);