<?php
require_once('../../loadEmsAutoloader.php');

\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();
\EmsAutoloader::initErrorReporting();

\header('Content-Type: text/html; charset=utf-8');



/**
 * getClientTableContent
 * 
 * @param array $tableTHeaderDataArray
 * @param array $tableRowDataArray
 * @param string $clientAbkz
 * @param string $clientPageBreakStyle
 * @param string $parentDeliveryDistributorTitle
 * @return string
 */
function getClientTableContent(array $tableTHeaderDataArray, array $tableRowDataArray, $clientAbkz, $clientPageBreakStyle, $parentDeliveryDistributorTitle = '') {
	/**
	 * createTable - Tag
	 */
	$tableDataArray = \HtmlTableUtils::createTable(
		array(
			'class' => 'tcm_table newTableView ' . (\strlen($parentDeliveryDistributorTitle) ? $parentDeliveryDistributorTitle : $clientAbkz),
			'style' => $clientPageBreakStyle
		)
	);

	$result = $tableDataArray['begin']
		. $tableTHeaderDataArray['begin']
			. $tableRowDataArray ['begin']
				. \HtmlTableUtils::createTableHeaderCellWidthContent(
					array(
						'class' => 'noBorder transparent',
						'colspan' => $_SESSION['tcmData']['totalColspan'],
						'style' => 'text-align: left;'
					),
					'<img src="https://campaign-treaction.de.de/system/img/logo/' . $clientAbkz . '.gif" alt="" style="width: 100px; height: auto; margin-right: 20px;" />'
						. '<img class="delFeld hidden" id="' . (\strlen($parentDeliveryDistributorTitle) ? $parentDeliveryDistributorTitle : $clientAbkz) . '" src="https://campaign-treaction.de/system/img/Tango/16/actions/dialog-close.png" title="Mandant entfernen" alt="" />'
				)
			. $tableRowDataArray['end']
	;

	return $result;
}

/**
 * createEditableTableHeadForMandatory
 * 
 * @param array $tableColumnsDataArray
 * @return string
 */
function createEditableTableHeadForMandatory(array $tableColumnsDataArray) {
	/**
	 * createTable - Tag
	 */
	$tableDataArray = \HtmlTableUtils::createTable(
		array(
			'class' => 'tcm_table newTableView headerEditColumns'
		)
	);

	$result = $tableDataArray['begin'];
	foreach ($tableColumnsDataArray as $key => $itemDataArray) {
		$result .= \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'noBorder ' . $key,
				'align' => 'center'
			),
			'<img class="delFeld" id="' . $key . '" src="https://campaign-treaction.de/system/img/Tango/16/actions/dialog-close.png" title="Spalte: ' . $itemDataArray['rowTitle'] . ' entfernen" alt="" />'
		);
	}
	$result .= $tableDataArray['end'];
	unset($tableDataArray);

	return $result;
}

/**
 * initFooterDataArray
 * 
 * @return array
 */
function initFooterDataArray() {
	return array(
		'targetVolume' => 0,
		'bookingAmount' => 0,
		'shippedAmount' => 0,
		'openingRate' => 0,
		'clickRate' => 0
	);
}

/**
 * getNoContentData
 * 
 * @param array $tableTHeaderDataArray
 * @param array $tableRowDataArray
 * @return string
 */
function getNoContentData(array $tableTHeaderDataArray, array $tableRowDataArray) {
	// viewData_noData
	require('View/viewData_noData.php');
	
	return $tableTHeaderDataArray['begin']
		. $tableContent
	;
}



/**
 * init $debugLogManager
 * init $clientManager
 */
require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager debugLogManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager ClientManager */


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);
#$debugLogManager->logData('SESSION', $_SESSION);


if (isset($_GET['dateBegin']) 
	&& \strlen($_GET['dateBegin']) > 0
) {
	$dateBeginObj = new \DateTime($_GET['dateBegin']);

	if (isset($_GET['dateEnd']) 
		&& \strlen($_GET['dateEnd']) > 0
	) {
		$dateEndObj = new \DateTime($_GET['dateEnd']);
	} else {
		$dateEndObj = $dateBeginObj;
	}
} else {
	$dateBeginObj = new \DateTime('now');

	$dateEndObj = new \DateTime('now');
	$dateEndObj->add(new \DateInterval('P3D'));
}
// debug
$debugLogManager->logData('dateBegin', $dateBeginObj);
$debugLogManager->logData('dateEnd', $dateEndObj);


/**
 * getClientsAccessDataArrayByLoggedUser
 * 
 * debug
 */
$userClientsDataArray = \CampaignAndOthersUtils::getClientsAccessDataArrayByLoggedUser();
$debugLogManager->logData('userClientsDataArray', $userClientsDataArray);


$tableColumnsDataArray = array(
	// bevorColspanGroup - begin
	'date' => array(
		'rowTitle' => 'Datum'
	),
	'time' => array(
		'rowTitle' => 'Uhrzeit'
	),
	'campaign' => array(
		'rowTitle' => 'Kampagne'
	),
	'customerCompany' => array(
		'rowTitle' => 'Kunde / Agentur'
	),
	'targetGroup' => array(
		'rowTitle' => \CampaignAndOthersUtils::$allTabFieldsDataArray['zielgruppe']['sublabel']
	),
	// bevorColspanGroup - end
	
	// colspanGroup - begin
	'targetVolume' => array(
		'rowTitle' => 'Zielmenge'
	),
	'bookingAmount' => array(
		'rowTitle' => 'G. Menge',
		'title' => 'Gebuchte Menge'
	),
	'shippedAmount' => array(
		'rowTitle' => 'V. Menge',
		'title' => 'Versandmenge bisher'
	),
	'openingRate' => array(
		'rowTitle' => '&Ouml;. Rate',
		'title' => '&Ouml;ffnungsrate',
		'nowrap' => 'nowrap'
	),
	'clickRate' => array(
		'rowTitle' => 'K. Rate',
		'title' => 'Klickrate',
		'nowrap' => 'nowrap'
	),
	// colspanGroup - end
	
	'notes' => array(
		'rowTitle' => \CampaignAndOthersUtils::$allTabFieldsDataArray['notiz']['sublabel']
	),
	'campaignEditor' => array(
		'rowTitle' => \CampaignAndOthersUtils::$allTabFieldsDataArray['bearbeiter']['sublabel']
	),
	'useCampaignStartDate' => array(
		'rowTitle' => \CampaignAndOthersUtils::$allTabFieldsDataArray['use_campaign_start_date']['sublabel']
	),
	'blacklist' => array(
		'rowTitle' => 'BL'
	),
	'status' => array(
		'rowTitle' => 'Status'
	),
	'deliverySystem' => array(
		'rowTitle' => \CampaignAndOthersUtils::$allTabFieldsDataArray['asp']['sublabel']
	),
	'deliverySystemDistributor' => array(
		'rowTitle' => \CampaignAndOthersUtils::$allTabFieldsDataArray['dsd_id']['sublabel']
	),
);
$debugLogManager->logData('tableColumnsDataArray', $tableColumnsDataArray);

$_SESSION['tcmData']['totalColspan'] = \count($tableColumnsDataArray);
$_SESSION['tcmData']['beforeColspanGroup'] = 5;
$_SESSION['tcmData']['colspanGroup'] = 5;
$_SESSION['tcmData']['afterColspanGroup'] = ($_SESSION['tcmData']['totalColspan'] - $_SESSION['tcmData']['beforeColspanGroup'] - $_SESSION['tcmData']['colspanGroup']);


/**
 * createTable - Tag
 */
$tableDataArray = \HtmlTableUtils::createTable(array());

/**
 * createTHeadTag - Tag
 */
$tableTHeaderDataArray = \HtmlTableUtils::createTHeadTag();

/**
 * createTableRow - Tag
 */
$tableRowDataArray = \HtmlTableUtils::createTableRow(array());


$campaign_queryPartsDataArray = array(
	'SELECT' => '*',
	'WHERE' => array(
		'dateFrom' => array(
			'sql' => 'DATE_FORMAT(`datum`, \'%Y-%m-%d\')',
			'value' => $dateBeginObj->format('Y-m-d'),
			'comparison' => '>='
		),
		'dateTo' => array(
			'sql' => 'DATE_FORMAT(`datum`, \'%Y-%m-%d\')',
			'value' => $dateEndObj->format('Y-m-d'),
			'comparison' => '<='
		),
		'special' => array(
			'sql' => '((`k_id` != `nv_id` AND `vorgabe_m` > 0) OR LEFT(`k_name`, 3) = "NV:")',
			'value' => '',
			'comparison' => ''
		),
	),
	'ORDER_BY' => '`datum` ASC'
);
$debugLogManager->logData('campaign_queryPartsDataArray', $campaign_queryPartsDataArray);


/**
 * list Settings - begin
 */
try {
	$i = 1;

	$tableData = $clientPageBreakStyle = '';
	
	// debug
	$debugLogManager->beginGroup('nvList');
	foreach ($userClientsDataArray as $clientId => $clientName) {
		$debugLogManager->beginGroup($clientName);

		/**
		 * getClientDataById
		 * 
		 * debug
		 */
		$clientEntity = $clientManager->getClientDataById($clientId);
		if (!($clientEntity instanceof \ClientEntity)) {
			throw new \DomainException('invalid ClientEntity');
		}
		$debugLogManager->logData('clientEntity', $clientEntity);
		
		
		$mandant = $clientEntity->getAbkz();
		require($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'db_connect.inc.php');


		/**
		 * init $campaignManager
		 */
		require(DIR_configsInit . 'initCampaignManager.php');
		/* @var $campaignManager CampaignManager */


		/**
		 * getDeliverySystemDistributorsDataItemsByClientId
		 * 
		 * debug
		 */
		$clientDeliverySystemDistributorEntityDataArray = $clientManager->getDeliverySystemDistributorsDataItemsByClientId(
			$clientId,
			$clientManager->getUnderClientIdsByClientId($clientId)
		);
		$debugLogManager->logData('clientDeliverySystemDistributorEntityDataArray', $clientDeliverySystemDistributorEntityDataArray);
		
		if ($clientEntity->getHas_multiple_distributors() === true) {
			$dsdCampaign_queryPartsDataArray = $campaign_queryPartsDataArray;
			$dsdCampaign_queryPartsDataArray['ORDER_BY'] .= ', `dsd_id` ASC';
			
			$deliverySystemDistributorDataArray = array();
			foreach ($clientDeliverySystemDistributorEntityDataArray as $deliverySystemDistributorEntity) {
				/* @var $deliverySystemDistributorEntity \DeliverySystemDistributorEntity */
				$deliverySystemDistributorDataArray[$deliverySystemDistributorEntity->getParent_id()][] = $deliverySystemDistributorEntity->getId();
			}
			$debugLogManager->logData('deliverySystemDistributorDataArray', $deliverySystemDistributorDataArray);
			
			foreach ($deliverySystemDistributorDataArray as $deliverySystemDistributorParentId => $deliverySystemDistributorValues) {
				/**
				 * CampaignAndOthersUtils::getParentClientDeliverySystemDistributorTitle
				 */
				$parentClientDeliverySystemDistributorTitle = \CampaignAndOthersUtils::getParentClientDeliverySystemDistributorTitle(
					$clientDeliverySystemDistributorEntityDataArray[$deliverySystemDistributorParentId]->getTitle()
				);
				
				if ($i > 1) {
					$clientPageBreakStyle = 'page-break-before: always;';
				}
				
				/**
				 * initFooterDataArray
				 */
				$footerDataArray = \initFooterDataArray();
				
				
				// debug
				$debugLogManager->beginGroup($parentClientDeliverySystemDistributorTitle);
				
				/**
				 * getClientTableContent
				 */
				$tableData .= \getClientTableContent(
					$tableTHeaderDataArray,
					$tableRowDataArray,
					$mandant,
					$clientPageBreakStyle,
					$parentClientDeliverySystemDistributorTitle
				);
				
				/**
				 * getCampaignsAndCustomerDataItemsByQueryPartsAndDsdIds
				 * 
				 * debug
				 */
				$campaignsDataArray = $campaignManager->getCampaignsAndCustomerDataItemsByQueryPartsAndDsdIds(
					$dsdCampaign_queryPartsDataArray,
					$deliverySystemDistributorValues,
					\CampaignManager::$fieldIn
				);
				#$debugLogManager->logData('campaignsDataArray', $campaignsDataArray);
				$debugLogManager->logData('campaignsCount', \count($campaignsDataArray));
				if (\count($campaignsDataArray) > 0) {
					// viewData_headerMultipleDistributors
					require('View/viewData_headerMultipleDistributors.php');
					
					// viewData_list
					require('View/viewData_list.php');
					
					// viewData_footer
					require('View/viewData_footer.php');
					
					$tableData .= $tableHeaderContent
						. $tableFooterContent
						. $tableContent
					;
					unset($tableHeaderContent, $tableContent, $tableFooterContent);
				} else {
					// viewData_noData
					/**
					 * getNoContentData
					 */
					$tableData .= \getNoContentData(
						$tableTHeaderDataArray,
						$tableRowDataArray
					);
					unset($tableContent);
				}
				
				$tableData .= $tableDataArray['end'];
				
				// debug
				$debugLogManager->endGroup();
				
				$i++;
			}
			unset($dsdCampaign_queryPartsDataArray);
		} else {
			if ($i > 1) {
				$clientPageBreakStyle = 'page-break-before: always;';
			}
			
			/**
			 * getClientTableContent
			 */
			$tableData .= \getClientTableContent(
				$tableTHeaderDataArray,
				$tableRowDataArray,
				$mandant,
				$clientPageBreakStyle
			);
			
			/**
			 * getCampaignsCountByQueryParts
			 * 
			 * debug
			 */
			$campaignsDataArray = $campaignManager->getCampaignsAndCustomerDataItemsByQueryParts($campaign_queryPartsDataArray);
			#$debugLogManager->logData('campaignsDataArray', $campaignsDataArray);
			$debugLogManager->logData('campaignsCount', \count($campaignsDataArray));
			if (\count($campaignsDataArray) > 0) {
				/**
				 * initFooterDataArray
				 */
				$footerDataArray = \initFooterDataArray();
				
				
				// viewData_header
				require('View/viewData_header.php');
				
				// viewData_list
				require('View/viewData_list.php');
				
				// viewData_footer
				require('View/viewData_footer.php');
				
				$tableData .= $tableHeaderContent
					. $tableFooterContent
					. $tableContent
				;
				unset($tableHeaderContent, $tableContent, $tableFooterContent);
			} else {
				// viewData_noData
				/**
				 * getNoContentData
				 */
				$tableData .= \getNoContentData(
					$tableTHeaderDataArray,
					$tableRowDataArray
				);
				unset($tableContent);
			}
			$tableData .= $tableDataArray['end'];
			
			$i++;
		}

		$debugLogManager->endGroup();
	}
	// debug
	$debugLogManager->endGroup();
} catch (\Exception $e) {
	require(DIR_configs . 'exceptions.php');

	$tableData = $exceptionMessage;
}

// getCalView
$calView = \file_get_contents('../calView.html');

$tbl = $calView .
	'<h1 class="tcmViewHeader">NV-Tagesliste Vertrieb</h1>'
	. \createEditableTableHeadForMandatory($tableColumnsDataArray)
	. $tableData
;


$_SESSION['data'] = '
    <div id="printInfoContent">
        <p>
            Die Spalten, die nicht ausgedruckt werden sollen, k&ouml;nnen mit einem Klick auf <img src="https://www.campaign-treaction.de/system/img/Tango/16/actions/dialog-close.png" alt="" /> entfernt werden.<br /><br />
            <button onclick="printContent();">Drucken</button> <button onclick="location.reload();">Zur&uuml;cksetzen</button>
        </p>
    </div>
    <div id="printContentData">' . \utf8_decode($tbl) . '</div>
';

require_once('../menu.php');

$_SESSION['tcmData']['useExtendedPrint'] = true;
?>
<script type="text/javascript">
	function changeView() {
		setRequest('kampagne/View/Specialisten/NvList/index.php' 
				+ '?dateBegin=' + YAHOO.util.Dom.get('in_tcmList').value 
				+ '&dateEnd=' + YAHOO.util.Dom.get('out_tcmList').value 
				+ '&p=6'
			,
			'k_tcm'
		);
	}
</script>