<?php
require_once('../../loadEmsAutoloader.php');

\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();
\EmsAutoloader::initErrorReporting();

\header('Content-Type: text/html; charset=utf-8');



/**
 * getClientTableContent
 * 
 * @param array $tableDataArray
 * @param string $clientAbkz
 * @param string $clientPageBreakStyle
 * @return string
 */
function getClientTableContent(array $tableDataArray, $clientAbkz, $clientPageBreakStyle = '') {
	return '<h2 style="font-weight: bold; font-size: 14px;' . $clientPageBreakStyle . '">' . $clientAbkz . '</h2>'
			. $tableDataArray['begin']
	;
}



/**
 * init $debugLogManager
 * init $clientManager
 */
require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager debugLogManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager ClientManager */


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);
#$debugLogManager->logData('SESSION', $_SESSION);


$year = isset($_GET['year']) ? \intval($_GET['year']) : \date('Y');
$debugLogManager->logData('year', $year);

$month = isset($_GET['month']) ? \intval($_GET['month']) : \intval(\date('m'));
$debugLogManager->logData('month', $month);

$countryId = isset($_GET['selection']) ? intval($_GET['selection']) : 0;
$debugLogManager->logData('countryId', $countryId);

$clickProfile = isset($_GET['clickProfile']) ? $_GET['clickProfile'] : '';
$debugLogManager->logData('clickProfile', $clickProfile);

$clientId = isset($_GET['client']) ? \intval($_GET['client']) : \intval($_SESSION['mID']);
$debugLogManager->logData('clientId', $clientId);

$campaignView = isset($_GET['campaignView']) ? (boolean) \intval($_GET['campaignView']) : false;
$debugLogManager->logData('campaignView', $campaignView);

$asp = isset($_GET['asp']) 
	? \DataFilterUtils::filterData(
		$_GET['asp'],
		\DataFilterUtils::$validateFilder['full_special_chars']
	) 
	: ''
;
$debugLogManager->logData('asp', $asp);

foreach ($_SESSION['deliverySystemsDataArray'] as $deliverySystemEntiy) {
	/* @var $deliverySystemEntiy \DeliverySystemEntity */
    $aspDataArray[$deliverySystemEntiy->getAsp_abkz()] =  $deliverySystemEntiy->getAsp();
}

$deliverySystemDistributor = isset($_GET['deliverySystemDistributor']) 
	? \DataFilterUtils::filterData(
		$_GET['deliverySystemDistributor'],
		\DataFilterUtils::$validateFilder['number_int']
	) 
	: ''
;
if(!empty($asp)){
    $deliverySystemDistributor_selected = 'selected="selected"';
    $deliverySystemDistributorOptionItems = '';
    $deliverySystemDistributorDataArray = $clientManager->getDeliverySystemDistributorsDataItemsByClientIdAndDeliverySystemAsp(
			$_SESSION['mID'],
			$asp,
			$_SESSION['underClientIds']
		);
		$debugLogManager->logData('deliverySystemDistributorDataArray', $deliverySystemDistributorDataArray);
                if (\count($deliverySystemDistributorDataArray) > 0) {
                  foreach($deliverySystemDistributorDataArray as $key => $deliverySystemDistributorEntity) {
				/* @var $deliverySystemDistributorEntity \DeliverySystemDistributorEntity */
				$selected = '';
				if ($deliverySystemDistributorEntity->getId() === (int)$deliverySystemDistributor) {
                                    
					$selected = 'selected';
					$deliverySystemDistributor_selected = '';

					$dsd_id =  $deliverySystemDistributorEntity->getId();
				}
				
				if ($deliverySystemDistributorEntity->getId() === $deliverySystemDistributorEntity->getParent_id()) {
					$optgroupDataArray = \HtmlFormUtils::createOptgroupDataArray(
						\CampaignAndOthersUtils::getParentClientDeliverySystemDistributorTitle($deliverySystemDistributorEntity->getTitle())
					);
					
					// openSelectOptgroup
					$deliverySystemDistributorOptionItems .= \HtmlFormUtils::openSelectOptgroup(
						$key,
						$optgroupDataArray
					);
				}

				$deliverySystemDistributorOptionItems .= \HtmlFormUtils::createOptionItem(
					array(
						'value' =>  $deliverySystemDistributorEntity->getId()
						,
						'selected' => $selected
					),
					$deliverySystemDistributorEntity->getTitle()
				);
				
				if (\count($deliverySystemDistributorDataArray) === $key) {
					// closeSelectOptgroup
					$deliverySystemDistributorOptionItems .= \HtmlFormUtils::closeSelectOptgroup($optgroupDataArray);
					unset($optgroupDataArray);
				}
			}
                }
}

$debugLogManager->logData('deliverySystemDistributor', $deliverySystemDistributor);

//clickProfiles
$clientClickProfilesDataArray = $clientManager->getActiveClientClickProfilesArray($_SESSION['mID']);
$debugLogManager->logData('clientClickProfilesDataArray', $clientClickProfilesDataArray);

$targetGroup = '';
if ($countryId > 0) {
	$targetGroup = $_SESSION['countrySelectionsDataArray'][$countryId]->getShort_title();
}



/**
 * monthDataArray
 * 
 * debug
 */
$monthDataArray = \DateUtils::$monthDataArray;
$debugLogManager->logData('monthDataArray', $monthDataArray);


/**
 * getClientsAccessDataArrayByLoggedUser
 * 
 * debug
 */
$userClientsDataArray = \CampaignAndOthersUtils::getClientsAccessDataArrayByLoggedUser();
$debugLogManager->logData('userClientsDataArray', $userClientsDataArray);

if (\intval($year) > 0) {
	$yearName = $year;
} else {
	$yearName = 'Alle Jahren';
}

if (\intval($month) > 0 
	&& \intval($year) > 0
) {
	$monthName = $monthDataArray[$month] . ' nach KW';
} else {
	$monthName = 'Alle Monate';
}

if (\intval($clientId) > 0) {
	$clientName = $userClientsDataArray[$clientId];
} else {
	$clientName = 'Alle Mandanten';
}

$campaignView_1 = '';
$campaignView_0 = 'checked';
if ((boolean) $campaignView === true) {
	$campaignView_1 = 'checked';
	$campaignView_0 = '';
}


$_SESSION['tcmData']['totalColspan'] = 7;

$campaign_queryPartsDataArray = array(
	'SELECT' => '*',
	'WHERE' => array(
		'month' => array(
			'sql' => 'MONTH(`datum`)',
			'value' => ($month) ? $month : '',
			'comparison' => '='
		),
		'year' => array(
			'sql' => 'YEAR(`datum`)',
			'value' => ($year) ? $year : '',
			'comparison' => '='
		),
		'targetGroup' => array(
			'sql' => '`zielgruppe`',
			'value' => $targetGroup,
			'comparison' => 'LIKE BINARY'
		),
		'asp' => array(
			'sql' => '`versandsystem`',
			'value' => ($asp) ? $asp : '',
			'comparison' => '='
		),
              'deliverySystemDistributor' => array(
			'sql' => '`dsd_id`',
			'value' => $deliverySystemDistributor,
			'comparison' => '='
		), 
                'click_profile' => array(
			'sql' => '`campaign_click_profiles_id`',
			'value' => ($clickProfile) ? $clickProfile : '',
			'comparison' => '='
		),
                
	),
);

if ((boolean) $campaignView === true) {
	$campaign_queryPartsDataArray['WHERE']['nv_id'] = array(
		'sql' => '`k_id`',
		'value' => '`nv_id`',
		'comparison' => 'fieldEqual'
	);

	$_SESSION['tcmData']['totalColspan'] = 9;
}
$debugLogManager->logData('campaign_queryPartsDataArray', $campaign_queryPartsDataArray);

/**
 * createTable - Tag
 */
$tableDataArray = \HtmlTableUtils::createTable(
	array(
		'class' => 'tcm_table newTableView',
	)
);

/**
 * createTableRow - Tag
 */
$tableRowDataArray = \HtmlTableUtils::createTableRow();

$cellSettingsDataArray = array(
	'style' => \CampaignFieldsUtils::alignRight
);

$cellHeaderSettingsDataArray = array(
	'width' => 90,
	'style' => \CampaignFieldsUtils::alignRight
);


try {
	$tableData = '';
	
	// debug
	$debugLogManager->beginGroup('tcmList');
	
	/**
	 * list Settings - begin
	 */
	if (\intval($clientId) > 0) {
		/**
		 * getClientDataById
		 * 
		 * debug
		 */
		$clientEntity = $clientManager->getClientDataById($clientId);
		if (!($clientEntity instanceof \ClientEntity)) {
			throw new \DomainException('invalid ClientEntity');
		}
		$debugLogManager->logData('clientEntity', $clientEntity);
		
		
		$mandant = $clientEntity->getAbkz();
		require($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'db_connect.inc.php');
		
		/**
		 * getClientTableContent
		 */
		$tableData .= \getClientTableContent(
			$tableDataArray,
			$clientName
		);
		
		
		/**
		 * init $campaignManager
		 */
		require(DIR_configsInit . 'initCampaignManager.php');
		/* @var $campaignManager CampaignManager */
		
		/**
		 * getCampaignsCountByQueryParts
		 * 
		 * debug
		 */
		$campaignCount = $campaignManager->getCampaignsCountByQueryParts($campaign_queryPartsDataArray);
		$debugLogManager->logData('campaignCount', $campaignCount);
		if ($campaignCount > 0) {
			$footerDataArray = array(
				'totalBooked' => 0,
				'totalSend' => 0,
				'totalOpener' => 0,
				'totalClicks' => 0,
				'totalHBounces' => 0,
				'totalSBounces' => 0,
				'totalUnsubscriber' => 0
			);

			if (\intval($year) > 0) {
				if (\intval($month) > 0) {
					$listName = 'KW';

					// viewData_week
					require('View/viewData_week.php');
				} else {
					$listName = 'Monat';

					// viewData_month
					require('View/viewData_month.php');
				}
			} else {
				$listName = 'Jahr';

				// viewData_year
				require('View/viewData_year.php');
			}
			
			// viewData_header
			require('View/viewData_header.php');

			// viewData_footer
			require('View/viewData_footer.php');

			$tableData .= $tableHeaderContent
				. $tableFooterContent
				. $tableContent
			;
			unset($footerDataArray, $tableHeaderContent, $tableFooterContent, $tableContent);
		} else {
			// viewData_noData
			require('View/viewData_noData.php');

			$tableData .= $tableContent;
			unset($tableContent);
		}
		
		$tableData .= $tableDataArray['end'];
	} else {
		$_SESSION['tcmData']['overview'] = array(
			'totalBooked' => 0,
			'totalSend' => 0,
			'totalOpener' => 0,
			'totalClicks' => 0,
			'totalHBounces' => 0,
			'totalSBounces' => 0,
			'totalUnsubscriber' => 0
		);
		
		foreach ($userClientsDataArray as $key => $clientTitle) {
			// debug
			$debugLogManager->beginGroup($clientTitle);
			
			/**
			 * getClientDataById
			 * 
			 * debug
			 */
			$clientEntity = $clientManager->getClientDataById($key);
			if (!($clientEntity instanceof \ClientEntity)) {
				throw new \DomainException('invalid ClientEntity');
			}
			$debugLogManager->logData('clientEntity', $clientEntity);
			
			
			$mandant = $clientEntity->getAbkz();
			require($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'db_connect.inc.php');
			
			
			/**
			 * getClientTableContent
			 */
			$tableData .= \getClientTableContent(
				$tableDataArray,
				$clientTitle
			);
			
			
			/**
			 * init $campaignManager
			 */
			require(DIR_configsInit . 'initCampaignManager.php');
			/* @var $campaignManager CampaignManager */
			
			
			/**
			 * getCampaignsCountByQueryParts
			 * 
			 * debug
			 */
			$campaignCount = $campaignManager->getCampaignsCountByQueryParts($campaign_queryPartsDataArray);
			$debugLogManager->logData('campaignCount', $campaignCount);
			if ($campaignCount > 0) {
				$footerDataArray = array(
					'totalBooked' => 0,
					'totalSend' => 0,
					'totalOpener' => 0,
					'totalClicks' => 0,
					'totalHBounces' => 0,
					'totalSBounces' => 0,
					'totalUnsubscriber' => 0
				);
				
				if (\intval($year) > 0) {
					if (\intval($month) > 0) {
						$listName = 'KW';

						// viewData_week
						require('View/viewData_week.php');
					} else {
						$listName = 'Monat';

						// viewData_month
						require('View/viewData_month.php');
					}
				} else {
					$listName = 'Jahr';

					// viewData_year
					require('View/viewData_year.php');
				}

				// viewData_header
				require('View/viewData_header.php');

				// viewData_footer
				require('View/viewData_footer.php');

				$tableData .= $tableHeaderContent
					. $tableFooterContent
					. $tableContent
				;
				unset($tableHeaderContent, $tableFooterContent, $tableContent);


				$_SESSION['tcmData']['overview']['totalBooked'] += $footerDataArray['totalBooked'];
				$_SESSION['tcmData']['overview']['totalSend'] += $footerDataArray['totalSend'];
				$_SESSION['tcmData']['overview']['totalOpener'] += $footerDataArray['totalOpener'];
				$_SESSION['tcmData']['overview']['totalClicks'] += $footerDataArray['totalClicks'];
				$_SESSION['tcmData']['overview']['totalHBounces'] += $footerDataArray['totalHBounces'];
				$_SESSION['tcmData']['overview']['totalSBounces'] += $footerDataArray['totalSBounces'];
				$_SESSION['tcmData']['overview']['totalUnsubscriber'] += $footerDataArray['totalUnsubscriber'];
				unset($footerDataArray);
			} else {
				// viewData_noData
				require('View/viewData_noData.php');

				$tableData .= $tableContent;
				unset($tableContent);
			}
			
			$tableData .= $tableDataArray['end'];
			
			// debug
			$debugLogManager->endGroup();
		}
		
		// viewData_totalFooter
		require_once('View/viewData_totalFooter.php');
		$tableData .= $tableTotalContent;
		unset($tableTotalContent);
	}
	
	unset($tableDataArray);
	
	// debug
	$debugLogManager->endGroup();
} catch (\Exception $e) {
	require(DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}

$tbl = 
	'<div style="margin-bottom: 10px;" id="hideContent">' 
		// tcmYear
		. \HtmlFormUtils::createSelectItem(
			array(
				'id' => 'tcmYear',
				'name' => 'tcmYear',
				'class' => 'floatLeft',
				'style' => 'font-size:14px;color:#565656;font-weight:bold;',
				'onchange' => 'changeMonth(this.value, \'tcmMonth\');'
			),
			\HtmlFormUtils::createOptionListItemDataWithoutKey(
				$_SESSION['campaign']['yearsDataArray'],
				$year
			),
			true,
			'- alle Jahre -'
		) 
		// tcmMonth
		. \HtmlFormUtils::createSelectItem(
			array(
				'id' => 'tcmMonth',
				'name' => 'tcmMonth',
				'class' => 'floatLeft',
				'style' => 'font-size:14px;color:#565656;font-weight:bold;'
			),
			\HtmlFormUtils::createOptionListItemData(
				$monthDataArray,
				$month
			),
			true,
			'- alle Monate -'
		) 
		// tcmAsp
		. \HtmlFormUtils::createSelectItem(
			array(
				'id' => 'tcmAsp',
				'name' => 'tcmAsp',
                                'onchange' => 'changeViewAsp();',
				'value' => 'anzeigen',
				'class' => 'floatLeft',
				'style' => 'font-size:14px;color:#565656;font-weight:bold;'
			),
			\HtmlFormUtils::createOptionListItemData(
				$aspDataArray,
				$asp
			),
			true,
			'- alle ASP -'
		)        
		// tcmSelection
		. \HtmlFormUtils::createSelectItem(
			array(
				'id' => 'tcmSelection',
				'name' => 'tcmSelection',
				'class' => 'floatLeft',
				'style' => 'font-size:14px;color:#565656;font-weight:bold;'
			),
			\CampaignAndOthersUtils::getCountriesSelectItems($countryId),
			true,
			'- alle L&auml;nder -'
		) 
               // tcmClickProfile
		. \HtmlFormUtils::createSelectItem(
			array(
				'id' => 'tcmClickProfile',
				'name' => 'tcmClickProfile',
				'class' => 'floatLeft',
				'style' => 'font-size:14px;color:#565656;font-weight:bold;'
			),
			\HtmlFormUtils::createOptionListItemData(
				$clientClickProfilesDataArray,
				$clickProfile
			),
			true,
			'- alle Klick Profiles -'
		)
		// TODO: deliverySystemDistributorEntity!!!
               . \HtmlFormUtils::createSelectItem(
			array(
				'id' => 'deliverySystemDistributor',
				'name' => 'deliverySystemDistributor',
				'class' => 'floatLeft',
				'style' => 'font-size:14px;color:#565656;font-weight:bold;'
			),
			$deliverySystemDistributorOptionItems,
			true,
			'- alle Verteiler -'
		) 
		// tcmClient
		. \HtmlFormUtils::createSelectItem(
			array(
				'id' => 'tcmClient',
				'name' => 'tcmClient',
				'class' => 'floatLeft',
				'style' => 'font-size:14px;color:#565656;font-weight:bold;'
			),
			\HtmlFormUtils::createOptionListItemData(
				$userClientsDataArray,
				$clientId
			),
			true,
			'- alle Mandanten -'
		) 
		. \HtmlFormUtils::createButton(
			array(
				'id' => 'changeView',
				'onclick' => 'changeView();',
				'value' => 'anzeigen',
				'style' => 'margin-left: 20px;'
			)
		) 
		. '<br style="clear: both;" />' 
		. '<div style="margin: 10px 0 0 5px;">' 
			. '<span style="float: left; padding-right: 10px;">Ber&uuml;cksichtigt nach:</span>' 
			. \HtmlFormUtils::createRadioboxItemWidthLabel(
				array(
					'name' => 'campaignView',
					'id' => 'campaignView_1',
					'value' => 1,
					'class' => 'floatLeft',
					'checked' => $campaignView_1
				),
				'Hauptkampagne-Datum',
				array(
					'for' => 'campaignView_1',
					'style' => 'color: #000000; width: auto; padding: 2px 20px 2px 10px; clear: none;'
				)
			) 
			. \HtmlFormUtils::createRadioboxItemWidthLabel(
				array(
					'name' => 'campaignView',
					'id' => 'campaignView_0',
					'value' => 0,
					'class' => 'floatLeft',
					'checked' => $campaignView_0
				),
				'Versanddatum',
				array(
					'for' => 'campaignView_0',
					'style' => 'color: #000000; width: auto; padding: 2px 0 2px 10px; clear: none;'
				)
			) 
			. '<br style="clear: both;" />'
		. '</div>' 
	. '</div>' 
	. '<h1 class="tcmViewHeader">' 
		. 'Performance-Statistik ' . $yearName . ', ' . $monthName . ', ' . $clientName . ' nach ' . (($campaignView) ? 'Hauptkampagne-Datum' : 'Versanddatum')
	. '</h1>' 
	. $tableData
;

$_SESSION['data'] = \utf8_decode($tbl);

require_once('../menu.php');

unset($_SESSION['tcmData']);
?>
<script type="text/javascript">
	function changeMonth(value, monthSelector) {
		if (isNaN(parseInt(value))) {
			resetSelectedValueById(monthSelector);
		}
	}
	
	function changeView() {
		setRequest('kampagne/View/Specialisten/PerformanceList/index.php' 
				+ '?year=' + YAHOO.util.Dom.get('tcmYear').value 
				+ '&month=' + YAHOO.util.Dom.get('tcmMonth').value 
				+ '&selection=' + YAHOO.util.Dom.get('tcmSelection').value 
                                + '&clickProfile=' + YAHOO.util.Dom.get('tcmClickProfile').value 
				+ '&client=' + YAHOO.util.Dom.get('tcmClient').value 
                                + '&asp=' + YAHOO.util.Dom.get('tcmAsp').value 
                                + '&deliverySystemDistributor=' + YAHOO.util.Dom.get('deliverySystemDistributor').value 
				+ '&campaignView=' + getCheckedValue(document.getElementsByName('campaignView')) 
				+ '&p=5'
			,
			'k_tcm'
		);
	}
        function changeViewAsp() {
		setRequest('kampagne/View/Specialisten/PerformanceList/index.php' 
				+ '?year=' + YAHOO.util.Dom.get('tcmYear').value 
				+ '&month=' + YAHOO.util.Dom.get('tcmMonth').value 
				+ '&selection=' + YAHOO.util.Dom.get('tcmSelection').value 
                                + '&clickProfile=' + YAHOO.util.Dom.get('tcmClickProfile').value 
				+ '&client=' + YAHOO.util.Dom.get('tcmClient').value 
                                + '&asp=' + YAHOO.util.Dom.get('tcmAsp').value 
                                + '&deliverySystemDistributor=' + YAHOO.util.Dom.get('deliverySystemDistributor').value 
				+ '&campaignView=' + getCheckedValue(document.getElementsByName('campaignView')) 
				+ '&p=5'
			,
			'k_tcm'
		);
	}
</script>