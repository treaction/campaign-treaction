<?php
/**
 * createTableHeaderRow - Tag
 */
$tableHeadDataArray = \HtmlTableUtils::createTableHeaderRow(
	array(
		'class' => 'headerStyleColumn'
	)
);

// createTableFooterRow - Tag
$tableFooterDataArray = \HtmlTableUtils::createTableFooterRow(
	array(
		'class' => 'footerRow'
	)
);

$tableTotalContent = 
	$tableDataArray['begin']
		. $tableHeadDataArray['begin']
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(
					'colspan' => $_SESSION['tcmData']['totalColspan']
				),
				'Gesamtsumme aus allen Mandanten:'
			)
		. $tableHeadDataArray['end']
		. $tableFooterDataArray['begin']
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(
					'width' => 160
				),
				''
			)
;

if ((boolean) $campaignView === true) {
	$tableTotalContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
		$cellHeaderSettingsDataArray,
		\FormatUtils::numberFormat($_SESSION['tcmData']['overview']['totalBooked'])
	);
}

$tableTotalContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
	$cellHeaderSettingsDataArray,
	\FormatUtils::numberFormat($_SESSION['tcmData']['overview']['totalSend'])
);

if ((boolean) $campaignView === true) {
	$tableTotalContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
		$cellHeaderSettingsDataArray,
		\FormatUtils::rateCalculation(
			$_SESSION['tcmData']['overview']['totalSend'],
			$_SESSION['tcmData']['overview']['totalBooked']
		) . ' %'
	);
}

$tableTotalContent .= 
			\HtmlTableUtils::createTableHeaderCellWidthContent(
				$cellHeaderSettingsDataArray,
				\FormatUtils::numberFormat($_SESSION['tcmData']['overview']['totalOpener'])
					. ' (' . \FormatUtils::rateCalculation(
						$_SESSION['tcmData']['overview']['totalOpener'],
						$_SESSION['tcmData']['overview']['totalSend']
					) . ' %)'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				$cellHeaderSettingsDataArray, \FormatUtils::numberFormat($_SESSION['tcmData']['overview']['totalClicks'])
					. ' (' . \FormatUtils::rateCalculation(
						$_SESSION['tcmData']['overview']['totalClicks'],
						$_SESSION['tcmData']['overview']['totalSend']
					) . ' %)'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				$cellHeaderSettingsDataArray,
				\FormatUtils::numberFormat($_SESSION['tcmData']['overview']['totalHBounces'])
					. ' (' . \FormatUtils::rateCalculation(
						$_SESSION['tcmData']['overview']['totalHBounces'],
						$_SESSION['tcmData']['overview']['totalSend']
					) . ' %)'
			) . \HtmlTableUtils::createTableHeaderCellWidthContent(
				$cellHeaderSettingsDataArray,
				\FormatUtils::numberFormat($_SESSION['tcmData']['overview']['totalSBounces'])
					. ' (' . \FormatUtils::rateCalculation(
						$_SESSION['tcmData']['overview']['totalSBounces'],
						$_SESSION['tcmData']['overview']['totalSend']
					) . ' %)'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				$cellHeaderSettingsDataArray,
				\FormatUtils::numberFormat($_SESSION['tcmData']['overview']['totalUnsubscriber'])
					. ' (' . \FormatUtils::rateCalculation(
						$_SESSION['tcmData']['overview']['totalUnsubscriber'],
						$_SESSION['tcmData']['overview']['totalSend']
					) . ' %)'
			)
		. $tableFooterDataArray['end']
	. $tableDataArray['end']
;