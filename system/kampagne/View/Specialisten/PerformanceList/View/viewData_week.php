<?php
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */

$weekBeginObj = new \DateTime();
$weekBeginObj->setDate($year, $month, 1);

$weekEndObj = new \DateTime();
$weekEndObj->setDate($year, $month, \DateUtils::getDayCountByMonthAndYear($month, $year));

$fakeWeekBeginDataArray = array();
$fakeWeekEndDataArray = array();
switch ($month) {
	case 12:
		// dezember
		if (\intval($weekEndObj->format('W')) === 1) {
			// TODO: evtl. eine bessere methode f�r die kv berechnung
			$tmpDateTime = new \DateTime();
			$tmpDateTime->setDate($year, $month, \DateUtils::getDayCountByMonthAndYear($month, $year) - 2);
			if (\intval($tmpDateTime->format('W')) === 1) {
				$tmpDateTime->setDate($year, $month, \DateUtils::getDayCountByMonthAndYear($month, $year) - 5);
				$weekEnd = \intval($tmpDateTime->format('W'));
			} else {
				$weekEnd = \intval($tmpDateTime->format('W')) + 1;
				$fakeWeekEndDataArray[$weekEnd] = $weekEndObj->format('W');
			}
			unset($tmpDateTime);
		} else {
			$weekEnd = \intval($weekEndObj->format('W'));
		}

		$weekBegin = \intval($weekBeginObj->format('W'));
		break;

	case 1:
		// januar
		if (\intval($weekBeginObj->format('W')) >= 52) {
			$weekBegin = 0;
			$fakeWeekBeginDataArray[0] = $weekBeginObj->format('W');
		} else {
			$weekBegin = \intval($weekBeginObj->format('W'));
		}

		$weekEnd = \intval($weekEndObj->format('W'));
		break;

	default:
		$weekBegin = \intval($weekBeginObj->format('W'));
		$weekEnd = \intval($weekEndObj->format('W'));
		break;
}

$weekDayBegin = new \DateTime();
$weekDayEnd = new \DateTime();

$tableContent = '';
$i = 0;
for ($week = $weekBegin; $week <= $weekEnd; $week++) {
	// debug
	$debugLogManager->beginGroup($week);
	
	$weekDayBegin->setISODate($year, $week, 1);
	$weekDayEnd->setISODate($year, $week, 7);

	if ((\array_key_exists($week, $fakeWeekBeginDataArray)) === true) {
		$realWeek = $fakeWeekBeginDataArray[$week];
		$sqlYear = $year - 1;
	} elseif ((\array_key_exists($week, $fakeWeekEndDataArray)) === true) {
		$realWeek = $fakeWeekEndDataArray[$week];
		$sqlYear = $year + 1;
	} else {
		$realWeek = $week;
		$sqlYear = $year;
	}

	$hvCampaign_queryPartsDataArray = $queryPartsDataArray = array(
		'SELECT' => array(
			'versendet' => 'SUM(`versendet`)',
			'openings_all' => 'SUM(`openings_all`)',
			'klicks_all' => 'SUM(`klicks_all`)',
			'sbounces' => 'SUM(`sbounces`)',
			'hbounces' => 'SUM(`hbounces`)',
			'abmelder' => 'SUM(`abmelder`)'
		),
		'WHERE' => array(
                        'asp' => array(
			'sql' => '`versandsystem`',
			'value' => ($asp) ? $asp : '',
			'comparison' => '='
		        ),  
                      'click_profile' => array(
			'sql' => '`campaign_click_profiles_id`',
			'value' => ($clickProfile) ? $clickProfile : '',
			'comparison' => '='
		        ),
			'targetGroup' => array(
				'sql' => '`zielgruppe`',
				'value' => $targetGroup,
				'comparison' => 'LIKE BINARY'
			),
			'week' => array(
				'sql' => 'WEEK(`datum`, 1)',
				'value' => $realWeek,
				'comparison' => '='
			),
			'year' => array(
				'sql' => 'YEAR(`datum`)',
				'value' => $sqlYear,
				'comparison' => '='
			),
                        'deliverySystemDistributor' => array(
			        'sql' => '`dsd_id`',
			        'value' => $deliverySystemDistributor,
			        'comparison' => '='
		  ),    
		)
	);
	$debugLogManager->logData('queryPartsDataArray', $queryPartsDataArray);


	$hvCampaign_queryPartsDataArray['SELECT'] = array(
		'k_id' => '`k_id`',
		'nv_id' => '`nv_id`',
		'gebucht' => '`gebucht`'
	);
	$hvCampaign_queryPartsDataArray['WHERE']['nv_id'] = array(
		'sql' => '`k_id`',
		'value' => '`nv_id`',
		'comparison' => 'fieldEqual'
	);
	$debugLogManager->logData('hvCampaign_queryPartsDataArray', $hvCampaign_queryPartsDataArray);


	$booked = 0;
	$campaignEntity = new \CampaignEntity();

	if ((boolean) $campaignView === true) {
		/**
		 * getCampaignsDataItemsByQueryParts
		 * 
		 * debug
		 */
		$hvCampaignsDataArray = $campaignManager->getCampaignsDataItemsByQueryParts(
			$hvCampaign_queryPartsDataArray,
			\PDO::FETCH_CLASS
		);
		$debugLogManager->logData('hvCampaignsDataArray', $hvCampaignsDataArray);

		if (\count($hvCampaignsDataArray) > 0) {
			$kIdsDataArray = array();
			foreach ($hvCampaignsDataArray as $hvCampaignEntity) {
				/* @var $hvCampaignEntity \CampaignEntity */

				$booked += $hvCampaignEntity->getGebucht();
				$kIdsDataArray[] = $hvCampaignEntity->getK_id();
			}
			$debugLogManager->logData('kIdsDataArray', $kIdsDataArray);
			unset($hvCampaignsDataArray);

			if (\count($kIdsDataArray) > 0) {
				/**
				 * getCampaignDataItemsForNvIds
				 * 
				 * debug
				 */
				$nvCampaignsDataArray = $campaignManager->getCampaignDataItemsForNvIds(
					$queryPartsDataArray['SELECT'],
					$kIdsDataArray,
					\CampaignManager::$fieldIn,
					\PDO::FETCH_CLASS
				);
				$debugLogManager->logData('nvCampaignsDataArray', $nvCampaignsDataArray);

				if (\count($nvCampaignsDataArray) > 0) {
					$nvCampaignEntity = current($nvCampaignsDataArray);
					$debugLogManager->logData('nvCampaignEntity', $nvCampaignEntity);

					if (!($nvCampaignEntity instanceof \CampaignEntity)) {
						throw new \DomainException('invalid CampaignEntity');
					}
					
					$campaignEntity->setGebucht($booked);
					$campaignEntity->setVersendet($nvCampaignEntity->getVersendet());
					$campaignEntity->setOpenings_all($nvCampaignEntity->getOpenings_all());
					$campaignEntity->setKlicks_all($nvCampaignEntity->getKlicks_all());
					$campaignEntity->setSbounces($nvCampaignEntity->getSbounces());
					$campaignEntity->setHbounces($nvCampaignEntity->getHbounces());
					$campaignEntity->setAbmelder($nvCampaignEntity->getAbmelder());
					unset($nvCampaignEntity);
				}
			}
			unset($kIdsDataArray);
		} else {
			#throw new \InvalidArgumentException('no hvCampaignsDataArray');
		}
	} else {
		/**
		 * getCampaignDataItemByQueryParts
		 * 
		 * debug
		 */
		$allCampaignEntity = $campaignManager->getCampaignDataItemByQueryParts(
			$queryPartsDataArray,
			\PDO::FETCH_CLASS
		);
		$debugLogManager->logData('allCampaignEntity', $allCampaignEntity);

		if ($allCampaignEntity instanceof \CampaignEntity) {
			$campaignEntity->setVersendet($allCampaignEntity->getVersendet());
			$campaignEntity->setOpenings_all($allCampaignEntity->getOpenings_all());
			$campaignEntity->setKlicks_all($allCampaignEntity->getKlicks_all());
			$campaignEntity->setSbounces($allCampaignEntity->getSbounces());
			$campaignEntity->setHbounces($allCampaignEntity->getHbounces());
			$campaignEntity->setAbmelder($allCampaignEntity->getAbmelder());
		} else {
			//throw new \DomainException('invalid CampaignEntity');
		}
		unset($allCampaignEntity);
	}


	// footerDataArray
	$footerDataArray['totalBooked'] += $campaignEntity->getGebucht();
	$footerDataArray['totalSend'] += $campaignEntity->getVersendet();
	$footerDataArray['totalOpener'] += $campaignEntity->getOpenings_all();
	$footerDataArray['totalClicks'] += $campaignEntity->getKlicks_all();
	$footerDataArray['totalHBounces'] += $campaignEntity->getHbounces();
	$footerDataArray['totalSBounces'] += $campaignEntity->getSbounces();
	$footerDataArray['totalUnsubscriber'] += $campaignEntity->getAbmelder();


	$tableContent .=
		$tableRowDataArray['begin']
		. \HtmlTableUtils::createTableCellWidthContent(
			array(),
			\intval($realWeek) . ' (' . $weekDayBegin->format('d.m.Y') . ' - ' . $weekDayEnd->format('d.m.Y') . ')'
		)
	;

	require('viewTableData.php');

	$tableContent .= $tableRowDataArray['end'];
	unset($campaignEntity);

	$i++;
	
	// debug
	$debugLogManager->endGroup();
}