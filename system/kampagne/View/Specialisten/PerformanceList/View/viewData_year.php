<?php
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */

$tableContent = '';
$i = 0;
foreach ($_SESSION['campaign']['yearsDataArray'] as $kYear) {
	// debug
	$debugLogManager->beginGroup($kYear);
	
	$hvCampaign_queryPartsDataArray = $queryPartsDataArray = array(
		'SELECT' => array(
			'versendet' => 'SUM(`versendet`)',
			'openings_all' => 'SUM(`openings_all`)',
			'klicks_all' => 'SUM(`klicks_all`)',
			'sbounces' => 'SUM(`sbounces`)',
			'hbounces' => 'SUM(`hbounces`)',
			'abmelder' => 'SUM(`abmelder`)'
		),
		'WHERE' => array(
                        'asp' => array(
			'sql' => '`versandsystem`',
			'value' => ($asp) ? $asp : '',
			'comparison' => '='
		        ),    
			'targetGroup' => array(
				'sql' => '`zielgruppe`',
				'value' => $targetGroup,
				'comparison' => 'LIKE BINARY'
			),
			'year' => array(
				'sql' => 'YEAR(`datum`)',
				'value' => $kYear,
				'comparison' => '='
			),
                      'click_profile' => array(
			'sql' => '`campaign_click_profiles_id`',
			'value' => ($clickProfile) ? $clickProfile : '',
			'comparison' => '='
		       ),
                     'deliverySystemDistributor' => array(
			        'sql' => '`dsd_id`',
			        'value' => $deliverySystemDistributor,
			        'comparison' => '='
		        ),    
		),
	);
	$debugLogManager->logData('queryPartsDataArray', $queryPartsDataArray);


	$hvCampaign_queryPartsDataArray['SELECT'] = array(
		'k_id' => '`k_id`',
		'nv_id' => '`nv_id`',
		'gebucht' => '`gebucht`'
	);
	$hvCampaign_queryPartsDataArray['WHERE']['nv_id'] = array(
		'sql' => '`k_id`',
		'value' => '`nv_id`',
		'comparison' => 'fieldEqual'
	);
	$debugLogManager->logData('hvCampaign_queryPartsDataArray', $hvCampaign_queryPartsDataArray);


	$booked = 0;
	$campaignEntity = new \CampaignEntity();

	if ((boolean) $campaignView === true) {
		/**
		 * getCampaignsDataItemsByQueryParts
		 * 
		 * debug
		 */
		$hvCampaignsDataArray = $campaignManager->getCampaignsDataItemsByQueryParts(
			$hvCampaign_queryPartsDataArray,
			\PDO::FETCH_CLASS
		);
		$debugLogManager->logData('hvCampaignsDataArray', $hvCampaignsDataArray);

		if (\count($hvCampaignsDataArray) > 0) {
			$kIdsDataArray = array();
			foreach ($hvCampaignsDataArray as $hvCampaignEntity) {
				/* @var $hvCampaignEntity \CampaignEntity */

				$booked += $hvCampaignEntity->getGebucht();
				$kIdsDataArray[] = $hvCampaignEntity->getK_id();
			}
			$debugLogManager->logData('kIdsDataArray', $kIdsDataArray);
			unset($hvCampaignsDataArray);

			if (\count($kIdsDataArray) > 0) {
				/**
				 * getCampaignDataItemsForNvIds
				 * 
				 * debug
				 */
				$nvCampaignsDataArray = $campaignManager->getCampaignDataItemsForNvIds(
					$queryPartsDataArray['SELECT'],
					$kIdsDataArray,
					\CampaignManager::$fieldIn,
					\PDO::FETCH_CLASS
				);
				$debugLogManager->logData('nvCampaignsDataArray', $nvCampaignsDataArray);

				if (\count($nvCampaignsDataArray) > 0) {
					$nvCampaignEntity = current($nvCampaignsDataArray);
					$debugLogManager->logData('nvCampaignEntity', $nvCampaignEntity);

					if (!($nvCampaignEntity instanceof \CampaignEntity)) {
						throw new \DomainException('invalid CampaignEntity');
					}
					
					$campaignEntity->setGebucht($booked);
					$campaignEntity->setVersendet($nvCampaignEntity->getVersendet());
					$campaignEntity->setOpenings_all($nvCampaignEntity->getOpenings_all());
					$campaignEntity->setKlicks_all($nvCampaignEntity->getKlicks_all());
					$campaignEntity->setSbounces($nvCampaignEntity->getSbounces());
					$campaignEntity->setHbounces($nvCampaignEntity->getHbounces());
					$campaignEntity->setAbmelder($nvCampaignEntity->getAbmelder());
					unset($nvCampaignEntity);
				}
			}
			unset($kIdsDataArray);
		} else {
			throw new \InvalidArgumentException('no hvCampaignsDataArray');
		}
	} else {
		/**
		 * getCampaignDataItemByQueryParts
		 * 
		 * debug
		 */
		$allCampaignEntity = $campaignManager->getCampaignDataItemByQueryParts(
			$queryPartsDataArray,
			\PDO::FETCH_CLASS
		);
		$debugLogManager->logData('allCampaignEntity', $allCampaignEntity);

		if ($allCampaignEntity instanceof \CampaignEntity) {
			$campaignEntity->setVersendet($allCampaignEntity->getVersendet());
			$campaignEntity->setOpenings_all($allCampaignEntity->getOpenings_all());
			$campaignEntity->setKlicks_all($allCampaignEntity->getKlicks_all());
			$campaignEntity->setSbounces($allCampaignEntity->getSbounces());
			$campaignEntity->setHbounces($allCampaignEntity->getHbounces());
			$campaignEntity->setAbmelder($allCampaignEntity->getAbmelder());
		} else {
			//throw new \DomainException('invalid CampaignEntity');
		}
		unset($allCampaignEntity);
	}


	// footerDataArray
	$footerDataArray['totalBooked'] += $campaignEntity->getGebucht();
	$footerDataArray['totalSend'] += $campaignEntity->getVersendet();
	$footerDataArray['totalOpener'] += $campaignEntity->getOpenings_all();
	$footerDataArray['totalClicks'] += $campaignEntity->getKlicks_all();
	$footerDataArray['totalHBounces'] += $campaignEntity->getHbounces();
	$footerDataArray['totalSBounces'] += $campaignEntity->getSbounces();
	$footerDataArray['totalUnsubscriber'] += $campaignEntity->getAbmelder();


	$tableContent .=
		$tableRowDataArray['begin']
			. \HtmlTableUtils::createTableCellWidthContent(
				array(),
				\intval($kYear)
			)
	;

	require('viewTableData.php');

	$tableContent .= $tableRowDataArray['end'];
	unset($campaignEntity);

	$i++;
	
	// debug
	$debugLogManager->endGroup();
}