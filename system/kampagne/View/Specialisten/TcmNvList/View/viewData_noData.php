<?php
$tableContent = 
	$tableRowDataArray['begin']
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'noCampaign',
				'colspan' => $_SESSION['tcmData']['totalColspan']
			),
			'Keine Kampagnen im aktuellen Zeitraum'
		)
	. $tableRowDataArray['end']
;