<?php
/* @var $clientManager \ClientManager */
/* @var $debugLogManager \debugLogManager */

/**
 * getActiveClientClickProfilesDataItems
 * 
 * debug
 */
$clientClickProfilesDataArray = $clientManager->getActiveClientClickProfilesDataItems($clientId);
$debugLogManager->logData('clientClickProfilesDataArray', $clientClickProfilesDataArray);
/* @var $clientClickProfilesDataArray \ClientClickProfileWidthClickProfileEntity */

$tableContent = '';
foreach ($campaignDataArray as $campaignEntity) {
	/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
	
	$debugLogManager->logData('campaignEntity', $campaignEntity);

	$tableContent .=
		$tableRowDataArray['begin']
			// Datum
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'tcm_date time'
				),
				$campaignEntity->getDatum()->format('H:i')
			) 
			// Kampagne
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'campaign'
				),
				\FormatUtils::cutString(
					$campaignEntity->getK_name(),
					40
				)
			) 
			// Kunde / Agentur
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'customerCompany'
				),
				\FormatUtils::cutString(
					$campaignEntity->getCustomerEntity()->getFirma(),
					40
				)
			) 
			// Asp.
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'deliverySystem',
					'nowrap' => 'nowrap'
				),
				$campaignEntity->getVersandsystem()
			) 
			// Verteiler
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'deliverySystemDistributor',
					'nowrap' => 'nowrap'
				),
				($campaignEntity->getDsd_id() > 0 
					? $clientDeliverySystemDistributorEntityDataArray[$campaignEntity->getDsd_id()]->getTitle() 
					: ''
				)
			) 
			// Gebucht
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'bookingAmount',
					'align' => 'right'
				),
				\FormatUtils::numberFormat($campaignEntity->getGebucht())
			) 
			// Real
			. \HtmlTableUtils::createTableCellWidthContent(
					array(
						'class' => 'targetVolume',
						'align' => 'right'
					),
					($campaignEntity->getVorgabe_m() > 0 
						? \FormatUtils::numberFormat($campaignEntity->getVorgabe_m()) 
						: ''
					)
			) 
			// Notiz
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'notiz',
				),
				(\strlen($campaignEntity->getNotiz()) > 0 
					? \nl2br($campaignEntity->getNotiz() . \chr(13))
					: ''
				) 
					. (\count($clientClickProfilesDataArray) > 0 
							&& $campaignEntity->getCampaign_click_profiles_id() > 0
						? \nl2br($clientClickProfilesDataArray[$campaignEntity->getCampaign_click_profiles_id()]->getClickProfile()->getShortcut()) 
						: ''
					)
			) 
			// Zielgruppe
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'targetGroup',
				),
				$campaignEntity->getZielgruppe()
			) 
			// BL
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'blacklist',
					'align' => 'center'
				),
				($campaignEntity->getBlacklist() == 'X' 
					? '<span style="background-color:red;color:#FFFFFF;padding:1px 5px 1px 5px;font-weight:bold;font-size:12px">X</span>' 
					: ''
				)
			) 
			// Status
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'status',
					'nowrap' => 'nowrap'
				),
				$_SESSION['campaign']['statusDataArray'][$campaignEntity->getStatus()]['label']
			) 
		. $tableRowDataArray['end']
	;
}
unset($clientClickProfilesDataArray);