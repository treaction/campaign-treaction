<?php
require_once('../../emsConfig.php');
require_once('../../EmsAutoloader.php');
\EmsAutoloader::init();

session_start();

header('Content-Type: text/html; charset=ISO-8859-1');


/**
 * init $debugLogManager
 * init $clientManager
 */
require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager debugLogManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager ClientManager */


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);
#$debugLogManager->logData('SESSION', $_SESSION);


try {
	/**
	 * getClientDataById
	 * 
	 * debug
	 */
	$clientEntity = $clientManager->getClientDataById($_SESSION['mID']);
	if (!($clientEntity instanceof ClientEntity)) {
		throw new \DomainException('no ClientEntity');
	}
	$debugLogManager->logData('clientEntity', $clientEntity);
	
	/**
	 * getUserDataItemById
	 * 
	 * debug
	 */
	$userEntity = $clientManager->getUserDataItemById($_SESSION['benutzer_id']);
	if (!($userEntity instanceof UserEntity)) {
		throw new \DomainException('no UserEntity');
	}
	$debugLogManager->logData('userEntity', $userEntity);
} catch (Exception $e) {
	require(DIR_configs . 'exceptions.php');
    
    die($exceptionMessage);
}
?>

<div style="height:120px;background-image:url(img/rep_bg.png);background-repeat:repeat-x;background-position:100% 100%;
	 ;border-bottom:1px solid #CCCCCC;width:100%">
	<div style="padding:15px;">
		<table cellpadding="0" cellspacing="0" border="0" style="margin-top:10px;">
			<tr>
				<td style="padding-left:35px;" valign="top"><img src="img/Oxygen/48/apps/utilities-text-editor.png" /></td>
				<td valign="top" style="padding-left:5px;">
					<span class="label_big">Vorlagen Editor</span><br />
					<span class="label_sub1"></span><br />
					<span class="label_sub2">Mit diesem Editor k&ouml;nnen Sie eine Email-Vorlage f&uuml;r Ihre Reportings erstellen.</span><br /><br />
					<span id="vorlage_buttons"><input type="button" style="cursor:pointer" id="vorlage_speichern" onClick="check_this();" value="Speichern" /> <input type="button" style="cursor:pointer" id="report_testmail" onClick="send_testmail();" value="Testmail" /></span>
				</td>
			</tr>
		</table>
	</div>
</div>
<div>
    <div id="vorlage_content" style="width:100%;height:100%;background-color:#FFFFFF;">	
        <div style="background-color:#FFFFFF;vertical-align:middle;padding:25px;width:95%;height:100%;">
			<a href="#" onclick="window.open('kampagne/report/rep_platzhalter.php', '', 'width=430,height=600,scrollbars=yes');"><img src="img/Oxygen/16/actions/help-faq.png" /> Platzhalter anzeigen</a><br /><br />
            <span style="color:#8e8901">Betreff:</span> <input type="text" name="vorlage_betreff" id="vorlage_betreff" style="width:55%;height:20px;border:1px solid #999999" value="<?php echo $clientEntity->getCm_rep_betreff(); ?>" /><br /><br />
			<span style="color:#8e8901"> Vorlagentext:</span> <br />
			<textarea name="vorlage_text0" id="vorlage_text0" style="width:80%;height:350px;border:1px solid #999999"><?php echo $clientEntity->getCm_rep_mail(); ?></textarea>
        </div>
    </div>
</div>
<input type="hidden" id="testEmail" value="" />


<script type="text/javascript">
	var hi_t = document.documentElement.clientHeight - 580;
	YAHOO.util.Dom.get('vorlage_text0').style.height = hi_t + 'px';


	if (CKEDITOR.instances['vorlage_text0']) {
		CKEDITOR.remove(CKEDITOR.instances['vorlage_text0']);
	}
	CKEDITOR.replace('vorlage_text0', {
		height: hi_t,
		toolbar: [
			['Source', '-', 'Save', 'NewPage', 'Preview'],
			['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print', 'SpellChecker', 'Scayt'],
			['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
			'/',
			['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
			['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
			['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
			['Link', 'Unlink', 'Anchor'],
			['Image', 'Flash', 'Table', 'HorizontalRule'],
			'/',
			['Styles', 'Format', 'Font', 'FontSize'],
			['TextColor', 'BGColor']
		]
	});

	function send_testmail() {
		var t_email = prompt('Geben Sie hier die Emailadresse ein:', '<?php echo $userEntity->getEmail(); ?>');
		var b;

		if (t_email !== '' && t_email !== undefined) {
			YAHOO.util.Dom.get('testEmail').value = t_email;
			b = YAHOO.util.Dom.get('vorlage_betreff').value;
			
			setRequest_vorlage(0);

		}
	}

	function check_this() {
		var b = YAHOO.util.Dom.get('vorlage_betreff').value;
		var t = CKEDITOR.instances.vorlage_text0.getData();
		var fehler = '';
		
		if (b === '') {
			fehler = "- Betreff fehlt\n";
		}

		if (t === '') {
			fehler += "- Vorlagentext fehlt";
		}

		if (fehler === '') {
			YAHOO.util.Dom.get('testEmail').value = '';
			setRequest_vorlage(0);
		} else {
			alert(fehler);
			return false;
		}
	}
</script>