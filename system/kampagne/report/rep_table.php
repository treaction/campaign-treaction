<?php

header('Content-Type: text/html; charset=ISO-8859-1');
session_start();
$mandant = $_SESSION['mandant'];
include('../../db_connect.inc.php');
include('../../library/util.php');
//$query = "SELECT DISTINCT k *, k.status as status
//			FROM $kampagne_db as k
//			LEFT JOIN $log as l ON k.k_id = l.kid
//			WHERE 1 = 1 
//			AND k.k_id > 1 
//			AND k.k_id = k.nv_id 
//			AND l.status != 22
//			AND (
//					(
//						k.status = 20 
//						OR k.status = 21 
//						OR k.status = 23 
//						OR k.status = 24 
//					) OR ( 
//                    	k.status > 30 
//                    	AND (
//    						l.status = 20 
//							OR l.status = 21 
//							OR l.status = 23 
//							OR l.status = 24 
//						)
//					) 
//				) ";
//$query = "SELECT `k_id`,`mail_id`,`agentur_id`,`ap_id`,`datum`,`bearbeiter`,`k_name`,`betreff`,`absendername`,`agentur`, `gebucht`,`versendet`,`gestartet`,`beendet`,`openings`,`openings_all`,`sbounces`,`hbounces`,`vorgabe_o`,`klicks`,`klicks_all`,`vorgabe_k`,`zielgruppe`,`reagierer`,`alter_von`,`alter_bis`,`plz_von`,`plz_bis`,`zg_kommentar`,`mailtyp`,`versandsystem`,`notiz`,`tkp`,`ap`,`edit_lock`,`abrechnungsart`,`preis`,`preis_gesamt`,`leads`,`report`,`rechnung`,`erstreporting`,`endreporting`,`anfrage`,`preis2`,`vorgabe_m`,`hash`,`klickrate_extern`,`openingrate_extern`,`leads_u`,`abmelder`,`preis_gesamt_verify`,`vertriebler_id`,`last_update`,`crm_order_id`,`crm_guid`,`dsd_id`,`unit_price`,`cs_id`,`use_campaign_start_date`,`campaign_click_profiles_id`,`selection_click_profiles_id`,`premium_campaign`,`advertising_materials`,`charset`,`sender_email`,`broking_volume`,`broking_price`,`send_mail_tcm`,`send_bounce_mail`,`advertisings_comment`,`externaljobid`,`monatsrechnung`,`hybrid_preis2`,`hybrid_preis`,`dasp_id`,`trello_id`,k.status as status FROM kampagnen as k LEFT JOIN log as l ON k.k_id = l.kid WHERE 1 = 1 GROUP BY k.k_id";

$query = "SELECT DISTINCT `k_name`,`k_id`,`mail_id`,`agentur_id`,`ap_id`,`datum`,`bearbeiter`,`betreff`,`absendername`,`agentur`, `gebucht`,`versendet`,`gestartet`,`beendet`,`openings`,`openings_all`,`sbounces`,`hbounces`,`vorgabe_o`,`klicks`,`klicks_all`,`vorgabe_k`,`zielgruppe`,`reagierer`,`alter_von`,`alter_bis`,`plz_von`,`plz_bis`,`zg_kommentar`,`mailtyp`,`versandsystem`,`notiz`,`tkp`,`ap`,`edit_lock`,`abrechnungsart`,`preis`,`preis_gesamt`,`leads`,`report`,`rechnung`,`erstreporting`,`endreporting`,`anfrage`,`preis2`,`vorgabe_m`,`hash`,`klickrate_extern`,`openingrate_extern`,`leads_u`,`abmelder`,`preis_gesamt_verify`,`vertriebler_id`,`last_update`,`crm_order_id`,`crm_guid`,`dsd_id`,`unit_price`,`cs_id`,`use_campaign_start_date`,`campaign_click_profiles_id`,`selection_click_profiles_id`,`premium_campaign`,`advertising_materials`,`charset`,`sender_email`,`broking_volume`,`broking_price`,`send_mail_tcm`,`send_bounce_mail`,`advertisings_comment`,`externaljobid`,`monatsrechnung`,`hybrid_preis2`,`hybrid_preis`,`dasp_id`,`trello_id`,k.status as status 
FROM kampagnen as k 
 WHERE 1 = 1 
 AND k.k_id > 1 
 AND k.k_id = k.nv_id 
 AND (( k.status = 20 OR k.status = 21 OR k.status = 23 OR k.status = 24 ) OR ( k.status > 30 )) GROUP BY k.k_id DESC";

$sql_rep_offen = mysql_query($query, $verbindung);
/*
								GROUP BY k.k_id
							ORDER BY datum DESC",$verbindung);
 */

$rep_offen_cnt = mysql_num_rows($sql_rep_offen);
$j = 1;
$tr = '';
if ($rep_offen_cnt > 0) {

	while ($kz =mysql_fetch_array($sql_rep_offen)) {
	
		if ($j==1 || $j==3) {
			$bg_color = "#FFFFFF";
			$bg_color_fest = "#FFFFFF";
			$j = 1;
		} elseif ($j==2) {
			$bg_color = "#E4E4E4";
			$bg_color_fest = "#E4E4E4";
		} 
		
		
		$kid = $kz['k_id'];
		#$k_name = $kz['kname'];
		$k_name_full = $kz['k_name'];
		
		$asp = $kz['versandsystem'];
		if ($asp == 'BM') {
			$datum = util::bmTime($kz['datum']);
		} else {
			$datum = $kz['datum'];
		}
		
		$datum = util::datum_de($datum,'');
		$agentur = $kz['agentur'];
		$status = $kz['status'];
		
		$tr .= "<tr onclick=\"doItrep(event,'".$kid."');\" bgcolor='".$bg_color_fest."' onMouseOver=\"this.bgColor='#A6CBFF'\" onMouseOut=\"this.bgColor='".$bg_color."'\" style='cursor:pointer'>";
		$tr .= "<td style='text-align:center;width:76px' id='".$kid."_st'>".util::getCampaignStatusName($status)."</td>";
		$tr .= "<td style='white-space:nowrap;' title='".$k_name_full."'><span style='font-size:11px;font-weight:bold'>".util::cutString($k_name_full,35)."</span><br/><span style='color:#9900FF'>".$agentur."</span><br /><span style='color:#666666;font-size:9px;'>".$datum."</span></td>";
		$tr .= "</tr>";
		
		$j++;
	
	}
}
?>
<div id="rep_table">
	<table cellpadding="0" cellspacing="0" class="table_widget" style="width:300px;">
          <tr class="trHeader fixed">
               <td width="75" align="center">Status</td>
               <td width="206">Kampagne</td>
           </tr>
           <tr>
               <td colspan="2">&nbsp;</td>
           </tr>
           <?php print $tr; ?>
     </table>
</div>
	   
<div id="spacer">&nbsp;</div>

<div id="rep_details_content">
       <div style="padding:25px">
            	<span style="font-size:16px;">Reporting</span><br /><br />
                	<?php 
						if ($rep_offen_cnt > 0) {
								print "Bitte w&auml;hlen Sie eine Kampagne aus, um das Reporting zu bearbeiten.";
						} else {
							print "Es liegen keine offenen Reportings vor.";
						}
					?>
                    
                </div>
</div>

<script type="text/javascript">
wi = document.documentElement.clientWidth;
wi = wi-528;

hi = document.documentElement.clientHeight;
hi = hi-163;

document.getElementById("rep_details_content").style.width = wi+"px";
document.getElementById("rep_details_content").style.height = hi+"px";
document.getElementById("rep_table").style.height = hi+"px";
document.getElementById("spacer").style.height = hi+"px";

Resize_tab_table();

function Resize_tab_table() {
	wi = document.documentElement.clientWidth;
	wi = wi-511;
	
	hi = document.documentElement.clientHeight;
	hi = hi-163;
	document.getElementById("rep_details_content").style.width = wi+"px";
	document.getElementById("rep_details_content").style.height = hi+"px";
	document.getElementById("rep_table").style.height = hi+"px";
	document.getElementById("spacer").style.height = hi+"px";
}

window.onresize = Resize_tab_table;

function doItrep(evt,kid) { 
	
	setRequest_default("kampagne/report/rep_details.php?kid="+kid,"rep_details_content");
	var selectedRow = ( evt.target || evt.srcElement ); 
    while ( selectedRow.tagName != "TR") 
        selectedRow = selectedRow.parentNode; 

    if ( oldRow ) 
    { 
		
        oldRow.style.backgroundColor = ""; 
		oldRow.style.color = "#000000"; 
        if ( oldRow == selectedRow ) 
            return true; 
    };

	selectedRow.style.backgroundColor = "#bfdaff";
    oldRow = selectedRow;
        
    
}


</script>