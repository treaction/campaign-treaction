<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Campaign Manager::Platzhalter</title>
		<style type="text/css">
			body {
				font-family:Arial, Helvetica, sans-serif;
				font-size: 11px;
				padding:0px;
				margin:0px;
			}

			table {
				border-collapse:collapse;
			}
			table td {
				border:1px solid #666666;
				padding:3px;
			}

			.kat {
				background-color:#D9EFFF;
			}
		</style>
	</head>
	<body>
		<table cellpadding="0" cellspacing="0">
			<tr bgcolor="#333333" style="color:#FFFFFF">
				<td>Platzhalter</td>
				<td>Beschreibung</td>
				<td>Beispiel</td>
			</tr>
			<tr class="kat">
				<td colspan="3">Begr&uuml;ssung</td>
			</tr>
			<tr>
				<td>{b_standard}</td>
				<td>Standard Begr&uuml;ssung</td>
				<td>Sehr geehrter Herr</td>
			</tr>
			<tr class="kat">
				<td colspan="3">Ansprechpartner</td>
			</tr>
			<tr>
				<td>{ap_anrede}</td>
				<td>Anrede</td>
				<td>Herr</td>
			</tr>
			<tr>
				<td>{ap_titel}</td>
				<td>Titel</td>
				<td>Dr.</td>
			</tr>
			<tr>
				<td>{ap_vorname}</td>
				<td>Vorname</td>
				<td>Max</td>
			</tr>
			<tr>
				<td>{ap_nachname}</td>
				<td>Nachname</td>
				<td>Mustermann</td>
			</tr>
			<tr>
				<td>{ap_email}</td>
				<td>Email</td>
				<td>mm@agentur.de</td>
			</tr>
			<tr>
				<td>{ap_telefon}</td>
				<td>Telefon</td>
				<td>01234 - 12345</td>
			</tr>
			<tr>
				<td>{ap_fax}</td>
				<td>Fax</td>
				<td>01234 - 12345</td>
			</tr>
			<tr>
				<td>{ap_mobil}</td>
				<td>Mobil</td>
				<td>01234 - 12345</td>
			</tr>
			<tr class="kat">
				<td colspan="3">Versandinformationen</td>
			</tr>
			<tr>
				<td>{k_agentur}</td>
				<td>Name der Agentur</td>
				<td>Musterkunde GmbH</td>
			</tr>
			<tr>
				<td>{k_name}</td>
				<td>Name der Kampagne</td>
				<td>Newsletter Gutschein</td>
			</tr>
			<tr>
				<td>{k_versanddatum}</td>
				<td>Versanddatum der Kampagne</td>
				<td>01.01.2010</td>
			</tr>
			<tr>
				<td>{k_wochentag}</td>
				<td>Wochentag der Kampagne</td>
				<td>Montag</td>
			</tr>
			<tr>
				<td>{k_versandzeit}</td>
				<td>Versandzeit der Kampagne</td>
				<td>9:30</td>
			</tr>
			<tr>
				<td>{k_gebucht}</td>
				<td>Gebuchtes Volumen</td>
				<td>100.000</td>
			</tr>
			<tr>
				<td>{k_versendet}</td>
				<td>Versendetes Volumen</td>
				<td>125.000</td>
			</tr>
			<tr>
				<td>{k_zielgruppe}</td>
				<td>Zielgruppe / Selektion der Kampagne</td>
				<td>DE, Frauen, 25+</td>
			</tr>
			<tr>
				<td>{k_abrechnungsart}</td>
				<td>Abrechnungsart</td>
				<td>TKP</td>
			</tr>
			<tr>
				<td>{k_preis}</td>
				<td>Preis in Euro</td>
				<td>25</td>
			</tr>
			<tr>
				<td>{k_betreff}</td>
				<td>Betreff des Mailings</td>
				<td>Ihr Newsletter [22/2010]</td>
			</tr>
			<tr>
				<td>{k_absendername}</td>
				<td>Absendername des Mailings</td>
				<td>Newsletter</td>
			</tr>
			<tr>
				<td>{k_mime}</td>
				<td>Mailformat des Mailings</td>
				<td>multipart/alternative</td>
			</tr>
			<tr>
				<td>{k_versandsystem}</td>
				<td>Versandsystem</td>
				<td>Broadmail</td>
			</tr>
			<tr class="kat">
				<td colspan="3">Performancezahlen</td>
			</tr>
			<tr>
				<td>{k_openings}</td>
				<td>Anzahl der &Ouml;ffnungen</td>
				<td>15.123</td>
			</tr>
			<tr>
				<td>{k_opening_rate}</td>
				<td>&Ouml;ffnungsrate in %</td>
				<td>15.12</td>
			</tr>
			<tr>
				<td>{k_openings_ext}</td>
				<td>Anzahl der &Ouml;ffnungen (extern)</td>
				<td>17.123</td>
			</tr>
			<tr>
				<td>{k_opening_rate_ext}</td>
				<td>&Ouml;ffnungsrate in % (extern)</td>
				<td>17.12</td>
			</tr>
			<tr>
				<td>{k_klicks}</td>
				<td>Anzahl der Klicks</td>
				<td>5.123</td>
			</tr>
			<tr>
				<td>{k_klick_rate}</td>
				<td>Klickrate in %</td>
				<td>5.12</td>
			</tr>
			<tr>
				<td>{k_klick_rate_rel}</td>
				<td>relative Klickrate in %</td>
				<td>11.12</td>
			</tr>
			<tr>
				<td>{k_klicks_ext}</td>
				<td>Anzahl der Klicks (extern)</td>
				<td>7.123</td>
			</tr>
			<tr>
				<td>{k_klick_rate_ext}</td>
				<td>Klickrate in % (extern)</td>
				<td>7.12</td>
			</tr>
			<tr>
				<td>{k_klick_rate_rel_ext}</td>
				<td>relative Klickrate in % (extern)</td>
				<td>13.12</td>
			</tr>
			<tr class="kat">
				<td colspan="3">Benutzerdaten</td>
			</tr>
			<tr>
				<td>{u_anrede}</td>
				<td>Anrede des Benutzers</td>
				<td>Herr</td>
			</tr>
			<tr>
				<td>{u_vorname}</td>
				<td>Vorname des Benutzers</td>
				<td>Hans</td>
			</tr>
			<tr>
				<td>{u_nachname}</td>
				<td>Nachname des Benutzers</td>
				<td>Schmidt</td>
			</tr>
			<tr>
				<td>{u_email}</td>
				<td>Email des Benutzers</td>
				<td>hans.schmidt@beispiel.de</td>
			</tr>
		</table>
	</body>
</html>