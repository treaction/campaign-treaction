<?php
header('Content-Type: text/html; charset=ISO-8859-1');

session_start();

// debug
function firePhPDebug() {
    $firePhp = null;
    #if (intval($_SESSION['benutzer_id']) === 24) {
    #    require_once($_SERVER['DOCUMENT_ROOT'] . '/system/library/FirePHPCore/FirePHP.class.php');
    #    $firePhp = FirePHP::getInstance(true);
    #}
    
    return $firePhp;
}

function sendBenachrichtigungsMail($abteilung, $message, $subject, $kID, $log_table, $userID, $smtp_host, $smtp_user, $smtp_pw, $userEmail, $userVorname, $userNachname) {
    $firePhp = firePhPDebug();
    
    $logManager = new logManager();
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->group('sendBenachrichtigungsMail');
    }
    
    $mID = $_SESSION['mID'];
    $mandant = $_SESSION['mandant'];
    $nvID = $kID;
    $message = stripslashes($message);
    
    include('../../db_pep.inc.php');
    $resBenutzer = mysql_query(
        'SELECT `email` FROM `benutzer` WHERE `abteilung` = "b" AND `mid` = "' . $mID . '" AND `active` = "1"',
        $verbindung_pep
    );
    if ($resBenutzer) {
        $empfaengerArr = array();
        while (($z = mysql_fetch_array($resBenutzer)) !== false) {
            $empfaengerArr[] = $z['email'];
        }
        mysql_free_result($resBenutzer);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($empfaengerArr, '$empfaengerArr');
        }
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung_pep), 'sql error: $resBenutzer');
        }
    }

    if (count($empfaengerArr)) {
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($userEmail, 'From');
            $firePhp->log($userVorname . " " . $userNachname, 'FromName');
            $firePhp->log($mandant . ": " . utf8_decode($subject), 'Subject');
        }
        
        $mail = new PHPMailer();
        $mail->IsMail();
        
        $mail->From = $userEmail;
        $mail->FromName = $userVorname . " " . $userNachname;
        $mail->Subject = $mandant . ": " . utf8_decode($subject);
        $mail->IsHTML(true);
        $mail->Body = $message;
        
        foreach ($empfaengerArr as $receiver) {
            $receiver = trim($receiver);
            if ($receiver != '') {
                $mail->AddAddress($receiver);
            }

            if (!$mail->Send()) {
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->log($mail->ErrorInfo, $receiver . ' $receiver');
                }
                
                echo 'Die Email konnte nicht gesendet werden';
                echo 'Fehler: ' . $receiver . ' -> ' . $mail->ErrorInfo;
            } else {
                echo 'Die Email wurde versandt.';
            }

            $mail->ClearAddresses();
        }
    }

    include('../../db_connect.inc.php');
    $resUpdateKampagne = mysql_query(
        'UPDATE `' . $kampagne_db . '` SET `status` = "30" WHERE `k_id` = "' . $kID . '"',
        $verbindung
    );
    if (!$resUpdateKampagne) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: $resUpdateKampagne');
        }
    }
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->groupEnd();
    }
    
    $setlog = $logManager->setLog(
        $log_table,
        $userID,
        6,
        $kID,
        $nvID,
        0,
        0
    );
}

function zustellreport($empfaenger, $betreff, $nachricht, $userEmail, $userVorname, $userNachname, $userID, $smtp_host, $smtp_user, $smtp_pw, $log_table, $kID) {
    $firePhp = firePhPDebug();
    
    $logManager = new logManager();
    
    $nvID = $kID;
    $empfaengerArr = explode(',', $empfaenger);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->group('zustellreport');
        
        $firePhp->log($empfaengerArr, '$empfaengerArr');
        $firePhp->log($userEmail, 'From');
        $firePhp->log($userVorname . ' ' . $userNachname, 'FromName');
        $firePhp->log($empfaenger, '$empfaenger');
    }

    $mail = new PHPMailer();
    $mail->IsMail();
    $mail->From = $userEmail;
    $mail->FromName = $userVorname . ' ' . $userNachname;
    $mail->AddAddress($empfaenger); // TODO: ???
    $mail->Subject = $betreff;
    $mail->IsHTML(true);
    $mail->Body = $nachricht;

    foreach ($empfaengerArr as $receiver) {
        $receiver = trim($receiver);
        if ($receiver != '') {
            $mail->AddAddress($receiver);
        }

        if (!$mail->Send()) {
            if ($firePhp instanceof FirePHP) {
                $firePhp->log($mail->ErrorInfo, $receiver . ' $receiver');
            }
            
            echo 'Die Email konnte nicht gesendet werden';
            echo 'Fehler: ' . $mail->ErrorInfo;
        } else {
            echo 'Die Email wurde versandt.';
        }

        $mail->ClearAddresses();
    }
    
    if ($firePhp instanceof FirePHP) {
        $firePhp->groupEnd();
    }

    $setlog = $logManager->setLog(
        $log_table,
        $userID,
        7,
        $kID,
        $nvID,
        0,
        0
    );
}

$firePhp = firePhPDebug();

$mandant = $_SESSION['mandant'];
$mID = $_SESSION['mID'];
$userID = $_SESSION['benutzer_id'];
$userEmail = $_SESSION['u_email'];
$userVorname = $_SESSION['u_vorname'];
$userNachname = $_SESSION['u_nachname'];

include('../../db_connect.inc.php');
include('../../library/util.php');
include('../../library/log.php');
include('../../library/packages/PHPMailer/class.phpmailer.php');

// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($_SESSION, '$_SESSION');
    $firePhp->log($_POST, '$_POST');
}

if ($_POST['vorlage_zustellreport_mail'] && !$_POST['buchhaltungmail_']) {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->group('zustellreport - vorlage_zustellreport_mail');
        
        $firePhp->log($_POST, '$_POST');
    }
    
    $zustell_kid = $_POST['zustell_kid'];
    $empfaenger = $_POST['zustellreport_email'];
    $betreff = utf8_decode($_POST['vorlage_zustellreport_betreff_edit']);
    $mail = utf8_decode($_POST['vorlage_zustellreport_mail']);
    
    $resUpdate = mysql_query(
        'UPDATE `' . $kampagne_db . '` SET `status` = "23" WHERE `k_id` = "' . $zustell_kid . '"',
        $verbindung
    );
    if (!$resUpdate) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: $resUpdate');
        }
    }
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($empfaenger, '$empfaenger');
        $firePhp->log($betreff, '$betreff');
        $firePhp->log($mail, '$mail');
        $firePhp->log($userEmail, '$userEmail');
        $firePhp->log($userVorname, '$userVorname');
        $firePhp->log($userNachname, '$userNachname');
        $firePhp->log($userID, '$userID');
        $firePhp->log($smtp_host, '$smtp_host');
        $firePhp->log($smtp_user, '$smtp_user');
        $firePhp->log($smtp_pw, '$smtp_pw');
        $firePhp->log($log, '$log');
        $firePhp->log($zustell_kid, '$zustell_kid');
    }
    
    zustellreport(
        $empfaenger,
        $betreff,
        $mail,
        $userEmail,
        $userVorname,
        $userNachname,
        $userID,
        $smtp_host,
        $smtp_user,
        $smtp_pw,
        $log,
        $zustell_kid
    );
    
    if ($firePhp instanceof FirePHP) {
        $firePhp->groupEnd();
    }
}

if ($_POST['kid_rep_edit']) {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->group('zustellreport - kid_rep_edit');
        
        $firePhp->log($_POST, '$_POST');
    }
    
    $klickrate_extern = $_POST['kRateExt'];
    $openingrate_extern = $_POST['oRateExt'];
    $kid_rep_edit = $_POST['kid_rep_edit'];

    $resUpdate = mysql_query(
        'UPDATE `' . $kampagne_db . '` SET `openingrate_extern` = "' . $openingrate_extern . '", `klickrate_extern` = "' . $klickrate_extern . '"' . 
            ' WHERE `k_id` = "' . $kid_rep_edit . '"'
        ,
        $verbindung
    );
    if (!$resUpdate) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: $resUpdate');
        }
    }

    $resInsert = mysql_query(
        'INSERT INTO `' . $log . '`' . 
            ' (`userid`, `action`, `kid`, `nv_id`, `timestamp`)' . 
            ' VALUES ("' . $userID . '", "5", "' . $kid_rep_edit . '", "' . $kid_rep_edit . '", NOW())'
        ,
        $verbindung
    );
    if (!$resInsert) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: $resInsert');
        }
    }
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->groupEnd();
    }
}

if ($_POST['buchhaltungmail_']) {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->group('zustellreport - buchhaltungmail_');
        
        $firePhp->log($_POST, '$_POST');
    }
    
    $kid = $_POST['zustell_kid'];
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($kid, '$kid');
        $firePhp->log($log, '$log');
        $firePhp->log($userID, '$userID');
        $firePhp->log($smtp_host, '$smtp_host');
        $firePhp->log($smtp_user, '$smtp_user');
        $firePhp->log($smtp_pw, '$smtp_pw');
        $firePhp->log($userEmail, '$userEmail');
        $firePhp->log($userVorname, '$userVorname');
        $firePhp->log($userNachname, '$userNachname');
    }
    
    sendBenachrichtigungsMail(
        'b',
        addslashes($_POST['vorlage_zustellreport_mail']),
        $_POST['vorlage_zustellreport_betreff_edit'],
        $kid,
        $log,
        $userID,
        $smtp_host,
        $smtp_user,
        $smtp_pw,
        $userEmail,
        $userVorname,
        $userNachname
    );
    
    if ($firePhp instanceof FirePHP) {
        $firePhp->groupEnd();
    }
}

if ($_POST['vorlage_betreff']) {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->group('zustellreport - vorlage_betreff');
        
        $firePhp->log($_POST, '$_POST');
    }
    
    $v_betreff = utf8_decode($_POST['vorlage_betreff']);
    $v_text = $_POST['vorlage_text'];
    $v_text = str_replace('_x_', '&', $v_text);
    $v_text = str_replace('_p_', '+', $v_text);
    $repTyp = $_POST['repTyp'];

    $buchhaltung = $_POST['buchhaltungmail'];
    if ($buchhaltung == 'b') {
        $kid = $_POST['kid'];
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($buchhaltung, 'buchhaltung');
            $firePhp->log($kid, 'kid');
            $firePhp->log($log, 'log');
            $firePhp->log($userID, 'userID');
            $firePhp->log($smtp_host, 'smtp_host');
            $firePhp->log($smtp_user, 'smtp_user');
            $firePhp->log($smtp_pw, 'smtp_pw');
            $firePhp->log($userEmail, 'userEmail');
            $firePhp->log($userVorname, 'userVorname');
            $firePhp->log($userNachname, 'userNachname');
        }
        
        sendBenachrichtigungsMail(
            $buchhaltung,
            addslashes($_POST['b_mail']),
            $_POST['b_betreff'],
            $kid,
            $log,
            $userID,
            $smtp_host,
            $smtp_user,
            $smtp_pw,
            $userEmail,
            $userVorname,
            $userNachname
        );
    }

    if ($_POST['check'] != 1) {
        include('../../db_pep.inc.php');
        
        $resUpdateMandant = mysql_query(
            'UPDATE `mandant` SET `cm_rep_betreff` = "' . $v_betreff . '", `cm_rep_mail` = "' . mysql_real_escape_string($v_text) . '" WHERE `id` = "' . $mID . '"',
            $verbindung_pep
        );
        if (!$resUpdateMandant) {
            // debug
            if ($firePhp instanceof FirePHP) {
                $firePhp->log(mysql_error($verbindung_pep), 'sql error: resUpdateMandant');
            }
        }
    }

    if ($_POST['email'] != '') {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->group('email vorhanden');
        }
        
        $empfaenger = $_POST['email'];
        $empfaengerArr = explode(',', $empfaenger);
        
        $betreff = $v_betreff;
        $nachricht = $v_text;
        $report = $_POST['report'];

        if ($_POST['check'] != 1) {
            $nachricht = util::fillTestmail($nachricht);
            $betreff = util::fillTestmail($betreff);
        }
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->group('send mail');
            
            $firePhp->log($empfaengerArr, 'empfaengerArr');
            $firePhp->log($userEmail, 'userEmail');
            $firePhp->log($userVorname . ' ' . $userNachname, 'FromName');
            $firePhp->log($betreff, 'betreff');
            $firePhp->log($empfaenger, 'empfaenger');
        }

        $mail = new PHPMailer();
        $mail->IsMail();
        $mail->From = $userEmail;
        $mail->FromName = $userVorname . ' ' . $userNachname;
        $mail->AddAddress($empfaenger);
        $mail->Subject = $betreff;
        $mail->IsHTML(true);
        $mail->Body = $nachricht;
        
        foreach ($empfaengerArr as $receiver) {
            $receiver = trim($receiver);
            if ($receiver != '') {
                $mail->AddAddress($receiver);
            }
            
            if (!$mail->Send()) {
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->log($mail->ErrorInfo, $receiver . ' receiver');
                }
                
                echo 'Die Email konnte nicht gesendet werden';
                echo 'Fehler: ' . $mail->ErrorInfo;
            } else {
                echo 'Die Email wurde versandt.';
            }

            $mail->ClearAddresses();
        }
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->groupEnd();
        }

        if ($report == '1') {
            if ($repTyp == 'rep1') {
                $status_neu = 21;
                $repTxt = 'Erst- / Zwischenreporting';
            }

            if ($repTyp == 'rep2') {
                $status_neu = 22;
                $repTxt = 'Endreporting';
            }

            $kid = $_POST['kid'];
            $resUpdateKampagne = mysql_query(
                'UPDATE `' . $kampagne_db . '` SET `status` = "' . $status_neu . '" WHERE `k_id` = "' . $kid . '"',
                $verbindung
            );
            if (!$resUpdateKampagne) {
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->log(mysql_error($verbindung), 'sql error: resUpdateKampagne');
                }
            }

            $resInsertLog = mysql_query(
                'INSERT INTO `' . $log . '`' . 
                    ' (`userid`, `action`, `kid`, `nv_id`, `timestamp`, `status`)' . 
                    ' VALUES ("' . $userID . '", "4", "' . $kid . '", "' . $kid . '", NOW(), "' . $status_neu . '")'
                ,
                $verbindung
            );
            if (!$resInsertLog) {
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->log(mysql_error($verbindung), 'sql error: resInsertLog');
                }
            }
            
            if (intval($status_neu) === 22) {
                $mandant = $_SESSION['mandant'];
                include('../../db_connect.inc.php');

                $resInsertCmRepTable = mysql_query(
                    'INSERT INTO `' . $cm_rep_table . '`' . 
                        ' (`kid`, `empfaenger`, `absender`, `betreff`, `report`, `typ`)' . 
                        ' VALUES ("' . $kid . '", "' . $empfaenger . '", "' . $userEmail . '", "' . $betreff . '"' . 
                            ', "' . mysql_real_escape_string($nachricht) . '", "' . $repTxt . '")'
                    ,
                    $verbindung
                );
                if (!$resInsertCmRepTable) {
                    // debug
                    if ($firePhp instanceof FirePHP) {
                        $firePhp->log(mysql_error($verbindung), 'sql error: resInsertCmRepTable');
                    }
                }
            }
        }
    }
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->groupEnd();
    }
}
?>