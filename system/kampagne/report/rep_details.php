<?php
header('Content-Type: text/html; charset=ISO-8859-1');

session_start();

// debug
function firePhPDebug() {
    $firePhp = null;
	/*
    if (intval($_SESSION['benutzer_id']) === 24) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/system/library/FirePHPCore/FirePHP.class.php');
        $firePhp = FirePHP::getInstance(true);
    }
	 * 
	 */
    
    return $firePhp;
}

function getHistoryDataArrayByStatusAndKampagneId($userTable, $logTable, $kampagneId, $status, $verbindung) {
    $firePhp = firePhPDebug();
    
    $row = null;
   /* $res = mysql_query(
        'SELECT *' . 
            ' FROM `' . $userTable . '` as `b`,' . 
                ' `' . $logTable . '` as `l`' . 
            ' WHERE `b`.`benutzer_id` = `l`.`userid`' . 
                ' AND `l`.`kid` = "' . $kampagneId . '"' . 
                ' AND `l`.`status` = ' . intval($status) . 
            ' ORDER BY `l`.`timestamp` DESC'
        ,
        $verbindung
    );*/
    $res = mysql_query(
        'SELECT *' . 
            ' FROM `' . $userTable . '` as `b`,' .           
            ' ORDER BY `l`.`timestamp` DESC'
        ,
        $verbindung
    );
    if ($res) {
        $row = mysql_fetch_assoc($res);
        mysql_free_result($res);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($row, '$row');
        }
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: getHistoryDataArrayByStatusAndKampagneId()');
        }
    }
    
    return $row;
}

$mandant = $_SESSION['mandant'];
$mID = $_SESSION['mID'];
$userID = $_SESSION['benutzer_id'];
$kid = $_GET['kid'];

if (!isset($_SESSION['campaign']['reporting'][$kid])) {
	$_SESSION['campaign']['reporting'][$kid] = array(
		'oldStatus' => \intval($_GET['campaign']['oldStatus'])
	);
}

include('../../library/util.php');
include('../../library/log.php');
include('../../library/campaignManagerWebservice.php');
include('../../library/cm_fillVorlage.php');
include('../../db_pep.inc.php');

$firePhp = firePhPDebug();

// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($_SESSION, '$_SESSION');
    $firePhp->log($_GET, '$_GET');
}

$logManager = new logManager();
$tr_empf = $userFreigabe = $userEndreporting = $lastEdit = null;

$resBenutzer = mysql_query(
    'SELECT *' . 
        ' FROM `benutzer`' . 
        ' WHERE `benutzer_id` = "' . $userID . '"'
    ,
    $verbindung_pep
);
if ($resBenutzer) {
    $z_user = mysql_fetch_assoc($resBenutzer);
    mysql_free_result($resBenutzer);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($z_user, 'z_user');
    }
} else {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log(mysql_error($verbindung_pep), 'sql error: $resBenutzer');
    }
}

$resVorlage = mysql_query(
    'SELECT *' . 
        ' FROM `mandant`' . 
        ' WHERE `id` = "' . $mID . '"'
    ,
    $verbindung_pep
);
if ($resVorlage) {
    $z_vorlage = mysql_fetch_assoc($resVorlage);
    mysql_free_result($resVorlage);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($z_vorlage, 'z_vorlage');
    }
    
    $z_vorlage_mail = fillVorlage(
        $z_vorlage['cm_rep_mail'],
        $kid
    );
    $z_vorlage_betreff = fillVorlage(
        $z_vorlage['cm_rep_betreff'],
        $kid
    );
    $z_vorlage_buchhaltungmail = fillVorlage(
        $z_vorlage['cm_rechnung_mail'],
        $kid
    );
    $z_vorlage_buchhaltungmail_betreff = fillVorlage(
        $z_vorlage['cm_rechnung_betreff'],
        $kid
    );
} else {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log(mysql_error($verbindung_pep), 'sql error: resVorlage');
    }
}

$resEmail = mysql_query(
    'SELECT *' . 
        ' FROM `benutzer`' . 
        ' WHERE `mid` = "' . $mID . '"' . 
            ' AND `email` != ""' . 
            ' AND `active` = 1' . 
        ' ORDER BY `vorname` ASC'
    ,
    $verbindung_pep
);
if ($resEmail) {
    $cntIntern = mysql_num_rows($resEmail);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->group('resEmail');
        
        $firePhp->log($cntIntern, '$cntIntern');
    }
    
    while (($z_e = mysql_fetch_assoc($resEmail)) !== false) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($z_e, '$z_e');
        }
        
        if ($z_user['email'] == $z_e['email']) {
            $checked = 'checked="checked"';
        } else {
            $checked = '';
        }
        
        $tr_empf .= '
            <tr>
                <td>
                    <a href="#" title="&lt;' . $z_e['email'] . '&gt; hinzuf&uuml;gen" onclick="addEmpf(\'' . $z_e['email'] . '\');" style="white-space:nowrap" id="rep_email_empf">' 
                        . $z_e['vorname'] . ' ' . $z_e['nachname'] . ' &lt;' . $z_e['email'] . '&gt;
                    </a>
                </td>
            </tr>
        ';
    }
    mysql_free_result($resEmail);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->groupEnd();
    }
} else {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log(mysql_error($verbindung_pep), 'sql error: $resEmail');
    }
}

include('../../db_connect.inc.php');
/*
$rechnungstellungmail_statusArr = $logManager->getLastLogByAction(
    $log,
    6,
    $kid
);
// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($rechnungstellungmail_statusArr, '$rechnungstellungmail_statusArr');
}
$display_remail = null;
if (count($rechnungstellungmail_statusArr) > 0) {
    $display_remail = 'style="display:none"';
}*/

if (!empty($_GET['newStatus'])) {
    $newStatus = $_GET['newStatus'];
    
    $resUpdateKampagne = mysql_query(
        'UPDATE `' . $kampagne_db . '`' . 
            ' SET `status` = "' . $newStatus . '"' . 
            ' WHERE `k_id` = "' . $kid . '"'
        ,
        $verbindung
    );
    if (!$resUpdateKampagne) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: $resUpdateKampagne');
        }
    }
    
   /* $resInsertLog = mysql_query(
        'INSERT INTO `' . $log . '`' . 
            ' (`userid`, `action`, `kid`, `nv_id`, `timestamp`, `status`)' . 
            ' VALUES ("' . $userID . '", 5, "' . $kid . '", "' . $kid . '", NOW(), "' . $newStatus . '")'
        ,
        $verbindung
    );
    if (!$resInsertLog) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: $resInsertLog');
        }
    }*/
}

$sql = mysql_query(
    'SELECT *' . 
        ' FROM `' . $kampagne_db . '`' . 
        ' WHERE `k_id` = "' . $kid . '"'
    ,
    $verbindung
);
if ($sql) {
    $z = mysql_fetch_assoc($sql);
    mysql_free_result($sql);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($z, '$z');
    }
} else {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log(mysql_error($verbindung), 'sql error: $sql');
    }
}

$asp = $z['versandsystem'];
if ($asp == 'BM') {
    $datum = util::bmTime($z['datum']);
} else {
    $datum = $z['datum'];
}

$mimeType = $z['mailtyp'];
if ($mimeType == 't') {
    $tr_openings_display = 'none';
}

$status = $z['status'];
$statusName = util::getCampaignStatusName($status);
$statusName = str_replace('<br />', ' ', $statusName);
if ($status == 23 || $status == 21 || $status == 5 || $status > 30) {
    $display_f = $display_fz = $display_rs = 'none';
} elseif ($status == 24) {
    $display_fe = 'none';
    $display_f = 'none';

    $lzh = getHistoryDataArrayByStatusAndKampagneId(
        $benutzer,
        $log,
        $kid,
        $status,
        $verbindung
    );
    if (is_array($lzh)) {
        $userFreigabe = '<span style="font-size:10px;color:#666666">von ' . $lzh['vorname'] . ' ' . $lzh['nachname'] . ', ' . util::datum_de($lzh['timestamp']) . '</span>';
    }
} elseif ($status == 20) {
    $display_fe = 'none';
    $display_fz = 'none';
    $display_rs = 'none';
} elseif ($status == 22) {
    $display_fe = 'none';
    $display_fz = 'none';
    $display_rs = 'none';
    $display_f = 'none';
    $display_edit = 'none';
    $display_tm = 'none';

    $lzhe = getHistoryDataArrayByStatusAndKampagneId(
        $benutzer,
        $log,
        $kid,
        $status,
        $verbindung
    );
    if (is_array($lzhe)) {
        $userEndreporting = '
            <span style="font-size:11px;color:#333333;font-weight:bold">Endreporting:</span> <span style="font-size:10px;color:#666666">' . 
                util::datum_de($lzhe['timestamp']) . ' Uhr von ' . $lzhe['vorname'] . ' ' . $lzhe['nachname'] . 
            '</span>
        ';
    }
}

switch ($mimeType) {
    case 't':
        $mime = 'Text';
        break;
    
    case 'h':
        $mime = 'Html';
        break;
    
    case 'm':
        $mime = 'Multipart';
        break;
}

$klickrate_extern = $z['klickrate_extern'];
if ($klickrate_extern == 0) {
    $klickrate_extern = '&plusmn;0%';
}

$openingrate_extern = $z['openingrate_extern'];
if ($openingrate_extern == 0) {
    $openingrate_extern = '&plusmn;0%';
}

// blablub
$lz = getHistoryDataArrayByStatusAndKampagneId(
    $benutzer,
    $log,
    $kid,
    '5',
    $verbindung
);
if (is_array($lz)) {
    $lastEdit = '<span style="font-size:10px;color:#666666">zuletzt bearbeitet von ' . $lz['vorname'] . ' ' . $lz['nachname'] . ', ' . util::datum_de($lz['timestamp']) . ' Uhr</span>';
}
$cnt_history = count($lz);  // TODO: entfernen

$cmMweb = new campaignManagerWebservice();
$ApDataArr = $cmMweb->getAllAP(
    $kunde_ap,
    $z['agentur_id']
);
$cntAps = count($ApDataArr);
// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($ApDataArr, '$ApDataArr');
    $firePhp->log($cntAps, '$cntAps');
}

$tr_empf_main = '
    <tr style="background-color:#F2F2F2;color:#9900FF;font-weight:bold">
        <td>' . $z['agentur'] . ' (' . $cntAps . ')</td>
    </tr>
';
if ($cntAps > 0) {
    for ($i = 0; $i < $cntAps; $i++) {
        $tr_empf_main .= '
            <tr>
                <td>
                    <a href="#" title="&lt;' . $ApDataArr[$i]['email'] . '&gt; hinzuf&uuml;gen" onclick="addEmpf(\'' . $ApDataArr[$i]['email'] . '\');"' . 
                            ' style="white-space:nowrap" id="rep_email_empf">' . 
                        $ApDataArr[$i]['titel'] . ' ' . $ApDataArr[$i]['vorname'] . ' ' . $ApDataArr[$i]['nachname'] . ' &lt;' . $ApDataArr[$i]['email'] . '&gt;
                    </a>
                </td>
            </tr>
        ';
    }
} else {
    $tr_empf_main .= '
        <tr>
            <td style="color:#999999">-- keine Kontakte vorhanden --</td>
        </tr>
    ';
}

$tr_empf_main .= '
    <tr style="background-color:#F2F2F2;color:#9900FF;font-weight:bold">
        <td>Intern (' . $cntIntern . ')</td>
    </tr>' . 
    $tr_empf
;

$kontaktDataArr = $cmMweb->getAP(
    $kunde_ap,
    $z['ap_id']
);
// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($kontaktDataArr, '$kontaktDataArr');
}

$resAll = mysql_query(
    'SELECT SUM(`versendet`) as `v`, SUM(`openings_all`) as `o`, SUM(`klicks_all`) as `k`' . 
        ' FROM `' . $kampagne_db . '`' . 
        ' WHERE `nv_id` = "' . $kid . '"'
    ,
    $verbindung
);
if ($resAll) {
    $za = mysql_fetch_assoc($resAll);
    mysql_free_result($resAll);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($za, '$za');
    }
} else {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log(mysql_error($verbindung), 'sql error: $resAll');
    }
}

$oRate = util::getQuote(
    $za['v'],
    $za['o']
);
$kRate = util::getQuote(
    $za['v'],
    $za['k']
);
$o_new = $za['o'] + ($za['o'] * $openingrate_extern) / 100;
$oRate_ext = util::getQuote(
    $z['gebucht'],
    $o_new
);

if ($openingrate_extern > 0) {
    $openingrate_extern = '<span style="color:green">+' . $openingrate_extern . '%</span>';
} elseif ($openingrate_extern < 0) {
    $openingrate_extern = '<span style="color:red">' . $openingrate_extern . '%</span>';
}

$k_new = $za['k'] + ($za['k'] * $klickrate_extern) / 100;
$kRate_ext = util::getQuote(
    $z['gebucht'],
    $k_new
);

if ($klickrate_extern > 0) {
    $klickrate_extern = '<span style="color:green">+' . $klickrate_extern . '%</span>';
} elseif ($klickrate_extern < 0) {
    $klickrate_extern = '<span style="color:red">' . $klickrate_extern . '%</span>';
}

$kRate_rel = util::getQuote(
    $za['o'],
    $za['k']
);
$kRate_rel_ext = util::getQuote(
    $o_new,
    $k_new
);

// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($oRate, 'oRate');
    $firePhp->log($kRate, 'kRate');
    $firePhp->log($o_new, 'o_new');
    $firePhp->log($oRate_ext, 'oRate_ext');
    $firePhp->log($k_new, 'k_new');
    $firePhp->log($kRate_ext, 'kRate_ext');
    $firePhp->log($kRate_rel, 'kRate_rel');
    $firePhp->log($kRate_rel_ext, 'kRate_rel_ext');
}
?>
<div id="details_header_div" style="height:150px;background-image:url(img/rep_bg.png);background-repeat:repeat-x;background-position:100% 100%;border-bottom:1px solid #CCCCCC;width:100%;">
    <table cellpadding="0" cellspacing="0" border="0" style="margin-top:10px;">
        <tr>
            <td style="padding-left:35px;" valign="top"><img src="img/Oxygen/48/mimetypes/office-spreadsheet.png" /></td>
            <td valign="top" style="padding-left:5px;text-align: left;">
                <span class="label_big"><?php echo $z['k_name']; ?></span><br />
                <span class="label_sub1"><?php echo $z['agentur']; ?></span><br />
                <span class="label_sub2">
                    <?php
                        echo util::datum_de(
                            $datum,
                            ''
                        );
                    ?> Uhr
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="2" height="15"></td>
        </tr>
        <tr>
            <td></td>
            <td style="padding-left:5px;color:#666666;text-align: left;">Status: <?php echo $statusName . '&nbsp;&nbsp;' . $userFreigabe; ?></td>
        </tr>
        <tr>
            <td colspan="2" height="20"></td>
        </tr>
        <tr>
            <td></td>
            <td style="padding-left:5px;color:#666666;white-space:nowrap;text-align: left;">
                <input style="display:<?php echo $display_rs; ?>;" class="positive" type="button" id="report_senden" onClick="loadReport('<?php echo $kontaktDataArr['email']; ?>');" value="Report erstellen &amp; senden" />
                <input style="display:none;" type="button" id="report_senden_now" class="positive" onClick="sendReport();" value="Report senden" />
                <input type="button" style="cursor:pointer;display:<?php echo $display_edit; ?>" id="report_bearbeiten" onClick="showrepEdit();" value="Bearbeiten" />
                <input style="display:<?php echo $display_tm; ?>;cursor:pointer" onclick="loadTestmail();" type="button" id="testmail_" value="Testmail" />
                <input style="display:none;cursor:pointer" type="button" onclick="sendTestmail();" id="testmail_senden" value="Testmail senden" />
                <input style="display:none;cursor:pointer" type="button" onclick="rep_back(<?php echo $kid; ?>);" id="rep_back" value="zur&uuml;ck zum Reporting" />
                <input style="display:<?php echo $display_fe; ?>;" class="positive" type="button" id="freigabe_erbitten" onClick="setNewStatus(<?php echo $kid; ?>,20,'<?php echo util::getStatusName(20); ?>');" value="Freigabe erbitten" />
                <input style="display:<?php echo $display_f; ?>;" class="positive" type="button" id="freigabe_erteilen" onClick="setNewStatus(<?php echo $kid; ?>,24,'<?php echo util::getStatusName(24); ?>');" value="Freigabe erteilen" />
                <input style="display:<?php echo $display_fz; ?>;" class="negative" type="button" id="freigabe_zurueck" onClick="setNewStatus(<?php echo $kid; ?>,23,'<?php echo util::getStatusName(23); ?>');" value="Freigabe zur&uuml;cknehmen" />
                <?php echo $userEndreporting; ?>
            </td>
        </tr>
    </table>
</div>
<div id="report_c">
    <div style="float:right;padding:10px;"><?php echo $lastEdit; ?></div>
    <br />
    <div style="padding:25px;">
        <fieldset class="fieldset_default" style="margin-top:-15px">
            <legend class="legend_default">Allgemeine Versandinformationen</legend>
            <table>
                <tr>
                    <td class="fieldset_label">Kampagne:</td>
                    <td class="fieldset_data" style="text-align: left;"><?php echo $z['k_name']; ?></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Agentur:</td>
                    <td class="fieldset_data" style="text-align: left;"><?php echo $z['agentur']; ?></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Ansprechpartner:</td>
                    <td class="fieldset_data" style="text-align: left;"><?php echo $z['ap']; ?></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Versanddatum:</td>
                    <td class="fieldset_data" style="text-align: left;">
                        <?php
                            echo util::datum_de(
                                $datum,
                                ''
                            );
                        ?> Uhr
                    </td>
                </tr>
                <tr>
                    <td class="fieldset_label">Gebucht:</td>
                    <td class="fieldset_data" style="text-align: left;"><?php echo util::zahl_format($z['gebucht']); ?></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Versendet:</td>
                    <td class="fieldset_data" style="text-align: left;"><?php echo util::zahl_format($za['v']); ?></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Selektion:</td>
                    <td class="fieldset_data" style="text-align: left;"><?php echo $z['zielgruppe']; ?></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Abrechnungsart:</td>
                    <td class="fieldset_data" style="text-align: left;"><?php echo $z['abrechnungsart']; ?></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Preis:</td>
                    <td class="fieldset_data" style="text-align: left;"><?php echo $z['preis']; ?> Euro</td>
                </tr>
                <tr>
                    <td class="fieldset_label">Betreff:</td>
                    <td class="fieldset_data" style="text-align: left;"><?php echo $z['betreff']; ?></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Absendername:</td>
                    <td class="fieldset_data" style="text-align: left;"><?php echo $z['absendername']; ?></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Mailformat:</td>
                    <td class="fieldset_data" style="text-align: left;"><?php echo $mime; ?></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Versandsystem:</td>
                    <td class="fieldset_data" style="text-align: left;"><?php echo $z['versandsystem']; ?></td>
                </tr>
            </table>
        </fieldset>
        <fieldset class="fieldset_default">
            <legend class="legend_default">Performancezahlen Gesamt</legend>
            <table class="performance_table">
                <tr class="performance_tr">
                    <td></td>
                    <td>Intern</td>
                    <td class="rep_zahlen_ext" style="border-top:1px solid #999999;">Extern</td>
                    <td>Anp.</td>
                    <td>Vorgabe</td>
                </tr>
                <tr>
                    <td class="fieldset_label">Empf&auml;nger</td>
                    <td><?php echo util::zahl_format($za['v']); ?></td>
                    <td class="rep_zahlen_ext"><?php echo util::zahl_format($z['gebucht']); ?></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="fieldset_label">&Ouml;ffnungen</td>
                    <td><?php echo util::zahl_format($za['o']); ?></td>
                    <td class="rep_zahlen_ext" style="color:#9900FF"><?php echo util::zahl_format($o_new); ?></td>
                    <td><?php echo $openingrate_extern; ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="fieldset_label">&Ouml;ffnungsrate</td>
                    <td><?php echo $oRate; ?>%</td>
                    <td class="rep_zahlen_ext" style="color:#9900FF"><?php echo $oRate_ext; ?>%</td>
                    <td></td>
                    <td><?php echo $z['vorgabe_o']; ?>%</td>
                </tr>
                <tr>
                    <td class="fieldset_label">Klicks</td>
                    <td><?php echo util::zahl_format($za['k']); ?></td>
                    <td class="rep_zahlen_ext" style="color:#9900FF"><?php echo util::zahl_format($k_new); ?></td>
                    <td><?php echo $klickrate_extern; ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Klickrate</td>
                    <td><?php echo $kRate; ?>%</td>
                    <td class="rep_zahlen_ext" style="color:#9900FF"><?php echo $kRate_ext; ?>%</td>
                    <td></td>
                    <td><?php echo $z['vorgabe_k']; ?>%</td>
                </tr>
                <tr>
                    <td class="fieldset_label">Klickrate rel.</td>
                    <td><?php echo $kRate_rel; ?>%</td>
                    <td class="rep_zahlen_ext" style="color:#9900FF;border-bottom:1px solid #999999;"><?php echo $kRate_rel_ext; ?>%</td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </fieldset>
    </div>
</div>
<div id="vorlage_content" style="display:none;">
    <div style="width:97%;background-color:#DFECFF;padding:10px;">
        <table class="performance_table">
            <tr id="report_tr" style="display:none">
                <td class="performance">Typ:</td>
                <td colspan="2" class="performance" style="text-align:left">
                    <select name="rep_typ" id="rep_typ">
                        <option value="rep1">Erstreporting</option>
                        <option value="rep2">Endreporting</option>
                    </select>
                </td>
            </tr>
            <tr <?php echo $display_remail; ?>>
                <td class="performance"></td>
                <td colspan="2" style="text-align:left;height:25px">
                    <input type="checkbox" name="buchhaltungmail" id="buchhaltungmail" value="b" /> Rechnungsstellungsmail an Buchhaltung <input type="button" onclick="b_mail_edit()" id="b_mail" value="Bearbeiten" style="font-size:9px;font-weight:normal" />
                    <textarea id="vorlage_buchhaltungmail" name="vorlage_buchhaltungmail" style="display:none"><?php echo $z_vorlage_buchhaltungmail; ?></textarea>
                    <input type="text" id="vorlage_buchhaltungmail_betreff" name="vorlage_buchhaltungmail_betreff" style="display:none" value="<?php echo $z_vorlage_buchhaltungmail_betreff; ?>" />
                </td>
            </tr>
            <tr>
                <td colspan="3" style="height:5px;"></td>
            </tr>
            <tr>
                <td class="performance" style="vertical-align:top">An:</td>
                <td class="performance" style="text-align:left;">
                    <textarea type="text" id="testEmail" name="testEmail" style="width:400px;padding:3px;overflow:hidden;height:15px;" value="<?php echo $z_user['email']; ?>"><?php echo $z_user['email']; ?></textarea>
                </td>
                <td style="vertical-align:top;text-align:left;float:left"><input type="button" id="rep_empf" onclick="menu_anz('empf_liste');return false;" style="padding:2px" value="mehr" /></td>
            </tr>
            <tr id="empf_liste" style="display:none">
                <td></td>
                <td colspan="2" style="text-align:left">
                    <div style="height:100px;overflow:auto;width:400px;background-color:#FFFFFF;padding:2px;margin-left:1px;border:1px solid #CCCCCC; margin-top:-1px;">
                        <table class="empf_liste" cellpadding="0" cellspacing="0" border="0">
                            <?php echo $tr_empf_main; ?>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="height:5px;"></td>
            </tr>
            <tr>
                <td class="performance">Betreff:</td>
                <td colspan="2" class="performance" style="text-align:left">
                    <input type="text" style="width:450px;padding:3px;" id="vorlage_betreff" value="<?php echo $z_vorlage_betreff; ?>" />
                </td>
            </tr>
            <tr>
                <td colspan="3" style="height:5px;"></td>
            </tr>
            <tr>
                <td class="performance">Von:</td>
                <td colspan="2" class="performance" style="text-align:left;color:#333333;"><?php echo $z_user['email']; ?></td>
            </tr>
        </table>
    </div>
    <textarea id="vorlage_text1" name="vorlage_text1" style="width:100%;height:400px;"><?php echo $z_vorlage_mail; ?></textarea>
</div>
<input type="hidden" id="Report" value="0" />
<input type="hidden" id="Report_kid" value="<?php echo $kid; ?>" />
<input type="hidden" id="reportCampaignOldStatus" name="campaign[oldStatus]" value="<?php echo \intval($_SESSION['campaign']['reporting'][$kid]['oldStatus']); ?>" />

<script type="text/javascript">
    hi_t = document.documentElement.clientHeight;
    hi_t = hi_t-500;
    document.getElementById('vorlage_text1').style.height = hi_t + 'px';

    if(CKEDITOR.instances['vorlage_text1']) {
        CKEDITOR.remove(CKEDITOR.instances['vorlage_text1']);
    }

    CKEDITOR.replace( 'vorlage_text1', {
        height: hi_t,
        toolbarStartupExpanded: false,
        toolbar : [
            ['Source','-','Save','NewPage','Preview'],
            ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
            ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
            '/',
            ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
            ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
            ['Link','Unlink','Anchor'],
            ['Image','Flash','Table','HorizontalRule'],
            '/',
            ['Styles','Format','Font','FontSize'],
            ['TextColor','BGColor']
        ]
    });

    function addEmpf(e) {
        var s = document.getElementById('testEmail').value;
        if (s != '') {
            s = s + ',';
        }
	
        e = e + ',';
        var vorhanden = s.search(e);
        if (vorhanden != -1){
            i = i-1;
        } else {
            s = s + e;
            i = i+1;
        }
	
        document.getElementById('testEmail').value = s;
        s2 = document.getElementById('testEmail').value;
        s2 = s2.substring(0,s2.length-1);
        document.getElementById('testEmail').value = s2;
        if (s2.length > 60) {
            document.getElementById('testEmail').style.height = '30px';
        }
	
        if (s2.length > 120) {
            document.getElementById('testEmail').style.height = '45px';
        } 
	
        if (s2.length > 180) {
            document.getElementById('testEmail').style.height = '60px';
        }
	
        if (s2.length > 240) {
            document.getElementById('testEmail').style.height = '100px';
        }
	
        if (s2.length <= 60) {
            document.getElementById('testEmail').style.height = '15px';
        }
    }

    function loadTestmail() {
        document.getElementById('report_bearbeiten').style.display = 'none';
        document.getElementById('testmail_').style.display = 'none';
        document.getElementById('freigabe_erbitten').style.display = 'none';
        document.getElementById('freigabe_erteilen').style.display = 'none';
        document.getElementById('report_senden').style.display = 'none';
        document.getElementById('freigabe_zurueck').style.display = 'none';
        document.getElementById('testmail_senden').style.display = '';
        document.getElementById('rep_back').style.display = '';
        document.getElementById('report_c').style.display = 'none';
        document.getElementById('vorlage_content').style.display = '';
        document.getElementById('Report').value = '0';
    }

    function loadReport(k) {
        document.getElementById('report_bearbeiten').style.display = 'none';
        document.getElementById('testmail_').style.display = 'none';
        document.getElementById('freigabe_erbitten').style.display = 'none';
        document.getElementById('freigabe_erteilen').style.display = 'none';
        document.getElementById('report_senden').style.display = 'none';
        document.getElementById('freigabe_zurueck').style.display = 'none';
        document.getElementById('testmail_senden').style.display = 'none';
        document.getElementById('rep_back').style.display = '';
        document.getElementById('report_c').style.display = 'none';
        document.getElementById('vorlage_content').style.display = '';
        document.getElementById('report_tr').style.display = '';
        document.getElementById('Report').value = '1';
        document.getElementById('testEmail').value = k;
        document.getElementById('report_senden_now').style.display = '';
    }

    function showrepEdit() {
        setRequest_default(
            'kampagne/report/rep_edit.php?kid=<?php echo $kid; ?>',
            'rep_edit'
        );
        YAHOO.example.container.container_rep_editieren.show();
    }

    function setNewStatus(kid,status,stName) {
		var color = '#7D960B';
        if (status < 24) {
            color = '#FF9900';
        }
		
        document.getElementById(kid + '_st').innerHTML = '<span style="color:' + color + '">' + stName + '</span>';
        setRequest_default(
            'kampagne/report/rep_details.php?kid=' + kid 
				+ '&newStatus=' + status
			,
            'rep_details_content'
        );
    }

    function rep_back(kid) {
        setRequest_default(
            'kampagne/report/rep_details.php?kid=' + kid,
            'rep_details_content'
        );
    }

    function sendTestmail() {
        t_email = document.getElementById('testEmail').value;
        if (t_email != '') {
            b = document.getElementById('vorlage_betreff').value;
            t = CKEDITOR.instances.vorlage_text1.getData();
            setRequest_vorlage(1);
        } else {
            alert('Email fehlt');
        }
    }

    function sendReport() {
        t_email = document.getElementById('testEmail').value;
        if (t_email != '') {
            b = document.getElementById('vorlage_betreff').value;
            t = CKEDITOR.instances.vorlage_text1.getData();
            setRequest_vorlage(1);
        } else {
            alert('Email fehlt');
        }
    }

    function b_mail_edit() {
        data = document.getElementById('vorlage_buchhaltungmail').value;
        betreff = document.getElementById('vorlage_buchhaltungmail_betreff').value;
        document.getElementById('vorlage_buchhaltungmail_betreff_edit').value = betreff;
        CKEDITOR.instances.vorlage_buchhaltungmail_edit.setData(data)
        YAHOO.example.container.container_rechnungsstellung.show();
    }
</script>