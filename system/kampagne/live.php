<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();

include('../library/util.php');
include('../library/mandant.class.php');
$mc = new mandant();

function zahl_format($zahl) {
    $zahl = number_format($zahl, 0, 0, '.');
    
    return $zahl;
}

$superview = $_SESSION['zugang'];
$zugang_teile = explode(',', $superview);

$heute = date('Y-m-d');
include('../bm_soap/broadmail_rpc.php');

foreach ($zugang_teile as $zugang_mandant) {
    $mandant = trim($zugang_mandant);
    
    include('../db_connect.inc.php');
    
    $mail_id_daten = mysql_query(
        'SELECT *' 
            . ' FROM ' . $kampagne_db 
            . ' WHERE versandsystem = "BM"' 
                . ' AND mail_id != ""' 
                . ' AND datum LIKE "' . $heute . '%"' 
            . ' ORDER BY datum DESC'
        ,
        $verbindung
    );

    include('../bm_soap/bm_' . $mandant . '.php');
    $factory = new BroadmailRpcFactory($bmMID, $bmUser, $bmPass);
    if ($factory->getError()) {
        die('<h2>Broadmail: Fehler beim Login</h2><code>' . $factory->getError() . '</code>');
    }

    $MailingWebservice = $factory->newMailingWebservice();
    $MailingReportingWebservice = $factory->newMailingReportingWebservice();

    if (mysql_num_rows($mail_id_daten) > 0) {
        while (($zeile_hv = mysql_fetch_assoc($mail_id_daten)) !== false) {
            $m_id = $zeile_hv['mail_id'];
            $k_id = $zeile_hv['k_id'];
            $nv_id = $zeile_hv['nv_id'];
            $k_name = $zeile_hv['k_name'];
            $gebucht = $zeile_hv['gebucht'];
            $versandsystem = $zeile_hv['versandsystem'];
            $datum_versand = $zeile_hv['datum'];
            $status = $zeile_hv['status'];
            $start_datum = $zeile_hv['gestartet'];
            $end_datum = $zeile_hv['beendet'];
            $versendet_ = $zeile_hv['versendet'];
            $from_ = $zeile_hv['absendername'];
            $betreff_ = $zeile_hv['betreff'];
            $openings = $zeile_hv['openings'];
            $openings_all = $zeile_hv['openings_all'];
            $klicks = $zeile_hv['klicks'];
            $klicks_all = $zeile_hv['klicks_all'];

            $mail_id_daten_hv = mysql_query(
                'SELECT agentur,k_name' 
                    . ' FROM ' . $kampagne_db 
                    . ' WHERE k_id = ' . intval($nv_id)
                ,
                $verbindung
            );
            $zeile_hv2 = mysql_fetch_assoc($mail_id_daten_hv);
            $agentur = $zeile_hv2['agentur'];
            $k_name_hv = $zeile_hv2['k_name'];

            switch ($status) {
                case 0:
                    $status_bm_img = 'NEU';
                    break;
                
                case 1:
                    $status_bm_img = 'Werbemittel';
                    break;
                
                case 2:
                    $status_bm_img = 'Optimiert';
                    break;
                
                case 3:
                    $status_bm_img = 'Testmail';
                    break;
                
                case 4:
                    $status_bm_img = 'Freigabe';
                    break;
            }

            $status_bm = $MailingWebservice->getStatus($m_id);
            switch ($status_bm) {
                case 'DONE':
                    $status_bm_img = '<img src="../img/icons/accept.png" />';
                    break;
                
                case 'SENDING':
                    $status_bm_img = 'sende...';
                    break;
                
                case 'CANCELED':
                    $status_bm_img = 'abgebrochen';
                    break;
            }
            
            if (strpos($start, 'T')) {
                $date_ = explode('T', $start);
                $datum = $date_[0];
                $zeit = substr($date_[1], 0, 5);
                $date_teile = explode('-', $date_[0]);
                $zeit_teile = explode(':', $zeit);
                $newTime = mktime($zeit_teile[0], $zeit_teile[1], 0, $date_teile[1], $date_teile[2], $date_teile[0]);
                $z = date('I', $newTime);

                if ($z == 0) {
                    $newTime += 3600;
                } else {
                    $newTime += 7200;
                }

                $start_neu = date('Y-m-d H:i:s', $newTime);
            } else {
                $start_neu = $datum_versand;
            }
            $e_teile = '';

            if (strpos($ende, 'T')) {
                $date_ = explode('T', $ende);
                $datum = $date_[0];
                $zeit = substr($date_[1], 0, 5);
                $date_teile = explode('-', $date_[0]);
                $zeit_teile = explode(':', $zeit);
                $newTime = mktime($zeit_teile[0], $zeit_teile[1], 0, $date_teile[1], $date_teile[2], $date_teile[0]);
                $z = date('I', $newTime);

                if ($z == 0) {
                    $newTime += 3600;
                } else {
                    $newTime += 7200;
                }

                $ende_neu = date('Y-m-d H:i:s', $newTime);
                $e_datum = util::bmTime($ende_neu);
                $e_datum = util::datum_de($e_datum);
                $e_teile = explode(' - ', $e_datum);
            } else {
                $ende_neu = '';
            }
            
            $versendet = $MailingWebservice->getSentRecipientCount($m_id);

            if (!$_SESSION['$mid_done']) {
                $mid_done_sess = '';
            } else {
                $mid_done_sess = $_SESSION['$mid_done'];
            }

            if ($status_bm == 'DONE') {
                $mid_done .= "[" . $mandant . $m_id . "]";
                if (strpos($mid_done_sess, $m_id) === false) {
                    $alert .= $mandant . ": " . $agentur . ", " . $k_name . " wurde versendet\\n";
                }
            }
            
            switch ($mandant) {
                case 'pepperos':
                case 'pepperosInternational':
                    $mandantTitle = 'trafficrunner';
                    break;
                
                default:
                    $mandantTitle = $mandant;
                    break;
            }
            
            $data .= '
                <tr>
                    <td align="center"><img src="../img/logo/' . $mandant . '.gif" height="15" alt="versendet" title="' . $mandantTitle . '" /></td>
                    <td align="center">' . $status_bm_img . '</td>
                    <td>' . $agentur . ', ' . utf8_encode($k_name) . '</td>
                    <td align="right">' . zahl_format($versendet) . '</td>
                </tr>
            ';
        }
    } else {
        $data .= '
            <tr>
                <td align="center">
                    <img src="../img/logo/' . $mandant . '.gif" height="15" />
                </td>
                <td colspan="7" style="border-right:1px #999999 solid; color:#999999" align="center">Keine Versendungen</td>
            </tr>
        ';
    }
};

if ($alert != '') {
    print '<script language="javascript" type="text/javascript">alert("' . utf8_encode($alert) . '");</script>';
}

$data .= '
    <tr>
        <td height="35" align="left" colspan="10" style="font-size:10px;color:#666666;border:0px;">
            <img src="../img/icons/accept.png" height="10" /> = versendet
        </td>
    </tr>
';

$_SESSION['$mid_done'] = $mid_done;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta http-equiv="Refresh" content="30; url=/system/kampagne/live.php" />
        
        <title>MaaS - Live Monitoring</title>
        
        <style type="text/css">
            body {font-family:Arial, Helvetica, sans-serif; font-size:11px;}
            table {font-family:Arial, Helvetica, sans-serif; font-size:11px; width:100%}
            table td {border-right:1px #999999 solid; border-bottom:1px #999999 solid; border-collapse:collapse;white-space: nowrap;}
        </style>
        
        <script type="text/javascript">
            <!--
            var secs;
            var timerID = null;
            var timerRunning = false;
            var delay = 1000;
            
            function InitializeTimer() {
                secs = 31;
                
                StartTheTimer();
            }
            
            function StopTheClock() {
                if (timerRunning) {
                    clearTimeout(timerID);
                }
                
                timerRunning = false
            }
            
            function StartTheTimer() {
                if (secs == 1) {
                    StopTheClock();
                } else {
                    self.status = secs;
                    secs = secs - 1;
                    timerRunning = true;
                    timerID = self.setTimeout("StartTheTimer()", delay);
                    document.getElementById('sek').innerHTML = secs;
                }
            }
            //-->
        </script>
    </head>
    <body onLoad="InitializeTimer()">
        <table cellpadding="5" cellspacing="0">
            <tr>
                <td colspan="14" style="border-right:0px; color:#666666">
                    <strong><i>Live Monitoring (Heute)</i></strong> <span style="font-size:9px">- Diese Seite aktualisiert sich automatisch in <span id='sek'>31</span>sek</span>
                </td>
            </tr>
            <tr style="background-color:#E0E7FC; font-weight:bold;">
                <td>Mandant</td>
                <td>Status</td>
                <td width="230">Kampagne</td>
                <td>Versendet</td>
            </tr>
            <?php print $data; ?>
        </table>
    </body>
</html>