<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();
$mandant = $_SESSION['mandant'];
$mID = $_SESSION['mID'];
$userID = $_SESSION['benutzer_id'];
$id = $_GET['id'];
include('../../limport/db_connect.inc.php');
include('../../library/util.php');
include('../../library/dataManagerWebservice.php');

$dmMweb = new dataManagerWebservice();
$kundeDataArr = $dmMweb->getKundeData(	$partner_db,
										$felderPartnerDB['PID'],
										$felderPartnerDB['Pkz'],
										$felderPartnerDB['GenArt'],
										$felderPartnerDB['Firma'],
										$felderPartnerDB['Website'],
										$felderPartnerDB['Strasse'],
										$felderPartnerDB['PLZ'],
										$felderPartnerDB['Ort'],
										$felderPartnerDB['Geschaeftsfuehrer'],
										$felderPartnerDB['Registergericht'],
										$felderPartnerDB['Telefon'],
										$felderPartnerDB['Fax'],
										$felderPartnerDB['Bedingung'],
										$felderPartnerDB['Rep_email'],
										$felderPartnerDB['Status'],
										$id
									 );


if ($kundeDataArr['status'] == 1) {
	$status = "<span style='color:green'>Aktiv</span>";
} else {
	$status = "<span style='color:red'>Inaktiv</span>";
}

$APDataArr = $dmMweb->getAllAP($partner_ap,$id);
$cnt = count($APDataArr);

if ($cnt > 0) {
	
	$tr_ap .= '<tr class="performance_tr">
					<td>Email</td>
                	<td>Anrede</td>
                	<td>Vorname</td>
                	<td>Nachname</td>
					<td>Telefon</td>
					<td></td>
                </tr>';
				
	for ($i = 0; $i < $cnt; $i++) {
		$tr_ap .= "<tr class='performance_tr2'>";
		$tr_ap .= "<td>".$APDataArr[$i]['email']."</td>";
		$tr_ap .= "<td>".$APDataArr[$i]['anrede']."</td>";
		$tr_ap .= "<td>".$APDataArr[$i]['titel']." ".$APDataArr[$i]['vorname']."</td>";
		$tr_ap .= "<td>".$APDataArr[$i]['nachname']."</td>";
		$tr_ap .= "<td>".$APDataArr[$i]['telefon']." <span style='color:#666666;cursor:pointer' onclick='menu_anz(\"ap".$APDataArr[$i]['ap_id']."\");'>[+]</span> <div id='ap".$APDataArr[$i]['ap_id']."' style='display:none'>Fax: ".$APDataArr[$i]['fax']."<br />Mobil:".$APDataArr[$i]['mobil']."</div></td>";
		$tr_ap .= "<td>&nbsp;&nbsp;&nbsp;<input type='image' onclick='showKontaktData(".$id.",".$APDataArr[$i]['ap_id'].");' src='img/Tango/16/apps/utilities-text-editor.png' title='bearbeiten' />&nbsp;&nbsp;<input type='image' onclick='delKontakt(".$APDataArr[$i]['ap_id'].");' src='img/Tango/16/actions/dialog-close.png' title='l�schen' /></td>";
		$tr_ap .= "</tr>";
	}
	
} 

?>
<input type="hidden" name="dm_kunde_id_def" id="dm_kunde_id_def" value="<?php print $id; ?>" />
<input type="hidden" name="dm_ap_id_def" id="dm_ap_id_def" value="" />
<div id="details_header_div" style="height:150px;background-image:url(img/rep_bg.png);background-repeat:repeat-x;background-position:100% 100%;
;border-bottom:1px solid #CCCCCC;width:100%;">
	<table cellpadding="0" cellspacing="0" border="0" style="margin-top:10px;">
    	<tr>
        	<td style="padding-left:35px;" valign="top"><img src="img/Tango/48/mimetypes/office-contact.png" /></td>
            <td valign="top" style="padding-left:5px;">
            	<span class="label_big"><?php print $kundeDataArr['firma']; ?></span><br />
                <span class="label_sub1"><?php print $kundeDataArr['genart']; ?></span><br />
				<span class="label_sub2"><a href="http://<?php print $kundeDataArr['website']; ?>" target="_blank" style="color:#333333">http://<?php print $kundeDataArr['website']; ?></a><br />
            </td>
        </tr>
        <tr>
        	<td colspan="2" height="15"></td>
        </tr>
         <tr>
        	<td></td>
            <td style="padding-left:5px;color:#666666;">Status: <?php print $status; ?></td>
        </tr>
        <tr>
        	<td colspan="2" height="20"></td>
        </tr>
        <tr>
        	<td></td>
            <td style="padding-left:5px;color:#666666;white-space:nowrap;">
				<input type="button" id="button_kunde_edit" onclick="showKundeData(<?php print $id; ?>);" style="cursor:pointer;" value="Bearbeiten" />
				<input type="button" class="negative" onclick="delKunde();" value="L&ouml;schen" />
            </td>
        </tr>
    </table>
</div>

<div id="report_c">
<br />
<div style="padding:25px;background-color:#FFFFFF">
<fieldset class="fieldset_default" style="margin-top:-15px">
	<legend class="legend_default">Allgemeine Informationen</legend>
        	<table>
				<tr>
                    <td class="fieldset_label">ID:</td>
                    <td class="fieldset_data"><?php print $kundeDataArr['id']; ?></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Firma:</td>
                    <td class="fieldset_data"><?php print $kundeDataArr['firma']; ?></td>
                </tr>
				<tr>
                    <td class="fieldset_label">K&uuml;rzel:</td>
                    <td class="fieldset_data"><?php print $kundeDataArr['firma_short']; ?></td>
                </tr>
                 <tr>
                    <td class="fieldset_label">Adresse:</td>
                    <td class="fieldset_data"><?php print $kundeDataArr['strasse']."<br />".$kundeDataArr['plz']." ".$kundeDataArr['ort']; ?></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Telefon:</td>
                    <td class="fieldset_data"><?php print $kundeDataArr['telefon'];; ?></td>
                </tr>
                 <tr>
                    <td class="fieldset_label">Fax:</td>
                    <td class="fieldset_data"><?php print $kundeDataArr['fax']; ?></td>
                </tr>
                 <tr>
                    <td class="fieldset_label">Webseite:</td>
                    <td class="fieldset_data"><a href="http://<?php print $kundeDataArr['website']; ?>" target="_blank">http://<?php print $kundeDataArr['website']; ?></a></td>
                </tr>
				<tr>
                    <td class="fieldset_label">Email:</td>
                    <td class="fieldset_data"><?php print $kundeDataArr['email']; ?></td>
                </tr>
                 <tr>
                    <td class="fieldset_label">Gesch&auml;ftsf&uuml;hrer:</td>
                    <td class="fieldset_data"><?php print $kundeDataArr['geschaeftsfuehrer']; ?></td>
                </tr>
                <tr>
                    <td class="fieldset_label">Registergericht:</td>
                    <td class="fieldset_data"><?php print $kundeDataArr['registergericht']; ?></td>
                </tr>
            </table>
</fieldset>

<fieldset class="fieldset_default">
	<legend class="legend_default">Kontakte</legend>
        	<table>
            	<tr><td colspan='7'><input type='image' src='img/Oxygen/32/actions/contact-new.png' onclick='showKontaktData(<?php print $id; ?>,"");' title='Neuen Kontakt anlegen' /></td></tr>
				<tr><td colspan='7'></td></tr>
				<?php print $tr_ap; ?>
			</table>
</fieldset>
</div>
</div>

<script type="text/javascript">
function showKundeData(id) {
	setRequest_default("limport/kunde/details_dialog.php?id="+id,"dm_kunde_content");
	YAHOO.example.container.container_dm_kunde.show();
}

function showKontaktData(kunde_id,id) {
	setRequest_default("limport/kunde/details_ap_dialog.php?kunde_id="+kunde_id+"&id="+id,"dm_kunde_ap_content");
	YAHOO.example.container.container_dm_kunde_ap.show();
}

function delKontakt(id) {
	document.getElementById('dm_ap_id_def').value = id;
	YAHOO.example.container.container_dm_del_kunde_ap.show();
}

function delKunde(id) {
	YAHOO.example.container.container_dm_del_kunde.show();
}

</script>
