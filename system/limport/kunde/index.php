<?php
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();

$mandant = $_SESSION['mandant'];

include('../../db_connect.inc.php');
?>
<div id="basic_layout" style="overflow:hidden;border:0px;">
    <div id="basic" style="overflow:hidden;background-color:#F2F2F2"></div>
    <div id="basic_content" style="background-color:#FFFFFF;height:100%">
        <div style="padding:25px">
            <span style="font-size:16px;">Lieferantenverwaltung</span><br /><br />
            Bitte w&auml;hlen Sie einen Lieferanten aus, um diesen zu bearbeiten.
        </div>
    </div>
</div>
<script type="text/javascript">
    var myContextMenu = false;

    var layoutb = new YAHOO.widget.Layout('basic_layout', {
        height: Dom.getClientHeight()-119, //Height of the viewport
        width: Dom.get('basic_layout').offsetWidth, //Width of the outer element
        minHeight: 150, //So it doesn't get too small
        units: [
            {position: 'left', width: 302, body: 'basic', scroll:false, gutter: '-1 0 -1 -1'},
            {position: 'center', width: (Dom.get('basic_layout').offsetWidth-368), body: 'basic_content', gutter: '0 -1 0 0'}
        ]
    });
    
    layoutb.render();
    
    //Handle the resizing of the window
    YAHOO.example.Basic = function() {
        var myColumnDefs = [
            {key:"id", width: 25, sortable: false, resizeable: false},
            {key:"firma_genart", label:"Firma", resizeable: false, sortable: false, width: 275}
        ];
        
        var myDataSource = new YAHOO.util.DataSource("limport/kunde/kunde_json.php?");
        myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSON;
        myDataSource.connXhrMode = "queueRequests";
        myDataSource.responseSchema = {
            resultsList: "Result",
            fields: ["id", "firma_genart"]
        };
        
        var myDataTable = new YAHOO.widget.ScrollingDataTable("basic", myColumnDefs, myDataSource, {
                height: (Dom.getClientHeight()-144)+"px",
                width: "301px",
                selectionMode:"single"
            }
        );
        
        // Subscribe to events for row selection
        myDataTable.subscribe("rowMouseoverEvent", myDataTable.onEventHighlightRow);
        myDataTable.subscribe("rowMouseoutEvent", myDataTable.onEventUnhighlightRow);
        myDataTable.subscribe("rowClickEvent", myDataTable.onEventSelectRow);
        myDataTable.subscribe("rowSelectEvent", function() {
            var myDataTableData = this.getRecordSet().getRecord(this.getSelectedRows()[0])._oData;
            setRequest_default("limport/kunde/details.php?id="+myDataTableData.id,"basic_content");
        });
        
        layoutb.on('resize', function() {
            height2 = Dom.getClientHeight()-119;
            width2 = Dom.getClientWidth()-207;
            
            YAHOO.util.Dom.setStyle('basic_layout', 'width', width2 + 'px');
            YAHOO.util.Dom.setStyle('basic_layout', 'height', height2 + 'px');
            YAHOO.util.Dom.setStyle('basic_content', 'width', (width2-327) + 'px');
            YAHOO.util.Dom.setStyle('basic_content', 'height', height2 + 'px');
            
            myDataTable.set('height', ((height2-25) + 'px'));
        });
        
        return {
            oDS: myDataSource,
            oDT: myDataTable
        };
    }();
    
    Event.on(window, 'resize', layoutb.resize, layoutb, true);
    
    function Resize_() {}
    window.onresize = Resize_;
</script>