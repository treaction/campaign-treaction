<?php
header('Content-Type: text/xml; charset=ISO-8859-1');
include('check_login.inc.php');
$_SESSION['stats_id'] = '';

if (isset($_GET['import_jahr'])) {
    $import_jahr = $_GET['import_jahr'];
    $_SESSION['import_jahr'] = $import_jahr;
} else {
    $import_jahr = date("Y");
}

if (isset($_SESSION['import_jahr'])) {
    $import_jahr = $_SESSION['import_jahr'];
}




if (isset($_POST['email'])) {
    $emailAbfrage = trim($_POST['email']);
}
if (file_exists("test_export.txt")) {
    unlink('test_export.txt');
}

$entferne = false;
$importe = "";

if ($step == "success") {
    $info = "<div style='width:450px;'><h4 align='center' style='color:#0066FF; border: 1px #0066FF solid; padding:3px;'>
											Datei erfolgreich importiert</h4>
										</div>";
}

if (isset($_GET['entfernt'])) {
    $info = "<div style='width:350px;'><h4 align='center' style='color:#0066FF; border: 1px #0066FF solid; padding:3px;'>
											Datens&auml;tze wurden entfernt</h4>
										</div>";
}


if ($entferne == 1) {
    $import_nr = $_GET['import_nr'];
    $import_datei_name_del = mysql_query("DELETE FROM $import_datei_db WHERE IMPORT_NR='$import_nr'");
    $import_del = mysql_query("DELETE FROM $mandant WHERE IMPORT_NR='$import_nr'");
    $import_del_confirm = "<p style='color:red'>Importdaten wurden aus der Meta-DB entfernt.</p>";
}

function suche_offene($p, $mo, $j) {

    global $mandant;
    include('db_connect.inc.php');


    $imp_daten_offen = mysql_query("SELECT *
							  FROM $import_datei_db
							  WHERE PARTNER='$p'
							  AND YEAR(GELIEFERT_DATUM) = '$j'
							  AND MONTH(GELIEFERT_DATUM) = '$mo'
							  AND IMPORT_NR=IMPORT_NR2
							  AND STATUS<3
								", $verbindung);

    $offen_anzahl = mysql_num_rows($imp_daten_offen);

    if ($offen_anzahl > 0) {
        $offen = "<span style='color:red'>" . $offen_anzahl . " offen</span>";
        return $offen;
    } else {
        return "green";
    }
}

function importe_anzahl($p, $mo, $j) {

    global $mandant;
    include('db_connect.inc.php');

    $imp_daten = mysql_query("SELECT *
							  FROM $import_datei_db
							  WHERE PARTNER='$p'
							  AND YEAR(GELIEFERT_DATUM) = '$j'
							  AND MONTH(GELIEFERT_DATUM) = '$mo'
							  AND IMPORT_NR=IMPORT_NR2
								", $verbindung);

    $importe_anzahl = mysql_num_rows($imp_daten);

    if ($importe_anzahl > 0) {
        $imp_anzahl = "<span style='color:#999999;' title='" . $importe_anzahl . " Datei(en)'>(" . $importe_anzahl . ")</span>";
        return $imp_anzahl;
    }
}

function details_footer() {
    global $mandant;
    include('db_connect.inc.php');
    global $import_jahr;
    global $details_brutto;
    global $details_netto;
    global $offen_anzahl;
    global $dateien_anzahl;

    $sql = mysql_query("SELECT SUM(BRUTTO) as B, SUM(NETTO) as N
				 FROM $import_datei_db
				 WHERE YEAR(GELIEFERT_DATUM) = '$import_jahr'
				 ", $verbindung);
    $z = mysql_fetch_array($sql);
    $details_brutto = $z['B'];
    $details_netto = $z['N'];

    $sql2 = mysql_query("SELECT *
				 FROM $import_datei_db
				 WHERE YEAR(GELIEFERT_DATUM) = '$import_jahr'
				 AND IMPORT_NR = IMPORT_NR2
				 ", $verbindung);
    $dateien_anzahl = mysql_num_rows($sql2);

    $sql3 = mysql_query("SELECT *
				 FROM $import_datei_db
				 WHERE YEAR(GELIEFERT_DATUM) = '$import_jahr'
				 AND IMPORT_NR = IMPORT_NR2
				 AND STATUS < 3
				 ", $verbindung);
    $offen_anzahl = mysql_num_rows($sql3);
}

function importe_raussuchen() {
    global $mandant;
    include('db_connect.inc.php');
    global $import_jahr;
    global $import_partner_sql;
    $i = 1;

    $imp_ges = mysql_query("SELECT p.Pkz, i.PARTNER, MONTH(GELIEFERT_DATUM) as monat, SUM(BRUTTO) as BRUTTO_GES, SUM(NETTO) as NETTO_GES
				 FROM $import_datei_db as i, $partner_db as p
				 WHERE YEAR(i.GELIEFERT_DATUM) = '$import_jahr'
				 AND i.PARTNER=p.PID
				 GROUP BY i.PARTNER, monat
				 ORDER BY monat DESC,
				 p.Pkz ASC", $verbindung);

    while ($igz = mysql_fetch_array($imp_ges)) {

        $monat_ges = $igz['monat'];

        switch ($monat_ges) {
            case 1: $monat_ = "Januar";
                break;
            case 2: $monat_ = "Februar";
                break;
            case 3: $monat_ = "M�rz";
                break;
            case 4: $monat_ = "April";
                break;
            case 5: $monat_ = "Mai";
                break;
            case 6: $monat_ = "Juni";
                break;
            case 7: $monat_ = "Juli";
                break;
            case 8: $monat_ = "August";
                break;
            case 9: $monat_ = "September";
                break;
            case 10: $monat_ = "Oktober";
                break;
            case 11: $monat_ = "November";
                break;
            case 12: $monat_ = "Dezember";
                break;
        }

        $pid_ges = $igz['PARTNER'];
        $b_ges = $igz['BRUTTO_GES'];
        $n_ges = $igz['NETTO_GES'];
        $pkz_ges = $igz['Pkz'];

        $timestamp2 = $timestamp;
        $timestamp = $monat_ges;

        $pid_display2 = $pid_display;
        $pid_display = $pid_ges;

        if ($i == 2) {
            $bg = "#E4E4E4";
        } else {
            $bg = "#FFFFFF";
            $i = 1;
        };

        $my = $monat_;

        if ($timestamp != $timestamp2) {

            $imp_zahlen_monat_query = mysql_query("SELECT SUM(BRUTTO) as b, SUM(NETTO) as n FROM $import_datei_db WHERE YEAR(GELIEFERT_DATUM) = '$import_jahr' AND MONTH(GELIEFERT_DATUM) = '$monat_ges'", $verbindung);

            $izmz = mysql_fetch_array($imp_zahlen_monat_query);

            $q_ = round(($izmz['n'] / $izmz['b']) * 100, 1);

            $importe .= '<tr><td colspan="50"></td></tr><tr style="background-color:#41557B;font-size:12px;color:#FFFFFF; border-bottom:2px solid #8e8901; font-weight:bold; background-image:url(img/treaction-gruen.png);background-repeat:repeat-x">
				<td colspan="4" valign="bottom"> ' . $my . '</td>
				<td align="right">' . zahl_formatieren($izmz['b']) . '</td>
				<td align="right">' . zahl_formatieren($izmz['n']) . '</td>
				<td align="right"><i>' . $q_ . '%</i></td>
				<td></td>
</tr>';
        }



        $offen = suche_offene($pid_ges, $monat_ges, $import_jahr);
        if ($offen == 'green') {
            $st_farbe = "green";
            $offen = '';
        } else {
            $st_farbe = "red";
        }

        $q_ges = round(($n_ges / $b_ges) * 100, 1);

        $hideArr[] = array("pid" => $pid_ges, "mon" => $monat_ges);

        $importe .= "<tr onclick=\"select_datei(event,'" . $pid_ges . "','" . $monat_ges . "','" . $import_jahr . "');\" ondblclick=\"show('" . $pid_ges . "','" . $monat_ges . "');\" class='container2' bgcolor='" . $bg . "' onMouseOver=\"this.bgColor='#bfdaff'\" onMouseOut=\"this.bgColor='" . $bg . "'\" style='cursor:pointer'>
4								<td>" . $offen . "</td>
								<td colspan='3' style='color:#800080;font-weight:bold'><img onclick=\"show('" . $pid_ges . "','" . $monat_ges . "');\"  id='img" . $pid_ges . $monat_ges . "' src='img/arrow_down.png' width='12' />&nbsp;&nbsp;" . $pkz_ges . " " . importe_anzahl($pid_ges, $monat_ges, $import_jahr) . "</td>
								<td align='right'>" . zahl_formatieren($b_ges) . "</td>
								<td align='right'>" . zahl_formatieren($n_ges) . "</td>
								<td align='right'><i>" . $q_ges . "%</i></td>
								<td></td>
						     </tr>";




        $imp_daten = mysql_query("SELECT *
							  FROM $import_datei_db as i, $partner_db as p
							  WHERE i.PARTNER=p.PID
							  AND p.PID='$pid_ges'
							  AND YEAR(i.GELIEFERT_DATUM) = '$import_jahr'
							  AND MONTH(i.GELIEFERT_DATUM) = '$monat_ges'
							  AND i.IMPORT_NR=i.IMPORT_NR2
							  ORDER BY i.GELIEFERT_DATUM desc
							", $verbindung);




        $m = 1;
        $feld = 1;

        while ($imp_zeile = mysql_fetch_array($imp_daten)) {


            $imp_nr = $imp_zeile['IMPORT_NR'];

            $imp_anz_sql = mysql_query("SELECT * FROM $import_datei_db WHERE IMPORT_NR2='$imp_nr'", $verbindung);
            $imp_anz = mysql_num_rows($imp_anz_sql);


            $imp_nr2 = $imp_zeile['IMPORT_NR2'];

            $imp_partner = $imp_zeile['Pkz'];
            $imp_partner_id = $imp_zeile['PARTNER'];
            $imp_datum = $imp_zeile['IMPORT_DATUM'];
            $imp_datum_teile = explode(" ", $imp_datum);
            $imp_datum_teile_zeit = explode(":", $imp_datum_teile[1]);
            $imp_datum_zeit = $imp_datum_teile_zeit[0] . ":" . $imp_datum_teile_zeit[1];
            $imp_datum_teile2 = explode("-", $imp_datum_teile[0]);
            $imp_datum_de = $imp_datum_teile2[2] . "." . $imp_datum_teile2[1] . "." . $imp_datum_teile2[0] . " " . $imp_datum_zeit;

            $geliefert_datum = $imp_zeile['GELIEFERT_DATUM'];
            $geliefert_teile = explode("-", $geliefert_datum);
            $geliefert_datum_de = $geliefert_teile[2] . "." . $geliefert_teile[1] . "." . $geliefert_teile[0];

            $imp_datei = $imp_zeile['IMPORT_DATEI'];
            $imp_intervall = $imp_zeile['INTERVALL'];
            if ($imp_intervall == 'w') {
                $imp_intervall = 'w�chentlich';
            }
            if ($imp_intervall == 'm') {
                $imp_intervall = 'monatlich';
            }

            $imp_bedingung = $imp_zeile['BEDINGUNG'];
            $imp_anzahl = $imp_zeile['BRUTTO'];
            $imp_netto = $imp_zeile['NETTO'];
            $imp_netto_ext = $imp_zeile['NETTO_EXT'];
            $imp_metadb = $imp_zeile['METADB'];
            $imp_rep_info = $imp_zeile['REPORTING_INFO'];
            $imp_rep_notiz = $imp_zeile['NOTIZ'];
            $imp_langadr = $imp_zeile['LANGADR'];
            $imp_bounces = $imp_zeile['BOUNCES'];
            $imp_blacklist = $imp_zeile['BLACKLIST'];
            $imp_dbl = $imp_zeile['DUBLETTEN'];
            $imp_fake = $imp_zeile['FAKE'];
            $imp_dbl_indatei = $imp_zeile['DUBLETTE_INDATEI'];
            $imp_hinweis = $imp_zeile['HINWEIS'];
            if ($imp_hinweis != '') {
                $hinweis = "";
                $h = unserialize($imp_hinweis);

                $hinweis_text = "<tr><td class=\'details\' valign=\'top\'>Hinweise:</td><td class=\'details2\'>";
                foreach ($h as $h_part) {
                    $hinweis_text .= "";
                }
                $hinweis_text .= "</td></tr>";
            } else {
                $hinweis = "";
                $hinweis_text = "";
            }

            $imp_opt_datum = $imp_zeile['INOPTDB_DATUM'];
            $opt_datum_teile = explode("-", $imp_opt_datum);
            $opt_datum_de = $opt_datum_teile[2] . "." . $opt_datum_teile[1] . "." . $opt_datum_teile[0];

            $ap_anrede = $imp_zeile['Rep_anrede'];
            $ap = $imp_zeile['Rep_ap'];
            $ap_email = $imp_zeile['Rep_email'];
            $abschlag = $imp_zeile['Abschlag'];

            if ($imp_metadb > 0) {
                $q_imp_metadb = round(($imp_metadb / $imp_anzahl) * 100, 1);
                $imp_meta_exp = "&nbsp;&nbsp;<a href=\'limport/export_dbl.php?a=m&impnr=" . $imp_nr . "\'><img src=\'img/icons/folder_next.png\' border=\'0\' width=\'15\' title=\'exportieren\' />";
            } else {
                $q_imp_metadb = 0;
                $imp_meta_exp = "";
            }

            if ($imp_dbl > 0) {
                $q_imp_dbl = round(($imp_dbl / $imp_anzahl) * 100, 1);
                $imp_dbl_exp = "&nbsp;&nbsp;<a href=\'limport/export_dbl.php?a=d&impnr=" . $imp_nr . "\'><img src=\'img/icons/folder_next.png\' border=\'0\' width=\'15\' title=\'exportieren\' />";
            } else {
                $q_imp_dbl = 0;
                $imp_dbl_exp = "";
            }

            if ($imp_dbl_indatei > 0) {
                $q_imp_dbl_indatei = round(($imp_dbl_indatei / $imp_anzahl) * 100, 1);
                $imp_dbl_indatei_exp = "&nbsp;&nbsp;<a href=\'limport/export_dbl.php?a=i&impnr=" . $imp_nr . "\'><img src=\'img/icons/folder_next.png\' border=\'0\' width=\'15\' title=\'exportieren\' />";
            } else {
                $q_imp_dbl_indatei = 0;
                $imp_dbl_indatei_exp = "";
            }

            if ($imp_blacklist > 0) {
                $q_imp_blacklist = round(($imp_blacklist / $imp_anzahl) * 100, 1);
                $imp_blacklist_exp = "&nbsp;&nbsp;<a href=\'limport/export_dbl.php?a=b&impnr=" . $imp_nr . "\'><img src=\'img/icons/folder_next.png\' border=\'0\' width=\'15\' title=\'exportieren\' />";
            } else {
                $q_imp_blacklist = 0;
                $imp_blacklist_exp = "";
            }

            if ($imp_bounces > 0) {
                $q_imp_bounces = round(($imp_bounces / $imp_anzahl) * 100, 1);
                $imp_bounces_exp = "&nbsp;&nbsp;<a href=\'limport/export_dbl.php?a=h&impnr=" . $imp_nr . "\'><img src=\'img/icons/folder_next.png\' border=\'0\' width=\'15\' title=\'exportieren\' />";
            } else {
                $q_imp_bounces = 0;
                $imp_bounces_exp = "";
            }


            if ($imp_fake > 0) {
                $q_imp_fake = round(($imp_fake / $imp_anzahl) * 100, 1);
                $imp_fake_exp = "&nbsp;&nbsp;<a href=\'limport/export_dbl.php?a=f&impnr=" . $imp_nr . "\'><img src=\'img/icons/folder_next.png\' border=\'0\' width=\'15\' title=\'exportieren\' />";
            } else {
                $q_imp_fake = 0;
                $imp_fake_exp = "";
            }

            if ($imp_dbl_indatei > 0) {
                $q_dbl_indatei = round(($imp_dbl_indatei / $imp_anzahl) * 100, 1);
            } else {
                $q_bl_indatei = 0;
            }
            if ($imp_langadr > 0) {
                $q_langadr = round(($imp_langadr / $imp_anzahl) * 100, 1);
            } else {
                $q_langadr = 0;
            }
            $q_netto_zw = ($imp_dbl / $imp_anzahl) * 100;
            $q_netto = round(($imp_netto / $imp_anzahl) * 100, 1);

            $imp_mehrfach = $imp_zeile['EMAIL_MEHRFACH'];
            $brutto_neu = $imp_anzahl - $imp_mehrfach;
            $imp_inoptdb = $imp_zeile['INOPTDB'];
            $netto = $brutto_neu - $imp_inoptdb;
            $q_blacklist = round(($imp_blacklist / $imp_anzahl) * 100, 1);
            $q_bounces = round(($imp_bounces / $imp_anzahl) * 100, 1);

            $in_bm = $imp_zeile['BROADMAIL'];
            $in_bm_datum = $imp_zeile['BROADMAIL_DATUM'];
            $in_bm_datum_teile = explode("-", $in_bm_datum);
            $in_bm_datum_de = $in_bm_datum_teile[2] . "." . $in_bm_datum_teile[1] . "." . $in_bm_datum_teile[0];


            $voll_name = $imp_zeile['VOLL_NAME'];
            if ($voll_name > 0) {
                $q_voll_name = round(($voll_name / $imp_anzahl) * 100, 1);
            } else {
                $voll_name = 0;
            }

            $land_de = $imp_zeile['LAND_DE'];
            if ($land_de > 0) {
                $q_land_de = round(($land_de / $imp_anzahl) * 100, 1);
            } else {
                $land_de = 0;
            }

            $land_at = $imp_zeile['LAND_AT'];
            if ($land_at > 0) {
                $q_land_at = round(($land_at / $imp_anzahl) * 100, 1);
            } else {
                $land_at = 0;
            }

            $land_ch = $imp_zeile['LAND_CH'];
            if ($land_ch > 0) {
                $q_land_ch = round(($land_ch / $imp_anzahl) * 100, 1);
            } else {
                $land_ch = 0;
            }


            $anr_null = 0;
            $anr_ar = $imp_zeile['ANR_AR'];
            $anr_vn = $imp_zeile['ANR_VN'];
            $anr_nn = $imp_zeile['ANR_NN'];
            $anr_str = $imp_zeile['ANR_STR'];
            $anr_plz = $imp_zeile['ANR_PLZ'];
            $anr_ort = $imp_zeile['ANR_ORT'];
            $anr_land = $imp_zeile['ANR_LAND'];
            $anr_land_de = $imp_zeile['ANR_DE'];
            $anr_land_at = $imp_zeile['ANR_AT'];
            $anr_land_ch = $imp_zeile['ANR_CH'];
            $anr_tel = $imp_zeile['ANR_TEL'];
            $anr_bkz = $imp_zeile['ANR_BKZ'];
            $anr_geb = $imp_zeile['ANR_GEB'];
            if ($anr_ar == 0 && $anr_vn == 0 && $anr_nn == 0 && $anr_str == 0 && $anr_plz == 0 && $anr_ort == 0 && $anr_land_de == 0 && $anr_tel == 0 && $anr_bkz == 0 && $anr_geb == 0) {
                $anr_null = 1;
            }

            $status = $imp_zeile['STATUS'];
            $check = $imp_zeile['CHECK'];

            #if ($imp_zeile['RE']!='') {$status = 4;}

            switch ($status) {
                case 0: $status_bez = "Neu";
                    $statusfarbe = "red";
                    break;
                case 1: $status_bez = "Neu";
                    $statusfarbe = "red";
                    break;
                case 2: $status_bez = "Reportfertig";
                    $statusfarbe = "#FF9900";
                    break;
                case 3: $status_bez = "Reporting versendet";
                    $statusfarbe = "green";
                    break;
                case 4: $status_bez = "Rechnung erhalten";
                    $statusfarbe = "#8000ff";
                    break;
                case 5: $status_bez = "Bezahlt";
                    $statusfarbe = "silver";
                    break;
            }




            if ($check == '') {
                $check = '<img src="img/icons/accept.png" width="16" />';
            }


            if ($mandant == 'intone') {
                $sql = 'SUM(INOPTDB) AS op,';
            } else {
                $sql = "";
            }
            if ($imp_anz > 1) {
                $imp_main_sql = mysql_query("SELECT SUM(BRUTTO) AS b,
								SUM(NETTO) AS n,
								SUM(LANGADR) AS l,
								SUM(BOUNCES) AS bc,
								SUM(BLACKLIST) AS bl,
								SUM(DUBLETTEN) AS du,
								SUM(EMAIL_MEHRFACH) AS em,
								" . $sql . "
								SUM(VOLL_NAME) AS vn,
								SUM(LAND_DE) AS de,
								SUM(LAND_AT) AS at,
								SUM(LAND_CH) AS ch,
								SUM(ANR_AR) AS aar,
								SUM(ANR_VN) AS avn,
								SUM(ANR_NN) AS ann,
								SUM(ANR_STR) AS ast,
								SUM(ANR_PLZ) AS apl,
								SUM(ANR_ORT) AS aor,
								SUM(ANR_DE) AS ade,
								SUM(ANR_AT) AS aat,
								SUM(ANR_CH) AS ach,
								SUM(ANR_TEL) AS ate,
								SUM(ANR_BKZ) AS abk,
								SUM(ANR_GEB) AS age
						 FROM $import_datei_db WHERE IMPORT_NR2='$imp_nr'", $verbindung);

                $imp_main_zeile = mysql_fetch_array($imp_main_sql);
                $brutto_main = $imp_main_zeile['b'];
                $netto_main = $imp_main_zeile['n'];





                $importe_main = "<tr onclick=\"doIt_adressen(event,'','" . $imp_nr . "','" . $imp_partner_id . "','S','" . $imp_partner . "','" . $geliefert_datum_de . "','" . $brutto_main . "','" . $netto_main . "','" . $imp_netto_ext . "','" . $imp_rep_info . "','" . $imp_rep_notiz . "','" . $ap_anrede . "','" . $ap . "','" . $ap_email . "','" . $status . "','" . $abschlag . "','" . $monat_ges . "','" . $import_jahr . "');\" class='container2' bgcolor='" . $bg . "' onMouseOver=\"this.bgColor='#bfdaff'\" onMouseOut=\"this.bgColor='" . $bg . "'\" id='" . $pid_ges . $monat_ges . $feld . "' style='cursor:pointer'>
4								<td align='right'><span style='color:" . $statusfarbe . "; font-size:12px;'>&#9632;</span></td>
								<td> " . $status_bez . "</td>
								<td align='center'>" . $geliefert_datum . "</td>
								<td>[Gesamt] " . $imp_datei . "</td>
								<td align='right'>" . zahl_formatieren($brutto_main) . "</td>
								<td align='right'>" . zahl_formatieren($netto_main) . "</td>
								<td align='right'><i>" . $q_netto . "%</i></td>
								<td></td>
						     </tr>";
                $feld++;
                $imp_add_daten = mysql_query("SELECT *
							  FROM $import_datei_db
							  WHERE $import_datei_db.IMPORT_NR2='$imp_nr'
							", $verbindung);
                while ($imp_add_zeile = mysql_fetch_array($imp_add_daten)) {

                    $imp_add_nr = $imp_add_zeile['IMPORT_NR'];
                    $imp_add_anzahl = $imp_add_zeile['BRUTTO'];
                    $imp_add_netto = $imp_add_zeile['NETTO'];
                    $imp_add_datei = $imp_add_zeile['IMPORT_DATEI'];
                    $imp_opt_add_datum = $imp_add_zeile['INOPTDB_DATUM'];

                    if ($mandant == 'intone' && $imp_opt_add_datum == "0000-00-00" && $imp_add_netto > 0) {
                        $import_optdb = "<button onclick='optDB(" . $imp_add_nr . "," . $imp_add_anzahl . "," . $imp_partner_id . ");'>=> OptDB</button>";
                    } else {
                        $import_optdb = '';
                    }

                    $importe_main .= "<tr style='color:#333333;cursor:pointer' class='container2' onclick=\"doIt_adressen(event,'','" . $imp_add_nr . "','" . $imp_partner_id . "','T');\" bgcolor='" . $bg . "' onMouseOver=\"this.bgColor='#bfdaff'\" onMouseOut=\"this.bgColor='" . $bg . "'\" id='" . $pid_ges . $monat_ges . $feld . "'>
4					 	<td colspan='3'>&nbsp;</td>
						<td>" . $imp_add_datei . " " . $import_optdb . "</td>
						<td align='right'>" . zahl_formatieren($imp_add_anzahl) . "</td>
						<td align='right'>" . zahl_formatieren($imp_add_netto) . "</td>
						<td align='right'>&nbsp;</td>
						<td></td>
						</tr>";
                    $feld++;
                }
            } else {
                $importe_main = "";
            }


            if ($imp_anz > 1) {
                $imp_partner = "";
                $geliefert_datum = "";
                $imp_intervall = "";
                $imp_bedingung = "";
                $importe .= $importe_main;
            } else {

                if ($mandant == 'intone' && $imp_opt_datum == "0000-00-00" && $imp_netto > 0 && $_SESSION['testimport'] == 0) {
                    $import_optdb2 = "<button onclick='optDB(" . $imp_nr . "," . $imp_anzahl . "," . $imp_partner_id . ");'>=> OptDB</button>";
                } else {
                    $import_optdb2 = '';
                }



                $importe .= "<tr onclick=\"doIt_adressen(event,'','" . $imp_nr . "','" . $imp_partner_id . "','H','" . $imp_partner . "','" . $geliefert_datum_de . "','" . $imp_anzahl . "','" . $imp_netto . "','" . $imp_netto_ext . "','" . $imp_rep_info . "','" . $imp_rep_notiz . "','" . $ap_anrede . "','" . $ap . "','" . $ap_email . "','" . $status . "','" . $abschlag . "','" . $monat_ges . "','" . $import_jahr . "');\" class='container2' bgcolor='" . $bg . "' onMouseOver=\"this.bgColor='#bfdaff'\" onMouseOut=\"this.bgColor='" . $bg . "'\" id='" . $pid_ges . $monat_ges . $feld . "' style='cursor:pointer'>
4							<td align='right'><span style='color:" . $statusfarbe . "; font-size:12px;'>&#9632;</span></td>
							<td> " . $status_bez . "</td>
							<td align='center'>" . $geliefert_datum . "</td>
							<td>" . $imp_datei . " " . $import_optdb2 . "</td>
							<td align='right'>" . zahl_formatieren($imp_anzahl) . "</td>
							<td align='right'>" . zahl_formatieren($imp_netto) . "</td>
							<td align='right'><i>" . $q_netto . "%</i></td>
							<td></td>
						</tr>";
                $m++;
                $feld++;
            }
        }
        $i++;
    }
    return $importe;
}

$rep_daten = mysql_query("SELECT SUM(BRUTTO) AS brutto, SUM(NETTO) AS netto, SUM(DUBLETTEN) AS dubletten, SUM(LANGADR) AS langadr, SUM(BOUNCES) AS bounces
						  FROM $import_datei_db
						  WHERE YEAR(IMPORT_DATUM) = '$import_jahr'
							", $verbindung);
$rep_zeile = mysql_fetch_array($rep_daten);
$rep_brutto = $rep_zeile['brutto'];
$rep_netto = $rep_zeile['netto'];
$rep_dubletten = $rep_zeile['dubletten'];
$rep_langadr = $rep_zeile['langadr'];
$rep_bounces = $rep_zeile['bounces'];
$rep_brutto_formatiert = number_format($rep_brutto, 0, 0, '.');
$q_meta_db = round(($rep_dubletten / $rep_brutto) * 100, 1);
$q_bounces = round(($rep_bounces / $rep_brutto) * 100, 1);

$rep_daten_all = mysql_query("SELECT SUM(BRUTTO) AS brutto, SUM(NETTO) AS netto, SUM(DUBLETTEN) AS dubletten, SUM(LANGADR) AS langadr, SUM(BOUNCES) AS bounces
						  	  FROM $import_datei_db
							 ", $verbindung);
$rep_zeile_all = mysql_fetch_array($rep_daten_all);
$rep_brutto_all = $rep_zeile_all['brutto'];
$rep_netto_all = $rep_zeile_all['netto'];
$rep_dubletten_all = $rep_zeile_all['dubletten'];
$rep_langadr_all = $rep_zeile_all['langadr'];
$rep_bounces_all = $rep_zeile_all['bounces'];
$rep_brutto_all_formatiert = number_format($rep_brutto_all, 0, 0, '.');
$q_meta_db_all = round(($rep_dubletten_all / $rep_brutto_all) * 100, 1);
$q_bounces_all = round(($rep_bounces_all / $rep_brutto_all) * 100, 1);


if (isset($import_del_confirm)) {
    print $import_del_confirm;
}

details_footer();
$q_all = round(($details_netto / $details_brutto) * 100, 1);
?>
<div id="fakeContainer">
    <table cellspacing="0" cellpadding="0" border="0" id="scrollTable2" bordercolor="#CCCCCC" style="border-collapse:collapse;">
        <tr class="tab_bg">
            <th>Status</th>
            <th>Partner</th>
            <th align="center">Lieferdatum</th>
            <th>Dateiname</th>
            <th align="right">Brutto</th>
            <th align="right">Netto</th>
            <th align="right">Quote</th>
            <th style="width:100%"></th>
        </tr>
        <tr>
            <td colspan="50" style="background-color:#FFFFFF;font-size:16px;font-weight:bold;color:#8e8901"><?php print $import_jahr; ?></td>
        </tr>
<?php print $offen_tr; ?>
<?php print importe_raussuchen(); ?>
    </table>
</div>

<div style="background-color:#8e8901;color:#FFFFFF;font-size:11px;height:23px;width:100%;">
    <div style="margin-left:5px;padding-top:5px;float:left;">
<?php
if ($dateien_anzahl > 0) {
    print zahl_formatieren($dateien_anzahl) . " Datei(en)";
} else {
    print "Keine Dateien vorhanden &nbsp;&nbsp;&nbsp;";
}
?>
    </div>
    <div style="float:left;margin-top:1px;">
        <img src='img/treaction_gruen_seperator.png' />
    </div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
<?php
print "Brutto: " . zahl_formatieren($details_brutto);
?>
    </div>
    <div style="float:left;margin-top:1px;">
        <img src='img/treaction_gruen_seperator.png' />
    </div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
<?php
print "Netto: " . zahl_formatieren($details_netto);
?>
    </div>
    <div style="float:left;margin-top:1px;">
        <img src='img/treaction_gruen_seperator.png' />
    </div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
<?php
print "Quote: " . $q_all . "%";
?>
    </div>
    <div style="float:left;margin-top:1px;">
        <img src='img/treaction_gruen_seperator.png' />
    </div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
<?php
print zahl_formatieren($offen_anzahl) . " offene Reportings1";
?>
    </div>
</div>



<script type="text/javascript">
    wi = document.documentElement.clientWidth;
    wi = wi-205;

    hi = document.documentElement.clientHeight;
    hi = hi-142;

    document.getElementById("fakeContainer").style.width = wi+"px";
    document.getElementById("fakeContainer").style.height = hi+"px";

    function Resize() {
        setRequest('limport/index.php','a');
    }

    window.onresize = Resize;

    var mySt = new superTable("scrollTable2", {
        cssSkin : "sSky",
        fixedCols : 0,
        headerRows : 2
    });

    function optDB(impNr,impAnzahl,impPID) {
        var url = "https://www.campaign-treaction.de/system/limport/action.php?import_nr="+impNr+"&anzahl_brutto="+impAnzahl+"&partner_id="+impPID+"&sql=1&step=sponsoring";
        window.open(url,'','width=500,height=350,scrollbars=yes');
    }
</script>