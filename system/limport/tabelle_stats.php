<?php
session_start();
$mandant = $_SESSION['mandant'];
include('db_connect.inc.php');


$monat = $_REQUEST['import_monat'];
$jahr = $_REQUEST['import_jahr'];
$_SESSION['mo'] = $monat;
$_SESSION['ja'] = $jahr;

function zahl_formatieren($zahl) {
	$zahl = number_format($zahl,0,0,'.');
	return $zahl;
}


$orig_daten_brutto_query = mysql_query("SELECT DISTINCT(PARTNER)
										 FROM $import_datei_db
										 WHERE MONTH(GELIEFERT_DATUM)='$monat'
										 AND YEAR(GELIEFERT_DATUM)='$jahr'",$verbindung);
$anzahl = mysql_num_rows($orig_daten_brutto_query);


	if ($anzahl>0) {
	
		while ($oz = mysql_fetch_array($orig_daten_brutto_query)) {
			$pids[] = $oz['PARTNER'];
		}
		
		
		foreach ($pids as $pid) {
			require_once('db_connect.inc.php');
			$partner_sql = mysql_query("SELECT * FROM $partner_db WHERE PID='$pid'", $verbindung);
			$pz = mysql_fetch_array($partner_sql);
			$partner = $pz['Pkz'];
			
			$orig_daten_brutto_query2 = mysql_query("SELECT  
													 	SUM($import_datei_db.BRUTTO) as b,
													 	SUM($import_datei_db.NETTO) as n,
														SUM($import_datei_db.DUBLETTEN) as d,
														SUM($import_datei_db.BLACKLIST) as bl,
														SUM($import_datei_db.BOUNCES) as bc,
														SUM($import_datei_db.ANR_AR) as ar,
														SUM($import_datei_db.ANR_VN) as vn,
														SUM($import_datei_db.ANR_NN) as nn,
														SUM($import_datei_db.ANR_STR) as str,
														SUM($import_datei_db.ANR_PLZ) as plz,
														SUM($import_datei_db.ANR_ORT) as ort,
														SUM($import_datei_db.ANR_DE) as de,
														SUM($import_datei_db.ANR_AT) as at,
														SUM($import_datei_db.ANR_CH) as ch,
														SUM($import_datei_db.ANR_GEB) as geb
													 FROM $import_datei_db, $partner_db
													 WHERE $import_datei_db.PARTNER='$pid'
													 AND $import_datei_db.PARTNER=$partner_db.PID
													 AND MONTH($import_datei_db.GELIEFERT_DATUM)='$monat'
													 AND YEAR($import_datei_db.GELIEFERT_DATUM)='$jahr'",$verbindung);
										 
			$oz2 = mysql_fetch_array($orig_daten_brutto_query2);
			
			
			$brutto = $oz2['b'];
			$netto = $oz2['n'];
			$d = $oz2['d'];
			$bl = $oz2['bl'];
			$bc = $oz2['bc'];
			
			$ar = $oz2['ar'];
			$vn = $oz2['vn'];
			$nn = $oz2['nn'];
			$str = $oz2['str'];
			$plz = $oz2['plz'];
			$ort = $oz2['ort'];
			$de = $oz2['de'];
			$at = $oz2['at'];
			$ch = $oz2['ch'];
			$geb = $oz2['geb'];
			$quote = round(($netto/$brutto)*100,2);
			
			$daten .= "<tr>
						   <td><strong>".$partner."</strong></td>
						   <td align='right'><strong>".zahl_formatieren($brutto)."</strong></td>
						   <td align='right'>".zahl_formatieren($bl)."</td>
						   <td align='right'>".zahl_formatieren($bc)."</td>
						    <td align='right'>".zahl_formatieren($d)."</td>
						   <td align='right'><strong>".zahl_formatieren($netto)."</strong></td>
						   <td align='right'><strong>".$quote."%</strong></td>
						   <td align='center'><a href='action.php?import_partner=".$pid."&import_monat=".$monat."&import_jahr=".$jahr."&step=statistik&GenArt=Import'><img src='img/icons/chart.png' title='detailierte Statistik' border='0' /></a></td>
						   <td align='right'>".$ar."</td>
						   <td align='right'>".$vn."</td>
						   <td align='right'>".$nn."</td>
						   <td align='right'>".$str."</td>
						   <td align='right'>".$plz."</td>
						   <td align='right'>".$ort."</td>
						   <td align='right'>".$de."</td>
						   <td align='right'>".$at."</td>
						   <td align='right'>".$ch."</td>
						   <td align='right'>".$geb."</td>
					   </tr>";
			$datensaetze .= $partner.";".$brutto.";".$netto."\r\n";
			
			$brutto_ges += $brutto;
			$bl_ges += $bl;
			$bc_ges += $bc;
			$d_ges += $d;
			$netto_ges += $netto;
			
		}
		
		$quote_ges = round(($netto_ges/$brutto_ges)*100,2);
		$daten .= "<tr style='background-color:#0099FF;color:#FFFFFF;font-weight:bold;'>
				       <td>Gesamt</td>
					   <td align='right'>".zahl_formatieren($brutto_ges)."</td>
					   <td align='right'>".zahl_formatieren($bl_ges)."</td>
					   <td align='right'>".zahl_formatieren($bc_ges)."</td>
					   <td align='right'>".zahl_formatieren($d_ges)."</td>
					   <td align='right'>".zahl_formatieren($netto_ges)."</td>
					   <td align='right'>".$quote_ges."%</td>
					   <td align='right'></td>
					   <td colspan='20' bgcolor='#FFFFFF'></td>
				   </tr>";
		
	} else {
		$daten = "<tr><td colspan='10'>Keine Daten gefunden</td></tr>";
	}
	
	if ($mandant=='intone' || $mandant=='pepperos') {
		if ($mandant=='intone') {include('db_optdb_connect.inc.php');}
		if ($mandant=='pepperos') {include('db_pep_optdb_connect.inc.php');}
		
		
		$orig_daten_brutto_rt_query = mysql_query("SELECT *
												 FROM Report, Partner
												 WHERE MONTH(Report.zeitraum)='$monat'
												 AND YEAR(Report.zeitraum)='$jahr'
												 AND Report.pid=Partner.PID
												 ORDER BY Partner.Pkz ASC",$verbindung_optdb);
		$anzahl_rt = mysql_num_rows($orig_daten_brutto_rt_query);
		
		if ($anzahl_rt>0) {
			while ($rt = mysql_fetch_array($orig_daten_brutto_rt_query)) {
				$pid_rt = $rt['PID'];
				$partner_rt = $rt['Pkz'];
				$brutto_rt = $rt['brutto'];
				$netto_rt = $rt['netto'];
				$quote_rt = round(($netto_rt/$brutto_rt)*100,2);
				
				$daten_rt .= "<tr>
							   <td>".$partner_rt."</td>
							   <td align='right'>".zahl_formatieren($brutto_rt)."</td>
							   <td align='right'>".zahl_formatieren($netto_rt)."</td>
							   <td align='right'>".$quote_rt."%</td>
							   <td align='center'><a href='action.php?import_partner=".$pid_rt."&import_monat=".$monat."&import_jahr=".$jahr."&step=statistik&GenArt=Realtime'><img src='img/icons/chart.png' title='detailierte Statistik' border='0' /></a></td>
						   </tr>";
						   
				$brutto_ges_rt += $brutto_rt;
				$netto_ges_rt += $netto_rt;
			}
			
			$quote_ges_rt = round(($netto_ges_rt/$brutto_ges_rt)*100,2);
			$daten_rt .= "<tr style='background-color:#9966FF;color:#FFFFFF;font-weight:bold;'>
						   <td>Gesamt</td>
						   <td align='right'>".zahl_formatieren($brutto_ges_rt)."</td>
						   <td align='right'>".zahl_formatieren($netto_ges_rt)."</td>
						   <td align='right'>".$quote_ges_rt."%</td>
						   <td colspan='20' bgcolor='#FFFFFF'></td>
					   </tr>";
				   
		} else {
			$daten_rt = "<tr><td colspan='10'>Keine Daten gefunden</td></tr>";
		}
	}
?>
<br />
<table cellpadding="4" cellspacing="0" border="1" style="font-size:11px;border-collapse:collapse;background-color:#FFFFFF;">
	<tr>
		<td colspan="8" align="center" style="background-color:#0066FF; color:#FFFFFF; font-weight:bold;">Importe</td>
		<td colspan="15" align="center" style="background-color:#CCCCCC; color:#666666; font-weight:bold;">Anreicherung</td>
	</tr>
	<tr style="background-color:#0066FF; color:#FFFFFF; font-weight:bold;">
			<td>Partner</td>
			<td>Brutto</td>
			<td>Blacklist</td>
			<td>Bounces</td>
			<td>in MetaDB</td>
			<td>Netto</td>
			<td>Quote</td>
			<td width="25"></td>
			<td style="background-color:#CCCCCC; color:#666666;">Anr.</td>
			<td style="background-color:#CCCCCC; color:#666666;">Vorn.</td>
			<td style="background-color:#CCCCCC; color:#666666;">Nachn.</td>
			<td style="background-color:#CCCCCC; color:#666666;">Str.</td>
			<td style="background-color:#CCCCCC; color:#666666;">Plz</td>
			<td style="background-color:#CCCCCC; color:#666666;">Ort</td>
			<td style="background-color:#CCCCCC; color:#666666;">DE</td>
			<td style="background-color:#CCCCCC; color:#666666;">AT</td>
			<td style="background-color:#CCCCCC; color:#666666;">CH</td>
			<td style="background-color:#CCCCCC; color:#666666;">Geb.</td>
	</tr>
		<?php print $daten; ?>
</table>
<br />
<?php
if ($mandant=='intone' || $mandant=='pepperos') {
	print '
	<table cellpadding="4" cellspacing="0" border="1" style="font-size:11px;border-collapse:collapse;background-color:#FFFFFF;">
		<tr style="background-color:#6633FF; color:#FFFFFF; font-weight:bold;">
			<td colspan="10" align="center">Realtime</td>
		</tr>
		<tr style="background-color:#6633FF; color:#FFFFFF; font-weight:bold;">
				<td>Partner</td>
				<td>Brutto</td>
				<td>Netto</td>
				<td>Quote</td>
				<td width="25"></td>
		</tr>
			'.$daten_rt.'
	</table>';
}
?>


