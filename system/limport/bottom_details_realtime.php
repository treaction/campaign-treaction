<?php
session_start();
header('Content-Type: text/html; charset=ISO-8859-1');
$mandant = $_SESSION['mandant'];
$id = $_GET['impnr'];
require_once('../db_optdb_connect.inc.php');
require_once('../library/util.php');

function zahl_format($zahl) {
	$zahl = number_format($zahl, 0,0,'.');
	return $zahl;
}

function datum_de($date) {
	$datum = explode('-', $date);
	$jahr = $datum[0];
	$monat = $datum[1];
	$tag = $datum[2];
	
	return $monat." / ".$jahr;
	
}

$rep_query = mysql_query("SELECT * 
							  FROM Report, Partner
							  WHERE Report.pid=Partner.PID
							  AND Report.id = '$id'"
							  ,$verbindung_optdb);

$imp_zeile = mysql_fetch_array($rep_query);
							  
$imp_partner = $imp_zeile['Firma'];
$geliefert_datum = datum_de($imp_zeile['zeitraum']);
$imp_anzahl = $imp_zeile['brutto'];
$imp_netto = $imp_zeile['netto'];
$imp_dbl_indatei = $imp_zeile['dublette_in_lieferung'];
$imp_dbl = $imp_zeile['dublette_post'];
$imp_metadb = $imp_zeile['bereits_in_db'];
$imp_blacklist = $imp_zeile['blacklist'];
$imp_fake = $imp_zeile['fake'];
$imp_bounces = $imp_zeile['bounces'];

		
		if ($imp_metadb>0) {
			$q_imp_metadb = round(($imp_metadb/$imp_anzahl)*100,1);
		} else {
			$q_imp_metadb = 0;
			$imp_meta_exp = "";
		}
		
		if ($imp_dbl>0) {
			$q_imp_dbl = round(($imp_dbl/$imp_anzahl)*100,1);
		} else {
			$q_imp_dbl = 0;
			$imp_dbl_exp = "";
		}
		
		if ($imp_dbl_indatei>0) {
			$q_imp_dbl_indatei = round(($imp_dbl_indatei/$imp_anzahl)*100,1);
		} else {
			$q_imp_dbl_indatei = 0;
			$imp_dbl_indatei_exp = "";
		}
		
		if ($imp_blacklist>0) {
			$q_imp_blacklist = round(($imp_blacklist/$imp_anzahl)*100,1);
		} else {
			$q_imp_blacklist = 0;
			$imp_blacklist_exp = "";
		}
		
		if ($imp_bounces>0) {
			$q_imp_bounces = round(($imp_bounces/$imp_anzahl)*100,1);
		} else {
			$q_imp_bounces = 0;
			$imp_bounces_exp = "";
		}

		
		if ($imp_fake>0) {
			$q_imp_fake = round(($imp_fake/$imp_anzahl)*100,1);
		} else {
			$q_imp_fake = 0;
			$imp_fake_exp = "";
		}
		
		if ($imp_dbl_indatei>0) {$q_dbl_indatei = round(($imp_dbl_indatei/$imp_anzahl)*100,1);} else {$q_bl_indatei = 0;}
		if ($imp_langadr>0) {$q_langadr = round(($imp_langadr/$imp_anzahl)*100,1);} else {$q_langadr = 0;}
		$q_netto_zw = ($imp_dbl/$imp_anzahl)*100;
		$q_netto = round(($imp_netto/$imp_anzahl)*100,1);
		
		
		
		$status = $imp_zeile['STATUS'];
		$check = $imp_zeile['CHECK'];

		#if ($imp_zeile['RE']!='') {$status = 4;}
		
		switch ($status) {
			case 0: $status_bez = "Neu";$statusfarbe = "red";break;
			case 1: $status_bez = "Neu";$statusfarbe = "red";break;
			case 2: $status_bez = "Reportfertig";$statusfarbe = "#FF9900";break;
			case 3: $status_bez = "Reporting versendet";$statusfarbe = "green";break;
			case 4: $status_bez = "Rechnung erhalten";$statusfarbe = "#8000ff";break;
			case 5: $status_bez = "Bezahlt";$statusfarbe = "silver";break;
		}
		
		

$details = "<table class='transp'>
				<tr>
					<td class='details_hd_info'>&Uuml;bersicht</td>
					<td class='details_hd_info'></td>
				</tr>
				<tr>
					<td class='details_info'>Partner</td>
					<td class='details2_info'>".$imp_partner."</td>
				</tr>
				<tr>
					<td class='details_info2'>Lieferzeitraum</td>
					<td class='details2_info2'>".$geliefert_datum."</td>
				</tr>
				<tr>
					<td class='details_info'>Liefermenge</td>
					<td class='details2_info'>".zahl_format($imp_anzahl)."</td>
				</tr>
				<tr>
					<td class='details_info2'>Netto (Neuzug&auml;nge)</td>
					<td class='details2_info2'>".zahl_format($imp_netto)." (".util::getQuote($imp_anzahl,$imp_netto)."%)</td>
				</tr>
			</table>";
				
				
	$auswertung = "<table class='transp' style='white-space: nowrap;'>
						<tr>
							<td class='details_hd_info'>Art</td>
							<td class='details_hd_info' style='text-align:left;'>Anzahl</td>
						</tr>
						<tr>
							<td class='details_info'>Liefermenge</td>
							<td class='details2_info'>".zahl_format($imp_anzahl)."</td>
						</tr>
						<tr>
							<td class='details_info'>Dubletten in Lieferung</td>
							<td class='details_info3'>".zahl_format($imp_dbl_indatei)." (".$q_imp_dbl_indatei."%) </td>
						</tr>
						<tr>
							<td class='details_info2'>Dubletten (postalisch)</td>
							<td class='details2_info3'>".zahl_format($imp_dbl)." (".$q_imp_dbl."%) </td>
						</tr>
						<tr>
							<td class='details_info'>Bereits erhalten</td>
							<td class='details_info3'>".zahl_format($imp_metadb)." (".$q_imp_metadb."%)</td>
						</tr>
						<tr>
							<td class='details_info2'>Blacklist</td>
							<td class='details2_info3'>".zahl_format($imp_blacklist)." (".$q_imp_blacklist."%) </td>
						</tr>
						<tr>
							<td class='details_info2'>Fake Email</td>
							<td class='details2_info3'>".zahl_format($imp_fake)." (".$q_imp_fake."%) </td>
						</tr>
						<tr>
							<td class='details_info'>Bounces</td>
							<td class='details_info3'>".zahl_format($imp_bounces)." (".$q_imp_bounces."%) </td>
						</tr>
					</table>";

?>

<div id="adr_div" class="yui-navset" style="padding:5px;text-align:left;border:0px;width:600">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class="selected"><a href="#tab1"><em>Allgemein</em></a></li>
        <li><a href="#tab2"><em>Qualit&auml;t</em></a></li>
    </ul>

    <div id="bottom_content_info" class="yui-content" style="height:250px;overflow:auto;margin:0px;padding:0px;">
        <div id="tab1"><?php print $details;?></div>
        <div id="tab2"><?php print $auswertung;?></div>
    </div>
</div>

<script type="text/javascript">
var tabView = new YAHOO.widget.TabView('adr_div');
</script>
