<?php
include('db_connect.inc.php');
if($_SESSION['testimport'] == 1) {
			$metaDB = $metaDB."_test";
			$import_datei_db = $import_datei_db."_test";
		}
$import_nr = $_REQUEST['import_nr'];
$partner_id = $_REQUEST['partner_id'];
$import_info_query = mysql_query("SELECT * FROM $import_datei_db WHERE IMPORT_NR='$import_nr'",$verbindung);
$zi = mysql_fetch_array($import_info_query);
$datei = $zi['IMPORT_DATEI'];
$datensaetze = $zi['BRUTTO'];
$_SESSION['datei_label'] = "DATEI EXPORT";
?>
<form action="action.php" method="post">
    <table width="100%" cellpadding="4" cellspacing="0" border="0" bgcolor="#666666" style="font-size:11px">
    	<tr>
        	<td height="70" colspan="2" style="background-color:#FFFFFF; font-weight:bold; font-size:16px; color:#0066FF; border-bottom:1px solid #666666;letter-spacing:0.1em" align="left">DATEI EXPORT<br /><span style="color:#999999;font-size:12px;font-weight:normal;letter-spacing:normal">Optionen</span></td>
        </tr>
        <tr style="background-color:#F2F2F2">
        	<td colspan="2" height="15"></td>
        </tr>
        <tr style="background-color:#0066FF;color:#FFFFFF;">
            <td align="right" style="font-weight:bold">Datei</td>
            <td><?php print $datei; ?></td>
        </tr>
        <tr style="background-color:#0066FF;color:#FFFFFF;">
            <td align="right" style="font-weight:bold">Datensätze</td>
            <td><?php print zahl_formatieren($datensaetze); ?></td>
        </tr>
        <tr style="background-color:#FFFFFF">
            <td align="right" style="font-weight:bold; color:#0066FF;">Datenanreicherung</td>
            <td><input type="radio" name="anr" value="anr_ja" checked/> Ja <input type="radio" name="anr" value="anr_nein" /> Nein</td>
        </tr>
        <tr style="background-color:#FFFFFF">
            <td align="right" style="font-weight:bold; color:#0066FF;">Blacklist-Abgleich</td>
            <td><input type="radio" name="blacklist" value="bl_ja" checked/> Ja <input type="radio" name="blacklist" value="bl_nein" /> Nein</td>
        </tr>
         <tr style="background-color:#FFFFFF">
            <td align="right" style="font-weight:bold; color:#0066FF;">Hardbounce-Abgleich</td>
            <td><input type="radio" name="bounce" value="bounce_ja" checked/> Ja <input type="radio" name="bounce" value="bounce_nein"/> Nein </td>
        </tr>
		<tr style="background-color:#FFFFFF">
            <td align="right" style="font-weight:bold; color:#0066FF;">Dubletten-Abgleich</td>
            <td><input type="radio" name="dubletten" value="dubletten_ja" checked/> Ja <input type="radio" name="dubletten" value="dubletten_nein" /> Nein </td>
        </tr>
         <tr style="background-color:#FFFFFF">
            <td align="right" style="font-weight:bold; color:#0066FF;">Fake-Email-Filter</td>
            <td><input type="radio" name="fake" value="fake_ja" checked/> Ein <input type="radio" name="fake" value="fake_nein" /> Aus</td>
        </tr>
        <tr style="background-color:#FFFFFF">
            <td align="right" style="font-weight:bold; color:#0066FF;" valign="top">Trennzeichen</td>
            <td><input type="radio" name="prog" value=";" checked/> [;] Semikolon (Standard)<br />
				<input type="radio" name="prog" value="," /> [,] Komma<br />
                <input type="radio" name="prog" value="t" /> [t] Tabulator (Backclick)</td>
        </tr>
		<tr style="background-color:#FFFFFF">
            <td align="right" style="font-weight:bold; color:#0066FF;" valign="top">Datumsformat</td>
            <td><input type="radio" name="datumsformat" value="dt" checked/> Datetime [YYYY-MM-DD hh:mm:ss]<br />
				<input type="radio" name="datumsformat" value="d" /> Date [YYYY-MM-DD] (Elaine)<br /></td>
        </tr>
        <tr style="background-color:#FFFFFF">
            <td align="right" style="font-weight:bold; color:#0066FF;">Zeichensatz</td>
            <td><input type="radio" name="zeichensatz" value="iso-8859-1" checked/> ANSI <input type="radio" name="zeichensatz" value="utf-8" /> UTF-8</td>
        </tr>
		<tr style="background-color:#FFFFFF">
            <td colspan="2"></td>
        </tr>
        <tr>
            <td align="right" colspan="2">
				<input type="hidden" name="import_nr" value="<?php print $import_nr; ?>" />
				<input type="hidden" name="partner_id" value="<?php print $partner_id; ?>" />
				<input type="hidden" name="step" value="export_ready" />
				<input type="hidden" name="anzahl_brutto" value="<?php print $datensaetze; ?>" value="1" />
				<input type="submit" name="submit_export" value="weiter" /> <input onclick="window.close();" type="button" name="cancel" value="abbrechen" />		</td>
        </tr>
    </table>
</form>
