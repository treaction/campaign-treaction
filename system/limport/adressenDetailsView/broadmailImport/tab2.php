<table class="transp" style="white-space: nowrap;">
    <tr>
        <td class="details_hd_info">Art</td>
        <td class="details_hd_info" style="text-align:left;">Anzahl</td>
        <td class="details_hd_info" style="text-align:left;">Export</td>
    </tr>
    <tr>
        <td class="details_info">Netto</td>
        <td class="details_info3"><?php echo zahl_format($imp_netto); ?></td>
        <td class="details2_info"><?php echo $imp_netto_exp; ?></td>
    </tr>
    <tr>
        <td class="details_info2">Bereits erhalten</td>
        <td class="details2_info3"><?php echo zahl_format($imp_metadb) . ' (' . $q_imp_metadb . '%)'; ?></td>
        <td class="details2_info2">&nbsp;</td>
    </tr>
    <tr>
        <td class="details_info">Intradubletten</td>
        <td class="details_info3"><?php echo zahl_format($imp_dbl_indatei) . ' (' . $q_imp_dbl_indatei . '%)'; ?></td>
        <td class="details2_info">&nbsp;</td>
    </tr>
    <tr>
        <td class="details_info2">Blacklist</td>
        <td class="details2_info3"><?php echo zahl_format($imp_blacklist) . ' (' . $q_imp_blacklist . '%)'; ?></td>
        <td class="details2_info2">&nbsp;</td>
    </tr>
    <tr>
        <td class="details_info">Fehlerhafte</td>
        <td class="details_info3"><?php echo zahl_format($imp_fake) . ' (' . $q_imp_fake . '%)'; ?></td>
        <td class="details2_info">&nbsp;</td>
    </tr>
</table>