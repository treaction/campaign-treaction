<?php
header('Content-Type: text/html; charset=ISO-8859-1');


function zahl_format($zahl) {
    $zahl = number_format($zahl, 0, 0, '.');
    
    return $zahl;
}

function datum_de($date_time, $form = '') {
    $date_1 = explode(' ', $date_time);
    $datum = explode('-', $date_1[0]);
    $jahr = $datum[0];
    $monat = $datum[1];
    $tag = $datum[2];

    $zeit = $date_1[1];
    $zeit_teile = explode(':', $date_1[1]);
    if ($form == 'DE' || $zeit_teile[0] == '00' || $form == 'zeit') {
        $h = $zeit_teile[0];
    } else {
        $h = $zeit_teile[0] + 2;
    }
    
    $m = $zeit_teile[1];
    $zeit = $h . ':' . $m;
    $de = $tag . '.' . $monat . '.' . $jahr;

    $dat_zeit = $de . ' ' . $zeit;
    if ($form == 'dat') {
        return $de;
    } elseif ($form == '' || $form == 'DE') {
        return $dat_zeit;
    } elseif ($form == 'zeit') {
        return $zeit;
    } elseif ($form == 'zeit_bm') {
        return $zeit;
    }
}


require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = $_SESSION['mandant'];
include('db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */


if ((int) $_SESSION['testimport'] === 1) {
    $metaDB = $metaDB . '_test';
    $import_datei_db = $import_datei_db . '_test';
}

$imp_daten = mysql_query(
    'SELECT * ' 
        . ' FROM ' . $import_datei_db . ' as i ' 
            . ' JOIN ' . $partner_db . ' as p ON i.PARTNER = p.' . $felderPartnerDB['PID']
        . ' WHERE i.IMPORT_NR = ' . intval($_GET['impnr'])
    ,
    $verbindung
);
$rowImportDataArray = mysql_fetch_assoc($imp_daten);
mysql_free_result($imp_daten);
// debug
$debugLogManager->logData('rowImportDataArray', $rowImportDataArray);

$imp_nr = (int) $rowImportDataArray['IMPORT_NR'];
$imp_partner = $rowImportDataArray[$felderPartnerDB['Pkz']];
$imp_datum = datum_de($rowImportDataArray['IMPORT_DATUM']);
$geliefert_datum = datum_de(
    $rowImportDataArray['GELIEFERT_DATUM'],
    'dat'
);
$imp_datei = $rowImportDataArray['IMPORT_DATEI'];


$imp_anz_sql = mysql_query(
    'SELECT SUM(BRUTTO) as b, SUM(NETTO) as n, SUM(METADB) as m, SUM(BOUNCES) as bc, SUM(BLACKLIST) as bl, SUM(DUBLETTEN) as d' 
            . ', SUM(FAKE) as f, SUM(DUBLETTE_INDATEI) as dd' 
        . ' FROM ' . $import_datei_db 
        . ' WHERE IMPORT_NR2 = ' . $imp_nr 
            . ' OR IMPORT_NR = ' . $imp_nr
    ,
    $verbindung
);
$sumImportDataArray = mysql_fetch_assoc($imp_anz_sql);
mysql_free_result($imp_anz_sql);
// debug
$debugLogManager->logData('sumImportDataArray', $sumImportDataArray);

$imp_anzahl = (int) $sumImportDataArray['b'];
$imp_netto = (int) $sumImportDataArray['n'];
$imp_metadb = (int) $sumImportDataArray['m'];
$imp_bounces = (int) $sumImportDataArray['bc'];
$imp_blacklist = (int) $sumImportDataArray['bl'];
$imp_dbl = (int) $sumImportDataArray['d'];
$imp_fake = (int) $sumImportDataArray['f'];
$imp_dbl_indatei = (int) $sumImportDataArray['dd'];

if ($imp_metadb > 0) {
    $q_imp_metadb = round(($imp_metadb / $imp_anzahl) * 100, 1);
    $imp_meta_exp = '<a href="limport/export_dbl.php?a=m&impnr=' . $imp_nr . '"><img src="img/Tango/16/actions/document-save.png" border=0"" title="Export als CSV" />';
} else {
    $q_imp_metadb = 0;
    $imp_meta_exp = '';
}

if ($imp_dbl > 0) {
    $q_imp_dbl = round(($imp_dbl / $imp_anzahl) * 100, 1);
    $imp_dbl_exp = '<a href="limport/export_dbl.php?a=d&impnr=' . $imp_nr . '"><img src="img/Tango/16/actions/document-save.png" border="0" title="Export als CSV" />';
} else {
    $q_imp_dbl = 0;
    $imp_dbl_exp = '';
}

if ($imp_dbl_indatei > 0) {
    $q_imp_dbl_indatei = round(($imp_dbl_indatei / $imp_anzahl) * 100, 1);
    $imp_dbl_indatei_exp = '<a href="limport/export_dbl.php?a=i&impnr=' . $imp_nr . '"><img src="img/Tango/16/actions/document-save.png" border="0" title="Export als CSV" />';
} else {
    $q_imp_dbl_indatei = 0;
    $imp_dbl_indatei_exp = '';
}

if ($imp_blacklist > 0) {
    $q_imp_blacklist = round(($imp_blacklist / $imp_anzahl) * 100, 1);
    $imp_blacklist_exp = '<a href="limport/export_dbl.php?a=b&impnr=' . $imp_nr . '"><img src="img/Tango/16/actions/document-save.png" border="0" title="Export als CSV" />';
} else {
    $q_imp_blacklist = 0;
    $imp_blacklist_exp = '';
}

if ($imp_bounces > 0) {
    $q_imp_bounces = round(($imp_bounces / $imp_anzahl) * 100, 1);
    $imp_bounces_exp = '<a href="limport/export_dbl.php?a=h&impnr=' . $imp_nr . '"><img src="img/Tango/16/actions/document-save.png" border="0" title="Export als CSV" />';
} else {
    $q_imp_bounces = 0;
    $imp_bounces_exp = '';
}

if ($imp_fake > 0) {
    $q_imp_fake = round(($imp_fake / $imp_anzahl) * 100, 1);
    $imp_fake_exp = '<a href="limport/export_dbl.php?a=f&impnr=' . $imp_nr . '"><img src="img/Tango/16/actions/document-save.png" border="0" title="Export als CSV" />';
} else {
    $q_imp_fake = 0;
    $imp_fake_exp = '';
}

$imp_netto_exp = '<a href="limport/export_dbl.php?a=&impnr=' . $imp_nr . '"><img src="img/Tango/16/actions/document-save.png" border="0" title="Export als CSV" />';
?>

<div id="adr_div" class="yui-navset" style="padding:5px;text-align:left;border:0px;width:600">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class="selected"><a href="#tab1"><em>Allgemein</em></a></li>
        <li><a href="#tab2"><em>Qualit&auml;t</em></a></li>
    </ul>
    <div id="bottom_content_info" class="yui-content" style="height:250px;overflow:auto;margin:0px;padding:0px;">
        <div id="tab1">
            <?php
                if (isset($rowImportDataArray['BROADMAIL_IMPORT']) && (int) $rowImportDataArray['BROADMAIL_IMPORT'] === 1) {
                    require('adressenDetailsView/broadmailImport/tab1.php');
                } else {
                    require('adressenDetailsView/defaultImport/tab1.php');
                }
            ?>
        </div>
        <div id="tab2">
            <?php
                if (isset($rowImportDataArray['BROADMAIL_IMPORT']) && (int) $rowImportDataArray['BROADMAIL_IMPORT'] === 1) {
                    require('adressenDetailsView/broadmailImport/tab2.php');
                } else {
                    require('adressenDetailsView/defaultImport/tab2.php');
                }
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    var tabView = new YAHOO.widget.TabView('adr_div');
</script>