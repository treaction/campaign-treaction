<?php
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();
require_once('../library/dataManagerWebservice.php');


function importe($gen) {
	$dmW = new dataManagerWebservice();
	require_once('../db_optdb_connect.inc.php');
	global $monat;
	global $jahr;
	global $tab;
	$gen_art = $gen;
	global $monat_jetzt;
	global $jahr_jetzt;
	
	if ($_GET['import_jahr']) {
		$jahr = $_GET['import_jahr'];
	} else {
		$jahr = date("Y");
	}
	
	#$tab .= "<span style='font-size:14px;font-weight:bold;'>".$gen_art." : ".$monat."-".$jahr."</span><br /><br />";
	
	$m=1;
	
	$rep_query = mysql_query("SELECT * 
							  FROM Report, Partner
							  WHERE YEAR(zeitraum) = '$jahr'
							  AND Report.pid=Partner.PID
							  ORDER BY zeitraum DESC"
							  ,$verbindung_optdb);
	$rep_vorhanden = mysql_num_rows($rep_query);
	

	
	if (!$rep_vorhanden) {
	$partner_sql = mysql_query('SELECT PID, Pkz
								FROM `Partner`
								WHERE GenArt = "'.$gen_art.'"
								ORDER BY PID desc
								',$verbindung_optdb);
	
	while($zeile_pid = mysql_fetch_array($partner_sql)) {
		$pid = $zeile_pid['PID'];
		$pid2 = $zeile_pid['PID'];
		$partner = $zeile_pid['Pkz'];
		if ($pid>1) {
			if ($pid=='102' && $gen_art=='Realtime') {$pid = "planet49";}
			
			$info_sql = mysql_query('SELECT notiz, count( * ) AS Anzahl
									FROM `History` 
									WHERE anmeldung LIKE "'.$jahr.'-'.$monat.'%"
									AND Partner = "'.$pid.'"
									GROUP BY notiz
									',$verbindung_optdb);
									
			$vorhanden = mysql_num_rows($info_sql);
			
			if ($vorhanden>=1) {
						$n = 1;
						$tab2 = '';
						$info_ges = '';
						while ($zeile_info = mysql_fetch_array($info_sql)) {
							$info_anzahl = $zeile_info['Anzahl'];
							$info_notiz = $zeile_info['notiz'];
							
							if ($n==1) {
								$brutto_anzahl = $zeile_info['Anzahl'];
								$notiz = $zeile_info['notiz'];
				
								$netto_sql = mysql_query('SELECT SID, count( * ) AS Netto_Anzahl
														FROM `Users` 
														WHERE anmeldung LIKE "'.$jahr.'-'.$monat.'%"
														AND PID = '.$pid2.'
														GROUP BY SID
													',$verbindung_optdb);
								$zeile_netto = mysql_fetch_array($netto_sql);
								$netto_anzahl = $zeile_netto['Netto_Anzahl'];
								$nutzbar = round(($netto_anzahl/$brutto_anzahl)*100, 1);
								
								
									$brutto_anzahl_formatiert = number_format($brutto_anzahl, 0,0,'.');
									$netto_anzahl_formatiert = number_format($netto_anzahl, 0,0,'.');
								
								
							}
							
							if ($n>1) {
								$tab2 .= " | ".$info_notiz.": ".$info_anzahl;
								$info_ges += $info_anzahl;
							}
							$tab_zw = number_format(($brutto_anzahl+$info_ges),0,0,'.');
							$n++;
						}
						
						if ($pid=='planet49' && $gen_art=='Realtime') {$pid = "102";}
						$i_anzahl[] = array('Partner' => $partner,
											'PID' => $pid,
													'Zeitraum' => $monat."-".$jahr,
													'Brutto' => $brutto_anzahl,
													'Netto' => $netto_anzahl,
													'Quote' => $nutzbar,
													'tab_details' => $tab2,
													'tab_zw' => $tab_zw,
													'zaehler' => $m
													);
						$gesamt_brutto += $brutto_anzahl;
						$gesamt_netto += $netto_anzahl;
						$m++;
						
						if (($monat<$monat_jetzt && $jahr<=$jahr_jetzt) || $jahr<$jahr_jetzt) {
							$zeitraum = $jahr."-".$monat."-01";
							$details = "Geliefert: ".$tab_zw.$tab2;
							$insert_rep_query = mysql_query("INSERT INTO Report 
																				(id,
																				 pid,
																				 zeitraum,
																				 brutto,
																				 netto,
																				 details,
																				 netto_ext
																				 ) 
															 VALUES ('','$pid','$zeitraum','$brutto_anzahl','$netto_anzahl','$details','$netto_anzahl')",$verbindung_optdb);
						}
					
				}
		}
		
	}
	
	} elseif ($rep_vorhanden>=1) {
		
			while($zeile_rep = mysql_fetch_array($rep_query)) {
				$id = $zeile_rep['id'];
				$rep_partner = $zeile_rep['Pkz'];
				$rep_anrede = $zeile_rep['Rep_anrede'];
				$rep_ap = $zeile_rep['Rep_ap'];
				$rep_email = $zeile_rep['Rep_email'];
				
				$rep_pid = $zeile_rep['pid'];
				$rep_zeitraum = $zeile_rep['zeitraum'];
				$rep_brutto = $zeile_rep['brutto'];
				$rep_netto = $zeile_rep['netto'];
				$rep_netto_ext = $zeile_rep['netto_ext'];
				$rep_details = $zeile_rep['details'];
				$rep_status = $zeile_rep['status'];
				$rep_nutzbar = round(($rep_netto/$rep_brutto)*100, 1);
				$tab_zw = number_format(($brutto_anzahl+$info_ges),0,0,'.');
				
				$rep_quote_ext = round(($rep_netto_ext/$rep_brutto)*100, 1);
				
				$i_anzahl[] = array('Partner' => $rep_partner,
									'PID' => $rep_pid,
									'Rep_anrede' => $rep_anrede,
									'Rep_ap' => $rep_ap,
									'Rep_email' => $rep_email,
									'Zeitraum' => $rep_zeitraum,
									'Brutto' => $rep_brutto,
									'Netto' => $rep_netto,
									'Quote' => $rep_nutzbar,
									'Quote_ext' => $rep_quote_ext,
									'tab_details' => $rep_details,
									'Status' => $rep_status,
									'zaehler' => $m,
									'id' => $id
													);
													
			$gesamt_brutto += $rep_brutto;
			$gesamt_netto += $rep_netto;
			$m++;									
			}
			
	}
	
	foreach ($i_anzahl as $feld => $zeile_i) {
		$partner_tab[$feld] = $zeile_i['Partner'];
		$pid_tab[$feld] = $zeile_i['PID'];
		$zeitraum_tab[$feld] = $zeile_i['Zeitraum'];
		$brutto_tab[$feld] = $zeile_i['Brutto'];
		$netto_tab[$feld] = $zeile_i['Netto'];
		$quote_tab[$feld] = $zeile_i['Quote'];
		$details_tab[$feld] = $zeile_i['tab_details'];
		$zw_tab[$feld] = $zeile_i['tab_zw'];
		$zaehler_tab[$feld] = $zeile_i['zaehler'];
		$ap_anrede_tab[$feld] = $zeile_i['Rep_anrede'];
		$ap_tab[$feld] = $zeile_i['Rep_ap'];
		$ap_email_tab[$feld] = $zeile_i['Rep_email'];
		$notiz_tab[$feld] = $zeile_i['Notiz'];
		$id_tab[$feld] = $zeile_i['id'];
	}
						
	#array_multisort($brutto_tab, SORT_DESC, $i_anzahl);
	
	
	
	$i=1;					
	foreach ($i_anzahl as $feld_name => $feld_wert) {
		if ($i==2) {$bg = "#E4E4E4";} else {$bg = "#FFFFFF";$i=1;}; 
		
		
		$datum_teile = explode(' ',$feld_wert['Zeitraum']);
		$timestamp2 = $timestamp;
		$timestamp = $datum_teile[0];
		$datum_teile2 = explode('-',$timestamp);
		$monat = $datum_teile2['1'];
		$jahr = $datum_teile2['0'];
		
		switch($monat) {
			case 1: $monat_ = "Januar";break;
			case 2: $monat_ = "Februar";break;
			case 3: $monat_ = "M�rz";break;
			case 4: $monat_ = "April";break;
			case 5: $monat_ = "Mai";break;
			case 6: $monat_ = "Juni";break;
			case 7: $monat_ = "Juli";break;
			case 8: $monat_ = "August";break;
			case 9: $monat_ = "September";break;
			case 10: $monat_ = "Oktober";break;
			case 11: $monat_ = "November";break;
			case 12: $monat_ = "Dezember";break;
		}
	
		$my = $monat_;
				
		
		if ($timestamp!=$timestamp2) {
		
			$imp_zahlen_monat_query = mysql_query("SELECT SUM(Brutto) as b, SUM(Netto) as n FROM Report WHERE YEAR(Zeitraum) = '$jahr' AND MONTH(Zeitraum) = '$monat'",$verbindung_optdb);
				
			$izmz = mysql_fetch_array($imp_zahlen_monat_query);
				
			$q_ = round(($izmz['n']/$izmz['b'])*100,1);
		
		
		
			$tab .= '<tr><td colspan="50"></td></tr><tr style="vertical-align:bottom; background-color:#40547A; font-size:12px;color:#FFFFFF; border-bottom:2px solid #40547A; font-weight:bold;background-image:url(img/bgk.png);background-repeat:repeat-x"><td width="175"> '.$my.'</td><td></td>
			<td>'.number_format($izmz['b'],0,0,'.').'</td>
				<td>'.number_format($izmz['n'],0,0,'.').'</td>
				<td><i>'.$q_.'%</i></td>
				<td></td>
				<td></td>
				</tr>';
			
			
			
		}
		
		$getStatusName = $dmW->getDataManagerStatusName($feld_wert['Status']);
		$details_adressen_rt = "";
		$tab .= "<tr bgcolor='".$bg."' onMouseOver=\"this.bgColor='#bfdaff'\" onMouseOut=\"this.bgColor='".$bg."'\" class='container4' onclick=\"doIt_realtime(event,'".$details_adressen_rt."','".$feld_wert['PID']."','".$monat."','".$jahr."','".$feld_wert['Brutto']."','".$feld_wert['Netto']."','".$feld_wert['Status']."','".$feld_wert['Notiz']."','".$feld_wert['Partner']."','".$feld_wert['id']."');\" style='cursor:pointer'>
					<td>".$getStatusName."</td>
				 	<td style='font-weight:bold;color:#800080;padding-right:35px'>".$feld_wert['Partner']."</td>
					<td align='right'>".number_format($feld_wert['Brutto'],0,0,'.')."</td>
					<td align='right'>".number_format($feld_wert['Netto'],0,0,'.')."</td>
					<td align='right'><i>".$feld_wert['Quote']."%</i></td>
					<td align='right'><i>".$feld_wert['Quote_ext']."%</i></td>
					<td></td>
				</tr>
				<tr id='details".$feld_wert['zaehler']."' name='details".$feld_wert['zaehler']."' style='display:none;'>
					<td colspan='10' style='background-color:#FFFF66'>".$feld_wert['tab_zw'].$feld_wert['tab_details']."</td>
				</tr>";
		$i++;
	}
						
		$_SESSION['tab'] = $tab;
	
;}

if ($_SESSION['mandant']=='intone') {
	importe('Realtime');
} else {
	$tab = "<tr><td height='30' colspan='7' align='center' style='border:0px'>Keine Daten vorhanden</td></tr>";
}

function details_footer () {
	global $mandant;
	include('../db_optdb_connect.inc.php');
	global $jahr;
	global $details_brutto;
	global $details_netto;
	global $offen_anzahl;
	global $dateien_anzahl;
	
	$sql = mysql_query("SELECT SUM(brutto) as B, SUM(netto) as N
				 FROM Report
				 WHERE YEAR(zeitraum) = '$jahr'
				 ",$verbindung_optdb);
	$z = mysql_fetch_array($sql);
	$details_brutto = $z['B'];
	$details_netto = $z['N'];
		
}

function zahl_formatieren($zahl) {
	$zahl = number_format($zahl,0,0,'.');
	return $zahl;
}

details_footer();
$q_all = round(($details_netto/$details_brutto)*100,1);
?>
<div id="fakeContainer">
<table class="scrollTable2" id="scrollTable2" cellspacing="0" cellpadding="0" border="0" bordercolor="#CCCCCC" style="border-collapse:collapse">
				<tr class='tab_bg'>
					<th>Status</th>
					<th>Partner</th>
					<th align="right">Brutto</th>
					<th align="right">Netto</th>
					<th align="right">Quote</th>
					<th align="right">Quote EXT</th>
					<th width="100%"></th>
				</tr>
				<tr>
				<td colspan="50" style="background-color:#FFFFFF;font-size:16px;font-weight:bold;color:#40547A"><?php print $jahr; ?></td>
			</tr>
				<?php print $tab; ?>
</table>
</div>


<div style="background-color:#9933FF;color:#FFFFFF;font-size:11px;height:23px;width:100%;">
		<div style="margin-left:5px;padding-top:5px;float:left;">
		<?php							
			print "Realtime";
		?>
		</div>
		<div style="float:left;margin-top:1px;">
			<img src='img/pink_seperator.png' />
			</div>
			<div style="margin-left:5px;padding-top:5px;float:left;">
			<?php							
				print "Brutto: ".zahl_formatieren($details_brutto);
			?>
		</div>
		<div style="float:left;margin-top:1px;">
			<img src='img/pink_seperator.png' />
			</div>
			<div style="margin-left:5px;padding-top:5px;float:left;">
			<?php							
				print "Netto: ".zahl_formatieren($details_netto);
			?>
		</div>
		<div style="float:left;margin-top:1px;">
			<img src='img/pink_seperator.png' />
			</div>
			<div style="margin-left:5px;padding-top:5px;float:left;">
			<?php							
				print "Quote: ".$q_all."%";
			?>
		</div>
</div>
<input type="hidden" id="realtime_page" value="realtime" />

<script type="text/javascript">

wi = document.documentElement.clientWidth;
hi = document.documentElement.clientHeight;
if (uBrowser == 'Firefox') {
	hi = hi-142;
	wi = wi-205;
} else {
	hi = hi-143;
	wi = wi-205;
}
document.getElementById("fakeContainer").style.width = wi+"px";
document.getElementById("fakeContainer").style.height = hi+"px";

function Resize() {
	setRequest('limport/realtime_inc.php','r');
}

window.onresize = Resize;

var mySt = new superTable("scrollTable2", {
	cssSkin : "sSky",
	fixedCols : 0,
	headerRows : 2
});
</script>