<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Campaign Manager::Platzhalter</title>
<style type="text/css">
body {
	font-family:Arial, Helvetica, sans-serif;
	font-size: 11px;
	padding:0px;
	margin:0px;
}

table {
	border-collapse:collapse;
}
table td {
	border:1px solid #666666;
	padding:3px;
}

.kat {
	background-color:#D9EFFF;
}
</style>
</head>
<body>
<table cellpadding="0" cellspacing="0" width="100%">
	<tr bgcolor="#333333" style="color:#FFFFFF">
		<td>Platzhalter</td>
		<td>Beschreibung</td>
		<td>Beispiel</td>
	</tr>
    <tr class="kat">
		<td colspan="3">Begr&uuml;ssung</td>
	</tr>
	<tr>
		<td>{b_standard}</td>
		<td>Standard Begr&uuml;ssung</td>
		<td>Sehr geehrter Herr</td>
	</tr>
	<tr class="kat">
		<td colspan="3">Lieferungsinformationen</td>
	</tr>
	<tr>
		<td>{l_lieferant}</td>
		<td>Lieferant</td>
		<td>Mustermann GmbH</td>
	</tr>
	<tr>
		<td>{l_lieferant_kurz}</td>
		<td>Lieferant K&uuml;rzel</td>
		<td>mustermann</td>
	</tr>
	<tr>
		<td>{l_name}</td>
		<td>Dateiname</td>
		<td>lieferung1.csv</td>
	</tr>
	<tr>
		<td>{l_lieferdatum}</td>
		<td>Lieferdatum</td>
		<td>11.01.2010</td>
	</tr>
	<tr class="kat">
		<td colspan="3">Abgleichszahlen</td>
	</tr>
<tr>
		<td>{l_brutto}</td>
		<td>Gelieferte Datens&auml;tze</td>
		<td>3.456</td>
	</tr>
	<tr>
		<td>{l_netto}</td>
		<td>Datens&auml;tze nach Abgleich</td>
		<td>3.123</td>
	</tr>
	<tr>
		<td>{l_netto_ext}</td>
		<td>Datens&auml;tze extern</td>
		<td>3.012</td>
	</tr>
	<tr>
		<td>{l_quote}</td>
		<td>Abgleichsquote in %</td>
		<td>88.12</td>
	</tr>
	<tr>
		<td>{l_quote_ext}</td>
		<td>Abgleichsquote in % extern</td>
		<td>77.12</td>
	</tr>
	<tr>
		<td>{l_dubletten}</td>
		<td>Anzahl der Dubletten (postalisch)</td>
		<td>15</td>
	</tr>
	<tr>
		<td>{l_bereits_vorhanden}</td>
		<td>Anzahl der bereits vorhandenen Datens&auml;tze</td>
		<td>213</td>
	</tr>
	<tr>
		<td>{l_bounces}</td>
		<td>Anzahl der Bounces</td>
		<td>55</td>
	</tr>
	<tr>
		<td>{l_dubletten_in_datei}</td>
		<td>Anzahl der Dubletten innerhalb der Lieferung</td>
		<td>5</td>
	</tr>
	<tr>
		<td>{l_blacklist}</td>
		<td>Anzahl der Blacklistadressen</td>
		<td>67</td>
	</tr>
        <tr>
		<td>{l_fake}</td>
		<td>Anzahl der Fakeadressen</td>
		<td>31</td>
	</tr>
	<tr class="kat">
		<td colspan="3">Benutzerdaten</td>
	</tr>
	<tr>
		<td>{u_anrede}</td>
		<td>Anrede des Benutzers</td>
		<td>Herr</td>
	</tr>
	<tr>
		<td>{u_vorname}</td>
		<td>Vorname des Benutzers</td>
		<td>Hans</td>
	</tr>
	<tr>
		<td>{u_nachname}</td>
		<td>Nachname des Benutzers</td>
		<td>Schmidt</td>
	</tr>
	<tr>
		<td>{u_email}</td>
		<td>Email des Benutzers</td>
		<td>hans.schmidt@beispiel.de</td>
	</tr>
	<tr class="kat">
		<td colspan="3">Spezial</td>
	</tr>
	<tr>
		<td>{x_bc_bl}</td>
		<td>Bounces + Blacklist + Fake</td>
		<td>122</td>
	</tr>
	<tr>
		<td>{x_im_verteiler}</td>
		<td>Brutto-(Bounces + Blacklist + Netto_ext + Fake)</td>
		<td>322</td>
	</tr>
</table>
</body>
</html>
