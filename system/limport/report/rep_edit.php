<?php
session_start();
header('Content-Type: text/html; charset=ISO-8859-1');
$mandant = $_SESSION['mandant'];
$mID = $_SESSION['mID'];
include('../../db_connect.inc.php');
include('../../library/util.php');
include('../../library/dataManagerWebservice.php');
include('../../limport/db_connect.inc.php');
$dm = new dataManagerWebservice();

$id = $_GET['id'];
$action = $_GET['action'];

if ($action == 'realtime') {

	$dm_dataArr = $dm->getRealtimeLieferungData($id);
	$dm_PartnerArr = $dm->getPartnerData($partner_db,$felderPartnerDB['Firma'],$felderPartnerDB['PID'],$dm_dataArr['PARTNER']);
	
	$dateiname = "Realtime";
	$partner = $dm_PartnerArr['Firma'];
	$geliefert_datum = strtotime($dm_dataArr['GELIEFERT_DATUM']);
	$geliefert_datum = date("Y-m-d", $geliefert_datum);
	$geliefert_datum = util::getMonat($geliefert_datum);
	$import_datum = $geliefert_datum;
	$brutto = $dm_dataArr['BRUTTO'];
	$netto = $dm_dataArr['NETTO'];
	$netto_ext = $dm_dataArr['NETTO_EXT'];
	
	
} else {

	$dm_dataArr = $dm->getLieferungData($import_datei_db,$partner_db,$felderPartnerDB['Firma'],$felderPartnerDB['PID'],$id);
	$dm_dataSumArr = $dm->getLieferungSumData($import_datei_db,$id);
	
	$partner = $dm_dataArr['PARTNER'];
	$dateiname = $dm_dataArr['IMPORT_DATEI'];
	$geliefert_datum = util::datum_de($dm_dataArr['GELIEFERT_DATUM'],'date');
	$import_datum = util::datum_de($dm_dataArr['IMPORT_DATUM'],'date');
	
	if ($dm_dataArr['ABSCHLAG'] == 0) {
		$abschlag = "-";
	} else {
		$abschlag = $dm_dataArr['ABSCHLAG']."%";
	}
	
	$brutto = $dm_dataSumArr['BRUTTO'];
	$netto = $dm_dataSumArr['NETTO'];
	$netto_ext = $dm_dataArr['NETTO_EXT'];
	
	
}

$q_intern = util::getQuote($brutto,$netto);
$q_extern = util::getQuote($brutto,$netto_ext);
if ($q_intern == $q_extern) {
	$q_anp = "100%";
}

?>
<input type="hidden" name="a_id_rep_edit" id="a_id_rep_edit" value="<?php print $id; ?>" />
<input type="hidden" name="new_rep_realtime" id="new_rep_realtime" value="<?php print $action; ?>" />

<div id="cm_rep" class="yui-navset" style="text-align:left;border:0px;">            
        	<table width="250" class="neu_k" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="3" class='details_hd_info' style="text-align:left">Lieferformationen</td>
				</tr>
                <tr>
                    <td align="right" class='details_info2'>Datei</td>
                    <td colspan="2" align="left" class='details2_info2'><?php print $dateiname; ?></td>
                </tr>
				<tr>
                    <td align="right" class='details_info'>Lieferant</td>
                    <td colspan="2" align="left" class='details2_info'><?php print $partner; ?></td>

                </tr>
                <tr>
                    <td align="right" class='details_info2'>Lieferdatum</td>
                    <td colspan="2" align="left" class='details2_info2'><?php print $geliefert_datum; ?></td>
                </tr>
                <tr>
                    <td align="right" class='details_info'>Importdatum</td>
                    <td colspan="2" align="left" class='details2_info'><?php print $import_datum; ?></td>

                </tr>
                 <tr>
                    <td align="right" class='details_info2'>Gelieferte Datens&auml;tze</td>
                    <td colspan="2" align="left" class='details2_info2'><?php print util::zahl_format($brutto); ?></td>
                </tr>
                 <tr>
                    <td align="right" class='details_info'>Datens&auml;tze nach Abgleich</td>
                    <td colspan="2" align="left" class='details2_info'><?php print util::zahl_format($netto); ?></td>

                </tr>
				<tr>
                    <td align="right" class='details_info'>Standardabschlag</td>
                    <td colspan="2" align="left" class='details2_info'><?php print $abschlag; ?></td>

                </tr>
                <tr>
                    <td align="right" class='details_info2'></td>
                    <td colspan="2" align="left" class='details2_info2'></td>
                </tr>
                <tr>
					<td colspan="3" class='details_hd_info' style="text-align:left">Abgleichszahlen</td>
				</tr>
                <tr>
                    <td colspan="3" align="center" class='details_info2'>
                    	<table class="performance_table" style="background-color:#FFFFFF;width:423px" align="center">
                            <tr class="performance_tr" style="color:#000000">
                                <td></td>
                                <td>Intern</td>
                                <td class="rep_zahlen_ext" style="border-top:1px solid #999999">Extern</td>
								<td style="text-align:center">Neue Abgleichsquote</td>
                            </tr>
							<tr>
                                <td class="fieldset_label">Brutto</td>
                                <td class="performance" style="color:#333333"><?php print util::zahl_format($brutto); ?></td>
								<td class="performance rep_zahlen_ext" style="color:#333333"><?php print util::zahl_format($brutto); ?></td>
                                
							</tr>
							<tr>
                                <td class="fieldset_label">Netto</td>
                                <td class="performance" style="color:#333333"><?php print util::zahl_format($netto); ?></td>
								<td class="performance rep_zahlen_ext" style="color:#333333"><input type="text" name="netto_ext" id="netto_ext" style="background-color:#E6E6E6;border:0px;text-align:right;width:50px;color:#9900FF" value="<?php print util::zahl_format($netto_ext); ?>" readonly/></td>
                                
							</tr>
							<tr>
                                <td class="fieldset_label">Quote</td>
                                <td class="performance" style="color:#333333"><?php print $q_intern; ?>%</td>
								<td class="performance rep_zahlen_ext" style="color:#9900FF;border-bottom:1px solid #999999;"><input type="text" id="q_ext" style="background-color:#E6E6E6;border:0px;text-align:right;width:50px;color:#9900FF" value="<?php print $q_extern ?>" readonly/>%</td>
								<td style="text-align:center;color:#333333"><input type="text" id="new_q" onkeyup="reCalc(<?php print $brutto;?>,this.value);" style="background-color:#FFFF99;border:1px dotted #999999;text-align:right;width:50px" value="<?php print $q_extern; ?>" /> %</td>
                                
							</tr>
                        </table>
                    </td>
                </tr>
            </table>
</div>
<script type="text/javascript">

function reCalc(brutto,v) {
	
	v = str_replace(",",".",v);
	if (v > 100) {v = 100;}
	if (v < 0) {v = 0;}
	document.getElementById('q_ext').value = v;
	document.getElementById('new_q').value = v;
	
	netto_new = (parseFloat(v)*parseInt(brutto))/100;
	netto_new = netto_new.toFixed(0);
	document.getElementById('netto_ext').value = Trenner(netto_new);

}

</script>