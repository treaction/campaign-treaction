<?php
ini_set('memory_limit', '256M');

require_once('../library/debugLogManager.php');

session_start();


function zahl_formatieren($zahl) {
    return number_format($zahl, 0, 0, '.');
}

function getRowDataArray(array $queryPartsDataArray, $functionName, $verbindung, $debugLogManager) {
    $dataArray = null;
    
    $sql = 'SELECT ' . $queryPartsDataArray['SELECT'] 
        . ' FROM ' . $queryPartsDataArray['FROM'] 
        . (strlen($queryPartsDataArray['WHERE']) > 0 ? ' WHERE ' . $queryPartsDataArray['WHERE'] : '') 
        . (strlen($queryPartsDataArray['GROUP_BY']) > 0 ? ' GROUP BY ' . $queryPartsDataArray['GROUP_BY'] : '') 
        . (strlen($queryPartsDataArray['ORDER_BY']) > 0 ? ' ORDER BY ' . $queryPartsDataArray['ORDER_BY'] : '') 
        . (strlen($queryPartsDataArray['LIMIT']) > 0 ? ' LIMIT ' . $queryPartsDataArray['LIMIT'] : '')
    ;
    $res = mysql_query(
        $sql,
        $verbindung
    );
    if ($res) {
        $dataArray = mysql_fetch_assoc($res);
        mysql_free_result($res);
        
        // debug
        $debugLogManager->logData('data: ' . __FUNCTION__ . ' -> ' . $functionName, $dataArray);
        #$debugLogManager->logData('sql: ' . __FUNCTION__ . ' -> ' . $functionName, $sql);
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__ . ' -> ' . $functionName, mysql_error($verbindung));
    }
    
    return $dataArray;
}

function getRowsDataArray(array $queryPartsDataArray, $functionName, $verbindung, $debugLogManager) {
    $dataArray = null;
    
    $sql = 'SELECT ' . $queryPartsDataArray['SELECT'] 
        . ' FROM ' . $queryPartsDataArray['FROM'] 
        . (strlen($queryPartsDataArray['WHERE']) > 0 ? ' WHERE ' . $queryPartsDataArray['WHERE'] : '') 
        . (strlen($queryPartsDataArray['GROUP_BY']) > 0 ? ' GROUP BY ' . $queryPartsDataArray['GROUP_BY'] : '') 
        . (strlen($queryPartsDataArray['ORDER_BY']) > 0 ? ' ORDER BY ' . $queryPartsDataArray['ORDER_BY'] : '') 
        . (strlen($queryPartsDataArray['LIMIT']) > 0 ? ' LIMIT ' . $queryPartsDataArray['LIMIT'] : '')
    ;
    $res = mysql_query(
        $sql,
        $verbindung
    );
    if ($res) {
        $dataArray = array();
        while (($row = mysql_fetch_assoc($res)) !== false) {
            $dataArray[] = $row;
        }
        mysql_free_result($res);
        
        // debug
        $debugLogManager->logData('data: ' . __FUNCTION__ . ' -> ' . $functionName, $dataArray);
        #$debugLogManager->logData('sql: ' . __FUNCTION__ . ' -> ' . $functionName, $sql);
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__ . ' -> ' . $functionName, mysql_error($verbindung));
    }
    
    return $dataArray;
}

function getCountRows(array $queryPartsDataArray, $functionName, $verbindung, $debugLogManager) {
    $count = null;
    
    $sql = 'SELECT count(' . $queryPartsDataArray['FIELD'] . ')' 
        . ' FROM `' . $queryPartsDataArray['FROM'] . '`' 
        . (strlen($queryPartsDataArray['WHERE']) > 0 ? ' WHERE ' . $queryPartsDataArray['WHERE'] : '')
    ;
    $res = mysql_query(
        $sql,
        $verbindung
    );
    if ($res) {
        list($count) = mysql_fetch_row($res);
        $count = intval($count);
        
        mysql_free_result($res);
        
        // debug
        $debugLogManager->logData('data: ' . __FUNCTION__ . ' -> ' . $functionName, $count);
        #$debugLogManager->logData('sql: ' . __FUNCTION__ . ' -> ' . $functionName, $sql);
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__ . ' -> ' . $functionName, mysql_error($verbindung));
    }
    
    return $count;
}

function getCountRowDataArrayByFieldName($fieldName, array $queryPartsDataArray, $functionName, $verbindung, $debugLogManager) {
    $queryPartsDataArray['SELECT'] = $fieldName . ' as `fieldName`, COUNT(*) as `count`';
    
    return getRowDataArray(
        $queryPartsDataArray,
        $functionName . ': ' . __FUNCTION__ . ' -> ' . $fieldName,
        $verbindung,
        $debugLogManager
    );
}

function getCountRowsDataArrayByFieldName($fieldName, array $queryPartsDataArray, $functionName, $verbindung, $debugLogManager) {
    $queryPartsDataArray['SELECT'] = $fieldName . ' as `fieldName`, COUNT(*) as `count`';
    
    return getRowsDataArray(
        $queryPartsDataArray,
        $functionName . ': ' . __FUNCTION__ . ' -> ' . $fieldName,
        $verbindung,
        $debugLogManager
    );
}

function getStatsCountByBirthday($start, array $tableFieldsArray, array $valueFieldsArray, $verbindung, $debugLogManager) {
    $count = 0;
    
    $sql = 'SELECT CAST(`' . $tableFieldsArray['birthday'] . '` as DECIMAL) as `count`' 
        . ' FROM `stats_' . $_SESSION['benutzer_name'] . '`' 
        . ' WHERE `' . $tableFieldsArray['pid'] . '` = "' . $valueFieldsArray['pid'] . '"' 
            . ' AND ' . $start . '(`' . $tableFieldsArray['birthday'] . '`,4) >= "' . $valueFieldsArray['birthday_great'] . '"' 
            . ' AND ' . $start . '(`' . $tableFieldsArray['birthday'] . '`,4) <= "' . $valueFieldsArray['birthday_thin'] . '"'
    ;
    $res = mysql_query(
        $sql,
        $verbindung
    );
    if ($res) {
        $count = intval(mysql_num_rows($res));
        
        mysql_free_result($res);
        
        // debug
        $debugLogManager->logData('data: ' . __FUNCTION__, $count);
        #$debugLogManager->logData('sql: ' . __FUNCTION__, $sql);
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__, mysql_error($verbindung));
    }
    
    return $count;
}

function dropStatsTable($userName, $verbindung, $debugLogManager) {
    $res = mysql_query(
        'DROP TABLE IF EXISTS `stats_' . $userName . '`',
        $verbindung
    );
    if ($res) {
        // debug
        $debugLogManager->logData(__FUNCTION__, true);
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__, mysql_error($verbindung));
    }
}

function createStatsTable($genart, $userName, $fieldsMetaDb, $fieldsPartnerDb,  $importFileTable, $metaDbTable, $partnerTable, $herkunftTable, $partnerValue, $month, $year, $addSql, $verbindung, $debugLogManager) {
    switch ($genart) {
        case 'Import':
            $sql = 'CREATE TABLE `stats_' . $userName . '`' 
                . ' SELECT `m`.`' . $fieldsMetaDb['EMAIL'] . '`, `m`.`' . $fieldsMetaDb['VORNAME'] . '`, `m`.`' . $fieldsMetaDb['NACHNAME'] . '`' 
                        . ', `m`.`' . $fieldsMetaDb['ANREDE'] . '`, `m`.`' . $fieldsMetaDb['STRASSE'] . '`, `m`.`' . $fieldsMetaDb['PLZ'] . '`' 
                        . ', `m`.`' . $fieldsMetaDb['ORT'] . '`, `m`.`' . $fieldsMetaDb['LAND'] . '`, `m`.`' . $fieldsMetaDb['GEBURTSDATUM'] . '`' 
                        . ', `m`.`' . $fieldsMetaDb['TELEFON'] . '`, `m`.`' . $fieldsMetaDb['EINTRAGSDATUM'] . '`, `m`.`' . $fieldsMetaDb['IP'] . '`' 
                        . ', `m`.`' . $fieldsMetaDb['HERKUNFT'] . '`, `m`.`' . $fieldsMetaDb['ATTRIBUTE'] . '`' 
                    . ', `p`.`' . $fieldsPartnerDb['PID'] . '`, `p`.`' . $fieldsPartnerDb['Pkz'] . '`' 
                    . ', `i`.`IMPORT_NR`, `i`.`IMPORT_NR2`, `i`.`BRUTTO`, `i`.`NETTO`, `i`.`NETTO_EXT`, `i`.`GELIEFERT_DATUM`' 
                    . ', `h`.`HERKUNFT_NAME`' 
                . ' FROM `' . $importFileTable . '` as `i`' 
                    . ' left join `' . $metaDbTable . '` as `m` on `m`.`' . $fieldsMetaDb['IMPORT_NR'] . '` = `i`.`IMPORT_NR`' 
                    . ' left join `' . $partnerTable . '` as `p` on `i`.`PARTNER` = `p`.`' . $fieldsPartnerDb['PID'] . '`' 
                    . ' left join `' . $herkunftTable . '` as `h` on `m`.`' . $fieldsMetaDb['HERKUNFT'] . '` = `h`.`ID`' 
                . ' WHERE `p`.`' . $fieldsPartnerDb['PID'] . '` = "' . $partnerValue . '"' 
                    . ' AND MONTH(`i`.`GELIEFERT_DATUM`) = ' . intval($month) . ' AND YEAR(`i`.`GELIEFERT_DATUM`) = ' . intval($year) 
                    . $addSql
            ;
            break;
        
        case 'Realtime':
            $sql = 'CREATE TABLE `stats_' . $userName . '`' 
                . ' SELECT `h`.`Email`, `h`.`Vorname`, `h`.`Nachname`, `h`.`Anrede`, `h`.`Strasse`, `h`.`PLZ`, `h`.`Ort`, `h`.`Land`' 
                        . ', `h`.`Geburtsdatum`, `h`.`Telefon`, `h`.`Anmeldung`, `h`.`Herkunft_IP`, `h`.`Herkunft`, `h`.`Partner`' 
                    . ', `p`.`PID`, `p`.`Pkz`' 
                . ' FROM `History` as `h`' 
                    . ', `' . $partnerTable . '` as `p`' 
                . ' WHERE `h`.`Partner` = "' . $partnerValue . '" AND MONTH(`h`.`Anmeldung`) = ' . intval($month) 
                    . ' AND YEAR(`h`.`Anmeldung`) = ' . intval($year) . ' AND `p`.`GenArt` = "Realtime"' 
                    . $addSql
            ;
            break;
    }
    
    $res = mysql_query(
        $sql,
        $verbindung
    );
    if ($res) {
        // debug
        $debugLogManager->logData(__FUNCTION__ . ' -> ' . $genart, true);
        $debugLogManager->logData('sql: ' . __FUNCTION__ . ' -> ' . $genart, $sql);
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__ . ' -> ' . $genart, mysql_error($verbindung));
    }
    
    return $res;
}

function alterStatsTable($userName, $field, $verbindung, $debugLogManager) {
    $res = mysql_query(
        'ALTER TABLE `stats_' . $userName . '` ADD INDEX (' . $field . ')',
        $verbindung
    );
    if (!$res) {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__ . ' -> ' . $field, mysql_error($verbindung));
    }
    
    return $res;
}

function create_minichart($id, $chart_type, $data_chart, $width, $height) {
    $chart = '
                <div style="width:' . $width . 'px; height:' . $height . 'px;float:left; margin-right:10px;"><div>
                <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="' . $width . '" height="' . $height . '" id="' . $id . '">
                    <param name="movie" value="FusionCharts/FusionCharts/' . $chart_type . '.swf" />
                    <param name="FlashVars" value="&dataXML=' . $data_chart . '">
                    <param name="quality" value="high" />
                    <embed src="FusionCharts/FusionCharts/' . $chart_type . '.swf" flashVars="&dataXML=' . $data_chart . '" quality="high" width="' . $width . '" height="' . $height . '" name="' . $id . '" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
                </object>
            </div>
        </div>'
    ;

    return $chart;
}

function dropAndCreateUserTempTable($userName, $genart, array $tableFieldsArray, array $valueFieldsArray, $verbindung, $debugLogManager) {
    $resDropTempTable = mysql_query(
        'DROP TEMPORARY TABLE IF EXISTS `id_' . $userName . '`',
        $verbindung
    );
    if ($resDropTempTable) {
        switch ($genart) {
            case 'Import':
                $resCreateTempTable = mysql_query(
                    'CREATE TEMPORARY TABLE `id_' . $userName . '`' 
                        . ' SELECT `' . $tableFieldsArray['email'] . '`, `' . $tableFieldsArray['pid'] . '`' 
                        . ' FROM `stats_' . $userName . '`' 
                        . ' WHERE `' . $tableFieldsArray['pid'] . '` = "' . $valueFieldsArray['pid'] . '"'
                    ,
                    $verbindung
                );
                break;
            
            case 'Realtime':
                $resCreateTempTable = mysql_query(
                    'CREATE TEMPORARY TABLE `id_' . $userName . '`' 
                        . ' SELECT `' . $tableFieldsArray['email'] . '`, `' . $tableFieldsArray['pid'] . '`' 
                        . ' FROM `History`' 
                        . ' WHERE `' . $tableFieldsArray['pid'] . '` = "' . $valueFieldsArray['pid'] . '"' 
                        . ' AND MONTH(`' . $tableFieldsArray['registration'] . '`) = ' . intval($valueFieldsArray['month']) 
                        . ' AND YEAR(`' . $tableFieldsArray['registration'] . '`) = ' . intval($valueFieldsArray['year']) 
                    ,
                    $verbindung
                );
                break;
        }
        
        if ($resCreateTempTable) {
            // debug
            $debugLogManager->logData(__FUNCTION__ . ' -> ' . $genart, true);
            
            mysql_query(
                'ALTER TABLE `id_' . $userName . '` ADD INDEX (`' . $tableFieldsArray['email'] . '`)',
                $verbindung
            );
        } else {
            // debug
            $debugLogManager->logData('sql error: ' . __FUNCTION__ . ' -> resCreateTempTable ' . $genart, mysql_error($verbindung));
        }
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__ . ' -> resDropTempTable ' . $genart, mysql_error($verbindung));
    }
}



function uebersicht($genart, $sql, $debugLogManager) {
    global $mandant;
    
    $chart = '<chart captionPadding=\'0\' ' . $_SESSION['statistik']['formatNumber'] . ' ' . $_SESSION['statistik']['tausendSep'] . 
        ' palette=\'3\' canvasBorderColor=\'999999\' canvasBorderThickness=\'1\' showLabels=\'0\' showValues=\'0\' showYAxisValues=\'0\'' . 
        ' chartTopMargin=\'0\' chartBottomMargin=\'1\' chartLeftMargin=\'0\' chartRightMargin=\'1\' showPercentValues=\'1\'>'
    ;

    $dataChart1 = $chart;
    $dataChart2 = $chart;
    $dataChart4 = $chart;
    $dataChart5 = $chart;
    $dataChart5_cat = '<categories>';
    $dataSet5 = '<dataset>';
    $dataSet5_2 = '<dataset>';
    
    $trHinweis = null;
    $geliefert = null;
    $impnr = null;
    $importiert = null;
    $dataTr4 = null;
    $dataTr5 = null;

    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            if ((int) $_SESSION['testimport'] === 1) {
                $metaDB = $metaDB . '_test';
                $import_datei_db = $import_datei_db . '_test';
            }
            
            $dataArray = getRowsDataArray(
                array(
                    'SELECT' => '*',
                    'FROM' => '`' . $import_datei_db . '` as `i`' 
                        .  ' join `' . $partner_db . '` as `p` on `i`.`PARTNER` = `p`.`' . $felderPartnerDB['PID'] . '`'
                    ,
                    'WHERE' => '`i`.`IMPORT_NR` = `i`.`IMPORT_NR2` AND `i`.`PARTNER` = "' . $_REQUEST['import_partner'] . '"' 
                        . ' AND MONTH(`i`.`GELIEFERT_DATUM`) = ' . intval($_REQUEST['import_monat']) . ' AND YEAR(`i`.`GELIEFERT_DATUM`) = ' . intval($_REQUEST['import_jahr']) 
                        . $sql
                ),
                __FUNCTION__ . ' -> getImportDataArrayByMonthAndYear',
                $verbindung,
                $debugLogManager
            );
            if (is_array($dataArray)) {
                // debug
                $debugLogManager->beginGroup(__FUNCTION__ . ' dataArray: ' . $genart);
                
                foreach ($dataArray as $row) {
                    // debug
                    $debugLogManager->logData('row', $row);
                    
                    $impnr .= $row['IMPORT_NR'] . ', ';
                    $partnerName = $row[$felderPartnerDB['Pkz']];
                    $geliefert .= $row['GELIEFERT_DATUM'] . '<br />';

                    $imp_datum_teile = explode(' ', $row['IMPORT_DATUM']);
                    $importiert .= $imp_datum_teile[0] . '<br />';

                    $imp_hinweis = $row['HINWEIS'];
                    if (strlen($imp_hinweis) > 0) {
                        $h = unserialize($imp_hinweis);
                        foreach ($h as $h_part) {
                            $trHinweis .= '<img src="img/icons/achtung.png" width="12" /> ' . $h_part . '<br />';
                        }
                    }
                }
                
                // debug
                $debugLogManager->endGroup();
            }
            
            $latestTenImportDataArray = getRowsDataArray(
                array(
                    'SELECT' => '`i`.`GELIEFERT_DATUM` as `ten_datum`, SUM(`i`.`BRUTTO`) as `ten_brutto`, SUM(`i`.`NETTO`) as `ten_netto`, SUM(`i`.`NETTO_EXT`) as `ten_netto_ext`',
                    'FROM' => '`' . $import_datei_db . '` as `i`' 
                        . ' join `' . $partner_db . '` as `p` on `i`.`PARTNER` = `p`.`' . $felderPartnerDB['PID'] . '`'
                    ,
                    'WHERE' => '`i`.`IMPORT_NR` = `i`.`IMPORT_NR2` AND `i`.`PARTNER` = "' . $_REQUEST['import_partner'] . '"',
                    'GROUP_BY' => '`i`.`GELIEFERT_DATUM`',
                    'ORDER_BY' => '`i`.`GELIEFERT_DATUM` DESC',
                    'LIMIT' => '0,10'
                ),
                __FUNCTION__ . ' -> getLatestTenImportDataArray',
                $verbindung,
                $debugLogManager
            );
            if (is_array($latestTenImportDataArray)) {
                // debug
                $debugLogManager->beginGroup(__FUNCTION__ . ' latestTenImportDataArray: ' . $genart);
                
                $t = 1;
                foreach ($latestTenImportDataArray as $row) {
                    // debug
                    $debugLogManager->logData('row', $row);
                    
                    if ($t === 3) {
                        $t = 1;
                    }
                    
                    $ten_q = round((($row['ten_netto'] / $row['ten_brutto']) * 100), 1);
                    $ten_ext_q = round((($row['ten_netto_ext'] / $row['ten_brutto']) * 100), 1);
                    
                    $dataChart5_cat .= '<category label=\'' . $row['ten_datum'] . '\' />';
                    $dataSet5 .= '<set value=\'' . $ten_q . '\' showValue=\'0\' showLabel=\'0\' />';
                    $dataSet5_2 .= '<set value=\'' . $ten_ext_q . '\' showValue=\'0\' showLabel=\'0\' />';
                    
                    $dataTr5 .= '
                        <tr>
                            <td class="key_ten_bg' . $t . '">' . $row['ten_datum'] . '</td>
                            <td class="stats_ten_bg' . $t . '">' . zahl_formatieren($row['ten_brutto']) . '</td>
                            <td class="stats_ten_bg' . $t . '">' . zahl_formatieren($row['ten_netto']) . '</td>
                            <td class="stats_ten_bg' . $t . '">' . $ten_q . '%</td>
                            <td class="stats_ten_bg' . $t . '">' . $ten_ext_q . '%</td>
                        </tr>
                    ';
                    
                    $t++;
                }
                
                // debug
                $debugLogManager->logData('dataTr5', $dataTr5);
                $debugLogManager->endGroup();
            }
            
            $impnr = substr($impnr, 0, -2);
            
            $dataArray2 = getRowDataArray(
                array(
                    'SELECT' => 'SUM(`BRUTTO`) AS `BRUTTO`, SUM(`NETTO`) AS `NETTO`, SUM(`DUBLETTEN`) AS `d`, SUM(`DUBLETTE_INDATEI`) AS `i`, SUM(`BLACKLIST`) as `b`' 
                        . ', SUM(`FAKE`) as `f`, SUM(`BOUNCES`) as `h`, SUM(`METADB`) as `m`, SUM(`ANR_AR`) as `ar`, SUM(`ANR_VN`) as `vn`, SUM(`ANR_NN`) as `nn`' 
                        . ', SUM(`ANR_STR`) as `str`, SUM(`ANR_PLZ`) as `plz`, SUM(`ANR_ORT`) as `ort`, SUM(`ANR_DE`) as `de`, SUM(`ANR_AT`) as `at`, SUM(`ANR_CH`) as `ch`' 
                        . ', SUM(`ANR_BKZ`) as `bkz`, SUM(`ANR_TEL`) as `tel`, SUM(`ANR_GEB`) as `geb`'
                    ,
                    'FROM' => '`' . $import_datei_db . '` as `i`' 
                        . ' join `' . $partner_db . '` as `p` on `i`.`PARTNER` = `p`.`' . $felderPartnerDB['PID'] . '`'
                    ,
                    'WHERE' => '`i`.`PARTNER` = "' . $_REQUEST['import_partner'] . '" AND MONTH(`i`.`GELIEFERT_DATUM`) = ' . intval($_REQUEST['import_monat']) 
                        . ' AND YEAR(`i`.`GELIEFERT_DATUM`) = ' . intval($_REQUEST['import_jahr']) 
                        . $sql
                ),
                __FUNCTION__ . ' -> getDataArray2',
                $verbindung,
                $debugLogManager
            );
            if (is_array($dataArray2)) {
                $brutto = $dataArray2['BRUTTO'];
                $netto = $dataArray2['NETTO'];
                $m = $dataArray2['m'];
                $d = $dataArray2['d'];
                $i = $dataArray2['i'];
                $b = $dataArray2['b'];
                $f = $dataArray2['f'];
                $h = $dataArray2['h'];
            }
            
            $a = 1;
            $anreichernDataArray = array(
                'ar' => 'Anrede',
                'vn' => 'Vorname',
                'nn' => 'Nachname',
                'str' => 'Strasse',
                'plz' => 'PLZ',
                'ort' => 'Ort',
                'de' => 'DE',
                'at' => 'AT',
                'ch' => 'CH',
                'bkz' => 'Bundesland',
                'tel' => 'Telefon',
                'geb' => 'Geburtsdatum'
            );
            foreach ($anreichernDataArray as $key => $item) {
                if ($a === 3) {
                    $a = 1;
                }
                
                if ($dataArray2[$key] > 0) {
                    $dataChart4 .= '<set value=\'' . $dataArray2[$key] . '\' label=\'' . $item . '\' />';
                    
                    $dataTr4 .= '
                        <tr>
                            <td class="key_bg' . $a . '">' . $item . '</td>
                            <td class="stats_bg' . $a . '">' . zahl_formatieren($dataArray2[$key]) . '</td>
                            <td class="stats_bg' . $a . '">' . round((($dataArray2[$key] / $brutto) * 100), 1) . '%</td>
                        </tr>
                    ';
                    $a++;
                }
            }
            // debug
            $debugLogManager->logData('dataTr4', $dataTr4);
            
            $netto_q = round((($netto / $brutto) * 100), 1);
            $m_q = round((($m / $brutto) * 100), 1);
            $d_q = round((($d / $brutto) * 100), 1);
            $i_q = round((($i / $brutto) * 100), 1);
            $b_q = round((($b / $brutto) * 100), 1);
            $f_q = round((($f / $brutto) * 100), 1);
            $h_q = round((($h / $brutto) * 100), 1);
            break;
        
        case 'Realtime':
            switch ($mandant) {
                case 'intone':
                case 'temp_mandant':
                    include('../db_optdb_connect.inc.php');
                    break;
                
                case 'pepperos':
                    include('../db_pep_optdb_connect.inc.php');
                    break;
            }
            
            $partner = $_REQUEST['import_partner'];
            if ($partner == 'planet49') {
                $partner = 102;
            }
            
            $dataArray = getRowDataArray(
                array(
                    'SELECT' => '*',
                    'FROM' => '`Report` as `r`' 
                        . ' join `Partner` as `p` on `r`.`pid` = `p`.`PID`'
                    ,
                    'WHERE' => '`r`.`pid` = "' . $partner . '" AND MONTH(`r`.`zeitraum`) = ' . intval($_REQUEST['import_monat']) 
                        . ' AND YEAR(`r`.`zeitraum`) = ' . intval($_REQUEST['import_jahr'])
                ),
                __FUNCTION__ . ' getImportDataArrayByMonthAndYear',
                $verbindung_optdb,
                $debugLogManager
            );
            if (is_array($dataArray)) {
                $impnr = '-';
                $partnerName = $dataArray['Pkz'];
                $geliefert = substr($dataArray['zeitraum'], 0, -3);
                $importiert = 'Realtime';
                $netto = $dataArray['netto'];
                $details = $dataArray['details'];
            }
            
            // debug
            $debugLogManager->beginGroup(__FUNCTION__ . ' details - settings');
            
            $detailsDataArray = explode('|', $details);
            foreach ($detailsDataArray as $detailsItem) {
                $sliced = 'isSliced=\'0\'';
                
                if (strpos($detailsItem, 'Geliefert') !== false) {
                    $brutto = preg_replace('/\D/', '', $detailsItem);
                    $quote = round((($netto / $brutto) * 100), 1);
                    $dz_anzahl = $netto;
                    
                    // debug
                    $debugLogManager->beginGroup('Geliefert');
                    $debugLogManager->logData('brutto', $brutto);
                    $debugLogManager->logData('quote', $quote);
                    $debugLogManager->logData('dz_anzahl', $dz_anzahl);
                    $debugLogManager->endGroup();
                }
                
                if (strpos($detailsItem, 'DELETED') !== false) {
                    $d = preg_replace('/\D/', '', $detailsItem);
                    $d_q = round((($d / $brutto) * 100), 1);
                } else {
                    $d = 0;
                    $d_q = 0;
                }
                
                if (strpos($detailsItem, 'Blacklist') !== false) {
                    $b = preg_replace('/\D/', '', $detailsItem);
                    $b_q = round((($b / $brutto) * 100), 1);
                } else {
                    $b = 0;
                    $b_q = 0;
                }
                
                if (strpos($detailsItem, 'falsch') !== false) {
                    $f = preg_replace('/\D/', '', $detailsItem);
                    $f_q = round((($f / $brutto) * 100), 1);
                } else {
                    $f = 0;
                    $f_q = 0;
                }
            }
            
            // debug
            $debugLogManager->endGroup();
            
            $m = $brutto - $netto - $d - $b - $f;
            $m_q = round((($m / $brutto) * 100), 1);
            $netto_q = round((($netto / $brutto) * 100), 1);
            $i = 0;
            $i_q = 0;
            $h = 0;
            $h_q = 0;
            
            $latestTenImportDataArray = getRowsDataArray(
                array(
                    'SELECT' => '*',
                    'FROM' => '`Report`',
                    'WHERE' => '`pid` = "' . $partner . '"',
                    'ORDER_BY' => '`zeitraum` DESC',
                    'LIMIT' => '0,10'
                ),
                __FUNCTION__ . ' getlatestTenImportDataArray',
                $verbindung_optdb,
                $debugLogManager
            );
            if (is_array($latestTenImportDataArray)) {
                // debug
                $debugLogManager->beginGroup(__FUNCTION__ . ' latestTenImportDataArray: ' . $genart);

                $t = 1;
                foreach ($latestTenImportDataArray as $row) {
                    // debug
                    $debugLogManager->logData('row', $row);
                    
                    if ($t === 3) {
                        $t = 1;
                    }
                    
                    $ten_q = round((($row['netto'] / $row['brutto']) * 100), 1);
                    $ten_ext_q = round((($row['netto_ext'] / $row['brutto']) * 100), 1);
                    
                    $dataChart5_cat .= '<category label=\'' . $row['zeitraum'] . '\' />';
                    $dataSet5 .= '<set value=\'' . $ten_q . '\' showValue=\'0\' showLabel=\'0\' />';
                    $dataSet5_2 .= '<set value=\'' . $ten_ext_q . '\' showValue=\'0\' showLabel=\'0\' />';
                    
                    $dataTr5 .= '
                        <tr>
                            <td class="key_ten_bg' . $t . '">' . $row['zeitraum'] . '</td>
                            <td class="stats_ten_bg' . $t . '">' . zahl_formatieren($row['brutto']) . '</td>
                            <td class="stats_ten_bg' . $t . '">' . zahl_formatieren($row['netto']) . '</td>
                            <td class="stats_ten_bg' . $t . '">' . $ten_q . '%</td>
                            <td class="stats_ten_bg' . $t . '">' . $ten_ext_q . '%</td>
                        </tr>
                    ';
                    
                    $t++;
                }
                
                // debug
                $debugLogManager->logData('dataTr5', $dataTr5);
                $debugLogManager->endGroup();
            }
            break;
    }
    
    // debug
    $debugLogManager->beginGroup(__FUNCTION__ . ' ' . $genart . ' - settings');
    $debugLogManager->logData('brutto', $brutto);
    $debugLogManager->logData('netto', $netto);
    $debugLogManager->logData('netto_q', $netto_q);
    $debugLogManager->logData('m', $m);
    $debugLogManager->logData('m_q', $m_q);
    $debugLogManager->logData('b', $b);
    $debugLogManager->logData('b_q', $b_q);
    $debugLogManager->logData('d', $d);
    $debugLogManager->logData('d_q', $d_q);
    $debugLogManager->logData('i', $i);
    $debugLogManager->logData('i_q', $i_q);
    $debugLogManager->logData('f', $f);
    $debugLogManager->logData('f_q', $f_q);
    $debugLogManager->logData('h', $h);
    $debugLogManager->logData('h_q', $h_q);
    $debugLogManager->endGroup();

    $dataChart5 .= $dataChart5_cat . '</categories>' . $dataSet5 . '</dataset>' . $dataSet5_2 . '</dataset>' . '</chart>';

    $dataChart1 .= '<set value=\'' . $brutto . '\' showValue=\'0\' showLabel=\'0\' />' . 
            '<set value=\'' . $netto . '\' showValue=\'0\' showLabel=\'0\' />' . 
        '</chart>
    ';

    $dataChart2 .= '<set value=\'' . $netto . '\' label=\'Netto\' />' . 
            '<set value=\'' . $m . '\' label=\'bereits in MetaDB\' />' . 
            '<set value=\'' . $b . '\' label=\'Blacklist\' />' . 
            '<set value=\'' . $d . '\' label=\'Dubletten\' />' . 
            '<set value=\'' . $i . '\' label=\'Dubletten in Datei\' />' . 
            '<set value=\'' . $f . '\' label=\'Fake-Emails\' />' . 
            '<set value=\'' . $h . '\' label=\'Bounces\' />' . 
        '</chart>'
    ;
    
    $dataChart4 .= '</chart>';
    
    // debug
    $debugLogManager->logData('dataChart5', $dataChart5);
    $debugLogManager->logData('dataChart1', $dataChart1);
    $debugLogManager->logData('dataChart2', $dataChart2);
    $debugLogManager->logData('dataChart4', $dataChart4);

    /**
     * datensaetze_analyse
     * 
     * debug
     */
    $debugLogManager->beginGroup(__FUNCTION__ . ' datensaetze_analyse -> begin');
    
    datensaetze_analyse(
        $genart,
        '',
        $debugLogManager
    );
    $dataChart3 = $chart . $_SESSION['statistik']['dataDatensaetze'];
    $trHinweis .= $_SESSION['statistik']['trHinweis2'];
    if (strlen($trHinweis) === 0) {
        $trHinweis = '
            <tr>
                <td><img src="img/icons/accept.png" /></td>
                <td>Keine Hinweise / Warnungen vorhanden</td>
            </tr>
        ';
        $hinweisClass = 'stats';
    } else {
        $hinweisClass = 'stats_red';
        $trHinweis = $trHinweis;
    }
    
    // debug
    $debugLogManager->logData('dataChart3', $dataChart3);
    $debugLogManager->endGroup();


    /**
     * anmeldung_analyse
     * 
     * debug
     */
    $debugLogManager->beginGroup(__FUNCTION__ . ' anmeldung_analyse -> begin');
    
    anmeldung_analyse(
        $genart,
        '',
        $debugLogManager
    );
    $dataChart8 = $chart . $_SESSION['statistik']['dataDatenalterAll'] . '</chart>';
    $dataTr8 = $_SESSION['statistik']['dataTr8'];
    
    // debug
    $debugLogManager->logData('dataChart8', $dataChart8);
    $debugLogManager->endGroup();
    

    if ($netto_q < 85) {
        $fontColor = 'color:red;';
    } else {
        $fontColor = 'color:green;';
    }

    $table1 = '
        <table class="table_stats" cellspacing="0" border="0">
            <tr>
                <td width="70" class="key_bg1">Import Nr.</td>
                <td width="100" class="stats_bg1" style="text-align:left;">' . $impnr . '</td>
            </tr>
            <tr>
                <td class="key_bg2">Partner</td>
                <td class="stats_bg2" style="text-align:left;font-weight:bold;">' . $partnerName . '</td>
            </tr>
            <tr>
                <td class="key_bg1">Geliefert am</td>
                <td class="stats_bg1" style="text-align:left;">' . $geliefert . '</td>
            </tr>
            <tr>
                <td class="key_bg2">Importiert am</td>
                <td class="stats_bg2" style="text-align:left;">' . $importiert . '</td>
            </tr>
            <tr>
                <td class="key_bg1">Brutto</td>
                <td class="stats_bg1" style="text-align:left;font-weight:bold;">' . zahl_formatieren($brutto) . '</td>
            </tr>
            <tr>
                <td class="key_bg2">Netto</td>
                <td class="stats_bg2" style="text-align:left;font-weight:bold;' . $fontColor . '">' . zahl_formatieren($netto) . ' (' . $netto_q . '%)</td>
            </tr>
        </table>
    ';

    $table2 = '
        <table class="table_stats" cellspacing="0" border="0">
            <tr>
                <td width="70" class="key_bg2">Bezeichnung</td>
                <td width="70" class="key_bg2" align="left">Anzahl</td>
                <td width="30" class="key_bg2">Quote</td>
            </tr>
            <tr>
                <td class="key_bg1">Netto</td>
                <td class="stats_bg1">' . zahl_formatieren($netto) . '</td>
                <td class="stats_bg1">' . $netto_q . '%</td>
            </tr>
            <tr>
                <td class="key_bg2">in MetaDB</td>
                <td class="stats_bg2">' . zahl_formatieren($m) . '</td>
                <td class="stats_bg2">' . $m_q . '%</td>
            </tr>
            <tr>
                <td class="key_bg1">Blacklist</td>
                <td class="stats_bg1">' . zahl_formatieren($b) . '</td>
                <td class="stats_bg1">' . $b_q . '%</td>
            </tr>
            <tr>
                <td class="key_bg2">Dubletten</td>
                <td class="stats_bg2">' . zahl_formatieren($d) . '</td>
                <td class="stats_bg2">' . $d_q . '%</td>
            </tr>
            <tr>
                <td class="key_bg1">Dubl. in Datei</td>
                <td class="stats_bg1">' . zahl_formatieren($i) . '</td>
                <td class="stats_bg1">' . $i_q . '%</td>
            </tr>
            <tr>
                <td class="key_bg2">Fake-Email</td>
                <td class="stats_bg2">' . zahl_formatieren($f) . '</td>
                <td class="stats_bg2">' . $f_q . '%</td>
            </tr>
            <tr>
                <td class="key_bg1">Bounces</td>
                <td class="stats_bg1">' . zahl_formatieren($h) . '</td>
                <td class="stats_bg1">' . $h_q . '%</td>
            </tr>
        </table>
    ';

    $table3 = '
        <table class="table_stats" cellspacing="0" border="0">
            <tr>
                <td width="70" class="key_bg2">Feld</td>
                <td width="70" class="key_bg2" align="left">Anzahl</td>
                <td width="30" class="key_bg2">Quote</td>
            </tr>' . $_SESSION['statistik']['dataTr3'] . 
        '</table>
    ';

    if (strlen($dataTr4) === 0) {
        $dataTr4 = '
            <tr>
                <td colspan="10">Keine Daten vorhanden</td>
            </tr>
        ';
    }
    $table4 = '
        <table class=2table_stats" cellspacing="0" border="0">
            <tr>
                <td width="70" class="key_bg2">Feld</td>
                <td width="70" class="key_bg2" align="left">Anzahl</td>
                <td width="30" class="key_bg2">Quote</td>
            </tr>' . $dataTr4 . 
        '</table>
    ';

    $table5 = '
        <table class="table_stats" cellspacing="0" border="0">
            <tr>
                <td width="54" class="key_ten_bg2">Lieferdatum</td>
                <td width="45" class="key_ten_bg2" align="left">Brutto</td>
                <td width="45" class="key_ten_bg2">Netto</td>
                <td width="30" class="key_ten_bg2">Quote</td>
                <td width="30" class="key_ten_bg2">EXT</td>
                </tr>' . $dataTr5 . 
        '</table>
    ';

    $table6 = '<table class="table_stats_red" cellspacing="0" border="0">' . $trHinweis . '</table>';

    $lieferveinbarung = $_SESSION['statistik']['trVereinbarung'] . $_SESSION['statistik']['alterHinweis'];
    if (strlen($lieferveinbarung) === 0) {
        $lieferveinbarung = '
            <tr>
                <td>Es wurden keine Vereinbarungen eingestellt.</td>
            </tr>
        ';
    }

    if (strpos($lieferveinbarung, 'achtung') != true && strlen($lieferveinbarung) > 0) {
        $lieferveinbarung = '
            <tr>
                <td style="border:none;"><img src="img/icons/accept.png" width="16" /></td>
                <td style=2border:none;">Alle Liefervereinbarungen wurden eingehalten.</td>
            </tr>
        ';
    }

    $table7 = '<table class="table_stats_vereinbarung" cellspacing="0" cellpadding="0" border="0">' . $lieferveinbarung . '</table>';

    if (strlen($dataTr8) === 0) {
        $dataTr8 = '
            <tr>
                <td colspan="10">Keine Daten vorhanden</td>
            </tr>
        ';
    }
    $table8 = '
        <table class="table_stats" cellspacing="0" border="0">
            <tr>
                <td width="80" class="key_ten_bg2">Anmeldung</td>
                <td width="80" class="key_ten_bg2" align="left">Anzahl</td>
                <td width="30" class="key_ten_bg2">Quote</td>
            </tr>' . $dataTr8 . 
        '</table>
    ';

    $div_left = '
        <div style="width:285px;float:left">
            <fieldset class="stats">
                <legend>Info</legend>' . 
                create_minichart(
                    'info',
                    'Column2D',
                    $dataChart1,
                    '75',
                    '75'
                ) . $table1 . 
            '</fieldset>
            <fieldset class="stats" style="margin-top:10px">
                <legend>Qualit&auml;t</legend>' . 
                create_minichart(
                    'quali',
                    'Pie2D',
                    $dataChart2,
                    '75',
                    '75'
                ) . $table2 . 
            '</fieldset>
            <fieldset class="stats" style="margin-top:10px">
                <legend>Datenalter</legend>' . 
                create_minichart(
                    'datenalter',
                    'Pie2D',
                    $dataChart8,
                    '75',
                    '75'
                ) . $table8 . 
            '</fieldset>
        </div>
    ';

    $div_middle = '
        <div style="width:285px;float:left;margin-left:8px;">
            <fieldset class="stats">
                <legend>Liefervereinbarungen</legend>' . 
                $table7 . 
            '</fieldset>
            <fieldset class="stats" style="margin-top:8px">
                <legend>Datenstruktur</legend>' . 
                create_minichart(
                    'datenstruktur',
                    'StackedBar2D',
                    $dataChart3,
                    '75',
                    '200'
                ) . $table3 . 
            '</fieldset>
        </div>
    ';

    $div_right = '
        <div style="width:285px;float:left;margin-left:8px;">
            <fieldset class="' . $hinweisClass . '">
                <legend>Hinweise</legend>' . 
                $table6 . 
            '</fieldset>
            <fieldset class="stats" style="margin-top:8px">
                <legend>Letzten Lieferungen</legend>' . 
                create_minichart(
                    'ten',
                    'MSLine',
                    $dataChart5,
                    '260',
                    '75'
                ) . $table5 . 
            '</fieldset>
            <fieldset class="stats" style="margin-top:8px">
                <legend>Anreicherung</legend>' . 
                create_minichart(
                    'anreicherung',
                    'Bar2D',
                    $dataChart4,
                    '75',
                    '75'
                ) . $table4 . 
            '</fieldset>
        </div>
    ';
    
    echo $div_left . $div_middle . $div_right;
}

function domain_analyse($genart, $subCaption, $debugLogManager) {
    global $mandant;
    
    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            $statsEmailDataArray = getRowsDataArray(
                array(
                    'SELECT' => '`EMAIL`',
                    'FROM' => '`stats_' . $_SESSION['benutzer_name'] . '`'
                ),
                __FUNCTION__ . ' -> getStatsEmailDataArray',
                $verbindung,
                $debugLogManager
            );
            break;
        
        case 'Realtime':
            include('../db_optdb_connect.inc.php');
            
            $statsEmailDataArray = getRowsDataArray(
                array(
                    'SELECT' => '`Email` as `EMAIL`',
                    'FROM' => '`stats_' . $_SESSION['benutzer_name'] . '`',
                    'WHERE' => '`Partner` = "' . $_REQUEST['import_partner'] . '"'
                ),
                __FUNCTION__ . ' -> getStatsEmailDataArray',
                $verbindung_optdb,
                $debugLogManager
            );
            break;
    }
    if (is_array($statsEmailDataArray)) {
        $i = 0;
        $domainsDataArray = array();
        foreach ($statsEmailDataArray as $row) {
            $email = explode('@', strtolower($row['EMAIL']));
            $email = $email[1];
            
            $domainsDataArray[$i]['Email'] = $email;
            $domainsDataArray[$i]['Anzahl'] = 1;
            $i++;
        }
    }

    $allDomainsDataArray = array();
    foreach ($domainsDataArray as $domainItem) {
        $allDomainsDataArray[$domainItem['Email']] = 0;
    }
    foreach ($domainsDataArray as $domainItem) {
        if (isset($allDomainsDataArray[$domainItem['Email']])) {
            $allDomainsDataArray[$domainItem['Email']] += $domainItem['Anzahl'];
        }
    }
    unset($domainsDataArray);
    arsort($allDomainsDataArray);
    
    // debug
    $debugLogManager->logData('allDomainsDataArray', $allDomainsDataArray);

    $dataChart = '<chart caption=\'Verteilung nach Emaildomain\' subcaption=\'' . $subCaption . '\' ' .  $_SESSION['statistik']['formatNumber'] . ' ' . 
        $_SESSION['statistik']['tausendSep'] . ' showZeroPies=\'0\' enableRotation=\'1\' palette=\'3\' decimals=\'1\' showPercentValues=\'1\'' . 
        ' enableRotation=\'0\' bgColor=\'FFFFFF\' bgAlpha=\'40,100\' bgRatio=\'0,100\' bgAngle=\'360\' showBorder=\'1\' startingAngle=\'120\'>
    ';
    
    $data = null;
    $ohne = 0;
    $j = 1;
    foreach ($allDomainsDataArray as $email => $itemCount) {
        if ($j <= 10) {
            $quote = round((($itemCount / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
            if (strlen($itemCount) === 0) {
                $itemCount = 0;
            }
            
            $data .= '{Domain: \'' . $email . '\', Anzahl:' . $itemCount . ', Quote: \'' . $quote . '%\'},';
            $dataChart .= '<set label=\'' . $email . '\' value=\'' . $itemCount . '\' />';
            
            $ohne +=$itemCount;
            
            $j++;
        } else {
            break;
        }
    }

    $ohne = $_SESSION['statistik']['brutto_analyse'] - $ohne;
    $quote_ohne = round((($ohne / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
    
    $data .= '{Domain: \'Sonst\', Anzahl:' . $ohne . ', Quote: \'' . $quote_ohne . '%\'}';
    $dataChart .= '<set label=\'Sonst\' value=\'' . $ohne . '\' isSliced=\'1\' /></chart>';
    
    // debug
    $debugLogManager->beginGroup(__FUNCTION__);
    $debugLogManager->logData('data', $data);
    $debugLogManager->logData('dataChart', $dataChart);
    $debugLogManager->endGroup();

    $_SESSION['statistik']['chartType'] = 'Pie3D';
    $_SESSION['statistik']['analyseTyp'] = 'domain';
    $_SESSION['statistik']['datasource'] = '
        <script type="text/javascript">
            YAHOO.example.domain = [' . $data . '];
            
            var opinionData = new YAHOO.util.DataSource(YAHOO.example.domain);
            opinionData.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            opinionData.responseSchema = {
                fields: ["Domain", "Anzahl", "Quote"]
            };
            
            var columns = [
                {key: "Domain", sortable: true, resizeable: true},
                {key: "Anzahl", sortable: true, resizeable: true},
                {key: "Quote", resizeable: true}
            ];
            var mytable = new YAHOO.widget.DataTable(
                "datatable",
                columns,
                opinionData,
                {
                    sortedBy: {key: "Domain"}
                }
            );
	</script>
    ';
    $_SESSION['statistik']['dataChart'] = $dataChart;
}

function verwertbarkeit_analyse($genart, $subCaption, $debugLogManager) {
    global $mandant;

    $dataChart = '<chart caption=\'Quote Brutto-Netto\' subcaption=\'' . $subCaption . '\' showValues=\'0\' PYAxisName=\'Brutto\' SYAxisName=\'Netto\'' . 
        ' useRoundEdges=\'1\' ' . $_SESSION['statistik']['formatNumber'] . ' ' . $_SESSION['statistik']['tausendSep'] . ' rotateLabels=\'1\' showPercentValues=\'1\'>
    ';
    $cat = '<categories>';
    $dataset1 = '<dataset seriesName=\'Brutto\'>';
    $dataset2 = '<dataset seriesName=\'Netto\'>';
    $dataset3 = '<dataset seriesName=\'Netto EXT\' parentYAxis=\'S\'>';
    
    $data = null;

    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            $dataArray = getRowsDataArray(
                array(
                    'SELECT' => 'DISTINCT(`IMPORT_NR`)',
                    'FROM' => '`stats_' . $_SESSION['benutzer_name'] . '`',
                    'WHERE' => '`' . $felderPartnerDB['PID'] . '` = "' . $_REQUEST['import_partner'] . '"',
                    'ORDER_BY' => '`GELIEFERT_DATUM` ASC'
                ),
                __FUNCTION__ . ' -> getDistinctImportNrDataArray',
                $verbindung,
                $debugLogManager
            );
            if (is_array($dataArray)) {
                $rowsArray = array();
                foreach ($dataArray as $row) {
                    $rowsArray[] = $row['IMPORT_NR'];
                }
                
                // debug
                $debugLogManager->logData('rowsArray', $rowsArray);
                $debugLogManager->beginGroup(__FUNCTION__ . ' -> importNrDataArray: ' . $genart);

                foreach ($rowsArray as $importNrItem) {
                    $row = getRowDataArray(
                        array(
                            'SELECT' => '*',
                            'FROM' => '`stats_' . $_SESSION['benutzer_name'] . '`',
                            'WHERE' => '`' . $felderMetaDB['IMPORT_NR'] . '` = ' . intval($importNrItem)
                        ),
                        __FUNCTION__ . ' -> getImportNrDataArray',
                        $verbindung,
                        $debugLogManager
                    );
                    if (is_array($row)) {
                        // debug
                        $debugLogManager->logData('importNrDataArray', $row);

                        $importNrQuote = round((($row['NETTO'] / $row['BRUTTO']) * 100), 1);
                        $importNrQuoteExt = round((($row['NETTO_EXT'] / $row['BRUTTO']) * 100), 1);

                        $data .= '{Lieferdatum: \'' . $row['GELIEFERT_DATUM'] . '\', Brutto:' . $row['BRUTTO'] . ', Netto:' . $row['NETTO'] . ', Quote_: ' . $importNrQuote . ',' . 
                            ' Quote: \'' . $importNrQuote . '%\', Quote_e: ' . $importNrQuoteExt . ', Quote_EXT: \'' . $importNrQuoteExt . '%\'},
                        ';

                        $cat .= '<category  label=\'' . $row['GELIEFERT_DATUM'] . '\' />';
                        $dataset1 .= '<set value=\'' . $row['BRUTTO'] . '\' />';
                        $dataset2 .= '<set value=\'' . $row['NETTO'] . '\' />';
                        $dataset3 .= '<set value=\'' . $row['NETTO_EXT'] . '\' />';
                    }
                }

                // debug
                $debugLogManager->endGroup();
            }
            break;
        
        case 'Realtime':
            $partner = $_REQUEST['import_partner'];
            if ($partner == 'planet49') {
                $partner = '102';
            }
            
            switch ($mandant) {
                case 'intone':
                case 'temp_mandant':
                    include('../db_optdb_connect.inc.php');
                    break;
                
                case 'pepperos':
                    include('../db_pep_optdb_connect.inc.php');
                    break;
            }
            
            $dataArray = getRowsDataArray(
                array(
                    'SELECT' => '*',
                    'FROM' => '`Report`',
                    'WHERE' => '`pid` = "' . $partner . '" AND MONTH(`zeitraum`) = ' . intval($_REQUEST['import_monat']) 
                        . ' AND YEAR(`zeitraum`) = ' . intval($_REQUEST['import_jahr'])
                ),
                __FUNCTION__ . ' -> getReportDataArray',
                $verbindung_optdb,
                $debugLogManager
            );
            if (is_array($dataArray)) {
                // debug
                $debugLogManager->beginGroup(__FUNCTION__ . ' reportDataArray: ' . $genart);
                
                foreach ($dataArray as $row) {
                    // debug
                    $debugLogManager->logData('row', $row);
                    
                    $importNrQuote = round((($row['netto'] / $row['brutto']) * 100), 1);
                    $importNrQuoteExt = round((($row['netto_ext'] / $row['brutto']) * 100), 1);

                    $data .= '{Lieferdatum: \'' . $row['zeitraum'] . '\', Brutto:' . $row['brutto'] . ', Netto:' . $row['netto'] . ', Quote_: ' . $importNrQuote . ',' . 
                        ' Quote: \'' . $importNrQuote . '%\', Quote_e: ' . $importNrQuoteExt . ', Quote_EXT: \'' . $importNrQuoteExt . '%\'},
                    ';
                    
                    $cat .= '<category label=\'' . $row['zeitraum'] . '\' />';
                    $dataset1 .= '<set value=\'' . $row['brutto'] . '\' />';
                    $dataset2 .= '<set value=\'' . $row['netto'] . '\' />';
                    $dataset3 .= '<set value=\'' . $row['netto_ext'] . '\' />';
                }
                
                // debug
                $debugLogManager->endGroup();
            }
            break;
    }

    $dataset1 .= '</dataset>';
    $dataset2 .= '</dataset>';
    $dataset3 .= '</dataset>';
    $cat .= '</categories>';
    
    $dataChart .= $cat . $dataset1 . $dataset2 . $dataset3 . 
        '<definition>' . 
            '<style name=\'bgAnim\' type=\'animation\' param=\'_xScale\' start=\'0\' duration=\'1\' />' . 
        ' </definition></chart>
    ';

    $data = substr($data, 0, -1);
    
    // debug
    $debugLogManager->beginGroup(__FUNCTION__);
    $debugLogManager->logData('data', $data);
    $debugLogManager->logData('dataChart', $dataChart);
    $debugLogManager->endGroup();

    $_SESSION['statistik']['chartType'] = 'MSCombi3D';
    $_SESSION['statistik']['analyseTyp'] = 'verwertbarkeit';
    $_SESSION['statistik']['datasource'] = '
        <script type="text/javascript">
            YAHOO.example.datensaetze =	[' . $data . '];

            var opinionData = new YAHOO.util.DataSource(YAHOO.example.datensaetze);
            opinionData.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            opinionData.responseSchema = {
                fields: ["Lieferdatum", "Brutto", "Netto", "Quote_", "Quote", "Quote_e", "Quote_EXT"]
            };
            
            var columns = [
                {key: "Lieferdatum", sortable: true, resizeable: true, width:50},
                {key: "Brutto", sortable: true, resizeable: true, width:30},
                {key: "Netto", sortable: true, resizeable: true, width:30},
                {key: "Quote", label: "Quote",  resizeable: true, width:25},
                {key: "Quote_EXT", label: "EXT",  resizeable: true, width:25}
            ];
            
            var mytable = new YAHOO.widget.DataTable(
                "datatable",
                columns,
                opinionData,
                {
                    sortedBy: {key: "Lieferdatum"}
                }
            );
	</script>
    ';
    $_SESSION['statistik']['dataChart'] = $dataChart;
}

function history_analyse($genart, $subCaption, $debugLogManager) {
    global $mandant;
    
    $dataChart = '<chart caption=\'Verlauf der letzten Abgleichquoten: ' . $_SESSION['statistik']['x_lieferungen'] . ' Lieferungen\' subcaption=\'' . $subCaption . '\'' . 
        ' showValues=\'0\' PYAxisName=\'Netto Quote\' SYAxisName=\'Netto Quote EXT\' useRoundEdges=\'1\' ' . $_SESSION['statistik']['formatNumber'] . 
        ' ' . $_SESSION['statistik']['tausendSep'] . ' rotateLabels=\'1\' showPercentValues=\'1\'>
    ';
    $cat = '<categories>';
    $dataset1 = '<dataset seriesName=\'Netto Quote\'>';
    $dataset2 = '<dataset seriesName=\'Netto Quote EXT\'>';
    
    $data = null;

    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            if ((int) $_SESSION['testimport'] === 1) {
                $metaDB = $metaDB . '_test';
                $import_datei_db = $import_datei_db . '_test';
            }
            
            $dataArray = getRowsDataArray(
                array(
                    'SELECT' => '`i`.`GELIEFERT_DATUM` as `ten_datum`, SUM(`i`.`BRUTTO`) as `ten_brutto`, SUM(`i`.`NETTO`) as `ten_netto`, SUM(`i`.`NETTO_EXT`) as `ten_netto_ext`',
                    'FROM' => '`' . $import_datei_db . '` as `i`' 
                        . ' join `' . $partner_db . '` as `p` on `i`.`PARTNER` = `p`.`' . $felderPartnerDB['PID'] . '`',
                    'WHERE' => '`i`.`IMPORT_NR` = `i`.`IMPORT_NR2` AND `i`.`PARTNER` = "' . $_REQUEST['import_partner'] . '"',
                    'GROUP_BY' => '`i`.`GELIEFERT_DATUM`',
                    'ORDER_BY' => '`i`.`GELIEFERT_DATUM` DESC',
                    'LIMIT' => '0,' . $_SESSION['statistik']['x_lieferungen']
                ),
                __FUNCTION__ . ' -> getDataArray',
                $verbindung,
                $debugLogManager
            );
            if (is_array($dataArray)) {
                // debug
                $debugLogManager->beginGroup(__FUNCTION__ . ' dataArray: ' . $genart);
                
                foreach ($dataArray as $row) {
                    // debug
                    $debugLogManager->logData('row', $row);
                    
                    $ten_q = round((($row['ten_netto'] / $row['ten_brutto']) * 100), 1);
                    $ten_ext_q = round((($row['ten_netto_ext'] / $row['ten_brutto']) * 100), 1);

                    $data .= '{Lieferdatum: \'' . $row['ten_datum'] . '\', Brutto:' . $row['ten_brutto'] . ', Netto:' . $row['ten_netto'] . ', Quote_: ' . $ten_q . ',' . 
                        ' Quote: \'' . $ten_q . '%\', Quote_e: ' . $ten_ext_q . ', Quote_EXT: \'' . $ten_ext_q . '%\'},
                    ';
                    $cat .= '<category label=\'' . $row['ten_datum'] . '\' />';
                    $dataset1 .= '<set value=\'' . $ten_q . '\' />';
                    $dataset2 .= '<set value=\'' . $ten_ext_q . '\' />';
                }
                
                // debug
                $debugLogManager->endGroup();
            }
            break;
        
        case 'Realtime':
            switch ($mandant) {
                case 'intone':
                case 'temp_mandant':
                    include('../db_optdb_connect.inc.php');
                    break;
                
                case 'pepperos':
                    include('../db_pep_optdb_connect.inc.php');
                    break;
            }
            
            $partner = $_REQUEST['import_partner'];
            if ($partner == 'planet49') {
                $partner = '102';
            }
            
            $dataArray = getRowsDataArray(
                array(
                    'SELECT' => '*',
                    'FROM' => '`Report`',
                    'WHERE' => '`pid` = "' . $partner . '" AND MONTH(`zeitraum`) = ' . intval($_REQUEST['import_monat']) 
                        . ' AND YEAR(`zeitraum`) = ' . intval($_REQUEST['import_jahr'])
                ),
                __FUNCTION__ . ' -> getReportDataArray',
                $verbindung_optdb,
                $debugLogManager
            );
            if (is_array($dataArray)) {
                // debug
                $debugLogManager->beginGroup(__FUNCTION__ . ' dataArray: ' . $genart);
                
                foreach ($dataArray as $row) {
                    // debug
                    $debugLogManager->logData('row', $row);
                    
                    $oz_quote = round((($row['netto'] / $row['brutto']) * 100), 1);
                    $oz_quote_ext = round((($row['netto_ext'] / $row['brutto']) * 100), 1);

                    $data .= '{Lieferdatum: \'' . $row['zeitraum'] . '\', Brutto:' . $row['brutto'] . ', Netto:' . $row['netto'] . ', Quote_: ' . $oz_quote . ',' . 
                        ' Quote: \'' . $oz_quote . '%\', Quote_e: ' . $oz_quote_ext . ', Quote_EXT: \'' . $oz_quote_ext . '%\'},
                    ';
                    $cat .= '<category  label=\'' . $row['zeitraum'] . '\' />';
                    $dataset1 .= '<set value=\'' . $row['brutto'] . '\' />';
                    $dataset2 .= '<set value=\'' . $row['netto'] . '\' />';
                    $dataset3 .= '<set value=\'' . $row['netto_ext'] . '\' />';
                }
                
                // debug
                $debugLogManager->endGroup();
            }
            break;
    }

    $dataset1 .= '</dataset>';
    $dataset2 .= '</dataset>';
    $cat .= '</categories>';
    $dataChart .= $cat . $dataset1 . $dataset2 . 
        '<definition>' . 
            '<style name=\'bgAnim\' type=\'animation\' param=\'_xScale\' start=\'0\' duration=\'1\' />' . 
        '</definition></chart>
    ';

    $data = substr($data, 0, -1);
    
    // debug
    $debugLogManager->beginGroup(__FUNCTION__);
    $debugLogManager->logData('data', $data);
    $debugLogManager->logData('dataChart', $dataChart);
    $debugLogManager->endGroup();

    $_SESSION['statistik']['chartType'] = 'ScrollLine2D';
    $_SESSION['statistik']['analyseTyp'] = 'history';
    $_SESSION['statistik']['datasource'] = '
        <script type="text/javascript">
            YAHOO.example.datensaetze = [' . $data . '];

            var opinionData = new YAHOO.util.DataSource(YAHOO.example.datensaetze);
            opinionData.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            opinionData.responseSchema = {
                fields: ["Lieferdatum", "Brutto", "Netto", "Quote_", "Quote", "Quote_e", "Quote_EXT"]
            };
            
            var columns = [
                {key: "Lieferdatum", sortable: true, resizeable: true, width:48},
                {key: "Brutto", sortable: true, resizeable: true, width:30},
                {key: "Netto", sortable: true, resizeable: true, width:30},
                {key: "Quote", label: "Quote",  resizeable: true, width:23},
                {key: "Quote_EXT", label: "EXT",  resizeable: true, width:23}
            ];
            
            var mytable = new YAHOO.widget.DataTable(
                "datatable",
                columns,
                opinionData
            );
	</script>
    ';
    $_SESSION['statistik']['dataChart'] = $dataChart;
}

function datensaetze_analyse($genart, $subCaption, $debugLogManager) {
    global $mandant;
    
    $dataChart = '<chart caption=\'Vollst&auml;ndigkeit der Spalten\' ' . $_SESSION['statistik']['formatNumber'] . ' ' . $_SESSION['statistik']['tausendSep'] . 
        ' subcaption=\'' . $subCaption . '\' showValues=\'0\' useRoundEdges=\'1\' rotateLabels=\'1\' decimals=\'3\' showPercentValues=\'1\'>
    ';
    $cat = '<categories>';
    $dataset1 = '<dataset>';
    $dataset2 = '<dataset>';
    
    $data = null;
    $trHinweis2 = null;
    $trVereinbarung = null;
    $dataTr3 = null;

    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            $ozDataArray = array(
                'ANREDE' => $felderMetaDB['ANREDE'],
                'VORNAME' => $felderMetaDB['VORNAME'],
                'NACHNAME' => $felderMetaDB['NACHNAME'],
                'STRASSE' => $felderMetaDB['STRASSE'],
                'PLZ' => $felderMetaDB['PLZ'],
                'ORT' => $felderMetaDB['ORT'],
                'LAND' => $felderMetaDB['LAND'],
                'GEBURTSDATUM' => $felderMetaDB['GEBURTSDATUM'],
                'TELEFON' => $felderMetaDB['TELEFON'],
                'EINTRAGSDATUM' => $felderMetaDB['EINTRAGSDATUM'],
                'IP' => $felderMetaDB['IP'],
                'HERKUNFT' => $felderMetaDB['HERKUNFT']
            );
            
            // debug
            $debugLogManager->logData('ozDataArray', $ozDataArray);
            $debugLogManager->beginGroup(__FUNCTION__ . ' ozDataArray: ' . $genart);
            
            $i = 1;
            $jx = 1;
            foreach ($ozDataArray as $key => $ozItem) {
                if ($i === 3) {
                    $i = 1;
                }
                
                $sql = ' AND `' . $ozItem . '` != ""';
                if ($jx === 1) {
                    $sql = ' AND `' . $ozItem . '` >= 0';
                }
                $jx++;
                
                $ozItemCount = getCountRows(
                    array(
                        'FIELD' => '`' . $ozItem . '`',
                        'FROM' => 'stats_' . $_SESSION['benutzer_name'],
                        'WHERE' => '`' . $felderPartnerDB['PID'] . '` = "' . $_REQUEST['import_partner'] . '"' 
                            . $sql
                    ),
                    __FUNCTION__ . ' -> Count: ' . $ozItem,
                    $verbindung,
                    $debugLogManager
                );
                if (!is_null($ozItemCount)) {
                    $oz_quote = round((($ozItemCount / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                    
                    $data .= '{Feld: \'' . $ozItem . '\', Anzahl:' . $ozItemCount . ', ohne:' . ($_SESSION['statistik']['brutto_analyse'] - $ozItemCount) . ', Quote: \'' . $oz_quote . '%\'},';
                    
                    $cat .= '<category label=\'' . $ozItem . '\' />';
                    $dataset1 .= '<set value=\'' . $ozItemCount . '\' />';
                    $dataset2 .= '<set value=\'' . ($_SESSION['statistik']['brutto_analyse'] - $ozItemCount) . '\' />';
                    
                    switch ($key) {
                        case 'EINTRAGSDATUM':
                            if ($oz_quote <= 99) {
                                $trHinweis2 .= '
                                    <tr>
                                        <td><img src="img/icons/achtung.png" width="15" /></td>
                                        <td><span style="color:red;">\'Eintragsdatum\'</span> ist leer oder unvollst&auml;ndig</td>
                                    </tr>
                                ';
                            }
                            
                            $ozItem = 'EINTRAGS.DAT';
                            break;
                            
                        case 'IP':
                            if ($oz_quote <= 99) {
                                $trHinweis2 .= '
                                    <tr>
                                        <td><img src="img/icons/achtung.png" width="15" /></td>
                                        <td><span style="color:red;">\'IP\'</span> ist leer oder unvollst&auml;ndig</td>
                                    </tr>
                                ';
                            }
                            break;
                        
                        case 'HERKUNFT':
                            if ($oz_quote <= 99) {
                                $trHinweis2 .= '
                                    <tr>
                                        <td><img src="img/icons/achtung.png" width="15" /></td>
                                        <td><span style="color:red;">\'Herkunft\'</span> ist leer oder unvollst&auml;ndig</td>
                                    </tr>
                                ';
                            }
                            break;
                        
                        case 'GEBURTSDATUM':
                            $ozItem = 'GEB.DAT';
                            break;
                    }
                    
                    if (strpos($_SESSION['statistik']['vereinbarung'], $ozItem) !== false) {
                        if ($oz_quote <= 95) {
                            $trVereinbarung .= '
                                <tr>
                                    <td><img src="img/icons/achtung.png" width="12" /></td>
                                    <td><span style="color:red;">\'' . $ozItem . '\'</span> wurde nicht oder nur teilweise geliefert</td>
                                </tr>
                            ';
                        } elseif ($oz_quote > 95) {
                            $trVereinbarung .= '
                                <tr>
                                    <td><img src="img/icons/accept.png" width="12"/></td>
                                    <td>\'' . $ozItem . '\' wurde geliefert</td>
                                </tr>
                            ';
                        }
                    }
                    
                    $dataTr3 .= '
                        <tr>
                            <td width="85" class="key_bg' . $i . '">' . $ozItem . '</td>
                    ';
                    if ($ozItem == 'EINTRAGS.DAT') {
                        $ozItem = $felderMetaDB['EINTRAGSDATUM'];
                    }
                    
                    if ($ozItem == 'GEB.DAT') {
                        $ozItem = $felderMetaDB['GEBURTSDATUM'];
                    }
                    
                    $dataTr3 .= '
                            <td class="stats_bg' . $i . '" width="80">' . zahl_formatieren($ozItemCount) . '</td>
                            <td width="20" class="stats_bg' . $i . '">' . $oz_quote . '%</td>
                        </tr>
                    ';
                    
                    $i++;
                }
            }
            
            // debug
            $debugLogManager->logData('trVereinbarung', $trVereinbarung);
            $debugLogManager->logData('dataTr3', $dataTr3);
            $debugLogManager->endGroup();
            
            $lz = getCountRows(
                array(
                    'FIELD' => '*',
                    'FROM' => 'stats_' . $_SESSION['benutzer_name'],
                    'WHERE' => '`' . $felderPartnerDB['PID'] . '` = "' . $_REQUEST['import_partner'] . '" AND `' . $felderMetaDB['VORNAME'] . '` != ""' . 
                        ' AND `' . $felderMetaDB['NACHNAME'] . '` != "" AND `' . $felderMetaDB['STRASSE'] . '` != ""' . 
                        ' AND `' . $felderMetaDB['PLZ'] . '` != "" AND `' . $felderMetaDB['ORT'] . '` != ""'
                ),
                __FUNCTION__ . ' -> Count: langDatansatz',
                $verbindung,
                $debugLogManager
            );
            if (!is_null($lz)) {
                $lz_quote = round((($lz / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                
                $kz = $_SESSION['statistik']['brutto_analyse'] - $lz;
                $kz_quote = round((($kz / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
            } else {
                $lz = 0;
                $lz_quote = 0;
                
                $kz = 0;
                $kz_quote = 0;
            }
            
            // debug
            $debugLogManager->beginGroup(__FUNCTION__ . ' ' . $genart . ' - settings');
            $debugLogManager->logData('lz', $lz);
            $debugLogManager->logData('lz_quote', $lz_quote);
            $debugLogManager->logData('kz', $kz);
            $debugLogManager->logData('kz_quote', $kz_quote);
            $debugLogManager->endGroup();
            break;
        
        case 'Realtime':
            switch ($mandant) {
                case 'intone':
                case 'temp_mandant':
                    include('../db_optdb_connect.inc.php');
                    break;
                
                case 'pepperos':
                    include('../db_pep_optdb_connect.inc.php');
                    break;
            }
            
            $ozDataArray = array(
                'Anrede' => 'Anrede',
                'Vorname' => 'Vorname',
                'Nachname' => 'Nachname',
                'Strasse' => 'Strasse',
                'Plz' => 'Plz',
                'Ort' => 'Ort',
                'Land' => 'Land',
                'Geburtsdatum' => 'Geburtsdatum',
                'Telefon' => 'Telefon',
                'Anmeldung' => 'Anmeldung',
                'Herkunft' => 'Herkunft',
                'Herkunft_IP' => 'Herkunft_IP'
            );
            
            // debug
            $debugLogManager->logData('ozDataArray', $ozDataArray);
            $debugLogManager->beginGroup(__FUNCTION__ . ' ozDataArray: ' . $genart);
            
            $i = 1;
            foreach ($ozDataArray as $key => $ozItem) {
                if ($i === 3) {
                   $i = 1;
                }
                
                $ozItemCount = getCountRows(
                    array(
                        'FIELD' => '`' . $ozItem . '`',
                        'FROM' => 'stats_' . $_SESSION['benutzer_name'],
                        'WHERE' => '`partner` = "' . $_REQUEST['import_partner'] . '" AND `' . $ozItem . '` != ""'
                    ),
                    __FUNCTION__ . ' -> Count: ' . $ozItem,
                    $verbindung_optdb,
                    $debugLogManager
                );
                if (!is_null($ozItemCount)) {
                    $oz_quote = round((($ozItemCount / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                    
                    $data .= '{Feld: \'' . $ozItem . '\', Anzahl:' . $ozItemCount . ', ohne:' . ($_SESSION['statistik']['brutto_analyse'] - $ozItemCount) . ', Quote: \'' . $oz_quote . '%\'},';
                    
                    $cat .= '<category label=\'' . $ozItem . '\' />';
                    $dataset1 .= '<set value=\'' . $ozItemCount . '\' />';
                    $dataset2 .= '<set value=\'' . ($_SESSION['statistik']['brutto_analyse'] - $ozItemCount) . '\' />';
                    
                    switch ($key) {
                        case 'Anmeldung':
                            if ($oz_quote <= 99) {
                                $trHinweis2 .= '
                                    <tr>
                                        <td><img src="img/icons/achtung.png" width="15" /></td>
                                        <td><span style="color:red;">\'Eintragsdatum\'</span> ist leer oder unvollst&auml;ndig</td>
                                    </tr>
                                ';
                            }
                            break;
                        
                        case 'Herkunft_IP':
                            if ($oz_quote <= 99) {
                                $trHinweis2 .= '
                                    <tr>
                                        <td><img src="img/icons/achtung.png" width="15" /></td>
                                        <td><span style="color:red;">\'IP\'</span> ist leer oder unvollst&auml;ndig</td>
                                    </tr>
                                ';
                            }
                            break;

                        case 'Herkunft':
                            if ($oz_quote <= 99) {
                                $trHinweis2 .= '
                                    <tr>
                                        <td><img src="img/icons/achtung.png" width="15" /></td>
                                        <td><span style="color:red;">\'Herkunft\'</span> ist leer oder unvollst&auml;ndig</td>
                                    </tr>
                                ';
                            }
                            break;
                    }
                    
                    if (strpos($_SESSION['statistik']['vereinbarung'], $ozItem) !== false) {
                        if ($oz_quote <= 95) {
                            $trVereinbarung .= '
                                <tr>
                                    <td><img src="img/icons/achtung.png" width="12" /></td>
                                    <td><span style="color:red;">\'' . $ozItem . '\'</span> wurde nicht oder nur teilweise geliefert</td>
                                </tr>
                            ';
                        }
                        
                        if ($oz_quote > 95) {
                            $trVereinbarung .= '
                                <tr>
                                    <td><img src="img/icons/accept.png" width="12" /></td>
                                    <td>\'' . $ozItem . '\' wurde geliefert</td>
                                </tr>
                            ';
                        }
                    }
                    
                    $dataTr3 .= '
                        <tr>
                            <td width="85" class="key_bg' . $i . '">' . $ozItem . '</td>
                            <td class="stats_bg' . $i . '" width="80">' . zahl_formatieren($ozItemCount) . '</td>
                            <td width="20" class=2stats_bg' . $i . '">' . $oz_quote . '%</td>
                        </tr>
                    ';
                    
                    $i++;
                }
            }
            
            // debug
            $debugLogManager->logData('trVereinbarung', $trVereinbarung);
            $debugLogManager->logData('dataTr3', $dataTr3);
            $debugLogManager->endGroup();
            
            $lz = getCountRows(
                array(
                    'FIELD' => '*',
                    'FROM' => 'stats_' . $_SESSION['benutzer_name'],
                    'WHERE' => '`partner` = "' . $_REQUEST['import_partner'] . '" AND `Vorname` != "" AND `Nachname` != ""' 
                        . ' AND `Strasse` != "" AND `Plz` != "" AND `Ort` != ""'
                ),
                __FUNCTION__ . ' -> Count: langDatansatz',
                $verbindung_optdb,
                $debugLogManager
            );
            if (!is_null($lz)) {
                $lz_quote = round((($lz / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                
                $kz = $_SESSION['statistik']['brutto_analyse'] - $lz;
                $kz_quote = round((($kz / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
            } else {
                $lz = 0;
                $lz_quote = 0;
                
                $kz = 0;
                $kz_quote = 0;
            }
            
            // debug
            $debugLogManager->beginGroup(__FUNCTION__ . ' ' . $genart . ' - settings');
            $debugLogManager->logData('lz', $lz);
            $debugLogManager->logData('lz_quote', $lz_quote);
            $debugLogManager->logData('kz', $kz);
            $debugLogManager->logData('kz_quote', $kz_quote);
            $debugLogManager->endGroup();
            break;
    }
    
    $data .= '{Feld: \'[Kurzdatensaetze]\', Anzahl:' . $kz . ', ohne:' . ($_SESSION['statistik']['brutto_analyse'] - $kz) . ', Quote: \'' . $kz_quote . '%\'},' . 
        '{Feld: \'[Langdatensaetze]\', Anzahl:' . $lz . ', ohne:' . ($_SESSION['statistik']['brutto_analyse'] - $lz) . ', Quote: \'' . $lz_quote . '%\'},
    ';
    
    $dataTr3 .= '
        <tr>
            <td width="85" class="key_bg1">[Kurzdatens&auml;tze]</td>
            <td class="stats_bg1" width="80">' . zahl_formatieren($kz) . '</td>
            <td width="20" class="stats_bg1">' . $kz_quote . '%</td>
        </tr>
        <tr>
            <td width="85" class="key_bg2">[Langdatens&auml;tze]</td>
            <td class="stats_bg2" width="80">' . zahl_formatieren($lz) . '</td>
            <td width="20" class="stats_bg2">' . $lz_quote . '%</td>
        </tr>
    ';
    
    $cat .= '<category label=\'[Kurzdatens&auml;tze]\' />' . 
        '<category label=\'[Langdatens&auml;tze]\' />
    ';
            
    $dataset1 .= '<set value=\'' . $kz . '\' />' . 
        '<set value=\'' . $lz . '\' />'
    ;
            
    $dataset2 .= '<set value=\'' . ($_SESSION['statistik']['brutto_analyse'] - $kz) . '\' />' . 
        '<set value=\'' . ($_SESSION['statistik']['brutto_analyse'] - $lz) . '\' />'
    ;
    
    $dataset1 .= '</dataset>';
    $dataset2 .= '</dataset>';
    $cat .= '</categories>';
    
    $dataChart .= $cat . $dataset1 . $dataset2 . '</chart>';
    $dataDatensaetze = $cat . $dataset1 . $dataset2 . '</chart>';

    $data = substr($data, 0, -1);
    
    // debug
    $debugLogManager->beginGroup(__FUNCTION__);
    $debugLogManager->logData('data', $data);
    $debugLogManager->logData('dataChart', $dataChart);
    $debugLogManager->endGroup();
    
    $_SESSION['statistik']['chartType'] = 'StackedBar3D';
    $_SESSION['statistik']['analyseTyp'] = 'chart';
    $_SESSION['statistik']['datasource'] = '
        <script type="text/javascript">
            YAHOO.example.datensaetze = [' . $data . '];

            var salesData = new YAHOO.util.DataSource( YAHOO.example.datensaetze );
            salesData.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            salesData.responseSchema = {
                fields: ["Feld", "Anzahl", "ohne", "Quote"]
            };
            
            var columns = [
                {key: "Feld", sortable: true, resizeable: true},
                {key: "Anzahl", sortable: true, resizeable: true},
                {key: "Quote",  resizeable: true}
            ];
            
            var mytable = new YAHOO.widget.DataTable(
                "datatable",
                columns,
                salesData,
                {
                    sortedBy: {key: "Feld"}
                }
            );
	</script>
    ';
    
    $_SESSION['statistik']['trHinweis2'] = $trHinweis2;
    $_SESSION['statistik']['trVereinbarung'] = $trVereinbarung;
    $_SESSION['statistik']['dataTr3'] = $dataTr3;
    $_SESSION['statistik']['dataChart'] = $dataChart;
    $_SESSION['statistik']['dataDatensaetze'] = $dataDatensaetze;
}

function land_analyse($genart, $subCaption, $debugLogManager) {
    global $mandant;

    $countrieDataArray = array();
    $totalCount = 0;
    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            $importCountryDataArray = getRowsDataArray(
                array(
                    'SELECT' => $felderMetaDB['LAND'] . ' as `country`',
                    'FROM' => 'stats_' . $_SESSION['benutzer_name'],
                    'WHERE' => '`' . $felderMetaDB['LAND'] . '` <> ""',
                    'GROUP_BY' => $felderMetaDB['LAND']
                ),
                __FUNCTION__,
                $verbindung,
                $debugLogManager
            );
            $debugLogManager->logData(__FUNCTION__ . ' ' . $genart, $importCountryDataArray);
            
            foreach ($importCountryDataArray as $item) {
                $countrieDataArray[$item['country']]['count'] = getCountRows(
                    array(
                        'FIELD' => '*',
                        'FROM' => 'stats_' . $_SESSION['benutzer_name'],
                        'WHERE' => '`' . $felderPartnerDB['PID'] . '` = "' . mysql_real_escape_string($_REQUEST['import_partner'] ). '"' 
                            . ' AND `' . $felderMetaDB['LAND'] . '` = "' . mysql_real_escape_string($item['country']) . '"'
                    ),
                    __FUNCTION__ . ' -> Count: ' . $item['country'],
                    $verbindung,
                    $debugLogManager
                );
                
                $countrieDataArray[$item['country']]['quote'] = round((($countrieDataArray[$item['country']]['count'] / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                $totalCount += $countrieDataArray[$item['country']]['count'];
            }
            break;
        
        case 'Realtime':
            switch ($mandant) {
                case 'intone':
                case 'temp_mandant':
                    include('../db_optdb_connect.inc.php');
                    break;
                
                case 'pepperos':
                    include('../db_pep_optdb_connect.inc.php');
                    break;
            }
            
            $importCountryDataArray = getRowsDataArray(
                array(
                    'SELECT' => $felderMetaDB['LAND'] . ' as `country`',
                    'FROM' => 'stats_' . $_SESSION['benutzer_name'],
                    'WHERE' => '`' . $felderMetaDB['LAND'] . '` <> ""',
                    'GROUP_BY' => $felderMetaDB['LAND']
                ),
                __FUNCTION__,
                $verbindung_optdb,
                $debugLogManager
            );
            $debugLogManager->logData(__FUNCTION__ . ' ' . $genart, $importCountryDataArray);
            
            foreach ($importCountryDataArray as $item) {
                $countrieDataArray[$item['country']]['count'] = getCountRows(
                    array(
                        'FIELD' => '*',
                        'FROM' => 'stats_' . $_SESSION['benutzer_name'],
                        'WHERE' => '`Partner` = "' . $_REQUEST['import_partner'] . '"' 
                            . ' AND `LAND` = "' . mysql_real_escape_string($item['country']) . '"'
                    ),
                    __FUNCTION__ . ' -> Count: ' . $item['country'],
                    $verbindung_optdb,
                    $debugLogManager
                );
                
                $countrieDataArray[$item['country']]['quote'] = round((($countrieDataArray[$item['country']]['count'] / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                $totalCount += $countrieDataArray[$item['country']]['count'];
            }
            break;
    }
    unset($importCountryDataArray);
    
    // debug
    $debugLogManager->logData(__FUNCTION__ . ' ' . $genart . ' totalCount', $totalCount);
    
    $ohne_land = $_SESSION['statistik']['brutto_analyse'] - $totalCount;
    $ohne_quote = round((($ohne_land / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
    
    // debug
    $debugLogManager->logData(__FUNCTION__ . ' ' . $genart . ' noCountrieData', $ohne_land);
    $debugLogManager->logData(__FUNCTION__ . ' ' . $genart . ' noCountrieQuoteData', $ohne_quote);

    $dataChart = '<chart caption=\'Verteilung nach Land\' subcaption=\'' . $subCaption . '\'' . $_SESSION['statistik']['formatNumber'] . ' ' . 
            $_SESSION['statistik']['tausendSep'] . 'enableRotation=\'1\' showZeroPies=\'0\' decimals=\'1\' showPercentValues=\'1\' enableRotation=\'0\'' . 
            ' bgColor=\'FFFFFF\' bgAlpha=\'40,100\' bgRatio=\'0,100\' bgAngle=\'360\' showBorder=\'1\' startingAngle=\'70\'>
    ';
    
    $data = '';
    foreach ($countrieDataArray as $key => $item) {
        $dataChart .= '<set label=\'' . $key . '\' value=\'' . $item['count'] . '\' />';
        
        $data .= json_encode(
            array(
                'Land' => $key,
                'Anzahl' => intval($item['count']),
                'Quote' => $item['quote'] . '%'
            )
        ) . ',';
    }
    
    if ($ohne_land > 0) {
        $dataChart .= '<set label=\'ohne\' value=\'' . $ohne_land . '\' isSliced=\'1\' />';
        
        $data .= json_encode(
            array(
                'Land' => 'ohne',
                'Anzahl' => $ohne_land,
                'Quote' => $ohne_quote . '%'
            )
        );
    } else {
        $data = substr($data, 0, -1);
    }
    $dataChart .= '</chart>';
    
    
    // debug
    $debugLogManager->beginGroup(__FUNCTION__);
    $debugLogManager->logData('data', $data);
    $debugLogManager->logData('dataChart', $dataChart);
    $debugLogManager->endGroup();

    $_SESSION['statistik']['chartType'] = 'Pie3D';
    $_SESSION['statistik']['analyseTyp'] = 'land';
    $_SESSION['statistik']['datasource'] = '
        <script type="text/javascript">
            YAHOO.example.laender = [' . $data . '];
            
            var opinionData = new YAHOO.util.DataSource( YAHOO.example.laender );
            opinionData.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            opinionData.responseSchema = {
                fields: ["Land", "Anzahl", "Quote"]
            };
            
            var columns = [
                {key: "Land", sortable: true, resizeable: true},
                {key: "Anzahl", sortable: true, resizeable: true},
                {key: "Quote",  resizeable: true}
            ];
            
            var mytable = new YAHOO.widget.DataTable(
                "datatable",
                columns,
                opinionData,
                {
                    sortedBy: {key: "Land"}
                }
            );
        </script>
    ';
    $_SESSION['statistik']['dataChart'] = $dataChart;
}

function geschlecht_analyse($genart, $subCaption, $debugLogManager) {
    global $mandant;

    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            $countW = getCountRows(
                array(
                    'FIELD' => '*',
                    'FROM' => 'stats_' . $_SESSION['benutzer_name'],
                    'WHERE' => '`' . $felderPartnerDB['PID'] . '` = "' . $_REQUEST['import_partner'] . '"' . 
                        ' AND (`' . $felderMetaDB['ANREDE'] . '` = "0" OR `' . $felderMetaDB['ANREDE'] . '` = "f")'
                ),
                __FUNCTION__ . ' -> Count: Women',
                $verbindung,
                $debugLogManager
            );
            $quoteW = round((($countW / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
            
            $countM = getCountRows(
                array(
                    'FIELD' => '*',
                    'FROM' => 'stats_' . $_SESSION['benutzer_name'],
                    'WHERE' => '`' . $felderPartnerDB['PID'] . '` = "' . $_REQUEST['import_partner'] . '"' . 
                        ' AND (`' . $felderMetaDB['ANREDE'] . '` = "1" OR `' . $felderMetaDB['ANREDE'] . '` = "h" OR `' . $felderMetaDB['ANREDE'] . '` = "m")'
                ),
                __FUNCTION__ . ' -> Count: Men',
                $verbindung,
                $debugLogManager
            );
            $quoteM = round((($countM / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
            break;
        
        case 'Realtime':
            switch ($mandant) {
                case 'intone':
                case 'temp_mandant':
                    include('../db_optdb_connect.inc.php');
                    break;
                
                case 'pepperos':
                    include('../db_pep_optdb_connect.inc.php');
                    break;
            }
            
            $countW = getCountRows(
                array(
                    'FIELD' => '*',
                    'FROM' => 'stats_' . $_SESSION['benutzer_name'],
                    'WHERE' => '`Partner` = "' . $_REQUEST['import_partner'] . '" AND `Anrede` = "Frau"'
                ),
                __FUNCTION__ . ' -> Count: Women',
                $verbindung_optdb,
                $debugLogManager
            );
            $quoteW = round((($countW / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
            
            $countM = getCountRows(
                array(
                    'FIELD' => '*',
                    'FROM' => 'stats_' . $_SESSION['benutzer_name'],
                    'WHERE' => '`Partner` = "' . $_REQUEST['import_partner'] . '" AND `Anrede` = "Herr"'
                ),
                __FUNCTION__ . ' -> Count: Men',
                $verbindung_optdb,
                $debugLogManager
            );
            $quoteM = round((($countM / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
            break;
    }
    
    $ohne_sex = $_SESSION['statistik']['brutto_analyse'] - $countW - $countM;
    $ohne_quote = round((($ohne_sex / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
    
    // debug
    $debugLogManager->logData(__FUNCTION__ . ' ' . $genart, $ohne_sex);

    $dataChart = ' <chart caption=\'Verteilung nach Geschlecht\' subcaption=\'' . $subCaption . '\'' . $_SESSION['statistik']['formatNumber'] . ' ' . 
            $_SESSION['statistik']['tausendSep'] . ' enableRotation=\'1\' showZeroPies=\'0\' palette=\'3\' decimals=\'1\' showPercentValues=\'1\'' . 
            ' enableRotation=\'0\' bgColor=\'FFFFFF\' bgAlpha=\'40,100\' bgRatio=\'0,100\' bgAngle=\'360\' showBorder=\'1\' startingAngle=\'70\'>
        <set label=\'m&auml;nnlich\' value=\'' . $countM . '\' />
        <set label=\'weiblich\' value=\'' . $countW . '\' />
    ';

    if ($ohne_sex > 0) {
        $dataChart .= '<set label=\'ohne\' value=\'' . $ohne_sex . '\' isSliced=\'1\' />';
    }
    $dataChart .= '</chart>';
    
    $data = '{Geschlecht: "maennlich", Anzahl: ' . $countM . ', Quote: "' . $quoteM . '%"},
        {Geschlecht: "weiblich", Anzahl: ' . $countW . ', Quote: "' . $quoteW . '%"},
        {Geschlecht: "ohne", Anzahl: ' . $ohne_sex . ', Quote: "' . $ohne_quote . '%"}
    ';
    
    // debug
    $debugLogManager->beginGroup(__FUNCTION__);
    $debugLogManager->logData('data', $data);
    $debugLogManager->logData('dataChart', $dataChart);
    $debugLogManager->endGroup();

    $_SESSION['statistik']['chartType'] = 'Pie3D';
    $_SESSION['statistik']['analyseTyp'] = 'sex';
    $_SESSION['statistik']['datasource'] = '
        <script type="text/javascript">
            YAHOO.example.geschlecht = [' . $data . '];
            
            var opinionData2 = new YAHOO.util.DataSource(YAHOO.example.geschlecht);
            opinionData2.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            opinionData2.responseSchema = {
                fields: ["Geschlecht", "Anzahl", "Quote"]
            };
            
            var columns = [
                {key: "Geschlecht", sortable: true, resizeable: true},
                {key: "Anzahl", sortable: true, resizeable: true},
                {key: "Quote", resizeable: true}
            ];
            
            var mytable = new YAHOO.widget.DataTable(
                "datatable",
                columns,
                opinionData2,
                {
                    sortedBy: {key: "Geschlecht"}
                }
            );
        </script>
    ';
    $_SESSION['statistik']['dataChart'] = $dataChart;
}

function alter_analyse($genart, $subCaption, $debugLogManager) {
    global $mandant;

    $yearNow = date('Y');
    $b18 = $yearNow - 18;
    $b29 = $yearNow - 29;
    $b39 = $yearNow - 39;
    $b49 = $yearNow - 49;
    $b59 = $yearNow - 59;
    $ab19 = $yearNow - 19;
    $ab30 = $yearNow - 30;
    $ab40 = $yearNow - 40;
    $ab50 = $yearNow - 50;
    $ab60 = $yearNow - 60;

    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            $dataArray = getRowDataArray(
                array(
                    'SELECT' => '`' . $felderMetaDB['GEBURTSDATUM'] . '` as `fieldName`',
                    'FROM' => '`stats_' . $_SESSION['benutzer_name'] . '`',
                    'WHERE' => '`' . $felderPartnerDB['PID'] . '` = "' . $_REQUEST['import_partner'] . '" AND `' . $felderMetaDB['GEBURTSDATUM'] . '` != ""',
                    'LIMIT' => '1'
                ),
                __FUNCTION__ . ' -> getBirthdayDataArray',
                $verbindung,
                $debugLogManager
            );
            if (is_array($dataArray)) {
                $birthdayFormat = $dataArray['fieldName'];
                
                if (strpos($birthdayFormat, '-')) {
                    $birthdayFormat = explode('-', $birthdayFormat);
                    if (strlen($birthdayFormat[0]) == 4) {
                        $start = 'LEFT';
                    } else {
                        $start = 'RIGHT';
                    }
                }
                
                if (strpos($birthdayFormat, '.')) {
                    $birthdayFormat = explode('.', $birthdayFormat);
                    if (strlen($birthdayFormat[0]) == 4) {
                        $start = 'LEFT';
                    } else {
                        $start = 'RIGHT';
                    }
                }
                
                $tableFieldsArray = array(
                    'birthday' => $felderMetaDB['GEBURTSDATUM'],
                    'pid' => $felderPartnerDB['PID']
                );
                
                $countB18 = getStatsCountByBirthday(
                    $start,
                    $tableFieldsArray,
                    array(
                        'pid' => $_REQUEST['import_partner'],
                        'birthday_great' => $b18,
                        'birthday_thin' => $yearNow
                    ),
                    $verbindung,
                    $debugLogManager
                );
                $quoteB18 = round((($countB18 / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                
                $countB28 = getStatsCountByBirthday(
                    $start,
                    $tableFieldsArray,
                    array(
                        'pid' => $_REQUEST['import_partner'],
                        'birthday_great' => $b29,
                        'birthday_thin' => $ab19
                    ),
                    $verbindung,
                    $debugLogManager
                );
                $quoteB29 = round((($countB28 / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                
                $countB39 = getStatsCountByBirthday(
                    $start,
                    $tableFieldsArray,
                    array(
                        'pid' => $_REQUEST['import_partner'],
                        'birthday_great' => $b39,
                        'birthday_thin' => $ab30
                    ),
                    $verbindung,
                    $debugLogManager
                );
                $quoteB39 = round((($countB39 / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                
                $countB49 = getStatsCountByBirthday(
                    $start,
                    $tableFieldsArray,
                    array(
                        'pid' => $_REQUEST['import_partner'],
                        'birthday_great' => $b49,
                        'birthday_thin' => $ab40
                    ),
                    $verbindung,
                    $debugLogManager
                );
                $quoteB49 = round((($countB49 / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                
                $countB59 = getStatsCountByBirthday(
                    $start,
                    $tableFieldsArray,
                    array(
                        'pid' => $_REQUEST['import_partner'],
                        'birthday_great' => $b59,
                        'birthday_thin' => $ab50
                    ),
                    $verbindung,
                    $debugLogManager
                );
                $quoteB59 = round((($countB59 / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                
                $countAb60 = getStatsCountByBirthday(
                    $start,
                    $tableFieldsArray,
                    array(
                        'pid' => $_REQUEST['import_partner'],
                        'birthday_great' => '1900',
                        'birthday_thin' => $ab60
                    ),
                    $verbindung,
                    $debugLogManager
                );
                $quoteAb60 = round((($countAb60 / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
            } else {
                $countB18 = 0;
                $countB28 = 0;
                $countB39 = 0;
                $countB49 = 0;
                $countB59 = 0;
                $countAb60 = 0;
                
                $quoteB18 = 0;
                $quoteB29 = 0;
                $quoteB39 = 0;
                $quoteB49 = 0;
                $quoteB59 = 0;
                $quoteAb60 = 0;
            }
            break;
        
        case 'Realtime':
            switch ($mandant) {
                case 'intone':
                case 'temp_mandant':
                    include('../db_optdb_connect.inc.php');
                    break;
                
                case 'pepperos':
                    include('../db_pep_optdb_connect.inc.php');
                    break;
            }
            
            $dataArray = getRowDataArray(
                array(
                    'SELECT' => '`Geburtsdatum` as `fieldName`',
                    'FROM' => '`stats_' . $_SESSION['benutzer_name'] . '`',
                    'WHERE' => '`Partner` = "' . $_REQUEST['import_partner'] . '" AND `Geburtsdatum` != ""',
                    'LIMIT' => '1'
                ),
                __FUNCTION__ . ' -> getBirthdayDataArray',
                $verbindung_optdb,
                $debugLogManager
            );
            if (is_array($dataArray)) {
                $birthdayFormat = $dataArray['fieldName'];
                
                if (strpos($birthdayFormat, '-')) {
                    $birthdayFormat = explode('-', $birthdayFormat);
                    if (strlen($birthdayFormat[0]) == 4) {
                        $start = 'LEFT';
                    } else {
                        $start = 'RIGHT';
                    }
                }
                
                if (strpos($birthdayFormat, '.')) {
                    $birthdayFormat = explode('.', $birthdayFormat);
                    if (strlen($birthdayFormat[0]) == 4) {
                        $start = 'LEFT';
                    } else {
                        $start = 'RIGHT';
                    }
                }
                
                $tableFieldsArray = array(
                    'birthday' => 'Geburtsdatum',
                    'pid' => 'Partner'
                );
                
                $countB18 = getStatsCountByBirthday(
                    $start,
                    $tableFieldsArray,
                    array(
                        'pid' => $_REQUEST['import_partner'],
                        'birthday_great' => $b18,
                        'birthday_thin' => $yearNow
                    ),
                    $verbindung_optdb,
                    $debugLogManager
                );
                $quoteB18 = round((($countB18 / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                
                $countB28 = getStatsCountByBirthday(
                    $start,
                    $tableFieldsArray,
                    array(
                        'pid' => $_REQUEST['import_partner'],
                        'birthday_great' => $b29,
                        'birthday_thin' => $ab19
                    ),
                    $verbindung_optdb,
                    $debugLogManager
                );
                $quoteB29 = round((($countB28 / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                
                $countB39 = getStatsCountByBirthday(
                    $start,
                    $tableFieldsArray,
                    array(
                        'pid' => $_REQUEST['import_partner'],
                        'birthday_great' => $b39,
                        'birthday_thin' => $ab30
                    ),
                    $verbindung_optdb,
                    $debugLogManager
                );
                $quoteB39 = round((($countB39 / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                
                $countB49 = getStatsCountByBirthday(
                    $start,
                    $tableFieldsArray,
                    array(
                        'pid' => $_REQUEST['import_partner'],
                        'birthday_great' => $b49,
                        'birthday_thin' => $ab40
                    ),
                    $verbindung_optdb,
                    $debugLogManager
                );
                $quoteB49 = round((($countB49 / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                
                $countB59 = getStatsCountByBirthday(
                    $start,
                    $tableFieldsArray,
                    array(
                        'pid' => $_REQUEST['import_partner'],
                        'birthday_great' => $b59,
                        'birthday_thin' => $ab50
                    ),
                    $verbindung_optdb,
                    $debugLogManager
                );
                $quoteB59 = round((($countB59 / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                
                $countAb60 = getStatsCountByBirthday(
                    $start,
                    $tableFieldsArray,
                    array(
                        'pid' => $_REQUEST['import_partner'],
                        'birthday_great' => '1900',
                        'birthday_thin' => $ab60
                    ),
                    $verbindung_optdb,
                    $debugLogManager
                );
                $quoteAb60 = round((($countAb60 / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
            } else {
                $countB18 = 0;
                $countB28 = 0;
                $countB39 = 0;
                $countB49 = 0;
                $countB59 = 0;
                $countAb60 = 0;
                
                $quoteB18 = 0;
                $quoteB29 = 0;
                $quoteB39 = 0;
                $quoteB49 = 0;
                $quoteB59 = 0;
                $quoteAb60 = 0;
            }
            break;
    }
    
    $ohne_anzahl = $_SESSION['statistik']['brutto_analyse'] - $countB18 - $countB28 - $countB39 - $countB49 - $countB59 - $countAb60;
    $ohne_quote = round((($ohne_anzahl / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
    
    // debug
    $debugLogManager->logData(__FUNCTION__ . ' ohne_anzahl: ' . $genart, $ohne_anzahl);

    $dataChart = '<chart caption=\'Verteilung nach Alter\' subcaption=\'' . $subCaption . '\'' . $_SESSION['statistik']['formatNumber'] . ' ' . 
                $_SESSION['statistik']['tausendSep'] . ' enableRotation=\'1\' showZeroPies=\'0\' palette=\'3\' decimals=\'1\' showPercentValues=\'1\'' . 
                ' enableRotation=\'0\' bgColor=\'FFFFFF\' bgAlpha=\'40,100\' bgRatio=\'0,100\' bgAngle=\'360\' showBorder=\'1\' startingAngle=\'0\'>
            <set label=\'u18\' value=\'' . $countB18 . '\' />
            <set label=\'18-29\' value=\'' . $countB28 . '\' />
            <set label=\'30-39\' value=\'' . $countB39 . '\' />
            <set label=\'40-49\' value=\'' . $countB49 . '\' />
            <set label=\'50-59\' value=\'' . $countB59 . '\' />
            <set label=\'60+\' value=\'' . $countAb60 . '\' />
            <set label=\'ohne\' value=\'' . $ohne_anzahl . '\' />
        </chart>
    ';
    
    $data = '{Alter: "<18", Anzahl: ' . $countB18 . ', Quote: "' . $quoteB18 . '%"},
        {Alter: "18-29", Anzahl: ' . $countB28 . ', Quote: "' . $quoteB29 . '%"},
        {Alter: "30-39", Anzahl: ' . $countB39 . ', Quote: "' . $quoteB39 . '%"},
        {Alter: "40-49", Anzahl: ' . $countB49 . ', Quote: "' . $quoteB49 . '%"},
        {Alter: "50-59", Anzahl: ' . $countB59 . ', Quote: "' . $quoteB59 . '%"},
        {Alter: "60+", Anzahl: ' . $countAb60 . ', Quote: "' . $quoteAb60 . '%"},
        {Alter: "ohne", Anzahl: ' . $ohne_anzahl . ', Quote: "' . $ohne_quote . '%"}
    ';
    
    // debug
    $debugLogManager->beginGroup(__FUNCTION__);
    $debugLogManager->logData('data', $data);
    $debugLogManager->logData('dataChart', $dataChart);
    $debugLogManager->endGroup();

    $_SESSION['statistik']['chartType'] = 'Pie3D';
    $_SESSION['statistik']['analyseTyp'] = 'alter';
    $_SESSION['statistik']['datasource'] = '
        <script type="text/javascript">
            YAHOO.example.alter = [' . $data . '];
            
            var opinionData2 = new YAHOO.util.DataSource(YAHOO.example.alter);
            opinionData2.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            opinionData2.responseSchema = {
                fields: ["Alter", "Anzahl", "Quote"]
            };
            
            var columns = [
                {key: "Alter", sortable: true, resizeable: true},
                {key: "Anzahl", sortable: true, resizeable: true},
                {key: "Quote", resizeable: true}
            ];
            var mytable = new YAHOO.widget.DataTable(
                "datatable",
                columns,
                opinionData2,
                {
                    sortedBy: {key: "Alter"}
                }
            );
            /*
            var mychart = new YAHOO.widget.PieChart(
                "chart",
                opinionData2,
                {
                    dataField: "Anzahl",
                    categoryField: "Alter",
                    style: {
			padding: 20,
			legend: {
                            display: "right",
                            padding: 10,
                            spacing: 5,
                            font: {
                                family: "Arial",
                                size: 13
                            }
                        }
                    }
                }
            );
            */
        </script>
    ';
    $_SESSION['statistik']['dataChart'] = $dataChart;
}

function ueberschneidung_analyse_gesamt($genart, $subCaption, $debugLogManager) {
    global $mandant;
    
    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            if ((int) $_SESSION['testimport'] === 1) {
                $metaDB = $metaDB . '_test';
                $import_datei_db = $import_datei_db . '_test';
            }
            
            $dataArray = getCountRowsDataArrayByFieldName(
                '`p`.`' . $felderPartnerDB['Pkz'] . '`',
                array(
                    'FROM' => '`' . $metaDB . '` as `m`' 
                        . ' join `stats_' . $_SESSION['benutzer_name'] . '` as `s` on `m`.`' . $felderMetaDB['EMAIL'] . '` = `s`.`' . $felderMetaDB['EMAIL'] . '`' 
                        . ' join `' . $import_datei_db . '` as `i` on `m`.`' . $felderMetaDB['IMPORT_NR'] . '` = `i`.`IMPORT_NR2`' 
                        . ' join `' . $partner_db . '` as `p` on `p`.`' . $felderPartnerDB['PID'] . '` = `i`.`PARTNER`'
                    ,
                    'WHERE' => '`s`.`' . $felderMetaDB['ATTRIBUTE'] . '` = "m" AND `m`.`' . $felderMetaDB['ATTRIBUTE'] . '` != "m"' 
                        . ' AND `i`.`PARTNER` != "' . $_REQUEST['import_partner'] . '"',
                    'GROUP_BY' => '`fieldName`',
                    'ORDER_BY' => '`count` DESC'
                ),
                __FUNCTION__ . ' -> getDataArray',
                $verbindung,
                $debugLogManager
            );
            break;
        
        case 'Realtime':
            $partner = $_REQUEST['import_partner'];
            
            switch ($mandant) {
                case 'intone':
                case 'temp_mandant':
                    include('../db_optdb_connect.inc.php');
                    
                    $partner = $_REQUEST['import_partner'];
                    if ($partner == 'planet49') {
                        $partner = 102;
                    }
                    break;
                
                case 'pepperos':
                    include('../db_pep_optdb_connect.inc.php');
                    break;
            }
            
            dropAndCreateUserTempTable(
                $_SESSION['benutzer_name'],
                'Realtime',
                array(
                    'email' => 'Email',
                    'pid' => 'Partner',
                    'registration' => 'Anmeldung'
                ),
                array(
                    'pid' => $_REQUEST['import_partner'],
                    'month' => $_REQUEST['import_monat'],
                    'year' => $_REQUEST['import_jahr']
                ),
                $verbindung_optdb,
                $debugLogManager
            );
            
            $countBruttoTempTable = getCountRows(
                array(
                    'FIELD' => '*',
                    'FROM' => 'id_' . $_SESSION['benutzer_name']
                ),
                __FUNCTION__ . ' -> getCountBruttoTempTable',
                $verbindung_optdb,
                $debugLogManager
            );
            
            $dataArray = getCountRowsDataArrayByFieldName(
                '`p`.`Pkz`',
                array(
                    'FROM' => '`Users` as `u`' 
                        . ' join `stats_' . $_SESSION['benutzer_name'] . '` as `s` on `u`.`Email` = `s`.`Email`' 
                        . ' join `Partner` as `p` on `u`.`PID` = `p`.`PID`'
                    ,
                    'WHERE' => '`u`.`PID` != "' . $partner . '"',
                    'GROUP_BY' => '`u`.`PID`',
                    'ORDER_BY' => '`count` DESC'
                ),
                __FUNCTION__ . ' -> getDataArray',
                $verbindung_optdb,
                $debugLogManager
            );
            
            $dataArrayByDate = getCountRowDataArrayByFieldName(
                '`p`.`Pkz`',
                array(
                    'FROM' => '`Users` as `u`' 
                        . ' join `stats_' . $_SESSION['benutzer_name'] . '` as `s` on `u`.`Email` = `s`.`Email`' 
                        . ' join `Partner` as `p` on `u`.`PID` = `p`.`PID`'
                    ,
                    'WHERE' => '`u`.`PID` = "' . $partner . '" AND MONTH(`u`.`Anmeldung`) = "' . $_REQUEST['import_monat'] . '"' 
                        . ' AND YEAR(`u`.`Anmeldung`) = "' . $_REQUEST['import_jahr'] . '"',
                    'GROUP_BY' => '`u`.`PID`',
                    'ORDER_BY' => '`count` DESC'
                ),
                __FUNCTION__ . ' -> getDataArrayByDate',
                $verbindung_optdb,
                $debugLogManager
            );
            break;
    }

    $dataChart = '<chart caption=\'&Uuml;berschneidungen\' subcaption=\'' . $subCaption . '\'' . $_SESSION['statistik']['formatNumber'] . ' ' . 
        $_SESSION['statistik']['tausendSep'] . ' enableRotation=\'1\' palette=\'3\' decimals=\'1\' showPercentValues=\'1\' enableSmartLabels=\'1\'' . 
        ' enableRotation=\'0\' bgColor=\'FFFFFF\' bgAlpha=\'40,100\' bgRatio=\'0,100\' bgAngle=\'360\' showBorder=\'1\' startingAngle=\'70\'>
    ';
    
    $data = null;
    $dsBruttoNew = 0;

    if (is_array($dataArray)) {
        // debug
        $debugLogManager->beginGroup(__FUNCTION__ . ' dataArray: ' . $genart);
        
        foreach ($dataArray as $row) {
            // debug
            $debugLogManager->logData('row', $row);
            
            $quote = round((($row['count'] / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
            
            $data .= '{ Partner: \'' . $row['fieldName'] . '\', Anzahl:' . $row['count'] . ', Quote: \'' . $quote . '%\'},';
            $dataChart .= '<set label=\'' . $row['fieldName'] . '\' value=\'' . $row['count'] . '\' />';
            
            $dsBruttoNew += $row['count'];
        }
        
        // debug
        $debugLogManager->endGroup();
    }

    $ohne = $_SESSION['statistik']['brutto_analyse'] - $dsBruttoNew;

    if ($genart == 'Realtime') {
        if (is_array($dataArrayByDate)) {
            // debug
            $debugLogManager->logData(__FUNCTION__ . ': dataArrayByDate', $dataArrayByDate);

            $anzahl2 = $ohne - $dataArrayByDate['count'];
            $quote = round((($anzahl2 / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
            
            $data .= '{Partner: \'' . $dataArrayByDate['fieldName'] . '\', Anzahl:' . $anzahl2 . ', Quote: \'' . $quote . '%\'},';
            $dataChart .= '<set label=\'' . $dataArrayByDate['fieldName'] . '\' value=\'' . $anzahl2 . '\' />';
            
            $dsBruttoNew += $anzahl2;
        }
    }

    $ohne = $_SESSION['statistik']['brutto_analyse'] - $dsBruttoNew;
    $quote_ohne = round((($ohne / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
    
    $data .= '{Partner: \'' . ucfirst($mandant) . '\', Anzahl:' . $ohne . ', Quote: \'' . $quote_ohne . '%\'}';
    $dataChart .= '<set label=\'' . ucfirst($mandant) . '\' value=\'' . $ohne . '\' isSliced=\'1\' /></chart>';
    
    // debug
    $debugLogManager->beginGroup(__FUNCTION__);
    $debugLogManager->logData('data', $data);
    $debugLogManager->logData('dataChart', $dataChart);
    $debugLogManager->endGroup();

    $_SESSION['statistik']['chartType'] = 'Pie3D';
    $_SESSION['statistik']['analyseTyp'] = 'ueberschneidung';
    $_SESSION['statistik']['datasource'] = '
        <script type="text/javascript">
            YAHOO.example.ueberschneidung = [' . $data . '];

            var opinionData = new YAHOO.util.DataSource(YAHOO.example.ueberschneidung);
            opinionData.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            opinionData.responseSchema = {
                fields: ["Partner", "Anzahl", "Quote"]
            };
            
            var columns = [
                {key: "Partner", sortable: true, resizeable: true},
                {key: "Anzahl", sortable: true, resizeable: true},
                {key: "Quote", resizeable: true}
            ];
            var mytable = new YAHOO.widget.DataTable(
                "datatable",
                columns,
                opinionData,
                {
                    sortedBy: {key: "Partner"}
                }
            );
	</script>
    ';
    $_SESSION['statistik']['dataChart'] = $dataChart;
}

function herkunft_analyse($genart, $subCaption, $debugLogManager) {
    global $mandant;
    
    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            $dataArray = getCountRowsDataArrayByFieldName(
                '`HERKUNFT_NAME`',
                array(
                    'FROM' => '`stats_' . $_SESSION['benutzer_name'] . '`',
                    'WHERE' => '`' . $felderPartnerDB['PID'] . '` = "' . $_REQUEST['import_partner'] . '"',
                    'GROUP_BY' => '`fieldName`',
                    'ORDER_BY' => '`count` DESC'
                ),
                __FUNCTION__ . ' -> getDataArray',
                $verbindung,
                $debugLogManager
            );
            break;
        
        case 'Realtime':
            include('../db_optdb_connect.inc.php');
            
            $dataArray = getCountRowsDataArrayByFieldName(
                '`Herkunft`',
                array(
                    'FROM' => '`stats_' . $_SESSION['benutzer_name'] . '`',
                    'WHERE' => '`Partner` = "' . $_REQUEST['import_partner'] . '"',
                    'GROUP_BY' => '`fieldName`',
                    'ORDER_BY' => '`count` DESC'
                ),
                __FUNCTION__ . ' > getDataArray',
                $verbindung_optdb,
                $debugLogManager
            );
            break;
    }

    $dataChart = '<chart caption=\'Verteilung nach Herkunft\' subcaption=\'' . $subCaption . '\'' . $_SESSION['statistik']['formatNumber'] . ' ' . 
        $_SESSION['statistik']['tausendSep'] . ' enableRotation=\'1\' showZeroPies=\'0\' palette=\'3\' decimals=\'1\' showPercentValues=\'1\'' . 
        ' enableRotation=\'0\' bgColor=\'FFFFFF\' bgAlpha=\'40,100\' bgRatio=\'0,100\' bgAngle=\'360\' showBorder=\'1\' startingAngle=\'120\'>
    ';
    
    $data = null;

    if (is_array($dataArray)) {
        // debug
        $debugLogManager->beginGroup(__FUNCTION__ . ' dataArray: ' . $genart);
        
        foreach ($dataArray as $row) {
            // debug
            $debugLogManager->logData('row', $row);
            
            $herkunft = str_replace('www.', '', $row['fieldName']);
            $herkunft = str_replace('http://', '', $herkunft);

            $quote = round((($row['count'] / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
            if (strlen($herkunft) === 0 || $herkunft == 5) {
                $data .= '{Herkunft: \'leer\', Anzahl:\'' . $row['count'] . '\', Quote: \'' . $quote . '%\'},';
                $dataChart .= '<set label=\'leer\' value=\'' . $row['count'] . '\' isSliced=\'1\' />';
            } else {
                $data .= '{Herkunft: \'' . $herkunft . '\', Anzahl:\'' . $row['count'] . '\', Quote: \'' . $quote . '%\'},';
                $dataChart .= '<set label=\'' . $herkunft . '\' value=\'' . $row['count'] . '\' />';
            }
        }
        
        // debug
        $debugLogManager->endGroup();
    }
    
    $data = substr($data, 0, -1);
    $dataChart .= '</chart>';
    
    // debug
    $debugLogManager->beginGroup(__FUNCTION__);
    $debugLogManager->logData('data', $data);
    $debugLogManager->logData('dataChart', $dataChart);
    $debugLogManager->endGroup();
    
    $_SESSION['statistik']['chartType'] = 'Pie3D';
    $_SESSION['statistik']['analyseTyp'] = 'Herkunft';
    $_SESSION['statistik']['datasource'] = '
        <script type="text/javascript">
            YAHOO.example.domain = [' . $data . '];

            var opinionData = new YAHOO.util.DataSource(YAHOO.example.domain);
            opinionData.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            opinionData.responseSchema = {
                fields: ["Herkunft", "Anzahl", "Quote"]
            };
            
            var columns = [
                {key: "Herkunft", sortable: true, resizeable: true, width:120},
                {key: "Anzahl", sortable: true, resizeable: true},
                {key: "Quote", resizeable: true}
            ];
            var mytable = new YAHOO.widget.DataTable(
                "datatable",
                columns,
                opinionData,
                {
                    sortedBy: {key: "Herkunft"}
                }
            );
	</script>
    ';
    $_SESSION['statistik']['dataChart'] = $dataChart;
}

function anmeldung_analyse($genart, $subCaption, $debugLogManager) {
    global $mandant;
    
    $vereinbarungsDate = $_SESSION['statistik']['vereinbarung_datum'];
    $lieferdatum = mktime(23, 59, 59, $_REQUEST['import_monat'], 1, $_REQUEST['import_jahr']);
    if ($vereinbarungsDate != 'altdaten') {
        $vereinbarungsDate = $lieferdatum - 2678400;
    }

    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            $dataArray = getCountRowsDataArrayByFieldName(
                'DATE_FORMAT(`' . $felderMetaDB['EINTRAGSDATUM'] . '`, "%d-%m-%Y")',
                array(
                    'FROM' => '`stats_' . $_SESSION['benutzer_name'] . '`',
                    'WHERE' => '`' . $felderPartnerDB['PID'] . '` = "' . $_REQUEST['import_partner'] . '"',
                    'GROUP_BY' => '`fieldName`',
                    'ORDER_BY' => '`fieldName` DESC'
                ),
                __FUNCTION__ . ' -> getDataArray',
                $verbindung,
                $debugLogManager
            );
            break;
        
        case 'Realtime':
            include('../db_optdb_connect.inc.php');
            
            $dataArray = getCountRowsDataArrayByFieldName(
                'DATE_FORMAT(`Anmeldung`, "%d-%m-%Y")',
                array(
                    'FROM' => '`stats_' . $_SESSION['benutzer_name'] . '`',
                    'WHERE' => '`Partner` = "' . $_REQUEST['import_partner'] . '"',
                    'GROUP_BY' => '`fieldName`',
                    'ORDER_BY' => '`fieldName` DESC'
                ),
                __FUNCTION__ . ' -> getDataArray',
                $verbindung_optdb,
                $debugLogManager
            );
            break;
    }
    
    $dataChart = '<chart caption=\'Verteilung nach Tag der Anmeldung\' subcaption=\'' . $subCaption . '\'' . $_SESSION['statistik']['formatNumber'] . ' ' . 
        $_SESSION['statistik']['tausendSep'] . ' enableRotation=\'1\' showZeroPies=\'0\' palette=\'3\' decimals=\'1\' showPercentValues=\'1\'' . 
        ' enableRotation=\'0\' bgColor=\'FFFFFF\' bgAlpha=\'40,100\' bgRatio=\'0,100\' bgAngle=\'360\' showBorder=\'1\' startingAngle=\'120\'>
    ';
    
    $data = null;
    $alterHinweis = null;
    $dataTr8 = null;
    $dataDatenalterAll = null;
    
    if (is_array($dataArray)) {
        // debug
        $debugLogManager->beginGroup(__FUNCTION__ . ' dataArray: ' . $genart);
        
        $i = 1;
        foreach ($dataArray as $row) {
            // debug
            $debugLogManager->logData('row', $row);
            
            $quote = round((($row['count'] / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
            if (empty($row['fieldName'])) {
                $data .= '{Anmeldedatum: \'leer\', Anzahl:\'' . $row['count'] . '\', Quote: \'' . $quote . '%\'},';
                $dataChart .= '<set label=\'leer\' value=\'' . $row['count'] . '\' isSliced=\'1\' />';
                
                $alterHinweis = '
                    <tr>
                        <td><img src="img/icons/achtung.png" width="12" /></td>
                        <td><span style="color:red;">\'Anmeldedatum\'</span> ist leer oder zu alt</td>
                    </tr>
                ';
            } else {
                if ($i === 3) {
                    $i = 1;
                }
                
                $data .= '{Anmeldedatum: \'' . $row['fieldName'] . '\', Anzahl:\'' . $row['count'] . '\', Quote: \'' . $quote . '%\'},';
                $dataChart .= '<set label=\'' . $row['fieldName'] . '\' value=\'' . $row['count'] . '\' />';
                $dataDatenalterAll .= '<set label=\'' . $row['fieldName'] . '\' value=\'' . $row['count'] . '\' />';
                
                $dataTr8 .= '
                    <tr>
                        <td class="stats_bg' . $i . '" width="80">' . $row['fieldName'] . '</td>
                        <td class="stats_bg' . $i . '" width="80">' . zahl_formatieren($row['count']) . '</td>
                        <td width="20" class="stats_bg' . $row['fieldName'] . '">' . $quote . '%</td>
                    </tr>
                ';
                
                if (strpos($row['fieldName'], '-') !== false) {
                    $explodeDataArray = explode('-', $row['fieldName']);
                    $year = $explodeDataArray[0];
                    $month = $explodeDataArray[1];
                }
                
                if (strpos($row['fieldName'], '.') !== false) {
                    $explodeDataArray = explode('.', $row['fieldName']);
                    $year = $explodeDataArray[0];
                    $month = $explodeDataArray[1];
                }
                
                $time = mktime(23, 59, 59, $month, 1, $year);
                if ($time < $vereinbarungsDate) {
                    $alterHinweis = '
                        <tr>
                            <td><img src="img/icons/achtung.png" width="12" /></td>
                            <td><span style="color:red;">\'Anmeldedatum\'</span> ist leer oder zu alt</td>
                        </tr>
                    ';
                }
                $i++;
            }
        }
        
        // debug
        $debugLogManager->endGroup();
    }
    
    $dataChart .= '</chart>';
    $data = substr($data, 0, -1);
    
    // debug
    $debugLogManager->beginGroup(__FUNCTION__);
    $debugLogManager->logData('data', $data);
    $debugLogManager->logData('dataChart', $dataChart);
    $debugLogManager->endGroup();
    
    $_SESSION['statistik']['chartType'] = 'Pie3D';
    $_SESSION['statistik']['analyseTyp'] = 'Anmeldedatum';
    $_SESSION['statistik']['datasource'] = '
        <script type="text/javascript">
            YAHOO.example.domain = [' . $data . '];

            var opinionData = new YAHOO.util.DataSource(YAHOO.example.domain);
            opinionData.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            opinionData.responseSchema = {
                fields: ["Anmeldedatum", "Anzahl", "Quote"]
            };
            
            var columns = [
                {key: "Anmeldedatum", sortable: true, resizeable: true},
                {key: "Anzahl", sortable: true, resizeable: true},
                {key: "Quote", resizeable: true}
            ];
            var mytable = new YAHOO.widget.DataTable(
                "datatable",
                columns,
                opinionData,
                {
                    sortedBy: {key: "Anmeldedatum"}
                }
            );
	</script>
    ';
    $_SESSION['statistik']['dataChart'] = $dataChart;
    $_SESSION['statistik']['alterHinweis'] = $alterHinweis;
    $_SESSION['statistik']['dataTr8'] = $dataTr8;
    $_SESSION['statistik']['dataDatenalterAll'] = $dataDatenalterAll;
}

function qualitaet_analyse($genart, $subCaption, $debugLogManager) {
    global $mandant;
    
    $dataChart = '<chart caption=\'Reporting nach Bereinigungsart\' subcaption=\'' . $subCaption . '\'' . $_SESSION['statistik']['formatNumber'] . ' ' . 
        $_SESSION['statistik']['tausendSep'] . ' showZeroPies=\'0\' palette=\'3\' decimals=\'1\' showPercentValues=\'1\' enableRotation=\'1\'' . 
        ' animation=\'1\' bgColor=\'FFFFFF\' bgAlpha=\'40,100\' bgRatio=\'0,100\' bgAngle=\'360\' showBorder=\'1\' startingAngle=\'120\'>
    ';
    
    $data = null;
    
    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            $dataArray = getCountRowsDataArrayByFieldName(
                '`' . $felderMetaDB['ATTRIBUTE'] . '`',
                array(
                    'FROM' => '`stats_' . $_SESSION['benutzer_name'] . '`',
                    'WHERE' => '`' .  $felderPartnerDB['PID'] . '` = "' . $_REQUEST['import_partner'] . '"',
                    'GROUP_BY' => '`fieldName`',
                    'ORDER_BY' => '`count` DESC'
                ),
                __FUNCTION__ . ' -> dataArray',
                $verbindung,
                $debugLogManager
            );
            if (is_array($dataArray)) {
                // debug
                $debugLogManager->beginGroup(__FUNCTION__ . ' dataArray: ' . $genart);
                
                foreach ($dataArray as $row) {
                    // debug
                    $debugLogManager->logData('row', $row);
                    
                    $sliced = '';
                    switch ($row['fieldName']) {
                        case 'm':
                            $a = 'bereits in MetaDB';
                            break;
                        
                        case 'b':
                            $a = 'Blacklist';
                            break;
                        
                        case 'h':
                            $a = 'Hardbounce';
                            break;
                        
                        case 'd':
                            $a = 'Dublette';
                            break;
                        
                        case 'i':
                            $a = 'Dublette in Datei';
                            break;
                        
                        case 'f':
                            $a = 'Fake-Email';
                            break;
                        
                        default:
                            $a = 'Netto';
                            $sliced = ' isSliced=\'1\'';
                            break;
                    }
                    
                    $quote = round((($row['count'] / $_SESSION['statistik']['brutto_analyse']) * 100), 1);
                    
                    $data .= '{Typ: \'' . $a . '\', Anzahl:\'' . $row['count'] . '\', Quote: \'' . $quote . '%\'},';
                    $dataChart .= '<set label=\'' . $a . '\' value=\'' . $row['count'] . '\'' . $sliced . ' />';
                }
                
                // debug
                $debugLogManager->endGroup();
            }
            break;
        
        case 'Realtime':
            include('../db_optdb_connect.inc.php');
            
            $partner = $_REQUEST['import_partner'];
            if ($partner == 'planet49') {
                $partner = 102;
            }
            
            $dataArray = getRowsDataArray(
                array(
                    'SELECT' => '*',
                    'FROM' => '`Report`',
                    'WHERE' => '`pid` = "' . $partner . '" AND MONTH(`zeitraum`) = ' .  intval($_REQUEST['import_monat']) 
                    . ' AND YEAR(`zeitraum`) = ' . intval($_REQUEST['import_jahr'])
                ),
                __FUNCTION__ . ' -> dataArray',
                $verbindung_optdb,
                $debugLogManager
            );
            if (is_array($dataArray)) {
                // debug
                $debugLogManager->beginGroup(__FUNCTION__ . ' dataArray: ' . $genart);
                
                foreach ($dataArray as $row) {
                    // debug
                    $debugLogManager->logData('row', $row);
                    
                    #$_SESSION['statistik']['brutto_analyse']
                    $brutto_neu = $row['brutto'];
                    $netto = $row['netto'];

                    // debug
                    $debugLogManager->beginGroup('details - settings');
                    
                    $d_teile = explode('|', $row['details']);
                    foreach ($d_teile as $d) {
                        $sliced = 'isSliced=\'0\'';

                        if (strpos($d, 'Geliefert') !== false) {
                            $brutto = preg_replace('/\D/', '', $d);
                            $quote = round((($netto / $brutto) * 100), 1);
                            $dz_anzahl = $netto;
                            
                            $a = 'Netto';
                            $sliced = 'isSliced=\'1\'';
                            
                            // debug
                            $debugLogManager->beginGroup('Geliefert');
                            $debugLogManager->logData('brutto', $brutto);
                            $debugLogManager->logData('quote', $quote);
                            $debugLogManager->logData('dz_anzahl', $dz_anzahl);
                            $debugLogManager->logData('a', $a);
                            $debugLogManager->endGroup();
                        }

                        if (strpos($d, 'DELETED') !== false) {
                            $dz_anzahl = preg_replace('/\D/', '', $d);
                            $quote = round((($dz_anzahl / $brutto) * 100), 1);
                            
                            $a = 'gel&ouml;scht';
                            
                            // debug
                            $debugLogManager->beginGroup('DELETED');
                            $debugLogManager->logData('dz_anzahl', $dz_anzahl);
                            $debugLogManager->logData('quote', $quote);
                            $debugLogManager->logData('a', $a);
                            $debugLogManager->endGroup();
                        }

                        if (strpos($d, 'Blacklist') !== false) {
                            $dz_anzahl = preg_replace('/\D/', '', $d);
                            $quote = round((($dz_anzahl / $brutto) * 100), 1);
                            
                            $a = 'Blacklist';
                            
                            // debug
                            $debugLogManager->beginGroup('Blacklist');
                            $debugLogManager->logData('dz_anzahl', $dz_anzahl);
                            $debugLogManager->logData('quote', $quote);
                            $debugLogManager->logData('a', $a);
                            $debugLogManager->endGroup();
                        }

                        if (strpos($d, 'falsch') !== false) {
                            $dz_anzahl = preg_replace('/\D/', '', $d);
                            $quote = round((($dz_anzahl / $brutto) * 100), 1);
                            
                            $a = 'Fake-Email';
                            
                            // debug
                            $debugLogManager->beginGroup('Falsch');
                            $debugLogManager->logData('dz_anzahl', $dz_anzahl);
                            $debugLogManager->logData('quote', $quote);
                            $debugLogManager->logData('a', $a);
                            $debugLogManager->endGroup();
                        }
                        
                        $dz_ges += $dz_anzahl;
                        
                        $data .= '{Typ: \'' . $a . '\', Anzahl:\'' . $dz_anzahl . '\', Quote: \'' . $quote . '%\'},';
                        $dataChart .= '<set label=\'' . $a . '\' value=\'' . $dz_anzahl . '\'' . $sliced . ' />';
                    }
                    
                    // debug
                    $debugLogManager->endGroup();
                }
                
                // debug
                $debugLogManager->endGroup();
            }
            
            $m = $brutto_neu - $dz_ges;
            $quote_m = round((($m / $brutto_neu) * 100), 1);
            
            $data .= '{Typ: \'bereits in metaDB\', Anzahl:\'' . $m . '\', Quote: \'' . $quote_m . '%\'},';
            $dataChart .= '<set label=\'bereits in metaDB\' value=\'' . $m . '\'' . $sliced . ' />';
            break;
    }
    
    $dataChart .= '</chart>';
    $data = substr($data, 0, -1);
    
    // debug
    $debugLogManager->beginGroup(__FUNCTION__);
    $debugLogManager->logData('data', $data);
    $debugLogManager->logData('dataChart', $dataChart);
    $debugLogManager->endGroup();
    
    $_SESSION['statistik']['chartType'] = 'Pie3D';
    $_SESSION['statistik']['analyseTyp'] = 'Qualitaet';
    $_SESSION['statistik']['datasource'] = '
        <script type="text/javascript">
            YAHOO.example.domain = [' . $data . '];

            var opinionData = new YAHOO.util.DataSource(YAHOO.example.domain);
            opinionData.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            opinionData.responseSchema = {
                fields: ["Typ", "Anzahl", "Quote"]
            };
            
            var columns = [
                {key: "Typ", sortable: true, resizeable: true},
                {key: "Anzahl", sortable: true, resizeable: true},
                {key: "Quote", resizeable: true}
            ];
            var mytable = new YAHOO.widget.DataTable(
                "datatable",
                columns,
                opinionData,
                {
                    sortedBy: {key: "Typ"}
                }
            );
	</script>
    ';
    $_SESSION['statistik']['dataChart'] = $dataChart;
}

function ueberschneidung_analyse($genart, $subCaption, $debugLogManager) {
    global $mandant;
    
    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            dropAndCreateUserTempTable(
                $_SESSION['benutzer_name'],
                'Import',
                array(
                    'email' => $felderMetaDB['EMAIL'],
                    'pid' => $felderPartnerDB['PID']
                ),
                array(
                    'pid' => $_REQUEST['import_partner']
                ),
                $verbindung,
                $debugLogManager
            );
            
            $countBruttoTempTable = getCountRows(
                array(
                    'FIELD' => '*',
                    'FROM' => 'id_' . $_SESSION['benutzer_name'] . ''
                ),
                __FUNCTION__ . ' -> getCountBruttoTempTable',
                $verbindung,
                $debugLogManager
            );
            
            $uniqueTempTableDataArray = getCountRowsDataArrayByFieldName(
                '`' . $felderPartnerDB['Pkz'] . '`',
                array(
                    'FROM' => '`id_' . $_SESSION['benutzer_name'] . '` as `i`' 
                        . ' join `stats_' . $_SESSION['benutzer_name'] . '` as `s` on `i`.`' . $felderMetaDB['EMAIL'] . '` = `s`.`' . $felderMetaDB['EMAIL'] . '`'
                    ,
                    'WHERE' => '`s`.`' . $felderPartnerDB['PID'] . '` != "' . $_REQUEST['import_partner'] . '"',
                    'GROUP_BY' => '`fieldName`',
                ),
                __FUNCTION__ . ' -> getUniqueTempTableDataArray',
                $verbindung,
                $debugLogManager
            );
            break;
        
        case 'Realtime':
            switch ($mandant) {
                case 'intone':
                case 'temp_mandant':
                    include('../db_optdb_connect.inc.php');
                    break;
                
                case 'pepperos':
                    include('../db_pep_optdb_connect.inc.php');
                    break;
            }
            
            dropAndCreateUserTempTable(
                $_SESSION['benutzer_name'],
                'Realtime',
                array(
                    'email' => 'Email',
                    'pid' => 'Partner',
                    'registration' => 'Anmeldung'
                ),
                array(
                    'pid' => $_REQUEST['import_partner'],
                    'month' => $_REQUEST['import_monat'],
                    'year' => $_REQUEST['import_jahr']
                ),
                $verbindung_optdb,
                $debugLogManager
            );
            
            $countBruttoTempTable = getCountRows(
                array(
                    'FIELD' => '*',
                    'FROM' => 'id_' . $_SESSION['benutzer_name']
                ),
                __FUNCTION__ . ' -> getCountBruttoTempTable',
                $verbindung_optdb,
                $debugLogManager
            );
            
            $uniqueTempTableDataArray = getCountRowsDataArrayByFieldName(
                '`p`.`Pkz`',
                array(
                    'FROM' => '`id_' . $_SESSION['benutzer_name'] . '` as `i`' 
                        . ' join `stats_' . $_SESSION['benutzer_name'] . '` as `s` on `i`.`Email` = `s`.`Email`' 
                        . ' join `Partner` as `p` on `s`.`Partner` = `p`.`PID`'
                    ,
                    'WHERE' => '`h`.`Partner` != "' . $_REQUEST['import_partner'] . '"',
                    'GROUP_BY' => '`fieldName`',
                    'ORDER_BY' => '`count` DESC'
                ),
                __FUNCTION__ . ' -> getUniqueTempTableDataArray',
                $verbindung_optdb,
                $debugLogManager
            );
            break;
    }

    $dataChart = '<chart caption=\'&Uuml;berschneidungen mit anderen Partnern in ' . $_REQUEST['import_monat'] . '/' . $_REQUEST['import_jahr'] . '\'' . 
        ' subcaption=\'' . $subCaption . '\'' . $_SESSION['statistik']['formatNumber'] . ' ' . $_SESSION['statistik']['tausendSep'] . '' . 
        ' enableRotation=\'1\' showZeroPies=\'0\' palette=\'3\' decimals=\'1\' showPercentValues=\'1\' enableSmartLabels=\'1\' enableRotation=\'0\'' . 
        ' bgColor=\'FFFFFF\' bgAlpha=\'40,100\' bgRatio=\'0,100\' bgAngle=\'360\' showBorder=\'1\' startingAngle=\'70\'>
    ';
    
    $data = null;
    $dsBruttoNew = 0;

    if (is_array($uniqueTempTableDataArray)) {
        // debug
        $debugLogManager->beginGroup(__FUNCTION__ . ' uniqueTempTableDataArray: ' . $genart);
        
        foreach ($uniqueTempTableDataArray as $row) {
            // debug
            $debugLogManager->logData('row', $row);
            
            $quote = round((($row['count'] / $countBruttoTempTable) * 100), 1);
            
            $data .= '{Partner: \'' . $row['fieldName'] . '\', Anzahl:' . $row['count'] . ', Quote: \'' . $quote . '%\'},';
            $dataChart .= '<set label=\'' . $row['fieldName'] . '\' value=\'' . $row['count'] . '\' isSliced=\'1\' />';
            
            $dsBruttoNew += $row['count'];
        }
        
        // debug
        $debugLogManager->endGroup();
    }

    $dsBruttoNew = $countBruttoTempTable - $dsBruttoNew;
    
    $ohne = $dsBruttoNew;
    $quote_ohne = round((($ohne / $countBruttoTempTable) * 100), 1);
    
    $data .= '{Partner: \'Ueberschneidungsfrei\', Anzahl:' . $ohne . ', Quote: \'' . $quote_ohne . '%\'}';
    $dataChart .= '<set label=\'&Uuml;-frei\' value=\'' . $ohne . '\' /></chart>';
    
    // debug
    $debugLogManager->beginGroup(__FUNCTION__);
    $debugLogManager->logData('data', $data);
    $debugLogManager->logData('dataChart', $dataChart);
    $debugLogManager->endGroup();

    $_SESSION['statistik']['chartType'] = 'Pie3D';
    $_SESSION['statistik']['analyseTyp'] = 'ueberschneidung';
    $_SESSION['statistik']['datasource'] = '
        <script type="text/javascript">
            YAHOO.example.ueberschneidung = [' . $data . '];

            var opinionData = new YAHOO.util.DataSource(YAHOO.example.ueberschneidung);
            opinionData.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            opinionData.responseSchema = {
                fields: ["Partner", "Anzahl", "Quote"]
            };
            
            var columns = [
                {key: "Partner", sortable: true, resizeable: true},
                {key: "Anzahl", sortable: true, resizeable: true},
                {key: "Quote", resizeable: true}
            ];
            var mytable = new YAHOO.widget.DataTable(
                "datatable",
                columns,
                opinionData,
                {
                    sortedBy: {key: "Partner"}
                }
            );
        </script>
    ';
    $_SESSION['statistik']['dataChart'] = $dataChart;
}


$debugLogManager = new debugLogManager();
$debugLogManager->setUserId($_SESSION['benutzer_id']);
$debugLogManager->init();

// debug
$debugLogManager->logData('REQUEST', $_REQUEST);

$mandant = $_SESSION['mandant'];
include('db_connect.inc.php');

if ((int) $_SESSION['testimport'] === 1) {
    $metaDB = $metaDB . '_test';
    $import_datei_db = $import_datei_db . '_test';
}

$benutzer_name = $_SESSION['benutzer_name'];

$monat = isset($_REQUEST['import_monat']) ? intval($_REQUEST['import_monat']) : '';
$jahr = isset($_REQUEST['import_jahr']) ? intval($_REQUEST['import_jahr']) : '';

$partner = $_REQUEST['import_partner'];
if ($partner == '102') {
    $partner = 'planet49';
}

$genart = $_REQUEST['GenArt'];
$analyse_req = $_REQUEST['analyse_req'];

$_SESSION['statistik'] = array();
if (strlen($_REQUEST['x_lieferungen']) === 0) {
    $_SESSION['statistik']['x_lieferungen'] = 10;
} else {
    $_SESSION['statistik']['x_lieferungen'] = intval($_REQUEST['x_lieferungen']);
}

$impnr = $_REQUEST['impnr'];
if ($impnr != 'all' && strlen($impnr) > 0) {
    $impnr = intval($_REQUEST['impnr']);
    $_SESSION['statistik']['impnr'] = $impnr;
} elseif (strlen($impnr) === 0) {
    $impnr = $_SESSION['statistik']['impnr'];
} elseif ($impnr == 'all') {
    $_SESSION['statistik']['impnr'] = '';
    $impnr = '';
}

$_SESSION['statistik']['formatNumber'] = ' formatNumberScale=\'0\'';
$_SESSION['statistik']['tausendSep'] = ' thousandSeparator=\'.\'';

$stats_id_new = $monat . $jahr . $partner . $impnr;
$stats_id_old = $_SESSION['statistik']['stats_id'];
$_SESSION['statistik']['stats_id'] = $stats_id_new;

$bed = null;
$partnerDataArray = getRowDataArray(
    array(
        'SELECT' => '*',
        'FROM' => '`' . $partner_db . '`',
        'WHERE' => '`' . $felderPartnerDB['PID'] . '` = "' . $partner . '"'
    ),
    'getPartnerDataArray',
    $verbindung,
    $debugLogManager
);
if (is_array($partnerDataArray)) {
    $pkz = $partnerDataArray[$felderPartnerDB['Pkz']];
    $bed = unserialize($partnerDataArray[$felderPartnerDB['Bedingung']]);
}

$vereinbarung = '';
$vereinbarung_datum = null;
if (strlen($bed) > 0) {
    $j = 1;
    foreach ($bed as $b) {
        if ($j === 1) {
            $vereinbarung_datum = $b;
        }

        foreach ($b as $b1 => $b2) {
            switch ($b2) {
                case 'ds_ar':
                    $b_imp = 'ANREDE';
                    break;
                
                case 'ds_vn':
                    $b_imp = 'VORNAME';
                    break;
                
                case 'ds_nn':
                    $b_imp = 'NACHNAME';
                    break;
                
                case 'ds_str':
                    $b_imp = 'STRASSE';
                    break;
                
                case 'ds_plz':
                    $b_imp = 'PLZ';
                    break;
                
                case 'ds_ort':
                    $b_imp = 'ORT';
                    break;
                
                case 'ds_land':
                    $b_imp = 'LAND';
                    break;
                
                case 'ds_geb':
                    $b_imp = 'GEBURTSDATUM';
                    break;
                
                case 'ds_tel':
                    $b_imp = 'TELEFON';
                    break;
            }
            
            $vereinbarung .= $b_imp . ',';
        }
        
        $j++;
    }
}
$_SESSION['statistik']['vereinbarung'] = $vereinbarung;
$_SESSION['statistik']['vereinbarung_datum'] = $vereinbarung_datum;

$sql = null;
if (is_numeric($impnr)) {
    $sql = ' AND `i`.`IMPORT_NR2` = ' . intval($impnr);
}

// debug
$debugLogManager->logData('stats_id_new', $stats_id_new);
$debugLogManager->logData('stats_id_old', $stats_id_old);
if ($stats_id_new != $stats_id_old) {
    switch ($genart) {
        case 'Import':
            dropStatsTable(
                $benutzer_name,
                $verbindung,
                $debugLogManager
            );
            
            $resCreateStatsTable = createStatsTable(
                $genart,
                $benutzer_name,
                $felderMetaDB,
                $felderPartnerDB,
                $import_datei_db,
                $metaDB,
                $partner_db,
                $herkunft_db,
                $partner,
                $monat,
                $jahr,
                $sql,
                $verbindung,
                $debugLogManager
            );
            if ($resCreateStatsTable) {
                $resUpdate = alterStatsTable(
                    $benutzer_name,
                    $felderMetaDB['EMAIL'],
                    $verbindung,
                    $debugLogManager
                );
                
                if ($resUpdate) {
                    $resUpdateStatsTable = mysql_query(
                        'UPDATE `stats_' . $benutzer_name . '`' 
                            . ' SET ' . $felderMetaDB['HERKUNFT'] . ' = ""' 
                            . ' WHERE `' . $felderMetaDB['HERKUNFT'] . '` = "5"'
                        ,
                        $verbindung
                    );
                    if (!$resUpdateStatsTable) {
                        // debug
                        $debugLogManager->logData('sql error: resUpdateStatsTable: ' . $genart, mysql_error($verbindung));
                    }
                }
            }
            break;
        
        case 'Realtime':
            switch ($mandant) {
                case 'intone':
                case 'temp_mandant':
                    include('../db_optdb_connect.inc.php');
                    
                    if ($partner == 'planet49') {
                        $p_sql = ' AND `h`.`Partner` = `p`.`Pkz`';
                    } else {
                        $p_sql = ' AND `h`.`Partner` = `p`.`PID`';
                    }
                    break;
                
                case 'pepperos':
                    include('../db_pep_optdb_connect.inc.php');
                    break;
            }
            
            dropStatsTable(
                $benutzer_name,
                $verbindung_optdb,
                $debugLogManager
            );
            
            $resCreateStatsTable = createStatsTable(
                $genart,
                $benutzer_name,
                '',
                '',
                '',
                '',
                'Partner',
                '',
                $partner,
                $monat,
                $jahr,
                $p_sql,
                $verbindung_optdb,
                $debugLogManager
            );
            if ($resCreateStatsTable) {
                $resUpdate = alterStatsTable(
                    $benutzer_name,
                    'Email',
                    $verbindung_optdb,
                    $debugLogManager
                );
            }
            break;
    }
}

if ($stats_id_new == $stats_id_old && isset($_SESSION['statistik']['brutto_analyse'])) {
    $brutto = $_SESSION['statistik']['brutto_analyse'];
} else {
    switch ($genart) {
        case 'Import':
            include('db_connect.inc.php');
            
            $brutto = getCountRows(
                array(
                    'FIELD' => '*',
                    'FROM' => 'stats_' . $benutzer_name,
                    'WHERE' => '`' . $felderPartnerDB['PID'] . '` = "' . $partner . '"'
                ),
                'Count - brutto',
                $verbindung,
                $debugLogManager
            );
            $_SESSION['statistik']['brutto_analyse'] = $brutto;
            break;
        
        case 'Realtime':
            include('../db_optdb_connect.inc.php');
            
            $brutto = getCountRows(
                array(
                    'FIELD' => '*',
                    'FROM' => 'stats_' . $benutzer_name,
                    'WHERE' => '`Partner` = "' . $partner . '"'
                ),
                'Count - brutto',
                $verbindung_optdb,
                $debugLogManager
            );
            $_SESSION['statistik']['brutto_analyse'] = $brutto;
            break;
    }
}

$info = strtoupper($pkz) . ' | ' . zahl_formatieren($brutto) . ' Datens&auml;tze | ' . $monat . '/' . $jahr;

// debug
$debugLogManager->logData('statistik', $_SESSION['statistik']);

switch ($analyse_req) {
    case 'verwertbarkeit':
        verwertbarkeit_analyse(
            $genart,
            $info,
            $debugLogManager
        );
        break;
    
    case 'ueberschneidung':
        // TODO:
        #ueberschneidung_analyse(
        #    $genart,
        #    $info,
        #    $debugLogManager
        #);
        break;
    
    case 'ueberschneidung_ges':
        ueberschneidung_analyse_gesamt(
            $genart,
            $info,
            $debugLogManager
        );
        break;
    
    case 'datensaetze':
        datensaetze_analyse(
            $genart,
            $info,
            $debugLogManager
        );
        break;
    
    case 'laender':
        land_analyse(
            $genart,
            $info,
            $debugLogManager
        );
        break;
    
    case 'sex':
        geschlecht_analyse(
            $genart,
            $info,
            $debugLogManager
        );
        break;
    
    case 'alter':
        alter_analyse(
            $genart,
            $info,
            $debugLogManager
        );
        break;
    
    case 'domain':
        domain_analyse(
            $genart,
            $info,
            $debugLogManager
        );
        break;
    
    case 'herkunft':
        herkunft_analyse(
            $genart,
            $info,
            $debugLogManager
        );
        break;
    
    case 'anmeldung':
        anmeldung_analyse(
            $genart,
            $info,
            $debugLogManager
        );
        break;
    
    case 'quali':
        qualitaet_analyse(
            $genart,
            $info,
            $debugLogManager
        );
        break;
    
    case 'history':
        history_analyse(
            $genart,
            $info,
            $debugLogManager
        );
        break;
    
    case 'all':
        uebersicht(
            $genart,
            $sql,
            $debugLogManager
        );
        break;
}

echo '<div style="padding-top:5px;"></div>';

if ($analyse_req != 'all') {
    switch ($_SESSION['statistik']['chartType']) {
        case 'Pie3D':
            $buttons = '<button onclick="change_chart(\'P2D\',\'' . $_SESSION['statistik']['chartType'] . '\');">Kuchen 2D</button> ' . 
                '<button onclick="change_chart(\'P3D\',\'' . $_SESSION['statistik']['chartType'] . '\');">Kuchen 3D</button> ' . 
                '<button onclick="change_chart(\'D2D\',\'' . $_SESSION['statistik']['chartType'] . '\');">Donut 2D</button> ' . 
                '<button onclick="change_chart(\'D3D\',\'' . $_SESSION['statistik']['chartType'] . '\');">Donut 3D</button>'
            ;
            break;
        
        case 'MSCombi3D':
            $buttons = '<button onclick="change_chart(\'R2D\',\'' . $_SESSION['statistik']['chartType'] . '\');">Balken 2D</button> ' . 
                '<button onclick="change_chart(\'R3D\',\'' . $_SESSION['statistik']['chartType'] . '\');">Balken Rotation 3D</button> ' . 
                '<button onclick="change_chart(\'S2D\',\'' . $_SESSION['statistik']['chartType'] . '\');">Balken 3D Dual</button> ' . 
                '<button onclick="change_chart(\'S3D\',\'' . $_SESSION['statistik']['chartType'] . '\');">Linien Graph</button>'
            ;
            break;
        
        case 'StackedBar3D':
            $buttons = '<button onclick="change_chart(\'ST2D\',\'' . $_SESSION['statistik']['chartType'] . '\');">Balken 2D</button> ' . 
                '<button onclick="change_chart(\'ST3D\',\'' . $_SESSION['statistik']['chartType'] . '\');">Balken 3D</button>'
            ;
            break;
        
        case 'ScrollLine2D':
            $buttons = '<button onclick="change_chart(\'SL2D\',\'' . $_SESSION['statistik']['chartType'] . '\');">Linien Graph</button> ' . 
                '<button onclick="change_chart(\'B2D\',\'' . $_SESSION['statistik']['chartType'] . '\');">Bereich Graph</button>' . 
                '<div style="float:right">
                    <button onclick="setRequest_st(\'limport/statistik.php?import_monat=' . $monat . '&import_jahr=' . $jahr . '&import_partner=' . $partner . 
                        '&GenArt=' . $genart . '&analyse_req=' . $analyse_req . '&x_lieferungen=10\');">10</button> ' . 
                    '<button onclick="setRequest_st(\'limport/statistik.php?import_monat=' . $monat . '&import_jahr=' . $jahr . '&import_partner=' . $partner . 
                        '&GenArt=' . $genart . '&analyse_req=' . $analyse_req . '&x_lieferungen=25\');">25</button> ' . 
                    '<button onclick="setRequest_st(\'limport/statistik.php?import_monat=' . $monat . '&import_jahr=' . $jahr . '&import_partner=' . $partner . 
                        '&GenArt=' . $genart . '&analyse_req=' . $analyse_req . '&x_lieferungen=50\');">50</button> ' . 
                    '<button onclick="setRequest_st(\'limport/statistik.php?import_monat=' . $monat . '&import_jahr=' . $jahr . '&import_partner=' . $partner . 
                        '&GenArt=' . $genart . '&analyse_req=' . $analyse_req . '&x_lieferungen=100\');">100</button> ' . 
                    '<button onclick="setRequest_st(\'limport/statistik.php?import_monat=' . $monat . '&import_jahr=' . $jahr . '&import_partner=' . $partner . 
                        '&GenArt=' . $genart . '&analyse_req=' . $analyse_req . '&x_lieferungen=200\');">200</button> ' . 
                    '<button onclick="setRequest_st(\'limport/statistik.php?import_monat=' . $monat . '&import_jahr=' . $jahr . '&import_partner=' . $partner . 
                        '&GenArt=' . $genart . '&analyse_req=' . $analyse_req . '&x_lieferungen=500\');">500</button>' . 
                '</div>'
            ;
            break;
    }

    echo '
        <div id="chart_fusion" style="width:600px; height:375px; margin-top:15px;float:left; margin-right:10px;">
            <div>
                <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0"' . 
                        ' width="600" height="400" id="' . $_SESSION['statistik']['chartType'] . '" >
                    <param name="movie" value="FusionCharts/FusionCharts/' . $_SESSION['statistik']['chartType'] . '.swf" />
                    <param name="FlashVars" value="&dataXML=' . $_SESSION['statistik']['dataChart'] . '">
                    <param name="quality" value="high" />
                    <embed src="FusionCharts/FusionCharts/' . $_SESSION['statistik']['chartType'] . '.swf" flashVars="&dataXML=' . $_SESSION['statistik']['dataChart'] . '"' . 
                        ' quality="high" width="600" height="400" name="' . $_SESSION['statistik']['chartType'] . '" type="application/x-shockwave-flash"' . 
                        ' pluginspage="http://www.macromedia.com/go/getflashplayer" />
                </object>' . $buttons . 
            '</div>
        </div>
        <div id="datatable" style="margin-top:15px;height:400px;overflow:auto;"></div>'
    ;
}
?>
<script type="text/javascript">
    function change_chart(chart_type, oldChart) {
        switch (chart_type) {
            case 'P2D':
                chart_type = 'Pie2D';
                break;
                
            case 'P3D':
                chart_type = 'Pie3D';
                break;
                
            case 'D2D':
                chart_type = 'Doughnut2D';
                break;
                
            case 'D3D':
                chart_type = 'Doughnut3D';
                break;
		
            case 'R2D':
                chart_type = 'MSCombi2D';
                break;
                
            case 'R3D':
                chart_type = 'MSCombi3D';
                break;
                
            case 'S2D':
                chart_type = 'MSColumn3DLineDY';
                break;
                
            case 'S3D':
                chart_type = 'MSLine';
                break;
		
            case 'ST2D':
                chart_type = 'StackedBar2D';
                break;
                
            case 'ST3D':
                chart_type = 'StackedBar3D';
                break;
		
            case 'SL2D':
                chart_type = 'ScrollLine2D';
                break;
                
            case 'B2D':
                chart_type = 'ScrollArea2D';
                break;
        }
	
        chart = document.getElementById('chart_fusion').innerHTML;
        chart = str_replace(oldChart,chart_type,chart);
        document.getElementById('chart_fusion').innerHTML = chart;
    }
</script>

<?php
if (isset($_SESSION['statistik']['datasource'])) {
    echo $_SESSION['statistik']['datasource'];
}

unset($_SESSION['statistik']);
?>