<?php
require_once("util.php");
class dataManagerWebservice {

	public function __construct() {
			$mandant = $_SESSION['mandant'];
			include('../limport/db_connect.inc.php');
			$this->dbh = new PDO('mysql:host='.$dbHostEms.';dbname='.$db_name, $db_user, $db_pw, array(PDO::ATTR_PERSISTENT => true));
			$this->dbh_opt = new PDO('mysql:host=s274363588.online.de;dbname=optDB', 'dbo39323310', 'ama7beg4', array(PDO::ATTR_PERSISTENT => true));
	}
	
	public function getKundenData($table,$firma,$pid,$genart,$type) {
		
		$qry = ' SELECT * '
	          .' FROM '.$table
			  .' ORDER BY '.$firma.' ASC ';
			  
	    $stmt = $this->dbh->prepare($qry);
		$stmt->execute();
        $result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$dataArr[] = array( 'id' => $row[$pid], 
								'firma' => utf8_encode($row[$firma]),
								'genart' => $row[$genart],
								'firma_genart', "<span style='font-weight:bold'>".utf8_encode($row[$firma])."</span><br/><span style='color:#9900FF'>".$row[$genart]."</span>"
							  );
        }
		
		if ($type == 'json') {
			$dataArr = '{"Result": '.json_encode($dataArr).'}';
		} 
		
		return $dataArr;
		
	}
	
	
	public function getKundeData($table,$pid,$pkz,$genart,$firma,$web,$str,$plz,$ort,$gf,$rg,$tel,$fax,$bed,$email,$status,$id) {
		
		$qry = ' SELECT * '
	          .' FROM '.$table
			  .' WHERE '.$pid.' = :id ';
			  
	    $stmt = $this->dbh->prepare($qry);
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
        $result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$dataArr = array( 	'id' 				=> $row[$pid],
								'firma' 			=> $row[$firma],
								'firma_short' 		=> $row[$pkz],
								'genart' 			=> $row[$genart],
								'strasse' 			=> $row[$str],
								'plz' 				=> $row[$plz],
								'ort' 				=> $row[$ort],
								'telefon' 			=> $row[$tel],
								'fax' 				=> $row[$fax],
								'website' 			=> $row[$web],
								'email' 			=> $row[$email],
								'geschaeftsfuehrer' => $row[$gf],
								'registergericht' 	=> $row[$rg],
								'status' 			=> $row[$status]
							);
        }
		
		return $dataArr;
		
	}
	
	
	public function getAllAP($table,$id) {
		
		$qry = ' SELECT * '
	          .' FROM '.$table
			  .' WHERE `kunde_id` = :id '
			  .' AND `status` = 1 '
			  .' ORDER BY `email` ASC ';
			  
	    $stmt = $this->dbh->prepare($qry);
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
        $result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$dataArr[] = array( 'ap_id' 	=> $row['ap_id'],
								'anrede' 	=> $row['anrede'],
								'titel' 	=> $row['titel'],
								'vorname' 	=> $row['vorname'],
								'nachname' 	=> $row['nachname'],
								'email' 	=> $row['email'],
								'telefon' 	=> $row['telefon'],
								'fax' 		=> $row['fax'],
								'mobil' 	=> $row['mobil'],
								'position' 	=> $row['position'],
								'status' 	=> $row['status']
							);
        }
		
		return $dataArr;
		
	}
	
	
	public function getAP($table,$id) {
		
		$qry = ' SELECT * '
	          .' FROM '.$table
			  .' WHERE `ap_id` = :id ';
			  
	    $stmt = $this->dbh->prepare($qry);
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
        $result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$dataArr = array( 'ap_id' 	=> $row['ap_id'],
								'anrede' 	=> $row['anrede'],
								'titel' 	=> $row['titel'],
								'vorname' 	=> $row['vorname'],
								'nachname' 	=> $row['nachname'],
								'email' 	=> $row['email'],
								'telefon' 	=> $row['telefon'],
								'fax' 		=> $row['fax'],
								'mobil' 	=> $row['mobil'],
								'position' 	=> $row['position'],
								'status' 	=> $row['status']
							);
        }
		
		return $dataArr;
		
	}
		
	
	public static function getDataManagerStatusName($status) {
	
		switch ($status) {
			case 0: $status_bez = "Neu";$status_k = "red";break;
			case 5: $status_bez = "importiert";$status_k = "red";break;
			case 20: $status_bez = "Report intern<br />verschickt";$status_k = "#FF9900";break;
			case 21: $status_bez = "Erstreporting<br />abgeschlossen";$status_k = "#FF9900";break;
			case 22: $status_bez = "Endreporting<br />abgeschlossen";$status_k = "#5DB00D";break;
			case 23: $status_bez = "Report<br />freigegeben";$status_k = "green";break;
			case 30: $status_bez = "Rechnung<br />erhalten";$status_k = "#FF9900";break;
			case 31: $status_bez = "Rechnung<br />best�tigt";$status_k = "#FF9900";;break;
			case 32: $status_bez = "Rechnung<br />gecheckt";$status_k = "#FF9900";break;
			case 33: $status_bez = "Rechnung<br />bezahlt";$status_k = "#F2F2F2";break;
		}
		
		return "<span style='color:".$status_k."'>".$status_bez."</span>";
	}
	
	public function getLieferungDataAll($import_datei_db,$partner_db,$firma,$pid,$view,$type) {
		
		switch ($view) {
			case 'archiv'	: $sql = " AND (i.STATUS = 22 OR i.STATUS >= 30)"; break;
			default 		: $sql = " AND (i.STATUS < 22 OR i.STATUS = 23)"; break;
		}
		
		$qry = ' SELECT * '
	          .' FROM '.$import_datei_db.' as i, '.$partner_db.' as p '
			  .' WHERE i.IMPORT_NR = i.IMPORT_NR2 '
			  .' AND i.PARTNER = p.'.$pid
			  .$sql
			  .' ORDER BY `GELIEFERT_DATUM` DESC ';
			  
	    $stmt = $this->dbh->prepare($qry);
		$stmt->execute();
        $result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$status = $this->getDataManagerStatusName($row['STATUS']);
			#$dataSumArr = $this->getLieferungSumData($import_datei_db,$row['IMPORT_NR']);
			#$brutto = $dataSumArr['BRUTTO'];
			
			$dataArr[] = array( 
								'id' 				=> $row['IMPORT_NR'], 
								'status' 			=> $status, 
								'datei' 			=> "<span style='font-size:11px;font-weight:bold' title='".utf8_encode($row['IMPORT_DATEI'])."'>".utf8_encode(util::cutString($row['IMPORT_DATEI'],25))."</span><br/><span style='color:#9900FF'>".utf8_encode($row[$firma])."</span><br /><span style='font-size:9px;color:#333333'>".util::datum_de($row['GELIEFERT_DATUM'],'date')."</span>"
							  );
        }
		
		if ($type == 'json') {
			$dataArr = '{"Result": '.json_encode($dataArr).'}';
		} 
		
		return $dataArr;
		
	}
	
	
	public function getRealtimeLieferungDataAll($partner_db,$firma,$pid,$view,$type) {
		
		switch ($view) {
			case 'real_archiv'	: $sql = " AND i.status >= 22 AND i.status != 23"; break;
			default 		: $sql = " AND (i.status < 22 OR i.status = 23)"; break;
		}
		
		$qry = ' SELECT * '
	          .' FROM Report as i, Partner as p '
			  .' WHERE i.pid = p.PID '
			  .$sql
			  .' ORDER BY `zeitraum` DESC ';
			  
	    $stmt = $this->dbh_opt->prepare($qry);
		$stmt->execute();
        $result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$status = $this->getDataManagerStatusName($row['status']);
			
			$brutto = $row['brutto'];
			$zeitraum = strtotime($row['zeitraum']." -1 months");
			$zeitraum = date("Y-m-d", $zeitraum);
			
			$dataArr[] = array( 
								'id' 				=> $row['id'], 
								'status' 			=> $status, 
								'datei' 			=> "<span style='font-size:11px;font-weight:bold'>".util::getMonat($zeitraum)."</span><br/><span style='color:#9900FF'>".utf8_encode($row['Pkz'])."</span><br /><span style='font-size:9px;color:#333333'>".util::zahl_format($brutto)."</span>"
							  );
        }
		
		if ($type == 'json') {
			$dataArr = '{"Result": '.json_encode($dataArr).'}';
		} 
		
		return $dataArr;
		
	}
	
	
	public function getLieferungData($import_datei_db,$partner_db,$firma,$pid,$id) {
		
		$qry = ' SELECT * '
	          .' FROM '.$import_datei_db.' as i, '.$partner_db.' as p '
			  .' WHERE i.IMPORT_NR = i.IMPORT_NR2 '
			  .' AND i.PARTNER = p.'.$pid
			  .' AND i.IMPORT_NR = '.$id
			  .' ORDER BY `GELIEFERT_DATUM` DESC ';
		
		$qry2 = ' SELECT SUM(BRUTTO) as b, SUM(NETTO) as n, SUM(DUBLETTEN) as d, SUM(BLACKLIST) AS bl, SUM(BOUNCES) as bo, SUM(METADB) AS m, SUM(DUBLETTE_INDATEI) as di '
	          .' FROM '.$import_datei_db
			  .' WHERE IMPORT_NR2 = '.$id;
			  	  
	    $stmt = $this->dbh->prepare($qry);
		$stmt->execute();
        $result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$dataArr = array( 
								'IMPORT_NR' 		=> $row['IMPORT_NR'], 
								'IMPORT_NR2' 		=> $row['IMPORT_NR2'], 
								'IMPORT_DATEI' 		=> $row['IMPORT_DATEI'],
								'IMPORT_DATUM' 		=> $row['IMPORT_DATUM'],
								'GELIEFERT_DATUM' 	=> $row['GELIEFERT_DATUM'],
								'BRUTTO' 			=> $row['BRUTTO'], 
								'NETTO' 			=> $row['NETTO'], 
								'NETTO_EXT' 		=> $row['NETTO_EXT'], 
								'DUBLETTEN' 		=> $row['DUBLETTEN'], 
								'BLACKLIST' 		=> $row['BLACKLIST'], 
								'BOUNCES' 			=> $row['BOUNCES'], 
								'NOTIZ' 			=> $row['NOTIZ'], 
								'FAKE' 				=> $row['FAKE'], 
								'METADB' 			=> $row['METADB'],
								'DUBLETTE_INDATEI' 	=> $row['DUBLETTE_INDATEI'],
								'STATUS' 			=> $row['STATUS'],
								'ABSCHLAG' 			=> $row['Abschlag'],
								'PARTNER' 			=> $row[$firma],
								'PID' 				=> $row[$pid]
							  );
        }
		
		return $dataArr;
		
	}
	
	
	public function getIdLastLieferungen($import_datei_db,$partner,$limit) {
		
		$qry = ' SELECT * '
	          .' FROM '.$import_datei_db
			  .' WHERE IMPORT_NR = IMPORT_NR2 '
			  .' AND PARTNER = '.$partner
			  .' ORDER BY `GELIEFERT_DATUM` DESC '
			  .' LIMIT 0,'.$limit;
		
	    $stmt = $this->dbh->prepare($qry);
		$stmt->execute();
        $result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$dataArr[] = $row['IMPORT_NR'];
        }
		
		return $dataArr;
		
	}
	
	
	public function getRealtimeLieferungData($id) {
		
		$qry = ' SELECT * '
	          .' FROM Report '
			  .' WHERE id = '.$id;
		
			  	  
	    $stmt = $this->dbh_opt->prepare($qry);
		$stmt->execute();
        $result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$dataArr = array( 
								'IMPORT_NR' 		=> $row['id'], 
								'IMPORT_DATUM' 		=> $row['zeitraum'],
								'GELIEFERT_DATUM' 	=> $row['zeitraum'],
								'BRUTTO' 			=> $row['brutto'], 
								'NETTO' 			=> $row['netto'], 
								'NETTO_EXT' 		=> $row['netto_ext'], 
								'NOTIZ' 			=> $row['notiz'], 
								'STATUS' 			=> $row['status'],
								'PARTNER' 			=> $row['pid']
							  );
        }
		
		return $dataArr;
		
	}
	
	
	public function getIdLastRealtimeLieferungen($partner,$limit) {
		
		$qry = ' SELECT * '
	          .' FROM Report '
			  .' WHERE pid = '.$partner
			  .' ORDER BY `zeitraum` DESC '
			  .' LIMIT 0,'.$limit;
		
	    $stmt = $this->dbh_opt->prepare($qry);
		$stmt->execute();
        $result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$dataArr[] = $row['id'];
        }
		
		return $dataArr;
		
	}
	
	public function getRealtimeDataByDay($date,$pid) {
		
		$qry = ' SELECT DAY( Anmeldung ) AS Tag, COUNT( * ) AS Anzahl '
	          .' FROM History '
			  .' WHERE Partner = "'.$pid.'"'
			  .' AND Anmeldung LIKE "'.$date.'%"'
			  .' GROUP BY Tag '
			  .' ORDER BY Tag ASC ';
		
			  	  
	    $stmt = $this->dbh_opt->prepare($qry);
		$stmt->execute();
        $result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$dataArr[] = array( 
								'Tag' 		=> $row['Tag'], 
								'Anzahl' 	=> $row['Anzahl']
							  );
        }
		
		return $dataArr;
		
	}
	
	
	public function getLieferungSumData($import_datei_db,$id) {
		
		
		$qry = ' SELECT SUM(BRUTTO) as b, SUM(NETTO) as n, SUM(DUBLETTEN) as d, SUM(BLACKLIST) AS bl, SUM(BOUNCES) as bo, SUM(METADB) AS m, SUM(DUBLETTE_INDATEI) as di '
	          .' FROM '.$import_datei_db
			  .' WHERE IMPORT_NR2 = '.$id;
			  	  
	    $stmt = $this->dbh->prepare($qry);
		$stmt->execute();
        $result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$dataArr = array( 
								'BRUTTO' 			=> $row['b'], 
								'NETTO' 			=> $row['n'], 
								'DUBLETTEN' 		=> $row['d'],
								'BLACKLIST' 		=> $row['bl'],
								'BOUNCES' 			=> $row['bo'],
								'METADB' 			=> $row['m'], 
								'DUBLETTE_INDATEI' 	=> $row['di']
							  );
        }
		
		return $dataArr;
		
	}
	
	
	public function getPartnerData($partner_db,$firma,$pid,$id) {
		
		$qry = ' SELECT * '
	          .' FROM '.$partner_db
			  .' WHERE '.$pid.' = '.$id;
			  
	    $stmt = $this->dbh->prepare($qry);
		$stmt->execute();
        $result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$dataArr = array( 
								'PID' 				=> $row[$pid], 
								'Firma' 			=> $row[$firma]
							  );
        }
		
		return $dataArr;
		
	}
	
}

?>