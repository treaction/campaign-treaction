<?php
session_start();

header('Content-Type: text/html; charset=ISO-8859-1');

// debug
function firePhPDebug() {
    $firePhp = null;
    if (intval($_SESSION['benutzer_id']) === 24) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/system/library/FirePHPCore/FirePHP.class.php');
        $firePhp = FirePHP::getInstance(true);
    }
    
    return $firePhp;
}

function update_report() {
    global $mandant;
    global $impnr;
    global $st;
    global $netto_ext;
    include('../db_connect.inc.php');
    
    $firePhp = firePhPDebug();
    
    $res = mysql_query(
        'SELECT * FROM `' . $import_datei_db . '` WHERE `IMPORT_NR` = "' . $impnr . '"',
        $verbindung
    );
    if ($res) {
        $iz = mysql_fetch_array($res);
        mysql_free_result($res);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($iz, 'update_report -> getImportDataArray');
        }
        
        $notiz = utf8_decode($_POST['notiz_rep']);
        $jetzt = date('d.m.Y - H:m');
        
        if ($iz['NETTO_EXT'] != $netto_ext || $iz['NOTIZ'] != $notiz) {
            if ($_POST['rep_email_status'] == 'send_rep_email') {
                $st = 20;
                $rep_info = 'Reporting versendet von ' . $_SESSION['username'] . ' am ' . $jetzt;
            } else {
                $rep_info = 'Aktualisiert von ' . $_SESSION['username'] . ' am ' . $jetzt;
            }

            $resUpdate = mysql_query(
                'UPDATE `' . $import_datei_db . '` SET `NETTO_EXT` = "' . $netto_ext . '", `STATUS` = "' . $st . '"' . 
                    ', `REPORTING_DATUM` = NOW(), `REPORTING_ABSENDER` = "' . $_SESSION['username'] . '", `REPORTING_INFO` = "' . $rep_info . '"' . 
                    ', `NOTIZ` = "' . $notiz . '" WHERE `IMPORT_NR` = "' . $impnr . '"'
                ,
                $verbindung
            );
            if (!$resUpdate) {
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->log(mysql_error($verbindung), 'sql error: update_report() -> resUpdate');
                }
            }
        }
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: update_report()');
        }
    }
}

function email_senden() {
    global $mandant;
    global $impnr;
    include('db_connect.inc.php');
    include('mail/class.phpmailer.php');
    
    $firePhp = firePhPDebug();

    $rep_email = $_POST['ap_email'];

    $empfaenger_array = $_POST['empfaenger'];
    $empfaenger_array[] = $rep_email;
    $empfaenger_array[] = $absender_email;

    $betreff = $_POST['betreff_rep'];
    $report_mail_text = nl2br($_POST['mail_text']);
    $report_mail_text = utf8_decode($report_mail_text);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($_POST, '$_POST');
        $firePhp->log($empfaenger_arrayas, '$empfaenger_arrayas');
    }

    foreach ($empfaenger_arrayas as $empfaenger) {
        $html_email = '' . 
            '<html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
                    <meta http-equiv="Content-Style-Type" content="text/css" >
                    <title>Datenabgleich</title>
		</head>
		<body style="margin:0px; padding:5px; background-color:#FFFFFF; font-family:Arial, sans-serif; font-size:12px;" bgcolor="#FFFFFF">
                    <br />' . $report_mail_text . '
		</body>
            </html>'
        ;

        $mail = new phpmailer();
        $mail->IsSMTP();
        $mail->Host = 'smtp.1und1.de';
        $mail->SMTPAuth = true;
        $mail->Username = $smtp_user;
        $mail->Password = $smtp_pw;
        $mail->From = $absender_email;
        $mail->FromName = $absender_name;
        $mail->AddAddress($empfaenger, '');
        $mail->Subject = $betreff;
        $mail->Body = $html_email;
        if (!$mail->Send()) {
            // debug
            if ($firePhp instanceof FirePHP) {
                $firePhp->log($mail->ErrorInfo, 'email_senden');
                $firePhp->log($empfaenger, 'empfaenger');
            }
            
            echo '<span style="color:red">' . $mail->ErrorInfo . '</span>';
            $fehler = true;
        }
    }

    $resUpdate = mysql_query(
        'UPDATE `' . $import_datei_db . '` SET `STATUS` = "20" WHERE `IMPORT_NR` = "' . $impnr . '"',
        $verbindung
    );
    if (!$resUpdate) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: email_senden() -> resUpdate');
        }
    }
}

function status_aendern($t) {
    global $mandant;
    
    $firePhp = firePhPDebug();
    
    $st = $_POST['status_neu'];
    if ($t != 'realtime') {
        include('db_connect.inc.php');
        
        if ($_SESSION['testimport'] == 1) {
            $metaDB = $metaDB . '_test';
            $import_datei_db = $import_datei_db . '_test';
        }
        
        $sql = null;
        if ($st == 0) {
            $sql = ', `REPORTING_DATUM` = ""';
        }
        
        $resUpdate = mysql_query(
            'UPDATE `' . $import_datei_db . '` SET `STATUS` = "' . $st . '"' . $sql . ' WHERE `IMPORT_NR` = "' . $_POST['import'] . '"',
            $verbindung
        );
        if (!$resUpdate) {
            // debug
            if ($firePhp instanceof FirePHP) {
                $firePhp->log(mysql_error($verbindung), 'sql error: status_aendern()');
            }
        }
    } else {
        include('../db_optdb_connect.inc.php');

        $resUpdate = mysql_query(
            'UPDATE `Report` SET `status` = "' . $st . '" WHERE `id` = "' . $_POST['realtime'] . '"',
            $verbindung_optdb
        );
        if (!$resUpdate) {
            // debug
            if ($firePhp instanceof FirePHP) {
                $firePhp->log(mysql_error($verbindung_optdb), 'sql error: status_aendern() -> realtime');
            }
        }
    }
}

function del_datei() {
    global $mandant;
    include('db_connect.inc.php');
    
    $firePhp = firePhPDebug();
    
    if ($_SESSION['testimport'] == 1) {
        $metaDB = $metaDB . '_test';
        $import_datei_db = $import_datei_db . '_test';
    }
    
    $import_nr = $_POST['impnr_del'];
    $resDelMetaDB = mysql_query(
        'DELETE FROM `' . $db_name . '`.`' . $metaDB . '` WHERE `' . $felderMetaDB['IMPORT_NR'] . '` = "' . $import_nr . '"',
        $verbindung
    );
    if (!$resDelMetaDB) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: del_datei() -> metaDb');
        }
		echo mysql_error($verbindung);
    }
    
    $resDelImportDb = mysql_query(
        'DELETE FROM `' . $db_name . '`.`' . $import_datei_db . '` WHERE `IMPORT_NR` = "' . $import_nr . '"',
        $verbindung
    );
    if (!$resDelImportDb) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: del_datei() -> importDb');
        }
		echo mysql_error($verbindung);
    }
}

function del_partner($pid) {
    global $mandant;
    include('db_connect.inc.php');
    
    $firePhp = firePhPDebug();
    
    $resDel = mysql_query(
        'DELETE FROM `' . $partner_db . '` WHERE `' . $felderPartnerDB['PID'] . '` = "' . $pid . '"',
        $verbindung
    );
    if (!$resDel) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: del_partner()');
        }
    }
}

$mandant = $_SESSION['mandant'];
include('db_connect.inc.php');


$firePhp = firePhPDebug();
// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($_POST, '$_POST');
}

if ($_SESSION['testimport'] == 1) {
    $metaDB = $metaDB . '_test';
    $import_datei_db = $import_datei_db . '_test';
}

$impnr = $_POST['import_nr'];
$st = $_POST['status_rep'];
$netto_ext = $_POST['netto_report_ext2'];
$p_action = $_POST['p_action'];

if ($_POST['import']) {
    status_aendern();
} else {
    //status_aendern('realtime');
}

if ($_POST['impnr_del']) {
	echo 'delDatei';
    del_datei();
}

if ($_POST['pid_del'] != '') {
    del_partner($_POST['pid_del']);
}

if ($p_action != '') {
    $p_pid = $_POST['p_pid'];
    $p_pkz = utf8_decode($_POST['p_pkz']);
    $genart = $_POST['genart'];
    $p_web = $_POST['p_web'];
    $p_firma = utf8_decode($_POST['p_firma']);
    $p_str = utf8_decode($_POST['p_str']);
    $p_plz = $_POST['p_plz'];
    $p_ort = utf8_decode($_POST['p_ort']);
    $p_gf = utf8_decode($_POST['p_gf']);
    $p_reg = utf8_decode($_POST['p_reg']);
    $p_tel = $_POST['p_tel'];
    $p_fax = $_POST['p_fax'];
    $ap_anrede = $_POST['ap_anrede'];
    $p_ap = utf8_decode($_POST['p_ap']);
    $p_email = $_POST['p_email'];
    $p_abschlag = $_POST['p_abschlag'];
    
    $vereinbarungen = array(
        'da' => $_POST['datenalter'],
        'ds' => $_POST['datenstruktur'],
        'p' => $_POST['permission']
    );
    $vereinbarungen_ser = serialize($vereinbarungen);

    if ($mandant == 'intone') {
        include('../db_optdb_connect.inc.php');
        
        $verbindung = $verbindung_optdb;
        $partner_db = 'Partner';
    }

    if ($p_action == 'edit') {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($felderPartnerDB, '$felderPartnerDB edit');
        }
        
        $resUpdateEdit = mysql_query(
            'UPDATE `' . $partner_db . '` SET `' . $felderPartnerDB['Pkz'] . '` = "' . $p_pkz . '", `' . $felderPartnerDB['GenArt'] . '` = "' . $genart . '"' . 
                    ', `' . $felderPartnerDB['Website'] . '` = "' . $p_web . '", `' . $felderPartnerDB['Firma'] . '` = "' . $p_firma . '"' . 
                    ', `' . $felderPartnerDB['Strasse'] . '` = "' . $p_str . '", `' . $felderPartnerDB['PLZ'] . '` = "' . $p_plz . '"' . 
                    ', `' . $felderPartnerDB['Ort'] . '` = "' . $p_ort . '", `' . $felderPartnerDB['Geschaeftsfuehrer'] . '` = "' . $p_gf . '"' . 
                    ', `' . $felderPartnerDB['Registergericht'] . '` = "' . $p_reg . '", `' . $felderPartnerDB['Telefon'] . '` = "' . $p_tel . '"' . 
                    ', `' . $felderPartnerDB['Fax'] . '` = "' . $p_fax . '", `' . $felderPartnerDB['Rep_anrede'] . '` = "' . $ap_anrede . '`' . 
                    ', `' . $felderPartnerDB['Rep_ap'] . '` = "' . $p_ap . '", `' . $felderPartnerDB['Rep_email'] . '` = "' . $p_email . '"' . 
                    ', `' . $felderPartnerDB['Abschlag'] . '` = "' . $p_abschlag . '", `' . $felderPartnerDB['Bedingung'] . '` = "' . $vereinbarungen_ser . '"' . 
                ' WHERE `' . $felderPartnerDB['PID'] . '` = "' . $p_pid . '"'
            ,
            $verbindung
        );
        if (!$resUpdateEdit) {
            // debug
            if ($firePhp instanceof FirePHP) {
                $firePhp->log(mysql_error($verbindung), 'sql error: resUpdateEdit');
            }
        }
    }

    if ($p_action == 'new') {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($felderPartnerDB, '$felderPartnerDB neu');
        }
        
        $resInsert = mysql_query(
            'INSERT INTO `' . $partner_db . '`' . 
                ' (`' . $felderPartnerDB['PID'] . '`, `' . $felderPartnerDB['Pkz'] . '`, `' . $felderPartnerDB['GenArt'] . '`' . 
                    ', `' . $felderPartnerDB['Website'] . '`, `' . $felderPartnerDB['Firma'] . '`, `' . $felderPartnerDB['Strasse'] . '`' . 
                    ', `' . $felderPartnerDB['PLZ'] . '`, `' . $felderPartnerDB['Ort'] . '`, `' . $felderPartnerDB['Geschaeftsfuehrer'] . '`' . 
                    ', `' . $felderPartnerDB['Registergericht'] . '`, `' . $felderPartnerDB['Telefon'] . '`, `' . $felderPartnerDB['Fax'] . '`' . 
                    ', `' . $felderPartnerDB['Abschlag'] . '`, `' . $felderPartnerDB['Bedingung'] . '`' . 
                ') VALUES (' . 
                    '"' . $p_pid . '", "' . $p_pkz . '", "' . $genart . '", "' . $p_web . '", "' . $p_firma . '", "' . $p_str . '", "' . $p_plz . '"' . 
                    ', "' . $p_ort . '", "' . $p_gf . '", "' . $p_reg . '", "' . $p_tel . '", "' . $p_fax . '", "' . $p_abschlag . '", "' . $vereinbarungen_ser . '"' . 
                ')'
            ,
            $verbindung
        );
        if (!$resInsert) {
            // debug
            if ($firePhp instanceof FirePHP) {
                $firePhp->log(mysql_error($verbindung), 'sql error: resInsert');
            }
        }
    }
}

if ($_POST['rep_email_status'] == 'send_rep_email') {
    email_senden();
}

if ($netto_ext) {
    update_report();
}
?>