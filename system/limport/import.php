<?php
include('check_login.inc.php');
include('db_connect.inc.php');
include('login.inc.php');
$step = $_REQUEST['step'];

switch ($step) {
	case 2: $importdatei = "import2.php";break;
	case 3: $importdatei = "import3.php";break;
	case 4: $importdatei = "import4.php";break;
	case 5: $importdatei = "import5.php";break;
	case 'export': $importdatei = "export_bc2.php";break;
	default: $importdatei = "import1.php";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Meta DB</title>
<link rel="stylesheet" type="text/css" href="layout.css"/>
<link rel="stylesheet" type="text/css" href="yui/build/fonts/fonts-min.css" />
<link rel="stylesheet" type="text/css" href="yui/build/tabview/assets/skins/sam/tabview.css" />
<script type="text/javascript" src="yui/build/tabview/tabview-min.js"></script>
<script language="javascript" type="text/javascript">
function formcheck() 
{ 
	form = document.optionen; 
 	error = ""; 
			
	if (form.partner.value == '') 
	{ error += " - Bitte Partner ausw�hlen"; } 
	
	if (error != "") 
	{ 
		alert(error); 
		return false; 
	} 
	return true;
}
</script>
</head>
<body class="yui-skin-sam">
<div id="demo" class="yui-navset">
    <ul class="yui-nav">
        <li <?php if ($mandant=='intone') {print 'class="selected"';} ?>><a href="index.php?mandant=intone"><em>Intone</em></a></li>
        <li <?php if ($mandant=='pepperos') {print 'class="selected"';} ?>><a href="index.php?mandant=pepperos"><em>Pepperos</em></a></li>
        <li <?php if ($mandant=='temp') {print 'class="selected"';} ?>><a href="#tab3"><em>Temp</em></a></li>
		<li class="selected"><a href="#"><em>Datei-Import</em></a></li>
    </ul>            
    <div class="yui-content">
        <div id="tab1"><br />
			<?php
				include('menu.inc.php');
				include($importdatei);
			?>
			<br />
		</div>
    </div>
</div>
</body>
</html>

