<?php
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();

if ($_SESSION['export_mandant']) {
	$mandant = $_SESSION['export_mandant'];
} else {
	$mandant = $_SESSION['mandant'];
}

$mandant_all = $_SESSION['mandant_all'];
$u_rechte = $_SESSION['rechte'];
$u_zugang = $_SESSION['zugang'];

$step = $_REQUEST['step'];

$import_ordner = "import_temp/"; 
$_SESSION['datei_label'] = "DATEI IMPORT";

$import_jahr = date("Y");
include('../db_connect.inc.php');


$imp_daten = mysql_query("SELECT *
							  FROM $import_datei_db, $partner_db
							  WHERE $import_datei_db.PARTNER = $partner_db.".$felderPartnerDB['PID']."
							  AND YEAR($import_datei_db.IMPORT_DATUM) = '$import_jahr'
							  AND $import_datei_db.IMPORT_NR=$import_datei_db.IMPORT_NR2
						  ORDER BY $import_datei_db.GELIEFERT_DATUM desc
							",$verbindung);
							
while ($imp_zeile = mysql_fetch_array($imp_daten)) {
	$imp_nr = $imp_zeile['IMPORT_NR'];
	$imp_partner_id = $imp_zeile['PARTNER'];
	$imp_partner = $imp_zeile[$felderPartnerDB['Pkz']];
	$imp_datum = $imp_zeile['GELIEFERT_DATUM'];
	$imp_datei = $imp_zeile['IMPORT_DATEI'];
	$text = $imp_partner." | ".$imp_datum." | ".$imp_datei;
	
	$option .= "<option value='".$imp_nr."'>".$text."</option>";
}

function erstelle_temp_tabellen() {
	global $mandant;
	include('../db_blacklist_connect.inc.php');
	mysql_query("CREATE TABLE temp_".$_SESSION['benutzer_name']." (
					`email` VARCHAR( 155 ) NOT NULL,
					PRIMARY KEY (`email`),
					INDEX (`email`),
					UNIQUE (`email`)
				) DEFAULT CHARSET = utf8", $verbindung_bl);
	
	$bl_query = mysql_query("SELECT Email FROM Blacklist", $verbindung_bl);
					while ($zeile = mysql_fetch_array($bl_query)) {
						$bl_email = $zeile['Email'];
						$pos = strpos($bl_email, '*');
						if ($pos!==false) {
							$bl_domain[] = $bl_email;
						} else {
							$bl_sql .= "('".$bl_email."'),";
						}
					}
					
	$_SESSION['bl_email'] = $bl_domain;
	$bl_sql = substr($bl_sql, 0, -1);
	
					
	$regex_ = mysql_query("SELECT * FROM Fake_email" , $verbindung_bl);
					while ($z_fake = mysql_fetch_array($regex_)) {
						$regex[] = $z_fake['regex'];
					}
					
	$_SESSION['regex'] = $regex;				
							
	include('../db_connect.inc.php');
	mysql_query("CREATE TABLE $metaDB_temp LIKE $metaDB", $verbindung);
	mysql_query("CREATE TABLE tmp_bl_".$_SESSION['benutzer_name']." (
					`email` VARCHAR( 155 ) NOT NULL,
					PRIMARY KEY (`email`),
					UNIQUE (`email`)
				) DEFAULT CHARSET = utf8", $verbindung);
	mysql_query("INSERT INTO $db_name.$bl_temp (email) VALUES $bl_sql", $verbindung);
	
}


function reset_all() {
	global $mandant;
	require_once('db_connect.inc.php');
	mysql_query("DROP TABLE $temp_db",$verbindung);
	mysql_query("DROP TABLE tmp_bl_".$_SESSION['benutzer_name'],$verbindung);
	mysql_query("DROP TABLE $metaDB_temp",$verbindung);
	require_once('../db_blacklist_connect.inc.php');
	mysql_query("DROP TABLE temp_".$_SESSION['benutzer_name'] , $verbindung_bl);
	require_once('../db_optdb_connect.inc.php');
	mysql_query("DROP TABLE temp_limport_".$_SESSION['benutzer_name'], $verbindung_optdb);
	print '<script type="text/javascript">window.resizeBy(0,-330);</script>
	<table width="100%" cellpadding="8" cellspacing="0" border="0" bgcolor="#666666" style="font-size:11px">
    	<tr>
        	<td height="70" colspan="2" style="background-color:#FFFFFF; font-weight:bold; font-size:16px; color:#0066FF; border-bottom:1px solid #666666;letter-spacing:0.1em" align="left">DATEI IMPORT<br /><span style="color:#999999;font-size:12px;font-weight:normal;letter-spacing:normal">Abbruch</span></td>
        </tr>
        <tr style="background-color:#F2F2F2">
        	<td colspan="2" height="35">Vorgang wurde abgebrochen!</td>
        </tr>
        <tr><td colspan="2"><button onclick="window.close();">OK</button></td></tr>
</table>';
}				



$partner_auslesen = mysql_query ("SELECT * FROM $partner_db WHERE ".$felderPartnerDB['GenArt']." !='Realtime' ORDER BY ".$felderPartnerDB['Pkz']." ASC", $verbindung);
	while ($zeile_partner = mysql_fetch_array($partner_auslesen)) {
		$partner_id = $zeile_partner[$felderPartnerDB['PID']];
		$partner_name = $zeile_partner[$felderPartnerDB['Pkz']];
		$partner .=	"<option value=".$partner_id.">".$partner_name." (".$partner_id.")</option>";
	}
?>
<div id="upload_text"></div>
<div id='upload_form'>
    <table width="100%" cellpadding="8" cellspacing="0" border="0" bgcolor="#666666" style="font-size:11px">
        <tr style="background-color:#F2F2F2;">
        	<td width="100" style="color:#0066FF" align="right">Dateiauswahl</td>
            <td><input name="Datei" id="Datei" type="file" style="width:225px" /><br />
             <span style='font-size:10px;color:#0066FF;'>max. Dateigr&ouml;&szlig;e: 25MB (150.000 Datens&auml;tze)</span></td>
        </tr>
        <tr style="background-color:#F2F2F2">
        	<td style="color:#0066FF" align="right" valign="top">Split hinzuf&uuml;gen</td>
            <td> <select name="datei_add_id" id="datei_add_id" style="width:225px"><option value="">-</option><?php print $option; ?></select><br />
            <span style="font-size:10px;color:#333333;">Diese Option verwenden wenn mehrere Teildateien aus einer<br />Lieferung erzeugt worden sind</span></td>
        </tr>
        <tr style="background-color:#F2F2F2">
        	<td style="color:#0066FF" align="right">Partner</td>
            <td>
				<select name="partner" id="partner" style="width:225px">
					<option value="">--- Partner ausw&auml;hlen (PID) ---</option>
					<?php print $partner;?>
				</select>
		</td>
        </tr>
        <tr style="background-color:#F2F2F2">

        	<td style="color:#0066FF" align="right">Geliefert am</td>
            <td id="datefields">
	        <select id="month" name="month">
	        	<option value="01">01</option>
	        	<option value="02">02</option>
	        	<option value="03">03</option>
	        	<option value="04">04</option>
	        	<option value="05">05</option>
	        	<option value="06">06</option>
	        	<option value="07">07</option>
	        	<option value="08">08</option>
	        	<option value="09">09</option>
	        	<option value="10">10</option>
	        	<option value="11">11</option>
	        	<option value="12">12</option>
	        </select>

	        <select id="day" name="day">
	        	<option value="01">01</option>
	        	<option value="02">02</option>
	        	<option value="03">03</option>
	        	<option value="04">04</option>
	        	<option value="05">05</option>
	        	<option value="06">06</option>
	        	<option value="07">07</option>
	        	<option value="08">08</option>
	        	<option value="09">09</option>
	        	<option value="10">10</option>
	        	<option value="11">11</option>
	        	<option value="12">12</option>
	        	<option value="13">13</option>
	        	<option value="14">14</option>
	        	<option value="15">15</option>
	        	<option value="16">16</option>
	        	<option value="17">17</option>
	        	<option value="18">18</option>
	        	<option value="19">19</option>
	        	<option value="20">20</option>
	        	<option value="21">21</option>
	        	<option value="22">22</option>
	        	<option value="23">23</option>
	        	<option value="24">24</option>
	        	<option value="25">25</option>
	        	<option value="26">26</option>
	        	<option value="27">27</option>
	        	<option value="28">28</option>
	        	<option value="29">29</option>
	        	<option value="30">30</option>
	        	<option value="31">31</option>
	        </select>
			<input type="text" id="year" name="year" value="">
			 <input type="hidden" id="date" name="date" value="" />
			</td>
        </tr>
        <tr style="background-color:#F2F2F2">
        	<td colspan="2" height="50"></td>
        </tr>
        <tr style="background-color:#666666">
        	<td height="50"></td>
        	<td valign="top"><input name="step" type="hidden" value="1" /><input name="Dateiname" type="hidden" value="1" />
			<input name="submit" type="submit" onclick="formcheck();" value="Datei hochladen" /> <input type="button" onclick="location.href='action.php?step=reset'" value="abbrechen" style="margin-left:15px" /></td>
        </tr>
    </table>
</div>
<script language='javascript' type='text/javascript'>
function dateiupload() {
		document.getElementById('upload_text').innerHTML = '<table width="100%" height="100%" cellpadding="8" cellspacing="0" border="0" bgcolor="#F2F2F2" style="font-size:11px"><tr><td height="70" colspan="2" style="background-color:#FFFFFF; font-weight:bold; font-size:16px; color:#0066FF; border-bottom:1px solid #666666;letter-spacing:0.1em" align="left">DATEI IMPORT<br /><span style="color:#999999;font-size:12px;font-weight:normal;letter-spacing:normal;">Dateiupload</span></td></tr><tr style="background-color:#F2F2F2"><td height="75"><span style="font-weight:bold; color:#0066FF;">Datei wird hochgeladen, bitte warten...</span></td></tr></table>';
		document.getElementById('upload_form').style.display = "none";
}
		
function formcheck() 
{
	d_name = document.getElementById('Datei').value;
	d_partner = document.getElementById('partner').value;
	d_date = document.getElementById('date').value;
	d_datei_add = document.getElementById('datei_add_id').value;
	
 	error = "";
	
	if (d_name == '')
	{ error += " - Bitte eine Datei ausw�hlen\n"; }
	
	if (d_partner == '')
	{ error += " - Bitte Partner ausw�hlen\n"; }

	if (d_date == '')
	{ error += " - Bitte Lieferdatum eingeben"; }
	
	if (error != "")
	{ 
		alert(error); 
		return false; 
	} else {
		
		setRequest_default('limport/import/import1.php?Datei='+d_name+'&partner='+d_partner+'&date='+d_date+'&datei_add_id='+d_datei_add,'upload_form');
	}
}

		
var Event = YAHOO.util.Event,
			Dom = YAHOO.util.Dom;	


		Event.onContentReady("datefields", function () {

			var oCalendarMenu;
	
			var onButtonClick = function () {
	
				// Create a Calendar instance and render it into the body 
				// element of the Overlay.
	
				var oCalendar = new YAHOO.widget.Calendar("buttoncalendar", oCalendarMenu.body.id);
				
				oCalendar.cfg.setProperty("START_WEEKDAY",[1]); 
				oCalendar.cfg.setProperty("MONTHS_LONG", ["Januar", "Februar", "M\u00E4rz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"]);
				oCalendar.cfg.setProperty("WEEKDAYS_SHORT", ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"]);
				oCalendar.render();
	
	
				// Subscribe to the Calendar instance's "select" event to 
				// update the Button instance's label when the user
				// selects a date.
	
				oCalendar.selectEvent.subscribe(function (p_sType, p_aArgs) {
	
					var aDate,
						nMonth,
						nDay,
						nYear;
	
					if (p_aArgs) {
						
						aDate = p_aArgs[0][0];
	
						nMonth = aDate[1];
						nDay = aDate[2];
						nYear = aDate[0];
						
						if (nDay<10) {nDay = "0"+nDay;}
						if (nMonth<10) {nMonth = "0"+nMonth;}
	
						oButton.set("label", (nDay + "." +  nMonth + "." + nYear));
						
						document.getElementById('date').value =  nYear+"-"+  nMonth +"-"+nDay;
	
						// Sync the Calendar instance's selected date with the date form fields
	
						Dom.get("month").selectedIndex = (nMonth - 1);
						Dom.get("day").selectedIndex = (nDay - 1);
						Dom.get("year").value = nYear;
	
					}
					
					oCalendarMenu.hide();
				
				});
	
	
				// Pressing the Esc key will hide the Calendar Menu and send focus back to 
				// its parent Button
	
				Event.on(oCalendarMenu.element, "keydown", function (p_oEvent) {
				
					if (Event.getCharCode(p_oEvent) === 27) {
						oCalendarMenu.hide();
						this.focus();
					}
				
				}, null, this);
				
				
				var focusDay = function () {

					var oCalendarTBody = Dom.get("buttoncalendar").tBodies[0],
						aElements = oCalendarTBody.getElementsByTagName("a"),
						oAnchor;

					
					if (aElements.length > 0) {
					
						Dom.batch(aElements, function (element) {
						
							if (Dom.hasClass(element.parentNode, "today")) {
								oAnchor = element;
							}
						
						});
						
						
						if (!oAnchor) {
							oAnchor = aElements[0];
						}


						// Focus the anchor element using a timer since Calendar will try 
						// to set focus to its next button by default
						
						YAHOO.lang.later(0, oAnchor, function () {
							try {
								oAnchor.focus();
							}
							catch(e) {}
						});
					
					}
					
				};


				// Set focus to either the current day, or first day of the month in 
				// the Calendar	when it is made visible or the month changes
	
				oCalendarMenu.subscribe("show", focusDay);
				oCalendar.renderEvent.subscribe(focusDay, oCalendar, true);
	

				// Give the Calendar an initial focus
				
				focusDay.call(oCalendar);
	
	
				// Re-align the CalendarMenu to the Button to ensure that it is in the correct
				// position when it is initial made visible
				
				oCalendarMenu.align();

	
				// Unsubscribe from the "click" event so that this code is 
				// only executed once
	
				this.unsubscribe("click", onButtonClick);
			
			};
	
	
			var oDateFields = Dom.get("datefields");
				oMonthField = Dom.get("month"),
				oDayField = Dom.get("day"),
				oYearField = Dom.get("year");
	
	
			// Hide the form fields used for the date so that they can be replaced by the 
			// calendar button.
	
			oMonthField.style.display = "none";
			oDayField.style.display = "none";
			oYearField.style.display = "none";
	
	
			// Create a Overlay instance to house the Calendar instance
	
			oCalendarMenu = new YAHOO.widget.Overlay("calendarmenu", { visible: false });
	
	
			// Create a Button instance of type "menu"
	
			var oButton = new YAHOO.widget.Button({ 
											type: "menu", 
											id: "calendarpicker", 
											label: "Datum ausw&auml;hlen", 
											menu: oCalendarMenu, 
											container: "datefields" });
	
	
			oButton.on("appendTo", function () {
			
				// Create an empty body element for the Overlay instance in order 
				// to reserve space to render the Calendar instance into.
		
				oCalendarMenu.setBody("&#32;");
		
				oCalendarMenu.body.id = "calendarcontainer";

			
			});
	
	
			// Add a listener for the "click" event.  This listener will be
			// used to defer the creation the Calendar instance until the 
			// first time the Button's Overlay instance is requested to be displayed
			// by the user.
	
			oButton.on("click", onButtonClick);
		
		});
		
</script>
