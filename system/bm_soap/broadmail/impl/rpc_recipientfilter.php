<?php
    /**
     * (C)opyright 2002-2009 optivo GmbH, Wallstraße 16, 10179 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface RecipientFilterWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Feb 24 16:21:04 CET 2010
     */

    class BroadmailRpcRecipientFilterWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcRecipientFilterWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'RecipientFilter', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function getName( $p1) {
            return $this->_call('getName', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setName( $p1,  $p2) {
            return $this->_call('setName', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function create( $p1,  $p2,  $p3,  $p4,  $p5) {
            return $this->_call('create', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.lang.String', $p3), 'p5' => $this->_convert('java.lang.String', $p4), 'p6' => $this->_convert('java.lang.String[]', $p5)));
        }

        function getDescription( $p1) {
            return $this->_call('getDescription', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setDescription( $p1,  $p2) {
            return $this->_call('setDescription', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getCount() {
            return $this->_call('getCount', array('p1' => $this->sessionId));
        }

        function getDataSet() {
            return $this->_call('getDataSet', array('p1' => $this->sessionId));
        }

        function getIds() {
            return $this->_call('getIds', array('p1' => $this->sessionId));
        }

        function isInUse( $p1) {
            return $this->_call('isInUse', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function createTemporary( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('createTemporary', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Boolean', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.String', $p3), 'p5' => $this->_convert('java.lang.String[]', $p4)));
        }

        function isTemporary( $p1) {
            return $this->_call('isTemporary', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function beginConditionModification( $p1) {
            return $this->_call('beginConditionModification', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function commitConditionModification( $p1) {
            return $this->_call('commitConditionModification', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function cancelConditionModification( $p1) {
            return $this->_call('cancelConditionModification', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function clearConditions( $p1) {
            return $this->_call('clearConditions', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function addAndCondition( $p1,  $p2,  $p3,  $p4,  $p5) {
            return $this->_call('addAndCondition', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.lang.String', $p3), 'p5' => $this->_convert('java.lang.String', $p4), 'p6' => $this->_convert('java.lang.String[]', $p5)));
        }

        function addOrCondition( $p1,  $p2,  $p3,  $p4,  $p5) {
            return $this->_call('addOrCondition', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.lang.String', $p3), 'p5' => $this->_convert('java.lang.String', $p4), 'p6' => $this->_convert('java.lang.String[]', $p5)));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>
