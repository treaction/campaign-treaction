<?php
    /**
     * (C)opyright 2002-2007 optivo GmbH, Stralauer Allee 2, 10247 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface AttachmentWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Oct 08 15:25:48 CEST 2008
     */

    class BroadmailRpcAttachmentWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcAttachmentWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'Attachment', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function getName( $p1) {
            return $this->_call('getName', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setName( $p1,  $p2) {
            return $this->_call('setName', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getSize( $p1) {
            return $this->_call('getSize', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function create( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('create', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.String', $p3), 'p5' => $this->_convert('java.lang.Byte[]', $p4)));
        }

        function getContent( $p1) {
            return $this->_call('getContent', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getFilename( $p1) {
            return $this->_call('getFilename', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setMimeType( $p1,  $p2) {
            return $this->_call('setMimeType', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getMimeType( $p1) {
            return $this->_call('getMimeType', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getCount() {
            return $this->_call('getCount', array('p1' => $this->sessionId));
        }

        function getAllIds() {
            return $this->_call('getAllIds', array('p1' => $this->sessionId));
        }

        function setContent( $p1,  $p2) {
            return $this->_call('setContent', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Byte[]', $p2)));
        }

        function setFilename( $p1,  $p2) {
            return $this->_call('setFilename', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getDataSet() {
            return $this->_call('getDataSet', array('p1' => $this->sessionId));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>