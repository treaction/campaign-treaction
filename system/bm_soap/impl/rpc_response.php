<?php
    /**
     * (C)opyright 2002-2007 optivo GmbH, Stralauer Allee 2, 10247 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface ResponseWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Oct 08 15:26:13 CEST 2008
     */

    class BroadmailRpcResponseWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcResponseWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'Response', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function getRecipientResponseCount( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getRecipientResponseCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String', $p3), 'p5' => $this->_convert('java.lang.Long', $p4)));
        }

        function getAllRecipientResponseCounts( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getAllRecipientResponseCounts', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String[]', $p3), 'p5' => $this->_convert('java.lang.Long', $p4)));
        }

        function getMailingResponseCount( $p1,  $p2) {
            return $this->_call('getMailingResponseCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.Long', $p2)));
        }

        function getRecipientResponses( $p1,  $p2,  $p3,  $p4,  $p5,  $p6) {
            return $this->_call('getRecipientResponses', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String', $p3), 'p5' => $this->_convert('java.lang.Long', $p4), 'p6' => $this->_convert('java.lang.Integer', $p5), 'p7' => $this->_convert('java.lang.Integer', $p6)));
        }

        function getMailingResponses( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getMailingResponses', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.Integer', $p3), 'p5' => $this->_convert('java.lang.Integer', $p4)));
        }

        function getBounceCounter( $p1,  $p2) {
            return $this->_call('getBounceCounter', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function resetBounceCounter( $p1) {
            return $this->_call('resetBounceCounter', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function getBounceCounterThreshold( $p1) {
            return $this->_call('getBounceCounterThreshold', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function isBounceCounterThresholdExeeded( $p1) {
            return $this->_call('isBounceCounterThresholdExeeded', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>