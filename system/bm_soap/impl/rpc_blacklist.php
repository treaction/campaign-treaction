<?php
    /**
     * (C)opyright 2002-2007 optivo GmbH, Stralauer Allee 2, 10247 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface BlacklistWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Oct 08 15:25:54 CEST 2008
     */

    class BroadmailRpcBlacklistWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcBlacklistWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'Blacklist', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function add( $p1,  $p2) {
            return $this->_call('add', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function contains( $p1) {
            return $this->_call('contains', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function addAll( $p1,  $p2) {
            return $this->_call('addAll', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String[]', $p1), 'p3' => $this->_convert('java.lang.String[]', $p2)));
        }

        function remove( $p1) {
            return $this->_call('remove', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function containsAll( $p1) {
            return $this->_call('containsAll', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String[]', $p1)));
        }

        function removeAll( $p1) {
            return $this->_call('removeAll', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String[]', $p1)));
        }

        function getCount() {
            return $this->_call('getCount', array('p1' => $this->sessionId));
        }

        function getDataSet() {
            return $this->_call('getDataSet', array('p1' => $this->sessionId));
        }

        function isBlacklisted( $p1) {
            return $this->_call('isBlacklisted', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function areBlacklisted( $p1) {
            return $this->_call('areBlacklisted', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String[]', $p1)));
        }

        function getFirstMatchingEntry( $p1) {
            return $this->_call('getFirstMatchingEntry', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function getAllEntries() {
            return $this->_call('getAllEntries', array('p1' => $this->sessionId));
        }

        function getReason( $p1) {
            return $this->_call('getReason', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function getCreated( $p1) {
            return $this->_call('getCreated', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>