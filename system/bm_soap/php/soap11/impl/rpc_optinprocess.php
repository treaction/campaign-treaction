<?php
    /**
     * (C)opyright 2002-2007 optivo GmbH, Stralauer Allee 2, 10247 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface OptinProcessWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Oct 08 15:26:04 CEST 2008
     */

    class BroadmailRpcOptinProcessWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcOptinProcessWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'OptinProcess', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function getName( $p1) {
            return $this->_call('getName', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setName( $p1,  $p2) {
            return $this->_call('setName', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getType( $p1) {
            return $this->_call('getType', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getDescription( $p1) {
            return $this->_call('getDescription', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setDescription( $p1,  $p2) {
            return $this->_call('setDescription', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getIds() {
            return $this->_call('getIds', array('p1' => $this->sessionId));
        }

        function getConfirmationUrl( $p1) {
            return $this->_call('getConfirmationUrl', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setConfirmationUrl( $p1,  $p2) {
            return $this->_call('setConfirmationUrl', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getConfirmationMailingId( $p1) {
            return $this->_call('getConfirmationMailingId', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setConfirmationMailingId( $p1,  $p2) {
            return $this->_call('setConfirmationMailingId', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2)));
        }

        function createSingleOptinProcess( $p1,  $p2) {
            return $this->_call('createSingleOptinProcess', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function createConfirmedOptinProcess( $p1,  $p2,  $p3) {
            return $this->_call('createConfirmedOptinProcess', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.Long', $p3)));
        }

        function createDoubleOptinProcess( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('createDoubleOptinProcess', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.Long', $p3), 'p5' => $this->_convert('java.lang.String', $p4)));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>