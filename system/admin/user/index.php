<?php
header('Content-Type: text/html; charset=UTF-8');
#error_reporting(0); 
session_start();

include('../../library/ems_package.php');
include('../../library/util.php');

function zahl_format($zahl) {
    $zahl = number_format($zahl, 0, 0, '.');
    
    return $zahl;
}

function datum_de($date_time, $form) {
    $date_1 = explode(' ', $date_time);
    $datum = explode('-', $date_1[0]);
    $jahr = $datum[0];
    $monat = $datum[1];
    $tag = $datum[2];

    $zeit = $date_1[1];
    $zeit_teile = explode(':', $date_1[1]);
    if ($form == 'DE' || $zeit_teile[0] == '00' || $form == 'zeit') {
        $h = $zeit_teile[0];
    } else {
        $h = $zeit_teile[0];
    }
    
    $m = $zeit_teile[1];
    $zeit = $h . ":" . $m;
    $de = $tag . "." . $monat . "." . $jahr;

    $dat_zeit = $de . " " . $zeit;
    if ($form == 'dat') {
        return $de;
    } elseif ($form == '' || $form == "DE") {
        return $dat_zeit;
    } elseif ($form == 'zeit') {
        return $zeit;
    } elseif ($form == 'zeit_bm') {
        return $zeit;
    }
}

$mandant = $_SESSION['mandant'];
$mID = $_SESSION['mID'];

include('../../db_pep.inc.php');
#var_dump($verbindung_pep);
$user_qry = mysql_query(
    "SELECT * FROM benutzer WHERE mid = '$mID' AND active = 1 ORDER BY benutzer_name ASC",
    $verbindung_pep
);

$user_all = mysql_num_rows($user_qry);
#var_dump($mID);
if ($user_all == 0) {
    $data_leer = "<tr><td height='30' colspan='30' align='center'><span style='color:#666666'>Keine Benutzer gefunden</span</td></tr>";
}

if ($user_all > 0) {
    $m = new emsPackage();

    while ($uz = mysql_fetch_array($user_qry)) {
        $user_zugang = '';
        $user_menu = '';
        $user_id = $uz['benutzer_id'];
        $user_name = $uz['name'];
        $user = $uz['benutzer_name'];
        $user_anrede = $uz['anrede'];
        $user_vorname = $uz['vorname'];
        $user_nachname = $uz['nachname'];
        $user_email = $uz['email'];
        $user_module = $uz['menu'];
        $user_abteilung = util::getAbteilungName($uz['abteilung']);
        $user_telefon = $uz['telefon'];

        $last_login = $uz['timestamp'];
        if ($last_login == 0) {
            $last_login = '';
        }

        $user_untermandantArr = $m->getUserUntermandantID($user_id);
        foreach ($user_untermandantArr as $uID) {
            $m_name = $m->getUntermandantAbkz($uID);
            $user_zugang .= $m_name . ",";
        }

        $user_zugang = substr($user_zugang, 0, -1);

        $user_moduleArr = explode(',', $user_module);
        foreach ($user_moduleArr as $moID) {
            $mo_name = $m->getPackageName($moID);
            $user_menu .= $mo_name . ",";
        }

        $user_menu = substr($user_menu, 0, -1);

        if ($j == 3 || !$j) {
            $j = 1;
        }
        
        if ($j == 1) {
            $bg_color = '#FFFFFF';
        } elseif ($j == 2) {
            $bg_color = '#E4E4E4';
        }

        $data .= "<tr onclick=\"doIt_user(event,'" . $user_id . "');\" bgcolor='" . $bg_color . "' onMouseOver=\"this.bgColor='#A6CBFF'\" onMouseOut=\"this.bgColor='" . $bg_color . "' \" style='cursor:pointer' >";
        $data .= "<td style='font-weight:bold;'>" . $user . "</td>";
        $data .= "<td>" . $user_anrede . "</td>";
        $data .= "<td>" . $user_vorname . "</td>";
        $data .= "<td>" . $user_nachname . "</td>";
        $data .= "<td>" . $user_email . "</td>";
        $data .= "<td>" . $user_abteilung . "</td>";
        $data .= "<td>" . $user_telefon . "</td>";
        $data .= "<td></td>";
        $data .= "</tr>";
        $j++;
    }
}

$_SESSION['datensaetze_unsub'] = $datensaetze;
?>
<div id="fakeContainer">
    <table class="scrollTable2" id="scrollTable2" cellpadding="0" cellspacing="0" border="0" bgcolor="#F5F5F5">
        <tr>
            <th>Anmeldename</th>
            <th>Anrede</th>
            <th>Vorname</th>
            <th>Nachname</th>
            <th>Email</th>
            <th>Abteilung</th>
            <th>Handy</th>
            <th style="width:100%"></th>
        </tr>
        <?php print $data; ?>
    </table>
</div>
<div style="background-color:#8e8901;color:#000000;font-size:11px;height:23px;width:100%;">
    <div style="float:left;margin-top:1px;">
        <img src='img/treaction_gruen_seperator.png' />
    </div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
    <?php
        if ($user_all > 0) {
            print zahl_format($user_all) . " Benutzer <a href='#' id='show_info' style='color:#9900FF'>&nbsp;</a>";
        } else {
            print 'Keine Benutzer vorhanden &nbsp;&nbsp;&nbsp;';
        }
    ?>
    </div>
</div>

<input type="hidden" name="page_sub" id="page_sub" value="unsub" /> 

<script type="text/javascript">
    wi = document.documentElement.clientWidth;
    hi = document.documentElement.clientHeight;
    
    if (uBrowser == 'Firefox') {
        hi = hi-142;
        wi = wi-205;
    } else {
        hi = hi-143;
        wi = wi-205;
    }
    
    document.getElementById("fakeContainer").style.width = wi+"px";
    document.getElementById("fakeContainer").style.height = hi+"px";

    function Resize() {
        setRequest('admin/user/','admin_user');
    }

    window.onresize = Resize;

    var mySt = new superTable("scrollTable2", {
        cssSkin : "sSky",
        fixedCols : 0,
        headerRows : 1
    });
</script>