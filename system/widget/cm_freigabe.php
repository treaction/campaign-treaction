<?php
$start_datum = date("Y-m-d", time());

$sql = mysql_query("SELECT 
						IF(LENGTH(`k_name`) > 35, CONCAT(LEFT(`k_name`, 32), '...'), `k_name`) AS kname,k_id, versandsystem, datum, agentur, gebucht, k_name, status
					FROM $kampagne_db
					WHERE k_id=nv_id
					AND datum LIKE '".$start_datum."%'
					AND k_id>1
					ORDER BY datum DESC",$verbindung);

$sql2 = mysql_query("SELECT 
						IF(LENGTH(`k_name`) > 35, CONCAT(LEFT(`k_name`, 32), '...'), `k_name`) AS kname,k_id, versandsystem, datum, agentur, gebucht, k_name, status
					FROM $kampagne_db
					WHERE k_id=nv_id
					AND (status >= '20' AND status < '22') OR status = 23 
					AND k_id>1
					ORDER BY datum DESC",$verbindung);
					
$j = 1;
$l = 1;
$n = 1;
$freigabe_cnt = 0;
$campaign_cnt = 0;
$campaign_offen_cnt = 0;
$reporting_cnt = 0;
$tr_today = '';
$tr_freigabe = '';

if (mysql_num_rows($sql) > 0) {

	while ($kz = mysql_fetch_array($sql)) {
		
		$kid = $kz['k_id'];
		$k_name = $kz['kname'];
		$k_name_full = $kz['k_name'];
		$asp = $kz['versandsystem'];
		if ($asp == 'BM') {
			$datum = util::bmTime($kz['datum']);
		} else {
			$datum = $kz['datum'];
		}
		
		$datum = util::datum_de($datum,'');
		$agentur = $kz['agentur'];
		$status = $kz['status'];
		
		if ($status <= 3) {
		
			if ($j==1 || $j==3) {
				$bg_color = "#FFFFFF";
				$j=1;
			} elseif ($j==2) {
				$bg_color = "#E4E4E4";
			} 
		
			$tr_freigabe .= "<tr style='background-color:".$bg_color."'>";
			$tr_freigabe .= "<td style='white-space:nowrap;'><a href='#' onClick='setRequest(\"kampagne/k_main.php?skid=".$kid."\",\"k\");' title='".$k_name_full."'>".util::cutString($k_name,25)."</a><br/><span style='color:#9900FF'>".util::cutString($agentur,25)."</span></td>";
			$tr_freigabe .= "<td align='center'>".$datum."</td>";
			$tr_freigabe .= "<td align='center' class='medium'>".util::getCampaignStatusName($status)."</td>";
			$tr_freigabe .= "</td>";
			$tr_freigabe .= "</tr>";
			
			$freigabe_cnt++;
			
			$j++;
		
		} 
		
		$v_datum = explode(" ",$kz['datum']);
		
		if ($v_datum[0] == $start_datum) {
		
				if ($l==1 || $l==3) {
					$bg_color = "#FFFFFF";
					$l=1;
				} elseif ($l==2) {
					$bg_color = "#E4E4E4";
				} 
			
				$tr_today .= "<tr style='background-color:".$bg_color."'>";
				$tr_today .= "<td style='white-space:nowrap;'><a href='#' onClick='setRequest(\"kampagne/k_main.php?skid=".$kid."\",\"k\");' title='".$k_name_full."'>".$k_name."</a><br/><span style='color:#9900FF'>".$agentur."</span></td>";
				$tr_today .= "<td align='center'>".$datum."</td>";
				$tr_today .= "<td align='center' class='medium'>".util::getCampaignStatusName($status)."</td>";
				$tr_today .= "</td>";
				$tr_today .= "</tr>";
				
				if ($status <= 4) {
					$campaign_offen_cnt++;
				} else {
					$campaign_cnt++;
				}
				
				
				$l++;
		
		} 
		
		
	}

} else {
	$tr_freigabe = "<tr><td colspan='5'><span style='color:#333333'>Keine Daten vorhanden</span></td></tr>";
	$tr_today = "<tr><td colspan='5'><span style='color:#333333'>Keine Daten vorhanden</span></td></tr>";
}

$tr_reporting = '';
if (mysql_num_rows($sql2) > 0) {

	while ($kz = mysql_fetch_array($sql2)) {
		
		$kid = $kz['k_id'];
		$k_name = $kz['kname'];
		$k_name_full = $kz['k_name'];
		$asp = $kz['versandsystem'];
		if ($asp == 'BM') {
			$datum = util::bmTime($kz['datum']);
		} else {
			$datum = $kz['datum'];
		}
		
		$datum = util::datum_de($datum,'');
		$agentur = $kz['agentur'];
		$status = $kz['status'];
		
		if ($n==1 || $n==3) {
			$bg_color = "#FFFFFF";
			$n=1;
		} elseif ($n==2) {
			$bg_color = "#E4E4E4";
		} 
		
			$tr_reporting .= "<tr style='background-color:".$bg_color."'>";
			$tr_reporting .= "<td style='white-space:nowrap;'><a href='#' onClick='setRequest(\"kampagne/k_main.php?skid=".$kid."\",\"k\");' title='".$k_name_full."'>".$k_name."</a><br/><span style='color:#9900FF'>".$agentur."</span></td>";
			$tr_reporting .= "<td align='center'>".$datum."</td>";
			$tr_reporting .= "<td align='center' class='medium'>".util::getCampaignStatusName($status)."</td>";
			$tr_reporting .= "</td>";
			$tr_reporting .= "</tr>";
			
			$reporting_cnt++;
			
			$n++;
	}
	
} 
$campaign_table = '';

if ($campaign_offen_cnt == 0) {
	$campaign_offen_cnt = "<img src='img/Tango/16/actions/dialog-apply.png' />";
} else {

	$campaign_offen_cnt = $campaign_offen_cnt." offen";
	$campaign_table = '
	<div id="campaignTable" style="display:none;">
		<table cellpadding="0" cellspacing="0" class="table_widget">
			<tr class="trHeader">
				<td width="150">Kampagne</td>
				<td align="center">Versanddatum</td>
				<td align="center">Status</td>
			</tr>
			'.$tr_today.'
		</table>
	</div>';
	
}

$freigabe_table = '';
if ($freigabe_cnt == 0) {
	$freigabe_cnt = "<img src='img/Tango/16/actions/dialog-apply.png' />";
} else {
	$freigabe_cnt = $freigabe_cnt." offen";
	$freigabe_table = '
	<div id="freigabeTable" style="display:none;">
		<table cellpadding="0" cellspacing="0" class="table_widget">
			<tr class="trHeader">
				<td width="150">Kampagne</td>
				<td align="center">Versanddatum</td>
				<td align="center">Status</td>
			</tr>
			'.$tr_freigabe.'
		</table>
	</div>';
}

if ($reporting_cnt == 0) {
	$reporting_cnt = "<img src='img/Tango/16/actions/dialog-apply.png' />";
} else {
	$reporting_cnt = $reporting_cnt." offen";
	$reporting_table = '
	<div id="reportingTable" style="display:none;">
		<table cellpadding="0" cellspacing="0" class="table_widget">
			<tr class="trHeader">
				<td width="150">Kampagne</td>
				<td align="center">Versanddatum</td>
				<td align="center">Status</td>
			</tr>
			'.$tr_reporting.'
		</table>
	</div>';
}
?>
<div class="ubersicht_campaign" title="einblenden/ausblenden" onclick="menu_anz('campaignTable');">
	<div style="float:left">Kampagnen [Heute]</div>
	<div style="float:right"><?php print $campaign_offen_cnt; ?></div>
</div>
<?php print $campaign_table; ?>


<div class="ubersicht_campaign" title="einblenden/ausblenden" onclick="menu_anz('freigabeTable');">
	<div style="float:left">Freigaben</div>
	<div style="float:right"><?php print $freigabe_cnt; ?></div>
</div>
<?php print $freigabe_table; ?>


<div class="ubersicht_campaign" title="einblenden/ausblenden" onclick="menu_anz('reportingTable');">
	<div style="float:left">Reportings</div>
	<div style="float:right"><?php print $reporting_cnt; ?></div>
</div>
<?php print $reporting_table; ?>




