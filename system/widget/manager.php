<?php
header('Content-Type: text/xml; charset=ISO-8859-1');
session_start();

$mandant = $_SESSION['mandant'];
$mandantID = $_SESSION['mID'];
$benutzer_id = $_SESSION['benutzer_id'];

include('../db_connect.inc.php');
require_once('../library/util.php');
require_once('../library/chart.php');
include('../library/ems_package.php');
include('../db_pep.inc.php');

function getWidgetDescription($widget) {
    switch ($widget) {
        case 'news.php':
            $descArr = array(
                "100",
                "News",
                "Dieses Widget informiert Sie über Neuigkeiten, Updates und Tipps rund um das EMS."
            );
            break;
        
        case 'cm_last5.php':
            $descArr = array(
                "0",
                "Letzten Versendungen",
                "Anzeige der letzten 5 Kampagnen."
            );
            break;
        
        case 'cm_freigabe.php':
            $descArr = array(
                "2",
                "&Uuml;bersicht",
                "Zeigt alle offenen Aufgaben von Heute an."
            );
            break;
        
        case 'cm_low_performance.php':
            $descArr = array(
                "3",
                "Low Performance",
                "Zeigt die letzten Kampagnen an, welche die Zielvorgaben nicht erreicht haben."
            );
            break;
        
        case 'cm_top_performance.php':
            $descArr = array(
                "4",
                "Top Performance",
                "Zeigt die letzten Kampagnen mit den besten Performancewerten an."
            );
            break;
        
        case 'cm_c_year.php':
            $descArr = array(
                "1",
                "Kampagnen pro Monat",
                "Zeigt die Anzahl der Kampagnen pro Monat als Diagramm."
            );
            break;
        
        case 'dm_last5.php':
            $descArr = array(
                "5",
                "Letzten Lieferungen",
                "Anzeige der letzten 5 Lieferungen."
            );
            break;
        
        case 'dm_l_year.php':
            $descArr = array(
                "6",
                "Lieferungen pro Monat",
                "Zeigt die Liefermenge pro Monat als Diagramm."
            );
            break;
        
        case 'bl_last5.php':
            $descArr = array(
                "7",
                "Letzten Blacklisteintr&auml;ge",
                "Zeigt die letzten 5 Blacklisteinträge."
            );
            break;
        
        case 'bl_year.php':
            $descArr = array(
                "8",
                "Blacklisteintr&auml;ge pro Monat",
                "Zeigt die Anzahl der Blacklisteintr&auml;ge pro Monat als Diagramm."
            );
            break;
        
        case 'bl_a_year.php':
            $descArr = array(
                "9",
                "Abmelder pro Monat",
                "Zeigt die Anzahl der Abmelder pro Monat als Diagramm."
        );
            break;
    }

    return $descArr;
}

function getGeneralWidgets($widget) {
    global $tab_g;
    global $widgets_user;
    global $info_text;
    
    $mandant = $_SESSION['mandant'];
    
    include('../db_connect.inc.php');
    require_once('../library/util.php');
    require_once('../library/chart.php');

    $widgetsArr = array("news.php");

    $cnt = count($widgetsArr);

    $tab_g = '<div id="tab_g" class="widget_manager_content">';
    $tab_g .= '<table><tr>' . $info_text . '</tr>';

    foreach ($widgetsArr as $w) {
        $wDataArr = getWidgetDescription($w);
        
        ob_start();
        include($w);
        $preview = ob_get_contents();
        ob_end_clean();

        if (in_array($wDataArr[0], $widgets_user)) {
            $checked = "checked";
            $label = '<label for="checkbox' . $wDataArr[0] . '" onclick="toggle_img(\'img_w' . $wDataArr[0] . '\');"><img src="img/button_widget_rot.png" id="img_w' . $wDataArr[0] . '" style="float:left;margin-top:-5px;" /></label>';
        } else {
            $checked = "";
            $label = '<label for="checkbox' . $wDataArr[0] . '" onclick="toggle_img(\'img_w' . $wDataArr[0] . '\');"><img src="img/button_widget_gruen.png" id="img_w' . $wDataArr[0] . '" style="float:left;margin-top:-5px;" /></label>';
        }

        $tab_g .= '
            <tr>
                <td valign="top">' . $preview . '</td>
                <td valign="top">
                    <div class="widget_beschreibung">
                        <h1>' . $wDataArr[1] . '</h1>' . 
                        $wDataArr[2] . 
                        '<br /><br /><br /><br /><br />' . 
                        $label . 
                        '<input type="checkbox" id="checkbox' . $wDataArr[0] . '" name="widgets[]" value="' . $wDataArr[0] . '" style="display:none" ' . $checked . ' />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2"><hr noshade /></td>
            </tr>'
        ;
    }

    $tab_g .= '</table></div>';
}

function getCampaignManagerWidgets() {
    global $li_cm;
    global $tab_cm;
    global $widgets_user;
    global $info_text;
    
    $mandant = $_SESSION['mandant'];
    
    include('../db_connect.inc.php');
    require_once('../library/util.php');
    require_once('../library/chart.php');

    $widgetsArr = array(
        "cm_last5.php",
        "cm_freigabe.php",
        "cm_low_performance.php",
        "cm_top_performance.php",
        "cm_c_year.php"
    );

    $cnt = count($widgetsArr);
    $li_cm = '<li><a href="#tab_cm"><em>Campaign Manager (' . $cnt . ')</em></a></li>';

    $tab_cm = '<div id="tab_cm" class="widget_manager_content">';
    $tab_cm .= '<table><tr>' . $info_text . '</tr>';

    foreach ($widgetsArr as $w) {
        $wDataArr = getWidgetDescription($w);
        
        ob_start();
        include($w);
        $preview = ob_get_contents();
        ob_end_clean();

        if (in_array($wDataArr[0], $widgets_user)) {
            $checked = "checked";
            $label = '<label for="checkbox' . $wDataArr[0] . '" onclick="toggle_img(\'img_w' . $wDataArr[0] . '\');"><img src="img/button_widget_rot.png" id="img_w' . $wDataArr[0] . '" style="float:left;margin-top:-5px;" /></label>';
        } else {
            $checked = "";
            $label = '<label for="checkbox' . $wDataArr[0] . '" onclick="toggle_img(\'img_w' . $wDataArr[0] . '\');"><img src="img/button_widget_gruen.png" id="img_w' . $wDataArr[0] . '" style="float:left;margin-top:-5px;" /></label>';
        }

        $tab_cm .= '
            <tr>
                <td valign="top">' . $preview . '</td>
                <td valign="top">
                    <div class="widget_beschreibung">
                        <h1>' . $wDataArr[1] . '</h1>' . 
                        $wDataArr[2] . '<br /><br /><br /><br /><br />' . 
                        $label . 
                        '<input type="checkbox" id="checkbox' . $wDataArr[0] . '" name="widgets[]" value="' . $wDataArr[0] . '" style="display:none" ' . $checked . ' />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2"><hr noshade /></td>
            </tr>'
        ;
    }

    $tab_cm .= '</table></div>';
}

function getDataManagerWidgets() {
    global $li_dm;
    global $tab_dm;
    global $widgets_user;
    global $info_text;
    
    $mandant = $_SESSION['mandant'];
    
    include('../db_connect.inc.php');
    require_once('../library/util.php');
    require_once('../library/chart.php');

    $widgetsArr = array(
        "dm_last5.php",
        "dm_l_year.php"
    );

    $cnt = count($widgetsArr);
    $li_dm = '<li><a href="#tab_dm"><em>Data Manager (' . $cnt . ')</em></a></li>';

    $tab_dm = '<div id="tab_dm" class="widget_manager_content">';
    $tab_dm .= '<table><tr>' . $info_text . '</tr>';

    foreach ($widgetsArr as $w) {
        $wDataArr = getWidgetDescription($w);
        
        ob_start();
        include($w);
        $preview = ob_get_contents();
        ob_end_clean();

        if (in_array($wDataArr[0], $widgets_user)) {
            $checked = "checked";
            $label = '<label for="checkbox' . $wDataArr[0] . '" onclick="toggle_img(\'img_w' . $wDataArr[0] . '\');"><img src="img/button_widget_rot.png" id="img_w' . $wDataArr[0] . '" style="float:left;margin-top:-5px;" /></label>';
        } else {
            $checked = "";
            $label = '<label for="checkbox' . $wDataArr[0] . '" onclick="toggle_img(\'img_w' . $wDataArr[0] . '\');"><img src="img/button_widget_gruen.png" id="img_w' . $wDataArr[0] . '" style="float:left;margin-top:-5px;" /></label>';
        }

        $tab_dm .= '
            <tr>
                <td valign="top">' . $preview . '</td>
                <td valign="top">
                    <div class="widget_beschreibung">
                        <h1>' . $wDataArr[1] . '</h1>' . 
                        $wDataArr[2] . '<br /><br /><br /><br /><br />' . 
                        $label . '
                            <input type="checkbox" id="checkbox' . $wDataArr[0] . '" name="widgets[]" value="' . $wDataArr[0] . '" style="display:none" ' . $checked . ' />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2"><hr noshade /></td>
            </tr>'
        ;
    }

    $tab_dm .= '</table></div>';
}

function getBlWidgets() {
    global $li_bl;
    global $tab_bl;
    global $widgets_user;
    global $mandantID;
    global $info_text;
    
    $mandant = $_SESSION['mandant'];
    
    include('../db_connect.inc.php');
    require_once('../library/util.php');
    require_once('../library/chart.php');

    $widgetsArr = array(
        "bl_last5.php",
        "bl_year.php",
        "bl_a_year.php"
    );

    $cnt = count($widgetsArr);
    $li_bl = '<li><a href="#tab_bl"><em>Blacklist-Abmelde-Manager (' . $cnt . ')</em></a></li>';

    $tab_bl = '<div id="tab_bl" class="widget_manager_content">';
    $tab_bl .= '<table><tr>' . $info_text . '</tr>';

    foreach ($widgetsArr as $w) {
        $wDataArr = getWidgetDescription($w);
        
        ob_start();
        include($w);
        $preview = ob_get_contents();
        ob_end_clean();

        if (in_array($wDataArr[0], $widgets_user)) {
            $checked = "checked";
            $label = '<label for="checkbox' . $wDataArr[0] . '" onclick="toggle_img(\'img_w' . $wDataArr[0] . '\');"><img src="img/button_widget_rot.png" id="img_w' . $wDataArr[0] . '" style="float:left;margin-top:-5px;" /></label>';
        } else {
            $checked = "";
            $label = '<label for="checkbox' . $wDataArr[0] . '" onclick="toggle_img(\'img_w' . $wDataArr[0] . '\');"><img src="img/button_widget_gruen.png" id="img_w' . $wDataArr[0] . '" style="float:left;margin-top:-5px;" /></label>';
        }

        $tab_bl .= '
            <tr>
                <td valign="top">' . $preview . '</td>
                <td valign="top">
                    <div class="widget_beschreibung">
                        <h1>' . $wDataArr[1] . '</h1>' . 
                        $wDataArr[2] . '<br /><br /><br /><br /><br />' . 
                        $label . 
                        '<input type="checkbox" id="checkbox' . $wDataArr[0] . '" name="widgets[]" value="' . $wDataArr[0] . '" style="display:none" ' . $checked . ' />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2"><hr noshade /></td>
            </tr>'
        ;
    }

    $tab_bl .= '</table></div>';
}

$w_sql = mysql_query(
    "SELECT widget FROM benutzer WHERE benutzer_id = '$benutzer_id'",
    $verbindung_pep
);
$wz = mysql_fetch_array($w_sql);
$widgets_user = unserialize($wz['widget']);

$info_text = "<td colspan='2' style='color:#666666;border-bottom:1px solid #666666;padding:10px;'>Um Widgets hinzuzuf&uuml;gen oder zu entfernen klicken Sie auf die entsprechenden Buttons. <br />Anschlie&szlig;end k&ouml;nnen Sie die Auswahl mit <b>'&uuml;bernehmen'</b> speichern.</td>";

$m = new emsPackage();
$user_package = $m->getUserModules($benutzer_id);

getGeneralWidgets();


// cr-neu
$excludeInternationalMandantenId = array(
    109,
    110
);

if (in_array(1, $user_package) || in_array(2, $user_package)) {
    getCampaignManagerWidgets();
}

if (in_array(3, $user_package) || in_array(4, $user_package)) {
    #if (in_array(intval($mandantID), $excludeInternationalMandantenId) !== true) {
        getDataManagerWidgets();
    #}
}

if (in_array(5, $user_package) || in_array(6, $user_package)) {
    #if (in_array(intval($mandantID), $excludeInternationalMandantenId) !== true) {
        getBlWidgets();
    #}
}
?>

<div id="tab_widget" class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class='selected'><a href="#tab_news"><em>Allgemein (1)</em></a></li>
        <?php
            echo $li_cm;
            echo $li_dm;
            echo $li_bl;
        ?>
    </ul>
    <div class="yui-content" style="margin:0px;padding:0px;">
        <?php 
            echo $tab_g;
            echo $tab_cm;
            echo $tab_dm;
            echo $tab_bl;
        ?>
    </div>
    <input type="checkbox" name="widgets[]" value="" style="display:none" checked />
</div>
<script type="text/javascript">
    var tabView_widget = new YAHOO.widget.TabView('tab_widget');

    function toggle_img(img) {
        var obj;

        obj = document.getElementById(img);
	
        var plus = new Image();
        plus.src = "img/button_widget_gruen.png";
        var minus = new Image();
        minus.src = "img/button_widget_rot.png";
	
        obj.src = (obj.src==plus.src) ? minus.src:plus.src;
    }
</script>