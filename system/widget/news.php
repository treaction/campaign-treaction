<div style="height:250px;overflow:auto;border:1px solid #CCCCCC;padding:5px;">
    <table cellpadding="0" cellspacing="0" class="table_news">
        <tr>
            <td bgcolor="#FFFFFF" class="news_hd">
                 Neu: Widgets! <span class="news_date">28.07.2010</span>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF" valign="top" class="news_bd">
                    Sie haben ab jetzt die M&ouml;glichkeit, die Startseite individuell mit Widgets (kleine Infofenster oder Miniprogramme) zu konfigurieren.<br />
					<br />Klicken Sie einfach auf <b>&quot;Widgets hinzuf&uuml;gen/entfernen&quot;</b> um die Widgets auszuw&auml;hlen.<br />Per &quot;Drag&amp;Drop&quot; lassen sich die Widgets auch verschieben.<br /><br />
					Weitere Widgets k�nnen auf Wunsch erstellt werden.
             </td>
        </tr>
    </table>
</div>

