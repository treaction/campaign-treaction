<?php
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();

$mandant = $_SESSION['mandant'];
$mandantID = $_SESSION['mID'];
$benutzer_id = $_SESSION['benutzer_id'];

include('../db_connect.inc.php');
require_once('../library/util.php');
require_once('../library/chart.php');
include('../db_pep.inc.php');

if (isset($_POST['widgets'])) {
    if (count($_POST['widgets']) > 0) {
        $widget_new = serialize($_POST['widgets']);
    } else {
        $widget_new = '';
    }

    $w_sql = mysql_query(
        "UPDATE benutzer SET widget = '$widget_new' WHERE benutzer_id = '$benutzer_id'",
        $verbindung_pep
    );
}

$w_sql = mysql_query(
    "SELECT widget, vorname, nachname FROM benutzer WHERE benutzer_id = '$benutzer_id'",
    $verbindung_pep
);
$wz = mysql_fetch_array($w_sql);
$widgets_user = unserialize($wz['widget']);
$widgets = array(0, 1, 2, 3, 4, 5, 6, 7);

$vorname = $wz['vorname'];
$nachname = $wz['nachname'];

if ($vorname != '' && $nachname != '') {
    $user_fullname = $vorname . ' ' . $nachname;
}

# Widgets generieren und anzeigen
$cnt_w = count($widgets_user);
if ($widgets_user[0] != '') {
    print "<script type='text/javascript'>";

    foreach ($widgets_user as $w) {
        if ($w != '') {
            $w_id = "Rec" . $w;
            print "document.getElementById(\"" . $w_id . "\").style.display=\"\";";
        }
    }

    print "</script>";
} else {
    print '
        <div class="FixedWidth">
            <div class="Rec">
                <div class="Handle"><img src="img/Oxygen/22/actions/help-about.png" /> Herzlich Willkommen!</div>
                <div class="Info_home">
                    <span style="font-weight:bold;">Hallo ' . $user_fullname . '!</span><br /><br />
                        Sie k�nnen diese Startseite mit Hilfe von Widgets selbst konfigurieren.<br /><br />
                        Klicken Sie dazu einfach auf <b>"Widgets hinzuf&uuml;gen/entfernen"</b> in der oberen Men&uuml;leiste und w&auml;hlen Sie die Widgets aus, die angezeigt werden sollen.<br /><br />
                        Sie haben jederzeit die M�glichkeit per "Drag&Drop" die Widgets neu anzuordnen.<br /><br /><br />
                        - Ihr EMS-Team<br /><br />
                </div>
            </div>
        </div>'
    ;
}
?>
<div id="eyebrow"></div>
<div class="FixedWidth" id="fixW">
    <div id="Column1">
        <div id="Rec100" class="Rec" style="display:none">
            <div id="Rec100Handle" class="Handle"><img src="img/Oxygen/22/actions/help-about.png" /> News</div>
            <div class="Info">
                <?php include("news.php"); ?>
            </div>
        </div>
        <div id="Rec0" class="Rec" style="display:none">
            <div id="Rec0Handle" class="Handle"><img src="img/Tango/22/apps/office-address-book.png" /> Letzten Kampagnen::Campaign Manager</div>
            <div class="Info">
                <?php include("cm_last5.php"); ?>
            </div>
        </div>
        <div id="Rec1" class="Rec" style="display:none;z-index:0;">
            <div id="Rec1Handle" class="Handle"><img src="img/Tango/22/apps/office-address-book.png" /> Kampagnen pro Monat::Campaign Manager</div>
            <div class="Info">
                <?php include("cm_c_year.php"); ?>
            </div>
        </div>
    </div>

    <div id="Column2">
        <div id="Rec2" class="Rec" style="display:none">
            <div id="Rec2Handle" class="Handle"><img src="img/Tango/22/apps/office-address-book.png" /> &Uuml;bersicht::Campaign Manager</div>
            <div class="Info">
                <?php include("cm_freigabe.php"); ?>
            </div>
        </div>
        <div id="Rec3" class="Rec" style="display:none">
            <div id="Rec3Handle" class="Handle"><img src="img/Tango/22/apps/office-address-book.png" /> Low Performance::Campaign Manager</div>
            <div class="Info">
                <?php include("cm_low_performance.php"); ?>
            </div>
        </div>
        <div id="Rec4" class="Rec" style="display:none">
            <div id="Rec4Handle" class="Handle"><img src="img/Tango/22/apps/office-address-book.png" /> Top Performance::Campaign Manager</div>
            <div class="Info">
                <?php include("cm_top_performance.php"); ?>
            </div>
        </div>
    </div>
<?php
    /**
     * cr-neu
     * 
     * f�r mandantenId 109 & 110 ausblenden
     */
    $excludeInternationalMandantenId = array(
        109,
        110
    );
    #if (in_array(intval($mandantID), $excludeInternationalMandantenId) !== true) {
    ?>
        <div id="Column3">
            <div id="Rec5" class="Rec" style="display:none">
                <div id="Rec5Handle" class="Handle"><img src="img/aesthetica/16/database.png" /> Letzten Lieferungen::Data Manager</div>
                <div class="Info">
                    <?php include("dm_last5.php"); ?>
                </div>
            </div>
            <div id="Rec6" class="Rec" style="display:none">
                <div id="Rec6Handle" class="Handle"><img src="img/aesthetica/16/database.png" /> Lieferungen pro Monat::Data Manager</div>
                <div class="Info">
                    <?php include("dm_l_year.php"); ?>
                </div>
            </div>
            <div id="Rec7" class="Rec" style="display:none">
                <div id="Rec7Handle" class="Handle"><img src="img/Tango/22/actions/mail-mark-important-deactive.png" /> Letzten Blacklisteintr&auml;ge::Blacklist</div>
                <div class="Info">
                    <?php include("bl_last5.php"); ?>
                </div>
            </div>
            <div id="Rec8" class="Rec" style="display:none">
                <div id="Rec8Handle" class="Handle"><img src="img/Tango/22/actions/mail-mark-important-deactive.png" /> Blacklisteintr&auml;ge pro Monat::Blacklist</div>
                <div class="Info">
                    <?php include("bl_year.php"); ?>
                </div>
            </div>
            <div id="Rec9" class="Rec" style="display:none">
                <div id="Rec9Handle" class="Handle"><img src="img/Tango/22/actions/mail-mark-important-deactive.png" /> Abmelder pro Monat::Abmelder</div>
                <div class="Info">
                    <?php include("bl_a_year.php"); ?>
                </div>
            </div>
        </div>
    <?php
    #}
?>
</div>


<script type="text/javascript">
    function Resize_home() {
    }

    window.onresize = Resize_home;

    //Widgets positionieren & Cookie ertsellen

    var zIndex = 0;

    // BEGIN :: Check if cookie is set
    var c1 = YAHOO.util.Cookie.getSub("igoogimi", "c1"); 
    var c2 = YAHOO.util.Cookie.getSub("igoogimi", "c2"); 
    var c3 = YAHOO.util.Cookie.getSub("igoogimi", "c3"); 

    if (c1||c2||c3) {
        var containerRef = [];
        var node;

        // BEGIN :: Removing the nodes
        var	cArray1 = c1.split(",");
        var len = cArray1.length;
        for(var i=0;i<len;i++){
            node = document.getElementById(cArray1[i]);	
            node?containerRef[cArray1[i]] = node.parentNode.removeChild(node):"";
        }
        var	cArray2 = c2.split(",");
        len = cArray2.length;
        for(var i=0;i<len;i++){
            node = document.getElementById(cArray2[i]);	
            node?containerRef[cArray2[i]] = node.parentNode.removeChild(node):"";
        }
        var cArray3 = c3.split(",");
        len = cArray3.length;
        for(var i=0;i<len;i++){
            node = document.getElementById(cArray3[i]);	
            node?containerRef[cArray3[i]] = node.parentNode.removeChild(node):"";
        }
        // END :: Removing the nodes
						
        // BEGIN :: Adding the nodes	
        len = cArray1.length;
        var col = document.getElementById("Column1");
        var tmpCR;
        for(var i=0;i<len;i++){
            tmpCR = containerRef[cArray1[i]]; 
            tmpCR?col.appendChild(tmpCR):"";
        }
        len = cArray2.length;
        var col = document.getElementById("Column2");
        for(var i=0;i<len;i++){
            tmpCR = containerRef[cArray2[i]]; 
            tmpCR?col.appendChild(tmpCR):"";
        }
        len = cArray3.length;
        var col = document.getElementById("Column3");
        for(var i=0;i<len;i++){
            tmpCR = containerRef[cArray3[i]]; 
            tmpCR?col.appendChild(tmpCR):"";
        }
        // END :: Adding the nodes	
    } 
    // END :: Check if cookie is set

    document.body.style.display="block";

    // BEGIN :: Non-draggable targets
    var col1Target = new YAHOO.util.DDTarget("Column1", "Group1");
    var col2Target = new YAHOO.util.DDTarget("Column2", "Group1");
    var col3Target = new YAHOO.util.DDTarget("Column3", "Group1");
    // END :: Non-draggable targets

    // BEGIN :: Objects to drag
    var rec100 = document.getElementById("Rec100");
    var Rec0 = document.getElementById("Rec0");
    var rec1 = document.getElementById("Rec1");
    var rec2 = document.getElementById("Rec2");
    var rec3 = document.getElementById("Rec3");
    var rec4 = document.getElementById("Rec4");
    var rec5 = document.getElementById("Rec5");
    var rec6 = document.getElementById("Rec6");
    var rec7 = document.getElementById("Rec7");
    var rec8 = document.getElementById("Rec8");
    var rec9 = document.getElementById("Rec9");

    var Rec0Drag = new YAHOO.util.DDProxy(Rec0, "Group1");
    Rec0Drag.setHandleElId("Rec0Handle");
    var rec1Drag = new YAHOO.util.DDProxy(rec1, "Group1");
    rec1Drag.setHandleElId("Rec1Handle");
    var rec2Drag = new YAHOO.util.DDProxy(rec2, "Group1");
    rec2Drag.setHandleElId("Rec2Handle");
    var rec3Drag = new YAHOO.util.DDProxy(rec3, "Group1");
    rec3Drag.setHandleElId("Rec3Handle");
    var rec4Drag = new YAHOO.util.DDProxy(rec4, "Group1");
    rec4Drag.setHandleElId("Rec4Handle");
    var rec5Drag = new YAHOO.util.DDProxy(rec5, "Group1");
    rec5Drag.setHandleElId("Rec5Handle");
    var rec6Drag = new YAHOO.util.DDProxy(rec6, "Group1");
    rec6Drag.setHandleElId("Rec6Handle");
    var rec7Drag = new YAHOO.util.DDProxy(rec7, "Group1");
    rec7Drag.setHandleElId("Rec7Handle");
    var rec8Drag = new YAHOO.util.DDProxy(rec8, "Group1");
    rec8Drag.setHandleElId("Rec8Handle");
    var rec9Drag = new YAHOO.util.DDProxy(rec9, "Group1");
    rec9Drag.setHandleElId("Rec9Handle");
				
    var rec100Drag = new YAHOO.util.DDProxy(rec100, "Group1");
    rec100Drag.setHandleElId("Rec100Handle");
    // END :: Objects to drag
				
    // BEGIN :: Event handlers
    var marker, container, oriContainer;
    var lastRectNode = [];
    marker = document.createElement("div");	

    Rec0Drag.startDrag = rec1Drag.startDrag = rec2Drag.startDrag = rec3Drag.startDrag = rec4Drag.startDrag = rec5Drag.startDrag = rec6Drag.startDrag = rec7Drag.startDrag = rec8Drag.startDrag = rec100Drag.startDrag = rec9Drag.startDrag = function(x, y) {
        var dragEl = this.getDragEl(); 
        var el = this.getEl();
        oriContainer = container = el.parentNode;
        el.style.display = "none";
        dragEl.style.zIndex = ++zIndex;
        dragEl.innerHTML = el.innerHTML;
        dragEl.style.color = "#999999";
        dragEl.style.opacity=0.7;
        dragEl.style.filter="progid:DXImageTransform.Microsoft.Alpha(Opacity=70)";
        dragEl.style.backgroundColor = "#fff";
        dragEl.style.textAlign = "center";
        marker.style.display = "none";
        marker.style.height = YAHOO.util.Dom.getStyle(dragEl, "height");	
        marker.style.width = YAHOO.util.Dom.getStyle(dragEl, "width");
        marker.style.margin = "5px"; 
        marker.style.marginBottom = "10px"; 
        marker.style.border = "2px dashed #7e7e7e";
        marker.style.display= "block";
        container.insertBefore(marker, el);
    }
    col1Target.onDragEnter = col2Target.onDragEnter = col3Target.onDragEnter = Rec0Drag.onDragEnter = rec1Drag.onDragEnter = rec2Drag.onDragEnter = rec3Drag.onDragEnter = rec4Drag.onDragEnter = rec5Drag.onDragEnter = rec6Drag.onDragEnter = rec7Drag.onDragEnter = rec8Drag.onDragEnter = rec100Drag.onDragEnter = rec9Drag.onDragEnter = function(e, id) {
        var el = document.getElementById(id);
        if (id.substr(0, 6)	=== "Column") {
            el.appendChild(marker);
        } else {
            container = el.parentNode;
            container.insertBefore(marker, el);
        }
    }
    Rec0Drag.onDragOut = rec1Drag.onDragOut = rec2Drag.onDragOut = rec3Drag.onDragOut = rec4Drag.onDragOut = rec5Drag.onDragOut = rec6Drag.onDragOut = rec7Drag.onDragOut = rec8Drag.onDragOut = rec100Drag.onDragOut = rec9Drag.onDragOut = function(e, id) {
        var el = document.getElementById(id);
        lastRectNode[container.id] = getLastNode(container.lastChild);

        if (lastRectNode[container.id] && el.id === lastRectNode[container.id].id) {
            container.appendChild(marker);
        }	
    }
    Rec0Drag.endDrag = rec1Drag.endDrag = rec2Drag.endDrag = rec3Drag.endDrag = rec4Drag.endDrag = rec5Drag.endDrag = rec6Drag.endDrag = rec7Drag.endDrag = rec100Drag.endDrag = rec8Drag.endDrag = rec9Drag.endDrag = function(e, id) {
        var el = this.getEl(); 

        try {
            marker = container.replaceChild(el, marker);
        } catch(err) {
            marker = marker.parentNode.replaceChild(el, marker);
        }	
        el.style.display = "block";
        setCookies();
    }

    var setCookies = function() {
        // BEGIN :: Calculate cookie expiration to 14 days from today
        var date = new Date();
        date.setTime(date.getTime()+(14*24*60*60*1000)); 
        // END :: Calculate cookie expiration to 14 days from today
        var getNode = function(node) {
            return (node.id==="Rec1"||node.id==="Rec2"||node.id==="Rec3"||node.id==="Rec4"||node.id==="Rec5"||node.id==="Rec6"||node.id==="Rec0"||node.id==="Rec7" || node.id==="Rec100" || node.id==="Rec8" || node.id==="Rec9");
        }
        var createString = function(colId) {
            var nodes = YAHOO.util.Dom.getChildrenBy(document.getElementById(colId), getNode);
            var list = [];
            var l = nodes.length;
            for(var i=0;i<l;i++) {
                list[i] = nodes[i].id;
            }
            return list.toString();
        }
        YAHOO.util.Cookie.setSub("igoogimi", "c1", createString("Column1"), {expires: date}); 
        YAHOO.util.Cookie.setSub("igoogimi", "c2", createString("Column2"), {expires: date}); 
        YAHOO.util.Cookie.setSub("igoogimi", "c3", createString("Column3"), {expires: date}); 
    }
    YAHOO.util.Event.on(window, "unload", setCookies); 	
    // END :: Event handlers
				
    // BEGIN :: Helper methods
    var getLastNode = function(lastChild) {
        if(lastChild) {
            var id = lastChild.id;
            if (id && id.substring(0, 3) === "Rec") {
                return lastChild;
            } 
							
            return getLastNode(lastChild.previousSibling);
        }
    }
    var isEmpty = function(el) {
        var test = function(el) { 
            return ((el && el.id) ? el.id.substr(0, 3) == "Rec" : false);
        } 
        var kids = YAHOO.util.Dom.getChildrenBy(el, test);
        return (kids.length == 0 ? true : false);
    }
    // END :: Helper methods
			
</script>