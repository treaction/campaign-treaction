<h3 class="error">Blacklist:</h3>
<p>Es gibt schon einen Eintrag in die Blacklist Tabelle:</p>
<dl>
    <dt>Email: </dt><dd><?php echo $detailsBacklistArr['Email']; ?>&nbsp;</dd>
    <dt>Datum: </dt><dd><?php echo date_converter($detailsBacklistArr['Datum'], 'd.m.Y h:s:i'); ?>&nbsp;</dd>
    <dt>Grund: </dt><dd><?php echo $detailsBacklistArr['Grund']; ?>&nbsp;</dd>
    <dt>Notizen: </dt><dd><?php echo $detailsBacklistArr['Notizen']; ?>&nbsp;</dd>
    <dt>Firma: </dt><dd><?php echo (\strlen($detailsBacklistArr['Firma']) > 0 ? $detailsBacklistArr['Firma'] : $detailsBacklistArr['FirmaAlt']); ?>&nbsp;</dd>
</dl>