<h3 class="error">Datenauskunft Extern:</h3>
<p>Die einfache Auskunft wurde an folgenden externe Email Adressen geschickt:</p>
<dl>
    <?php
    foreach ($daisExternInfoArr as $item) {
        ?>
        <dt>Email: </dt><dd><?php echo $item['permission_email']; ?> &nbsp;</dd>
        <dt>Datum: </dt><dd><?php echo date_converter($item['versanddatum_normal'], 'd.m.Y h:s:i'); ?> Uhr</dd>
        <dt>&nbsp;</dt><dd>&nbsp;</dd>
        <?php
    }
    ?>
</dl>