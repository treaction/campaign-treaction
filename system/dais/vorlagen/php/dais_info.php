<h3 class="error">Datenauskunft:</h3>
<p>
    Die einfache Auskunft wurde dem Kunden bereits am <b><?php echo date_converter($daisInfoArr[0]['versanddatum_normal'], 'd.m.Y h:s:i'); ?> Uhr</b> versendet<br /><br />
    Hier haben Sie die Möglichkeit, die Datenauskunft erneut zu senden.
</p>