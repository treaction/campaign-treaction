<?php
define('K_PATH_FONTS', __DIR__ . '/Fonts/');
define('PDF_FONT_NAME_MAIN', 'roboto');
define('PDF_FONT_SIZE_MAIN', 9);

define('PDF_FONT_NAME_DATA', \PDF_FONT_NAME_MAIN);
define('PDF_FONT_SIZE_DATA', \PDF_FONT_SIZE_MAIN);

define('PDF_MARGIN_TOP_NEW', '40');
define('PDF_MARGIN_LEFT_NEW', '26.7');
define('PDF_MARGIN_RIGHT_NEW', '26');


// always load config file
require_once(DIR_configs . 'Tcpdf/Dais/pdfConfig.php');

// Include the main TCPDF library.
require_once(DIR_Packages . 'Pdf/DaisPdf.php');
try {
	$pdf = new \MAAS\Pdf\DaisPdf(
		\PDF_PAGE_ORIENTATION,
		\PDF_UNIT,
		\PDF_PAGE_FORMAT,
		true,
		'UTF-8',
		false
	);
	$pdf->setKPathImages($daisRootPath . 'vorlagen/mandanten/' . $detailsMandantenArr['abkz'] . \DIRECTORY_SEPARATOR);
        
	$pdf->SetCreator(\utf8_decode($detailsMandantenArr['firma']));
	$pdf->SetAuthor(\utf8_decode($detailsMandantenArr['firma']));
	$pdf->SetTitle('Datenauskunft');

	// set header and footer fonts
	$pdf->setHeaderFont(
		array(
			\PDF_FONT_NAME_MAIN,
			'',
			\PDF_FONT_SIZE_MAIN
		)
	);

	// set margins
	$pdf->SetMargins(
		\PDF_MARGIN_LEFT_NEW,
		\PDF_MARGIN_TOP_NEW,
		\PDF_MARGIN_RIGHT_NEW
	);
	$pdf->SetHeaderMargin(\PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(\PDF_MARGIN_FOOTER);

	// remove default footer
	$pdf->setPrintFooter(false);

	// set auto page breaks
	$pdf->SetAutoPageBreak(
		true,
		\PDF_MARGIN_BOTTOM
	);

	// set image scale factor
	$pdf->setImageScale(\PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (\file_exists(\DIR_configs . 'Tcpdf/Dais/Language/de.php')) {
		require_once(\DIR_configs . 'Tcpdf/Dais/Language/de.php');
		$pdf->setLanguageArray($l);
	}


	/**
	 * page begin
	 */
	// set font
	$pdf->SetFont(\PDF_FONT_NAME_MAIN, '',  \PDF_FONT_SIZE_MAIN);

	// add a page
	$pdf->AddPage();

	// pdfContentBlock1
	$pdf->writeHTML(
		\nl2br($pdfContentBlock1),
		true,
		false,
		true,
		false,
		''
	);

	// $userTableContent (userDaten)
	$pdf->setCellHeightRatio(0.9);
	$pdf->writeHTML(
		\nl2br($userTableContent),
		true,
		false,
		true,
		false,
		''
	);
	
	// pdfContentBlock2
	$pdf->writeHTML(
		\nl2br($pdfContentBlock2),
		true,
		false,
		true,
		false,
		''
	);
	// $extendedUserTableContent (userDaten)
	$pdf->setCellHeightRatio(0.9);
	$pdf->writeHTML(
		\nl2br($extendedUserTableContent),
		true,
		false,
		true,
		false,
		''
	);

	// $pdfContentBlock3
	$pdf->setCellHeightRatio(\K_CELL_HEIGHT_RATIO);
	$pdf->writeHTML(
		\nl2br($pdfContentBlock3),
		true,
		false,
		true,
		false,
		''
	);

	// $pdfContentBlock4 (lieferantenDaten)
	$pdf->setCellHeightRatio(\K_CELL_HEIGHT_RATIO);
	$pdf->writeHTML(
		\nl2br($pdfContentBlock4),
		true,
		false,
		true,
		false,
		''
	);
	
	// $suplierTableContent (lieferantenDaten)
	$pdf->setCellHeightRatio(0.9);
	$pdf->writeHTML(
		\nl2br($suplierTableContent),
		true,
		false,
		true,
		false,
		''
	);

	// $pdfContentBlock5
	$pdf->setCellHeightRatio(\K_CELL_HEIGHT_RATIO);
	$pdf->writeHTML(
		\nl2br($pdfContentBlock5),
		true,
		false,
		true,
		false,
		''
	);

	//Close and output PDF document
	$pdf->Output($filename, $outputDestination);
} catch (\Exception $e) {
	\DebugAndExceptionUtils::sendDebugData($e);
	
	die('Pdf Error!');
}