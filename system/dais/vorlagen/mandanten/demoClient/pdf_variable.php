<?php
// user daten
$pdfUserDataArray = \createDefaultUserDataArray($detailsUsersArr);
\clearEmptyDataFromArrayForPdfTemplate($pdfUserDataArray);

$userTableContent = \createDaisPdfContentTable($pdfUserDataArray);

// lieferanten daten
$pdfSupplierDataArray = \createDefaultSupplierDataArray($detailsLieferantenArr);
$suplierTableContent = \createDaisPdfContentTable($pdfSupplierDataArray);


$pdfContentBlock1 = \utf8_encode(
	'<h1 style="font-size: 14pt; font-weight: bold; color: #7c7c7c;">' . $pdfAnrede . '</h1>'
	. '<p>'
		. $briefanrede . ',' . \chr(13) . \chr(13)
		. 'gerne kommen wir Ihrer Anfrage nach einer Datenauskunft nach.'
			. ' Im Folgenden erhalten Sie die Daten die uns bei Ihrer Eintragung �bermittelt wurden:'
		. \chr(13)
	. '</p>'
);

$pdfContentBlock2 = \utf8_encode('Ihre Daten wurden uns von folgender Firma �bermittelt:') . \chr(13);

$pdfContentBlock3 = \utf8_encode(
	'<p style="font-weight: bold;">' 
		. 'Wir haben Ihre Daten wunschgem�� in unserem System gesperrt, daher erhalten Sie ab sofort keine Werbeemails mehr von unserer Firma.'
			. ' Ihre Daten wurden selbstverst�ndlich nicht an Dritte weitergegeben und wurden bei uns nur bez�glich der gesetzlichen Nachweispflicht gespeichert.'
	. '</p>'
	. '<p>'
		. 'Bei weiteren Fragen zur Datenauskunft wenden Sie sich bitte direkt an die oben genannte Firma.' . \chr(13) . \chr(13)
		. 'Mit freundlichen Gr��en' . \chr(13)
		. $detailsMandantenArr['firma']
	. '</p>'
) . \chr(13);