<?php
define('PDF_MARGIN_TOP', 64.5);
define('PDF_MARGIN_BOTTOM', 37.9);
define('PDF_MARGIN_LEFT', 19.4);
define('PDF_MARGIN_RIGHT', 35);


function createNewDefaultUserDataArray(array $userDataArray) {
	return array(
		'Email:' => $userDataArray['email'],
		'Name:' => $userDataArray['anrede'] . ' ' . $userDataArray['vorname'] . ' ' . $userDataArray['nachname'],
		'Strasse:' => $userDataArray['strasse'],
		'Plz und Ort:' => $userDataArray['plz'] . ' ' . $userDataArray['ort'],
		'Telefon:' => $userDataArray['telefon'],
	);
}

function createExtendedUserDataArray(array $userDataArray) {
	return array(
		'Herkunftsseite:' => $userDataArray['herkunft'],
		'Datum der Registrierung:' => $userDataArray['anmeldung'],
		'IP bei Registrierung:' => $userDataArray['anmeldung_ip'],
		'Datum der Best�tigungsemail:' => $userDataArray['doi_datum'],
		'IP bei Best�tigung:' => $userDataArray['doi_ip'],
	);
}

function createNewDefaultSupplierDataArray(array $supplierDataArray) {
	return array(
		'Firma:' => $supplierDataArray['firma'],
		'Anschrift:' => $supplierDataArray['strasse'] . ', ' . $supplierDataArray['plz'] . ' - ' . $supplierDataArray['ort'],
		'Gesch�ftsf�hrung:' => $supplierDataArray['geschaeftsfuehrer'],
		'Telefon:' => $supplierDataArray['telefon'],
		'Fax:' => $supplierDataArray['fax'],
		'Website:' => $supplierDataArray['website']
	);
}


$pdfAnrede = 'Datenauskunft f�r: ' . $detailsUsersArr['email'];

// user daten
$pdfUserDataArray = \createNewDefaultUserDataArray($detailsUsersArr);
\clearEmptyDataFromArrayForPdfTemplate($pdfUserDataArray);
$userTableContent = \createDaisPdfContentTable(
	$pdfUserDataArray,
	1,
	'30%',
	'65%'
);

$pdfExtendedUserDataArray = \createExtendedUserDataArray($detailsUsersArr);
\clearEmptyDataFromArrayForPdfTemplate($pdfExtendedUserDataArray);
$extendedUserTableContent = \createDaisPdfContentTable(
	$pdfExtendedUserDataArray,
	1,
	'30%',
	'65%'
);

// lieferanten daten
$pdfSupplierDataArray = \createNewDefaultSupplierDataArray($detailsLieferantenArr);
$suplierTableContent = \createDaisPdfContentTable(
	$pdfSupplierDataArray,
	1,
	'30%',
	'65%'
);


$pdfContentBlock1 = \utf8_encode(
	'<h1 style="font-size: 12pt; font-weight: bold; color: #e30053;">' . $pdfAnrede . '</h1>'
	. '<p>'
		. \utf8_decode($briefanrede) . ',' . \chr(13) . \chr(13)
		. 'Sie machen gem�� Art. 15 DSGVO Ihr Recht auf Auskunft geltend.' . \chr(13)
			. 'Hiermit informieren wir Sie �ber Ihre bei uns gespeicherten pers�nlichen Daten.' . \chr(13) . \chr(13)
			.'a) Verarbeitungszwecke:' . \chr(13)
                .'Versand von teils personalisierten Werbeemails.'
        . \chr(13) . \chr(13)
		.'b) Kategorien personenbezogener Daten/gespeicherte Daten'
	. '</p>'
);

$pdfContentBlock2 = \utf8_encode('Zum Nachweis Ihres Double-opt-Ins wurden folgende technischen Informationen bei der Registrierung erhoben.') . \chr(13);

#$pdfContentBlock3 = \utf8_encode('c) Herkunft Ihrer Daten:') . \chr(13);
$pdfContentBlock3 ='';

$pdfContentBlock4 = \utf8_encode( 'c) Herkunft Ihrer Daten:' . \chr(13)
                                 .'Ihre personenbezogenen Daten wurden im Rahmen einer unserer Online-Aktionen erhoben.') . \chr(13);

$pdfContentBlock5 = \utf8_encode(
	'<p>'
		. 'Sollten Sie weitere Fragen haben, so wenden Sie sich bitte direkt an obiges Unternehmen' . \chr(13) . \chr(13) 
		        . 'd) Kategorien von Empf�ngern:'. \chr(13)
				. 'Ihre Daten werden nicht von uns an einen Dritten Empf�nger weiter gegeben.'. \chr(13) . \chr(13)
				. 'e) Dauer der Speicherung Ihrer personenbezogenen Daten:'. \chr(13)
                . 'Ihre Daten werden bis zu Ihrem Widerruf bei uns gespeichert.' . \chr(13) 
                . 'Im Rahmen der Listhygiene entfernen wir in regelm��igen Abst�nden Email-Adressen, welche nicht auf unsere Mailings reagieren.'
                . ' Es ist also m�glich, dass Sie trotz ausbleibendem Widerruf aus unserem Mailing-Verteiler entfernt werden.'. \chr(13)
                . 'Bitte haben Sie Verst�ndnis daf�r, dass wir, um einen weiteren Versand an Ihre bei uns gespeicherte Email-Adresse ausschlie�en zu k�nnen, in beiden F�llen diese weiterhin in unserem System behalten.'. \chr(13)  . \chr(13)
				.'f) Berichtigung und L�schung' . \chr(13)
                . 'Sollten wir fehlerhafte Daten von Ihnen gespeichert haben, so k�nnen Sie eine Berichtigung Ihrer personenbezogenen Daten durch eine Nachricht an datenschutz@rabattriese.de  verlangen.'. \chr(13) . \chr(13)
                . 'Sollten Sie keine Mailings mehr von uns w�nschen, werden wir Sie auf unsere Sperrliste setzen um einen weiteren Versand an Ihre Email-Adresse auszuschlie�en.'. \chr(13) . \chr(13)
                . 'Bitte senden Sie uns hierzu eine Email mit entsprechender Nachricht an csc@rabattriese.de .'. \chr(13)
                . 'Bitte haben Sie Verst�ndnis daf�r, dass wir, um einen weiteren Versand an Ihre bei uns gespeicherte Email-Adresse ausschlie�en zu k�nnen, diese weiterhin in unserem System behalten.'. \chr(13)  . \chr(13)
				. 'g) Profiling' . \chr(13)
                .'Im Rahmen unserer Mailings erheben wir sogenannte Profiling-Daten. �ffnen und/oder klicken Sie auf eines unserer Mailings, speichern wir diese Information in unserem System. Hier�ber sammeln wir Informationen �ber die Affinit�ten unserer User und hoffen, Ihnen so immer relevantere und zu Ihren Interessen passende Angebote zu senden zu k�nnen.' . \chr(13)  . \chr(13)
                .'Wir hoffen Ihnen hiermit umfangreiche und detaillierte Informationen �ber die Erhebung, Speicherung und Nutzung Ihrer personenbezogenen Daten gegeben zu haben. Bei weiteren Fragen und Anregungen, wenden Sie sich bitte an unseren Ansprechpartner f�r Datenschutz, Florian Ganz, unter folgender Email Adresse: datenschutz@rabattriese.de .' . \chr(13)  . \chr(13)
		. 'Mit freundlichen Gr��en' . \chr(13)
		. 'Ihre Rabattriese UG' . \chr(13)
	. '</p>'
) . \chr(13);