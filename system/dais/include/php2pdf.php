<?php
/**
 * sendEmailAboutSupplierInfo
 * 
 * @param array $userDataArray
 * @param array $supplierDataArray
 * @param array $clientDataArray
 * @param boolean $notSupplierInfo
 * @param boolean $sendSupplierEmail
 * @return string
 */
function sendEmailAboutSupplierInfo(array $userDataArray, array $supplierDataArray, array $clientDataArray, $notSupplierInfo, $sendSupplierEmail) {
	$result = '';
	
	if (empty($userDataArray['partner']) 
		&& $notSupplierInfo == false 
		&& !empty($supplierDataArray['firma']) 
		&& $sendSupplierEmail === true
	) {
		$emailResult = \sendLieferantenInfos(
			$supplierDataArray,
			$clientDataArray,
			$_POST['mandant_intern']
		);

		if (!empty($emailResult)) {
			$result .= '<br /><br />'
				. $emailResult
				. 'Email konnten an Eduard nicht versendet werden!'
			;
		}
	}
	
	return $result;
}

/**
 * createEmailBody
 * 
 * @param string $daisRootPath
 * @param string $clientIntern
 * @param string $emailTemplateFilename
 * @param array $userDataArray
 * @param array $supplierDataArray
 * @param array $clientDataArray
 * @return string
 */
function createEmailBody($daisRootPath, $clientIntern, $emailTemplateFilename, array $userDataArray, array $supplierDataArray, array $clientDataArray) {
	// HTML body laden
	$bodyHtml = \file_get_contents($daisRootPath . 'vorlagen/mandanten/' . $clientIntern . '/' . $emailTemplateFilename);

	// footer bereich laden
	$emailSignaturHtml = \file_get_contents($daisRootPath . 'vorlagen/mandanten/' . $clientIntern . '/email_footer_template.html');

	// userDaten personalisieren
	$bodyHtml = \preg_replace('/{user_(.*?)}/e', 'isset($userDataArray[\'$1\']) ? $userDataArray[\'$1\'] : ""', $bodyHtml);
	// lieferantenDaten personalisieren
	$bodyHtml = \preg_replace('/{lieferanten_(.*?)}/e', 'isset($supplierDataArray[\'$1\']) ? $supplierDataArray[\'$1\'] : ""', $bodyHtml);

	// footer bereich personalisieren
	$emailSignaturHtml = \preg_replace('/{(.*?)}/e', 'isset($clientDataArray[\'$1\']) ? $clientDataArray[\'$1\'] : ""', $emailSignaturHtml);
	
	// email content zusammen f�hren
	return \str_replace('{emailSignaturHtml}', $emailSignaturHtml, $bodyHtml);
}


// pdf functions
/**
 * createPdfEmailAttachmentFromFile
 * 
 * @param string $filename
 * @return string
 */
function createPdfEmailAttachmentFromFile($filename) {
	$pdfFileContent = \chunk_split(
		\base64_encode(
			\fread(
				\fopen($filename, 'r'),
				\filesize($filename)
			)
		)
	);
	
	return \base64_decode($pdfFileContent);
}

/**
 * createDaisPdfContentTable
 * 
 * @param array $dataArray
 * @param string $widthCol1
 * @param string $widthCol2
 * @param string $widthCol3
 * @return string
 */
function createDaisPdfContentTable(array $dataArray, $widthCol1 = '20', $widthCol2 = '24%', $widthCol3 = '60%') {
	$result = '<table border="0" cellspacing="2" cellpadding="2" style="width: 100%">';
	foreach ($dataArray as $key => $value) {
		$result .= 
			'<tr>' 
				. '<td width="' . $widthCol1 . '"></td>'
				. '<td width="' . $widthCol2 . '">' . \utf8_encode($key) . '</td>'
				. '<td width="' . $widthCol3 . '">' . \utf8_encode(\utf8_decode($value)) . '</td>'
			. '</tr>'
		;
	}
	$result .= '</table>';
	
	return $result;
}

/**
 * createDefaultUserDataArray
 * 
 * @param array $userDataArray
 * @return array
 */
function createDefaultUserDataArray(array $userDataArray) {
	return array(
		'Email:' => $userDataArray['email'],
		'Herkunftsseite:' => $userDataArray['herkunft'],
		'Teilnahmedatum:' => $userDataArray['anmeldung'],
		'Teilnahme-IP:' => $userDataArray['anmeldung_ip'],
		'E-Mail-Best�tigungsdatum:' => $userDataArray['doi_datum'],
		'E-Mail-Best�tigungs-IP:' => $userDataArray['doi_ip'],
		'Name:' => $userDataArray['anrede'] . ' ' . $userDataArray['vorname'] . ' ' . $userDataArray['nachname'],
		'Strasse:' => $userDataArray['strasse'],
		'Plz und Ort:' => $userDataArray['plz'] . ' ' . $userDataArray['ort']
	);
}

/**
 * createDefaultSupplierDataArray
 * 
 * @param array $supplierDataArray
 * @return array
 */
function createDefaultSupplierDataArray(array $supplierDataArray) {
	return array(
		'Firma:' => $supplierDataArray['firma'],
		'Anschrift:' => $supplierDataArray['strasse'] . ', ' . $supplierDataArray['plz'] . ' - ' . $supplierDataArray['ort'],
		'Gesch�ftsf�hrer:' => $supplierDataArray['geschaeftsfuehrer'],
		'Telefon:' => $supplierDataArray['telefon'],
		'Fax:' => $supplierDataArray['fax'],
		'Website:' => $supplierDataArray['website']
	);
}

/**
 * clearEmptyDataFromArrayForPdfTemplate
 * 
 * @param array $dataArray
 * @return void
 */
function clearEmptyDataFromArrayForPdfTemplate(array &$dataArray) {
	foreach ($dataArray as $key => $value) {
		if (\strlen($key) > 0 
			&& \strlen(\trim($value)) === 0
		) {
			unset($dataArray[$key]);
		}
	}
}



\date_default_timezone_set('Europe/Berlin');
\setlocale(\LC_ALL, 'de_DE', 'de_DE.utf-8');

\ini_set('memory_limit', '256M');
\ini_set('max_execution_time', 0);

\header('Content-Type: text/html');
\header('Content-Transfer-Encoding: charset=utf-8');

require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');


$mandant = \RegistryUtils::get('clientEntity')->getAbkz();
require_once('adminGroupSettings.php');

\ini_set('error_reporting', 0);
\ini_set('display_errors', false);

// datenbank verbindung
require_once('connect_inc.php');
require_once('functionen.php');

$daisRootPath = \str_replace('include', '', __DIR__);
\debug_info('daisRootPath', $daisRootPath, false);

// pdfAktion abfangen
$postPdf = isset($_POST['pdf']) ? $_POST['pdf'] : null;

// debug
\debug_info('detail daten', $_POST, false);
#\DebugAndExceptionUtils::showDebugData($_POST);

// benutzer gruppen
$adminZugang = false;
$datenauskunftUrl = '';
$lieferantenInfos = false;

$sendLieferantenInfo = false;
$lieferantenTmpArr = array(
	'firma' => '',
	'strasse' => '',
	'plz' => '',
	'ort' => '',
	'geschaeftsfuehrer' => '',
	'telefon' => '',
	'fax' => '',
	'website' => ''
);


/**
 * exceptionSettings
 */
$webHeader = 'Fehler:';
$webText = '';

try {
	// formular wurde abgeschickt
	if ($postPdf) {
		/**
		 * schaue nach ob der ausgew�hlter datensatz zum eingeloggten mandant passt 
		 * bzw. ob es sich um einen manuellen eintrag handelt
		 */
		if (
			(
				($_POST['mandant'] != $_POST['mandant_intern']) 
				&& (boolean) $adminZugang === false
			) 
			&& (boolean) $_POST['manuelle_eingabe'] !== true
		) {
			$webText = 'Sie haben keine Berechtigung f�r das Ausgew�hlte Datensatz eine Auskunft zu verschicken weil der Datensatz ' 
				. '<b>' . $_POST['mandant_intern'] . '</b> geh�rt und Sie sind als: <span style="font-weight: bold; color: #ff0000;">' . $_POST['mandant'] . '</span> eingeloggt!'
			;
			
			throw new \InvalidArgumentException('no valid authorization!', 1436942346);
		}
		
		// mandanten details anhand der mandant_intern
                 
		foreach (\mandanten_details($_POST['mandant_intern']) as $key => $value) {
			$detailsMandantenArr[\strtolower($key)] = $value;
		}
		\debug_info('detailsMandantenArr', $detailsMandantenArr, false);
		
		// userArr sowie lieferantenArr bef�llen
		foreach ($_POST as $key => $value) {
			if (!\is_array($value)) {
				// userdaten in detailsUsersArr speichern
				$detailsUsersArr[\strtolower($key)] = $value;
			} elseif (\is_array($value)) {
				foreach ($value as $key2 => $value2) {
					$detailsLieferantenArr[\strtolower($key2)] = $value2;
				}
			}
		}
		\debug_info('detailsLieferantenArr', $detailsLieferantenArr, false);
		
		// unn�tigen balast aus $detailsUsersArr entfernen
		unset(
			$detailsUsersArr['pdf'],
			$detailsUsersArr['mitarbeiter'],
			$detailsUsersArr['mandant'],
			$detailsUsersArr['mandant_intern'],
			$detailsUsersArr['manuelle_eingabe'],
			$detailsUsersArr['senden'],
			$detailsUsersArr['weiter'],
			$detailsUsersArr['externe_email_m'],
			$detailsUsersArr['doi_infos'],
			$detailsUsersArr['db_abkz']
		);
		\debug_info('detailsUsersArr', $detailsUsersArr, false);
		
		if (\is_array($detailsLieferantenArr)) {
			$lieferantenTmpArr = array(
				'firma' => $detailsLieferantenArr['firma'],
				'strasse' => $detailsLieferantenArr['strasse'],
				'plz' => $detailsLieferantenArr['plz'],
				'ort' => $detailsLieferantenArr['ort'],
				'geschaeftsfuehrer' => $detailsLieferantenArr['geschaeftsfuehrer'],
				'telefon' => $detailsLieferantenArr['telefon'],
				'fax' => $detailsLieferantenArr['fax'],
				'website' => $detailsLieferantenArr['website']
			);
		}
		\debug_info('lieferantenTmpArr', $lieferantenTmpArr, false);
		
		$zusammenfassung = \serialize(
			array(
				// user daten
				'anrede' => $detailsUsersArr['anrede'],
				'vorname' => $detailsUsersArr['vorname'],
				'nachname' => $detailsUsersArr['nachname'],
				'strasse' => $detailsUsersArr['strasse'],
				'plz' => $detailsUsersArr['plz'],
				'ort' => $detailsUsersArr['ort'],
				'land' => $detailsUsersArr['land'],
				'email' => $detailsUsersArr['email'],
				'anmeldung' => $detailsUsersArr['anmeldung'],
				'partner' => $detailsUsersArr['partner'],
				'ip-adresse' => $detailsUsersArr['anmeldung_ip'],
				'herkunft' => $detailsUsersArr['herkunft'],
				'doi_ip' => $detailsUsersArr['doi_ip'],
				'doi_datum' => $detailsUsersArr['doi_datum'],
				'dais_anfrage_datum' => $detailsUsersArr['dais_anfrage_datum'],
				'username' => $detailsUsersArr['username'],
				'geburtsdatum' => $detailsUsersArr['geburtsdatum'],
				// lieferanten daten
				'lieferanten' => $lieferantenTmpArr
			)
		);
		
		// pr�fe nach ob lieferan schon in die db vorhanden war oder nicht
		$tmpLieferantenArr = \lieferanten_details(
			$_POST['db_abkz'],
			$detailsUsersArr['partner']
		);
		\debug_info('tmpLieferantenArr', $tmpLieferantenArr, false);
		
		if (\is_array($tmpLieferantenArr) 
			&& (
				!empty($tmpLieferantenArr['Firma']) 
				|| !empty($tmpLieferantenArr['firma'])
			)
		) {
			$lieferantenInfos = true;
		}
		
		
		// wenn alles in ordnung ist fahre mit dem programm fort
		$datei = \date('Y-m-j') . '_' . $detailsUsersArr['email'];
		
		if ($detailsUsersArr['vorname'] && $detailsUsersArr['nachname']) {
			if ($detailsUsersArr['anrede'] == 'Herr') {
				$briefanrede = 'Sehr geehrter Herr ' . $detailsUsersArr['vorname'] . ' ' . $detailsUsersArr['nachname'];
			} else {
				$briefanrede = 'Sehr geehrte Frau ' . $detailsUsersArr['vorname'] . ' ' . $detailsUsersArr['nachname'];
			}

			$pdfAnrede = 
				'<table border="0" cellspacing="0" cellpadding="0" style="width: 100%">'
					. '<tr>'
						. '<td colspan="2">Datenauskunft f�r: ' . $detailsUsersArr['vorname'] . ' ' . $detailsUsersArr['nachname'] . '</td>'
					. '</tr>'
					. '<tr>'
						. '<td width="130.5pt"></td>'
						. '<td>' . $detailsUsersArr['email'] . '</td>'
					. '</tr>'
				. '</table>'
			;
		} else {
			$briefanrede = 'Sehr geehrte/r Damen und Herren';

			$pdfAnrede = 'Datenauskunft f�r: ' . $detailsUsersArr['email'];
		}
		
		$pdfDaisTemplateFile = $daisRootPath . 'vorlagen/mandanten/' . $_POST['mandant_intern'] . '/pdfTemplate.php';
		
		require_once($daisRootPath . 'vorlagen/mandanten/' . $_POST['mandant_intern'] . '/pdf_variable.php');
		
		switch ($postPdf) {                        
			case 'pdf-vorschau':
				/**
				 * pdf generieren und als datei speichern
				 */
				$filename = 'datenauskunft.pdf';
				$outputDestination = 'I';
				
				if ((\file_exists($pdfDaisTemplateFile)) === true) {
					include_once($pdfDaisTemplateFile);
				} else {
					include_once($daisRootPath . 'vorlagen/mandanten/default/pdfTemplate.php');
				}
				break;
			
			case 'send-pdf-kunde':
			case 'send-pdf-email':
                          
				/**
				 * pdf erzeugen und an kunde schicken
				 * 
				 * pdf im verzeichniss speichern
				 */
				if ($postPdf == 'send-pdf-email') {
					$filename = $daisRootPath . '/auskunft_dateien/' . $_POST['mandant_intern'] . '/extern_' . ($_POST['doi_infos'] ? 'doi_' : '') . $datei . '.pdf';
					$emailTemplateFilename = 'email_externe_email_template.html';
					
					$toEmail = $externeEmail = $_POST['externe_email_m'];
					$toName = '';
					
					$permissionType = 'Datenauskunft_Extern';
					$zusammenfassung = '';
					
					$webText = 'Die Datenauskunftinformation wurde erfolgreich an folgende Email: ' 
						. $externeEmail . ' geschickt!'
					;
				} else {
                                     
                                      
					$filename = $daisRootPath . '/auskunft_dateien/' . $_POST['mandant_intern'] . '/' . $datei . '.pdf';
                                          
					$emailTemplateFilename = 'email_dais_template.html';
					
					$toEmail = $detailsUsersArr['email'];
					$toName = $detailsUsersArr['vorname'] . ' ' . $detailsUsersArr['nachname'];
					
					$permissionType = 'Datenauskunft';
					
					$webText = 'Die Datenauskunftinformation wurde erfolgreich an ' 
						. $detailsUsersArr['vorname'] . ' ' . $detailsUsersArr['nachname'] 
						. ' ( Email: ' . $detailsUsersArr['email'] . ') geschickt!'
					;
				}
				$outputDestination = 'F';
				 
				if ((\file_exists($pdfDaisTemplateFile)) === true) {                                   
					include_once($pdfDaisTemplateFile);
				} else {
					include_once($daisRootPath . 'vorlagen/mandanten/default/pdfTemplate.php');
				}
				 
                                 
				/**
				 * createEmailBody
				 */
				$bodyHtml = \createEmailBody(
					$daisRootPath,
					$_POST['mandant_intern'],
					$emailTemplateFilename,
					$detailsUsersArr,
					$detailsLieferantenArr,
					$detailsMandantenArr
				);
				
				/**
				 * createPdfEmailAttachmentFromFile
				 * 
				 * erzeugtes pdf vom server �ffnen und f�r den mail vorbereiten
				 */
				$attachment = \createPdfEmailAttachmentFromFile($filename);
				;
				/**
				 * mail_to_username
				 * 
				 * email versand vorbereiten und email verschicken
				 */
                                
				$sendInfo = \mail_to_username(
					$toEmail,
					$toName,
					$detailsMandantenArr['firma'] . ': Datenauskunft',
					$_POST['mandant_intern'],
					$detailsMandantenArr,
					$bodyHtml,
					$attachment,
					'Datenauskunft.pdf'
				);

			
				if (!empty($sendInfo)) {
					$webText = $sendInfo
						. 'Email konnte nicht versendet werden weil die Empf�nger Email-Adresse falsch ist oder der dazugeh�riger SMTP - Postfach nicht reagiert.' 
							. ' Versuchen Sie es noch einmal. Falls der fehler weiterhin auftritt informieren Sie den Systemadministrator!'
					;
					
					throw new \Exception(\strip_tags($sendInfo), 1436944123);
				}
				
				/**
				 * dais_eintrag
				 * 
				 * Erstelle ein neues datensatz in datenauskunft table falls es keinen gibt
				 */
				\dais_eintrag(
					$detailsMandantenArr['id'],
					$_POST['mitarbeiter'],
					$detailsUsersArr['email'],
					$detailsUsersArr['username'],
					$toEmail,
					$permissionType,
					$zusammenfassung,
					$_POST['db_abkz']
				);
				
				// Blacklist eintrag checken und ggbf. hinzuf�gen
				if (!empty($_POST['black_list'])) {
					/**
					 * blacklist_eintrag
					 */
					\blacklist_eintrag(
						$detailsUsersArr['email'],
						$_POST['grund'],
						$_POST['mandantId'],
						$_POST['mitarbeiter']
					);
				}
				
				$webHeader = 'Hinweis:';
				
				/**
				 * sendEmailAboutSupplierInfo
				 */
				$webText .= \sendEmailAboutSupplierInfo(
					$detailsUsersArr,
					$detailsLieferantenArr,
					$detailsMandantenArr,
					$lieferantenInfos,
					$sendLieferantenInfo
				);
				
				// infobox anzeigen
				echo \info_seite($webHeader, $webText);
				break;
			
                        case 'delete-contact':
                              
                                 switch ($mandant) {
                                    case 'stellarPerformance':
                                    $toEmail = $externeEmail = 'zentrale@stellar-performance.de';
                                    $mandant_id = 6;
                                        break;
                                    
                                    case '4waveMarketing':
                                    $toEmail = $externeEmail = 'office@4wave-marketing.de';
                                    $mandant_id = 7;
                                        break;
                                    
                                    case 'leadWorld':
                                    $toEmail = $externeEmail = 'contact@leadworld.online';

                                    $mandant_id = 8;
                                      break;
                                
                                    case 'taeglichAngebote':
                                    $toEmail = $externeEmail = 'contact@leadworld.online';
                                    $mandant_id = 9;

                                    
                                    case 9:
                                    $toEmail = $externeEmail = 'contact@täglich-angebote.de';

                                        break;

                                }
                            // Ausgewaelte Kontakt in DB speichern
                            \insertEmailForCronjob(
                                    $mandant_id,
					$_POST['mitarbeiter'],
					$detailsUsersArr['email'],
					$detailsUsersArr['username'],
					$toEmail,
					$zusammenfassung,
					$_POST['db_abkz']
                                    );
                          
                            $filename = $daisRootPath . '/auskunft_dateien/' . $_POST['mandant_intern'] . '/extern_' . ($_POST['doi_infos'] ? 'doi_' : '') . $datei . '.pdf';
                             
			    $emailTemplateFilename = 'email_externe_email_template.html';
				//Email an an das Zentrale Postfach senden                             
                                       #$toEmail = $externeEmail ='sami.jarmoud@treaction.de';
					$toName = '';
					
					$permissionType = 'Datenauskunft_Extern';
					$zusammenfassung = '';
					$webHeader = 'Hinweis:';
					$webText = 'Die Datenauskunftinformation wurde erfolgreich an folgende Email: ' 
						. $externeEmail . ' geschickt!'
					;
                            
                            $outputDestination = 'F';
				
				if ((\file_exists($pdfDaisTemplateFile)) === true) {                                   
					include_once($pdfDaisTemplateFile);
				} else {
					include_once($daisRootPath . 'vorlagen/mandanten/default/pdfTemplate.php');
				}
                  
                               /**
				 * createEmailBody
				 */
				$bodyHtml = \createEmailBody(
					$daisRootPath,
					$_POST['mandant_intern'],
					$emailTemplateFilename,
					$detailsUsersArr,
					$detailsLieferantenArr,
					$detailsMandantenArr
				);
				 
				/**
				 * createPdfEmailAttachmentFromFile
				 * 
				 * erzeugtes pdf vom server �ffnen und f�r den mail vorbereiten
				 */
				$attachment = \createPdfEmailAttachmentFromFile($filename);
				
                                
				/**
				 * mail_to_username
				 * 
				 * email versand vorbereiten und email verschicken
				 */
                                
				$sendInfo = \mail_to_username(
					$toEmail,
					$toName,			
                                        $detailsMandantenArr['firma'] . ': Löschauftrag Datensatz - ' . $_POST['email'],
					$mandant,
					$detailsMandantenArr,
					$bodyHtml,
					$attachment,
					'Datenauskunft.pdf'
				);  
                                
                                if (!empty($sendInfo)) {
					$webText = $sendInfo
						. 'Email konnte nicht versendet werden weil die Empf�nger Email-Adresse falsch ist oder der dazugeh�riger SMTP - Postfach nicht reagiert.' 
							. ' Versuchen Sie es noch einmal. Falls der fehler weiterhin auftritt informieren Sie den Systemadministrator!'
					;
					
					throw new \Exception(\strip_tags($sendInfo), 1436944123);
				}
                                // infobox anzeigen
				echo \info_seite($webHeader, $webText);
				break;
				
		}
	} else {
		throw new \InvalidArgumentException('keine g�ltige Aktion!', 1436942076);
	}
} catch (\Exception $e) {
	\DebugAndExceptionUtils::sendDebugData($e);
	
	// infobox anzeigen
	echo \info_seite($webHeader, $webText);
}

// datenbankverbindung trennen
require_once('close_connection.php');