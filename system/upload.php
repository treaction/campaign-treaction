<?php
/**
 * removeOldFiles
 * 
 * @param boolean $cleanupTargetDir
 * @param string $targetDir
 * @return void
 * @throws \LogicException
 */
function removeOldFiles($cleanupTargetDir, $targetDir) {
	if ($cleanupTargetDir === true) {
		if (!\is_dir($targetDir) 
			|| !$dir = \opendir($targetDir)
		) {
			throw new \LogicException('Failed to open temp directory', 100);
		}

		while (($file = \readdir($dir)) !== false) {
			$tmpfilePath = $targetDir . \DIRECTORY_SEPARATOR . $file;

			// If temp file is current file proceed to the next
			if ($tmpfilePath == '{$filePath}.part') {
				continue;
			}

			// Remove temp file if it is older than the max age and is not the current file
			if (\preg_match('/\.part$/', $file) 
				&& (\filemtime($tmpfilePath) < \time() - $maxFileAge)
			) {
				\unlink($tmpfilePath);
			}
		}
		\closedir($dir);
	}
}

/**
 * createAndCleanFilename
 * 
 * @param string $filename
 * @return string
 */
function createAndCleanFilename($filename) {
	return \time() . '_' . \preg_replace('/\s/', '_', $filename);
}



/**
 * upload.php
 *
 * Copyright 2013, Moxiecode Systems AB
 * Released under GPL License.
 *
 * License: http://www.plupload.com/license
 * Contributing: http://www.plupload.com/contributing
 */
// Make sure file is not cached (as it happens for example on iOS devices)
\header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
\header('Last-Modified: ' . \gmdate('D, d M Y H:i:s') . ' GMT');
\header('Cache-Control: no-store, no-cache, must-revalidate');
\header('Cache-Control: post-check=0, pre-check=0', false);
\header('Pragma: no-cache');

/*
// Support CORS
\header('Access-Control-Allow-Origin: *');

// other CORS headers if any...
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	exit; // finish preflight CORS requests here
}
*/

// 5 minutes execution time
@\set_time_limit(5 * 60);

$jsonResult = array(
	'jsonrpc' => 2.0,
	'id' => NULL
);

$configsPath = $_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'Configs' . \DIRECTORY_SEPARATOR;
try {
	require_once($configsPath . 'config.php');
	
	$clientId = (int) $_SESSION['mID'];
	if ($clientId <= 0) {
		throw new \InvalidArgumentException('invalid clientId.', 1447752476);
	}
	
	$loggedUserId = (int) $_SESSION['benutzer_id'];
	if ($loggedUserId <= 0) {
		throw new \InvalidArgumentException('no valid user logged.', 1447752488);
	}
	
	if (\count($_POST) === 0) {
		throw new \InvalidArgumentException('no data send!', 1447752496);
	}
	
	/**
	 * init
	 * 
	 * systemConnectionHandle
	 * systemRepository
	 * userRepository
	 */
	require_once(\DIR_Inits . 'Connections' . \DIRECTORY_SEPARATOR . 'systemConnection.php');
	/* @var $systemConnectionHandle \Packages\Core\Manager\Database\AbstractConnection */
	
	require_once(\DIR_Inits . 'Repository' . \DIRECTORY_SEPARATOR . 'systemRepository.php');
	/* @var $systemRepository \Packages\Core\Domain\Repository\SystemRepository */
	
	require_once(\DIR_Inits . 'Repository' . \DIRECTORY_SEPARATOR . 'userRepository.php');
	/* @var $userRepository \Packages\Core\Domain\Repository\UserRepository */
	
	$clientEntity = $systemRepository->findById($clientId);
	if (!($clientEntity instanceof \Packages\Core\Domain\Model\ClientEntity)) {
		throw new \DomainException('invalid clientEntity', 1447243979);
	}
	
	
	// uploadSettings
	$targetDir = \DIR_Uploads . $clientEntity->getAbkz();
	$cleanupTargetDir = false; // Remove old files
	$maxFileAge = 5 * 3600; // Temp file age in seconds
	
	// Create target dir
	\Packages\Core\Utility\FileUtility::createDirectory($targetDir);
	
	// TODO: refactor
	// Get a file name
	if (isset($_POST['name'])) {
		$fileName = \createAndCleanFilename($_POST['name']);
	} elseif (!empty($_FILES)) {
		\Packages\Core\Utility\DebugUtility::debug($_FILES);
		$fileName = \createAndCleanFilename($_FILES['file']['name']);
	} else {
		$fileName = \uniqid('file_');
	}
	
	if (isset($_POST['fileType'])) {
		$fileType = $_POST['fileType'];
	} elseif (!empty($_FILES)) {
		$extension = pathinfo($fileName, PATHINFO_EXTENSION);
		$mappingFileTypeDataArray = \Packages\Core\Utility\FileUtility::$mappingFileTypeDataArray;
		foreach ($mappingFileTypeDataArray as $fileTypeName => $fileTypeDataArray){		     
			foreach ($fileTypeDataArray as $fileTypeExtension){
				if(strcasecmp($fileTypeExtension, $extension) == 0){
				  $fileType =  \ucfirst($fileTypeName);
				 }
			}
		}
		if(empty($fileType)){
			   $fileType = 'Others';
			}
	} else {
		$fileType = '';
	}
	
	$filePath = $targetDir . \DIRECTORY_SEPARATOR . $fileName;
	
	// Chunking might be enabled
	$chunk = isset($_POST['chunk']) ? \intval($_POST['chunk']) : 0;
	$chunks = isset($_POST['chunks']) ? \intval($_POST['chunks']) : 0;
	
	removeOldFiles(
		$cleanupTargetDir,
		$targetDir
	);
	
	// Open temp file
	if (!$out = \fopen('{$filePath}.part', $chunks ? 'ab' : 'wb')) {
		throw new \RuntimeException('Failed to open output stream', 102);
	}
	
	if (!empty($_FILES)) {
		if ($_FILES['file']['error'] 
			|| !\is_uploaded_file($_FILES['file']['tmp_name'])
		) {
			throw new \InvalidArgumentException('Failed to move uploaded file', 103);
		}

		// Read binary input stream and append it to temp file
		if (!$in = \fopen($_FILES['file']['tmp_name'], 'rb')) {
			throw new \InvalidArgumentException('Failed to open input stream', 101);
		}
	} else {
		if (!$in = \fopen('php://input', 'rb')) {
			throw new \InvalidArgumentException('Failed to open input stream', 101);
		}
	}
	
	while ($buff = \fread($in, 4096)) {
		\fwrite($out, $buff);
	}
	\fclose($out);
	\fclose($in);
	
	// Check if file has been uploaded
	if (!$chunks 
		|| $chunk === $chunks - 1
	) {
		// Strip the temp .part suffix off 
		\rename('{$filePath}.part', $filePath);
	}

	$jsonResult['result'] = array(
		'message' => 'success',
		'fileName' => $fileName,
		'fileType' => $fileType,
		'extension' => $extension,
	);
} catch (\Exception $e) {
	require_once($configsPath . 'exceptionsHandling.php');
	
	$jsonResult['error'] = array(
		'code' => $e->getCode(),
		'message' => $e->getMessage()
	);
}

// Return Success JSON-RPC response
echo \json_encode($jsonResult);
