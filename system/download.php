<?php
$configsPath = $_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'Configs' . \DIRECTORY_SEPARATOR;
try {
	require_once($configsPath . 'config.php');
	
	// TODO: min validation check (mandant, user usw)
	
	if (!isset($_GET['download']['file'])) {
		throw new \InvalidArgumentException('no data given!', 1449673833);
	}
	
	$fileData = \Packages\Core\Utility\FilterUtility::filterData(
		$_GET['download']['file'],
		\Packages\Core\Utility\FilterUtility::$validateFilterDataArray['string']
	);
	if (\strlen($fileData) === 0) {
		throw new \LengthException('no file given!', 1449673922);
	}
	
	$file = \base64_decode($fileData);
	if (\Packages\Core\Utility\FileUtility::isReadable($file)) {
		\header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		\header('Cache-Control: no-store, no-cache, must-revalidate');
		\header('Cache-Control: post-check=0, pre-check=0', false);
		\header('Content-Description: File Transfer');
		\header('Content-Type: application/octet-stream');
		\header('Content-Disposition: attachment; filename="' . \basename($file) . '"');
		\header('Content-Length: ' . \filesize($file));

		\readfile($file);
		exit;
	}
} catch (\Exception $e) {
	require_once($configsPath . 'exceptionsHandling.php');
	
	// TODO: prod/test und dev unterscheidung
	\Packages\Core\Utility\DebugUtility::debug(
		$exceptionDataArray,
		__FILE__ . ' -> ' . __LINE__
	);
	
	die();
}