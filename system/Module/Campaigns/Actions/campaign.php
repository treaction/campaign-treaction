<?php
\header('Content-Type: text/html; charset=utf-8');



/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * 
 * $debugLogManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */


$resultData = $resultViewMode = null;

// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);


$actionMethod = isset($_POST['actionMethod']) ? \DataFilterUtils::filterData($_POST['actionMethod']) : '';
$debugLogManager->logData('actionMethod', $actionMethod);

$actionPath = __DIR__ . \DIRECTORY_SEPARATOR . 'Campaign' . \DIRECTORY_SEPARATOR;

try {
	if (\strlen($actionMethod) === 0) {
		throw new \BadMethodCallException('No action given!', 1424768295);
	} else {
		if ((\file_exists($actionPath . $actionMethod . '.php')) === true) {
			$mandant = \RegistryUtils::get('clientEntity')->getAbkz();
			require_once(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');
			
			/**
			 * init
			 * 
			 * $campaignManager
			 * $swiftMailerWebservice
			 */
			require_once(DIR_configsInit . 'initCampaignManager.php');
			/* @var $campaignManager \CampaignManager */
			
			require_once(DIR_configsInit . 'initSwiftMailerWebservice.php');
			/* @var $swiftMailerWebservice \SwiftMailerWebservice */
			
			
			// debug
			$debugLogManager->beginGroup($actionMethod);
			
			// campaignFunctions
			require_once(DIR_functions . 'campaignFunctions.php');
			
			// actionMethod
			require_once($actionPath . $actionMethod . '.php');
			
			// debug
			$debugLogManager->endGroup();
		} else {
			throw new \BadMethodCallException('Unknown action: ' . $actionMethod, 1424768303);
		}
	}
} catch (\Exception $e) {
	require(DIR_configs . 'exceptions.php');
	
	\DebugAndExceptionUtils::sendDebugData(
		$e,
		__FILE__ . ' -> ' . $actionMethod
	);
	
	die($exceptionMessage);
}

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());

if (!\is_null($resultData)) {
	switch ($resultViewMode) {
		case 'json':
			echo \json_encode($resultData);
			break;
		
		default:
			break;
	}
}