<?php
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */
/* @var $debugLogManager \debugLogManager */
/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $campaignDataArray array */


/**
 * updateMailingInDeliverySystemAndLogData
 * 
 * @param \ClientManager $clientManager
 * @param \CampaignManager $campaignManager
 * @param \debugLogManager $debugLogManager
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @throws \DomainException
 * @return void
 */
function updateMailingInDeliverySystemAndLogData(\ClientManager $clientManager, \CampaignManager $campaignManager, \debugLogManager $debugLogManager, \CampaignWidthCustomerAndContactPersonEntity $campaignEntity) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getDeliverySystemDistributorWidthDeliverySystemEntityById
	 * 
	 * debug
	 */
	$deliverySystemDistributorWidthClientDeliverySystemEntity = $clientManager->getDeliverySystemDistributorWidthDeliverySystemEntityById($campaignEntity->getDsd_id());
	if (!($deliverySystemDistributorWidthClientDeliverySystemEntity instanceof \DeliverySystemDistributorWidthClientDeliverySystemEntity)) {
		throw new \DomainException('invalid DeliverySystemDistributorWidthClientDeliverySystemEntity');
	}
	$debugLogManager->logData('deliverySystemDistributorWidthClientDeliverySystemEntity', $deliverySystemDistributorWidthClientDeliverySystemEntity);


	/**
	 * DeliverySystemFactory::getAndInitDeliverySystem
	 * 
	 * debug
	 */
	$deliverySystemWebservice = \DeliverySystemFactory::getAndInitDeliverySystem($deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity());
	$debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);

	/**
	 * connect to deliveryWebservice
	 */
	$deliverySystemWebservice->login();

	if ((file_exists(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'updateMailingInDeliverySystem.php')) === true) {
		require_once(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'updateMailingInDeliverySystem.php');
	}

	/**
	 * disconnect from deliveryWebservice
	 */
	$deliverySystemWebservice->logout();
	
	// debug
	$debugLogManager->endGroup();
}

/**
 * updateCampaignStartDateInDeliverySystem
 * 
 * @param \ClientManager $clientManager
 * @param \CampaignManager $campaignManager
 * @param \debugLogManager $debugLogManager
 * @param \CampaignEntity $campaignEntity
 * @throws \DomainException
 * @return void
 */
function updateCampaignStartDateInDeliverySystem(\ClientManager $clientManager, \CampaignManager $campaignManager, \debugLogManager $debugLogManager, \CampaignEntity &$campaignEntity) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getDeliverySystemDistributorWidthDeliverySystemEntityById
	 * 
	 * debug
	 */
	$deliverySystemDistributorWidthClientDeliverySystemEntity = $clientManager->getDeliverySystemDistributorWidthDeliverySystemEntityById($campaignEntity->getDsd_id());
	if (!($deliverySystemDistributorWidthClientDeliverySystemEntity instanceof \DeliverySystemDistributorWidthClientDeliverySystemEntity)) {
		throw new \DomainException('invalid DeliverySystemDistributorWidthClientDeliverySystemEntity');
	}
	$debugLogManager->logData('deliverySystemDistributorWidthClientDeliverySystemEntity', $deliverySystemDistributorWidthClientDeliverySystemEntity);


	/**
	 * DeliverySystemFactory::getAndInitDeliverySystem
	 * 
	 * debug
	 */
	$deliverySystemWebservice = \DeliverySystemFactory::getAndInitDeliverySystem($deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity());
	$debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);

	/**
	 * connect to deliveryWebservice
	 */
	$deliverySystemWebservice->login();

	if ((file_exists(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'updateCampaignStartDateInDeliverySystem.php')) === true) {
		require_once(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'updateCampaignStartDateInDeliverySystem.php');
	}

	/**
	 * disconnect from deliveryWebservice
	 */
	$deliverySystemWebservice->logout();
	
	// debug
	$debugLogManager->endGroup();
}



$campaignEntity->setK_id(\intval($_POST['campaign']['kid']));
$campaignEntity->setNv_id(\intval($_POST['campaign']['nv_id']));

if ($mailingTyp == 'HV') {
	$campaignDataArray = array(
		'agentur_id' => array(
			'value' => $campaignEntity->getCustomerEntity()->getKunde_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'ap_id' => array(
			'value' => $campaignEntity->getContactPersonEntity()->getAp_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'edit_lock' => array(
			'value' => '',
			'dataType' => \PDO::PARAM_STR
		),
		'versendet' => array(
			'value' => $campaignEntity->getVersendet(),
			'dataType' => \PDO::PARAM_INT
		),
		'vertriebler_id' => array(
			'value' => $campaignEntity->getVertriebler_id(),
			'dataType' => \PDO::PARAM_INT
		),
		
		// TODO: deprecated, entfernen
		'agentur' => array(
			'value' => $campaignEntity->getCustomerEntity()->getFirma(),
			'dataType' => \PDO::PARAM_STR
		),
		'ap' => array(
			'value' => $campaignEntity->getContactPersonEntity()->getVorname() . ' ' . $campaignEntity->getContactPersonEntity()->getNachname(),
			'dataType' => \PDO::PARAM_STR
		),
	);
} else {
	// NV
	$campaignDataArray = array(
		'versendet' => array(
			'value' => $campaignEntity->getVersendet(),
			'dataType' => \PDO::PARAM_INT
		),
		'edit_lock' => array(
			'value' => '',
			'dataType' => \PDO::PARAM_STR
		),
		'use_campaign_start_date' => array(
			'value' => $campaignEntity->getUse_campaign_start_date(),
			'dataType' => \PDO::PARAM_INT
		),
	);
}

// debug
$debugLogManager->logData('campaignDataArray', $campaignDataArray);


/**
 * updateMailingInDeliverySystem
 * 
 * au�er kampagne wurde schon versendet
 */
if (\strlen($campaignEntity->getMail_id()) > 0 
	&& (
		$campaignEntity->getStatus() < 20 
		&& $campaignEntity->getStatus() !== 5
	)
) {
	// debug
	$debugLogManager->beginGroup('updateMailingInDeliverySystem');

	/**
	 * getCampaignDataItemById
	 * 
	 * debug
	 */
	$dbCampaignEntity = $campaignManager->getCampaignDataItemById($campaignEntity->getK_id());
	if (!($dbCampaignEntity instanceof \CampaignEntity)) {
		throw new \DomainException('invalid campaignEntity' . $dbCampaignEntity);
	}
	$debugLogManager->logData('dbCampaignEntity', $dbCampaignEntity);

	// nur f�r nv
	if ($dbCampaignEntity->getK_id() !== $dbCampaignEntity->getNv_id()) {
		/**
		 * updateMailingDistributorInDeliverySystemAndLogData
		 */
		\updateMailingInDeliverySystemAndLogData(
			$clientManager,
			$campaignManager,
			$debugLogManager,
			$campaignEntity
		);

		/**
		 * bei �nderungen von useCampaignStartDate bzw. versanddatum
		 * 
		 * TODO: evtl. anpassen dass nur bei bestimmte aktion m�glich ist
		 */
		if ($dbCampaignEntity->getUse_campaign_start_date() !== $campaignEntity->getUse_campaign_start_date() 
			|| $dbCampaignEntity->getDatum()->format('U') <> $campaignEntity->getDatum()->format('U')
		) {
			/**
			 * updateCampaignStartDateInDeliverySystem
			 */
			\updateCampaignStartDateInDeliverySystem(
				$clientManager,
				$campaignManager,
				$debugLogManager,
				$campaignEntity
			);

			// mail_id ggbf. updaten
			$campaignDataArray['mail_id'] = array(
				'value' => $campaignEntity->getMail_id(),
				'dataType' => \PDO::PARAM_STR
			);
			$campaignDataArray['status'] = array(
				'value' => $campaignEntity->getStatus(),
				'dataType' => \PDO::PARAM_INT
			);
		}
	}

	// debug
	$debugLogManager->endGroup();
}


/**
 * createCampaignDataArray
 * 
 * debug
 */
$newCampaignDataArray = \array_merge(\createCampaignDataArray($campaignEntity), $campaignDataArray);
$debugLogManager->logData('newCampaignDataArray', $newCampaignDataArray);
unset($campaignDataArray);


/**
 * updateCampaignAndAddLogItem
 * 
 * debug
 */
$resUpdateCampaignEdit = $campaignManager->updateCampaignAndAddLogItem(
	$campaignEntity->getK_id(),
	$campaignEntity->getNv_id(),
	\intval($_SESSION['benutzer_id']),
	2,
	0,
	$newCampaignDataArray
);
$debugLogManager->logData('resUpdateCampaignEdit', $resUpdateCampaignEdit);


/**
 * updateCampaignDataRowByNvId
 * 
 * update agentur_id,ap_id
 */
$resUpdateCampaignByNvId = $campaignManager->updateCampaignDataRowByNvId(
	$campaignEntity->getNv_id(),
	array(
		'agentur_id' => array(
			'value' => $campaignEntity->getCustomerEntity()->getKunde_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'ap_id' => array(
			'value' => $campaignEntity->getContactPersonEntity()->getAp_id(),
			'dataType' => \PDO::PARAM_INT
		),
		// TODO: deprecated, entfernen
		'agentur' => array(
			'value' => $campaignEntity->getCustomerEntity()->getFirma(),
			'dataType' => \PDO::PARAM_STR
		),
		'ap' => array(
			'value' => $campaignEntity->getContactPersonEntity()->getVorname() . ' ' . $campaignEntity->getContactPersonEntity()->getNachname(),
			'dataType' => \PDO::PARAM_STR
		),
	)
);
$debugLogManager->logData('resUpdateCampaignByNvId', $resUpdateCampaignByNvId);