<?php
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */


$campaignId = isset($_GET['kid']) ? \intval($_GET['kid']) : 0;
if ($campaignId > 0) {
	/**
	 * getCampaignDataItemById
	 * 
	 * debug
	 */
	$campaignEntity = $campaignManager->getCampaignDataItemById($campaignId);
	if (!($campaignEntity instanceof \CampaignEntity)) {
		throw new \DomainException('invalid campaignEntity', 1427121123);
	}
	$debugLogManager->logData('campaignEntity', $campaignEntity);

	if ($campaignEntity->getEdit_lock() == $_SESSION['username']) {
		/**
		 * updateCampaignDataRowByKid
		 * 
		 * debug
		 */
		$resCampaignUpdate = $campaignManager->updateCampaignDataRowByKid(
			$campaignEntity->getK_id(),
			array(
				'edit_lock' => array(
					'value' => '',
					'dataType' => \PDO::PARAM_STR
				)
			)
		);
		$debugLogManager->logData('resCampaignUpdate', $resCampaignUpdate);
	}
}