<?php
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */


// debug
$campaignId = isset($_POST['campaign']['k_id']) ? \intval($_POST['campaign']['k_id']) : 0;
$debugLogManager->logData('campaignId to change', $campaignId);

if ($campaignId > 0) {
	if (!isset($_POST['campaign']['date']) 
		|| $_POST['campaign']['date'] < 0
	) {
		throw new \InvalidArgumentException('no campaign date', 1424876123);
	}
	
	
	/**
	 * getCampaignAndCustomerDataItemById
	 * 
	 * debug
	 */
	$campaignEntity = $campaignManager->getCampaignAndCustomerDataItemById($campaignId);
	if (!($campaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
		throw new \DomainException('no CampaignWidthCustomerAndContactPersonEntity',  1424766475);
	}
	$debugLogManager->logData('campaignEntity', $campaignEntity);
	
       
	/**
	 * updateCampaignDataRowByKid
	 * 
	 * debug
	 */
	$resCampaignUpdate = $campaignManager->updateCampaignDataRowByKid(
		$campaignEntity->getK_id(),
		array(
			'datum' => array(
				'value' => $_POST['campaign']['date'],
				'dataType' => PDO::PARAM_STR
			)
		)
	);
	$debugLogManager->logData('resCampaignUpdate', $resCampaignUpdate);
     
} else {
	throw new \InvalidArgumentException('invalid campaignId', 1424766235);
}