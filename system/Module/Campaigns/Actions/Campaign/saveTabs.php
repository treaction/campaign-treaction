<?php
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */


/**
 * init $clientManager
 */
require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */



if ((int) $_SESSION['benutzer_id'] > 0) {
	if (isset($_POST['saveTabs']['tabs']) 
		&& \is_array($_POST['saveTabs']['tabs'])
	) {
		/**
		 * updateUserDataRowById
		 * 
		 * debug
		 */
		$resUpdateUserDataRowById = $clientManager->updateUserDataRowById(
			$_SESSION['benutzer_id'],
			array(
				'tab_config' => array(
					'value' => \implode(',', $_POST['saveTabs']['tabs']),
					'dataType' => \PDO::PARAM_STR
				)
			)
		);
		$debugLogManager->logData('resUpdateUserDataRowById', $resUpdateUserDataRowById);
	} else {
		throw new \InvalidArgumentException('invalid tabConfigData', 1424947366);
	}
} else {
	throw new \InvalidArgumentException('invalid userId', 1424947235);
}