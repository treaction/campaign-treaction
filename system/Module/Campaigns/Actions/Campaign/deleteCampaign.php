<?php
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */



/**
 * processDeliverySystemWebserviceAndDeleteCampaigns
 * 
 * @param \CampaignEntity $campaignEntity
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @param array $resultData
 * @return boolean
 */
function processDeliverySystemWebserviceAndDeleteCampaigns(\CampaignEntity $campaignEntity, \ClientManager $clientManager, \debugLogManager $debugLogManager, array &$resultData) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getDeliverySystemDistributorWidthDeliverySystemEntityById
	 * 
	 * debug
	 */
	$deliverySystemDistributorWidthClientDeliverySystemEntity = $clientManager->getDeliverySystemDistributorWidthDeliverySystemEntityById($campaignEntity->getDsd_id());
	if (!($deliverySystemDistributorWidthClientDeliverySystemEntity instanceof \DeliverySystemDistributorWidthClientDeliverySystemEntity)) {
		throw new \DomainException('invalid DeliverySystemDistributorWidthClientDeliverySystemEntity', 1424766373);
	}
	$debugLogManager->logData('deliverySystemDistributorWidthClientDeliverySystemEntity', $deliverySystemDistributorWidthClientDeliverySystemEntity);


	/**
	 * DeliverySystemFactory::getAndInitDeliverySystem
	 * 
	 * debug
	 */
	$deliverySystemWebservice = \DeliverySystemFactory::getAndInitDeliverySystem($deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity());
	$debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);


	/**
	 * connect to deliveryWebservice
	 */
	$deliverySystemWebservice->login();
	$asps = $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp();
               switch ($asps) {
                            case '4Wave 2018':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Marketing Sandbox':
                            case 'Stellar 2018':
                            case 'Stellar Basis':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Basis':
                            case '4Wave Black':
                            case 'Stellar Black': 
                            case 'Stellar Reserve':
                            case '4Wave Extra': 
		            case 'Lead World Black':   
                            case 'BlueLeads2019':
                            case 'BlueLeads Basis':   
                            case 'fulle.media Basis':
                            case 'fulle.media 2019':  
                            case 'BlueLeads Extra':
                            case 'fulle.media Extra': 
                            case 'BlueLeads Black':
                            case 'fulle.media Black':    
                            case 'Maileon':
                            case 'Mail-In-One':    
                              $asp = 'Maileon';
                                break;
                            case 'Sendeffect':
                            case 'Stellar SE':
                            case '4Wave SE':
                            case 'CEOO SE':    
                              $asp = 'Sendeffect';
                                break;
                            case 'Promio';
                                $asp = 'Promio';
                                break;
                            case 'Broadmail';
                                $asp = 'Broadmail';
                                break;
                            default:
                             $asp = 'Maileon';
                                break;
                        }
                        
	if ((\file_exists(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'deleteCampaigns.php')) === true) {
		require_once(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'deleteCampaigns.php');
	} else {
		$resultData[$campaignEntity->getK_id()]['deleteFromDeliverySystem'] = false;
		$resultData[$campaignEntity->getK_id()]['deliverySystemNotImplement'] = true;
		
		$result = true;
	}

	/**
	 * disconnect to deliveryWebservice
	 */
	$deliverySystemWebservice->logout();
	
	$debugLogManager->endGroup();

	return $result;
}

/**
 * checkAndProcessDeliverySystemWebservice
 * 
 * @param \CampaignEntity $campaignEntity
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @param array $resultData
 * @return boolean
 */
function checkAndProcessDeliverySystemWebservice(\CampaignEntity $campaignEntity, \ClientManager $clientManager, \debugLogManager $debugLogManager, array &$resultData) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	$result = false;

	// rechte �berpr�fen anhand der session und campagne Status
	if ((int) $_SESSION['rechte'] < 100 
		&& (
			$campaignEntity->getStatus() === 5 
			&& $campaignEntity->getStatus() < 20
		) 
	) {
		$resultData['mailingStatus'][$campaignEntity->getK_id()] = \utf8_encode('Das Mailing (' . $campaignEntity->getK_id() . ') darf nur vom Administrator gelöscht werden!');
	} else {
		if (\strlen($campaignEntity->getMail_id()) > 0) {
			/**
			 * processDeliverySystemWebserviceAndDeleteCampaigns
			 */
			$result = \processDeliverySystemWebserviceAndDeleteCampaigns(
				$campaignEntity,
				$clientManager,
				$debugLogManager,
				$resultData
			);
		} else {
			// debug
			$debugLogManager->logData('campaignIsNotSynchronized', true);

			$resultData[$campaignEntity->getK_id()]['deleteFromDeliverySystem'] = false;
			$resultData[$campaignEntity->getK_id()]['campaignIsNotSynchronized'] = true;
			
			// eintr�ge aus db trotzdem l�schen
			$result = true;
		}
	}
	
	$debugLogManager->endGroup();

	return $result;
}

/**
 * processDeleteCampaignData
 * 
 * @param \CampaignEntity $campaignEntity
 * @param \CampaignManager $campaignManager
 * @param array $delCampaignsStatus
 * @param array $delCampaignLogsStatus
 * @return void
 */
function processDeleteCampaignData(\CampaignEntity $campaignEntity, \CampaignManager $campaignManager, array &$delCampaignsStatus, array &$delCampaignLogsStatus) {
	/**
	 * deleteCampaignDataRowById by campaignManager
	 */
	$delCampaignsStatus[$campaignEntity->getK_id()] = $campaignManager->deleteCampaignDataRowById($campaignEntity->getK_id());

	/**
	 * deleteAllCampaignsLogByKid by logManager
	 */
	#$delCampaignLogsStatus[$campaignEntity->getK_id()] = $campaignManager->deleteAllCampaignsLogByKid($campaignEntity->getK_id());
}

/**
 * processAddActionLogItem
 * 
 * @param array $actionLogDataArray
 * @param \CampaignManager $campaignManager
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @return void
 */
function processAddActionLogItem(array $actionLogDataArray, \CampaignManager $campaignManager, \ClientManager $clientManager, \debugLogManager $debugLogManager) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);

	/**
	 * EmsActionLogEntity
	 */
	$emsActionLogEntity = new \EmsActionLogEntity();
	$emsActionLogEntity->setClient_id($_SESSION['mID']);
	$emsActionLogEntity->setUser_id($_SESSION['benutzer_id']);
	$emsActionLogEntity->setAction_id(22);
	$emsActionLogEntity->setRecord_table($campaignManager->getCampaignTable());

	foreach ($actionLogDataArray as $campaignId) {
		$emsActionLogEntity->setRecord_id($campaignId);

		// debug
		$debugLogManager->logData('emsActionLogEntity', $emsActionLogEntity);

		/**
		 * addEmsActionLogItem
		 * 
		 * debug
		 */
		$idAddActionLogItem = $clientManager->addEmsActionLogItem($emsActionLogEntity);
		$debugLogManager->logData('idAddActionLogItem', $idAddActionLogItem);
	}
	unset($emsActionLogEntity);

	// debug
	$debugLogManager->endGroup();
}

/**
 * getResultAndProcessDeletedCampaignData
 * 
 * @param \CampaignEntity $campaignEntity
 * @param \CampaignManager $campaignManager
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @param array $delCampaignsStatus
 * @param array $delCampaignLogsStatus
 * @param array $actionLogDataArray
 * @param array $resultData
 * @return void
 */
function getResultAndProcessDeletedCampaignData(\CampaignEntity $campaignEntity, \CampaignManager $campaignManager, \ClientManager $clientManager, \debugLogManager $debugLogManager, array &$delCampaignsStatus, array &$delCampaignLogsStatus, array &$actionLogDataArray,  array &$resultData) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * checkAndProcessDeliverySystemWebservice
	 * 
	 * debug
	 */
	$resultCheckAndProcessDeliverySystemWebservice = \checkAndProcessDeliverySystemWebservice(
		$campaignEntity,
		$clientManager,
		$debugLogManager,
		$resultData
	);
	$debugLogManager->logData('resultCheckAndProcessDeliverySystemWebservice', $resultCheckAndProcessDeliverySystemWebservice);
	
	if ($resultCheckAndProcessDeliverySystemWebservice === true) {
		/**
		 * processDeleteCampaignData
		 */
		\processDeleteCampaignData(
			$campaignEntity,
			$campaignManager,
			$delCampaignsStatus,
			$delCampaignLogsStatus
		);
		
		$actionLogDataArray[] = $campaignEntity->getK_id();
		
		$resultData['success'] = true;
		if ((boolean) $resultData[$campaignEntity->getK_id()]['deleteFromDeliverySystem'] === true) {
			$resultData['content'] = \utf8_encode('Kampagne wurde aus EMS und Versandsystem(' . $campaignEntity->getVersandsystem() . ') erfolgreich geloescht.');
		} else {
			$resultData['content'] = \utf8_encode('Kampagne wurde nur aus EMS erfolgreich geloescht.');
		}
	} else {
		$resultData['success'] = false;
	}
	
	// debug
	$debugLogManager->endGroup();
}



/**
 * init $clientManager
 */
require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

// Factory
require_once(DIR_Factory . 'DeliverySystemFactory.php');


$resultViewMode = 'json';
$campaignId = isset($_POST['campaign']['k_id']) ? \intval($_POST['campaign']['k_id']) : 0;
$debugLogManager->logData('campaign to delete', $campaignId);

$resultData = $nvCampaignsDataArray = $allCampaignsData = $delCampaignsStatus = $delCampaignLogsStatus = $actionLogDataArray = array();
if ($campaignId > 0) {
	/**
	 * getCampaignAndCustomerDataItemById
	 * 
	 * debug
	 */
	$campaignEntity = $campaignManager->getCampaignAndCustomerDataItemById($campaignId);
	if (!($campaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
		throw new \DomainException('no CampaignWidthCustomerAndContactPersonEntity',  1424766475);
	}
	$debugLogManager->logData('campaignEntity', $campaignEntity);

	// check if Campaign is a HV-Campaign
	if ($campaignEntity->getK_id() === $campaignEntity->getNv_id()) {
		/**
		 * getNvCampaignDataItemsByKid
		 * 
		 * campaign has nvCampaigns
		 * 
		 * debug
		 */
		$nvCampaignsDataArray = $campaignManager->getNvCampaignDataItemsByKid($campaignEntity->getK_id());
		$debugLogManager->logData('nvCampaignsDataArray', $nvCampaignsDataArray);
	}

	if (\count($nvCampaignsDataArray) > 0) {
		// nvCampaigns and hvCampaigns
		$processDeleteNvCampaignsDataArray = array();

		/**
		 * processDeleteNvCampaignsDataArray
		 * 
		 * delete campaignData from deliverySystem
		 * 
		 * debug
		 */
		foreach ($nvCampaignsDataArray as $nvCampaignEntity) {
			/* @var $nvCampaignEntity \CampaignEntity */
			
			/**
			 * checkAndProcessDeliverySystemWebservice
			 */
			$result = \checkAndProcessDeliverySystemWebservice(
				$nvCampaignEntity,
				$clientManager,
				$debugLogManager,
				$resultData
			);
			if (\strlen($nvCampaignEntity->getMail_id()) > 0) {
				if ((boolean) $result === true) {
					$processDeleteNvCampaignsDataArray[$nvCampaignEntity->getK_id()] = $result;
				}
			} else {
				// eintr�ge aus db trotzdem l�schen
				$processDeleteNvCampaignsDataArray[$nvCampaignEntity->getK_id()] = true;
			}
		}
		// debug
		$debugLogManager->logData('processDeleteNvCampaignsDataArray', $processDeleteNvCampaignsDataArray);

		// delete campaignData from db
		if (\count($nvCampaignsDataArray) === \count($processDeleteNvCampaignsDataArray)) {
			foreach ($nvCampaignsDataArray as $nvCampaignEntity) {
				/* @var $nvCampaignEntity \CampaignEntity */

				if (\array_key_exists($nvCampaignEntity->getK_id(), $processDeleteNvCampaignsDataArray)) {
					/**
					 * delete NV - Campaign
					 * processDeleteCampaignData
					 */
					\processDeleteCampaignData(
						$nvCampaignEntity,
						$campaignManager,
						$delCampaignsStatus,
						$delCampaignLogsStatus
					);

					$actionLogDataArray[] = $nvCampaignEntity->getK_id();
				}
			}

			/**
			 * delete HV - Campaign
			 * 
			 * getResultAndProcessDeletedCampaignData
			 */
			\getResultAndProcessDeletedCampaignData(
				$campaignEntity,
				$campaignManager,
				$clientManager,
				$debugLogManager,
				$delCampaignsStatus,
				$delCampaignLogsStatus,
				$actionLogDataArray,
				$resultData
			);
		} else {
			$resultData['success'] = false;
		}
	} else {
		/**
		 * getResultAndProcessDeletedCampaignData
		 */
		\getResultAndProcessDeletedCampaignData(
			$campaignEntity,
			$campaignManager,
			$clientManager,
			$debugLogManager,
			$delCampaignsStatus,
			$delCampaignLogsStatus,
			$actionLogDataArray,
			$resultData
		);
	}

	// debug
	$debugLogManager->logData('delCampaignsStatus', $delCampaignsStatus);
	$debugLogManager->logData('delCampaignLogsStatus', $delCampaignLogsStatus);
	$debugLogManager->logData('actionLogsDataArray', $actionLogDataArray);

	if (\count($actionLogDataArray) > 0) {
		\processAddActionLogItem(
			$actionLogDataArray,
			$campaignManager,
			$clientManager,
			$debugLogManager
		);
	}

	if ($resultData['success'] !== true) {
		$resultData['content'] = \utf8_encode('Kampagnen konnten nicht gelöscht werden, wegen folgenden Fehlern:') . \nl2br(\chr(13))
			. \nl2br(\implode(\chr(13), $resultData['mailingStatus']))
		;
	}
} else {
	// keine kampagne id �bermittelt
	throw new \InvalidArgumentException('invalid campaignId', 1424766235);
}