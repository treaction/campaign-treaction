<?php
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */
/* @var $swiftMailerWebservice \SwiftMailerWebservice */


/**
 * sendMailToRecipient
 * 
 * @param array $recipientsDataArray
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param \CampaignManager $campaignManager
 * @param \SwiftMailerWebservice $swiftMailerWebservice
 * @param \debugLogManager $debugLogManager
 * @return void
 */
function sendMailToRecipient(array $recipientsDataArray, \CampaignWidthCustomerAndContactPersonEntity $campaignEntity, \CampaignManager $campaignManager, \SwiftMailerWebservice $swiftMailerWebservice, \debugLogManager $debugLogManager) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
	if (isset($_POST['changeStatus']['sendEmail']) 
		&& (int) $_POST['changeStatus']['sendEmail'] === 1
	) {
		$oldCampaignStatus = \CampaignAndCustomerUtils::getStatusFieldById(
			$campaignEntity->getStatus(),
			'label'
		);
		$debugLogManager->logData('oldCampaignStatus', $oldCampaignStatus);
		
		$newCampaignStatus = \CampaignAndCustomerUtils::getStatusFieldById(
			(int) $_POST['campaign']['newStatus'],
			'label'
		);
		$debugLogManager->logData('newCampaignStatus', $newCampaignStatus);
		
		$debugLogManager->logData('sendMailToRecipient', true);
		$debugLogManager->logData('recipientsDataArray', $recipientsDataArray);
		
		/**
		 * createAndSendEmailToRecipientsDataArray
		 */
		$sendEmailResult = \createAndSendEmailToRecipientsDataArray(
			$recipientsDataArray,
			'Status = ' . $newCampaignStatus . ', ' . $campaignEntity->getK_name(), // subject
			'Statusupdate: ' . $campaignEntity->getK_name() . \chr(13)
				. '----------------------------------------------' . \chr(13) 
				. 'Mandant: ' . \RegistryUtils::get('clientEntity')->getMandant() . \chr(13) 
				. 'Kunde: ' . $campaignEntity->getCustomerEntity()->getFirma() . \chr(13) 
				. 'Bearbeiter: ' . \current($recipientsDataArray) . ', ' . \key($recipientsDataArray) . \chr(13) 
				. 'Kampagne: ' . $campaignEntity->getK_name() . \chr(13) 
				. 'Geplante Versandmenge: ' . \FormatUtils::numberFormat($campaignEntity->getVorgabe_m()) . \chr(13) 
				. 'Versanddatum & Uhrzeit: ' . $campaignEntity->getDatum()->format('d.m.Y, H:i') . \chr(13) 
				. '----------------------------------------------' . \chr(13) 
				. 'Alter Status: ' . $oldCampaignStatus . \chr(13) 
				. 'Neuer Status: ' . $newCampaignStatus
			, // emailBody
			$swiftMailerWebservice,
			$campaignEntity,
			$campaignManager,
			$debugLogManager
		);
		$debugLogManager->logData('createAndSendEmailToRecipientsDataArray', $sendEmailResult);
	}
	
	// debug
	$debugLogManager->endGroup();
}



// debug
$campaignId = isset($_POST['campaign']['k_id']) ? \intval($_POST['campaign']['k_id']) : 0;
$debugLogManager->logData('campaignId to change', $campaignId);

if ($campaignId > 0) {
	if (!isset($_POST['campaign']['newStatus']) 
		|| (int) $_POST['campaign']['newStatus'] < 0
	) {
		throw new \InvalidArgumentException('no campaignStatus', 1424876123);
	}
	
	
	/**
	 * getCampaignAndCustomerDataItemById
	 * 
	 * debug
	 */
	$campaignEntity = $campaignManager->getCampaignAndCustomerDataItemById($campaignId);
	if (!($campaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
		throw new \DomainException('no CampaignWidthCustomerAndContactPersonEntity',  1424766475);
	}
	$debugLogManager->logData('campaignEntity', $campaignEntity);
	

	/**
	 * updateCampaignDataRowByKid
	 * 
	 * debug
	 */
	$resCampaignUpdate = $campaignManager->updateCampaignDataRowByKid(
		$campaignEntity->getK_id(),
		array(
			'status' => array(
				'value' => (int) $_POST['campaign']['newStatus'],
				'dataType' => PDO::PARAM_INT
			)
		)
	);
	$debugLogManager->logData('resCampaignUpdate', $resCampaignUpdate);

	/**
	 * addNewLogEntry
	 * 
	 * debug
	 */
	/*$logId = \addNewLogEntry(
		$campaignManager,
		$campaignEntity,
		4,
		(int) $_POST['campaign']['newStatus']
	);
	$debugLogManager->logData('logId', $logId);
	*/
	/**
	 * sendMailToRecipient
	 */
	\sendMailToRecipient(
		array(
			\DataFilterUtils::filterData(
				$_POST['changeStatus']['recipient']['email'],
				\DataFilterUtils::$validateFilterDataArray['email']
			) => \DataFilterUtils::filterData(
				$_POST['changeStatus']['recipient']['name'],
				\DataFilterUtils::$validateFilterDataArray['string']
			)
		),
		$campaignEntity,
		$campaignManager,
		$swiftMailerWebservice,
		$debugLogManager
	);
} else {
	throw new \InvalidArgumentException('invalid campaignId', 1424766235);
}