<?php
session_start();

$sessionDataArray = $_SESSION['S_yuiDataArray'];
$sessionTurnoverArray = $_SESSION['S_turnoverDataArray'];

$excelDataArrayHeader = array(
    'Agentur / Kunde',
    'Umsatz',
    'Umsatz (validiert)',
    'TKP',
    'CPX',
    'Hybrid',
    'Festpreis',
    'Versendet',
    'Gebucht',
    'kosten',
    'Deckungsbeiträgen',
    'eff. TKP'
    
);
$excelDataArrayHeader2 = array(
    'Gesamt',
    'TKP',
    'CPX',
    'Hybrid',
    'Festpreis',
    'Versendet',
    'Gebucht',
    'kosten',
    'Deckungsbeiträgen'   
);
$excelDataArrayContent = array();

foreach ($sessionDataArray as $key => $value) {
    $excelDataArrayContent[$value['company']]= $value;
}

include_once("xlsxwriter.class.php");

$filename = "umsatz.xlsx";
header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');


$rows2 = array(
    $excelDataArrayHeader2,
    $sessionTurnoverArray['total'],
);


$rows =  array(
    array($excelDataArrayHeader),
    $excelDataArrayContent,
);
 
$writer = new \XLSXWriter();
$writer->setAuthor('Some Author'); 
foreach($rows as $rowss){
    foreach ($rowss as $row)
	$writer->writeSheetRow('Tab1', $row);
}
foreach($rows2 as $row2){
$writer->writeSheetRow('Tab2', $row2);
}
$writer->writeToStdOut();
//$writer->writeToFile('example.xlsx');
//echo $writer->writeToString();
exit(0);


