<?php
\header('Content-Type: text/html; charset=utf-8');

/**
 * getOptionListByCount
 * 
 * @param integer $end
 * @return string
 */
function getOptionListByCount($end) {
	$result = '';
	
	for ($i = 1; $i <= $end; $i++) {
		if ($i < 10) {
			$i = '0' . $i;
		}
		
		$result .= \HtmlFormUtils::createOptionItem(
			array(
				'value' => $i
			),
			$i
		);
	}
	
	return $result;
}

/**
 * hourOptionList
 * 
 * @param integer $hour
 * @return string
 */
function hourOptionList($hour) {
	$result = '';
	
	for ($i = 0; $i <= 23; $i++) {
		$selected = '';
		
		if ($hour > 0 && $hour == $i) {
			$selected = 'selected';
		} elseif ($hour == 0 && $i == 9) {
			$selected = 'selected';
		}
		
		if ($i < 10) {
			$i = '0' . $i;
		}
		
		$result .= \HtmlFormUtils::createOptionItem(
			array(
				'value' => $i,
				'selected' => $selected
			),
			$i
		);
	}
	
	return $result;
}

/**
 * minutesOtionList
 * 
 * @param integer $min
 * @return string
 */
function minutesOtionList($min) {
	$result = '';
	
	for ($i = 0; $i <= 59; $i += 10) {
		$selected = '';
		
		if ($min != '00' 
			&& $min == $i
		) {
			$selected = 'selected';
		} elseif ($i == 0) {
			$selected = 'selected';
		}
		
		if ($i < 10) {
			$i = '0' . $i;
		}
		
		$result .= \HtmlFormUtils::createOptionItem(
			array(
				'value' => $i,
				'selected' => $selected
			),
			$i
		);
	}
	
	return $result;
}

/**
 * getSalesNameByApId
 * 
 * @param \CampaignEntity $campaignEntity
 * @param \CampaignManager $campaignManager
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @return string
 */
function getSalesNameByApId(\CampaignEntity $campaignEntity, \CampaignManager $campaignManager, \ClientManager $clientManager, \debugLogManager $debugLogManager) {
	$result = '<span style="color:red">- nicht zugewiesen -</span>';
	
	if ($campaignEntity->getAp_id() > 0) {
		/**
		 * getContactPersonDataItemById
		 * 
		 * debug
		 */
		$contactPersonEntity = $campaignManager->getContactPersonDataItemById($campaignEntity->getAp_id());
		$debugLogManager->logData('contactPersonEntity', $contactPersonEntity);
		
		if ($contactPersonEntity instanceof \ContactPersonEntity) {
			if ($contactPersonEntity->getVertriebler_id() > 0) {
				/**
				 * getUserDataItemById
				 * 
				 * debug
				 */
				$salesEntity = $clientManager->getUserDataItemById($contactPersonEntity->getVertriebler_id());
				$debugLogManager->logData('salesEntity', $salesEntity);
				
				if ($salesEntity instanceof \UserEntity) {
					$result = $salesEntity->getVorname() . ' ' . $salesEntity->getNachname() 
						. \HtmlFormUtils::createHiddenField(
							'campaign[vertriebler_id]',
							$salesEntity->getBenutzer_id()
						)
					;
				}
			}
		}
	}
	
	return $result;
}

/**
 * getSelectedClientClickProfileFromFieldType
 * 
 * @param \CampaignEntity $campaignEntity
 * @param \ClientClickProfileWidthClickProfileEntity $clientClickProfile
 * @param string $fieldType
 * @return string
 */
function getSelectedClientClickProfileFromFieldType(\CampaignEntity $campaignEntity, \ClientClickProfileWidthClickProfileEntity $clientClickProfile, $fieldType) {
	$result = '';
	
	switch ($fieldType) {
		case 'campaign_click_profiles_id':
			if ($campaignEntity->getCampaign_click_profiles_id() === $clientClickProfile->getUid()) {
				$result = 'selected';
			}
			break;
		
		case 'selection_click_profiles_id':
			if ($campaignEntity->getSelection_click_profiles_id() === $clientClickProfile->getUid()) {
				$result = 'selected';
			}
			break;
	}
	return $result;
}

/**
 * createGenderSelectionItem
 * 
 * @param integer $genderKey
 * @param string $checked
 * @param string $genderItem
 * @return string
 */
function createGenderSelectionItem($genderKey, $checked, $genderItem) {
	return \HtmlFormUtils::createRadioboxItemWidthLabel(
		array(
			'name' => 'campaign[genderSelection]',
			'id' => 'campaign_genderSelection_' . $genderKey,
			'value' => $genderKey,
			'checked' => $checked
		),
		$genderItem,
		array(
			'for' => 'campaign_genderSelection_' . $genderKey,
		)
	);
}

/**
 * createCampaignDeliverySystemStartDateRadios
 * 
 * @param array $dataArray
 * @param integer $checkedItem
 * @param boolean $disabled
 * @return string
 */
function createCampaignDeliverySystemStartDateRadios(array $dataArray, $checkedItem, $disabled) {
	$result = '';
	
	foreach ($dataArray as $key => $value) {
		$checked = '';
		if ((int) $key === $checkedItem) {
			$checked = 'checked';
		}
		
		$result .= \HtmlFormUtils::createRadioboxItemWidthLabel(
			array(
				'name' => 'campaign[use_campaign_start_date]',
				'id' => 'campaign_use_campaign_start_date_' . $key,
				'value' => $key,
				'checked' => $checked,
				'disabled' => ($disabled ? 'disabled' : ''),
			),
			$value,
			array(
				'for' => 'campaign_use_campaign_start_date_' . $key,
			)
		);
	}
	
	return $result;
}

/**
 * processCampaignLooked
 * 
 * @param \CampaignEntity $campaignEntity
 * @param \CampaignManager $campaignManager
 * @param \debugLogManager $debugLogManager
 * @return string
 */
function processCampaignLooked(\CampaignEntity $campaignEntity, \CampaignManager $campaignManager, \debugLogManager $debugLogManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	$result = '';
	
	if (\strlen($campaignEntity->getEdit_lock()) > 0 
		&& $campaignEntity->getEdit_lock() != $_SESSION['username']
	) {
		$result = '
			<div style="color:#FFFFFF; background:red; width:327px; padding:3px; text-align:center; font-size:10px;">' 
				. 'Dieses Mailing wird bereits von <b>' . $campaignEntity->getEdit_lock() . '</b> bearbeitet.' 
			. '</div>'
		;
	} else {
		/**
		 * updateCampaignDataRowByKid
		 * 
		 * debug
		 */
		$campaignUpdate = $campaignManager->updateCampaignDataRowByKid(
			$campaignEntity->getK_id(),
			array(
				'edit_lock' => array(
					'value' => $_SESSION['username'],
					'dataType' => \PDO::PARAM_STR
				)
			)
		);
		$debugLogManager->logData('campaignUpdate', $campaignUpdate);
	}
	
	$debugLogManager->endGroup();
	
	return $result;
}



/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * db_connect
 * 
 * $debugLogManager
 * $campaignManager
 * $clientManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = $_SESSION['mandant'];
require_once(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);


// genderDataArray
$genderDataArray = \CampaignAndCustomerUtils::$genderDataArray;
$debugLogManager->logData('genderDataArray', $genderDataArray);
$genderSelectionItem = '';


$kid = isset($_GET['kid']) ? \intval($_GET['kid']) : 0;
$debugLogManager->logData('kid', $kid);

$mailing = isset($_GET['mailing']) ? $_GET['mailing'] : '';
$debugLogManager->logData('mailing', $mailing);

$action = isset($_GET['action']) ? $_GET['action'] : 'new';
$debugLogManager->logData('action', $action);

$menge = isset($_GET['menge']) ? \intval($_GET['menge']) : 0;
$debugLogManager->logData('menge', $menge);

$hvCampaignId = isset($_GET['hvCampaignId']) ? \intval($_GET['hvCampaignId']) : 0;
$debugLogManager->logData('hvCampaignId', $hvCampaignId);

$currentCampaignId = isset($_GET['currentCampaignId']) ? \intval($_GET['currentCampaignId']) : 0;
$debugLogManager->logData('currentCampaignId', $currentCampaignId);


$campaignIsLock = $hvCampaignDate = $checkedB2b = $deliverySystemOptionItems = $deliverySystemDistributorItem = $deliverySystemDistributorDomainItem = null;
$zielgruppeDataArray = array();
$useDateObj = $campaignStartDate_state = $disableNotSelectedClientId = false;

$jsNewKampaign = 'false';

try {
	/**
	 * getCustomersDataItemsByQueryParts
	 * 
	 * debug
	 */
	$customersDataArray = $campaignManager->getCustomersDataItemsByQueryParts(
		array(
			'ORDER_BY' => '`firma` ASC'
		),
		\PDO::FETCH_CLASS
	);
	if (\count($customersDataArray) === 0) {
		throw new \InvalidArgumentException('no customersDataArray');
	}
	$debugLogManager->logData('customersDataArray', $customersDataArray);
	
	
	/**
	 * getActiveUsersDataItemsByClientId
	 * 
	 * debug
	 */
	$userDataArray = $clientManager->getActiveUsersDataItemsByClientId(
		$_SESSION['mID'],
		true
	);
	if (\count($userDataArray) === 0) {
		throw new \InvalidArgumentException('no userDataArray');
    }
	\asort($userDataArray);
	$debugLogManager->logData('userDataArray', $userDataArray);
	
	$dateNow = new \DateTime();
	$debugLogManager->logData('dateNow', $dateNow);
	
	if ($kid > 0) {
		$debugLogManager->beginGroup('edit');
		
		/**
		 * getCampaignDataItemById
		 */
		$campaignEntity = $campaignManager->getCampaignDataItemById($kid);
		if (!($campaignEntity instanceof \CampaignEntity)) {
			throw new \DomainException('no CampaignEntity');
		}
		
		if ($campaignEntity->getK_id() !== $campaignEntity->getNv_id()) {
			/**
			 * getCampaignDataItemById
			 * getDataFromHVCampaign
			 * 
			 * debug
			 */
			$hvCampaignEntity = $campaignManager->getCampaignDataItemById($campaignEntity->getNv_id());
			if (!($hvCampaignEntity instanceof \CampaignEntity)) {
				throw new \DomainException('no CampaignEntity');
			}
			$debugLogManager->logData('hvCampaignEntity', $hvCampaignEntity);
			
			$hvCampaignDate = $hvCampaignEntity->getDatum();
		} else {
			$hvCampaignDate = clone $campaignEntity->getDatum();
		}
		
		if (\strlen($campaignEntity->getAbrechnungsart()) === 0) {
			/**
			 * getDataFromHVCampaign (only for old campaigns)
			 */
			$campaignEntity->setAbrechnungsart(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray[$hvCampaignEntity->getAbrechnungsart()]);
			$campaignEntity->setPreis($hvCampaignEntity->getPreis());
			$campaignEntity->setPreis2($hvCampaignEntity->getPreis2());
			$campaignEntity->setPreis_gesamt($hvCampaignEntity->getPreis_gesamt());
			$campaignEntity->setUnit_price($hvCampaignEntity->getUnit_price());
		} else {
			$campaignEntity->setAbrechnungsart(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray[$campaignEntity->getAbrechnungsart()]);
		}
		
		$tmpBillingTypeDataArray = \array_flip(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray);
		$campaignBillingTypeValue = $tmpBillingTypeDataArray[$campaignEntity->getAbrechnungsart()];
		unset($tmpBillingTypeDataArray);
		$debugLogManager->logData('campaignBillingTypeValue', $campaignBillingTypeValue);
		
		switch ($action) {
			case 'edit':
				/**
				* processCampaignLooked
				*/
				$campaignIsLock = \processCampaignLooked(
					$campaignEntity,
					$campaignManager,
					$debugLogManager
				);
				
				$disableNotSelectedClientId = ($campaignEntity->getMail_id() ? true: false);
				break;
		}
		
		
		/**
		 * getDeliverySystemDistributorWidthDeliverySystemEntityById
		 * 
		 * debug
		 */
		$campaignDeliverySystemDistributorWidthClientDeliverySystemEntity = $clientManager->getDeliverySystemDistributorWidthDeliverySystemEntityById($campaignEntity->getDsd_id());
		if (!($campaignDeliverySystemDistributorWidthClientDeliverySystemEntity instanceof \DeliverySystemDistributorWidthClientDeliverySystemEntity)) {
			throw new \DomainException('no DeliverySystemDistributorWidthClientDeliverySystemEntity');
		}
		$debugLogManager->logData('campaignDeliverySystemDistributorWidthClientDeliverySystemEntity', $campaignDeliverySystemDistributorWidthClientDeliverySystemEntity);
		
		/**
		 * getDeliverySystemDistributorsDataItemsByClientIdAndDeliverySystemId
		 * 
		 * debug
		 */
		$deliverySystemDistributorDataArray = $clientManager->getDeliverySystemDistributorsDataItemsByClientIdAndDeliverySystemId(
			$_SESSION['mID'],
			$campaignDeliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getId(),
			$_SESSION['underClientIds']
		);
		$debugLogManager->logData('deliverySystemDistributorDataArray', $deliverySystemDistributorDataArray);
		
		
		/**
		 * CampaignAndOthersUtils::getDeliverySystemsItems
		 */
		$deliverySystemOptionItems = \CampaignAndOthersUtils::getDeliverySystemsItems(
			$campaignDeliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp_abkz()
		);
		
		/**
		 * CampaignAndOthersUtils::getDeliverySystemDistributorsItems
		 */
		$deliverySystemDistributorItem = \CampaignAndOthersUtils::getDeliverySystemDistributorsItems(
			$deliverySystemDistributorDataArray,
			$campaignDeliverySystemDistributorWidthClientDeliverySystemEntity,
			$disableNotSelectedClientId
		);
		
		
		$zielgruppeDataArray = \explode(', ', $campaignEntity->getZielgruppe());
		// b2b
		if ((in_array('B2B', $zielgruppeDataArray)) === true) {
			$checkedB2b = 'checked="checked"';
		}

		// genderSelection
		foreach ($genderDataArray as $genderKey => $genderItem) {
			$checked = '';
			if (
				(\in_array($genderItem, $zielgruppeDataArray)) === true 
				|| (\in_array('Damen und Herren', $zielgruppeDataArray)) === true
			) {
				$checked = 'checked';
			}
			
			/**
			 * createGenderSelectionItem
			 */
			$genderSelectionItem .= \createGenderSelectionItem(
				$genderKey,
				$checked,
				$genderItem
			);
		}
	} else {
		$debugLogManager->beginGroup('new');
		
		// newCampaign
        $campaignEntity = new \CampaignEntity();
		if (!($campaignEntity instanceof \CampaignEntity)) {
			throw new \DomainException('invalid CampaignEntity');
		}
		
		$jsNewKampaign = 'true';
		
		$firstDeliverySystemItemEntity = \current($_SESSION['deliverySystemsDataArray']);
		/* @var $firstDeliverySystemItemEntity \DeliverySystemEntity */
		$debugLogManager->logData('firstDeliverySystemItemEntity', $firstDeliverySystemItemEntity);
		\reset($_SESSION['deliverySystemsDataArray']);
		
		/**
		 * CampaignAndOthersUtils::getDeliverySystemsItems
		 */
		$deliverySystemOptionItems = \CampaignAndOthersUtils::getDeliverySystemsItems($firstDeliverySystemItemEntity->getAsp_abkz());
		
		// changeDeliverySystem
		echo '
			<script type="text/javascript">
				YAHOO.util.Dom.get("deliverySystem").selectedIndex = 1;
				changeDeliverySystem(
					"' . $firstDeliverySystemItemEntity->getAsp_abkz() . '",
					"deliverySystemDistributor",
					0,
					0
				);
			</script>
		';
		unset($firstDeliverySystemItemEntity);

		// genderSelection
		foreach ($genderDataArray as $genderKey => $genderItem) {
			$checked = '';
			if ((int) $genderKey === 0) {
				$checked = 'checked';
			}
			
			/**
			 * createGenderSelectionItem
			 */
			$genderSelectionItem .= \createGenderSelectionItem(
				$genderKey,
				$checked,
				$genderItem
			);
		}
	}
	
	// debug
	$debugLogManager->logData('deliverySystemDistributorItem', $deliverySystemDistributorItem);
	$debugLogManager->logData('zielgruppeDataArray', $zielgruppeDataArray);
	$debugLogManager->logData('disableNotSelectedClientId', $disableNotSelectedClientId);


	$billingTypeSettings = $mengeText = '';
	if ($mailing == 'NV') {
		if ($menge > 0) {
			$mengeText = '[' . $menge . ']';
		}

		$billingTypeSettings = 'disabled="disabled"'; 
	}

	$priceClass = 'toggleShow';
	$performanceContentClass = $incomeContentClass = 'toggleHide';
	switch ($action) {
		case 'edit':
			$useDateObj = true;

			$performanceContentClass = 'toggleShow';

			switch ($campaignEntity->getAbrechnungsart()) {
				case 'TKP':
				case 'Festpreis':
				case 'Hybrid':
					if ($mailing == 'NV') {
						$jsNewKampaign = 'true';

						$priceClass = $incomeContentClass = 'toggleHide';
					} else {
						$incomeContentClass = 'toggleShow';
					}
					break;

				default:
					$incomeContentClass = 'toggleShow';
					break;
			}

			if ($mailing == 'NV') {
				/**
				* clone campaignDatum to tmpCalcCampaignDate
				* 
				* debug
				*/
			   $tmpCalcCampaignDate = clone $campaignEntity->getDatum();
			   $tmpCalcCampaignDate->sub(new \DateInterval('PT15M'));
			   $debugLogManager->logData('tmpCalcCampaignDate', $tmpCalcCampaignDate);

			   if ((boolean) $campaignEntity->getUse_campaign_start_date() === true 
				   && $dateNow->format('U') > $tmpCalcCampaignDate->format('U')
			   ) {
				   $campaignStartDate_state = true;
			   }
			   unset($tmpCalcCampaignDate);
			}
			break;

		case 'add_nv':
			$useDateObj = true;

			if ($hvCampaignId > 0) {
				// change kid
				$kid = $hvCampaignId;
				$campaignEntity->setUse_campaign_start_date(false);
			}

			$jsNewKampaign = 'true';
			$performanceContentClass = 'toggleShow';

			if ($mailing == 'NV') {
				if (strpos($campaignEntity->getK_name(), '[NV') !== false) {
					$campaignEntity->setK_name($campaignEntity->getK_name() . $mengeText);
				} else {
					$campaignEntity->setK_name($campaignEntity->getK_name() . ' [NV ' . $campaignEntity->getDatum()->format('d.m') . ']' . $mengeText);
				}
			}

			if ($campaignEntity->getDatum()->format('U') < $dateNow->format('U')) {
				$campaignEntity->setDatum('NOW');
			}
			// min. 1 stunde nach hv campagne
			$campaignEntity->getDatum()->modify('+1 hour');
			$campaignEntity->getDatum()->setTime($campaignEntity->getDatum()->format('H'), 0, 0);

			// reset campaignsEntity values
			$campaignEntity->setVorgabe_m(0);
			$campaignEntity->setVersendet(0);
			$campaignEntity->setOpenings(0);
			$campaignEntity->setOpenings_all(0);
			$campaignEntity->setKlicks(0);
			$campaignEntity->setKlicks_all(0);
			$campaignEntity->setLeads(0);
			$campaignEntity->setLeads_u(0);
			$campaignEntity->setStatus(0);
			$campaignEntity->setAdvertising_materials(1); // Optimierung, see: CampaignAndCustomerUtils::$advertisingMaterialsDataArray
			$campaignEntity->setPremium_campaign(0);

			switch ($campaignEntity->getAbrechnungsart()) {
				case 'TKP':
				case 'Festpreis':
				case 'Hybrid':
					$priceClass = 'toggleHide';

					$campaignEntity->setPreis(0);
					$campaignEntity->setPreis2(0);
					$campaignEntity->setUnit_price(0);
					$campaignEntity->setPreis_gesamt(0);
					$campaignEntity->setPreis_gesamt_verify(0);
					break;

				default:
					break;
			}
			break;

		case 'copy':
			$useDateObj = true;

			$jsNewKampaign = 'true';

			// reset campaignsEntity values
			$campaignEntity->setVersendet(0);
			$campaignEntity->setOpenings(0);
			$campaignEntity->setOpenings_all(0);
			$campaignEntity->setKlicks(0);
			$campaignEntity->setKlicks_all(0);
			$campaignEntity->setLeads(0);
			$campaignEntity->setLeads_u(0);
			$campaignEntity->setStatus(0);
			$campaignEntity->setAdvertising_materials(1); // Optimierung, see: CampaignAndCustomerUtils::$advertisingMaterialsDataArray
			$campaignEntity->setPremium_campaign(0);
			break;
	}
	// debug
	$debugLogManager->logData('campaignStartDate_state', $campaignStartDate_state);


	if ($campaignEntity->getVorgabe_o() === '') {
		$campaignEntity->setVorgabe_o($cm_vorgabe_o);
	}

	if ($campaignEntity->getVorgabe_k() === '') {
		$campaignEntity->setVorgabe_k($cm_vorgabe_k);
	}

	if ($campaignEntity->getMailtyp() == 't') {
		$campaignEntity->setVorgabe_o(0);
	}

	$performanceInfoContent = 'Bitte f&uuml;llen Sie die Daten manuell aus.';
	$readOnlyPerformance = '';
	if (\strlen($campaignEntity->getMail_id()) > 0 
		&& (
			$action != 'add_nv' 
			&& $action != 'copy'
		)
	) {
		$performanceInfoContent = 'Daten werden von ' . $campaignEntity->getVersandsystem() . ' automatisch synchronisiert.';
		$readOnlyPerformance = 'readonly="readonly" class="inactive"';
	}

	switch ($campaignEntity->getAbrechnungsart()) {
		case 'TKP':
		case 'Festpreis':
			$leadsContentClass = 'toggleHide';
			break;

		default:
			$leadsContentClass = 'toggleShow';
			break;
	}

	// js and input Settings
	$readOnly = $disabledCampaignDate = '';
	if ($campaignEntity->getStatus() === 5 
		|| $campaignEntity->getStatus() > 20
	) {
		$jsShowOButton = 'false';
		#$readOnly = 'readonly="readonly"';
		#$disabledCampaignDate = 'disabled="disabled"';
	} else {
		$jsShowOButton = 'true';
	}

	// debug
	$debugLogManager->logData('campaignEntity', $campaignEntity);
	$debugLogManager->logData('hvCampaignDate', $hvCampaignDate);
	$debugLogManager->logData('jsShowOButton', $jsShowOButton);
	$debugLogManager->logData('disabledCampaignDate', $disabledCampaignDate);


	/**
	 * customerSelectItem, sales_fullname
	 */
	$customerSelectItem = '';
	$sales_fullname = '<span style="color:red">- nicht zugewiesen -</span>';
	foreach ($customersDataArray as $customerItemEntity) {
		/* @var $customerItemEntity \CustomerEntity */

		$selected = '';
		if ($customerItemEntity->getKunde_id() === $campaignEntity->getAgentur_id()) {
			$selected = 'selected';

			echo '
				<script type="text/javascript">
					getAP(' . $campaignEntity->getAgentur_id() . ', ' . $campaignEntity->getAp_id() . ');
				</script>
			';


			/**
			 * getSalesNameByApId
			 * 
			 * debug
			 */
			$sales_fullname = \getSalesNameByApId(
				$campaignEntity,
				$campaignManager,
				$clientManager,
				$debugLogManager
			);
			$debugLogManager->logData('sales_fullname', $sales_fullname);
		}

		$customerSelectItem .= \HtmlFormUtils::createOptionItem(
			array(
				'value' => $customerItemEntity->getKunde_id(),
				'selected' => $selected
			),
			$customerItemEntity->getFirma()
		);
	}
	
	
	/**
	 * campaignClickProfileSelectionItems
	 * selectionClickProfileSelectionItems
	 */
	$campaignClickProfileSelectionItems = $selectionClickProfileSelectionItems = '';
    #var_dump("test click profiles");
	foreach (\RegistryUtils::get('campaign' . \RegistryUtils::$arrayKeyDelimiter . 'clientClickProfilesDataArray') as $clientClickProfile) {
		/* @var $clientClickProfiles \ClientClickProfileWidthClickProfileEntity */

		$campaignClickProfileSelectionItems .= \HtmlFormUtils::createOptionItem(
			array(
				'value' => $clientClickProfile->getUid(),
				'selected' => \getSelectedClientClickProfileFromFieldType(
					$campaignEntity,
					$clientClickProfile,
					'campaign_click_profiles_id'
				)
			),
            
			$clientClickProfile->getClickProfile()->getTitle() . ' (' . $clientClickProfile->getClickProfile()->getShortcut() . ')'
		);
		#var_dump($campaignClickProfileSelectionItems);
		$selectionClickProfileSelectionItems .= \HtmlFormUtils::createOptionItem(
			array(
				'value' => $clientClickProfile->getUid(),
				'selected' => \getSelectedClientClickProfileFromFieldType(
					$campaignEntity,
					$clientClickProfile,
					'selection_click_profiles_id'
				)
			),
			$clientClickProfile->getClickProfile()->getTitle() . ' (' . $clientClickProfile->getClickProfile()->getShortcut() . ')'
		);
	}


	/**
	 * userCheckboxItems
	 */
	$userCheckboxItems = '';
	foreach ($userDataArray as $userItemEntity) {
		/* @var $userItemEntity UserEntity */
		if (\strlen($userItemEntity->getEmail()) > 0 
			&& (
				\strlen($userItemEntity->getVorname()) > 0 
				|| \strlen($userItemEntity->getNachname()) > 0 
			)
		) {
			$userCheckboxItems .= \HtmlFormUtils::createCheckboxItem(
				array(
					'name' => 'campaign[benachrichtigungEmail][]',
					'value' => $userItemEntity->getEmail()
				),
				$userItemEntity->getVorname() . ' ' . $userItemEntity->getNachname()
			);
		}
	}
	unset($userDataArray);

	$debugLogManager->endGroup();
} catch (\Exception $e) {
	require(DIR_configs . 'exceptions.php');
	
	die($exceptionMessage);
}

#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());

require_once(DIR_Module_Campaigns . 'Templates/Campaign/newOrEdit.php');