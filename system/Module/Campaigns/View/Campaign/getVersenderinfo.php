<?php
\header('Content-Type: text/html; charset=utf-8');


/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * db_connect
 * 
 * $debugLogManager
 * $campaignManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = $_SESSION['mandant'];
require_once(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);

$campaignId = isset($_GET['campaign']['k_id']) ? \intval($_GET['campaign']['k_id']) : 0;
$debugLogManager->logData('campaignId', $campaignId);

try {
	if ($campaignId > 0) {
		/**
		 * getCampaignAndCustomerDataItemById
		 * 
		 * debug
		 */
		$campaignEntity = $campaignManager->getCampaignAndCustomerDataItemById($campaignId);
		/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
		if (!($campaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
			throw new \DomainException('no CampaignWidthCustomerAndContactPersonEntity', 1424766475);
		}
		$debugLogManager->logData('campaignEntity', $campaignEntity);
	} else {
		throw new \InvalidArgumentException('invalid campaignId', 1424766235);
	}
} catch (\Exception $e) {
	$debugLogManager->logData('Exception', $e);
	
	require(DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());

require_once(DIR_Module_Campaigns . 'Templates/Campaign/consignorInfo.php');