<?php
\header('Content-Type: text/html; charset=utf-8');

/**
 * createOrderStatusSelectItems
 * 
 * @param array $statusDataArray
 * @param integer $selectedItem
 * @return string
 */
function createOrderStatusSelectItems(array $statusDataArray, $selectedItem) {
	$result = '';

	foreach ($statusDataArray as $key => $value) {
		$selected = '';
		if ((int) $selectedItem === (int) $key) {
			$selected = 'selected';
		}

		$result .= \HtmlFormUtils::createOptionItem(
			array(
				'value' => $key,
				'selected' => $selected
			),
			$value['label']
		);
	}

	return $result;
}

/**
 * createUserTableContent
 * 
 * @param array $userTabFieldsDataArray
 * @param string $tableHeaderContent
 * @return void
 */
function createUserTableContent(array $userTabFieldsDataArray, &$tableHeaderContent) {
	// createTableRow - Tag
	$tableHeaderRowDataArray = \HtmlTableUtils::createTableRow();
	
	$tableHeaderContent = 
		$tableHeaderRowDataArray['begin'] 
			// checkbox
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				''
			) 
			// Datum
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Datum'
			) 
			// Status
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Status'
			) 
			// Kampagne width companyData
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Kampagne'
			) 
			// Land
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Land'
			)
	;
	
	foreach ($userTabFieldsDataArray as $userField) {
		$allTabFieldsDataArray = \CampaignAndCustomerUtils::$allTabFieldsDataArray;
		$allTabFieldsDataArray['campaignHasDifferentPrices'] = array(
			'label' => 'Kampagne hat unterschiedliche Preise',
			'sublabel' => 'Info'
		);
		
		foreach ($allTabFieldsDataArray as $tabField => $tabArray) {
			if ($userField == $tabField) {
				switch ($tabArray['sublabel']) {
					case 'Re-St':
						$title = $tabArray['label'];
						break;
					
					case 'Z-Rep':
						$title = $tabArray['label'];
						break;
					
					case 'Infomail':
						$title = $tabArray['label'];
						break;
					
					default:
						$title = '';
						break;
				}
				
				$tableHeaderContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
					array(
						'class' => $tabField,
						'title' => $title
					),
					$tabArray['sublabel']
				);
			}
		}
		
		unset($allTabFieldsDataArray);
	}
	
	$tableHeaderContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
		array(),
		''
	) . $tableHeaderRowDataArray['end'];
}

/**
 * getTableContentCellByUserTabDataArray
 * 
 * @param array $userTabFieldsDataArray
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param \CampaignEntity $tmpHvCampaigEntity
 * @param string $campaignType
 * @param integer $nvCampaignsCount
 * @param \ActionLogEntity $campaignActionLogEntity
 * @param boolean $campaignHasDifferentPrices
 * @return string
 */
function getTableContentCellByUserTabDataArray(array $userTabFieldsDataArray, \CampaignWidthCustomerAndContactPersonEntity $campaignEntity, \CampaignEntity $tmpHvCampaigEntity, $campaignType, $nvCampaignsCount, $campaignActionLogEntity, $campaignHasDifferentPrices) {
	$result = '';
	foreach ($userTabFieldsDataArray as $field) {
		switch ($field) {
			case 'dsd_id':
				$result .= \HtmlTableUtils::createTableCellWidthContent(
					array(),
					\CampaignFieldsUtils::getDsdIdResult(
						false,
						$campaignEntity->getDsd_id()
					)
				);
				break;
			
			case 'actionLogInfo':
				$result .= \HtmlTableUtils::createTableCellWidthContent(
					array(),
					($campaignActionLogEntity instanceof \ActionLogEntity) 
						? 'Rechnung wurde am: ' . $campaignActionLogEntity->getTimestamp()->format('d.m.Y H:I') . ' erstellt.'
						: ''
				);
				break;
			
			case 'bearbeiter':
				$result .= \HtmlTableUtils::createTableCellWidthContent(
					array(),
					$campaignEntity->getBearbeiter()
				);
				break;
			case 'monatsrechnung':
				$result .= \HtmlTableUtils::createTableCellWidthContent(
					array(),
					($campaignEntity->getMonatsrechnung() === '1') ? 'JA' : 'NEIN'
				);
				break;
			case 'preis_gesamt':
				$result .= \HtmlTableUtils::createTableCellWidthContent(
					array(
						'style' => \CampaignFieldsUtils::alignRight
					),
					\CampaignFieldsUtils::getTotalPriceResult(
						$campaignType,
						($nvCampaignsCount 
							? true 
							: false
						),
						$campaignEntity,
						$tmpHvCampaigEntity
					)
				);
				break;
			
			case 'gebucht':
				$result .= \HtmlTableUtils::createTableCellWidthContent(
					array(
						'style' => \CampaignFieldsUtils::alignRight
					),
					\CampaignFieldsUtils::getGebuchtResult($campaignEntity)
				);
				break;
			
			case 'preis':
				$result .= \HtmlTableUtils::createTableCellWidthContent(
					array(
						'style' => \CampaignFieldsUtils::alignRight
					),
					\CampaignFieldsUtils::getPriceResult(
						$campaignType,
						false,
						$campaignEntity
					)
				);
				break;
			
			case 'notiz':
				$result .= \HtmlTableUtils::createTableCellWidthContent(
					array(
						'title' => \nl2br(\htmlspecialchars($campaignEntity->getNotiz()))
					),
					\CampaignFieldsUtils::getCropDataResult(
						false,
						\nl2br($campaignEntity->getNotiz())
					)
				);
				break;
			
			case 'zielgruppe':
				$result .= \HtmlTableUtils::createTableCellWidthContent(
					array(
						'title' => \nl2br(\htmlspecialchars($campaignEntity->getZielgruppe()))
					),
					\CampaignFieldsUtils::getCropDataResult(
						false,
						$campaignEntity->getZielgruppe()
					)
				);
				break;
			
			case 'abrechnungsart':
				$result .= \HtmlTableUtils::createTableCellWidthContent(
					array(
						'title' => \nl2br(\htmlspecialchars($campaignEntity->getNotiz()))
					),
					$campaignEntity->getAbrechnungsart()
				);
				break;
			
			case 'leads':
				$result .= \HtmlTableUtils::createTableCellWidthContent(
					array(
						'style' => \CampaignFieldsUtils::alignRight
					),
					($nvCampaignsCount 
						? \FormatUtils::numberFormat($tmpHvCampaigEntity->getLeads()) 
						: \FormatUtils::numberFormat($campaignEntity->getLeads())
					) . ' ('
						. ($nvCampaignsCount 
							? \FormatUtils::numberFormat($tmpHvCampaigEntity->getLeads_u()) 
							: \FormatUtils::numberFormat($campaignEntity->getLeads_u())
						)
						. ')'
				);
				break;
			
			case 'campaignHasDifferentPrices':
				$result .= \HtmlTableUtils::createTableCellWidthContent(
					array(),
					\CampaignFieldsUtils::getCampaignHasDifferentPricesResult($campaignHasDifferentPrices)
				);
				break;
			
			default:
				$getCampaignValue = 'get' . \ucfirst($field);
				
				$result .= \HtmlTableUtils::createTableCellWidthContent(
					array(),
					$campaignEntity->$getCampaignValue()
				);
				break;
		}
                
	}
	return $result;
}
/**
 * getSummePreisByUserTabDataArray
 * 
 * @param array $userTabFieldsDataArray
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param \CampaignEntity $tmpHvCampaigEntity
 * @param string $campaignType
 * @param integer $nvCampaignsCount
 * @param \ActionLogEntity $campaignActionLogEntity
 * @param boolean $campaignHasDifferentPrices
 * @return string
 */
function getSummePreisByUserTabDataArray(array $userTabFieldsDataArray, \CampaignWidthCustomerAndContactPersonEntity $campaignEntity, \CampaignEntity $tmpHvCampaigEntity, $campaignType, $nvCampaignsCount, $campaignActionLogEntity, $campaignHasDifferentPrices) {
	$result = '';
	foreach ($userTabFieldsDataArray as $field) {
		switch ($field) {			
			case 'preis_gesamt':
                            $result = \CampaignFieldsUtils::totalPriceFormat(\CampaignFieldsUtils::getIntegerTotalPriceResult(
						$campaignType,
						($nvCampaignsCount 
							? true 
							: false
						),
						$campaignEntity,
						$tmpHvCampaigEntity
					)
                                    );
                                        
				break;
			

		}
                
	}
	return $result;
}

/**
 * processNvCampaignsAndUpdateTmpHvCampaignEntity
 * 
 * @param integer $nvCampaignsCount
 * @param array $nvCampaignsDataArray
 * @param \CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity
 * @param \CampaignEntity $tmpHvCampaigEntity
 * @param \debugLogManager $debugLogManager
 * @return boolean
 */
function processNvCampaignsAndUpdateTmpHvCampaignEntity($nvCampaignsCount, array $nvCampaignsDataArray, \CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity, \CampaignEntity &$tmpHvCampaigEntity, \debugLogManager $debugLogManager) {
	$result = false;
	
	if ($nvCampaignsCount > 0) {
		$allCampaignsDataArray = array(
			'leads' => 0,
			'leads_u' => 0,
			'unit_price' => 0,
			'totalPrice' => 0
		);
		
		// debug
		$debugLogManager->beginGroup('nvCampaign');
		
		foreach ($nvCampaignsDataArray as $nvCampaignEntity) {
			/* @var $nvCampaignEntity \CampaignEntity */
			if (\strlen($nvCampaignEntity->getAbrechnungsart()) > 0) {
				$nvCampaignEntity->setAbrechnungsart(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray[$nvCampaignEntity->getAbrechnungsart()]);
			}
			
			// debug
			$debugLogManager->logData('nvCampaignEntity', $nvCampaignEntity);
			
			$allCampaignsDataArray['leads'] += $nvCampaignEntity->getLeads();
			$allCampaignsDataArray['leads_u'] += $nvCampaignEntity->getLeads_u();
			$allCampaignsDataArray['unit_price'] += $nvCampaignEntity->getUnit_price();
			$allCampaignsDataArray['totalPrice'] += \FormatUtils::totalPriceCalculationByBillingType($nvCampaignEntity);
			
			if ($hvCampaignEntity->getPreis() <> $nvCampaignEntity->getPreis()) {
				$result = true;
			}
		}
		
		// debug
		$debugLogManager->endGroup();
		
		$tmpHvCampaigEntity->setLeads($allCampaignsDataArray['leads']);
		$tmpHvCampaigEntity->setLeads_u($allCampaignsDataArray['leads_u']);
		$tmpHvCampaigEntity->setUnit_price($allCampaignsDataArray['unit_price']);
		$tmpHvCampaigEntity->setPreis($allCampaignsDataArray['totalPrice']);
		unset($allCampaignsDataArray);
	}
	
	return $result;
}



/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * db_connect
 * 
 * $debugLogManager
 * $campaignManager
 * $clientManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = \RegistryUtils::get('clientEntity')->getAbkz();
require_once(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */



// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);

/*
 * invoiceFilter Settings
 */
$year = isset($_GET['invoice']['filter']['year']) ? \intval($_GET['invoice']['filter']['year']) : \date('Y');
$debugLogManager->logData('year', $year);

$month = isset($_GET['invoice']['filter']['month']) ? \intval($_GET['invoice']['filter']['month']) : \intval(\date('m'));
$debugLogManager->logData('month', $month);

$billingType = isset($_GET['invoice']['filter']['billingType']) 
	? \DataFilterUtils::filterData(
		$_GET['invoice']['filter']['billingType'],
		\DataFilterUtils::$validateFilder['special_chars']
	) 
	: ''
;
$debugLogManager->logData('billingType', $billingType);

$campaignStatus = isset($_GET['invoice']['filter']['campaignStatus']) ? \intval($_GET['invoice']['filter']['campaignStatus']) : 99;
$debugLogManager->logData('campaignStatus', $campaignStatus);


/**
 * today
 * 
 * debug
 */
$today = new \DateTime();
$debugLogManager->logData('today', $today);


/**
 * monthDataArray
 * 
 * debug
 */
$monthDataArray = \DateUtils::$monthDataArray;
$debugLogManager->logData('monthDataArray', $monthDataArray);


/**
 * orderStatusDataArray
 * 
 * debug
 */
$orderStatusDataArray = \CampaignAndCustomerUtils::$campaignsOrderStatusDataArray;
$orderStatusDataArray[31] = \CampaignAndCustomerUtils::$campaignsOthersStatusDataArray[31];
$orderStatusDataArray[0] = array(
	'status' => 0,
	'label' => 'Versendete Kampagnen',
);
\asort($orderStatusDataArray);
$debugLogManager->logData('orderStatusDataArray', $orderStatusDataArray);


$campaign_queryPartsDataArray = array(
    'SELECT' => '*',
    'WHERE' => array(
		'hvCampaigns' => array(
			'sql' => 'nv_id',
			'value' => 'k_id',
			'comparison' => 'fieldEqual'
		),
                'year' => array(
			'sql' => 'YEAR(`datum`)',
			'value' => $year,
			'comparison' => 'integerEqual'
		),
		'month' => array(
			'sql' => 'MONTH(`datum`)',
			'value' => ($month) ? $month : '',
			'comparison' => 'integerEqual'
		),
		'billingType' => array(
			'sql' => 'abrechnungsart',
			'value' => ($billingType) ? $billingType : '',
			'comparison' => \CampaignManager::$fieldEqual
		)
    ),
        'ORDER_BY' => '`datum` ASC ',
	#'ORDER_BY' => '`datum` DESC ',
        #'LIMIT' => '50'                
);

if (($campaignStatus < 30)) {
	$campaignStatusDataArray = array(
		5
	);
	for ($i = 20; $i < 30; $i++) {
		$campaignStatusDataArray[] = $i;
	}
	$debugLogManager->logData('campaignStatusDataArray', $campaignStatusDataArray);

    $campaign_queryPartsDataArray['WHERE']['status'] = array(
		'sql' => '`status`' . \CampaignManager::$fieldIn . '(' . \implode(',', $campaignStatusDataArray) . ')',
        'value' => '',
        'comparison' => ''
      );
} else {
        if($campaignStatus == 99){
            	$campaignStatusDataArray = array(
		  5,
                  30,
                  31,
                  32,
                  33,
                  40              
	);
          for ($i = 20; $i < 30; $i++) {
		$campaignStatusDataArray[] = $i;
	      }
             $debugLogManager->logData('campaignStatusDataArray', $campaignStatusDataArray);

             $campaign_queryPartsDataArray['WHERE']['status'] = array(
		'sql' => '`status`' . \CampaignManager::$fieldIn . '(' . \implode(',', $campaignStatusDataArray) . ')',
                'value' => '',
               'comparison' => ''
      );
        }else{
	$campaign_queryPartsDataArray['WHERE']['status'] = array(
        'sql' => '`status`',
        'value' => $campaignStatus,
        'comparison' => 'integerEqual'
          );
        }
}
$debugLogManager->logData('campaign_queryPartsDataArray', $campaign_queryPartsDataArray);


/**
 * createTable - Tag
 */
$tableDataArray = \HtmlTableUtils::createTable(
    array(
        'class' => 'scrollTable',
		'width' => '100%'
    )
);


$tableData = $tableHeaderContent = $timestamp = '';
try {
	/**
	 * getCampaignsAndCustomerDataItemsByQueryParts
	 * 
	 * debug
	 */
    
	$campaignsDataArray = $campaignManager->getCampaignsAndCustomerDataItemsByQueryParts($campaign_queryPartsDataArray);
        
	#$debugLogManager->logData('campaignsDataArray', $campaignsDataArray);
	
	$campaignsCount = \count($campaignsDataArray);
       
	$debugLogManager->logData('campaignsCount', $campaignsCount);
        
	if ($campaignsCount > 0) {
		// debug
		$debugLogManager->beginGroup('invoiceView');
		
		/**
		 * createUserTableContent
		 */
		$userTabFieldsDataArray = array(
			'dsd_id',
			'actionLogInfo',
			'bearbeiter',
			'preis_gesamt',
                        'monatsrechnung'
		);
		switch ($billingType) {
			case 'CPL':
			case 'CPO':
			case 'CPC':
				$userTabFieldsDataArray[] = 'campaignHasDifferentPrices';
				
				\array_push($userTabFieldsDataArray, 'leads', 'preis', 'notiz', 'zielgruppe');
				break;
			
			default:
				\array_push($userTabFieldsDataArray, 'gebucht', 'preis', 'notiz', 'zielgruppe');
				break;
		}
		\array_push($userTabFieldsDataArray, 'abrechnungsart');
		
		/**
		 * createUserTableContent
		 */
		\createUserTableContent(
			$userTabFieldsDataArray,
			$tableHeaderContent
		);
		
		
		\CampaignFieldsUtils::$textLenght = 40;
		\CampaignFieldsUtils::$bounceHThreshold = $bounceHThreshold;
		\CampaignFieldsUtils::$bounceSThreshold = $bounceSThreshold;
		
		$campaignType = 'HV';
		$c = 0;
                $summe_einnahmen = 0;
                
		foreach ($campaignsDataArray as $hvCampaignEntity) {
			/* @var $hvCampaignEntity \CampaignWidthCustomerAndContactPersonEntity */
			$bgColor = $bgColorFest = ($c % 2 ? '#E4E4E4' : '#FFFFFF');
			
			// debug
			$debugLogManager->beginGroup($hvCampaignEntity->getK_id());
			
			$tmpHvCampaigEntity = new \CampaignEntity();
			if (!($tmpHvCampaigEntity instanceof \CampaignEntity)) {
				throw new \DomainException('no CampaignEntity');
			}
			
			$hvCampaignEntity->setAbrechnungsart(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray[$hvCampaignEntity->getAbrechnungsart()]);
			
			
			/**
			 * getNVCampaignsDataItemsByKid
			 * 
			 * debug
			 */
			$nvCampaignsDataArray = $campaignManager->getNVCampaignsDataItemsByKid($hvCampaignEntity->getK_id());
			#$debugLogManager->logData('nvCampaignsCount', $nvCampaignsCount);
			
			$nvCampaignsCount = \count($nvCampaignsDataArray);
			$debugLogManager->logData('nvCampaignsCount', $nvCampaignsCount);
			
			/**
			 * processNvCampaignsAndUpdateTmpHvCampaignEntity
			 */
			$campaignHasDifferentPrices = \processNvCampaignsAndUpdateTmpHvCampaignEntity(
				$nvCampaignsCount,
				$nvCampaignsDataArray,
				$hvCampaignEntity,
				$tmpHvCampaigEntity,
				$debugLogManager
			);
			$debugLogManager->logData('campaignHasDifferentPrices', $campaignHasDifferentPrices);
			unset($nvCampaignsDataArray);
			
			
			$timestamp2 = $timestamp;
			$timestamp = $hvCampaignEntity->getDatum()->format('d.m.Y');
			$actWeekDay = \DateUtils::getWeekDay(
				$hvCampaignEntity->getDatum(),
				true
			);

			if ($actWeekDay == 'Sa.' 
				|| $actWeekDay == 'So.'
			) {
				$bgColorFest = $bgColor = '#C4DDFF';
			}
			
			$dateStyle = $dateHeader = '';
			$isToday = \DateUtils::isDateToday($hvCampaignEntity->getDatum());
			if ($timestamp != $timestamp2) {
				$dateHeader = $actWeekDay . ', ' . $timestamp;
			}
			if ((boolean) $isToday === true) {
				$dateStyle = 'background-color:#E63C3C; color:#FFFFFF';
				$dateHeader = 'Heute';
			}
			
			// debug
			$debugLogManager->logData('hvCampaignEntity', $hvCampaignEntity);
			$debugLogManager->logData('tmpHvCampaigEntity', $tmpHvCampaigEntity);
			
                        
                        $einnahmen = \getSummePreisByUserTabDataArray(
			$userTabFieldsDataArray,
			$hvCampaignEntity,
			$tmpHvCampaigEntity,
			$campaignType,
			$nvCampaignsCount,
			$campaignActionLogEntity,
			$campaignHasDifferentPrices
                                );
                        $summe_einnahmen += $einnahmen;
                       
			// viewData_list
			require(DIR_Module_Campaigns . 'Templates/Invoice/ViewData/list.php');
                        
			$tableData .= $contentList;
			                        
			// debug
			$debugLogManager->endGroup();
			
			$c++;
		}
                
              $summeEinnahmen = \FormatUtils::sumFormat($summe_einnahmen);

		// debug
		$debugLogManager->endGroup();
	} else {
		require_once(DIR_Module_Campaigns . 'Templates/Campaign/viewData_noMailingsFound.php');
		
		$tableData .= $tableNoContent;
	}
} catch (\Exception $e) {
	require(\DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}
$_SESSION['data'] = 
	$tableDataArray['begin'] 
		. \utf8_decode($tableHeaderContent) 
		. \utf8_decode($tableData) 
	. $tableDataArray['end'] 
;

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());

require_once(DIR_Module_Campaigns . 'Templates/Invoice/index.php');