<?php
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */
?>
<div id="suche">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd">
        <span class="hd_label">Suche</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd" style="text-align:left">
        <form id="search_k_form" name="search_k_form" action="#" method="post">
			<table cellpadding="5" cellspacing="0" border="0" style="font-size:11px; width: 100%;">
				<tr>
					<td colspan="2">
						<span class="info">Volltextsuche:</span> <input type="text" name="suchwort" id="suchwort" size="20" style="margin-left: 28px; width: 73%;" />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" width="48%">
						<table cellpadding="5" cellspacing="0" border="0" style="font-size:11px; width: 100%;">
							<tr>
								<td class="info">Firma:</td>
								<td>
									<select style="width:135px;font-size:11px;margin-top:3px" name="kAgentur" id="kAgentur" onchange="getContactPersons(this.value, 'kAnsprechpartner')">
										<option value="">- alle -</option>
										<?php
											echo \getAgenturItems($campaignManager);
										?>
									</select>
								</td>
							</tr>
							<tr id="kAp">
								<td class="info">Ansprechpartner:</td>
								<td>
									<select style="width:135px;font-size:11px;margin-top:3px" name="kAnsprechpartner" id="kAnsprechpartner">
									</select>
								</td>
							</tr>
							<tr>
								<td class="info">Bearbeiter:</td>
								<td>
									<select style="width:135px;font-size:11px;margin-top:3px" name="kEditors" id="kEditors">
										<option value="">- alle -</option>
										<?php
											echo \getEditorsItems(
												$clientManager,
												isset($_POST['kEditors']) ? $_POST['kEditors'] : ''
											);
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="info">Status:</td>
								<td>
									<select style="width:135px;font-size:11px;margin-top:3px" name="kStatus" id="kStatus">
										<option value="">- alle -</option>
										<?php
											echo \getStatusItems(
												\RegistryUtils::get('campaign' . \RegistryUtils::$arrayKeyDelimiter . 'statusDataArray' . \RegistryUtils::$arrayKeyDelimiter . 'all'),
												isset($_POST['kStatus']) ? $_POST['kStatus'] : ''
											);
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="info">Abrechnungsart:</td>
								<td>
									<select style="width:135px;font-size:11px;margin-top:3px" name="kBillingType" id="kBillingType">
										<option value="">- alle -</option>
										<?php
											echo \HtmlFormUtils::createOptionListItemData(
												\RegistryUtils::get('campaign' . \RegistryUtils::$arrayKeyDelimiter . 'searchBillingsTypeDataArray'),
												isset($_POST['kBillingType']) ? $_POST['kBillingType'] : ''
											);
										?>
									</select>
								</td>
							</tr>
						</table>
					</td>
					<td align="left" valign="top">
						<table cellpadding="5" cellspacing="0" border="0" style="font-size:11px; width: 100%;">
							<tr>
								<td class="info">Versandsystem:</td>
								<td>
									<select style="width:135px;font-size:11px;margin-top:3px" name="kDeliverySystem" id="kDeliverySystem" onchange="changeCampaignSearchDeliverySystem(this.value, 'kDeliverySystemDistributor');">
										<option value="">- alle -</option>
										<?php 
											echo \CampaignAndOthersUtils::getDeliverySystemsItems(isset($_POST['kDeliverySystem']) ? $_POST['kDeliverySystem'] : '');
										?>
									</select>
								</td>
							</tr>
							<tr id="kDeliverySystemDistributorCell">
								<td class="info">Verteiler:</td>
								<td>
									<div id="kDeliverySystemDistributorContent"></div>
									<div id="kDeliverySystemDistributor">
										<select style="width:135px;font-size:11px;margin-top:3px" name="kDsdId" id="kDeliverySystemDistributorItems">
										</select>
									</div>
								</td>
							</tr>
							
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td class="info">Sonstiges:<br />(fr&uuml;her Selektion)</td>
								<td>
									<select style="width:135px;font-size:11px;margin-top:3px" name="kOldSelektion" id="kOldSelektion">
										<option value="">- alle -</option>
										<?php
											$countriesDataArray = \CampaignAndOthersUtils::$countriesDataArray;
											$countriesDataArray['M&auml;nner'] = 'M&auml;nner';
											$countriesDataArray['Frauen'] = 'Frauen';

											echo \HtmlFormUtils::createOptionListItemData(
												$countriesDataArray,
												isset($_POST['kOldSelektion']) ? $_POST['kOldSelektion'] : ''
											);
										?>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
				<tr>
                    <td colspan="2">
						<input type="checkbox" name="kExtendedView" id="kExtendedView" value="1" onchange="changeKCampaignSettings(this, 'kCampaignSettings')" /><label for="kExtendedView" class="info ioLabel">Erweiterte Optionen</label>
						
						<div id="kCampaignSettings">
							<div id="hvCampaignView" style="margin: 5px 0;">
								<div class="info" style="margin-bottom: 5px;">Kampagnen Optionen:</div>
								<input type="radio" name="kCampaignSettings[hvCampaignView]" class="hvCampaignView" id="kCampaignSettings_hvCampaignView_allCampaign" value="0" onchange="changeNvCampaign(this, 'showNvCampaign')"  /><label for="kCampaignSettings_hvCampaignView_allCampaign" class="ioLabel">Alle Kampagnen durchsuchen (HV/NV)</label>&nbsp;&nbsp;&nbsp;
								<input type="radio" name="kCampaignSettings[hvCampaignView]" class="hvCampaignView" id="kCampaignSettings_hvCampaignView_onlyHV" value="1" checked="checked" onchange="changeNvCampaign(this, 'showNvCampaign')"  /><label for="kCampaignSettings_hvCampaignView_onlyHV" class="ioLabel">nur HV-Kampagnen durchsuchen</label>
							</div>
							<div id="showNvCampaign" style="margin: 5px 0;">
								<div class="info" style="margin-bottom: 5px;">NV Kampagnen Optionen:</div>
								<input type="radio" name="kCampaignSettings[showNvCampaign]" class="showNvCampaign" id="kCampaignSettings_showNvCampaign_hideNV" value="0" /><label for="kCampaignSettings_showNvCampaign_hideNV" class="ioLabel">Alle NV Kampagnen anzeigen</label>&nbsp;&nbsp;&nbsp;
								<input type="radio" name="kCampaignSettings[showNvCampaign]" class="showNvCampaign" id="kCampaignSettings_showNvCampaign_showNV" value="1" checked="checked" /><label for="kCampaignSettings_showNvCampaign_showNV" class="ioLabel">Nur passende NV Kampagnen anzeigen</label>
							</div>
						</div>
					</td>
                </tr>
				<tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="info">Zeitraum ausw&auml;hlen:</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="hidden" id="in" name="in" />
                        <input type="hidden" id="out" name="out" />
                        <div id="cal1Container"></div>
                    </td>
                </tr>
			</table>
        </form>
    </div>
</div> 


<div id="dialog1">
    <div class="panel_label_k">Campaign Manager</div>
    <div class="hd" style="height:35px">
        <div id="k_hd" class="hd_label"></div>
        <div id="hd_sublabel"></div>
    </div>
    <div class="bd" style="text-align:left;">
        <form id="neu_k_form" name="neu_k_form" action="dispatch.php" method="post" enctype="multipart/form-data">
            <div id="k_new">
            </div>
        </form>
    </div>			
</div>

<div id="dialog2">
    <div class="panel_label_k">Campaign Manager</div>
      <div class="hd" id="nv_hd">
        <span class="hd_label">Testadressen</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd">
        <table class="transp">
		
            <tr>
                <td class="details_info">Anrede</td>
                <td class="details2_info">
					<input type="radio" name="person[anrede]" id="personGender_1" class="floatLeft" value="Herr" checked="checked" /> <label for="personGender_1" class="widthAuto" style="color: #000000; padding: 0 15px 2px 10px; clear: none; font-weight: normal;">Herr</label>
					<input type="radio" name="person[anrede]" id="personGender_2" class="floatLeft" value="Frau" /> <label for="personGender_1" class="widthAuto" style="color: #000000; padding: 0 0 2px 10px; clear: none; font-weight: normal;">Frau</label>
					<br style="clear: both;" />
				</td>
            </tr>
            <tr>
                <td class="details_info2">Vorname*</td>
                <td class="details2_info2"><input type="text" name="person[vorname]" id="personFirstname" value="" /></td>
            </tr>
            <tr>
                <td class="details_info">Nachname*</td>
                <td class="details2_info"><input type="text" name="person[nachname]" id="personLastname" value="" /></td>
            </tr>
            <tr>
                <td class="details_info2">Email*</td>
                <td class="details2_info2"><input type="text" name="person[email]" id="personEmail" value="" /></td>
            </tr>
           
        </table>
    </div>			
</div>
<div id="container_versenderinfo">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Versenderinfo</span>
        <span class="hd_sublabel"></span>
    </div>
    <div id="versenderinfo_div" class="bd"></div>	
</div>

<div id="container_vs_anlegen">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Mailing im Versandsystem anlegen / editieren</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd" style="text-align:left;">
        <form id="k_vs_form" name="k_vs_form" action="kampagne/create_in_vs.php" method="post">
            <div id="k_vs"></div>
        </form>
    </div>			
</div>

<div id="container_update">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Kampagnen aktualisieren</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd">
        <div id="update_frame"></div>
    </div>
</div>

<div id="container_info">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Kampagne Details</span>
        <span class="hd_sublabel"></span>
    </div>
    <div id="info_div" class="bd">
        <div id="infocontent"></div>
    </div>
    <div class="ft"></div>
</div>

<div id="container_prognose">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Kampagne Prognose</span>
        <span class="hd_sublabel"></span>
    </div>
    <div id="info_div" class="bd">
        <div id="prognosecontent"><div style="height:300px;"></div></div>
    </div>
    <div class="ft"></div>
</div>

<div id="container_sync">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Kampagne synchronisieren</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd">
        <div id="sync_frame" style="height:600px;text-align:left;overlow:auto;"></div>
    </div>
</div>

<div id="container_testmail">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Testmail versenden</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd">
        <div id="testmail_frame" style="height:450px;text-align:left;overlow:auto;"></div>
    </div>
</div>

<div id="container_testmail_selbst">
</div>

<div id="container_k_neuer_kunde">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Neuer Kunde</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd">
        <table class="transp">
            <tr>
                <td class="details_info">Firma*</td>
                <td class="details2_info"><input type="text" name="customer[firma]" id="cmCustomerCompany" value="" /></td>
            </tr>
            <tr>
                <td class="details_info2">K&uuml;rzel</td>
                <td class="details2_info2"><input type="text" name="customer[firma_short]" id="cmCustomerShortCompany" value="" /></td>
            </tr>
            <tr>
                <td class="details_info">Strasse*</td>
                <td class="details2_info"><input type="text" name="customer[strasse]" id="cmCustomerStreet" value="" /></td>
            </tr>
            <tr>
                <td class="details_info2">Plz*</td>
                <td class="details2_info2"><input type="text" name="customer[plz]" id="cmCustomerZip" value="" /></td>
            </tr>
            <tr>
                <td class="details_info">Ort*</td>
                <td class="details2_info"><input type="text" name="customer[ort]" id="cmCustomerCity" value="" /></td>
            </tr>
			<tr>
                <td class="details_info2" style="white-space: normal;">Land*</td>
                <td class="details2_info2" style="white-space: normal;">
					<input type="text" name="cmCountry" id="cmCustomerCountrySearch" value="" style="position: relative;" />
					<div id="cmCountriesListContainer"></div>
				
					<input type="hidden" name="customer[country_id]" id="cmCustomerCountryId" value="" />
				</td>
            </tr>
			<tr>
				<td class="details_info">Telefon</td>
				<td class="details2_info"><span id="cmCustomerCountryPhoneCode"></span><input type="text" name="customer[telefon]" id="cmCustomerPhone" value="" /></td>
			</tr>
			<tr>
				<td class="details_info2">Fax</td>
				<td class="details2_info2"><span id="cmCustomerCountryPhoneFaxCode"></span><input type="text" name="customer[fax]" id="cmCustomerFax" value="" /></td>
			</tr>
			<tr>
				<td class="details_info">Webseite</td>
				<td class="details2_info">http://<input type="text" name="customer[website]" id="cmCustomerWebsite" style="width:158px" value="" /></td>
			</tr>
			<tr>
				<td class="details_info2">Email</td>
				<td class="details2_info2"><input type="text" name="customer[email]" id="cmCustomerEmail" value="" /></td>
			</tr>
			<tr>
				<td class="details_info">Gesch&auml;ftsf&uuml;hrer</td>
				<td class="details2_info"><input type="text" name="customer[geschaeftsfuehrer]" id="cmCustomerCeo" value="" /></td>
			</tr>
			<tr>
				<td class="details_info2">Registergericht</td>
				<td class="details2_info2"><input type="text" name="customer[registergericht]" id="cmCustomerRegistergericht"  value="" /></td>
			</tr>
			<tr>
				<td class="details_info">USt-IdNr. (Vat.Nr.)*</td>
				<td class="details2_info"><input type="text" name="customer[vat_number]" id="cmCustomerVatNumber" value="" /></td>
			</tr>
			<tr>
				<td class="details_info2">Zahlungsfrist</td>
				<td class="details2_info2"><input type="text" name="customer[payment_deadline]" id="cmCustomerPaymentDeadline" style="width:60px" value="" /> Tage</td>
			</tr>
			<tr>
				<td class="details_info">Opt-In</td>
				<td class="details2_info">
					<select name="customer[data_selection]" id="cmCustomerDataSelection" style1="width:89%">
                        <option value="">- bitte ausw&auml;hlen -</option>
						<?php
							echo \CampaignAndOthersUtils::getCustomerDataSelectionItems(null);
						?>
                    </select>
				</td>
			</tr>
        </table>
    </div>
</div>

<div id="container_k_neuer_kontakt">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Neuer Kontakt</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd">
        <table class="transp">
			<tr>
                <td class="details_info">Vertriebler*</td>
                <td class="details2_info">
                    <select name="contactPerson[vertriebler_id]" id="cmContactPersonVertriebler" style="width:89%">
                        <option value="">- bitte ausw&auml;hlen -</option>
						<?php
							echo \getSalesManagerItems($clientManager);
						?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="details_info2">Anrede</td>
                <td class="details2_info2">
					<input type="radio" name="contactPerson[anrede]" id="cmContactPersonGender_1" class="floatLeft" value="Herr" checked="checked" /> <label for="cmContactPersonGender_1" class="widthAuto" style="color: #000000; padding: 0 15px 2px 10px; clear: none; font-weight: normal;">Herr</label>
					<input type="radio" name="contactPerson[anrede]" id="cmContactPersonGender_2" class="floatLeft" value="Frau" /> <label for="cmContactPersonGender_2" class="widthAuto" style="color: #000000; padding: 0 0 2px 10px; clear: none; font-weight: normal;">Frau</label>
					<br style="clear: both;" />
				</td>
            </tr>
            <tr>
                <td class="details_info">Titel</td>
                <td class="details2_info"><input type="text" name="contactPerson[titel]" id="cmContactPersonTitle" value="" /></td>
            </tr>
            <tr>
                <td class="details_info2">Vorname*</td>
                <td class="details2_info2"><input type="text" name="contactPerson[vorname]" id="cmContactPersonFirstname" value="" /></td>
            </tr>
            <tr>
                <td class="details_info">Nachname*</td>
                <td class="details2_info"><input type="text" name="contactPerson[nachname]" id="cmContactPersonLastname" value="" /></td>
            </tr>
            <tr>
                <td class="details_info2">Email*</td>
                <td class="details2_info2"><input type="text" name="contactPerson[email]" id="cmContactPersonEmail" value="" /></td>
            </tr>
            <tr>
				<td class="details_info">Telefon</td>
				<td class="details2_info"><input type="text" name="contactPerson[telefon]" id="cmContactPersonPhone" value="" /></td>
			</tr>
			<tr>
				<td class="details_info2">Fax</td>
				<td class="details2_info2"><input type="text" name="contactPerson[fax]" id="cmContactPersonFax" value="" /></td>
			</tr>
			<tr>
				<td class="details_info">Mobil</td>
				<td class="details2_info"><input type="text" name="contactPerson[mobil]" id="cmContactPersonMobil" value="" /></td>
			</tr>
			<tr>
				<td class="details_info2">Position</td>
				<td class="details2_info2"><input type="text" name="contactPerson[position]" id="cmContactPersonPosition" value="" /></td>
			</tr>
        </table>
    </div>
</div>

<div id="container_k_statistik">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd">
        <span class="hd_label">Analyse</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd">
        <table class="adr" cellpadding="0" cellspacing="0" border="0" style='font-size:11px;'>
            <tr>
                <td align="left" class="info"><a onclick="setRequest_k_st('overview');" href="#" style="color:#8e8901">&Uuml;bersicht</a>&nbsp; | &nbsp;
                    <select onchange="checkAnalyse(this.value);">
                        <option id="analyse_selected">-- Analyse ausw&auml;hlen --</option>
                        <option value="all">Komplettanalyse</option>
                        <option value="vo">Versandvolumen</option>
                        <option value="kl_o">&Ouml;ffnungen und Klicks</option>
                        <option value="be_a">Betreff und Absender</option>
                        <option value="time_date">Versandzeit und Tag</option>
                        <option value="oeffner">&Ouml;ffner Detailsanalyse</option>
                    </select>
                </td>
                <td align="right">
                    <button onClick="window.open('kampagne/drucken.php', '', 'width=800,height=600,scrollbars=yes');" style="padding:3px;margin-right:36px;border:1px solid #CCCCCC;cursor:pointer" title="drucken"><img src="img/Tango/16/devices/printer.png" /></button>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left"><div id="k_charts" style="width:910px;height:500px;overflow:auto"></div></td>
            </tr>
        </table>
    </div>
</div>

<div id="container_del">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd">
        <span class="hd_label">Kampagne l&ouml;schen</span>
    </div>
    <div class="bd">
        <form name="delform" id="delform" action="Module/Campaigns/Actions/campaign.php" method="post">
			<input type="hidden" name="actionMethod" value="deleteCampaign" />
			
            <table id="del_v" cellpadding="5" cellspacing="0" border="0" align="center">
                <tr>
                    <td width="375" align="center" class="info" valign="top">Soll folgendes Mailing gel&ouml;scht werden?</td>
                </tr>
                <tr>
                    <td align="center" id="mailing"></td>
                </tr>
            </table>
			<input type="hidden" name="campaign[k_id]" id="del_k_id" />
        </form>
    </div>
</div>

<div id="container_noRightDel">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd">
        <span class="hd_label">Kampagne l&ouml;schen</span>
    </div>
    <div class="bd">
        <table id="noRightDelV" cellpadding="5" cellspacing="0" border="0" align="center">
            <tr>
                <td width="375" align="center" class="info" valign="top">Das Mailing kann nur vom Administrator gel&ouml;scht werden!</td>
            </tr>
            <tr>
                <td align="center" id="noRightDelMailing"></td>
            </tr>
        </table>
    </div>
</div>

<div id="container_status">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" style="height:35px">
        <span class="hd_label">Status &auml;ndern</span>
    </div>
    <div class="bd">
        <form name="status_form" id="status_form" action="Module/Campaigns/Actions/campaign.php" method="post">
			<input type="hidden" name="actionMethod" value="changeStatus" />
			
            <div id="status_new"><div style="height:350px"></div></div>
        </form>
    </div>
</div>

<div id="container_date">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" style="height:35px">
        <span class="hd_label">Datum &auml;ndern</span>
    </div>
    <div class="bd">
        <form name="datum_form" id="datum_form" action="Module/Campaigns/Actions/campaign.php" method="post">
            <input type="hidden" name="actionMethod" value="changeDate" />
            
            <div id="date_new"><div style="height:350px"></div></div>
        </form>
    </div>
</div>


<div id="container_cm_report_kontext">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" style="height:35px">
        <span class="hd_label">Reporting</span>
    </div>
    <div class="bd" style="overflow: auto;padding-bottom:15px">
        <div id="rep_details_content" style="width:100%"></div>
        <div id="rep_ext"></div>
    </div>
    <div class="ft" style="padding:5px;background-color:#CCCCCC;"></div>
</div>

<div id="container_tab_felder">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" id="tab_felder_hd">
        <span class="hd_label">Anzeige konfigurieren</span>
    </div>
    <div class="bd">
        <form name="tab_felder_form" id="tab_felder_form" action="Module/Campaigns/Actions/campaign.php" method="post">
			<input type="hidden" name="actionMethod" value="saveTabs" />
			
            <table id="tab_felder_v" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="left" style="color:#666666">Bitte die Felder, die angezeigt werden sollen per Drag &amp; Drop in das linke Fenster ziehen:</td>
                </tr>
                <tr>
                    <td align="left">
                        <div id="anzeige_config"><div style="height:400px"></div></div>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<div id="container_calc">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd"><span class="hd_label">Nachversand Rechner</span></div>
    <div class="bd" id="nv_calc_content">
        <form id="nv_calc" name="nv_calc">
            <table class="transp">
                <tr>
                    <td class="details_hd_info">Mailingdaten</td>
                    <td class="details_hd_info" colspan="2"></td>
                </tr>
                <tr>
                    <td class="details_info">Kampagne</td>
                    <td class="details2_info" id="k_name_td" colspan="2"></td>
                </tr>
                <tr>
                    <td class="details_info2">Versanddatum</td>
                    <td class="details2_info2" id="k_v_datum" colspan="2"></td>
                </tr>
                <tr>
                    <td class="details_info">Gebucht</td>
                    <td class="details2_info" id="calc_gebucht" colspan="2"></td>
                </tr>
                <tr>
                    <td class="details_info2">Versendet</td>
                    <td class="details2_info2" id="calc_versendet" colspan="2"></td>
                </tr>
                <tr>
                    <td class="details_info">&Uuml;SQ</td>
                    <td class="details2_info" id="calc_usq" colspan="2"></td>
                </tr>
                <tr>
                    <td class="details_hd_info"></td>
                    <td class="details_hd_info" style="text-align:left">&Ouml;ffnungsrate</td>
                    <td class="details_hd_info" style="text-align:left">Klickrate</td>
                </tr>
                <tr>
                    <td class="details_info">Aktuell</td>
                    <td class="details2_info"><input type="text" name="o" id="o" style="width:30px;border:0px;font-weight:bold;background-color:#F2F2F2;background-image:none;" readonly/>%</td>
                    <td class="details2_info"><input type="text" name="k" id="k" style="width:30px;border:0px;font-weight:bold;background-color:#F2F2F2;background-image:none;" readonly/>%</td>
                </tr>
                <tr>
                    <td class="details_info2">Ziel</td>
                    <td class="details2_info2"><input type="text" name="oz" id="oz" style="width:30px;text-align:right;" /> %</td>
                    <td class="details2_info2"><input type="text" name="kz" id="kz" style="width:30px;text-align:right;" /> %</td>
                </tr>
                <tr>
                    <td class="details_info">Volumen</td>
                    <td class="details2_info"><input type="text" name="on" id="on" style="width:100px;border:0px;background-color:#F2F2F2;background-image:none;font-weight:bold;font-size:14px" readonly/></td>
                    <td class="details2_info"><input type="text" name="kn" id="kn" style="width:100px;border:0px;background-color:#F2F2F2;background-image:none;font-weight:bold;font-size:14px" readonly/></td>
                </tr>
                <tr>
                    <td class="details_info2">&Uuml;SQ</td>
                    <td class="details2_info2" id="calc_ousq"></td>
                    <td class="details2_info2" id="calc_kusq"></td>
                </tr>
                <tr>
                    <td class="details_info">Aktion</td>
                    <td class="details2_info"><button id="oNVcalc">NV anlegen</button></td>
                    <td class="details2_info"><button id="kNVcalc">NV anlegen</button></td>
                </tr>
            </table>
        </form>
    </div>
</div>

<div id="container_rep_editieren">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Reporting bearbeiten</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd" style="text-align:left;">
        <form id="k_rep_form" name="k_rep_form" action="kampagne/report/rep_save.php" method="post">
            <div id="rep_edit"><div style="height:500px"></div></div>
        </form>
    </div>			
</div>

<div id="container_rechnungsstellung">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd">
        <div class="hd_label">Rechnungstellung</div>
        <div class="hd_sublabel"></div>
    </div>
    <div class="bd" style="text-align:left;">
        <div id="k_rechnungsstellung">
            <span style="font-weight:bold;color:#8e8901">Betreff:</span> <input type="text" id="vorlage_buchhaltungmail_betreff_edit" value="" style="width:350px;padding:3px;" /><br /><br />
            <textarea id="vorlage_buchhaltungmail_edit" name="vorlage_buchhaltungmail_edit" ></textarea>
            <script type="text/javascript">
				CKEDITOR.replace('vorlage_buchhaltungmail_edit', {
					height: 250,
					toolbarStartupExpanded: false,
					toolbar: "Basic"
				});
            </script>
        </div>
    </div>			
</div>

<div id="container_zustellreport">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd">
        <div class="hd_label">Zustellreport</div>
        <div class="hd_sublabel"></div>
    </div>
    <div class="bd" style="text-align:left;">
        <div id="k_zustellreport">
            <form id="k_zustellreport_form" name="k_zustellreport_form" action="kampagne/report/rep_save.php" method="post">
                <div style="height:350px"></div>
            </form>
        </div>
    </div>			
</div>

<div id="container_re_stellung">
    <div class="panel_label">Campaign Manager</div>
    <div class="hd">
        <div class="hd_label">Rechnungsstellungsmail</div>
        <div class="hd_sublabel"></div>
    </div>
    <div class="bd" style="text-align:left;">
        <div id="k_zustellreport">
            <form id="k_re_stellung_form" name="k_re_stellung_form" action="kampagne/report/rep_save.php" method="post">
                <div style="height:350px"></div>
            </form>
        </div>
    </div>			
</div>


<script type="text/javascript">
	function getContactPersons(customerId, contactPersonSelector) {
		if (customerId > 0) {
			showViewById('kAp');
			
			processAjaxRequest(
				'<?php echo \BaseUtils::getAjaxRequestUrl(); ?>',
				'<?php echo \BaseUtils::setAjaxRequestUri('campaign', 'contactPersonsItems'); ?>'
					+ '&customerId=' + parseInt(customerId)
				,
				contactPersonSelector,
				function () {}
			);
		} else {
			removeSelectItemsById(contactPersonSelector);
			resetSelectedValueById(contactPersonSelector);
			hideViewById('kAp');
		}
	}
	
	function changeCampaignSearchDeliverySystem(deliverySystemValue, inputDistributorId) {
		hideViewById(inputDistributorId + 'Cell');
		YAHOO.util.Dom.get(inputDistributorId + 'Content').innerHTML = '';
		
		if (deliverySystemValue !== '') {
			processAjaxRequest(
				'<?php echo \BaseUtils::getAjaxRequestUrl(); ?>',
				'<?php echo \BaseUtils::setAjaxRequestUri('client', 'deliverySystemDistributor'); ?>'
					+ '&deliverySystem=' + deliverySystemValue 
					+ '&createSelectItem=0'
				,
				'',
				function (deliverySystemDistributorData) {
					if (typeof deliverySystemDistributorData !== 'undefined') {
						if ((deliverySystemDistributorData.indexOf('<span')) !== -1) {
							showViewById(inputDistributorId + 'Content');
							YAHOO.util.Dom.get(inputDistributorId + 'Content').innerHTML = deliverySystemDistributorData;
							
							YAHOO.util.Dom.get('kDeliverySystemDistributorItems').innerHTML = '';
							hideViewById(inputDistributorId);
						} else {
							YAHOO.util.Dom.get(inputDistributorId + 'Items').innerHTML = '<option value="">-- Alle --</option>' + deliverySystemDistributorData;

							hideViewById(inputDistributorId + 'Content');
							showViewById(inputDistributorId);
							showViewById(inputDistributorId + 'Items');
						}
						
						showViewById(inputDistributorId + 'Cell');
					}
				}
			);
		} else {
			removeSelectItemsById(inputDistributorId + 'Items');
			hideViewById(inputDistributorId + 'Items');
		}
	}
	
	function changeKCampaignSettings(obj, elementId) {
		if (YAHOO.util.Dom.get(obj).checked) {
			showViewById(elementId);
			showViewById('hvCampaignView');
		} else {
			hideViewById(elementId);
		}
	}
	
	function changeNvCampaign(obj, elementId) {
		if (parseInt(YAHOO.util.Dom.get(obj).value) === 0) {
			showViewById(elementId);
		} else {
			hideViewById(elementId);
		}
	}
	
	
	hideViewById('kAp');
	hideViewById('kDeliverySystemDistributorCell');
	hideViewById('kCampaignSettings');
	hideViewById('showNvCampaign');
	
	
	YAHOO.example.ItemSelectCmCountryHandler = function() {
		var cmCustomerCountryId = YAHOO.util.Dom.get('cmCustomerCountryId');
		var cmCustomerCountryPhoneCode = YAHOO.util.Dom.get('cmCustomerCountryPhoneCode');
		var cmCustomerCountryPhoneFaxCode = YAHOO.util.Dom.get('cmCustomerCountryPhoneFaxCode');
		var cmCustomerPaymentDeadline = YAHOO.util.Dom.get('cmCustomerPaymentDeadline');
		
		var cmCountriesDS = new YAHOO.util.LocalDataSource(<?php echo \json_encode(\RegistryUtils::get('campaign' . \RegistryUtils::$arrayKeyDelimiter . 'staticCountryDataArray')); ?>);
		cmCountriesDS.responseSchema = {fields: ['title', 'phoneCode', 'uid']};
		
		var aAC = new YAHOO.widget.AutoComplete(
			'cmCustomerCountrySearch',
			'cmCountriesListContainer',
			cmCountriesDS,
			{
				useShadow: true,
				resultTypeList: false,
				minQueryLength: 2,
				animVert: 5
			}
		);
		
		aAC.itemSelectEvent.subscribe(function(sType, aArgs) {
			var oData = aArgs[2]; // object literal of selected item's result data
			var uidData = parseInt(oData.uid);
			
			// update hidden form field with the selected item's ID
			cmCustomerCountryId.value = uidData;
			cmCustomerCountryPhoneCode.innerHTML = cmCustomerCountryPhoneFaxCode.innerHTML = '(+' + parseInt(oData.phoneCode) + ') ';
			
			var cmCustomerPaymentDeadlineValue = 10;
			if (uidData !== 54) {
				cmCustomerPaymentDeadlineValue = 14;
			}
			cmCustomerPaymentDeadline.value = cmCustomerPaymentDeadlineValue;
		});
		aAC.unmatchedItemSelectEvent.subscribe(function(sType, aArgs) {
			cmCustomerCountryId.value = cmCustomerCountryPhoneCode.innerHTML = cmCustomerCountryPhoneFaxCode.innerHTML = '';
		});
		
		return {
			countriesDS: cmCountriesDS,
			aAC: aAC
		};
	}();
</script>