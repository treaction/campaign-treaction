<?php
namespace EMS\Campaigns\Invoice;

require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'Dto' . \DIRECTORY_SEPARATOR . 'CountryDto.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'Dto' . \DIRECTORY_SEPARATOR . 'ProductDto.php');

abstract class InvoiceAbstract  {
	const SUMMARIZED_CAMPAIGN_VALUES_DELIMITER = '; ';
	
	/**
	 * @var float
	 */
	protected $cashDiscount = 0.02;
	
	/**
	 * @var string
	 */
	protected $fileExtensions;
	
	protected $settings = array();
	
	/**
	 * @var \DateTime 
	 */
	protected $dateNow;
	
	/**
	 * @var string
	 */
	protected $invoiceResourcePath;
	
	/**
	 * @var string
	 */
	protected $invoiceResultsPath;
	
	/**
	 * @var integer
	 */
	protected $invoiceOrderNumber = 1;
	
	/**
	 * @var boolean
	 */
	protected $campaignHasDifferentPrices = false;
	
	/**
	 * summarized - Settings
	 */
	/**
	 * @var \CampaignWidthCustomerAndContactPersonEntity
	 */
	protected $summarizedCampaignEntity;
	
	/**
	 * @var \EMS\Campaigns\Invoice\Dto\ProductDto
	 */
	protected $productDto;
	
	/**
	 * @var \EMS\Campaigns\Invoice\Dto\CountryDto
	 */
	protected $countryDto;
	
	protected $summarizedCampaignDateDataArray = array();
	protected $summarizedProductDtoDataArray = array();
	
	
	/**
	 * emailContent - Settings
	 */
	protected $tableSettings = array();
	
	protected $emailTableContent;
	protected $failedRecipients = array();
	
	protected $processedInvoiceCampaignsFilesDataArray = array();
	protected $processedInvoiceCampaignsIdsDataArray = array();
	
	
	/**
	 * @var \EMS\Campaigns\Invoice\Writer\WriterAbstract
	 */
	private $writer;
	
	/**
	 * @var \CampaignManager
	 */
	protected $campaignManager;
	
	/**
	 * @var \SwiftMailerWebservice
	 */
	protected $swiftMailerWebservice;
	
	/**
	 * @var \debugLogManager
	 */
	protected $debugLogManager;
	
	
	
	
	
	/**
	 * main
	 * 
	 * @param array $invoicePostDataArray
	 * @param string $fileExtensions
	 * @return array
	 * @throws \InvalidArgumentException
	 */
	public function main(array $invoicePostDataArray, $fileExtensions) {
		/**
		 * init
		 */
		$this->init($invoicePostDataArray);
		$this->fileExtensions = $fileExtensions;
		
		if ($this->settings['templateSelection'] > 0 
			&& \count($this->settings['invoiceCampaigns']) > 0
		) {
			/**
			 * initWriter
			 */
			$this->initWriter();
			
			if ((boolean) $this->settings['summarized'] === true) {
				/**
				 * initAdvancedSettings
				 */
				$this->initAdvancedSettings();
			}
			
			/**
			 * createTableHeadForEmailContent
			 */
			$this->createTableHeadForEmailContent();
			
			/**
			 * processInvoiceCampaigns
			 */
			$this->processInvoiceCampaigns();
			
			/**
			 * processInvoiceEmail
			 */
			$jsonData = $this->processInvoiceEmail();
		} else {
			throw new \InvalidArgumentException('no invoiceData!', 1424770773);
		}
		
		return $jsonData;
	}
	
	
	/**
	 * init
	 * 
	 * @return void
	 */
	protected function init(array $invoicePostDataArray) {
		/**
		 * init - settings
		 */
		$this->settings['templateSelection'] = isset($invoicePostDataArray['templateSelection']) ? \intval($invoicePostDataArray['templateSelection']) : 0;
		$this->settings['summarized'] = isset($invoicePostDataArray['summarized']) ? \intval($invoicePostDataArray['summarized']) : 0;
		$this->settings['headerFooter'] = isset($invoicePostDataArray['headerFooter']) ? (boolean) \intval($invoicePostDataArray['headerFooter']) : false;
		$this->settings['invoiceCampaigns'] = isset($invoicePostDataArray['campaignIds']) ? $invoicePostDataArray['campaignIds'] : 0;
		$this->settings['billingType'] = \DataFilterUtils::filterData($invoicePostDataArray['billingType']);
		$this->debugLogManager->logData('settings', $this->settings);
		
		/**
		 * init - dateTime
		 */
		$this->dateNow = new \DateTime();
		$this->debugLogManager->logData('dateNow', $this->dateNow);
		
		/**
		 * init - paths
		 */
		$invoiceActionPath = \DIR_ModuleClientSettings . 'Campaigns/Invoice/';

		$this->invoiceResourcePath = $invoiceActionPath . 'Resources/' . \RegistryUtils::get('clientEntity')->getAbkz() . \DIRECTORY_SEPARATOR;
		\DirectoryUtils::isReadable($this->invoiceResourcePath);
		$this->debugLogManager->logData('invoiceResourcePath', $this->invoiceResourcePath);

		$this->invoiceResultsPath = $invoiceActionPath . 'Results/' . \RegistryUtils::get('clientEntity')->getAbkz() . \DIRECTORY_SEPARATOR;
		\DirectoryUtils::isWritable($this->invoiceResultsPath);
		$this->debugLogManager->logData('invoiceResultsPath', $this->invoiceResultsPath);
		
		/**
		 * init - tableSettings
		 */
		$this->tableSettings['tableColumnsDataArray'] = $this->initAndGetTableColumnsDataArray();
		$this->tableSettings['tableDataArray'] = \HtmlTableUtils::createTable(
			array(
				'border' => '1',
				'cellpadding' => '0',
				'cellspacing' => '0',
				'style' => 'border-collapse: collapse;'
			)
		);
		$this->tableSettings['tableRowDataArray'] = \HtmlTableUtils::createTableRow(array());
		$this->debugLogManager->logData('tableSettings', $this->tableSettings);
	}
	
	/**
	 * initWriter
	 * 
	 * @throws \InvalidArgumentException
	 * @return void
	 */
	protected function initWriter() {
		/**
		 * init - templateName
		 */
		switch ((int) $this->settings['templateSelection']) {
			case 1:
				$templateName = 'default'; // Standard (DE)
				break;

			case 2:
				$templateName = 'international'; // International
				break;

			default:
				throw new \InvalidArgumentException('Unknown templateSelection: ' . \htmlspecialchars($this->settings['templateSelection']), 1424768973);
		}
		
		if ((boolean) $this->settings['headerFooter'] === true) {
			$templateName .= '_headerAndFooter'; // load template width headerAndFooter
		}
		$this->debugLogManager->logData('templateName', $templateName);
		
		switch ($this->fileExtensions) {
			case 'doc':
			case 'docx':
				require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'Writer/Word.php');
				
				$this->writer = new \EMS\Campaigns\Invoice\Writer\Word();
				$this->writer->setInvoiceResultsPath($this->invoiceResultsPath);
				$this->writer->setInvoiceResourcePath($this->invoiceResourcePath);
				$this->writer->setTemplateName($templateName);
				$this->writer->setDateNow($this->dateNow);
				break;
			
			default:
				throw new \InvalidArgumentException('Unknown writer implementation: ' . $this->fileExtensions, 1432732687);
		}
	}
	
	/**
	 * initAdvancedSettings
	 * 
	 * @return void
	 */
	protected function initAdvancedSettings() {
		$this->summarizedCampaignEntity = new \CampaignWidthCustomerAndContactPersonEntity();
		$this->productDto = new \EMS\Campaigns\Invoice\Dto\ProductDto();
		$this->countryDto = new \EMS\Campaigns\Invoice\Dto\CountryDto();
	}
	
	
	/**
	* createTableHeadForEmailContent
	* 
	* @return void
	*/
	protected function createTableHeadForEmailContent() {
		$this->emailTableContent = $this->tableSettings['tableDataArray']['begin'] 
			. $this->tableSettings['tableRowDataArray']['begin']
		;

		foreach ($this->tableSettings['tableColumnsDataArray'] as $itemDataArray) {
			$this->emailTableContent .= \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => 'border: 1px solid #CCCCCC; white-space: nowrap; padding: 3px;'
				),
				$itemDataArray['rowTitle']
			);
		}

		$this->emailTableContent .= $this->tableSettings['tableRowDataArray']['end'];
	}
	
	/**
	 * processInvoiceCampaigns
	 * 
	 * @throws \DomainException
	 * @return void
	 */
	protected function processInvoiceCampaigns() {
		$this->debugLogManager->beginGroup(__FUNCTION__);
		
		foreach ($this->settings['invoiceCampaigns'] as $campaignId) {
			$this->debugLogManager->beginGroup($campaignId);
			
			/**
			 * getCampaignAndCustomerDataItemById
			 */
			$hvCampaignEntity = $this->campaignManager->getCampaignAndCustomerDataItemById($campaignId);
			if (!($hvCampaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
				throw new \DomainException('no CampaignWidthCustomerAndContactPersonEntity', 1424766475);
			}
			$hvCampaignEntity->setAbrechnungsart(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray[$hvCampaignEntity->getAbrechnungsart()]);
			
			$tmpHvCampaigEntity = new \CampaignWidthCustomerAndContactPersonEntity();
			$tmpHvCampaigEntity->setCustomerEntity($hvCampaignEntity->getCustomerEntity());
			$tmpHvCampaigEntity->setContactPersonEntity($hvCampaignEntity->getContactPersonEntity());
			$tmpHvCampaigEntity->setK_id($hvCampaignEntity->getK_id());
			
			$countryDto = new \EMS\Campaigns\Invoice\Dto\CountryDto();
			
			
			/**
			 * processNvCampaignsDataArray
			 */
			$nvCampaignsDataArray = $this->processNvCampaignsDataArray(
				$hvCampaignEntity,
				$tmpHvCampaigEntity
			);
			$nvCampaignsCount = \count($nvCampaignsDataArray);
			$this->debugLogManager->logData('nvCampaignsCount', $nvCampaignsCount);
			
			
			/**
			 * getPaymentDeadlineAndUpdateCountryDto
			 */
			$paymentDeadline = $this->getPaymentDeadlineAndUpdateCountryDto(
				$hvCampaignEntity,
				$countryDto
			);
			$this->debugLogManager->logData('paymentDeadline', $paymentDeadline);
			$this->debugLogManager->logData('countryDto', $countryDto);
			
			// calculate billingDate
			$billingDate = new \DateTime('+' . $paymentDeadline . ' day');
			$this->debugLogManager->logData('billingDate', $billingDate);
			
			
			if ($this->campaignHasDifferentPrices) {
				$tmpHvCampaigEntity->setAbrechnungsart($hvCampaignEntity->getAbrechnungsart());
				
				/**
				 * processCpxCampaignHasDifferentPrices
				 */
				$this->processCpxCampaignHasDifferentPrices(
					$nvCampaignsDataArray,
					$tmpHvCampaigEntity,
					$countryDto
				);
				
				/**
				 * processSummarizedCampaign
				 */
				$this->processSummarizedCampaign($billingDate);
				
				// reset values
				$this->resetSummarizedValues();
			} else {
				/**
				 * processInvoiceCampaign
				 */
				$this->processInvoiceCampaign(
					$nvCampaignsCount,
					$hvCampaignEntity,
					$tmpHvCampaigEntity,
					$countryDto,
					$billingDate
				);
			}
			
			$this->processedInvoiceCampaignsIdsDataArray[] = $hvCampaignEntity->getK_id();
			
			/**
			 * createCampaignTableRowForEmailContent
			 */
			$this->emailTableContent .= $this->createCampaignTableRowForEmailContent(
				$hvCampaignEntity,
				$tmpHvCampaigEntity,
				$nvCampaignsCount
			);
			
			$this->invoiceOrderNumber++;
			
			
			unset($hvCampaignEntity, $tmpHvCampaigEntity, $countryDto);
			$this->debugLogManager->endGroup();
		}
		
		$this->debugLogManager->logData('invoiceOrderNumber', $this->invoiceOrderNumber);
		
		if ((boolean) $this->settings['summarized'] === true) {
			$this->invoiceOrderNumber = 1;
			
			/**
			 * processSummarizedCampaign
			 */
			$this->processSummarizedCampaign($billingDate);
		}
		
		$this->debugLogManager->endGroup();
	}
	
	/**
	 * processCreateProductDto
	 * 
	 * @param integer $nvCampaignsCount
	 * @param \CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity
	 * @param \CampaignWidthCustomerAndContactPersonEntity $tmpHvCampaigEntity
	 * @return \EMS\Campaigns\Invoice\Dto\ProductDto
	 */
	private function processCreateProductDto($nvCampaignsCount, \CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity, \CampaignWidthCustomerAndContactPersonEntity $tmpHvCampaigEntity) {
		$this->debugLogManager->beginGroup(__FUNCTION__);
		
		/**
		 * updateProductDto
		 */
		$productDto = $this->updateProductDto(
			$nvCampaignsCount,
			$hvCampaignEntity,
			$tmpHvCampaigEntity,
			new \EMS\Campaigns\Invoice\Dto\ProductDto()
		);

		// createProductDataArrayForInvoice
		$productDto->setDataArray(
			$this->createProductDataArrayForInvoice(
				((boolean) $this->settings['summarized'] === true 
					? $this->invoiceOrderNumber
					: 1
				),
				$nvCampaignsCount,
				$hvCampaignEntity,
				$tmpHvCampaigEntity
			)
		);
		
		$this->debugLogManager->logData('productDto', $productDto);
		$this->debugLogManager->endGroup();
		
		return $productDto;
	}
	
	/**
	 * processInvoiceEmail
	 * 
	 * @return array
	 */
	protected function processInvoiceEmail() {
		$this->debugLogManager->beginGroup(__FUNCTION__);
		$this->debugLogManager->logData('processedInvoiceCampaignsFilesDataArray', $this->processedInvoiceCampaignsFilesDataArray);
		$this->debugLogManager->logData('processedInvoiceCampaignsIdsDataArray', $this->processedInvoiceCampaignsIdsDataArray);
		
		$this->emailTableContent .= $this->tableSettings['tableDataArray']['end'];
		$this->debugLogManager->logData('emailTableContent', \utf8_decode($this->emailTableContent));
		
		$resultDataArray = array(
			'success' => false,
			'message' => ''
		);
		
		/**
		 * sendInvoiceStatusInfoEmail
		 */
		$mailerSendResults = $this->sendInvoiceStatusInfoEmail();
		if ((int) $mailerSendResults > 0) {
			/**
			 * deleteCreatedFiles
			 */
			$this->deleteCreatedFiles(\array_flip($this->processedInvoiceCampaignsFilesDataArray));
			
			/**
			 * updateAndLogData
			 */
			$resultUpdateAndLogData = $this->updateAndLogData(\CampaignAndCustomerUtils::$campaignsOrderStatusDataArray[32]['status']);
			
			/**
			 * auf fehler überprüfen
			 */
			if (!\in_array(false, $resultUpdateAndLogData, true)) {
				$resultDataArray['success'] = true;
			} else {
				$resultDataArray['success'] = false;
				$resultDataArray['message'] = 'Technischer fehler!';
			}
		} else {
			// email konnte nicht verschickt werden
			$resultDataArray['message'] = \nl2br(\implode(\chr(11), $this->failedRecipients));
		}
		
		$this->debugLogManager->endGroup();
		
		return $resultDataArray;
	}
	
	/**
	 * processNvCampaignsDataArray
	 * 
	 * @param \CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity
	 * @param \CampaignEntity $tmpHvCampaigEntity
	 * @return array
	 */
	protected function processNvCampaignsDataArray(\CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity, \CampaignWidthCustomerAndContactPersonEntity &$tmpHvCampaigEntity) {
		$this->debugLogManager->beginGroup(__FUNCTION__);
		
		$nvCampaignsDataArray = $this->campaignManager->getNVCampaignsDataItemsByKid($hvCampaignEntity->getK_id());
		#$this->debugLogManager->logData('nvCampaignsDataArray', $nvCampaignsDataArray);
		if (\count($nvCampaignsDataArray) > 0) {
			switch ($this->settings['billingType']) {
				case 'CPL':
				case 'CPO':
				case 'CPC':
					/**
					 * checkIfNvCampaignsHasDifferentPrices
					 */
					$this->checkIfNvCampaignsHasDifferentPrices(
						$nvCampaignsDataArray,
						$hvCampaignEntity
					);
					break;
			}
			
			$allCampaignsDataArray = array(
				'leads' => 0,
				'leads_u' => 0,
				'unit_price' => 0,
				'totalPrice' => 0
			);

			foreach ($nvCampaignsDataArray as $nvCampaignEntity) {
				$this->debugLogManager->beginGroup('nvCampaignEntity');
				
				/* @var $nvCampaignEntity \CampaignEntity */
				if (\strlen($nvCampaignEntity->getAbrechnungsart()) > 0) {
					$nvCampaignEntity->setAbrechnungsart(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray[$nvCampaignEntity->getAbrechnungsart()]);
				}
				$this->debugLogManager->logData('nvCampaignEntity', $nvCampaignEntity);

				$allCampaignsDataArray['leads'] += $nvCampaignEntity->getLeads();
				$allCampaignsDataArray['leads_u'] += $nvCampaignEntity->getLeads_u();
				$allCampaignsDataArray['unit_price'] += $nvCampaignEntity->getUnit_price();
				$allCampaignsDataArray['totalPrice'] += \FormatUtils::totalPriceCalculationByBillingType($nvCampaignEntity);
				
				$this->debugLogManager->endGroup();
			}
			$this->debugLogManager->logData('allCampaignsDataArray', $allCampaignsDataArray);

			$tmpHvCampaigEntity->setLeads($allCampaignsDataArray['leads']);
			$tmpHvCampaigEntity->setLeads_u($allCampaignsDataArray['leads_u']);
			$tmpHvCampaigEntity->setUnit_price($allCampaignsDataArray['unit_price']);
			$tmpHvCampaigEntity->setPreis($allCampaignsDataArray['totalPrice']);
			unset($allCampaignsDataArray);
		}
		
		$this->debugLogManager->logData('hvCampaignEntity', $hvCampaignEntity);
		$this->debugLogManager->logData('tmpHvCampaigEntity', $tmpHvCampaigEntity);
		$this->debugLogManager->endGroup();
		
		return $nvCampaignsDataArray;
	}
	
	/**
	 * updateSummarizedCampaignEntity
	 * 
	 * @param integer $invoiceOrderPositionNumber
	 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
	 * @param \EMS\Campaigns\Invoice\Dto\ProductDto $productDto
	 * @param \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto
	 * @return void
	 */
	protected function updateSummarizedCampaignEntity($invoiceOrderPositionNumber, \CampaignWidthCustomerAndContactPersonEntity $campaignEntity, \EMS\Campaigns\Invoice\Dto\ProductDto $productDto, \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto) {
		$this->debugLogManager->beginGroup(__FUNCTION__);
		
		switch ($this->settings['billingType']) {
			case 'CPL':
			case 'CPO':
			case 'CPC':
				if ($invoiceOrderPositionNumber === 1) {
					/**
					 * campagne Informationen zwischenspeichern
					 * 
					 * nur bei der erste kampagne (interneRechnungsNr)
					 */
					/**
					 * updateSummarizedCampaignEntityForFirstInvoiceOrderNumber
					 */
					$this->updateSummarizedCampaignEntityForFirstInvoiceOrderNumber(
						$campaignEntity,
						$countryDto
					);
				}
				
				$this->summarizedProductDtoDataArray[] = $productDto;
				break;
			
			default:
				if ($invoiceOrderPositionNumber === 1) {
					/**
					 * campagne Informationen zwischenspeichern
					 * 
					 * nur bei der erste kampagne (interneRechnungsNr)
					 */
					/**
					 * updateSummarizedCampaignEntityForFirstInvoiceOrderNumber
					 */
					$this->updateSummarizedCampaignEntityForFirstInvoiceOrderNumber(
						$campaignEntity,
						$countryDto
					);
					
					$this->summarizedCampaignEntity->setK_name($campaignEntity->getK_name());
					$this->summarizedCampaignEntity->setGebucht($campaignEntity->getGebucht());
					$this->summarizedCampaignEntity->setPreis($campaignEntity->getPreis());
					$this->summarizedCampaignEntity->setPreis2($campaignEntity->getPreis2());
					$this->summarizedCampaignEntity->setPreis_gesamt($campaignEntity->getPreis_gesamt());
					$this->summarizedCampaignEntity->setUnit_price($campaignEntity->getUnit_price());
					$this->summarizedCampaignEntity->setZielgruppe($campaignEntity->getZielgruppe());
				} else {
					$this->summarizedCampaignEntity->setK_name($this->summarizedCampaignEntity->getK_name() . self::SUMMARIZED_CAMPAIGN_VALUES_DELIMITER . $campaignEntity->getK_name());
					$this->summarizedCampaignEntity->setGebucht($this->summarizedCampaignEntity->getGebucht() + $campaignEntity->getGebucht());
					$this->summarizedCampaignEntity->setZielgruppe($this->summarizedCampaignEntity->getZielgruppe() . self::SUMMARIZED_CAMPAIGN_VALUES_DELIMITER . $campaignEntity->getZielgruppe());
				}
				
				$this->summarizedCampaignDateDataArray[] = $campaignEntity->getDatum();
				break;
		}
		
		// productDto
		$this->productDto->addNetPrice($productDto->getNetPrice());
		$this->productDto->addCountryTaxPrice($productDto->getCountryTaxPrice());
		$this->productDto->addTotalPrice($productDto->getTotalPrice());
		
		$this->debugLogManager->endGroup();
	}
	
	/**
	 * processSummarizedCampaign
	 * 
	 * @param \DateTime $billingDate
	 * @return void
	 */
	protected function processSummarizedCampaign(\DateTime $billingDate) {
		$this->debugLogManager->beginGroup(__FUNCTION__);
		
		switch ($this->settings['billingType']) {
			case 'CPL':
			case 'CPO':
			case 'CPC':
				// nothing
				break;
			
			default:
				// createSummarizedProductDataArrayForDefaultBillingType
				$this->productDto->setDataArray($this->createSummarizedProductDataArrayForDefaultBillingType());
				
				/**
				 * calculateDiscount
				 */
				$this->productDto = $this->calculateDiscount($this->productDto);
				break;
		}
		
		$this->debugLogManager->logData('summarizedCampaignEntity', $this->summarizedCampaignEntity);
		$this->debugLogManager->logData('productDto', $this->productDto);
		$this->debugLogManager->logData('countryDto', $this->countryDto);
		$this->debugLogManager->logData('summarizedProductDtoDataArray', $this->summarizedProductDtoDataArray);
		
		/**
		 * createInvoice
		 * 
		 * mehrere kampagnen in eine rechnung zusammenfassen
		 */
		$this->writer->createInvoice(
			$billingDate,
			$this->summarizedCampaignEntity,
			$this->productDto,
			$this->countryDto,
			$this->fileExtensions,
			$this->summarizedProductDtoDataArray
		);
		
		// processedInvoiceCampaignsFilesDataArray
		$this->processedInvoiceCampaignsFilesDataArray[$this->summarizedCampaignEntity->getK_id()] = $this->invoiceOrderNumber 
			. '_' . $this->dateNow->format('d.m.Y') 
			. '_' . $this->summarizedCampaignEntity->getCustomerEntity()->getFirma()
		;
		
		$this->debugLogManager->endGroup();
	}
	
	/**
	 * processCpxCampaignHasDifferentPrices
	 * 
	 * @param array $nvCampaignsDataArray
	 * @param \CampaignWidthCustomerAndContactPersonEntity $tmpHvCampaigEntity
	 * @param \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto
	 * @return void
	 */
	protected function processCpxCampaignHasDifferentPrices(array $nvCampaignsDataArray, \CampaignWidthCustomerAndContactPersonEntity $tmpHvCampaigEntity, \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto) {
		$this->debugLogManager->beginGroup(__FUNCTION__);
		
		$invoiceOrderPositionNumber = 1;
		foreach ($nvCampaignsDataArray as $nvCampaignEntity) {
			/* @var $nvCampaignEntity \CampaignEntity */
			
			$productDto = new \EMS\Campaigns\Invoice\Dto\ProductDto();

			// createProductDataArrayForInvoice
			$productDto->setDataArray(
				$this->createProductDataArrayForInvoice(
					$invoiceOrderPositionNumber,
					0,
					$nvCampaignEntity,
					$tmpHvCampaigEntity
				)
			);
			
			$productDto = $this->updateProductDto(
				0,
				$nvCampaignEntity,
				$tmpHvCampaigEntity,
				$productDto
			);
			
			/**
			 * updateSummarizedCampaignEntity
			 */
			$this->updateSummarizedCampaignEntity(
				$invoiceOrderPositionNumber,
				$tmpHvCampaigEntity,
				$productDto,
				$countryDto
			);
			unset($productDto);
			
			$invoiceOrderPositionNumber++;
		}

		$this->debugLogManager->endGroup();
	}
	
	/**
	 * processInvoiceCampaign
	 * 
	 * @param integer $nvCampaignsCount
	 * @param \CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity
	 * @param \CampaignWidthCustomerAndContactPersonEntity $tmpHvCampaigEntity
	 * @param \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto
	 * @param \DateTime $billingDate
	 * @return void
	 */
	protected function processInvoiceCampaign($nvCampaignsCount, \CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity, \CampaignWidthCustomerAndContactPersonEntity $tmpHvCampaigEntity, \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto, \DateTime $billingDate) {
		/**
		 * processCreateProductDto
		 */
		$productDto = $this->processCreateProductDto(
			$nvCampaignsCount,
			$hvCampaignEntity,
			$tmpHvCampaigEntity
		);
		
		if ((boolean) $this->settings['summarized'] === true) {
			/**
			 * updateSummarizedCampaignEntity
			 */
			$this->updateSummarizedCampaignEntity(
				$this->invoiceOrderNumber,
				$hvCampaignEntity,
				$productDto,
				$countryDto
			);
		} else {
			/**
			 * createInvoice (rechnung einzeln erstellen)
			 */
			$this->writer->createInvoice(
				$billingDate,
				$hvCampaignEntity,
				$productDto,
				$countryDto,
				$this->fileExtensions
			);

			// processedOrders
			$this->processedInvoiceCampaignsFilesDataArray[$hvCampaignEntity->getK_id()] = $this->invoiceOrderNumber . '_' . $this->dateNow->format('d.m.Y')
				. '_' . $hvCampaignEntity->getCustomerEntity()->getFirma()
			;
		}
		unset($productDto);
	}
	
	
	/**
	* createCampaignTableRowForEmailContent
	* 
	* @param \CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity
	* @param \CampaignEntity $tmpHvCampaigEntity
	* @param integer $nvCampaignsCount
	* @return string
	*/
	protected function createCampaignTableRowForEmailContent(\CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity, \CampaignEntity $tmpHvCampaigEntity, $nvCampaignsCount) {
		$tableCellConfigDataArray = array(
			'style' => 'border: 1px solid #CCCCCC; white-space: nowrap; padding: 3px; font-size: x-small; font-family: verdana,geneva;'
		);

		$result = $this->tableSettings['tableRowDataArray']['begin'];

		foreach ($this->tableSettings['tableColumnsDataArray'] as $key => $itemArray) {
			switch ($key) {
				case 'customerCompany':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						$tableCellConfigDataArray,
						$hvCampaignEntity->getCustomerEntity()->getFirma()
					);
					break;

				case 'preis':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						$tableCellConfigDataArray,
						\CampaignFieldsUtils::getPriceResult(
							'HV',
							false,
							$hvCampaignEntity
						)
					);
					break;

				case 'preis_gesamt':
					$priceTotal = \FormatUtils::cleanPriceContent(
						\CampaignFieldsUtils::getTotalPriceResult(
							'HV',
							($nvCampaignsCount 
								? true
								: false
							),
							$hvCampaignEntity,
							$tmpHvCampaigEntity
						)
					);

					$result .= \HtmlTableUtils::createTableCellWidthContent(
						$tableCellConfigDataArray,
						\FormatUtils::sumFormat(
							\DataFilterUtils::convertDataToFloatvalue($priceTotal)
						)  . \FormatUtils::euroSignHtmlCode
					);
					break;

				case 'datum':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						$tableCellConfigDataArray,
						$hvCampaignEntity->getDatum()->format('d.m.Y')
					);
					break;

				case 'gebucht':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						$tableCellConfigDataArray,
						\FormatUtils::numberFormat($hvCampaignEntity->getGebucht())
					);
					break;
				
				case 'leads':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						$tableCellConfigDataArray,
						($nvCampaignsCount 
							? \FormatUtils::numberFormat($tmpHvCampaigEntity->getLeads()) 
							: \FormatUtils::numberFormat($hvCampaignEntity->getLeads())
						) . ' ('
							. ($nvCampaignsCount 
								? \FormatUtils::numberFormat($tmpHvCampaigEntity->getLeads_u()) 
								: \FormatUtils::numberFormat($hvCampaignEntity->getLeads_u())
							)
							. ')'
					);
					break;

				default:
					$getCampaignValue = 'get' . \ucfirst($key);

					$result .= \HtmlTableUtils::createTableCellWidthContent(
						$tableCellConfigDataArray,
						$hvCampaignEntity->$getCampaignValue()
					);
					break;
			}
		}

		$result .= $this->tableSettings['tableRowDataArray']['end'];

		return \utf8_decode($result);
	}
	
	/**
	 * updateAndLogData
	 * 
	 * @param integer $newCampaignStatus
	 * @return array
	 */
	protected function updateAndLogData($newCampaignStatus) {
		$this->debugLogManager->beginGroup(__FUNCTION__);
		
		$resultDataArray = array();
		$campaignUpdateDataArray = array(
			'status' => array(
				'value' => $newCampaignStatus,
				'dataType' => \PDO::PARAM_INT
			),
		);
		
		if ((boolean) \BaseUtils::isProdSystem() 
			|| (
				(boolean) \BaseUtils::isTestSystem() 
				&& \BaseUtils::getDebugUserId() !== \RegistryUtils::get('userEntity')->getBenutzer_id()
			)
		) {
			foreach ($this->processedInvoiceCampaignsIdsDataArray as $campaignId) {
				/**
				 * updateCampaignAndAddLogItem
				 */
				$resultDataArray[$campaignId] = $this->campaignManager->updateCampaignAndAddLogItem(
					$campaignId,
					$campaignId,
					\RegistryUtils::get('userEntity')->getBenutzer_id(),
					4,
					(int) $newCampaignStatus,
					$campaignUpdateDataArray
				);
				$this->debugLogManager->logData($campaignId, $resultDataArray[$campaignId]);
			}
		} else {
			/**
			 * isDevSystem 
			 * or isTestSystem width logged debugLoged
			 */
			$resultDataArray[] = true;
		}
		
		$this->debugLogManager->endGroup();

		return $resultDataArray;
	}
	

	/**
	 * updateSummarizedCampaignEntityForFirstInvoiceOrderNumber
	 * 
	 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
	 * @param \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto
	 * @return void
	 */
	private function updateSummarizedCampaignEntityForFirstInvoiceOrderNumber(\CampaignWidthCustomerAndContactPersonEntity $campaignEntity, \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto) {
		$this->debugLogManager->logData(__FUNCTION__, 1);
		
		// summarizedCampaignEntity
		$this->summarizedCampaignEntity->setK_id($campaignEntity->getK_id());
		$this->summarizedCampaignEntity->setAbrechnungsart($campaignEntity->getAbrechnungsart());
		$this->summarizedCampaignEntity->setCustomerEntity($campaignEntity->getCustomerEntity());
		$this->summarizedCampaignEntity->setContactPersonEntity($campaignEntity->getContactPersonEntity());
		
		// countryDto
		$this->countryDto->setPriceInfo($countryDto->getPriceInfo());
		$this->countryDto->setAdditionalInfo($countryDto->getAdditionalInfo());
	}
	
	/**
	 * sendInvoiceStatusInfoEmail
	 * 
	 * @return integer
	 */
	private function sendInvoiceStatusInfoEmail() {
		$this->debugLogManager->beginGroup(__FUNCTION__);
		$this->debugLogManager->logData('swiftMailerWebservice', $this->swiftMailerWebservice);

		/* @var $swiftMessage \Swift_Message */
		$swiftMessage = $this->swiftMailerWebservice->createMessage();

		$swiftMessage
			->setBody(
				\nl2br('Folgende Rechnungen (' . \count($this->processedInvoiceCampaignsFilesDataArray) . ') wurden erstellt bzw. zusammengefa&szlig;t: ' . \chr(13) . \chr(13)) 
					. \utf8_encode($this->emailTableContent)
				,
				'text/html',
				'iso-8859-1'
			)
			->setCharset('utf-8')
			->setSubject('Rechnungsstellung')
			->setTo($_SESSION['u_email'])
		;

		foreach ($this->processedInvoiceCampaignsFilesDataArray as $campaignId => $filename) {
			$swiftMessage->attach(
				\Swift_Attachment::fromPath($this->invoiceResultsPath . $campaignId . '.' . $this->fileExtensions)->setFilename($filename . '.' . $this->fileExtensions)
			);
		}

		/**
		 * sendMessage
		 */
		$mailerSendResults = $this->swiftMailerWebservice->sendMessage($swiftMessage);
		$this->debugLogManager->logData('mailerSendResults', $mailerSendResults);

		/**
		 * getFailedRecipients
		 */
		$this->failedRecipients = $this->swiftMailerWebservice->getFailedRecipients();
		$this->debugLogManager->logData('failedRecipients', $this->failedRecipients);
		$this->swiftMailerWebservice->setFailedRecipients(array());

		$this->debugLogManager->endGroup();

		return $mailerSendResults;
	}

	/**
	 * deleteCreatedFiles
	 * 
	 * @param array $processedInvoiceCampaignsDataArray
	 * @return void
	 */
	private function deleteCreatedFiles(array $processedInvoiceCampaignsDataArray) {
		$this->debugLogManager->beginGroup(__FUNCTION__);
		
		foreach ($processedInvoiceCampaignsDataArray as $filename) {
			/**
			 * remove File
			 */
			$result = \unlink($this->invoiceResultsPath . $filename . '.' . $this->fileExtensions);
			$this->debugLogManager->logData($this->invoiceResultsPath . $filename . '.' . $this->fileExtensions, $result);
		}
		
		$this->debugLogManager->endGroup();
	}
	
	/**
	 * checkIfNvCampaignsHasDifferentPrices
	 * 
	 * @param array $nvCampaignsDataArray
	 * @param \CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity
	 * @return void
	 */
	private function checkIfNvCampaignsHasDifferentPrices(array $nvCampaignsDataArray, \CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity) {
		$this->campaignHasDifferentPrices = false;
		
		foreach ($nvCampaignsDataArray as $nvCampaignEntity) {
			/* @var $nvCampaignEntity \CampaignEntity */
			if ($hvCampaignEntity->getPreis() <> $nvCampaignEntity->getPreis()) {
				$this->campaignHasDifferentPrices = true;
				
				/**
				 * initAdvancedSettings
				 */
				$this->initAdvancedSettings();
				break;
			}
		}
		
		if ((boolean) $this->settings['summarized'] === true 
			&& $this->campaignHasDifferentPrices
		) {
			throw new \LogicException('summarizedCampaign and campaignHasDifferentPrices can not be shared between!', 1434356873);
		}
		
		$this->debugLogManager->logData(__FUNCTION__, $this->campaignHasDifferentPrices);
	}
	
	/**
	 * resetSummarizedValues
	 * 
	 * @return void
	 */
	private function resetSummarizedValues() {
		$this->summarizedCampaignDateDataArray = $this->summarizedProductDtoDataArray = array();
		
		$this->initAdvancedSettings();
	}
	
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	 ****************************************************************************************** */
	public function setCashDiscount($cashDiscount) {
		$this->cashDiscount = \floatval($cashDiscount);
	}

   	public function setCampaignManager(\CampaignManager $campaignManager) {
		$this->campaignManager = $campaignManager;
	}
	
	public function setSwiftMailerWebservice(\SwiftMailerWebservice $swiftMailerWebservice) {
		$this->swiftMailerWebservice = $swiftMailerWebservice;
	}

	public function setDebugLogManager(\debugLogManager $debugLogManager) {
		$this->debugLogManager = $debugLogManager;
	}
}