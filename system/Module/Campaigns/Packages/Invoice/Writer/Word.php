<?php
namespace EMS\Campaigns\Invoice\Writer;

require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'WriterAbstract.php');

/**
 * PhpWord
 */
require_once(DIR_Vendor . 'PhpWord/Autoloader.php');
\PhpOffice\PhpWord\Autoloader::register();

class Word extends \EMS\Campaigns\Invoice\Writer\WriterAbstract {
	/**
	 * @var \PhpOffice\PhpWord\Template 
	 */
	protected $document;
	
	
	
	/**
	 * createInvoice
	 * 
	 * @param \DateTime $billingDate
	 * @param \CampaignEntity $campaignEntity
	 * @param \EMS\Campaigns\Invoice\Dto\ProductDto $productDto
	 * @param \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto
	 * @param string $fileExtensions
	 * @param array $summarizedProductDtoDataArray
	 * @return void
	 */
	public function createInvoice(\DateTime $billingDate, \CampaignEntity $campaignEntity, \EMS\Campaigns\Invoice\Dto\ProductDto $productDto, \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto, $fileExtensions, array $summarizedProductDtoDataArray = array()) {
		/**
		 * getBillingDirectoryByCampaignBillingType
		 */
		$billingTypeDir = $this->getBillingDirectoryByCampaignBillingType($campaignEntity);
		
		
		$phpWord = new \PhpOffice\PhpWord\PhpWord();

		// loadTemplate
		$this->document = $phpWord->loadTemplate($this->invoiceResourcePath . $billingTypeDir . $this->templateName . '.' . $fileExtensions);
		
		
		/*
		 * search and repleace templates values
		 */
		switch ($campaignEntity->getAbrechnungsart()) {
			case 'CPL':
			case 'CPO':
			case 'CPC':
				$this->document->setValue('order.number', $campaignEntity->getAbrechnungsart() . ' ' . $this->dateNow->format('Y'));
				
				/**
				 * processProductDtoDataForCpxBillingTypeIntoDocument
				 */
				$this->processProductDtoDataForCpxBillingTypeIntoDocument(
					$productDto,
					$summarizedProductDtoDataArray
				);
				break;
			
			default:
				$this->document->setValue('order.number', $this->dateNow->format('Y'));
				
				$documentVariables = $this->document->getVariables();
				if (\in_array('product.dataTitle', $documentVariables)) {
					/**
					* processProductDtoDataForDefaultBillingTypeIntoDocument
					*/
					$this->processProductDtoDataForDefaultBillingTypeIntoDocument($productDto);
				}
				break;
		}
		
		/**
		 * product
		 */
		$this->document->setValue('product.netPrice', \FormatUtils::sumFormat($productDto->getNetPrice()));
		$this->document->setValue('product.countryTaxPrice', \FormatUtils::sumFormat($productDto->getCountryTaxPrice()));
		$this->document->setValue('product.totalPrice', \FormatUtils::sumFormat($productDto->getTotalPrice()));
		$this->document->setValue('product.cashDiscountPrice', \FormatUtils::sumFormat($productDto->getCashDiscountPrice()));
		$this->document->setValue('product.cashDiscountTotalPrice', \FormatUtils::sumFormat($productDto->getCashDiscountTotalPrice()));

		
		// date (year, now, billing)
		$this->document->setValue('date.now', $this->dateNow->format('d.m.Y'));
		$this->document->setValue('date.billing', $billingDate->format('d.m.Y'));

		// user (name, email)
		$this->document->setValue('user.name', \RegistryUtils::get('userEntity')->getVorname() . ' ' . \RegistryUtils::get('userEntity')->getNachname());
		$this->document->setValue('user.email', \RegistryUtils::get('userEntity')->getEmail());

		/**
		 * processCustomerDataIntoDocument
		 */
		$this->processCustomerDataIntoDocument($campaignEntity);
		
		/**
		 * processCountryDtoDataIntoDocument
		 */
		$this->processCountryDtoDataIntoDocument($countryDto);

		$this->document->saveAs($this->invoiceResultsPath . $campaignEntity->getK_id() . '.' . $fileExtensions);
		unset($this->document);
	}
	
	
	/**
	 * processCustomerDataIntoDocument
	 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
	 * @return void
	 */
	private function processCustomerDataIntoDocument(\CampaignWidthCustomerAndContactPersonEntity $campaignEntity) {
		$this->document->setValue('customer.company', $campaignEntity->getCustomerEntity()->getFirma());
		$this->document->setValue('customer.address', $campaignEntity->getCustomerEntity()->getStrasse());
		$this->document->setValue('customer.zipAndCity', \trim($campaignEntity->getCustomerEntity()->getPlz()) . ' ' . \trim($campaignEntity->getCustomerEntity()->getOrt()));

		if ($campaignEntity->getCustomerEntity()->getCountry_id() !== 54) {
			// bei auslšndische Kunden
			$this->document->setValue('customer.country', \strtoupper($campaignEntity->getCustomerEntity()->getStaticCountry()->getCn_short_de()));
		} else {
			$this->document->setValue('customer.country', '');
		}
	}
	
	/**
	 * processCountryDtoDataIntoDocument
	 * 
	 * @param \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto
	 * @return void
	 */
	private function processCountryDtoDataIntoDocument(\EMS\Campaigns\Invoice\Dto\CountryDto $countryDto) {
		$this->document->setValue('country.priceInfo', $countryDto->getPriceInfo());
		$this->document->setValue('country.additionalInfo', $countryDto->getAdditionalInfo());
	}
	
	
	/**
	 * processProductDtoDataForDefaultBillingTypeIntoDocument
	 * 
	 * @param \EMS\Campaigns\Invoice\Dto\ProductDto $productDto
	 * @return void
	 */
	private function processProductDtoDataForDefaultBillingTypeIntoDocument(\EMS\Campaigns\Invoice\Dto\ProductDto $productDto) {
		$this->document->cloneRow('product.dataTitle', \count($productDto->getDataArray()));
		
		$productsColumn = 1;
		foreach ($productDto->getDataArray() as $item) {
			$this->document->setValue('product.dataTitle#' . $productsColumn, $item['title']);
			$this->document->setValue('product.dataContent#' . $productsColumn, $item['content']);

			$productsColumn++;
		}
	}
	
	/**
	 * processProductDtoDataForCpxBillingTypeIntoDocument
	 * 
	 * @param \EMS\Campaigns\Invoice\Dto\ProductDto $productDto
	 * @param array $summarizedProductDtoDataArray
	 * @return void
	 */
	private function processProductDtoDataForCpxBillingTypeIntoDocument(\EMS\Campaigns\Invoice\Dto\ProductDto $productDto, array $summarizedProductDtoDataArray) {
		if (\count($summarizedProductDtoDataArray) > 0) {
			$this->document->cloneRow('product.data.position', \count($summarizedProductDtoDataArray));
			
			$productsColumn = 1;
			foreach ($summarizedProductDtoDataArray as $itemProductDto) {
				/* @var $itemProductDto \EMS\Campaigns\Invoice\Dto\ProductDto */
				
				foreach ($itemProductDto->getDataArray() as $key => $item) {
					$this->document->setValue('product.data.' . $key . '#' . $productsColumn, $item);
				}
				
				$this->document->setValue('product.netPrice#' . $productsColumn, \FormatUtils::sumFormat($itemProductDto->getNetPrice()));
				
				$productsColumn++;
			}
		} else {
			foreach ($productDto->getDataArray() as $key => $item) {
				$this->document->setValue('product.data.' . $key, $item);
			}
		}
	}
}
