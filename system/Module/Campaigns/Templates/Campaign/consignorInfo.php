<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
?>
<table id="versenderInfo" width="100%">
    <tr>
        <td class="vLabel">Versandtag:</td>
        <td><?php echo $campaignEntity->getDatum()->format('d.m.Y'); ?></td>
    </tr>
    <tr>
        <td class="vLabel">Uhrzeit:</td>
        <td><?php echo $campaignEntity->getDatum()->format('H:i'); ?> Uhr</td>
    </tr>
    <tr>
        <td class="vLabel">Versandsystem:</td>
        <td><?php echo \CampaignAndOthersUtils::getDeliverySystemEntityByAspAbkz($campaignEntity->getVersandsystem())->getAsp(); ?></td>
    </tr>
	<tr class="bBottom">
        <td class="vLabel">Verteiler:</td>
        <td>
			<?php
			if ($campaignEntity->getDsd_id() > 0) {
				echo $_SESSION['deliverySystemDistributorEntityDataArray'][$campaignEntity->getDsd_id()]->getTitle();
			}
			?>
		</td>
    </tr>
    <tr>
        <td class="vLabel">Agentur</td>
        <td><?php echo $campaignEntity->getCustomerEntity()->getFirma(); ?></td>
    </tr>
    <tr class="bBottom">
        <td class="vLabel">Kampagne:</td>
        <td><?php echo $campaignEntity->getK_name(); ?></td>
    </tr>
    <tr>
        <td class="vLabel">Betreff:</td>
        <td><?php echo $campaignEntity->getBetreff(); ?></td>
    </tr>
    <tr class="bBottom">
        <td class="vLabel">Absendername:</td>
        <td><?php echo $campaignEntity->getAbsendername(); ?></td>
    </tr>
    <tr>
        <td class="vLabel">Versandmenge:</td>
        <td><?php echo \FormatUtils::numberFormat($campaignEntity->getVorgabe_m()); ?></td>
    </tr>
    <tr class="bBottom">
        <td class="vLabel">Selektion:</td>
        <td><?php echo $campaignEntity->getZielgruppe(); ?></td>
    </tr>
    <tr>
        <td class="vLabel">Hash:</td>
        <td><?php echo $campaignEntity->getHash(); ?></td>
    </tr>
    <tr class="bBottom">
        <td class="vLabel">Blacklist:</td>
        <td>
			<?php
			if (\strlen($campaignEntity->getBlacklist()) > 0) {
				echo '<span style="color:red"><b>vorhanden</b></span>';
			}
			?>
		</td>
    </tr>
    <tr>
        <td class="vLabel">Testmail:</td>
        <td>
			<?php
			echo $campaignEntity->getContactPersonEntity()->getAnrede() 
				. ' ' . $campaignEntity->getContactPersonEntity()->getVorname() 
				. ' ' . $campaignEntity->getContactPersonEntity()->getNachname() 
				. ' (' . $campaignEntity->getContactPersonEntity()->getEmail() . ')'
			;
			?>
		</td>
    </tr>
    <tr>
        <td class="vLabel">Notizen:</td>
        <td><?php echo \nl2br(\FormatUtils::cleanUpNotes($campaignEntity->getNotiz())); ?></td>
    </tr>
</table>