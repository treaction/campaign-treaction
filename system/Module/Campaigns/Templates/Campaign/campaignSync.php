<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
?>
<div id="campaignSyncContent">
	<table class="table clearTable" >
		<tr>
			<td class="fontStyle bold" style="width: 10em;">Status wechseln:</td>
			<td>
				<select id="deliverySystemStatus" name="deliverySystemStatus" onchange="changeSyncStatus(this.value);">
					<?php echo $syncTabStatusSelectItem; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="align center" style="height: 30px;">
				Welches Mailing soll mit 
				<span class="fontStyle bold" style="color:#9900FF;">"<?php echo $campaignEntity->getAgentur() . ', ' . $campaignEntity->getK_name(); ?>"</span> verkn&uuml;pft werden?
			</td>
		</tr>
	</table>
	
	<div id="campaignSync"></div>
</div>
<script language="javascript" type="text/javascript">
	function changeSyncStatus(status) {
		setRequest_default2(
			'<?php echo \BaseUtils::setScriptFileforRequestData('syncTab.php'); ?>'
				+ '?campaign[k_id]=<?php echo $campaignEntity->getK_id(); ?>' 
				+ '&deliverySystem[status]=' + status
			,
			'campaignSync'
		);
	}
	
	function hideCampaignSyncContainer() {
		YAHOO.example.container.container_sync.hide();
		
		pageReload(
			YAHOO.util.Dom.get('page').value,
			YAHOO.util.Dom.get('page_sub').value
		);
	}
	
	function error(){
		alert('Achtung bitte überprüfen Sie Ihre Zuordnung zur Kampagne im Versandsystem.\n Diese Kampagne <?php echo $syncCampagnArray[0]; ?> wurde bereits mit dem Kampagne <?php echo $syncCampagnArray[1]; ?> verknüpft.\n');
	}
	
	<?php
	if (\strlen($mailingId) > 0) {
		echo 'hideCampaignSyncContainer();';
	} else {
		echo 'changeSyncStatus(document.getElementById(\'deliverySystemStatus\').value)';
	}
	if($errorMailing == 'TRUE'){
		echo 'error();';
	}
	?>
</script>