<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $campaignRecipientUserEntity \UserEntity */
?>

<div class="align left">
	<span class="fontStyle bold" style="color:#333333;"></span><?php echo $campaignEntity->getK_name(); ?>
</div>

<div id="tab_date" class="yui-navset" style="padding-top:7px; text-align:left; border:0px;">

    <div class="yui-content" style="margin:0px;padding:0px;">
        <div id="tab1">
            <tr>
                    <td align="right" class="info" title="Datum &auml;ndern">Datum ausw&auml;hlen:</td>
                    <td align="left"> 
                        <input type="date" id="date" name="campaign[date]" value="<?php echo $campaignEntity->getDatum()->format('Y-m-d'); ?>" />
                    </td>
          </tr>
           
        </div>
		
    </div>
	
</div>
<input type="hidden" name="campaign[k_id]" value="<?php echo $campaignEntity->getK_id(); ?>" />

<script type="text/javascript">
	var tabView_status = new YAHOO.widget.TabView('tab_date');
</script>