<?php
$tableNoContentRowDataArray = \HtmlTableUtils::createTableRow(
	array(
		'style' => 'background: #fff;'
	)
);

$tableNoContent = 
	$tableNoContentRowDataArray['begin']
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'colspan' => \count($userTabFieldsDataArray) + 3,
				'height' => 30,
				'align' => 'center'
			), '<span style="color: #666666;">Keine Eintr&auml;ge gefunden</span>'
		)
	. $tableNoContentRowDataArray['end']
;
unset($tableNoContentRowDataArray);