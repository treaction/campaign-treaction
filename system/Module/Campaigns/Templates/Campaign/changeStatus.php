<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $campaignRecipientUserEntity \UserEntity */
?>

<div class="align left">
	<span class="fontStyle bold" style="color:#333333;"><?php echo $campaignEntity->getCustomerEntity()->getFirma(); ?></span>, <?php echo $campaignEntity->getK_name(); ?>
</div>

<div id="tab_status" class="yui-navset" style="padding-top:7px; text-align:left; border:0px;">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li <?php echo $campaignStatusSelected; ?>>
            <a href="#tab1"><em>Kampagne</em></a>
        </li>
		<?php echo $campaignLiItems; ?>
    </ul>
    <div class="yui-content" style="margin:0px;padding:0px;">
        <div id="tab1">
			<?php
				/**
				* createTableContentByStatusDataArray - campaignsStatusDataArray
				*/
				echo \createTableContentByStatusDataArray(
					$tableDataArray,
					$tableRowDataArray,
					$campaignsStatusDataArray,
					$campaignEntity->getStatus(),
					array(
						'radioBox' => 'changeInfomailContent(this.id);',
						'label' => 'changeInfomailContent(htmlFor);'
					)
				);
			?>
        </div>
		<?php
		if ($campaignEntity->getK_id() === $campaignEntity->getNv_id()) {
			/**
			 * hvCampaign
			 * 
			 * createTableContentByStatusDataArray - campaignsReportingDataArray
			 */
			echo '<div id="tab2">'
				. createTableContentByStatusDataArray(
					$tableDataArray,
					$tableRowDataArray,
					$campaignsReportingDataArray,
					$campaignEntity->getStatus()
				) . '</div>' . chr(13)
			;
			
			// createTableContentByStatusDataArray - campaignsOrderDataArray
			echo '<div id="tab3">'
				. createTableContentByStatusDataArray(
					$tableDataArray,
					$tableRowDataArray,
					$campaignsOrderDataArray,
					$campaignEntity->getStatus()
				) . '</div>' . chr(13)
			;
		}
		?>
    </div>
	
	<div class="spacerTopWrap defaultFormFields" id="infoMailRecipientContent">
		<input type="checkbox" name="changeStatus[sendEmail]" value="1" id="changeStatus_sendEmail" /> 
		<label for="changeStatus_sendEmail">Infomail an <span id="changeStatus_recipientName"></span> senden!</label>
		
		<input type="hidden" name="changeStatus[recipient][name]" id="changeStatus_recipientNameForEmail" value="" />
		<input type="hidden" name="changeStatus[recipient][email]" id="changeStatus_recipientEmail" value="" />
	</div>
</div>
<input type="hidden" name="campaign[k_id]" value="<?php echo $campaignEntity->getK_id(); ?>" />

<script type="text/javascript">
	var tabView_status = new YAHOO.widget.TabView('tab_status');
	var campaignOldStatus = parseInt(<?php echo $campaignEntity->getStatus(); ?>);
	
	hideAndUncheckField();
	
	function changeInfomailContent(elementId) {
		var newStatus = parseInt(YAHOO.util.Dom.get(elementId).value);
		
		switch (campaignOldStatus) {
			case 0:
				// neu
				if (newStatus === 2) {
					// von neu auf optimiert
					processEditorView();
				} else if (newStatus === 4) {
					// von neu auf freigabe
					processTcmView();
				} else {
					hideAndUncheckField();
				}
				break;
				
			case 1:
				// werbemittel
				if (newStatus === 2) {
					// von werbemittel auf optimiert
					processEditorView();
				} else {
					hideAndUncheckField();
				}
				break;
				
			case 2:
				// optimiert
				if (newStatus === 0) {
					// von optimiert wieder auf neu
					processEditorView();
				} else if (newStatus === 4) {
					// von optimiert auf freigabe
					processTcmView();
				} else {
					hideAndUncheckField();
				}
				break;
				
			case 4:
				// freigabe
				if (newStatus === 2) {
					// von freigabe wieder auf optimiert
					processTcmView();
				} else if (newStatus === 0) {
					// von freigabe wieder auf neu
					processTcmView();
				} else {
					hideAndUncheckField();
				}
				break;
		}
	}
	
	function processTcmView() {
		changeStatusFormFieldsValue(
			'TCM - Technik',
			'technik-tcm@treaction.de'
		);
		
		showAndCheckContent();
	}
	
	function processEditorView() {
		changeStatusFormFieldsValue(
			'<?php echo $campaignRecipientUserEntity->getVorname() . ' ' . $campaignRecipientUserEntity->getNachname(); ?>',
			'<?php echo $campaignRecipientUserEntity->getEmail(); ?>'
		);
		
		showAndCheckContent();
	}
	
	function showAndCheckContent() {
		YAHOO.util.Dom.get('changeStatus_sendEmail').checked = true;
		
		showViewById('infoMailRecipientContent');
	}
	
	function hideAndUncheckField() {
		YAHOO.util.Dom.get('changeStatus_sendEmail').checked = false;
		YAHOO.util.Dom.get('changeStatus_recipientName').value = '';
		YAHOO.util.Dom.get('changeStatus_recipientNameForEmail').value = '';
		YAHOO.util.Dom.get('changeStatus_recipientEmail').value = '';
		
		hideViewById('infoMailRecipientContent');
	}
	
	function changeStatusFormFieldsValue(recipientName, recipientEmail) {
		YAHOO.util.Dom.get('changeStatus_recipientName').innerHTML = '"' + recipientName + '"';
		YAHOO.util.Dom.get('changeStatus_recipientEmail').value = recipientEmail;
		YAHOO.util.Dom.get('changeStatus_recipientNameForEmail').value = recipientName;
	}	
</script>