<?php
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */
/* @var $hvCampaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $nvCampaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $tmpHvCampaigEntity \CampaignEntity */

$nvCampaignStatusColor = \CampaignAndCustomerUtils::getStatusFieldById(
	$nvCampaignEntity->getStatus(),
	'campaignStatusColor'
);

$onClick = 'doIt(event'
	. ', \'' . $nvCampaignEntity->getMailtyp() . '\''
	. ', \'' . $nvCampaignEntity->getMail_id() . '\''
	. ', \'' . \htmlspecialchars($nvCampaignEntity->getK_name()) . '\''
	. ' , ' . $nvCampaignEntity->getK_id()
	. ', \'NV\''
	. ', ' . $nvCampaignEntity->getStatus()
	. ', \'' . \htmlspecialchars($hvCampaignEntity->getCustomerEntity()->getFirma()) . '\''
	. ', \'' . \htmlspecialchars($nvCampaignEntity->getK_name()) . '\''
	. ' ,\'' . $nvCampaignEntity->getDatum()->format('Y-m-d') . '\''
	. ', \'' . $nvCampaignEntity->getDatum()->format('H:i') . '\''
	. ', \'\''
	. ', \'\''
	. ', \'\''
	. ', \'\''
	. ', \'' . $nvCampaignEntity->getVersandsystem() . '\''
	. ', \'\''
	. ', \'\''
	. ', \'\''
	. ', \'\''
	. ', \'' . $nvCampaignEntity->getOpenings() . '\''
	. ', \'' . $nvCampaignEntity->getOpenings_all() . '\''
	. ', \'' . $nvCampaignEntity->getKlicks() . '\''
	. ', \'' . $nvCampaignEntity->getKlicks_all() . '\''
	. ', \'' . $nvCampaignEntity->getVersendet() . '\''
	. ', \'main\''
	. ', ' . \intval($_SESSION['rechte'])
	. ', \'' . $mandant . '\''
	. '); HighLightTR(this);'
;
$tableNvContentRowDataArray = \HtmlTableUtils::createTableRow(
	array(
		'id' => 'snv' . $nvCampaignEntity->getK_id(),
		'class' => 'container',
		'onclick' => $onClick,
		'bgcolor' => $bgColor,
		'style' => 'color: #333333;',
		'onMouseOver' => 'this.bgColor=\'#A6CBFF\'',
		'onMouseOut' => 'this.bgColor=\'' . $bgColor . '\'',
		'data-hvCampaignId' => $nvCampaignEntity->getNv_id(),
		'data-currentCampaignId' => $nvCampaignEntity->getK_id(),
	)
);


$nvContentList = $tableNvContentRowDataArray['begin'];
if ($nvCampaignEntity->getStatus() === 5) {
	$nvContentList .= \HtmlTableUtils::createTableCellWidthContent(
		array(
			'style' => 'border-right: 1px solid #DBB7FF; padding: 0; text-align: center;'
		),
		'<img src="img/Tango/16/actions/dialog-apply.png" title="versendet" />'
	);
} else {
	$nvContentList .= \HtmlTableUtils::createTableCellWidthContent(
		array(
			'style' => 'border-right: 1px solid #DBB7FF; padding: 0;'
		),
		'<div style="line-height: 11px; margin-top: -1px; width: 79px;">'
			. '<div class="floatLeft">'
				. '<div class="floatLeft" style="border: 1px solid #999999; width:77px; height:2px; background-color: ' . $nvCampaignStatusColor . ';"></div>'
			. '</div>'
			. '<div class="floatLeft" style="font-size: 9px; width: 77px; text-align: center; height: 22px; color: #666666;">' 
				. \CampaignAndCustomerUtils::getStatusFieldById(
					$nvCampaignEntity->getStatus(),
					'campaignViewLabel'
				)
			. '</div>'
		. '</div>'
	);
}


$nvContentList .= \HtmlTableUtils::createTableCellWidthContent(
	array(
		'style' => 'border-right: 1px solid #DBB7FF;',
		'title' => \htmlspecialchars($nvCampaignEntity->getK_name())
	),
	'<div style="padding-left: 15px; padding-right: 5px;">'
		. '&bull; ' . \FormatUtils::cutString(
				$nvCampaignEntity->getK_name(),
				($_SESSION['campaignView']['textLenght'] - 6)
			)
		. \getSyncContentByDeliverySystem($nvCampaignEntity)
	. '</div>'
);


$nvContentList .= 
		\CampaignFieldsUtils::getCellContentByUserTabFieldsDataArray(
			$campaignManager,
			$nvCampaignEntity,
			$tmpHvCampaigEntity,
			$today,
			$userTabFieldsDataArray,
			false,
			'NV'
		) . \HtmlTableUtils::createTableCellWidthContent(
			array(),
			''
		)
	. $tableNvContentRowDataArray['end']
;
$nvKosten = \CampaignFieldsUtils::getKosten(
		$nvCampaignEntity
		);