<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
?>
<script type="text/javascript">
    YAHOO.util.Event.onDOMReady(function() {
        YAHOO.example.ClientPagination = function() {
            var myColumnDefs = [
                {key: 'id', label: '', width:5},
                {key: 'email', label: 'Email', sortable:true, width:180},
                {key: 'vn', label: 'Vorname', sortable:true, width:100},
                {key: 'nn', label: 'Nachname', sortable:true, width:100}
            ];
            
            var myDataSource = new YAHOO.util.DataSource(YAHOO.example.Data.testempfaenger);
            myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            myDataSource.responseSchema = {
                resultsList: 'records',
                fields: ['id', 'email', 'vn', 'nn']
            };
            
            var myDataTable = new YAHOO.widget.ScrollingDataTable(
                'testadressTable',
                myColumnDefs,
                myDataSource,
                {
                    height: '30em'
                }
            );
            
            return {
                oDS: myDataSource,
                oDT: myDataTable
            };
        }();
    });
</script>