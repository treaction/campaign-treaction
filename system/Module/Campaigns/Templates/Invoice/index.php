<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */

?>
<div style="background-color:#FFFFFF; padding: 10px;">
	<div id="invoiceFilter" style="border-bottom: 1px solid #ddd; padding-bottom: 10px;">
		<?php
			require('Filter/header.php');
		?>
	</div>
	<div id="invoiceData" style="margin-top:20px;">
		<form id="invoiceFormView" name="invoiceFormView" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
			<table class="scrollTable" id="scrollTable" cellpadding="0" cellspacing="0" border="0" bgcolor="#F5F5F5" width="100%">
				<?php
					echo $tableHeaderContent 
						. $tableData
					;
				?>
			</table>
                    <div style="margin-top: 20px; border-top: 1px solid #ddd; padding-top: 15px;">
				<div id="headerFooter" class="floatLeft" style="margin-right: 30px;">			
					<label for="Summe der Einnahmen" style="color: #2e3463; padding: 0 0 2px 15px; clear: none; font-size:12px;" class="widthAuto">Summe der Einnahmen:</label>
                                        <label for="Summe der Einnahmen" style="color: #7D960B; padding: 0 0 2px 15px; clear: none; font-size:12px; font-weight: bold;" class="widthAuto"><?php echo is_null($summeEinnahmen) ? '0 &euro;' : $summeEinnahmen.' &euro;';?></label>
					<br style="clear: both;" />
				</div>
                    </div>
			<div style="margin-top: 20px; border-top: 1px solid #ddd; padding-top: 15px;">
				<div id="headerFooter" class="floatLeft" style="margin-right: 30px;">
					<input type="checkbox" id="invoiceHeaderFooter" name="invoice[headerFooter]" value="1" class="floatLeft" checked="checked" />
					<label for="invoiceHeaderFooter" style="color: #000000; padding: 0 0 2px 15px; clear: none; font-size:12px;" class="widthAuto">Kopf/Fu&szlig; &uuml;bernehmen</label>
					<br style="clear: both;" />
				</div>
				
				<div id="headerFooter1" class="floatLeft spacerRightWrap">
					<input type="checkbox" id="invoiceSummarized" name="invoice[summarized]" value="1" class="floatLeft" />
					<label for="invoiceSummarized" style="color: #000000; padding: 0 0 2px 15px; clear: none; font-size:12px;" class="widthAuto">Rechnungen zusammenfassen</label>
					<br style="clear: both;" />
				</div>
				
				<div id="templateSelection" class="floatLeft spacerRightWrap">
					<span class="floatLeft" style="padding-right: 10px;">Templateauswahl:</span>

					<input type="radio" id="invoiceTemplateSelection_2" name="invoice[templateSelection]" value="2" class="floatLeft" />
					<label for="invoiceTemplateSelection_2" style="color: #000000; padding: 0 15px 2px 10px; clear: none; font-size:12px;" class="widthAuto">International</label>

					<input type="radio" id="invoiceTemplateSelection_1" name="invoice[templateSelection]" value="1" class="floatLeft" checked="checked" />
					<label for="invoiceTemplateSelection_1" style="color: #000000; padding: 0 0 2px 10px; clear: none; font-size:12px;" class="widthAuto">Standard (DE)</label>
					<br style="clear: both;" />
				</div>

				<input type="button" id="invoiceProcessView" onclick="processInvoice();" value="Rechnung(en) erstellen" style="margin-left: 5px; margin-top: -5px;" />
				<input type="button" id="invoiceCustomerCreditNoteView" onclick="processInvoiceCustomerCreditNote();" value="Gutschrift(en) erhalten" style="margin-left: 15px; margin-top: -5px;" />
				<br style="clear: both;" />
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	function createInvoiceDataElements() {
		var invoiceElements = YAHOO.util.Dom.getElementsByClassName('invoiceCampaigns', 'input');
		var invoiceDataElements = [];
		
		if (invoiceElements.length > 0) {
			for (var i = 0; i < invoiceElements.length; i++) {
				if (invoiceElements[i].checked) {
					invoiceDataElements.push(parseInt(invoiceElements[i].value));
				}
			}
		} else {
			if (invoiceElements.checked) {
				invoiceDataElements.push(parseInt(invoiceElements.value));
			}
		}
		
		return invoiceDataElements;
	}
	
	function createInvoiceFilterUrl() {
		return '<?php echo \BaseUtils::getScriptFileForRequestData(); ?>' 
			+ '?invoice[filter][year]=' + YAHOO.util.Dom.get('invoiceYear').value 
			+ '&invoice[filter][month]=' + YAHOO.util.Dom.get('invoiceMonth').value 
			+ '&invoice[filter][billingType]=' + YAHOO.util.Dom.get('invoiceBillingType').value 
			+ '&invoice[filter][campaignStatus]=' + YAHOO.util.Dom.get('invoiceCampaignStatus').value
		;
	}
	
    function invoiceFilterChangeView() {
        setRequest(
			createInvoiceFilterUrl(),
			'invoiceFilter'
		);
    }
	
	function processInvoice() {
		var invoiceElements = YAHOO.util.Dom.getElementsByClassName('invoiceCampaigns', 'input');
		var invoiceDataElements = createInvoiceDataElements(invoiceElements);
		
		if (invoiceDataElements.length === 0) {
			alert('<?php echo \utf8_encode('Sie haben keine Kampagnen ausgewählt!'); ?>');
		} else {
			processAjaxRequest(
                '<?php echo \BaseUtils::getAjaxRequestUrl(); ?>',
				'<?php echo \BaseUtils::setAjaxRequestUri('campaign', 'invoiceCampaigns'); ?>'
					+ '&invoice[headerFooter]=' + getCheckedValue(document.getElementsByName('invoice[headerFooter]')) 
					+ '&invoice[summarized]=' + getCheckedValue(document.getElementsByName('invoice[summarized]')) 
					+ '&invoice[templateSelection]=' + getCheckedValue(document.getElementsByName('invoice[templateSelection]'))
					+ '&invoice[campaignIds][]=' + invoiceDataElements.join('&invoice[campaignIds][]=')
					+ '&invoice[billingType]=' + YAHOO.util.Dom.get('invoiceBillingType').value
				,
                '',
                function (jsonResultData) {
					if (jsonResultData.success !== true) {
						alert(jsonResultData.message);
					}
					
					invoiceFilterChangeView();
                }
            );
		}
	}
	
	function processInvoiceCustomerCreditNote() {
		var invoiceElements = YAHOO.util.Dom.getElementsByClassName('invoiceCampaigns', 'input');
		var invoiceDataElements = createInvoiceDataElements(invoiceElements);
		
		if (invoiceDataElements.length === 0) {
			alert('<?php echo \utf8_encode('Sie haben keine Kampagnen ausgewählt!'); ?>');
		} else {
			processAjaxRequest(
                '<?php echo \BaseUtils::getAjaxRequestUrl(); ?>',
				'<?php echo \BaseUtils::setAjaxRequestUri('campaign', 'invoiceCreditNote'); ?>'
					+ '&invoice[campaignIds][]=' + invoiceDataElements.join('&invoice[campaignIds][]=')
				,
                '',
                function (jsonResultData) {
					if (jsonResultData.success !== true) {
						alert(jsonResultData.message);
					}
					
					invoiceFilterChangeView();
                }
            );
		}
	}
	
	
	// set parameter for setRequest()
	YAHOO.util.Dom.get('page').value = createInvoiceFilterUrl();
	YAHOO.util.Dom.get('page_sub').value = 'invoice';
</script>