<?php
/* @var $campaignManager \CampaignManager */

/* @var $hvCampaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $tmpHvCampaigEntity \CampaignEntity */
/* @var $statusDataArray \CampaignAndCustomerUtils */


/*$campaignActionLogEntity = $campaignManager->getCampaignLastLogItemByStatus(
	$hvCampaignEntity->getK_id(),
	32
);*/
$campaignActionLogEntity = '';
$onClick = 'doIt(event'
	. ', \'' . $hvCampaignEntity->getMailtyp() . '\''
	. ', \'' . $hvCampaignEntity->getMail_id() . '\''
	. ', \'' . \htmlspecialchars($hvCampaignEntity->getCustomerEntity()->getFirma() . ', ' . $hvCampaignEntity->getK_name()) . '\''
	. ' , ' . $hvCampaignEntity->getK_id()
	. ', \'' . $campaignType . '\''
	. ', ' . $hvCampaignEntity->getStatus()
	. ', \'' . \htmlspecialchars($hvCampaignEntity->getCustomerEntity()->getFirma()) . '\''
	. ', \'' . \htmlspecialchars($hvCampaignEntity->getK_name()) . '\''
	. ' ,\'' . $hvCampaignEntity->getDatum()->format('Y-m-d') . '\''
	. ', \'' . $hvCampaignEntity->getDatum()->format('H:i') . '\''
	. ', \'' . $hvCampaignEntity->getGebucht() . '\''
	. ', \'' . $hvCampaignEntity->getVorgabe_o() . '\''
	. ', \'' . $hvCampaignEntity->getVorgabe_k() . '\''
	. ', \'' . $hvCampaignEntity->getBlacklist() . '\''
	. ', \'' . $hvCampaignEntity->getVersandsystem() . '\''
	. ', \'' . \htmlspecialchars($hvCampaignEntity->getBearbeiter()) . '\''
	. ', \'' . $hvCampaignEntity->getTkp() . '\''
	. ', \'' . \htmlspecialchars($hvCampaignEntity->getContactPersonEntity()->getVorname() . ' ' . $hvCampaignEntity->getContactPersonEntity()->getNachname()) . '\''
	. ', \'\''
	. ', \'' . $hvCampaignEntity->getOpenings() . '\''
	. ', \'' . $hvCampaignEntity->getOpenings_all() . '\''
	. ', \'' . $hvCampaignEntity->getKlicks() . '\''
	. ', \'' . $hvCampaignEntity->getKlicks_all() . '\''
	. ', \'' . $hvCampaignEntity->getVersendet() . '\''
	. ', \'main\''
	. ', ' . \intval($_SESSION['rechte'])
	. ', \'' . $mandant . '\''
	. ');'
;
$tableContentRowDataArray = \HtmlTableUtils::createTableRow(
	array(
		'id' => 's' . $hvCampaignEntity->getK_id(),
		'class' => $hvCampaignEntity->getK_id() . ', container',
		'bgcolor' => $bgColorFest,
		'onMouseOver' => 'this.bgColor=\'#A6CBFF\'',
		'onMouseOut' => 'this.bgColor=\'' . $bgColor . '\''
	)
);

$v_zeichen = '';
if ($hvCampaignEntity->getVorgabe_k() != $cm_vorgabe_k 
	|| $hvCampaignEntity->getVorgabe_o() != $cm_vorgabe_o
) {
	$v_zeichen = '<a href="#" title="Ziel &Ouml;ffnung: ' . $hvCampaignEntity->getVorgabe_o() . '%, Ziel Klicks: ' . $hvCampaignEntity->getVorgabe_k() . '%" style="color: red;"><b>*</b></a>';
}


if ($hvCampaignEntity->getStatus() === 5 
	|| $hvCampaignEntity->getStatus() === 22 
	|| $hvCampaignEntity->getStatus() >= 30
) {
	$campaignStatusColor = '#666666';
} else {
	$campaignStatusColor = \CampaignAndCustomerUtils::getStatusFieldById(
		$hvCampaignEntity->getStatus(),
		'campaignStatusColor'
	);
}


$contentList = 
	$tableContentRowDataArray['begin'] 
		// checkbox
		. \HtmlTableUtils::createTableCellWidthContent(
			array(),
			\HtmlFormUtils::createCheckbox(
				array(
					'id' => 'invoiceCampaigns_' . $hvCampaignEntity->getK_id(),
					'class' => 'invoiceCampaigns',
					'name' => 'invoice[campaign][kId]',
					'value' => $hvCampaignEntity->getK_id()
				)
			)
		) 
		// Datum
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'style' => 'font-weight:bold;color:#565656;' . $dateStyle
			),
			\HtmlFormUtils::createLabelFieldItem(
				$dateHeader,
				array(
					'for' => 'invoiceCampaigns_' . $hvCampaignEntity->getK_id(),
					'style' => 'color: #000000; width: 100%;' . (\strlen($dateHeader) === 0 ? 'height: 20px;' : '')
				)
			)
		) 
		// Status
		. \HtmlTableUtils::createTableCellWidthContent(
			array(),
			'<span style="color: ' . $campaignStatusColor . ';">' 
				. \CampaignAndCustomerUtils::getStatusFieldById(
					$hvCampaignEntity->getStatus(),
					'campaignViewLabel'
				)
			. '</span>'
		) 
		// Kampagne width companyData
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'title' => \htmlspecialchars($hvCampaignEntity->getK_name() . ', ' . $hvCampaignEntity->getCustomerEntity()->getFirma()),
				'onclick' => $onClick
			),
			'<div style="font-weight: bold;">'
				. \FormatUtils::cutString(
					$hvCampaignEntity->getK_name(),
					\CampaignFieldsUtils::$textLenght
				) 
				. $v_zeichen
				. '<br /><span style="font-size: 10px; color: #9933FF;">'
					. \FormatUtils::cutString(
						$hvCampaignEntity->getCustomerEntity()->getFirma(),
						\CampaignFieldsUtils::$textLenght
					) 
				. '</span>'
			. '</div>'
		) 
		// Land
		. \HtmlTableUtils::createTableCellWidthContent(
			array(),
			\CampaignFieldsUtils::getStaticCountryResultByFieldname(
				$hvCampaignEntity,
				'short_de'
			)
		) 
		
		. \getTableContentCellByUserTabDataArray(
			$userTabFieldsDataArray,
			$hvCampaignEntity,
			$tmpHvCampaigEntity,
			$campaignType,
			$nvCampaignsCount,
			$campaignActionLogEntity,
			$campaignHasDifferentPrices
		)
		
		. \HtmlTableUtils::createTableCellWidthContent(
			array(),
			''
		)
		
	. $tableContentRowDataArray['end']
;