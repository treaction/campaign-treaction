<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */

?>
<select id="tunoverYear" name="tunover[filter][year]" style="font-size:14px;color:#565656;font-weight:bold;" class="spacerRightWrap">
	<?php
		echo \HtmlFormUtils::createOptionListItemDataWithoutKey(
			$_SESSION['campaign']['yearsDataArray'],
			$year
		);
	?>
</select>
<select id="tunoverMonth" name="tunover[filter][month]" style="font-size:14px;color:#565656;font-weight:bold;" class="spacerRightWrap">
	<option value="">- alle -</option>
	<?php
		echo \HtmlFormUtils::createOptionListItemData(
			$monthDataArray,
			$month
		);
	?>
</select>
<select id="tunoverEditors" name="tunover[filter][editor]" style="font-size:14px;color:#565656;font-weight:bold;" class="spacerRightWrap">
	<option value="">- alle Bearbeiter -</option>
	<?php
		echo \getTurnoverEditorsItems(
			$clientManager,
			$debugLogManager,
			\RegistryUtils::get('clientEntity')->getId(),
			$editor
		);
	?>
</select>
<select id="tunoverDeliverySystem" name="tunover[filter][deliverySystem]" style="font-size:14px;color:#565656;font-weight:bold;" class="spacerRightWrap" onchange="changeTurnoverDeliverySystemFilter(this.value, 'tunoverDeliverySystemDistributor');">
	<option value="">- Alle ASP -</option>
	<?php
		echo \CampaignAndOthersUtils::getDeliverySystemsItems($deliverySystem);
	?>
</select>

<div id="tunoverDeliverySystemDistributorCell">
	<div id="tunoverDeliverySystemDistributorContent"></div>
	<div id="tunoverDeliverySystemDistributor">
		<select id="tunoverDeliverySystemDistributorItems" name="tunover[filter][dsdId]" style="font-size:14px;color:#565656;font-weight:bold;">
			<option value="">- Alle -</option>
			<?php
				echo $deliverySystemDistributorItems;
			?>
		</select>
	</div>
</div>

<input type="button" id="changeView" onclick="changeView();" value="anzeigen" style="margin-left: 5px;" />

<input type="button" id="Export" onclick="Export();" value="Export" style="margin-left: 5px;" /><br class="clearBoth" />