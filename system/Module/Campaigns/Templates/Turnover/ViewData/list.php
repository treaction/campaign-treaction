<?php
/* @var $customerEntity \CustomerEntity */
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */


// debug
$debugLogManager->beginGroup('campaignsData');

foreach ($campaignsDataArray as $key => $hvCampaignEntity) {
	/* @var $hvCampaignEntity \CampaignEntity */
	
	// debug
	$debugLogManager->beginGroup($hvCampaignEntity->getK_id());
	
	$hvCampaignEntity->setAbrechnungsart(\CampaignAndOthersUtils::$campaignBillingsTypeDataArray[$hvCampaignEntity->getAbrechnungsart()]);
	// debug
	$debugLogManager->logData('campaignEntity', $hvCampaignEntity);
	
	
	/**
	 * getNVCampaignsDataItemsByKid
	 * 
	 * debug
	 */
	$nvCampaignsDataArray = $campaignManager->getNVCampaignsDataItemsByKid($hvCampaignEntity->getK_id());
	#$debugLogManager->logData('nvCampaignsDataArray', $nvCampaignsDataArray);
	
	$nvCampaignsCount = \count($nvCampaignsDataArray);
	$debugLogManager->logData('nvCampaignsCount', $nvCampaignsCount);
	if ($nvCampaignsCount > 0) {
		// debug
		$debugLogManager->beginGroup('nvCampaign');
		
		foreach ($nvCampaignsDataArray as $nvCampaignEntity) {
			/* @var $nvCampaignEntity \CampaignEntity */
			
			if (\strlen($nvCampaignEntity->getAbrechnungsart()) > 0) {
				$nvCampaignEntity->setAbrechnungsart(\CampaignAndOthersUtils::$campaignBillingsTypeDataArray[$nvCampaignEntity->getAbrechnungsart()]);
			}
			
			// debug
			$debugLogManager->logData('nvCampaignEntity', $nvCampaignEntity);
			
			/**
			 * calculateAndUpdateSalesByBillingTypeData
			 */
			\calculateAndUpdateSalesByBillingTypeData(
				$nvCampaignEntity,
				$salesByBillingType
			);
			
			$tkpTotalSend += $nvCampaignEntity->getVersendet();			
			$customerTotalSend += $nvCampaignEntity->getVersendet();
			
			/**
			 * getKosten
			 */
			$customerTotalCost += \CampaignFieldsUtils::getKosten(
									$nvCampaignEntity
					             );
		}
		
		// debug
		$debugLogManager->endGroup();
	} else {
		/**
		 * calculateAndUpdateSalesByBillingTypeData
		 */
		\calculateAndUpdateSalesByBillingTypeData(
			$hvCampaignEntity,
			$salesByBillingType
		);
		
		$tkpTotalSend += $hvCampaignEntity->getVersendet();
		$customerTotalSend += $hvCampaignEntity->getVersendet();
		
		/**
		 * getKosten
		 */
		$customerTotalCost += \CampaignFieldsUtils::getKosten(
								 $hvCampaignEntity
					           );		
	}
	unset($nvCampaignsDataArray);
	
	$customerTotalBooked += $hvCampaignEntity->getGebucht();
	
	// debug
	$debugLogManager->endGroup();
}
// debug
$debugLogManager->endGroup();

$debugLogManager->logData('salesByBillingType', $salesByBillingType);
$debugLogManager->logData('customerTotalBooked', $customerTotalBooked);
$debugLogManager->logData('customerTotalSend', $customerTotalSend);
$debugLogManager->logData('$customerTotalCost', $customerTotalCost);
