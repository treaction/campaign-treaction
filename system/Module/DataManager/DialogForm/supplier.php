<div id="container_dm_kunde">
    <div class="panel_label">Lieferantenverwaltung</div>
	<div class="hd">
        <span class="hd_label">Lieferant</span>
		<span class="hd_sublabel"></span>
    </div>
   	<div class="bd" style="text-align:left;">
		<form method="post" action="limport/kunde/save.php">
			<div id="dm_kunde_content">
				<div style="height:400px"></div>
			</div>
		</form>
	</div>
</div>

<div id="container_dm_del_kunde">
    <div class="panel_label">Lieferantenverwaltung</div>
	<div class="hd">
        <span class="hd_label">Lieferant l&ouml;schen</span>
		<span class="hd_sublabel"></span>
    </div>
   	<div class="bd" style="text-align:left;">
		<form method="post" action="limport/kunde/save.php">
			<input type="hidden" name="dm_del_kunde_id" id="dm_del_kunde_id" value="" />
			<input type="hidden" name="dm_action" id="dm_action" value="kunde" />
			Wollen Sie diesen Lieferanten wirklich l&ouml;schen?
		</form>
	</div>
</div>


<div id="container_dm_kunde_ap">
    <div class="panel_label">Lieferantenverwaltung</div>
	<div class="hd">
        <span class="hd_label">Kontakt</span>
		<span class="hd_sublabel"></span>
    </div>
   	<div class="bd" style="text-align:left;">
		<form method="post" action="limport/kunde/save.php">
			<div id="dm_kunde_ap_content">
				<div style="height:400px"></div>
			</div>
		</form>
	</div>
</div>

<div id="container_dm_del_kunde_ap">
    <div class="panel_label">Lieferantenverwaltung</div>
	<div class="hd">
        <span class="hd_label">Kontakt l&ouml;schen</span>
		<span class="hd_sublabel"></span>
    </div>
   	<div class="bd" style="text-align:left;">
		<form method="post" action="limport/kunde/save.php">
			<input type="hidden" name="dm_del_ap_id" id="dm_del_ap_id" value="" />
			<input type="hidden" name="dm_action" id="dm_action" value="kunde_ap" />
			Wollen Sie diesen Kontakt wirklich l&ouml;schen?
		</form>
	</div>
</div>