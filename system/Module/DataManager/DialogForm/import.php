<div id="container_a_rep_editieren">
    <div class="panel_label">Data Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Reporting bearbeiten</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd" style="text-align:left;">
        <form id="a_rep_form" name="a_rep_form" action="limport/report/rep_save.php" method="post">
            <div id="a_rep_edit"><div style="height:500px"></div></div>
        </form>
    </div>			
</div>

<div id="container_info_adr">
    <div class="panel_label">Data Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Adressen Details</span>
        <span class="hd_sublabel"></span>
    </div>
    <div id="info_div" class="bd">
        <div id="infocontent_adr"></div>
    </div>
    <div class="ft"></div>
</div>

<div id="container_import_edit">
    <div class="panel_label">Data Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Import editieren</span>
        <span class="hd_sublabel"></span>
    </div>

   <div id="info_div" class="bd">
        <div id="edit_import_content"></div>
    </div>
</div>

<div id="container_a_rep_vorlage">
    <div class="panel_label">Data Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Vorlage Reporting Email</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd">
        <div id="a_rep_vorlage"></div>
    </div>
</div>

<div id="del_datei">
    <div class="panel_label">Data Manager</div>
    <div class="hd">
        <span class="hd_label">Datei l&ouml;schen</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd">
        M&ouml;chten Sie diese Datei l&ouml;schen?
        <form id="del_datei" name="del_datei" action="limport/save_rep.php" method="post">
            <input type="hidden" id="impnr_del" name="impnr_del" value="" />
        </form>
    </div>
</div>

<div id="container_statistik">
    <div class="panel_label">Data Manager</div>
    <div class="hd">
        <span class="hd_label">Analyse</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd">
        <table class="adr" cellpadding="0" cellspacing="0" border="0" style='font-size:11px;'>
            <tr>
                <td align="left" class="info">
                    <form name="selA" id="selA">
                        <a onclick="document.selA.selAopt.options[0].selected = true; setRequest_default('limport/analyse/', 'dm_charts');" href="#">Hilfe</a>&nbsp;| 
                        <a onclick="document.selA.selAopt.options[0].selected = true; setRequest_st('all', 'Import');" href="#">Schnell&uuml;bersicht</a>&nbsp;
						|&nbsp;Diagramm:&nbsp;
                        <select onchange="setRequest_st(this.value, 'Import');" id="selAopt" name="selAopt">
                            <option id="analyse_selected">-- Analyse ausw&auml;hlen --</option>
                            <option value="quali">Reporting nach Bereinigungsart</option>
                            <option value="verwertbarkeit">Quote Brutto-Netto</option>
                            <option value="datensaetze">Vollst&auml;ndigkeit der Spalten</option>
                            <option value="laender">Verteilung nach Land</option>
                            <option value="sex">Verteilung nach Geschlecht</option>
                            <option value="alter">Verteilung nach Alter</option>
                            <option value="domain">Verteilung nach Emaildomain</option>
                            <option value="herkunft">Verteilung nach Herkunft</option>
                            <option value="anmeldung">Verteilung nach Tag der Anmeldung</option>
                            <option value="history">Verlauf der letzten Abgleichquoten</option>
                            <option value="ueberschneidung_ges">&Uuml;berschneidungen</option>
                        </select>
                    </form>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <div id="dm_charts" style="height:450px;width:890px;background-color:#efeff1;border:1px solid silver;padding:5px;overflow:auto;">
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>


<div id="container_adr_partner" style="display:none">
    <div class="panel_label">Data Manager</div>
    <div class="hd">
        <span class="hd_label">Partner</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd" style="text-align:left">
        <form id="partner_form" name="partner_form" action="limport/save_rep.php" method="POST">
            <div id="neu_p_tab" class="yui-navset">
                <ul class="yui-nav">
                    <li id="tab_p1" class="selected"><a href="#tab_p1"><em>Allgemein</em></a></li>
                    <li id="tab_p2" ><a href="#tab_p2"><em>Liefervereinbarungen / Reporting</em></a></li>
                </ul>
                <div class="yui-content"> 
                    <div id="tab_p1">
                        <table class="adr" cellpadding="0" cellspacing="0" border="0" style='font-size:11px;'>
                            <tr>
                                <td align="right" class="info">Partner ID (PID)*</td>
                                <td align="left"><input type="text" id="p_pid" name="p_pid"  /></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Partnerk&uuml;rzel (Pkz)*</td>
                                <td align="left"><input type="text" id="p_pkz" name="p_pkz" /></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Generierungsart</td>
                                <td align="left"><input type="radio" id="p_import" name="genart" checked="checked" value="Import" style="width:20px;" /> Import &nbsp;&nbsp;<input type="radio" id="p_realtime" name="genart" value="Realtime" style="width:20px;" /> Realtime</td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Webseite</td>
                                <td align="left"><input type="text" id="p_web" name="p_web" /></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Firma</td>
                                <td align="left"><input type="text" id="p_firma" name="p_firma" /></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Strasse</td>
                                <td align="left"><input type="text" id="p_str" name="p_str" /></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Plz</td>
                                <td align="left"><input type="text" id="p_plz" name="p_plz" /></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Ort</td>
                                <td align="left"><input type="text" id="p_ort" name="p_ort" /></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Gesch&auml;ftsf&uuml;hrer</td>
                                <td align="left"><input type="text" id="p_gf" name="p_gf" /></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Registergericht</td>
                                <td align="left"><input type="text" id="p_reg" name="p_reg" /></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Telefon</td>
                                <td align="left"><input type="text" id="p_tel" name="p_tel" /></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Fax</td>
                                <td align="left"><input type="text" id="p_fax" name="p_fax" /></td>
                            </tr>
                        </table>
                    </div>
                    <div id="tab_p2">
                        <table class="adr" cellpadding="0" cellspacing="0" border="0" style='font-size:11px;'>
                            <tr>
                                <td align="right" colspan="2" class="info" style="border-bottom:1px solid #999999;">Liefervereinbarungen</td>
                            </tr>
                            <tr>
                                <td valign="top" align="right" class="info">Datenalter</td>
                                <td valign="top" align="left"><input type="radio" id="neudaten" name="datenalter" checked="checked" style="width:20px;" value="neudaten" /> Neudaten (<= 1 Monat)<br /><input type="radio" id="altdaten" name="datenalter" style="width:20px;margin-top:7px;" value="altdaten" /> Altdaten (> 1 Monat)</td>
                            </tr>
                            <tr>
                                <td align="right" class="info" valign="top">Datenstruktur</td>
                                <td align="left">
                                    <div style="float:left;width:100px;">
                                        <input style="width:20px;" type="checkbox" name="datenstruktur[]" id="ds_ar" value="ds_ar" /> Anrede <br />
                                        <input style="width:20px; margin-top:3px;"  type="checkbox" name="datenstruktur[]" id="ds_vn" value="ds_vn" /> Vorname<br />
                                        <input style="width:20px; margin-top:3px;"  type="checkbox" name="datenstruktur[]" id="ds_nn" value="ds_nn" /> Nachname<br />
                                        <input style="width:20px; margin-top:3px;"  type="checkbox" name="datenstruktur[]" id="ds_str" value="ds_str" /> Strasse<br />
                                        <input style="width:20px; margin-top:3px;"  type="checkbox" name="datenstruktur[]" id="ds_plz" value="ds_plz" /> PLZ<br />
                                        <input style="width:20px; margin-top:3px;"  type="checkbox" name="datenstruktur[]" id="ds_ort" value="ds_ort" /> Ort
                                    </div>
                                    <div>
                                        <input style="width:20px;margin-top:3px;"  type="checkbox" name="datenstruktur[]" id="ds_land" value="ds_land" /> Land<br />
                                        <input style="width:20px;margin-top:3px;"  type="checkbox" name="datenstruktur[]" id="ds_geb" value="ds_geb" /> Geb.dat<br />
                                        <input style="width:20px;margin-top:3px;"  type="checkbox" name="datenstruktur[]" id="ds_tel" value="ds_tel" /> Telefon
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Permission</td>
                                <td align="left">
                                    <input style="width:20px;" type="checkbox" name="permission[]" id="p_soi" value="p_soi" /> SOI
                                    <input style="width:20px;margin-left:5px;" checked="checked" type="checkbox" name="permission[]" id="p_doi" value="p_doi" /> DOI
                                </td>
                            </tr>
                            <tr>
                                <td height="50" valign="bottom" align="right" colspan="2" class="info" style="border-bottom:1px solid #999999;">Reporting</td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Anrede</td>
                                <td align="left"><input type="radio" id="ap_herr" name="ap_anrede" checked="checked" style="width:20px;" value="Herr" /> Herr &nbsp;&nbsp;<input type="radio" id="ap_frau" name="ap_anrede" style="width:20px;" value="Frau" /> Frau</td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Ansprechpartner</td>
                                <td align="left"><input type="text" id="p_ap" name="p_ap" /></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Email</td>
                                <td align="left"><input type="text" id="p_email" name="p_email" /></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Standardabschlag (%)</td>
                                <td align="left"><input type="text" id="p_abschlag" name="p_abschlag" /><input type="hidden" id="p_action" name="p_action" value='' /></td>
                            </tr>
                        </table> 
                    </div>
                </div>
            </div>
    </div></form>
</div>


<div id="adr_status">
    <div class="panel_label">Data Manager</div>
    <div class="hd">
        <span class="hd_label">Status &auml;ndern</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd">
        <form name="adr_status_form" id="adr_status_form" action="limport/save_rep.php" method="post">
            <div id="a_status_new"><div style="height:150px"></div></div>
        </form>
    </div>
</div>