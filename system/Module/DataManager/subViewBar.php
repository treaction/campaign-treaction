<?php
/**
 * getAdrSelectOption
 * 
 * @param array $yearDataArray
 * @param integer $yearNow
 * @return string
 */
function getAdrSelectOption(array $yearDataArray, $yearNow) {
	$result = '';
	foreach ($yearDataArray as $yearValue) {
		$selected = '';
		if ((int) $yearNow === (int) $yearValue 
			|| (
				isset($_SESSION['import_jahr']) 
				&& (int) $_SESSION['import_jahr'] === (int) $yearValue
			)
		) {
			$selected = 'selected';
		}
    
		$result .= \HtmlFormUtils::createOptionItem(
			array(
				'value' => $yearValue,
				'selected' => $selected
			),
			$yearValue
		);
	}
	
	return $result;
}


\rsort($yearDataArray);


// Import
require_once('SubViewBar/import.php');

// Lieferant
require_once('SubViewBar/supplier.php');