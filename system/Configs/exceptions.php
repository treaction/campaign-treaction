<?php
// TODO: --email-- and or logging implementation

// send debug email
\DebugAndExceptionUtils::sendDebugData(
	array(
		'client' => \RegistryUtils::get('clientEntity')->getAbkz() . ' (' . \RegistryUtils::get('clientEntity')->getId() . ')',
		'userFullname' => \RegistryUtils::get('userEntity')->getVorname() . ' ' . \RegistryUtils::get('userEntity')->getNachname() 
			. ' (' . \RegistryUtils::get('userEntity')->getBenutzer_id() . ')'
		,
		'ip' => \BaseUtils::getUserIp(),
		'exception' => $e,
	),
	$e->getMessage()
);

if (\RegistryUtils::get('userEntity')->getBenutzer_id() === \BaseUtils::getDebugUserId()
	&& \BaseUtils::getUserIp() == \BaseUtils::getDevIpAddress()
) {
	$resultContent = '';
	if (isset($jsonData)) {
		$resultContent = \json_encode(
			array(
				'content' => $jsonData['message'],
				'jsonData' => $jsonData
			)
		);
	} else {
		\DebugAndExceptionUtils::showDebugData($e);
	}
	
	die($resultContent);
} else {
    $exceptionMessage = $e->getMessage();
}