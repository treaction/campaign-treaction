<?php
/**
 * Path for PDF fonts.
 * By default it is automatically set but you can also set it as a fixed string to improve performances.
 */
define('K_PATH_FONTS', DIR_configs . 'Tcpdf/Dais/Fonts/');

/**
 * Page format.
 */
define('PDF_PAGE_FORMAT', 'A4');

/**
 * Page orientation (P=portrait, L=landscape).
 */
define('PDF_PAGE_ORIENTATION', 'P');

/**
 * Document unit of measure [pt=point, mm=millimeter, cm=centimeter, in=inch].
 */
define('PDF_UNIT', 'mm');

/**
 * Header margin.
 */
define('PDF_MARGIN_HEADER', 0);

/**
 * Footer margin.
 */
define('PDF_MARGIN_FOOTER', 0);

/**
 * Top margin.
 */
define('PDF_MARGIN_TOP', 37.9);

/**
 * Bottom margin.
 */
define('PDF_MARGIN_BOTTOM', 37.9);

/**
 * Left margin.
 */
define('PDF_MARGIN_LEFT', 15.4);

/**
 * Right margin.
 */
define('PDF_MARGIN_RIGHT', 14.6);

/**
 * Default main font name.
 */
define('PDF_FONT_NAME_MAIN', 'helvetica'); // arial

/**
 * Default main font size.
 */
define('PDF_FONT_SIZE_MAIN', 9.6);

/**
 * Default data font name.
 */
define('PDF_FONT_NAME_DATA', 'helvetica'); // arial

/**
 * Default data font size.
 */
define('PDF_FONT_SIZE_DATA', 9.6);

/**
 * Ratio used to adjust the conversion of pixels to user units.
 */
define('PDF_IMAGE_SCALE_RATIO', 1);

/**
 * Magnification factor for titles.
 */
define('HEAD_MAGNIFICATION', 1.1);

/**
 * Height of cell respect font height.
 */
define('K_CELL_HEIGHT_RATIO', 1.4);

/**
 * Title magnification respect main font size.
 */
define('K_TITLE_MAGNIFICATION', 1);

/**
 * Reduction factor for small font.
 */
define('K_SMALL_RATIO', 2/3);

/**
 * Set to true to enable the special procedure used to avoid the overlappind of symbols on Thai language.
 */
define('K_THAI_TOPCHARS', false);

/**
 * If true allows to call TCPDF methods using HTML syntax
 * IMPORTANT: For security reason, disable this feature if you are printing user HTML content.
 */
define('K_TCPDF_CALLS_IN_HTML', false);

/**
 * If true and PHP version is greater than 5, then the Error() method throw new exception instead of terminating the execution.
 */
define('K_TCPDF_THROW_EXCEPTION_ERROR', true);

/**
 * Default timezone for datetime functions
 */
define('K_TIMEZONE', 'Europe/Berlin');