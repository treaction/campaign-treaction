<?php
\date_default_timezone_set('Europe/Berlin');
\setlocale(LC_TIME, 'de_DE', 'de_DE.utf-8');

$rootPath = \str_replace('Configs', '', __DIR__);

\define('DIR_configs', $rootPath . 'Configs' . \DIRECTORY_SEPARATOR);
\define('DIR_configsInit', DIR_configs . 'Init' . \DIRECTORY_SEPARATOR);
\define('DIR_deliverySystems', DIR_configs . 'DeliverySystems' . \DIRECTORY_SEPARATOR);
\define('DIR_userPackages', DIR_configs . 'UserPackages' . \DIRECTORY_SEPARATOR);
\define('DIR_functions', DIR_configs . 'Functions' . \DIRECTORY_SEPARATOR);
\define('DIR_ModuleClientSettings', DIR_configs . 'ModuleClientSettings' . \DIRECTORY_SEPARATOR);

\define('DIR_Module', $rootPath . \DIRECTORY_SEPARATOR . 'Module' . \DIRECTORY_SEPARATOR);

\define('DIR_Module_Campaigns', DIR_Module . 'Campaigns' . \DIRECTORY_SEPARATOR);
\define('DIR_Module_Campaigns_Actions', DIR_Module_Campaigns . 'Actions' . \DIRECTORY_SEPARATOR);

\define('DIR_Module_DataManager', DIR_Module . 'DataManager' . \DIRECTORY_SEPARATOR);
\define('DIR_Module_BlacklistUnsubscribe', DIR_Module . 'BlacklistUnsubscribe' . \DIRECTORY_SEPARATOR);
\define('DIR_Module_Admin', DIR_Module . 'Admin' . \DIRECTORY_SEPARATOR);

\define('DIR_library', $rootPath . \DIRECTORY_SEPARATOR . 'library' . \DIRECTORY_SEPARATOR);
\define('DIR_Entity', DIR_library . 'Entity' . \DIRECTORY_SEPARATOR);
\define('DIR_Factory', DIR_library . 'Factory' . \DIRECTORY_SEPARATOR);
\define('DIR_Manager', DIR_library . 'Manager' . \DIRECTORY_SEPARATOR);
\define('DIR_Utils', DIR_library . 'Utils' . \DIRECTORY_SEPARATOR);
\define('DIR_Webservice', DIR_library . 'Webservice' . \DIRECTORY_SEPARATOR);
\define('DIR_deliverySystemWebservice', DIR_Webservice . 'DeliverySystemWebservice' . \DIRECTORY_SEPARATOR);

\define('DIR_Packages', $rootPath . \DIRECTORY_SEPARATOR . 'Packages' . \DIRECTORY_SEPARATOR);
\define('DIR_Vendor', $rootPath . \DIRECTORY_SEPARATOR . 'Vendor' . \DIRECTORY_SEPARATOR);

\define('DIR_Pear', DIR_library . 'contrib' . \DIRECTORY_SEPARATOR . 'pear' . \DIRECTORY_SEPARATOR);


/**
 * @deprecated -> use DIR_Module
 */
\define('DIR_Action', $rootPath . \DIRECTORY_SEPARATOR . 'kampagne' . \DIRECTORY_SEPARATOR . 'Actions' . \DIRECTORY_SEPARATOR);

// move into Packages
\define('DIR_packages', DIR_library . 'packages' . \DIRECTORY_SEPARATOR);