<?php

/**
 * addNewLogEntry
 * 
 * @param \CampaignManager $campaignManager
 * @param \CampaignEntity $campaignEntity
 * @param integer $actionId
 * @param integer $campaignStatus
 * @param boolean $addLogData
 * @return integer
 */
function addNewLogEntry(\CampaignManager $campaignManager, \CampaignEntity $campaignEntity, $actionId, $campaignStatus = 0, $addLogData = TRUE) {
    return $campaignManager->addLogItem(
		array(
			'userid' => array(
				'value' => \RegistryUtils::get('userEntity')->getBenutzer_id(),
				'dataType' => \PDO::PARAM_INT
			),
			'action' => array(
				'value' => $actionId,
				'dataType' => \PDO::PARAM_INT
			),
			'kid' => array(
				'value' => $campaignEntity->getK_id(),
				'dataType' => \PDO::PARAM_INT
			),
			'timestamp' => array(
				'value' => \date('Y-m-d H:i:s'),
				'dataType' => \PDO::PARAM_STR
			),
			'status' => array(
				'value' => ((int) $campaignStatus > 0 ? (int) $campaignStatus : $campaignEntity->getStatus()),
				'dataType' => \PDO::PARAM_INT
			),
			'log_data' => array(
				'value' => ((boolean) $addLogData === true ? \serialize($campaignEntity) : null),
				'dataType' => \PDO::PARAM_STR
			),
		)
	);
}

/**
 * addNewLogEntryWidthLogdata
 * 
 * @param \CampaignManager $campaignManager
 * @param \CampaignEntity $campaignEntity
 * @param integer $actionId
 * @param array $logData
 * @return integer
 */
function addNewLogEntryWidthLogdata(\CampaignManager $campaignManager, \CampaignEntity $campaignEntity, $actionId, array $logData) {
    return $campaignManager->addLogItem(
		array(
			'userid' => array(
				'value' => \RegistryUtils::get('userEntity')->getBenutzer_id(),
				'dataType' => \PDO::PARAM_INT
			),
			'action' => array(
				'value' => $actionId,
				'dataType' => \PDO::PARAM_INT
			),
			'kid' => array(
				'value' => $campaignEntity->getK_id(),
				'dataType' => \PDO::PARAM_INT
			),
			'timestamp' => array(
				'value' => \date('Y-m-d H:i:s'),
				'dataType' => \PDO::PARAM_STR
			),
			'status' => array(
				'value' => $campaignEntity->getStatus(),
				'dataType' => \PDO::PARAM_INT
			),
			'log_data' => array(
				'value' => \serialize($logData),
				'dataType' => \PDO::PARAM_STR
			),
		)
	);
}


/**
 * createAndSendEmailToRecipientsDataArray
 * 
 * @param array $recipientsDataArray
 * @param string $subject
 * @param string $body
 * @param \SwiftMailerWebservice $swiftMailerWebservice
 * @param \CampaignEntity $campaignEntity
 * @param \CampaignManager $campaignManager
 * @param \debugLogManager $debugLogManager
 * @return string
 */
function createAndSendEmailToRecipientsDataArray(array $recipientsDataArray, $subject, $body, \SwiftMailerWebservice $swiftMailerWebservice, \CampaignEntity $campaignEntity, \CampaignManager $campaignManager, \debugLogManager $debugLogManager) {
    // debug
	$debugLogManager->beginGroup(__FUNCTION__);
	$debugLogManager->logData('recipientsDataArray', $recipientsDataArray);
	$debugLogManager->logData('subject', $subject);
	$debugLogManager->logData('body', $body);
	$debugLogManager->logData('campaignEntity', $campaignEntity);

	/**
	 * createMessage
	 */
	$swiftMessage = $swiftMailerWebservice->createMessage();
	/* @var $swiftMessage \Swift_Message */

	$swiftMessage->setBody($body)
		->setSubject($subject)
		->setTo($recipientsDataArray)
	;

	/**
	 * sendToMultipleAddress
	 * 
	 * debug
	 */
	$mailerSendResults = $swiftMailerWebservice->sendMessage($swiftMessage);
	$debugLogManager->logData('mailerSendResults', $mailerSendResults);

	/**
	 * getFailedRecipients
	 * 
	 * debug
	 */
	$failedRecipients = $swiftMailerWebservice->getFailedRecipients();
	$debugLogManager->logData('failedRecipients', $failedRecipients);
	$swiftMailerWebservice->setFailedRecipients(array());


	/**
	 * addNewLogEntry
	 * 
	 * debug
	 */
	$idAddLogEntry = addNewLogEntry(
		$campaignManager,
		$campaignEntity,
		8,
		0,
		false
	);
	$debugLogManager->logData('idAddLogEntry', $idAddLogEntry);

	// debug
	$debugLogManager->endGroup();

	$result = '';
	if (\count($failedRecipients) > 0) {
		$result = 'Die Email konnte an folgende Addressen nicht gesendet werden:' 
			. \implode(\chr(13), $failedRecipients)
		;
	} else {
		$result = 'Die Email wurde versandt.';
	}
	
	return $result;
}

/**
 * getCampaignRecipientUserEntity
 * 
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @return \UserEntity
 */
function getCampaignRecipientUserEntity(\CampaignWidthCustomerAndContactPersonEntity $campaignEntity, \ClientManager $clientManager, \debugLogManager $debugLogManager) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
	$campaignRecipientId = ($campaignEntity->getContactPersonEntity()->getVertriebler_id()) 
		? $campaignEntity->getContactPersonEntity()->getVertriebler_id() 
		: $campaignEntity->getVertriebler_id()
	;
	$debugLogManager->logData('campaignRecipientId', $campaignRecipientId);
	
	/**
	 * getUserDataItemById
	 */
	$campaignRecipientUserEntity = $clientManager->getUserDataItemById($campaignRecipientId);
	if (!($campaignRecipientUserEntity instanceof \UserEntity)) {
		// eingeloggter user
		$campaignRecipientUserEntity = new \UserEntity();
		$campaignRecipientUserEntity->setEmail(\RegistryUtils::get('userEntity')->getEmail());
		$campaignRecipientUserEntity->setNachname(\RegistryUtils::get('userEntity')->getNachname());
		$campaignRecipientUserEntity->setVorname(\RegistryUtils::get('userEntity')->getVorname());
		
		\MailUtils::sendEmailToUser(
			$_SESSION['u_email'],
			'Mandant: ' . \ClientUtils::getClientNameByClientId(\RegistryUtils::get('clientEntity')->getId()) . \chr(11) 
				. 'Kampagne: ' . $campaignEntity->getK_name() . \chr(11)
				. 'Kunde: ' . $campaignEntity->getCustomerEntity()->getFirma() . \chr(11)
				. 'Ansprechpartner: ' . $campaignEntity->getContactPersonEntity()->getVorname() . ' ' . $campaignEntity->getContactPersonEntity()->getNachname() . \chr(11) . \chr(11)
				. 'Nachricht: ' . 'hat kein g�ltiger Vertriebler mehr!'
		);
	}
	$debugLogManager->logData('campaignRecipientUserEntity', $campaignRecipientUserEntity);
	
	// debug
	$debugLogManager->endGroup();
	
	return $campaignRecipientUserEntity;
}