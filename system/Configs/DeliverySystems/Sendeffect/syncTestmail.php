<?php

/* @var $deliverySystemWebservice \SendeffectWebservice */
/* @var $debugLogManager \debugLogManager */

try {

    $subscribersArray = $deliverySystemWebservice->GetListSubscribers(
            $recipientsID
            );
  
    foreach ($subscribersArray as $recipient) {
            
	        $testListDataArray[$recipient->Email] = $recipient->Email;
                $deliverySystemWebservice->ActivateSubscriber(
                        $recipient->Email, 
                        $recipientsID, 
                        $url
                        );     
	}

        // debug
	$debugLogManager->logData('testListDataArray', $testListDataArray);
        
} catch (Exception $ex) {
    
}