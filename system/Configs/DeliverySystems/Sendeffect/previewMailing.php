<?php
/* @var $deliverySystemWebservice \SendeffectWebservice */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


try {
	/**
	 * getContent
	 * 
	 * debug
	 */
                         
     $result =  $deliverySystemWebservice->getHTMLContent(
                $campaignEntity->getMail_id()
                );

	$debugLogManager->logData('content', $result);
        
	$result = \str_replace("src=", " src=", $result);
	
	echo ($mailType == 'html' ? $result : \nl2br($result));
} catch (\Exception $e) {
	require_once('processException.php');
}