<?php
/**
 * getSendeffectMailingDataForCampaignDataArray
 * 
 * @param \SendeffectWebservice $deliverySystemWebservice
 * @param \CampaignEntity $campaignEntity
 * @param \debugLogManager $debugLogManager
 * @param array $campaignUpdateDataArray
 * @return void
 */
function getSendeffectMailingDataForCampaignDataArray(\SendeffectWebservice $deliverySystemWebservice, \CampaignEntity $campaignEntity, \debugLogManager $debugLogManager, array &$campaignUpdateDataArray) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getMailingData
	 * 
	 * debug
	 */
	$mailingData = $deliverySystemWebservice->getMailingData($campaignEntity->getMail_id());
	/* @var $mailingData \SimpleXMLElement */
	$debugLogManager->logData('mailingData', $mailingData);
         if($campaignEntity->getK_id() == 14621){
                if( \count($mailingData->item1) > 0){
           
	$campaignUpdateDataArray['versendet'] = array(
		'value' => (int) $mailingData->item1->sendsize,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['openings'] = array(
		'value' => (int) $mailingData->item1->emailopens_unique,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['openings_all'] = array(
		'value' => (int) $mailingData->item1->emailopens,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['klicks'] = array(
		'value' => (int) $mailingData->item1->linkclicks_unique,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['klicks_all'] = array(
		'value' => (int) $mailingData->item1->linkclicks,
		'dataType' => \PDO::PARAM_INT
	);
        $softbounce = (int) $mailingData->item1->bouncecount_sof + (int) $mailingData->item1->bouncecount_unknown;
        
	$campaignUpdateDataArray['sbounces'] = array(
		'value' => (int) $softbounce,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['hbounces'] = array(
		'value' => (int) $mailingData->item1->bouncecount_hard,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['abmelder'] = array(
		'value' => (int) $mailingData->item1->unsubscribecount,
		'dataType' => \PDO::PARAM_INT
	);        
	$campaignUpdateDataArray['gestartet']  = $campaignUpdateDataArray['datum'] = array(
		'value' => \date('Y-m-d H:i:s',(int) $mailingData->item1->starttime),
		'dataType' => \PDO::PARAM_STR
	);
	$campaignUpdateDataArray['beendet']  = array(
		'value' => \date('Y-m-d H:i:s',(int) $mailingData->item1->finishtime),
		'dataType' => \PDO::PARAM_STR
	); 
       $campaignUpdateDataArray['betreff'] = array(
		'value' => \htmlspecialchars( 
                        $mailingData->item1->newslettersubject, 
			\ENT_QUOTES
		),
		'dataType' => \PDO::PARAM_STR
	);
       $campaignUpdateDataArray['absendername'] = array(
		'value' => \htmlspecialchars( 
                        $mailingData->item1->sendfromname, 
			\ENT_QUOTES
		),
		'dataType' => \PDO::PARAM_STR
	);       
     
	if ($campaignEntity->getStatus() !== 5 
		&& $campaignEntity->getStatus() < 20 
		&& $campaignUpdateDataArray['versendet']['value'] > 100
	) {
		$campaignUpdateDataArray['status'] = array(
			'value' => 5,
			'dataType' => \PDO::PARAM_INT
		);
	}
	
        }
        }else{
	if( \count($mailingData->item0) > 0){
           
	$campaignUpdateDataArray['versendet'] = array(
		'value' => (int) $mailingData->item0->sendsize,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['openings'] = array(
		'value' => (int) $mailingData->item0->emailopens_unique,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['openings_all'] = array(
		'value' => (int) $mailingData->item0->emailopens,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['klicks'] = array(
		'value' => (int) $mailingData->item0->linkclicks_unique,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['klicks_all'] = array(
		'value' => (int) $mailingData->item0->linkclicks,
		'dataType' => \PDO::PARAM_INT
	);
        $softbounce = \intval($mailingData->item0->bouncecount_soft) + intval($mailingData->item0->bouncecount_unknown);
	$campaignUpdateDataArray['sbounces'] = array(
		'value' => (int) $softbounce,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['hbounces'] = array(
		'value' => (int) $mailingData->item0->bouncecount_hard,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['abmelder'] = array(
		'value' => (int) $mailingData->item0->unsubscribecount,
		'dataType' => \PDO::PARAM_INT
	);        
	$campaignUpdateDataArray['gestartet']  = $campaignUpdateDataArray['datum'] = array(
		'value' => \date('Y-m-d H:i:s',(int) $mailingData->item0->starttime),
		'dataType' => \PDO::PARAM_STR
	);
	$campaignUpdateDataArray['beendet']  = array(
		'value' => \date('Y-m-d H:i:s',(int) $mailingData->item0->finishtime),
		'dataType' => \PDO::PARAM_STR
	); 
       $campaignUpdateDataArray['betreff'] = array(
		'value' => \htmlspecialchars( 
                        $mailingData->item0->newslettersubject, 
			\ENT_QUOTES
		),
		'dataType' => \PDO::PARAM_STR
	);
       $campaignUpdateDataArray['absendername'] = array(
		'value' => \htmlspecialchars( 
                        $mailingData->item0->sendfromname, 
			\ENT_QUOTES
		),
		'dataType' => \PDO::PARAM_STR
	);       
     
	if ($campaignEntity->getStatus() !== 5 
		&& $campaignEntity->getStatus() < 20 
		&& $campaignUpdateDataArray['versendet']['value'] > 100
	) {
		$campaignUpdateDataArray['status'] = array(
			'value' => 5,
			'dataType' => \PDO::PARAM_INT
		);
	}
	
        }
}
	$debugLogManager->endGroup();
}