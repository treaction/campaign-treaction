<?php
/* @var $deliverySystemWebservice \SendeffectWebservice */
/* @var $campaignEntity \CampaignEntity */
/* @var $debugLogManager \debugLogManager */


/**
 * cleanRecipientsDataArray
 * 
 * @param array $recipientsDataArray
 * @return void
 */
function cleanRecipientsDataArray(array &$recipientsDataArray) {
	foreach ($recipientsDataArray as $key => $emailAddress) {
		if (\strlen($emailAddress) === 0) {
			unset($recipientsDataArray[$key]);
		}
	}
}

/**
 * correctRecipientsDataArray
 * 
 * @param array $recipientsDataArray
 * @return void
 */
function correctRecipientsDataArray(array &$recipientsDataArray) {
	foreach ($recipientsDataArray as $key => $emailAddress) {
		$recipientsDataArray[$key] = \correctEmailAddress($emailAddress);
	}
}

/**
 * correctEmailAddress
 * 
 * @param string $emailAddress
 * @return string
 */
function correctEmailAddress($emailAddress) {
	$emailAddressDataArray = \explode('@', $emailAddress);
	
	return \strtolower($emailAddressDataArray[0]) . '@' . $emailAddressDataArray[1];
}




$blacklistRecipients = array();
try {
		
	/**
	 * cleanRecipientsDataArray
	 * 
	 * debug
	 */
	\cleanRecipientsDataArray($recipientsDataArray);
	$debugLogManager->logData('recipientsDataArray', $recipientsDataArray);
	
	/**
	 * correctRecipientsDataArray
	 */
	\correctRecipientsDataArray($recipientsDataArray);
        
         switch((int)$recipientListId){
                    case 1180:
                        $sendermail = 'service@four-wave.de';
                        $replytomail = 'antwort@four-wave.de';
                        break;
                    case 9:
                        $sendermail = 'news@stellar-mail.de';
                        $replytomail = 'antwort@news.stellar-mail.de';
                        break;
                     case 49:
                        $sendermail = 'service@four-wave.de';
                        $replytomail = 'antwort@four-wave.de';
                        break;
                    
                    default:
                       $sendermail = 'news@stellar-mail.de';
                        $replytomail = 'antwort@news.stellar-mail.de';
                        
                }
        foreach ($recipientsDataArray as $recipientsData){
            $result = $deliverySystemWebservice->SendTestMail(
                              $campaignEntity->getMail_id(),
                              $recipientListId, 
                              $campaignEntity->getAbsendername(), 
                              $recipientsData,
                              $sendermail,
                              $replytomail
                         );

        }
	//TODO
	/**
	 * getWebserviceObjectByType
	 * 
	 * BlacklistWebservice
	 */
	/*/*$blacklistWebservice = $deliverySystemWebservice->getWebserviceObjectByType('BlacklistWebservice');
	
	foreach ($recipientsDataArray as $emailAddress) {
		// processEmailBlacklist
		\processEmailBlacklist(
			$emailAddress,
			$blacklistWebservice,
			$deliverySystemWebservice,
			$debugLogManager,
			$blacklistRecipients
		);
	}
	*/
	/**
	 * getMimeType
	 * 
	 * debug
	 */
	/*$mimeType = $mailingWebservice->getMimeType(
		$deliverySystemWebservice->getSession(),
        $campaignEntity->getMail_id()
	);
	$debugLogManager->logData('mimeType', $mimeType);
	*/
	

	/*
	if ($mimeType == 'multipart/alternative') {
		// processSendMultipartTestmail
		\processSendMultipartTestmail(
			$campaignEntity->getMail_id(),
			$recipientListId,
			$recipientsDataArray,
			$mailingWebservice,
			$deliverySystemWebservice,
			$debugLogManager
		);
	}
*/
	/**
	 * sendTestmailToRecipentsDataArray
	 * 
	 * debug
	 */
	/*$result = \sendTestmailToRecipentsDataArray(
		$campaignEntity->getMail_id(),
		$recipientListId,
		$recipientsDataArray,
		$mailingWebservice,
		$deliverySystemWebservice
	);*/
	$debugLogManager->logData('sendTestmailToRecipentsDataArray', $result);

	/**
	 * processBlacklistRecipients
	 * 
	 * debug
	 */
	/*\processBlacklistRecipients(
		$blacklistWebservice,
		$deliverySystemWebservice,
		$blacklistRecipients
	);
	$debugLogManager->logData('processBlacklistRecipients', $blacklistRecipients);*/
} catch (\Exception $e) {
	require_once('processException.php');

}