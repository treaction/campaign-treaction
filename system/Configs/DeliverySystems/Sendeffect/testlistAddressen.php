<?php
/* @var $deliverySystemWebservice \SendeffectWebservice */
/* @var $clientManager \ClientManager */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


try {
	
	/**
	 * getContactsByFilterId
	 * 
	 * debug
	 */
        //TODO distributor ID
	$listDataArray = $deliverySystemWebservice->GetListSubscribers(
                $recipientListId
                );
   
    $debugLogManager->logData('listDataArray', $listDataArray);
    
    foreach ($listDataArray as $recipient) {
          
	$resultDataArray[] = array(
                'id' => \HtmlFormUtils::createCheckbox(
                    array(
                        'onclick' => 'addRecipientToList(this.value)',
                        'name' => 'testempfaenger[]',
                        'value' => $recipient->Email
                    )
                ),
                'email' => (string)$recipient->Email
            );
                	
	}
       
} catch (\Exception $e) {
	require_once('processException.php');
}