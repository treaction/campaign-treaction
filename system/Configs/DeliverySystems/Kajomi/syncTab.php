<?php
/* @var $deliverySystemWebservice \KajomiWebservice */
/* @var $campaignEntity \CampaignEntity */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


/**
 * sortAscByQueuenum
 * 
 * @param \stdClass $a
 * @param \stdClass $b
 * @return int
 */
function sortAscByQueuenum(\stdClass $a, \stdClass $b) {
	$result = 0;

	if ($a->id < $b->id) {
		$result = -1;
	} elseif ($a->id > $b->id) {
		$result = 1;
	}

	return $result;
}

/**
 * sortDescByQueuenum
 * 
 * @param \stdClass $a
 * @param \stdClass $b
 * @return int
 */
function sortDescByQueuenum(\stdClass $a, \stdClass $b) {
	$result = 0;

	if ($a->id < $b->id) {
		$result = 1;
	} elseif ($a->id > $b->id) {
		$result = -1;
	}

	return $result;
}

/**
 * createDataList
 * 
 * @param integer $startPage
 * @param integer $nextPage
 * @param \CampaignEntity $campaignEntity
 * @param integer $deliverySystemStatus
 * @param \KajomiWebservice $deliverySystemWebservice
 * @param \debugLogManager $debugLogManager
 * @return string
 */
function createDataList($startPage, $nextPage, \CampaignEntity $campaignEntity, $deliverySystemStatus, \KajomiWebservice $deliverySystemWebservice, \debugLogManager $debugLogManager) {
	$result = '';
	
	// debug
	$debugLogManager->beginGroup(__FUNCTION__ . '_' . $startPage . '-' . $nextPage);
	for ($startPage; $startPage < $nextPage; $startPage++) {
		$bgColor = ($startPage % 2 ? '#E4E4E4' : '#FFFFFF');
		
		if (!isset($_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds'][$startPage])) {
			break;
		}
		
		$mailingData = $_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds'][$startPage];
		
		/**
		 * queuenum < 1 wird ignorieren,
		 * solche kampagnen wurden noch nicht versendet und haben somit noch keine eindeutige id (laut aussage von kajomi)
		 */
		if (\intval($mailingData->queuenum) <= 1) {
			continue;
		}
		
		// debug
		$debugLogManager->logData('mailingData', $mailingData);

		/**
		* createTableRow
		*/
		$tableRowDataArray = \HtmlTableUtils::createTableRow(
			array(
				'onclick' => 'submitData(' . $mailingData->queuenum . ', ' . $campaignEntity->getK_id() . ')',
				'onMouseOver' => 'this.style.backgroundColor=\'#A6CBFF\';return false;',
				'onMouseOut' => 'this.style.backgroundColor=\'' . $bgColor . '\';this.style.color=\'#000000\';return false;',
				'style' => 'cursor:pointer;background-color:' . $bgColor . ';border:1px solid #999999;'
			)
		);

		$result .=
			$tableRowDataArray['begin']
				. \HtmlTableUtils::createTableCellWidthContent(
					array(
						'colspan' => 2
					),
					$mailingData->queuenum . ': ' . (string) $deliverySystemWebservice->getMailingDataByQueuenumId($mailingData->queuenum)->m->description
				)
			. $tableRowDataArray['end']
		;
		
		// debug
		$debugLogManager->endGroup();
	}
	// debug
	$debugLogManager->endGroup();
	
	return $result;
}


// debug
$debugLogManager->beginGroup(__FILE__);
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);

try {
	$blaettern = '';
	
	if (isset($_GET['start_p']) 
		&& (int) $_GET['start_p'] > 0
	) {
		$startPage = (int) $_GET['start_p'];
		
		$prevPage = $startPage - $countMailings;
		$nextPage = $startPage + $countMailings;
		$pageFrom = ($startPage / $countMailings) + 1;

		$blaettern .= '<button class="button" onclick="setRequest_default2(\'' . \BaseUtils::setScriptFileforRequestData('syncTab.php') . '?' . \http_build_query($urlParamDataArray) . '&start_p=' . $prevPage . '\', \'campaignSync\');"><</button>';
		if (\count($_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds']) > $nextPage) {
			$blaettern .= ' <button class="button" onclick="setRequest_default2(\'' . \BaseUtils::setScriptFileforRequestData('syncTab.php') . '?' . \http_build_query($urlParamDataArray) . '&start_p=' . $nextPage . '\', \'campaignSync\');">></button>';
		}
	} else {
		/**
		 * getMailingsList
		 * 
		 * debug
		 */
		$_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds'] = $deliverySystemWebservice->getMailingsList(false);
		\usort($_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds'], 'sortDescByQueuenum');
		
		$nextPage = $countMailings;
		$startPage = $prevPage = 0;
		$pageFrom = 1;
		
		if (\count($_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds']) > $countMailings) {
			$blaettern = '<button class="button" onclick="setRequest_default2(\'' . \BaseUtils::setScriptFileforRequestData('syncTab.php') . '?' . \http_build_query($urlParamDataArray) . '&start_p=' . $countMailings . '\', \'campaignSync\');">></button>';
		}
	}
	
	$totalCountMailingsIds = \count($_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds']);
	$pageTotal = \round(
		($totalCountMailingsIds / $countMailings),
		0
	) + 1;
	
	// debug
	$debugLogManager->logData('countMailings', $countMailings);
	$debugLogManager->logData('startPage', $startPage);
	$debugLogManager->logData('nextPage', $nextPage);
	$debugLogManager->logData('prevPage', $prevPage);
	
	$debugLogManager->logData('pageFrom', $pageFrom);
	$debugLogManager->logData('totalCountMailingsIds', $totalCountMailingsIds);
	$debugLogManager->logData('pageTotal', $pageTotal);
	
	if ($totalCountMailingsIds > 0) {
		/**
		 * createDataList
		 */
		$contentResult = \createDataList(
			$startPage,
			$nextPage,
			$campaignEntity,
			$deliverySystemStatus,
			$deliverySystemWebservice,
			$debugLogManager
		);
	} else {
		$tableRowDataArray = \HtmlTableUtils::createTableRow();
		
		$contentResult = 
			$tableRowDataArray['begin'] 
				. \HtmlTableUtils::createTableCellWidthContent(
					array(
						'colspan' => 2,
						'align'=> 'center'
					),
					'Keine Kampagnen vorhanden!'
				)
			. $tableRowDataArray['end']
		;
	}
} catch (\Exception $e) {
	require_once('processException.php');
}

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());
$debugLogManager->endGroup();
?>
<table cellpadding="0" cellspacing="0" border="0" class="table">
	<thead>
		<tr>
			<th class="align center noBottomBorder">
				Seite <?php echo $pageFrom; ?> von <?php echo $pageTotal; ?> (Gesamtkampagnen: <?php echo $totalCountMailingsIds; ?>)
			</th>
			<th class="align right noBottomBorder"><?php echo $blaettern; ?></th>
		</tr>
		<tr>
			<th colspan="2" class="headerStyleColumn noTopBorder">Kampagnenname</th>
		</tr>
	</thead>

	<tfoot>
		<tr class="footerStyleColumn">
			<td colspan="2" class="align right"><?php echo $blaettern; ?></td>
		</tr>
	</tfoot>

	<tbody>
		<?php echo $contentResult; ?>
	</tbody>
</table>