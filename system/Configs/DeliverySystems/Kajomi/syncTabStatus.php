<?php
/* @var $debugLogManager \debugLogManager */
/* @var $clientManager \ClientManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $deliverySystemDistributorWidthClientDeliverySystemEntity \DeliverySystemDistributorWidthClientDeliverySystemEntity */


$optionsItems = null;
foreach (\KajomiWebservice::getMailingStatusDataArray() as $key => $value) {
	$optionsItems .= \HtmlFormUtils::createOptionItem(
		array(
			'value' => $key
		),
		$value
	);
}