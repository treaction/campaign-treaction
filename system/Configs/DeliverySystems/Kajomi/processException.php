<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


$debugLogManager->logData('exception', $e);

// Kajomi Error
$emailBody = 'Die Kampagne: ' . $campaignEntity->getK_id()
	. ' (MailingId: ' . $campaignEntity->getMail_id() . ','
		. ' MailingName: ' . $campaignEntity->getK_name() . ','
		. ' Agentur: ' . $campaignEntity->getCustomerEntity()->getFirma()
	. ')'
	. ' konnte wegen einen Technisches Problem im Kajomi nicht aktualisiert werden.' . \chr(11) . \chr(11)
;

// email an User
if (\strlen($_SESSION['u_email']) > 0) {
	/**
	 * sendEmailToUser
	 */
	\MailUtils::$EMAIL_BETREFF = 'Kajomi Error: ' . \basename(__FILE__);
	\MailUtils::sendEmailToUser(
		$_SESSION['u_email'], $emailBody
			. 'Der Ems-Support wurde schon informiert.'
	);
}

/**
 * sendMail (toAdmin)
 */
\MailUtils::$EMAIL_BETREFF = 'Admin - Kajomi Error: ' . \basename(__FILE__);
\MailUtils::sendMail(
	'client: ' . $_SESSION['mandant'] . ' (' . (int) $_SESSION['mID'] . ')' . \chr(11)
	. 'userFullname: ' . $_SESSION['u_vorname'] . ' ' . $_SESSION['u_nachname'] . ' (' . $_SESSION['benutzer_id'] . ')' . \chr(11) . \chr(11) 
	. $emailBody
	. 'KajomiError:' . \chr(11)
	. (string) $e->getMessage() . ': ' . $e->getCode()
);

$result = FALSE;