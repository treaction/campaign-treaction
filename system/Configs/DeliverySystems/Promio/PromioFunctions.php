<?php
/**
 * getPromioMailingDataForCampaignDataArray
 * 
 * @param \PromioWebservice $deliverySystemWebservice
 * @param \CampaignEntity $campaignEntity
 * @param \debugLogManager $debugLogManager
 * @param array $campaignUpdateDataArray
 * @return void
 */
function getPromioMailingDataForCampaignDataArray(\PromioWebservice $deliverySystemWebservice, \CampaignEntity $campaignEntity, \debugLogManager $debugLogManager, array &$campaignUpdateDataArray) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getMailingData
	 * 
	 * debug
	 */
	$mailingData = $deliverySystemWebservice->getMailingData($campaignEntity->getMail_id());
        
	$debugLogManager->logData('mailingData', $mailingData);
	if( \count($mailingData->recipients) > 0){
           
	$campaignUpdateDataArray['versendet'] = array(
		'value' => (int) $mailingData->recipients,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['openings'] = array(
		'value' => (int) $mailingData->opensUnique,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['openings_all'] = array(
		'value' => (int) $mailingData->opens,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['klicks'] = array(
		'value' => (int) $mailingData->clicksUnique,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['klicks_all'] = array(
		'value' => (int) $mailingData->clicks,
		'dataType' => \PDO::PARAM_INT
	);
       
	$campaignUpdateDataArray['sbounces'] = array(
		'value' => (int) $mailingData->softBounces,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['hbounces'] = array(
		'value' => (int) $mailingData->hardBounces,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['abmelder'] = array(
		'value' => (int) $mailingData->unsubscribes,
		'dataType' => \PDO::PARAM_INT
	);        
	
	/*$campaignUpdateDataArray['beendet']  = array(
		'value' => \date('Y-m-d H:i:s',(int) $mailingData->item0->finishtime),
		'dataType' => \PDO::PARAM_STR
	); 
       $campaignUpdateDataArray['betreff'] = array(
		'value' => \htmlspecialchars( 
                        $mailingData->item0->newslettersubject, 
			\ENT_QUOTES
		),
		'dataType' => \PDO::PARAM_STR
	);
       $campaignUpdateDataArray['absendername'] = array(
		'value' => \htmlspecialchars( 
                        $mailingData->item0->sendfromname, 
			\ENT_QUOTES
		),
		'dataType' => \PDO::PARAM_STR
	);  */     
     
	if ($campaignEntity->getStatus() !== 5 
		&& $campaignEntity->getStatus() < 20 
		&& $campaignUpdateDataArray['versendet']['value'] > 0
	) {
		$campaignUpdateDataArray['status'] = array(
			'value' => 5,
			'dataType' => \PDO::PARAM_INT
		);
                $campaignUpdateDataArray['gestartet']  = $campaignUpdateDataArray['datum'] = array(
		'value' => $mailingData->letterResultList->letterResult->sendDateTime,
		'dataType' => \PDO::PARAM_STR
	);
	}
	
        }

	$debugLogManager->endGroup();
}