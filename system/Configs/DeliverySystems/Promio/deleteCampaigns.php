<?php
/* @var $deliverySystemWebservice \PromioWebservice */
/* @var $campaignEntity \CampaignEntity */
/* @var $debugLogManager \debugLogManager */


try {
	/**
	 * getWebserviceObjectByType
	 */
	$result = $deliverySystemWebservice->deleteMailing($campaignEntity->getExternal_job_id());
	$resultData[$campaignEntity->getK_id()]['deleteFromDeliverySystem'] = $result;
	
	$resultData['mailingStatus'][$campaignEntity->getK_id()] = $campaignEntity->getStatus();
} catch (\Exception $e) {
	require_once('processException.php');
}