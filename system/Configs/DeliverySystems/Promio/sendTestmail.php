<?php
/* @var $deliverySystemWebservice \PromioWebservice */
/* @var $campaignEntity \CampaignEntity */
/* @var $debugLogManager \debugLogManager */


/**
 * cleanRecipientsDataArray
 * 
 * @param array $recipientsDataArray
 * @return void
 */
function cleanRecipientsDataArray(array &$recipientsDataArray) {
	foreach ($recipientsDataArray as $key => $emailAddress) {
		if (\strlen($emailAddress) === 0) {
			unset($recipientsDataArray[$key]);
		}
	}
}

/**
 * correctRecipientsDataArray
 * 
 * @param array $recipientsDataArray
 * @return void
 */
function correctRecipientsDataArray(array &$recipientsDataArray) {
	foreach ($recipientsDataArray as $key => $emailAddress) {
		$recipientsDataArray[$key] = \correctEmailAddress($emailAddress);
	}
}

/**
 * correctEmailAddress
 * 
 * @param string $emailAddress
 * @return string
 */
function correctEmailAddress($emailAddress) {
	$emailAddressDataArray = \explode('@', $emailAddress);
	
	return \strtolower($emailAddressDataArray[0]) . '@' . $emailAddressDataArray[1];
}

function setCurl($ch,$endpoint, $apiKey, $sendId){   
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
           'API-Key:'. $apiKey,
           'Sender-ID:'. $sendId,
            'Content-Type: application/json'
           ));
          
          curl_setopt($ch, CURLOPT_URL, $endpoint);
     }


$blacklistRecipients = array();
try {
		
	/**
	 * cleanRecipientsDataArray
	 * 
	 * debug
	 */
	#\cleanRecipientsDataArray($recipientsDataArray);
	#$debugLogManager->logData('recipientsDataArray', $recipientsDataArray);
	
	/**
	 * correctRecipientsDataArray
	 */
	#\correctRecipientsDataArray($recipientsDataArray);
       /**
	* getWebserviceObjectByType
	 * 
	 * MailingWebservice
	*/
        /*
        $mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');
        
        foreach ($recipientsDataArray as $recipientsData){
                      $result = $mailingWebservice->sendTestmailToSingle(
                                $campaignEntity->getMail_id(),
                                $recipientsData
                                );
        }
        */
         $ch = curl_init();
        
         $messageId = (int)$campaignEntity->getExternal_job_id();    
         setCurl( $ch,                
                           'https://api.promio-connect.com/3.0/api/message/'.$messageId.'/send-test',
                            $deliverySystemWebservice->getSecretKey(),                           
                           (int)$deliverySystemWebservice->getMLogin()
                    );
  
         $userIdentification  = new \stdClass();
         $userIdentification->recipients = new \stdClass();
         $userIdentification->recipients->mail   = array('sami.jarmoud@treaction.net');
         $jsonData = json_encode( $userIdentification);
         curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonData );
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         
         $result= curl_exec($ch);
 
	$debugLogManager->logData('sendTestmailToRecipentsDataArray', $result);

	/**
	 * processBlacklistRecipients
	 * 
	 * debug
	 */
	/*\processBlacklistRecipients(
		$blacklistWebservice,
		$deliverySystemWebservice,
		$blacklistRecipients
	);
	$debugLogManager->logData('processBlacklistRecipients', $blacklistRecipients);*/
} catch (\Exception $e) {
	require_once('processException.php');

}