<?php
/* @var $deliverySystemWebservice \PromioWebservice */
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $deliverySystemDistributorWidthClientDeliverySystemEntity \DeliverySystemDistributorWidthClientDeliverySystemEntity */


/**
 * getTestRecipientList
 * 
 * @param \DeliverySystemDistributorWidthClientDeliverySystemEntity $deliverySystemDistributorWidthClientDeliverySystemEntity
 * @param \PromioWebservice $deliverySystemWebservice
 * @param \debugLogManager $debugLogManager
 * @return void
 */

try {
	
    $customersDataArray = $campaignManager->getCustomersDataItemsByQueryParts(
			array(
				'ORDER_BY' => '`firma` ASC'
			),
			\PDO::FETCH_CLASS
		);
    if (\count($customersDataArray) > 0) {
			foreach ($customersDataArray as $customerEntity) {
				/* @var $customerEntity \CustomerEntity */
                            $email = $customerEntity->getEmail();
                            if(!empty($email)){
                 $testListDataArray = array(               
                         $customerEntity->getKunde_id() => $email
                 );
                            }

			}
		}
	foreach ($testListDataArray as $recipientListId => $recipientLisName) {
		$selected = null;
		if ((string) $recipientListId === (string) $deliverySystemDistributorWidthClientDeliverySystemEntity->getTestlist_distributor_id()) {
			$selected = 'selected';
		}
		
		$optionsItems .= \HtmlFormUtils::createOptionItem(
			array(
				'value' => '40',
				'selected' => $selected
			),
			'testListe'
		);
	}
} catch (\Exception $e) {
	require_once('processException.php');
}