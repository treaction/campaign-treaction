<?php
/* @var $deliverySystemWebservice \PromioWebservice */
/* @var $clientManager \ClientManager */
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


try {
	/**
	* getWebserviceObjectByType
	 * 
	 * RecipientWebservice
	*/
	#$recipientWebservice = $deliverySystemWebservice->getWebserviceObjectByType('ContactsService');
	
	/**
	 * getContactsByFilterId
	 * 
	 * debug
	 */
        //TODO distributor ID
/*	$response = $recipientWebservice->getContactsByFilterId(
                $recipientListId
                );
         if($response->isSuccess()){
             $listDataObject = $response->getResult();
        }
    $debugLogManager->logData('$listDataObject', $listDataObject);
    */
      $customersDataArray = $campaignManager->getContactPersonDataItemsByQueryParts(
			array(
				'ORDER_BY' => '`email` ASC'
			),
			\PDO::FETCH_CLASS
		);
    if (\count($customersDataArray) > 0) {
			foreach ($customersDataArray as $customerEntity) {
				/* @var $customerEntity \CustomerEntity */
                            
                            if(!empty($customerEntity->getEmail())){
                 $listDataArray[$customerEntity->getKunde_id()] = $customerEntity->getEmail();

                            }

			}
		}
 foreach ($listDataArray as  $recipient) {
          
	$resultDataArray[] = array(
                'id' => \HtmlFormUtils::createCheckbox(
                    array(
                        'onclick' => 'addRecipientToList(this.value)',
                        'name' => 'testempfaenger[]',
                        'value' => $recipient
                    )
                ),
                'email' => (string)$recipient
            );
                	
	}
       
} catch (\Exception $e) {
	require_once('processException.php');
}