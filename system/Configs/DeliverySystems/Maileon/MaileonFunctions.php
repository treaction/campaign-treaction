<?php
/**
 * getMaileonMailingReportingDataForCampaignDataArray
 * 
 * @param \MaileonWebservice $deliverySystemWebservice
 * @param \CampaignEntity $campaignEntity
 * @param \debugLogManager $debugLogManager
 * @param array $campaignUpdateDataArray
 * @return void
 */
function getMaileonMailingReportingDataForCampaignDataArray(\MaileonWebservice $deliverySystemWebservice, \CampaignEntity $campaignEntity, \debugLogManager $debugLogManager, array &$campaignUpdateDataArray) {
	$debugLogManager->beginGroup(__FUNCTION__);
    
        $mailingData = array(
            'versendet' => 'getRecipientsCount',
            'openings' => 'getUniqueOpensCount',
            'openings_all' => 'getOpensCount',
            'klicks' => 'getUniqueClicksCount',
            'klicks_all' => 'getClicksCount',
            'abmelder' => 'getUnsubscribersCount'
        );
	/**
	 * getWebserviceObjectByType
	 * 
	 * mailingWebservice
	 */
	$mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('ReportsWebservice');
        
          foreach ($mailingData as $key => $value){
              $response = $mailingWebservice->{$value}(
                                     null,
                                     null, 
                                    array(
                                        $campaignEntity->getMail_id()
                      )
                             );
              ${$key} = $response->getResult();
          }
         //Hard Bounce
         $hardBounce = $mailingWebservice->getBouncesCount(
                 null, 
                 null, 
                 array($campaignEntity->getMail_id()), 
                 null, 
                 null, 
                 null, 
                 null,
                'permanent',
                 null,
                 null
                 );
         
         $hbounces = $hardBounce->getResult();             
         
         //Soft Bounce
         $softBounce = $mailingWebservice->getBouncesCount(
                 null, 
                 null, 
                 array($campaignEntity->getMail_id()), 
                 null, 
                 null, 
                 null, 
                 null,
                'transient',
                 null,
                 null
                 );
         
         $sbounces = $softBounce->getResult();           
                      
	$campaignUpdateDataArray['versendet'] = array(
		'value' => $versendet,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['openings'] = array(
		'value' => $openings,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['openings_all'] = array(
		'value' => $openings_all,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['klicks'] = array(
		'value' => $klicks,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['klicks_all'] = array(
		'value' => $klicks_all,
		'dataType' => \PDO::PARAM_INT
	);
       $campaignUpdateDataArray['sbounces'] = array(
		'value' => $sbounces,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['hbounces'] = array(
		'value' => $hbounces,
		'dataType' => \PDO::PARAM_INT
	);
          $campaignUpdateDataArray['abmelder'] = array(
		'value' => $abmelder,
		'dataType' => \PDO::PARAM_INT
	);        
	/*$campaignUpdateDataArray['gestartet']  = $campaignUpdateDataArray['datum'] = array(
		'value' => '2016-09-21 12:10:01',
		'dataType' => \PDO::PARAM_STR
	);
	/*$campaignUpdateDataArray['beendet']  = array(
		'value' => '2016-09-21 12:30:01',
		'dataType' => \PDO::PARAM_STR
	); */
	
	$debugLogManager->endGroup();
}

function getMaileonlMailingDataForCampaignDataArray(\MaileonWebservice $deliverySystemWebservice, \CampaignEntity $campaignEntity, \debugLogManager $debugLogManager, array &$campaignUpdateDataArray){
     	$debugLogManager->beginGroup(__FUNCTION__);
	
	$status = 0;      
        
    $mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');
        
        /**
         * getState
         * Debug
         */
        $responceStatus = $mailingWebservice->getState( 
                          $campaignEntity->getMail_id()
                      );
               $mailingStatus = (string)$responceStatus->getResultXML(); 
               
         $debugLogManager->logData('mailingStatus', $mailingStatus);
         
         $datumSchedule = $mailingWebservice->getSchedule(
                 $campaignEntity->getMail_id()
                 );
        $startDatumArray =  (array)$datumSchedule->getResultXML();
        //gestarte Time
        $startDatum = new \DateTime(); 
          $date_1 = explode(' ', $startDatumArray['date']);
          $datum = explode('-', $date_1[0]);
          $year = $datum[0];
          $month = $datum[1];
          $day = $datum[2];
        $startDatum->setDate($year, $month, $day);
        $startDatum->setTime(
                    $startDatumArray['hours'],
                    $startDatumArray['minutes'], 
                    00);
                        
         if ($mailingStatus !== 'draft') {
		switch ($mailingStatus) {
			case 'done':
				if ($campaignEntity->getStatus() != 5 
					&& $campaignEntity->getStatus() < 20 
				) {
					$status = 5;
				}
			                              
				break;

			case 'sending':
				$status = 6;
				break;

			case 'canceled':
				$status = 7;
				break;
		}
               
        if ($status > 0) {
			$campaignUpdateDataArray['status'] = array(
				'value' => $status,
				'dataType' => \PDO::PARAM_INT
			);
		}
        $campaignUpdateDataArray['gestartet']  = $campaignUpdateDataArray['datum'] = array(
		                'value' => $startDatum->format('Y-m-d H:i:s'),
		                'dataType' => \PDO::PARAM_STR
	                     );
         }
     $debugLogManager->endGroup();
     
    return $mailingStatus;
}