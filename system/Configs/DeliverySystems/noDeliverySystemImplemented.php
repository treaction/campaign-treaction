<?php
\DebugAndExceptionUtils::sendDebugData(
	array(
		'file' => $file,
		'userFullname' => $_SESSION['u_vorname'] . ' ' . $_SESSION['u_nachname'] . ' (' . $_SESSION['benutzer_id'] . ')',
		'client' => $_SESSION['mandant'] . ' (' . $_SESSION['mID'] . ')',
		'deliverySystem' => $campaignEntity->getVersandsystem(),
		'kId' => $campaignEntity->getK_id(),
	),
	__FUNCTION__
);

die('Tech. fehler!');