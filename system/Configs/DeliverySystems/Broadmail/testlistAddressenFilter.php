<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $clientManager \ClientManager */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


try {
	
	$recipientFilterWebservice = $deliverySystemWebservice->getWebserviceObjectByType('RecipientFilterWebservice');
	$filterId = $recipientFilterWebservice->createTemporary(
			$deliverySystemWebservice->getSession(),
			false,
			'emailaddress',
			'contains',
			array(
				$filter
			)
			);
	
	$modiId = $recipientFilterWebservice->beginConditionModification(
			$deliverySystemWebservice->getSession(),
            $filterId
			);
	    
	$recipientFilterWebservice->addOrCondition(
			$deliverySystemWebservice->getSession(),
			$modiId,
			false,
			'Vorname',
			'contains',
			array(
				$filter
			)
			);
	$recipientFilterWebservice->addOrCondition(
			$deliverySystemWebservice->getSession(),
			$modiId,
			false,
			'Nachname',
			'contains',
			array(
				$filter
			)
			);
			
			
	$recipientFilterWebservice->commitConditionModification(
			$deliverySystemWebservice->getSession(),
            $modiId
			);
	/**
	* getWebserviceObjectByType
	 * 
	 * RecipientWebservice
	*/
	$recipientWebservice = $deliverySystemWebservice->getWebserviceObjectByType('RecipientWebservice');
	
	/**
	 * getAll
	 * 
	 * debug
	 */
	$recipientDataArray = $recipientWebservice->getAllAdvanced(
        $deliverySystemWebservice->getSession(),
        $recipientListId,
        $attributesDataArray,
		$filterId,
		'NULL',
		false,
		0,
		1000
    );
    $debugLogManager->logData('recipientDataArray', $recipientDataArray);
	
	if (\count($recipientDataArray) > 0) {
		foreach ($recipientDataArray as $item) {
			$resultDataArray[] = array(
                'id' => \HtmlFormUtils::createCheckbox(
                    array(
                        'onclick' => 'addRecipientToList(this.value)',
                        'name' => 'testempfaenger[]',
                        'value' => $item[0]
                    )
                ),
                'email' => $item[0],
                'vn' => \htmlspecialchars($item[1]),
                'nn' => \htmlspecialchars($item[2])
            );
		}
	}
} catch (\Exception $e) {
	require_once('processException.php');
}