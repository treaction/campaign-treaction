<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $campaignEntity \CampaignEntity */
/* @var $debugLogManager \debugLogManager */


try {
	/**
	 * getWebserviceObjectByType
	 */
	$mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');

	/**
	 * getStatus
	 * 
	 * debug
	 */
	$mailingStatus = $mailingWebservice->getStatus(
		$deliverySystemWebservice->getSession(),
		$campaignEntity->getMail_id()
	);
	$debugLogManager->logData('mailingStatus', $mailingStatus);

	switch ($mailingStatus) {
		case 'NEW':
			$mailingWebservice->remove(
				$deliverySystemWebservice->getSession(),
				$campaignEntity->getMail_id()
			);

			$resultData[$campaignEntity->getK_id()]['deleteFromDeliverySystem'] = true;
			$result = true;
			break;

		default:
			/**
			 * kampagne wurde schon versendet, l�schen nicht m�glich
			 * 
			 * TODO: abfrage der rechte und ggbf info text anzeigen
			 */
			$resultData[$campaignEntity->getK_id()]['deleteFromDeliverySystem'] = false;
			$result = false;
			break;
	}
	
	$resultData['mailingStatus'][$campaignEntity->getK_id()] = $mailingStatus;
} catch (\Exception $e) {
	require_once('processException.php');
}