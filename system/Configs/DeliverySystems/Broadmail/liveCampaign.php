<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $clientEntity \ClientEntity */
/* @var $deliverySystemDistributorWidthClientDeliveryEntity \DeliverySystemDistributorWidthClientDeliverySystemEntity */


try {
	/**
	 * getWebserviceObjectByType
	 */
	$mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');

	/**
	 * getStatus
	 * 
	 * debug
	 */
	$mailingStatus = $mailingWebservice->getStatus(
		$deliverySystemWebservice->getSession(),
		$campaignEntity->getMail_id()
	);
	$debugLogManager->logData('mailingStatus', $mailingStatus);
	
	switch ($mailingStatus) {
		case 'DONE':
			$campaignStatus = '<img src="/system/img/icons/accept.png" />';
			
			/*
			$mailingIdDone[] = $clientEntity->getId();
			if (\RegistryUtils::get('campaign' . \RegistryUtils::$arrayKeyDelimiter . 'mailingIdDone') 
				&& \array_search($clientEntity->getId(), \RegistryUtils::get('campaign' . \RegistryUtils::$arrayKeyDelimiter . 'mailingIdDone')) === false
			) {
				$result = $clientEntity->getMandant() . ': ' . $campaignEntity->getCustomerEntity()->getFirma() . ', ' . $campaignEntity->getK_name() . ' wurde versendet' . \chr(11);
			} else {
				$result = $clientEntity->getMandant() . ': ' . $campaignEntity->getCustomerEntity()->getFirma() . ', ' . $campaignEntity->getK_name() . ' wurde versendet' . \chr(11);
			}
			 * 
			 */
			break;
		
		case 'SENDING':
			$campaignStatus = 'sende...';
			break;
		
		case 'CANCELED':
			$campaignStatus = 'abgebrochen';
			break;
	}
	
	/**
	 * getSentRecipientCount
	 * 
	 * debug
	 */
	$campaignShips = $mailingWebservice->getSentRecipientCount(
		$deliverySystemWebservice->getSession(),
		$campaignEntity->getMail_id()
	);
	$debugLogManager->logData('campaignShips', $campaignShips);
} catch (\Exception $e) {
	require_once('processException.php');
}