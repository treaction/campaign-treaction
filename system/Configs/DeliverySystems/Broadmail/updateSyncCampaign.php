<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $campaignManager \CampaignManager */
/* @var $campaignEntity \CampaignEntity */
/* @var $debugLogManager \debugLogManager */


try {
	require_once('BroadmailFunctions.php');
	
	$campaignUpdateDataArray = array(
		'mail_id' => array(
			'value' => $campaignEntity->getMail_id(),
			'dataType' => \PDO::PARAM_STR
		)
	);
	
	/**
	 * getBroadmailMailingDataForCampaignDataArray
	 */
	$mailingStatus = \getBroadmailMailingDataForCampaignDataArray(
		$deliverySystemWebservice,
		$campaignEntity,
		$debugLogManager,
		$campaignUpdateDataArray
	);
	
	// mailing wurde gestartet bzw. abgebrochen
	if ($mailingStatus !== 'NEW') {
		/**
		* getBroadmailMailingReportingDataForCampaignDataArray
		*/
		\getBroadmailMailingReportingDataForCampaignDataArray(
			$deliverySystemWebservice,
			$campaignEntity,
			$debugLogManager,
			$campaignUpdateDataArray
		);
		
		/**
		 * getBroadmailResponseDataForCampaignDataArray
		 */
		\getBroadmailResponseDataForCampaignDataArray(
			$deliverySystemWebservice,
			$campaignEntity,
			$debugLogManager,
			$campaignUpdateDataArray
		);
	}
	
	// debug
	$debugLogManager->logData('updataDataArray', $campaignUpdateDataArray);
	
	
	/**
	* updateCampaignAndAddLogItem
	* 
	* debug
	*/
	$resLogCampaignUpdate = $campaignManager->updateCampaignAndAddLogItem(
		$campaignEntity->getK_id(),
		$campaignEntity->getNv_id(),
		(int) $_SESSION['benutzer_id'],
		$actionId,
		0,
		$campaignUpdateDataArray
	);
	$debugLogManager->logData('resLogCampaignUpdate', $resLogCampaignUpdate);
	unset($campaignUpdateDataArray);
} catch (\Exception $e) {
	require_once('processException.php');
}