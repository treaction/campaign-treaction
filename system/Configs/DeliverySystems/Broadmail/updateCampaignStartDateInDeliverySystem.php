<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */
/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


/**
 * startMailingToDeliveryTime
 * 
 * @param \SoapClient $mailingWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \CampaignEntity $campaignEntity
 * @param \debugLogManager $debugLogManager
 * @return void
 */
function startMailingToDeliveryTime(\SoapClient $mailingWebservice, \BroadmailWebservice $deliverySystemWebservice, \CampaignEntity &$campaignEntity, \debugLogManager $debugLogManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	$mailingWebservice->start(
		$deliverySystemWebservice->getSession(),
		$campaignEntity->getMail_id(),
		$campaignEntity->getDatum()->format('c')
	);
	$debugLogManager->logData('startDatum', $campaignEntity->getDatum()->format('c'));
	
	// status �ndern
	$campaignEntity->setStatus(13);
	
	$debugLogManager->endGroup();
}

/**
 * copyAndDeleteOldMailingFromDeliverySystem
 * 
 * @param \SoapClient $mailingWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \CampaignEntity $campaignEntity
 * @param \debugLogManager $debugLogManager
 * @return void
 * 
 * @throws \InvalidArgumentException
 */
function copyAndDeleteOldMailingFromDeliverySystem(\SoapClient $mailingWebservice, \BroadmailWebservice $deliverySystemWebservice, \CampaignEntity &$campaignEntity, \debugLogManager $debugLogManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * copy mailing
	 * 
	 * debug
	 */
	$newMailingId = $mailingWebservice->copy(
		$deliverySystemWebservice->getSession(),
		$campaignEntity->getMail_id()
	);
	if (!\is_numeric($newMailingId)) {
		throw new \InvalidArgumentException((string) $mailingWebservice->getError());
	}
	$debugLogManager->logData('newMailingId', $newMailingId);
	
	
	/**
	 * altes mailing aus broadmail l�schen
	 * 
	 * debug
	 */
	$mailingWebservice->remove(
		$deliverySystemWebservice->getSession(),
		$campaignEntity->getMail_id()
	);
	$debugLogManager->logData('removeMailingId', $campaignEntity->getMail_id());
	
	
	// mailing id aktualisieren
	$campaignEntity->setMail_id($newMailingId);
	
	// status �ndern
	$campaignEntity->setStatus(1);
	
	$debugLogManager->endGroup();
}


try {
	/**
	 * getWebserviceObjectByType
	 */
	$mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');
	
	if ((boolean) $campaignEntity->getUse_campaign_start_date() === true) {
		/**
		 * startMailingToDeliveryTime
		 * 
		 * mailing zum programiertes zeitpunkt starten
		 */
		\startMailingToDeliveryTime(
			$mailingWebservice,
			$deliverySystemWebservice,
			$campaignEntity,
			$debugLogManager
		);
		
		$aspAction = 'startMailingToDeliveryTime';
	} else {
		/**
		 * copyAndDeleteOldMailingFromDeliverySystem
		 */
		\copyAndDeleteOldMailingFromDeliverySystem(
			$mailingWebservice,
			$deliverySystemWebservice,
			$campaignEntity,
			$debugLogManager
		);
		
		$aspAction = 'copyAndDeleteOldMailingFromDeliverySystem';
	}
	
	// debug
	$debugLogManager->logData('action', $aspAction);
	
	
	/**
	 * addNewLogEntry
	 * 
	 * debug
	 */
	$idAddLogEntry = \addNewLogEntry(
		$campaignManager,
		$campaignEntity,
		18
	);
	$debugLogManager->logData('idAddLogEntry', $idAddLogEntry);
} catch (\Exception $e) {
	require_once('processException.php');
}