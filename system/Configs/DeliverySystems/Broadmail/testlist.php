<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $deliverySystemDistributorWidthClientDeliverySystemEntity \DeliverySystemDistributorWidthClientDeliverySystemEntity */


/**
 * getTestRecipientList
 * 
 * @param \DeliverySystemDistributorWidthClientDeliverySystemEntity $deliverySystemDistributorWidthClientDeliverySystemEntity
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \debugLogManager $debugLogManager
 * @return void
 */
function getTestRecipientList(\DeliverySystemDistributorWidthClientDeliverySystemEntity $deliverySystemDistributorWidthClientDeliverySystemEntity, \BroadmailWebservice $deliverySystemWebservice, \debugLogManager $debugLogManager) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getWebserviceObjectByType
	 * 
	 * RecipientListWebservice
	 */
	$recipientListWebservice = $deliverySystemWebservice->getWebserviceObjectByType('RecipientListWebservice');
	
	/**
	 * getAllIds
	 * 
	 * debug
	 */
	$listDataArray = $recipientListWebservice->getAllIds($deliverySystemWebservice->getSession());
	$debugLogManager->logData('listDataArray', $listDataArray);
	
	
	$testListDataArray = array();
	foreach ($listDataArray as $recipientListId) {
		if ($recipientListWebservice->isTestRecipientList($deliverySystemWebservice->getSession(), $recipientListId)) {
			$testListDataArray[$recipientListId] = $recipientListWebservice->getName(
				$deliverySystemWebservice->getSession(),
				$recipientListId
			);
		}
	}
	
	// debug
	$debugLogManager->logData('testListDataArray', $testListDataArray);
	
	\RegistryUtils::set(
		'campaign',
		array(
			$deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getId() => array(
				$deliverySystemDistributorWidthClientDeliverySystemEntity->getM_asp_id() => array(
					'testListDataArray' => $testListDataArray
				)
			)
		)
	);
	unset($testListDataArray);
	
	// debug
	$debugLogManager->endGroup();
}


try {
	if (!isset($testListDataArray)) {
		/**
		 * getTestRecipientList
		 */
		\getTestRecipientList(
			$deliverySystemDistributorWidthClientDeliverySystemEntity,
			$deliverySystemWebservice,
			$debugLogManager
		);
		
		// getTestListDataArray
		$testListDataArray = \getTestListDataArray($deliverySystemDistributorWidthClientDeliverySystemEntity);
	}
	
	foreach ($testListDataArray as $recipientListId => $recipientLisName) {
		$selected = null;
		if ((string) $recipientListId === (string) $deliverySystemDistributorWidthClientDeliverySystemEntity->getTestlist_distributor_id()) {
			$selected = 'selected';
		}
		
		$optionsItems .= \HtmlFormUtils::createOptionItem(
			array(
				'value' => $recipientListId,
				'selected' => $selected
			),
			$recipientLisName
		);
	}
} catch (\Exception $e) {
	require_once('processException.php');
}