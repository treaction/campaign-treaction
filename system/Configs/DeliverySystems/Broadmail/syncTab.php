<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $campaignEntity \CampaignEntity */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


/**
 * createDataList
 * 
 * @param integer $startPage
 * @param integer $nextPage
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param integer $deliverySystemStatus
 * @param \SoapClient $mailingWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \debugLogManager $debugLogManager
 * @return string
 */
function createDataList($startPage, $nextPage, \CampaignWidthCustomerAndContactPersonEntity $campaignEntity, $deliverySystemStatus, \SoapClient $mailingWebservice, \BroadmailWebservice $deliverySystemWebservice, \debugLogManager $debugLogManager) {
	$result = $foundStyle = '';
	
	// debug
	$debugLogManager->beginGroup(__FUNCTION__ . '_' . $startPage . '-' . $nextPage);
	for ($startPage; $startPage < $nextPage; $startPage++) {
		$bgColor = ($startPage % 2 ? '#E4E4E4' : '#FFFFFF');

		
		$mailingId = $_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds'][$startPage];
		if (!$mailingId) {
			break;
		}
	
		// debug
		$debugLogManager->beginGroup($mailingId);

		/**
		* getName
		* 
		* debug
		*/
		$mailingName = $mailingWebservice->getName(
			$deliverySystemWebservice->getSession(),
			$mailingId
		);
		$debugLogManager->logData('mailingName', $mailingName);
		
		if (\trim($mailingName) === trim($campaignEntity->getCustomerEntity()->getFirma() . ', ' . $campaignEntity->getK_name())) {
			$bgColor = '#51A825';
		}
		
		/**
		* createTableRow
		*/
		$tableRowDataArray = \HtmlTableUtils::createTableRow(
			array(
				'onclick' => 'submitData(' . $mailingId . ', ' . $campaignEntity->getK_id() . ')',
				'onMouseOver' => 'this.style.backgroundColor=\'#A6CBFF\';return false;',
				'onMouseOut' => 'this.style.backgroundColor=\'' . $bgColor . '\';this.style.color=\'#000000\';return false;',
				'style' => 'cursor:pointer;background-color:' . $bgColor . ';border:1px solid #999999;'
			)
		);
		
		/**
		 * getStatus
		 * 
		 * debug
		 */
		$mailingStatus = $mailingWebservice->getStatus(
			$deliverySystemWebservice->getSession(),
			$mailingId
		);
		$debugLogManager->logData('mailingStatus', $mailingStatus);
	
		if (\strlen($mailingName) > 0) {
			$result .=
				$tableRowDataArray['begin']
					. \HtmlTableUtils::createTableCellWidthContent(
						array(),
						$mailingName
					)
					. \HtmlTableUtils::createTableCellWidthContent(
						array(),
						\getMailingStatusOrDate(
							$mailingStatus,
							$mailingId,
							$mailingWebservice,
							$deliverySystemWebservice,
							$debugLogManager
						)
					)
				. $tableRowDataArray['end']
			;
		}
		
		// debug
		$debugLogManager->endGroup();
	}
	// debug
	$debugLogManager->endGroup();
	
	return $result;
}

/**
 * getMailingStatusOrDate
 * 
 * @param string $mailingStatus
 * @param integer $mailingId
 * @param \SoapClient $mailingWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \debugLogManager $debugLogManager
 * @return string
 */
function getMailingStatusOrDate($mailingStatus, $mailingId, \SoapClient $mailingWebservice, \BroadmailWebservice $deliverySystemWebservice, \debugLogManager $debugLogManager) {
	if ($mailingStatus !== 'NEW') {
		$mailingStartetDate = new \DateTime(
			$mailingWebservice->getSendingStartedDate(
				$deliverySystemWebservice->getSession(),
				$mailingId
			)
		);
		$mailingStartetDate->setTimezone(new DateTimeZone('Europe/Berlin'));
		$debugLogManager->logData(__FUNCTION__, $mailingStartetDate);
		
		$result = $mailingStartetDate->format('Y-m-d H:i');
	} else {
		$result = 'NEU';
	}
	
	return $result;
}


// debug
$debugLogManager->beginGroup(__FILE__);
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);

try {
	$blaettern = '';
	
	/**
	* getWebserviceObjectByType
	*/
	$mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');
	
	if (isset($_GET['start_p']) 
		&& (int) $_GET['start_p'] > 0
	) {
		$startPage = (int) $_GET['start_p'];
		$prevPage = $startPage - $countMailings;
		$nextPage = $startPage + $countMailings;
		$pageFrom = ($startPage / $countMailings) + 1;
		
		$blaettern .= '<button class="button" onclick="setRequest_default2(\'' . \BaseUtils::setScriptFileforRequestData('syncTab.php') . '?' . \http_build_query($urlParamDataArray) . '&start_p=' . $prevPage . '\', \'campaignSync\');"><</button>';
		if (\count($_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds']) > $nextPage) {
			$blaettern .= ' <button class="button" onclick="setRequest_default2(\'' . \BaseUtils::setScriptFileforRequestData('syncTab.php') . '?' . \http_build_query($urlParamDataArray) . '&start_p=' . $nextPage . '\', \'campaignSync\');">></button>';
		}
	} else {
		$mailingStatusDataArray = \BroadmailWebservice::getMailingStatusDataArray();
		
		if ($deliverySystemStatus === 100) {
			/**
			* getIds
			*/
			$_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds'] = $mailingWebservice->getIds(
				$deliverySystemWebservice->getSession(),
				'regular'
			);
		} else {
			/**
			* getIdsInStatus
			*/
			$_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds'] = $mailingWebservice->getIdsInStatus(
				$deliverySystemWebservice->getSession(),
				'regular',
				$mailingStatusDataArray[$deliverySystemStatus]
			);
		}
		\rsort($_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds']);
		
		$nextPage = $countMailings;
		$startPage = $prevPage = 0;
		$pageFrom = 1;
		
		if (\count($_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds']) > $countMailings) {
			$blaettern = '<button class="button" onclick="setRequest_default2(\'' . \BaseUtils::setScriptFileforRequestData('syncTab.php') . '?' . \http_build_query($urlParamDataArray) . '&start_p=' . $countMailings . '\', \'campaignSync\');">></button>';
		}
	}
	
	$totalCountMailingsIds = \count($_SESSION['campaign']['sync'][$deliverySystemStatus]['mailingIds']);
	$pageTotal = \round(
		($totalCountMailingsIds / $countMailings),
		0
	) + 1;
	
	// debug
	$debugLogManager->logData('countMailings', $countMailings);
	$debugLogManager->logData('startPage', $startPage);
	$debugLogManager->logData('nextPage', $nextPage);
	$debugLogManager->logData('prevPage', $prevPage);
	
	$debugLogManager->logData('pageFrom', $pageFrom);
	$debugLogManager->logData('totalCountMailingsIds', $totalCountMailingsIds);
	$debugLogManager->logData('pageTotal', $pageTotal);
	
	if ($totalCountMailingsIds > 0) {
		/**
		* createDataList
		*/
		$contentResult = \createDataList(
			$startPage,
			$nextPage,
			$campaignEntity,
			$deliverySystemStatus,
			$mailingWebservice,
			$deliverySystemWebservice,
			$debugLogManager
		);
	} else {
		$tableRowDataArray = \HtmlTableUtils::createTableRow();
		
		$contentResult = 
			$tableRowDataArray['begin'] 
				. \HtmlTableUtils::createTableCellWidthContent(
					array(
						'colspan' => 2,
						'align'=> 'center'
					),
					'Keine Kampagnen vorhanden!'
				)
			. $tableRowDataArray['end']
		;
	}
} catch (\Exception $e) {
	require_once('processException.php');
}

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());
$debugLogManager->endGroup();
?>
<table cellpadding="0" cellspacing="0" border="0" class="table">
	<thead>
		<tr>
			<th class="align center noBottomBorder">
				Seite <?php echo $pageFrom; ?> von <?php echo $pageTotal; ?> (Gesamtkampagnen: <?php echo $totalCountMailingsIds; ?>)
			</th>
			<th class="align right noBottomBorder"><?php echo $blaettern; ?></th>
		</tr>
		<tr>
			<th class="headerStyleColumn noTopBorder" width="580">Kampagnenname</th>
			<th class="headerStyleColumn noTopBorder">Gestartet</th>
		</tr>
	</thead>

	<tfoot>
		<tr class="footerStyleColumn">
			<td colspan="2" class="align right"><?php echo $blaettern; ?></td>
		</tr>
	</tfoot>

	<tbody>
		<?php echo $contentResult; ?>
	</tbody>
</table>