<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $clientManager \ClientManager */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


try {
	/**
	* getWebserviceObjectByType
	 * 
	 * RecipientWebservice
	*/
	$recipientWebservice = $deliverySystemWebservice->getWebserviceObjectByType('RecipientWebservice');
	
	/**
	 * getAll
	 * 
	 * debug
	 */
	$recipientDataArray = $recipientWebservice->getAll(
        $deliverySystemWebservice->getSession(),
        $recipientListId,
        $attributesDataArray
    );
    $debugLogManager->logData('recipientDataArray', $recipientDataArray);
	
	if (\count($recipientDataArray) > 0) {
		foreach ($recipientDataArray as $item) {
			$resultDataArray[] = array(
                'id' => \HtmlFormUtils::createCheckbox(
                    array(
                        'onclick' => 'addRecipientToList(this.value)',
                        'name' => 'testempfaenger[]',
                        'value' => $item[0]
                    )
                ),
                'email' => $item[0],
                'vn' => \htmlspecialchars($item[1]),
                'nn' => \htmlspecialchars($item[2])
            );
		}
	}
} catch (\Exception $e) {
	require_once('processException.php');
}