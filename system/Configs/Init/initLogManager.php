<?php
require_once(\DIR_Manager . 'LogManager.php');

$logManager = new \LogManager();
/* @var $logManager LogManager */
$logManager->setHost($dbHostEms);
$logManager->setDbName($db_name);
$logManager->setDbUser($db_user);
$logManager->setDbPassword($db_pw);
$logManager->setLogTable($log);
$logManager->setPdoDebug($pdoDebug);
$logManager->setStatementsDebug($pdoStatmentDebug);
$logManager->init();

/**
 * @deprecated moved into: $campaignManager
 */