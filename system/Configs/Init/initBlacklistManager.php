<?php
require_once(\DIR_Manager . 'BlacklistManager.php');

$blacklistManager = new \BlacklistManager();
$blacklistManager->setPdoDebug($pdoDebug);
$blacklistManager->setStatementsDebug($pdoStatmentDebug);
$blacklistManager->init();