<?php
require_once(DIR_ModuleClientSettings . 'Campaigns' . \DIRECTORY_SEPARATOR . 'Invoice' . \DIRECTORY_SEPARATOR . 'Resources' . \DIRECTORY_SEPARATOR . 'DefaultClient.php');

class InvoiceClient extends \DefaultClient {
	protected $defaultProductDataArray = array(
		array(
			'title' => 'Typ:',
			'content' => 'Standalone permission-based'
		),
		array(
			'title' => 'Verteiler:',
			'content' => 'Ceoo'
		)
	);
}