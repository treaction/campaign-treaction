<?php
require_once(DIR_Module_Campaigns . 'Packages' . \DIRECTORY_SEPARATOR . 'Invoice' . \DIRECTORY_SEPARATOR . 'InvoiceAbstract.php');

class DefaultClient extends \EMS\Campaigns\Invoice\InvoiceAbstract {
	protected $defaultProductDataArray = array(
		array(
			'title' => 'Typ:',
			'content' => 'Standalone permission-based'
		),
		array(
			'title' => 'Verteiler:',
			'content' => 'Interactive Mailing'
		)
	);
	
	private $defaultTableColumnsDataArray = array(
		'k_id' => array(
			'rowTitle' => 'EMS ID'
		),
		'datum' => array(
			'rowTitle' => 'Datum'
		),
		'k_name' => array(
			'rowTitle' => 'Kampagne'
		),
		'customerCompany' => array(
			'rowTitle' => 'Kunde / Agentur'
		)
	);
	
	
	
	/**
	* initAndGetTableColumnsDataArray
	* 
	* @return array
	*/
	protected function initAndGetTableColumnsDataArray() {
		switch ($this->settings['billingType']) {
			case 'CPL':
			case 'CPO':
			case 'CPC':
				$resultDataArray = array(
					'leads' => array(
						'rowTitle' => 'Leads'
					),
					'preis' => array(
						'rowTitle' => \CampaignAndCustomerUtils::$allTabFieldsDataArray['preis']['sublabel']
					),
					'zielgruppe' => array(
						'rowTitle' => 'Land Empfänger',
					),
					'preis_gesamt' => array(
						'rowTitle' => 'Preis netto'
					),
				);
				break;
			
			default:
				$resultDataArray = array(
					'gebucht' => array(
						'rowTitle' => 'Empfänger'
					),
					'preis' => array(
						'rowTitle' => \CampaignAndCustomerUtils::$allTabFieldsDataArray['preis']['sublabel']
					),
					'zielgruppe' => array(
						'rowTitle' => 'Land Empfänger',
					),
					'preis_gesamt' => array(
						'rowTitle' => 'Preis netto'
					),
				);
				break;
		}
		
		return \array_merge($this->defaultTableColumnsDataArray, $resultDataArray);
	}
	
	/**
	 * updateProductDto
	 * 
	 * @param integer $nvCampaignsCount
	 * @param \CampaignEntity $campaignEntity
	 * @param \CampaignWidthCustomerAndContactPersonEntity $tmpHvCampaigEntity
	 * @param \EMS\Campaigns\Invoice\Dto\ProductDto $productDto
	 * @return \EMS\Campaigns\Invoice\Dto\ProductDto
	 */
	protected function updateProductDto($nvCampaignsCount, \CampaignEntity $campaignEntity, \CampaignWidthCustomerAndContactPersonEntity $tmpHvCampaigEntity, \EMS\Campaigns\Invoice\Dto\ProductDto $productDto) {
		$productDto->setNetPrice($this->calculateProductNetPrice(
			$nvCampaignsCount,
			$campaignEntity,
			$tmpHvCampaigEntity
		));
		
		/**
		 * default country (DE)
		 */
		$productDto->setTotalPrice($productDto->getNetPrice() * 1.19);
		$productDto->setCountryTaxPrice($productDto->getTotalPrice() - $productDto->getNetPrice());
		
		if ($tmpHvCampaigEntity->getCustomerEntity()->getCountry_id() > 0) {
			if ((string) $tmpHvCampaigEntity->getCustomerEntity()->getStaticCountry()->getCn_iso_2() !== 'DE') {
				$productDto->setCountryTaxPrice(0);
				$productDto->setTotalPrice($productDto->getNetPrice());
			}
		}
		
		/**
		 * calculateDiscount
		 */
		$productDto = $this->calculateDiscount($productDto);
		
		return $productDto;
	}
	
	/**
	 * calculateDiscount
	 * 
	 * @param \EMS\Campaigns\Invoice\Dto\ProductDto $productDto
	 * @return \EMS\Campaigns\Invoice\Dto\ProductDto
	 */
	protected function calculateDiscount(\EMS\Campaigns\Invoice\Dto\ProductDto $productDto) {
		$productDto->setCashDiscountPrice($productDto->getTotalPrice() * $this->cashDiscount);
		$productDto->setCashDiscountTotalPrice($productDto->getTotalPrice() - $productDto->getCashDiscountPrice());
		
		return $productDto;
	}
	
	/**
	 * getPaymentDeadlineAndUpdateCountryDto
	 * 
	 * @param \CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity
	 * @param \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto
	 * @return integer
	 */
	protected function getPaymentDeadlineAndUpdateCountryDto(\CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity, \EMS\Campaigns\Invoice\Dto\CountryDto &$countryDto) {
		$paymentDeadline = 10; // nur für alte kunden daten
		
		$countryDto->setPriceInfo('+ 19% Umsatzsteuer');
		if ($hvCampaignEntity->getCustomerEntity()->getCountry_id() > 0) {
			if ((string) $hvCampaignEntity->getCustomerEntity()->getStaticCountry()->getCn_iso_2() === 'DE') {
				$paymentDeadline = ($hvCampaignEntity->getCustomerEntity()->getPayment_deadline() 
					? $hvCampaignEntity->getCustomerEntity()->getPayment_deadline() 
					: $paymentDeadline
				);
			} else {
				$paymentDeadline = $hvCampaignEntity->getCustomerEntity()->getPayment_deadline();

				if ((string) $hvCampaignEntity->getCustomerEntity()->getStaticCountry()->getCn_iso_2() === 'CH') {
					#$countryDto->setPriceInfo('In Deutschland nicht steuerbare Leistung §3a (2), 1 UstG.');
                                        $countryDto->setPriceInfo('In Deutschland nicht umsatzsteuerbare Leistung, § 3a Abs. 2 Satz 1 UStG.');
				} else {
					#$countryDto->setPriceInfo('VAT-No. ' . $hvCampaignEntity->getCustomerEntity()->getVat_number());
					#$countryDto->setAdditionalInfo('Steuerschuldnerschaft des Leistungsempfängers gem. §13b UStG.');
                                        $countryDto->setPriceInfo('In Deutschland nicht umsatzsteuerbare Leistung, § 3a Abs. 2 Satz 1 UStG.');
                                        $countryDto->setAdditionalInfo('Leistungsempfänger ist Steuerschuldner.'
                                                                        . 'Ihre UID-Nr. ' . $hvCampaignEntity->getCustomerEntity()->getVat_number());
				}
			}
		}
		
		// International
		if ((int) $this->settings['templateSelection'] === 2) {
			#$countryDto->setPriceInfo('In Germany non-taxable service §3a (3) Set 1 UStG.');
                        $countryDto->setPriceInfo('in Germany non-taxable service, § 3a section 2 set 1 UStG.');
                        $countryDto->setAdditionalInfo('Paying the tax is the recipient');
		}
		
		return $paymentDeadline;
	}
	
	/**
	 * createProductDataArrayForInvoice
	 * 
	 * @param integer $invoiceOrderPositionNumber
	 * @param integer $nvCampaignsCount
	 * @param \CampaignEntity $hvCampaignEntity
	 * @param \CampaignWidthCustomerAndContactPersonEntity $tmpHvCampaigEntity
	 * @return array
	 */
	protected function createProductDataArrayForInvoice($invoiceOrderPositionNumber, $nvCampaignsCount, \CampaignEntity  $hvCampaignEntity, \CampaignWidthCustomerAndContactPersonEntity $tmpHvCampaigEntity) {
		switch ($hvCampaignEntity->getAbrechnungsart()) {
			case 'CPL':
			case 'CPO':
			case 'CPC':
				/**
				 * createProductDataArrayForCpxBillingType
				 */
				$productDataArray = $this->createProductDataArrayForCpxBillingType(
					$invoiceOrderPositionNumber,
					$nvCampaignsCount,
					$hvCampaignEntity,
					$tmpHvCampaigEntity
				);
				break;
			
			default:
				/**
				 * createProductDataArrayForDefaultBillingType
				 */
				$productDataArray = $this->createProductDataArrayForDefaultBillingType($hvCampaignEntity);
				break;
		}
		
		return $productDataArray;
	}
	
	/**
	 * createProductDataArrayForDefaultBillingType
	 * 
	 * @param \CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity
	 * @return array
	 */
	protected function createProductDataArrayForDefaultBillingType(\CampaignWidthCustomerAndContactPersonEntity $hvCampaignEntity) {
		$this->debugLogManager->beginGroup(__FUNCTION__);

		$additionalProductDataArray = array(
			array(
				'title' => 'Mailing:',
				'content' => \htmlspecialchars_decode($hvCampaignEntity->getK_name())
			),
			array(
				'title' => 'Empfänger:',
				'content' => \FormatUtils::numberFormat($hvCampaignEntity->getGebucht())
			),
			array(
				'title' => 'Selektion:',
				'content' => \htmlspecialchars_decode($hvCampaignEntity->getZielgruppe()) // TODO: evtl. verteiler und/oder land selection
			),
			array(
				'title' => 'Termin:',
				'content' => \DateUtils::getWeekDay(
					$hvCampaignEntity->getDatum(),
					false
				) . $hvCampaignEntity->getDatum()->format(', d.m.Y') . '*'
			),
			array(
				'title' => 'Uhrzeit:',
				'content' => $hvCampaignEntity->getDatum()->format('h:i') . ' Uhr'
			),
			array(
				'title' => 'AP:',
				'content' => $hvCampaignEntity->getContactPersonEntity()->getVorname() . ' ' . $hvCampaignEntity->getContactPersonEntity()->getNachname()
			),
			array(
				'title' => '',
				'content' => ''
			),
			array(
				'title' => 'Abrechnungsart:',
				'content' => $hvCampaignEntity->getAbrechnungsart()
			),
		);
		$productDataArray = \array_merge($this->defaultProductDataArray, $additionalProductDataArray);
		unset($additionalProductDataArray);

		/**
		 * createPriceContentForProductDataArray
		 */
		$this->createPriceContentForProductDataArray(
			$hvCampaignEntity,
			$productDataArray
		);

		$this->debugLogManager->logData('productDataArray', $productDataArray);
		$this->debugLogManager->endGroup();

		return $productDataArray;
	}
	
	/**
	 * createProductDataArrayForCpxBillingType
	 * 
	 * @param integer $invoiceOrderPositionNumber
	 * @param integer $nvCampaignsCount
	 * @param \CampaignEntity $hvCampaignEntity
	 * @param \CampaignEntity $tmpHvCampaigEntity
	 * @return array
	 */
	protected function createProductDataArrayForCpxBillingType($invoiceOrderPositionNumber, $nvCampaignsCount, \CampaignEntity $hvCampaignEntity, \CampaignEntity $tmpHvCampaigEntity) {
		$this->debugLogManager->beginGroup(__FUNCTION__);

		$productDataArray = array(
			'position' => $invoiceOrderPositionNumber . '.',
			'campaignTitle' => $hvCampaignEntity->getK_name(),
			'campaignDate' => \DateUtils::$monthDataArray[(int) $hvCampaignEntity->getDatum()->format('m')] 
				. $hvCampaignEntity->getDatum()->format(' Y') . '*',
			'campaignLeads' => \FormatUtils::numberFormat(
				($nvCampaignsCount 
						? (
							$tmpHvCampaigEntity->getLeads_u() > 0
								? $tmpHvCampaigEntity->getLeads_u() 
								: $tmpHvCampaigEntity->getLeads()
						)
						: (
							$hvCampaignEntity->getLeads_u() > 0
								? $hvCampaignEntity->getLeads_u() 
								: $hvCampaignEntity->getLeads()
						)
				)
			)
		);

		/**
		 * createPriceContentForProductDataArray
		 */
		$this->createPriceContentForProductDataArray(
			$hvCampaignEntity,
			$productDataArray
		);

		$this->debugLogManager->logData('productDataArray', $productDataArray);
		$this->debugLogManager->endGroup();

		return $productDataArray;
	}
	
	/**
	 * createSummarizedProductDataArrayForDefaultBillingType
	 * 
	 * @return array
	 */
	protected function createSummarizedProductDataArrayForDefaultBillingType() {
		$this->debugLogManager->beginGroup(__FUNCTION__);
		
		$dateContent = $dateTimeContent = '';
		foreach ($this->summarizedCampaignDateDataArray as $dateObject) {
			$dateContent .= \DateUtils::getWeekDay(
				$dateObject,
				false
			) . $dateObject->format(', d.m.Y') . '*; ';

			$dateTimeContent .= $dateObject->format('h:i') . '  Uhr; ';
		}

		$additionalProductDataArray = array(
			array(
				'title' => 'Mailing:',
				'content' => \rtrim(
					$this->summarizedCampaignEntity->getK_name(),
					self::SUMMARIZED_CAMPAIGN_VALUES_DELIMITER
				)
			),
			array(
				'title' => 'Empfänger:',
				'content' => \FormatUtils::numberFormat($this->summarizedCampaignEntity->getGebucht())
			),
			array(
				'title' => 'Selektion:',
				'content' => \rtrim(
					$this->summarizedCampaignEntity->getZielgruppe(), // TODO: evtl. verteiler und/oder land selection
					self::SUMMARIZED_CAMPAIGN_VALUES_DELIMITER
				)
			),
			array(
				'title' => 'Termin:',
				'content' => \rtrim(
					$dateContent,
					self::SUMMARIZED_CAMPAIGN_VALUES_DELIMITER
				)
			),
			array(
				'title' => 'Uhrzeit:',
				'content' => \rtrim(
					$dateTimeContent,
					self::SUMMARIZED_CAMPAIGN_VALUES_DELIMITER
				)
			),
			array(
				'title' => 'AP:',
				'content' => $this->summarizedCampaignEntity->getContactPersonEntity()->getVorname() 
					. ' ' . $this->summarizedCampaignEntity->getContactPersonEntity()->getNachname()
			),
			array(
				'title' => '',
				'content' => ''
			),
			array(
				'title' => 'Abrechnungsart:',
				'content' => $this->summarizedCampaignEntity->getAbrechnungsart()
			),
		);
		$productDataArray = \array_merge($this->defaultProductDataArray, $additionalProductDataArray);
		unset($additionalProductDataArray);

		/**
		 * createPriceContentForProductDataArray
		 */
		$this->createPriceContentForProductDataArray(
			$this->summarizedCampaignEntity,
			$productDataArray
		);

		$this->debugLogManager->logData('productDataArray', $productDataArray);
		$this->debugLogManager->endGroup();

		return $productDataArray;
	}
	
	/**
	* createPriceContentForProductDataArray
	* 
	* @param \CampaignEntity $campaignEntity
	* @param array $productDataArray
	* @return void
	*/
	protected function createPriceContentForProductDataArray(\CampaignEntity $campaignEntity, array &$productDataArray) {
		switch ($campaignEntity->getAbrechnungsart()) {
			case 'CPL':
			case 'CPO':
			case 'CPC':
				$productDataArray['campaignPrice'] = \FormatUtils::sumFormat($this->getConvertetPriceResult($campaignEntity)) 
					. \FormatUtils::euroSignInUtf8Unicode
				;
				break;
			
			case 'Hybrid':
				/**
				 * getHybridPriceTableDataArray
				 */
				$this->getHybridPriceTableDataArray(
					\explode(
						'<br />',
						\CampaignFieldsUtils::getPriceResult(
							'HV',
							false,
							$campaignEntity
						)
					),
					$productDataArray
				);
				
				/**
				 * addUnitPriceIntoProductDataArray
				 */
				$this->addUnitPriceIntoProductDataArray(
					$campaignEntity,
					$productDataArray
				);
				break;

			default:
				$productDataArray[] = array(
					'title' => 'Preis:',
					'content' => \FormatUtils::sumFormat($this->getConvertetPriceResult($campaignEntity)) 
						. \FormatUtils::euroSignInUtf8Unicode
				);
				
				/**
				 * addUnitPriceIntoProductDataArray
				 */
				$this->addUnitPriceIntoProductDataArray(
					$campaignEntity,
					$productDataArray
				);
				break;
		}
	}
	
	/**
	 * getHybridPriceTableDataArray
	 * 
	 * @param array $priceDataArray
	 * @param array $productDataArray
	 * @return void
	 */
	protected function getHybridPriceTableDataArray(array $priceDataArray, array &$productDataArray) {
		$this->debugLogManager->beginGroup(__FUNCTION__);
		$this->debugLogManager->logData('priceDataArray', $priceDataArray);

		foreach ($priceDataArray as $key => $item) {
			$this->debugLogManager->beginGroup(__FUNCTION__ . '_' . $key);

			// explode data
			$tmpDataArray = \explode(': ', $item);
			$this->debugLogManager->logData('tmpDataArray', $tmpDataArray);

			// getPrice
			$price = \DataFilterUtils::convertDataToFloatvalue(\FormatUtils::cleanPriceContent($tmpDataArray[1]));
			$this->debugLogManager->logData('price', $price);

			$productDataArray[] = array(
				'title' => $tmpDataArray[0] . ':',
				'content' => \FormatUtils::sumFormat($price) 
					. \FormatUtils::euroSignInUtf8Unicode
			);
			unset($tmpDataArray);

			$this->debugLogManager->endGroup();
		}

		$this->debugLogManager->endGroup();
	}
	
	/**
	 * calculateProductNetPrice
	 * 
	 * @param integer $nvCampaignsCount
	 * @param \CampaignEntity $hvCampaignEntity
	 * @param \CampaignEntity $tmpHvCampaigEntity
	 * @return float
	 */
	private function calculateProductNetPrice($nvCampaignsCount, \CampaignEntity $hvCampaignEntity, \CampaignEntity $tmpHvCampaigEntity) {
		$priceValue = \FormatUtils::cleanPriceContent(
			\CampaignFieldsUtils::getTotalPriceResult(
				'HV',
				($nvCampaignsCount 
					? true 
					: false
				),
				$hvCampaignEntity,
				$tmpHvCampaigEntity
			)
		);
		$result = \DataFilterUtils::convertDataToFloatvalue($priceValue);
		
		$this->debugLogManager->logData(__FUNCTION__, $result);
		
		return $result;
	}
	
	/**
	 * getConvertetPriceResult
	 * 
	 * @param \CampaignEntity $campaignEntity
	 * @return float
	 */
	private function getConvertetPriceResult(\CampaignEntity $campaignEntity) {
		return \DataFilterUtils::convertDataToFloatvalue(
			\FormatUtils::cleanPriceContent(
				\CampaignFieldsUtils::getPriceResult(
					'HV',
					false,
					$campaignEntity
				)
			)
		);
	}
	
	/**
	 * addUnitPriceIntoProductDataArray
	 * 
	 * @param \CampaignEntity $campaignEntity
	 * @param array $productDataArray
	 * @return void
	 */
	private function addUnitPriceIntoProductDataArray(\CampaignEntity $campaignEntity, array &$productDataArray) {
		if ($campaignEntity->unit_price > 0) {
			$productDataArray[] = array(
				'title' => 'Pauschale:',
				'content' => \FormatUtils::sumFormat($campaignEntity->getUnit_price()) 
					. \FormatUtils::euroSignInUtf8Unicode
			);
		}
	}
}
