<?php
/* @var $debugLogManager \debugLogManager */
/* @var $clientManager \ClientManager */


/**
 * init $campaignManager
 */
require_once(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */



/**
 * getActiveCountrySelectionDataItems
 */
\RegistryUtils::set('countrySelectionsDataArray', $clientManager->getActiveCountrySelectionDataItems());
$_SESSION['countrySelectionsDataArray'] = \RegistryUtils::get('countrySelectionsDataArray'); // TODO: remove from $_SESSION and use RegistryUtils
if (\count($_SESSION['countrySelectionsDataArray']) === 0) {
	throw new \DomainException('no countrySelectionsDataArray', 1426498972);
}


/**
 * getDeliverySystemDistributorsDataItemsByClientId
 */
$_SESSION['deliverySystemDistributorEntityDataArray'] = $clientManager->getDeliverySystemDistributorsDataItemsByClientId(
	$_SESSION['mID'],
	$_SESSION['underClientIds']
);
if (\count($_SESSION['deliverySystemDistributorEntityDataArray']) === 0) {
	throw new \DomainException('no deliverySystemDistributorEntityDataArray', 1426499057);
}


$jsACDataArray = array();
foreach (\RegistryUtils::get('staticCountryDataArray') as $staticCountryEntity) {
	/* @var $staticCountryEntity \StaticCountryEntity */
	$jsACDataArray[] = array(
		'title' => $staticCountryEntity->getCn_short_de(),
		'phoneCode' => $staticCountryEntity->getCn_phone(),
		'uid' => $staticCountryEntity->getUid()
	);
}
\RegistryUtils::set(
	'campaign',
	array(
		'staticCountryDataArray' => $jsACDataArray
	)
);
$_SESSION['campaign']['staticCountryDataArray'] = $jsACDataArray; // TODO: remove from $_SESSION and use RegistryUtils
unset($jsACDataArray);


/**
 * hvStatusDataArray and nvStatusDataArray
 */
$hvCampaignStatusDataArray = \CampaignAndCustomerUtils::$hvCampaignsStatusDataArray;
$hvCampaignStatusDataArray += \CampaignAndCustomerUtils::$campaignsReportingStatusDataArray;
$hvCampaignStatusDataArray += \CampaignAndCustomerUtils::$campaignsOrderStatusDataArray;
$hvCampaignStatusDataArray += \CampaignAndCustomerUtils::$campaignsOthersStatusDataArray;
\asort($hvCampaignStatusDataArray);

$statusDataArray = $hvCampaignStatusDataArray;
$statusDataArray += \CampaignAndCustomerUtils::$nvCampaignsStatusDataArray;
\asort($statusDataArray);

\RegistryUtils::set(
	'campaign',
	array(
		'statusDataArray' => array(
			'allHv' => $hvCampaignStatusDataArray,
			'allNv' => \CampaignAndCustomerUtils::$nvCampaignsStatusDataArray,
			'all' => $statusDataArray
		)
	)
);
unset($hvCampaignStatusDataArray);
$_SESSION['campaign']['statusDataArray'] = $statusDataArray; // TODO: remove from $_SESSION and use RegistryUtils
unset($statusDataArray);


/**
 * $campaignSearchBillingsTypeDataArray
 */
$campaignSearchBillingsTypeDataArray = \CampaignAndCustomerUtils::$campaignBillingsTypeDataArray;
unset($campaignSearchBillingsTypeDataArray['Festp']);
$campaignSearchBillingsTypeDataArray['CP'] = 'CPX';
$campaignSearchBillingsTypeDataArray['Festp'] = \CampaignAndCustomerUtils::$campaignBillingsTypeDataArray['Festp'];
\RegistryUtils::set('campaign', array('searchBillingsTypeDataArray' => $campaignSearchBillingsTypeDataArray));
unset($campaignSearchBillingsTypeDataArray);


/**
 * getActiveClientClickProfilesDataItems
 */
\RegistryUtils::set(
	'campaign',
	array(
		'clientClickProfilesDataArray' => $clientManager->getActiveClientClickProfilesDataItems($_SESSION['mID'])
	)
);