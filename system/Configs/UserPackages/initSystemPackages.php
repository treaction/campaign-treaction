<?php
/* @var $debugLogManager \debugLogManager */


/**
 * getAllStaticCountryDataItems
 */
\RegistryUtils::set('staticCountryDataArray', $clientManager->getAllStaticCountryDataItems());
$_SESSION['staticCountryDataArray'] = \RegistryUtils::get('staticCountryDataArray'); // TODO: remove from $_SESSION and use RegistryUtils
if (\count($_SESSION['staticCountryDataArray']) === 0) {
	throw new \DomainException('no StaticCountryEntity', 1426499074);
}

/**
 * getJsUserPackageVarDataArray
 * 
 * debug
 */
$jsUserPackageDataArray = \UserUtils::getJsUserPackageVarDataArray($userPackages);
$debugLogManager->logData('jsUserPackageDataArray', $jsUserPackageDataArray);