var Dom = YAHOO.util.Dom,
	Event = YAHOO.util.Event
;

Event.onDOMReady(function() {
	document.body.style.visibility = 'visible';

	var layout = new YAHOO.widget.Layout({
		minWidth: 1024,
		minHeight: 500,
		units: [
			{position: 'top', height: 53, resize: false, body: 'top1', gutter: '-1 -1 2 -1'},
			{position: 'left', minWidth: 200, width: 200, proxy: true},
			{position: 'center', minWidth: 400, minHeight: 200}
		]
	});

	layout.on('render', function() {
		var el = layout.getUnitByPosition('left').get('wrap');
		var layout3 = new YAHOO.widget.Layout(el, {
			parent: layout,
			minWidth: 200,
			minHeight: 200,
			units: [
				{position: 'top', body: 'top_check', gutter: '1 2 2 3', height: 40},
				{position: 'center', header: 'Men�', width: 200, body: 'left1', gutter: '1 2 3 3', scroll: true}
			]
		});
		layout3.render();
	});

	layout.on('render', function() {
		var el = layout.getUnitByPosition('center').get('wrap');
		var layout2 = new YAHOO.widget.Layout(el, {
			parent: layout,
			minWidth: 400,
			minHeight: 200,
			units: [
				{position: 'top', header: "Home", body: 'top2', gutter: '1 3 2 1', height: 66},
				{position: 'center', body: 'standard', height: 400, maxHeight: 1080, gutter: '-3 3 2 1', scroll: true}
			]
		});
		layout2.render();
	});
	layout.render();
	
	
	YAHOO.util.Event.on('treewrapper', 'click', function(ev) {
		var tar = YAHOO.util.Event.getTarget(ev);
		
		if (tar.tagName.toLowerCase() !== 'a') {
			tar = null;
		}
		if (tar && YAHOO.util.Selector.test(tar, '#treewrapper a')) {
			//if the href is a '#' then select the proper tab and change it's label
			if (tar && tar.getAttribute('href', 2) === '#') {

				layout.getUnitByPosition('center', 'top').set('header', tar.id);

				YAHOO.util.Dom.removeClass(YAHOO.util.Selector.query('#treewrapper a'), 'selected');
				var feedName = tar.parentNode.className;
				YAHOO.util.Dom.addClass(tar.parentNode, 'selected');
				YAHOO.util.Event.stopEvent(ev);
				var title = tar.innerHTML;
			}
		}
	});
});

document.onmouseup = hide;

function getXY(evt, site) {
	var tempX = 0;
	var tempY = 0;
	var IE = document.all ? true : false;
	
	if (!IE) {
		document.captureEvents(Event.MOUSEMOVE);
	}
	
	if (IE) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	} else {
		tempX = evt.pageX;
		tempY = evt.pageY;
	}

	if (tempX < 0) {
		tempX = 0;
	}
	if (tempY < 0) {
		tempY = 0;
	}

	if (site === 'k') {
		render_submenu(tempX, tempY, 'k_submenu');
	}

	if (site === 'a') {
		render_submenu(tempX, tempY, 'a_submenu');
	}

	if (site === 'u') {
		render_submenu(tempX, tempY, 'admin_user_submenu');
	}
	if (site === 'c') {
		render_submenu(tempX, tempY, 'admin_clickProfil_submenu');
	}
	if (site === 'dsd') {
		render_submenu(tempX, tempY, 'admin_dsd_submenu');
	}
	if (site === 'dsdn') {
		render_submenu(tempX, tempY, 'admin_dsdn_submenu');
	}
        if (site === 'esp') {
        render_submenu(tempX, tempY, 'admin_esp_submenu');
        }
        if (site === 'hf') {
        render_submenu(tempX, tempY, 'admin_hf_submenu');
        }


}

function render_submenu(tempX, tempY, m) {
	var oMenu = new YAHOO.widget.Menu(
		m,
		{shadow: false, x: tempX, y: tempY}
	);
	oMenu.render();
	oMenu.show();
}

function hide() {
	var oMenu = new YAHOO.widget.Menu('k_submenu', {shadow: false, x: 0, y: 0});
	oMenu.render();
	oMenu.blur();
	
	var aMenu = new YAHOO.widget.Menu('a_submenu', {shadow: false, x: 0, y: 0});
	aMenu.render();
	aMenu.blur();
	
	var uMenu = new YAHOO.widget.Menu('admin_user_submenu', {shadow: false, x: 0, y: 0});
	uMenu.render();
	uMenu.blur();
	
	var cMenu = new YAHOO.widget.Menu('admin_clickProfil_submenu', {shadow: false, x: 0, y: 0});
	cMenu.render();
	cMenu.blur();
	
	var dsdMenu = new YAHOO.widget.Menu('admin_dsd_submenu', {shadow: false, x: 0, y: 0});
	dsdMenu.render();
	dsdMenu.blur();
        
        var dsdnMenu = new YAHOO.widget.Menu('admin_dsdn_submenu', {shadow: false, x: 0, y: 0});
	dsdnMenu.render();
	dsdnMenu.blur();
        
        var espMenu = new YAHOO.widget.Menu('admin_esp_submenu', {shadow: false, x: 0, y: 0});
        espMenu.render();
        espMenu.blur();
        
        var hfMenu = new YAHOO.widget.Menu('admin_hf_submenu', {shadow: false, x: 0, y: 0});
        hfMenu.render();
        hfMenu.blur();


			
}