document.getElementById('button_neu_user').onclick = function() {
	setRequest_default('admin/user/userProfile.php', 'userProfile');
};

document.getElementById('button_edit_user').onclick = function() {
	var userid = document.getElementById('userID').value;
	setRequest_default('admin/user/userProfile.php?userID=' + userid, 'userProfile');
};

document.getElementById('button_neu_clickProfil').onclick = function() {
	setRequest_default('dispatch.php?_module=Admin&_controller=index&_action=viewDialogClickProfil', 'clickProfil');
};

document.getElementById('button_edit_clickProfil').onclick = function() {
	var clickProfilID = document.getElementById('clickProfilID').value;
	setRequest_default('dispatch.php?_module=Admin&_controller=index&_action=viewDialogClickProfil&clickProfilID=' + clickProfilID, 'clickProfil');
};

document.getElementById('button_neu_dsdn').onclick = function() {
	setRequest_default('dispatch.php?_module=Admin&_controller=index&_action=viewDialogDeliverySystemDomain', 'dsdn');
};
document.getElementById('button_edit_dsdn').onclick = function() {
       var dsdnID = document.getElementById('dsdnID').value;
	setRequest_default('dispatch.php?_module=Admin&_controller=index&_action=viewDialogDeliverySystemDomain&dsdnID=' + dsdnID, 'dsdn');
};

document.getElementById('button_neu_dsd').onclick = function() {
	setRequest_default('dispatch.php?_module=Admin&_controller=index&_action=viewDialogDeliverySystemDistributor', 'dsd');
};

document.getElementById('button_edit_dsd').onclick = function() {
	var dsdID = document.getElementById('dsdID').value;
	setRequest_default('dispatch.php?_module=Admin&_controller=index&_action=viewDialogDeliverySystemDistributor&dsdID=' + dsdID, 'dsd');
};

document.getElementById('button_neu_esp').onclick = function() {
    setRequest_default('dispatch.php?_module=Admin&_controller=index&_action=viewDialogEspMapping', 'esp');
};
document.getElementById('button_edit_esp').onclick = function() {
    var espID = document.getElementById('espID').value;
    setRequest_default('dispatch.php?_module=Admin&_controller=index&_action=viewDialogEspMapping&espID=' + espID, 'esp');
};

document.getElementById('button_neu_hf').onclick = function() {
    setRequest_default('dispatch.php?_module=Admin&_controller=index&_action=viewDialogHeaderFooter', 'hf');
};
document.getElementById('button_edit_hf').onclick = function() {
    var id = document.getElementById('id').value;
    setRequest_default('dispatch.php?_module=Admin&_controller=index&_action=viewDialogHeaderFooter&id=' + id, 'hf');
};

function doIt_user(evt, userID) {
	getXY(evt, 'u');

	document.getElementById('userID').value = userID;
	document.getElementById('userID_del').value = userID;
	
	var selectedRow = (evt.target || evt.srcElement);
	while (selectedRow.tagName !== 'TR') {
		selectedRow = selectedRow.parentNode;
	}

	if (oldRow) {
		oldRow.style.backgroundColor = '';
		oldRow.style.color = '#000000';
		if (oldRow === selectedRow)
			return true;
	};
	
	selectedRow.style.backgroundColor = '#bfdaff';
	oldRow = selectedRow;
}
function doIt_clickProfil(evt, clickProfilID) {
	getXY(evt, 'c');

	document.getElementById('clickProfilID').value = clickProfilID;
	
	var selectedRow = (evt.target || evt.srcElement);
	while (selectedRow.tagName !== 'TR') {
		selectedRow = selectedRow.parentNode;
	}

	if (oldRow) {
		oldRow.style.backgroundColor = '';
		oldRow.style.color = '#000000';
		if (oldRow === selectedRow)
			return true;
	};
	
	selectedRow.style.backgroundColor = '#bfdaff';
	oldRow = selectedRow;
}
function doIt_dsd(evt, dsdID) {
	getXY(evt, 'dsd');

	document.getElementById('dsdID').value = dsdID;
	
	var selectedRow = (evt.target || evt.srcElement);
	while (selectedRow.tagName !== 'TR') {
		selectedRow = selectedRow.parentNode;
	}

	if (oldRow) {
		oldRow.style.backgroundColor = '';
		oldRow.style.color = '#000000';
		if (oldRow === selectedRow)
			return true;
	};
	
	selectedRow.style.backgroundColor = '#bfdaff';
	oldRow = selectedRow;
}

function doIt_dsdn(evt, dsdnID) {
	getXY(evt, 'dsdn');

	document.getElementById('dsdnID').value = dsdnID;
	
	var selectedRow = (evt.target || evt.srcElement);
	while (selectedRow.tagName !== 'TR') {
		selectedRow = selectedRow.parentNode;
	}

	if (oldRow) {
		oldRow.style.backgroundColor = '';
		oldRow.style.color = '#000000';
		if (oldRow === selectedRow)
			return true;
	};
	
	selectedRow.style.backgroundColor = '#bfdaff';
	oldRow = selectedRow;
}

function doIt_esp(evt, espID) {
    getXY(evt, 'esp');

    document.getElementById('espID').value = espID;
    
    var selectedRow = (evt.target || evt.srcElement);
    while (selectedRow.tagName !== 'TR') {
        selectedRow = selectedRow.parentNode;
    }

    if (oldRow) {
        oldRow.style.backgroundColor = '';
        oldRow.style.color = '#000000';
        if (oldRow === selectedRow)
            return true;
    };
    
    selectedRow.style.backgroundColor = '#bfdaff';
    oldRow = selectedRow;
}

function doIt_hf(evt, id) {
    getXY(evt, 'hf');

    document.getElementById('id').value = id;
    
    var selectedRow = (evt.target || evt.srcElement);
    while (selectedRow.tagName !== 'TR') {
        selectedRow = selectedRow.parentNode;
    }

    if (oldRow) {
        oldRow.style.backgroundColor = '';
        oldRow.style.color = '#000000';
        if (oldRow === selectedRow)
            return true;
    };
    
    selectedRow.style.backgroundColor = '#bfdaff';
    oldRow = selectedRow;
}

