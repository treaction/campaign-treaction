function initAdmin() {
    var handleSubmit_user = function () {
        this.submit();

        setRequest('admin/user/index.php', 'admin_user');
    };

    var handleSubmit_clickProfil = function () {
        this.submit();

        setRequest('dispatch.php?_module=Admin&_controller=index&_action=viewClickProfil', 'admin_clickProfil');
    };

    var handleSubmit_dsd = function () {
        this.submit();

        setRequest('dispatch.php?_module=Admin&_controller=index&_action=viewDeliverySystemDistributor', 'admin_dsd');
    };

    var handleSubmit_dsdn = function () {
        this.submit();

        setRequest('dispatch.php?_module=Admin&_controller=index&_action=viewDeliverySystemDomain', 'admin_dsdn');
    };

    var handleSubmit_esp = function () {
        this.submit();

        setRequest('dispatch.php?_module=Admin&_controller=index&_action=viewEspMapping', 'admin_esp');
    };

    var handleSubmit_hf = function () {
        this.submit();

        setRequest('dispatch.php?_module=Admin&_controller=index&_action=viewHeaderFooter', 'admin_hf');
    };

    YAHOO.example.container.container_admin_user = new YAHOO.widget.Dialog('container_admin_user', {
        width: '400px',
        fixedcenter: true,
        visible: false,
        modal: true,
        close: true,
        constraintoviewport: true,
        buttons: [{
                text: 'speichern',
                handler: handleSubmit_user,
                isDefault: true
            }, {
                text: 'abbrechen',
                handler: handleCancel_default
            }]
    });
    YAHOO.example.container.container_admin_user.validate = function () {
        var ufehler = '';

        var user_login = YAHOO.util.Dom.get('user_login').value;
        var user_pw = YAHOO.util.Dom.get('user_pw').value;
        var user_pw1 = YAHOO.util.Dom.get('user_pw1').value;
        var user_pw2 = YAHOO.util.Dom.get('user_pw2').value;

        if (user_login === '') {
            ufehler = "- Bitte Anmeldename angeben\n";
        }

        if (user_pw === ''
                && user_pw1 === ''
                && user_pw2 === ''
                ) {
            ufehler += "- Bitte Passwort angeben\n";
        }

        if (
                (
                        user_pw1 !== ''
                        || user_pw2 !== ''
                        ) && (
                user_pw1 !== user_pw2
                )
                ) {
            ufehler += "- Passwoerter stimmen nicht ueberein\n";
        }

        if (ufehler === '') {
            return true;
        } else {
            alert(ufehler);

            return false;
        }
    };
    YAHOO.example.container.container_admin_user.render();
    YAHOO.util.Event.addListener('button_edit_user', 'click', YAHOO.example.container.container_admin_user.show, YAHOO.example.container.container_admin_user, true);
    YAHOO.util.Event.addListener('button_neu_user', 'click', YAHOO.example.container.container_admin_user.show, YAHOO.example.container.container_admin_user, true);

    YAHOO.example.container.container_admin_user_del = new YAHOO.widget.Dialog('container_admin_user_del', {
        width: '300px',
        fixedcenter: true,
        visible: false,
        constraintoviewport: true,
        buttons: [{
                text: 'l&ouml;schen',
                handler: handleSubmit_user,
                isDefault: true
            }, {
                text: 'abbrechen',
                handler: handleCancel_default
            }]
    });
    YAHOO.example.container.container_admin_user_del.render();
    YAHOO.util.Event.addListener('button_del_user', 'click', YAHOO.example.container.container_admin_user_del.show, YAHOO.example.container.container_admin_user_del, true);

    YAHOO.example.container.container_admin_clickProfil = new YAHOO.widget.Dialog('container_admin_clickProfil', {
        width: '400px',
        fixedcenter: true,
        visible: false,
        modal: true,
        close: true,
        constraintoviewport: true,
        buttons: [{
                text: 'speichern',
                handler: handleSubmit_clickProfil,
                isDefault: true
            }, {
                text: 'abbrechen',
                handler: handleCancel_default
            }]
    });
    YAHOO.example.container.container_admin_clickProfil.validate = function () {
        var ufehler = '';

        var name = YAHOO.util.Dom.get('name').value;
        var shortcut = YAHOO.util.Dom.get('abkuerzung').value;

        if (name === ''
                ) {
            ufehler = "- Bitte Name angeben\n";
        }

        if (shortcut === ''
                ) {
            ufehler += "- Bitte Abkürzung angeben\n";
        }

        if (ufehler === '') {
            return true;
        } else {
            alert(ufehler);

            return false;
        }
    };
    YAHOO.example.container.container_admin_clickProfil.render();
    YAHOO.util.Event.addListener('button_neu_clickProfil', 'click', YAHOO.example.container.container_admin_clickProfil.show, YAHOO.example.container.container_admin_clickProfil, true);
    YAHOO.util.Event.addListener('button_edit_clickProfil', 'click', YAHOO.example.container.container_admin_clickProfil.show, YAHOO.example.container.container_admin_clickProfil, true);

    YAHOO.example.container.container_admin_dsdn = new YAHOO.widget.Dialog('container_admin_dsdn', {
        width: '400px',
        fixedcenter: true,
        visible: false,
        modal: true,
        close: true,
        constraintoviewport: true,
        buttons: [{
                text: 'speichern',
                handler: handleSubmit_dsdn,
                isDefault: true
            }, {
                text: 'abbrechen',
                handler: handleCancel_default
            }]
    });
    
        YAHOO.example.container.container_admin_dsdn.validate = function () {
        var ufehler = '';

        var name = YAHOO.util.Dom.get('domain').value;
        var deliverySystem = YAHOO.util.Dom.get('deliverySystem').value;

        if (name === ''
                ) {
            ufehler = "- Bitte Versanddomain angeben\n";
        }
        
        if (deliverySystem === ''
                ) {
            ufehler += "- Bitte Versandsystem ausw&auml;hlen\n";
        }

        if (ufehler === '') {
            return true;
        } else {
            alert(ufehler);

            return false;
        }
    };

        

    YAHOO.example.container.container_admin_dsdn.render();
    YAHOO.util.Event.addListener('button_neu_dsdn', 'click', YAHOO.example.container.container_admin_dsdn.show, YAHOO.example.container.container_admin_dsdn, true);
    YAHOO.util.Event.addListener('button_edit_dsdn', 'click', YAHOO.example.container.container_admin_dsdn.show, YAHOO.example.container.container_admin_dsdn, true); 

        YAHOO.example.container.container_admin_esp = new YAHOO.widget.Dialog('container_admin_esp', {
        width: '400px',
        fixedcenter: true,
        visible: false,
        modal: true,
        close: true,
        constraintoviewport: true,
        buttons: [{
            text: 'speichern',
            handler: handleSubmit_esp,
            isDefault: true
        }, {
            text: 'abbrechen',
            handler: handleCancel_default
        }]
    });

    YAHOO.example.container.container_admin_esp.validate = function () {
        var ufehler = '';
      
        var deliverySystem = YAHOO.util.Dom.get('deliverySystem').value;
        var unsubscribe_link = YAHOO.util.Dom.get('unsubscribe_link').value;
        var online_version = YAHOO.util.Dom.get('online_version').value;
        var email = YAHOO.util.Dom.get('email').value;
        var anrede = YAHOO.util.Dom.get('anrede').value;
        var vorname = YAHOO.util.Dom.get('vorname').value;
        var nachname = YAHOO.util.Dom.get('nachname').value;

        if (deliverySystem === ''
        ) {
            ufehler += "- Bitte Versandsystem auswählen\n";
        }

        if (unsubscribe_link === ''
        ) {
            ufehler = "- Das Feld Abmelder Link muss nicht leer sein\n";
        }
          if (online_version === ''
        ) {
            ufehler = "- Das Feld Online Version muss nicht leer sein\n";
        }
          if (email === ''
        ) {
            ufehler = "- Das Feld Email muss nicht leer sein\n";
        }
         if (anrede === ''
        ) {
            ufehler = "- Das Feld Anrede muss nicht leer sein\n";
        }
         if (vorname === ''
        ) {
            ufehler = "- Das Feld Vorname muss nicht leer sein\n";
        }
         if (nachname === ''
        ) {
            ufehler = "- Das Feld Nachname muss nicht leer sein\n";
        }
        
        if (ufehler === '') {
            return true;
        } else {
            alert(ufehler);

            return false;
        }
    };



    YAHOO.example.container.container_admin_esp.render();
    YAHOO.util.Event.addListener('button_neu_esp', 'click', YAHOO.example.container.container_admin_esp.show, YAHOO.example.container.container_admin_esp, true);
    YAHOO.util.Event.addListener('button_edit_esp', 'click', YAHOO.example.container.container_admin_esp.show, YAHOO.example.container.container_admin_esp, true);


        YAHOO.example.container.container_admin_hf = new YAHOO.widget.Dialog('container_admin_hf', {
        width: '400px',
        fixedcenter: true,
        visible: false,
        modal: true,
        close: true,
        constraintoviewport: true,
        buttons: [{
            text: 'speichern',
            handler: handleSubmit_hf,
            isDefault: true
        }, {
            text: 'abbrechen',
            handler: handleCancel_default
        }]
    });

    YAHOO.example.container.container_admin_hf.validate = function () {
        var ufehler = '';
      
        var deliverySystem = YAHOO.util.Dom.get('deliverySystem').value;
        var header = YAHOO.util.Dom.get('header').value;
        var footer = YAHOO.util.Dom.get('footer').value;


        if (deliverySystem === ''
        ) {
            ufehler += "- Bitte Versandsystem auswählen\n";
        }

        if (header === ''
        ) {
            ufehler = "- Das Feld Header Link muss nicht leer sein\n";
        }
          if (footer === ''
        ) {
            ufehler = "- Das Feld Footer muss nicht leer sein\n";
        }
             
        if (ufehler === '') {
            return true;
        } else {
            alert(ufehler);

            return false;
        }
    };



    YAHOO.example.container.container_admin_hf.render();
    YAHOO.util.Event.addListener('button_neu_hf', 'click', YAHOO.example.container.container_admin_hf.show, YAHOO.example.container.container_admin_hf, true);
    YAHOO.util.Event.addListener('button_edit_hf', 'click', YAHOO.example.container.container_admin_hf.show, YAHOO.example.container.container_admin_hf, true);

    YAHOO.example.container.container_admin_dsd = new YAHOO.widget.Dialog('container_admin_dsd', {
        width: '400px',
        fixedcenter: true,
        visible: false,
        modal: true,
        close: true,
        constraintoviewport: true,
        buttons: [{
                text: 'speichern',
                handler: handleSubmit_dsd,
                isDefault: true
            }, {
                text: 'abbrechen',
                handler: handleCancel_default
            }]
    });
    YAHOO.example.container.container_admin_dsd.validate = function () {
        var ufehler = '';

        var name = YAHOO.util.Dom.get('name').value;
        var description = YAHOO.util.Dom.get('description').value;
        var deliverySystem = YAHOO.util.Dom.get('deliverySystem').value;
        var externe_id = YAHOO.util.Dom.get('externe_id').value;

        if (name === ''
                ) {
            ufehler = "- Bitte Name angeben\n";
        }

        if (description === ''
                ) {
            ufehler += "- Bitte Beschreibung angeben\n";
        }

        if (deliverySystem === ''
                ) {
            ufehler += "- Bitte Versandsystem auswählen\n";
        }

        if (externe_id === ''
                ) {
            ufehler += "- Bitte Externe ID angeben\nExternal ID pflegt der Mitarbeiter die Kennung aus Optivo oder Kajomi ein";
        }

        if (ufehler === '') {
            return true;
        } else {
            alert(ufehler);

            return false;
        }
    };
    YAHOO.example.container.container_admin_dsd.render();
    YAHOO.util.Event.addListener('button_neu_dsd', 'click', YAHOO.example.container.container_admin_dsd.show, YAHOO.example.container.container_admin_dsd, true);
    YAHOO.util.Event.addListener('button_edit_dsd', 'click', YAHOO.example.container.container_admin_dsd.show, YAHOO.example.container.container_admin_dsd, true);

}
YAHOO.util.Event.addListener(window, 'load', initAdmin);