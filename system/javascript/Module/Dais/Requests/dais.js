function setRequestDais(url) {
	request = createRequest();

	// überprüfen, ob Request erzeugt wurde
	if (!request) {
		alert('Kann keine XMLHTTP-Instanz erzeugen');
		
		return false;
	} else {
		document.getElementById('loaderimg').style.visibility = 'visible';
		document.getElementById('loaderimg').innerHTML = '<img src="img/k_loader.gif" />';

		request.open('POST', url, true);
		// Requestheader senden
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		// Request senden
		request.send();
		// Request auswerten

		request.onreadystatechange = interpretRequestDais;

	}
}

// Request auswerten
function interpretRequestDais() {
	switch (request.readyState) {
		// wenn der readyState 4 und der request.status 200 ist, dann ist alles korrekt gelaufen
		case 4:
			if (request.status !== 200) {
				document.getElementById('standard').innerHTML = 'Fehler: ' + request.status + ' ' + request.statusText;
			} else {
				document.getElementById('bottom').innerHTML = '';
				document.getElementById('loaderimg').style.visibility = 'hidden';
				
				var contentd = request.responseText;
				contentd = '<iframe src="dais/index2.php" width="100%" height="700" style="border:0px;"></iframe>';

				// den Inhalt des Requests in das <div> schreiben
				document.getElementById('standard').innerHTML = '<div style="width:100%;height:100%">' + contentd + '</div>';
				
				evalScript(contentd);
			}
			break;
			
		default:
			break;
	}

}