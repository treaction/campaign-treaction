function setRequest_bottom(url, p) {
	request = createRequest();
    
    // überprüfen, ob Request erzeugt wurde
    if (!request) {
        alert('Kann keine XMLHTTP-Instanz erzeugen');
        
        return false;
    } else {
		request.open('POST', url, true);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send();
		request.onreadystatechange = interpretRequest_bottom;
	}
}

// Request auswerten
function interpretRequest_bottom() {
	switch (request.readyState) {
		// wenn der readyState 4 und der request.status 200 ist, dann ist alles korrekt gelaufen
		case 4:
			if (request.status !== 200) {
				document.getElementById('infocontent').innerHTML = 'Fehler: ' + request.status + ' ' + request.statusText;
			} else {
				document.getElementById('loaderimg').style.visibility = 'hidden';
				
				var contentb = request.responseText;
				
				// den Inhalt des Requests in das <div> schreiben
				document.getElementById('infocontent').innerHTML = contentb;
				
				evalScript(contentb);

			}
			break;
			
		default:
			break;
	}

}