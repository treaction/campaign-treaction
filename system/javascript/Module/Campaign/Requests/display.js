function setRequestAnzeige(url) {
	request = createRequest();

	// überprüfen, ob Request erzeugt wurde
	if (!request) {
		alert('Kann keine XMLHTTP-Instanz erzeugen');
		
		return false;
	} else {
		request.open('POST', url, true);
		// Requestheader senden
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		// Request senden
		request.send();
		// Request auswerten
		request.onreadystatechange = interpretRequestAnzeige;
	}
}

// Request auswerten
function interpretRequestAnzeige() {
	switch (request.readyState) {
		// wenn der readyState 4 und der request.status 200 ist, dann ist alles korrekt gelaufen
		case 4:
			if (request.status !== 200) {
				document.getElementById('anzeige_config').innerHTML = 'Fehler: ' + request.status + ' ' + request.statusText;
			} else {
				var contentA = request.responseText;
				
				// den Inhalt des Requests in das <div> schreiben
				document.getElementById('anzeige_config').innerHTML = contentA;
				
				evalScript(contentA);
			}
			break;
			
		default:
			break;
	}

}