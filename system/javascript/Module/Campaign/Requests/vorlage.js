function setRequest_vorlage(c) {
	request = createRequest();
    
    // überprüfen, ob Request erzeugt wurde
    if (!request) {
        alert('Kann keine XMLHTTP-Instanz erzeugen');
        
        return false;
    } else {
		var url = '',
			vorlage_text = '',
			b_mail = '',
			b_betreff = '',
			buchhaltung = '',
			Report = '',
			kid = '',
			rep_typ = ''
		;
		
		switch (c) {
			case 0:
				vorlage_text = CKEDITOR.instances.vorlage_text0.getData();
				url = 'kampagne/report/rep_save.php';
				break;
				
			case 1:
				vorlage_text = CKEDITOR.instances.vorlage_text1.getData();
				url = 'kampagne/report/rep_save.php';
				break;
				
			case 2:
				vorlage_text = CKEDITOR.instances.vorlage_text2.getData();
				url = 'limport/report/rep_save.php';
				break;
				
			case 3:
				vorlage_text = CKEDITOR.instances.vorlage_text3.getData();
				url = 'limport/report/rep_save.php';
				break;
		}

		vorlage_text = str_replace('&', '_x_', vorlage_text);
		vorlage_text = str_replace('+', '_p_', vorlage_text);
		
		var vorlage_betreff = document.getElementById('vorlage_betreff').value;

		if (document.getElementById('buchhaltungmail')) {
			if (document.getElementById('buchhaltungmail').checked === true) {
				b_mail = document.getElementById('vorlage_buchhaltungmail').value;
				b_betreff = document.getElementById('vorlage_buchhaltungmail_betreff').value;
				buchhaltung = 'b';
			}
		}

		var testEmail = document.getElementById('testEmail').value;

		if (document.getElementById('Report')) {
			Report = document.getElementById('Report').value;
			kid = document.getElementById('Report_kid').value;
			
			if (document.getElementById('rep_typ')) {
				rep_typ = document.getElementById('rep_typ').value;
			}
		}

		request.open('POST', url, true);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send('vorlage_betreff=' + vorlage_betreff + '&vorlage_text=' + vorlage_text + '&email=' + testEmail + '&check=' + c + '&report=' + Report + '&repTyp=' + rep_typ + '&kid=' + kid + '&buchhaltungmail=' + buchhaltung + '&b_mail=' + b_mail + '&b_betreff=' + b_betreff);
		request.onreadystatechange = function() {
			interpretRequest_vorlage(testEmail, c, Report);
		};

	}
}

// Request auswerten
function interpretRequest_vorlage(testEmail, check, Report) {
	switch (request.readyState) {
		// wenn der readyState 4 und der request.status 200 ist, dann ist alles korrekt gelaufen
		case 4:
			if (request.status !== 200) {
				document.getElementById('vorlage_content').innerHTML = 'Fehler: ' + request.status + ' ' + request.statusText;
			} else {
				var content_vorlage = request.responseText;
				
				switch (check) {
					case 0:
						if (testEmail === '') {
							document.getElementById('vorlage_content').innerHTML = "<div style=padding:25px;width:80%;background-color:#FFFFFF'><img src='img/Tango/22/actions/dialog-apply.png' style='margin-bottom:-4px' /> Vorlage gespeichert<br /><br /><input type='button' class='positive' value='zur&uuml;ck' onclick='setRequest_default(\"kampagne/report/rep_vorlage.php\",\"rep_vorlage_content\");' /></div>";
						} else {
							document.getElementById('vorlage_content').innerHTML = "<div style='padding:25px;width:80%;background-color:#FFFFFF'><img src='img/Tango/22/actions/dialog-apply.png' style='margin-bottom:-4px' /> Testmail versendet<br /><br /><input type='button' class='positive' value='zur&uuml;ck' onclick='setRequest_default(\"kampagne/report/rep_vorlage.php\",\"rep_vorlage_content\");' /></div>";
						}
						break;
						
					case 1:
						if (Report === 1) {
							document.getElementById('vorlage_content').innerHTML = "<div style='padding:25px;width:80%;background-color:#FFFFFF'><img src='img/Tango/22/actions/dialog-apply.png' style='margin-bottom:-4px' /> Reporting versendet<br /><br /></div>";
							document.getElementById('report_senden_now').style.display = 'none';
						} else {
							document.getElementById('vorlage_content').innerHTML = "<div style='padding:25px;width:80%;background-color:#FFFFFF'><img src='img/Tango/22/actions/dialog-apply.png' style='margin-bottom:-4px' /> Testmail versendet<br /><br /></div>";
							document.getElementById('testmail_senden').style.display = 'none';
						}

						if (document.getElementById('vorlage_buttons')) {
							document.getElementById('vorlage_buttons').innerHTML = '';
						}
						break;
						
					case 2:
						if (testEmail === '') {
							document.getElementById('vorlage_content').innerHTML = "<div style='padding:25px;width:80%;background-color:#FFFFFF'><img src='img/Tango/22/actions/dialog-apply.png' style='margin-bottom:-4px' /> Vorlage gespeichert<br /><br /><input type='button' class='positive' value='zur&uuml;ck' onclick='setRequest_default(\"limport/report/rep_vorlage.php\",\"global_layout\");' /></div>";
						} else {
							document.getElementById('vorlage_content').innerHTML = "<div style='padding:25px;width:80%;background-color:#FFFFFF'><img src='img/Tango/22/actions/dialog-apply.png' style='margin-bottom:-4px' /> Testmail versendet<br /><br /><input type='button' class='positive' value='zur&uuml;ck' onclick='setRequest_default(\"limport/report/rep_vorlage.php\",\"global_layout\");' /></div>";
						}
						break;
						
					case 3:
						if (Report === 1) {
							document.getElementById('vorlage_content').innerHTML = "<div style='padding:25px;width:80%;background-color:#FFFFFF'><img src='img/Tango/22/actions/dialog-apply.png' style='margin-bottom:-4px' /> Reporting versendet<br /><br /></div>";
							document.getElementById('report_senden_now').style.display = 'none';
						} else {
							document.getElementById('vorlage_content').innerHTML = "<div style='padding:25px;width:80%;background-color:#FFFFFF'><img src='img/Tango/22/actions/dialog-apply.png' style='margin-bottom:-4px' /> Testmail versendet<br /><br /></div>";
							document.getElementById('testmail_senden').style.display = 'none';
						}
						break;
				}
			}
			break;
			
		default:
			break;
	}
}