function objectToArray(jsonData, parentKey) {
	var resultData = [];
	for (var key in jsonData) {
		var arrayKey;
		var item = jsonData[key];
		
		if (parentKey.length > 0) {
			arrayKey = parentKey + '[' + key + ']';
		} else {
			arrayKey = key;
		}
		
		if (typeof item === 'object') {
			resultData.push(
				objectToArray(
					item,
					arrayKey
				)
			);
		} else {
			resultData.push(arrayKey + '=' + item);
		}
	}
	
	return resultData;
}

function objectToUrlParameter(jsonData) {
	var resultData = objectToArray(jsonData, '');
	
	return resultData.join('&');
}

function buildCampaignRequestUrlWidthParameter(controller, action, additionalParamaterData) {
	return 'dispatch.php?' 
		+ '_module=Campaign' 
		+ '&_controller=' + controller 
		+ '&_action=' + action 
		+ '&' + objectToUrlParameter(additionalParamaterData)
	;
}

function processDeleteCampaign(u_rechte, kid, status, mailid, v_system, campaignType, k_name) {
    var vSystemInfo = '';
    
    if (mailid !== '') {
        vSystemInfo = '<br /><br />Bitte beachten Sie, dass dann das zugeh&ouml;rige Mailing in Versandsystem("' + v_system + '") ebenfalls gel&ouml;scht wird!';
    }
    
    switch (campaignType) {
        case 'HV':
            processAjaxRequest(
                'AjaxRequests/ajax.php',
                'actionMethod=campaign' 
					+ '&actionType=getNvCountsByCampaignId' 
					+ '&campaignId=' + parseInt(kid)
				,
                '',
                function (campaignCount) {
                    if (campaignCount > 0) {
                        vSystemInfo += '<br /><br /><span class="alertInfo">Achtung:<br />Sie l&ouml;schen eine Hauptkampagne inkl. ' + campaignCount + ' Nachversendung/en</span>';
                    }
                    
                    document.getElementById('mailing').innerHTML = k_name + vSystemInfo;
                }
            );
            break;
            
        default:
            document.getElementById('mailing').innerHTML = k_name + vSystemInfo;
            break;
    }
    
    document.getElementById('noRightDelMailing').innerHTML = k_name;
    
    if (status < 20 
		&& status !== 5
	) {
        document.getElementById('button_noRightDel_k').style.display = 'none';
        
        document.getElementById('button_del_k').style.display = '';
        document.getElementById('del_k_id').value = parseInt(kid);
    } else {
        switch (u_rechte) {
            case 100:
                document.getElementById('button_noRightDel_k').style.display = 'none';
                
                document.getElementById('button_del_k').style.display = '';
                document.getElementById('del_k_id').value = parseInt(kid);
                break;
                
            default:
                document.getElementById('button_noRightDel_k').style.display = '';
                
                document.getElementById('button_del_k').style.display = 'none';
                break;
        }
    }
}

function processButtonCampaignSync(deliverySystem, site, campaignId) {
	document.getElementById('button_sync_k').style.display = 'none';
	
	if (site === 'main') {
		switch (deliverySystem) {
			case 'BM':
			case 'K':
				document.getElementById('button_sync_k').style.display = '';
				document.getElementById('button_sync_k').onclick = function() {
					setRequest_default(
						'Module/Campaigns/View/Campaign/campaignSync.php?campaign[k_id]=' + parseInt(campaignId),
						'sync_frame'
					);
				};
				break;
		}
	}
}

function processButtonTestmailSelbst(deliverySystem, mailId, mandant, campaignId) {
	document.getElementById('button_testmail_selbst').style.display = 'none';
	
	if (mailId.length > 0) {
		switch (deliverySystem) {
                        case 'BM':
			case 'M':
                        case '4W18':
                        case '4WMS':
                        case 'S18':
                        case 'SB':
                        case 'SS':
                        case '4WB':
                        case '4WBL':
                        case 'SBL':
                        case 'SRES':
                        case '4WE':
                        case 'LWB':
                        case 'BL19':
                        case 'BLB':
                        case 'FM19':
                        case 'FMB':
                        case 'BLE':
                        case 'FME':
                        case 'BLBL':
                        case 'FMBL':
                        case 'PM':
                        case 'CEOO':
                        case 'CEOOBL':    
                            document.getElementById('button_testmail_selbst').style.display = '';
				document.getElementById('button_testmail_selbst').onclick = function() {
					setRequestK(
				                  buildCampaignRequestUrlWidthParameter(
					               'index',
					                'SendTestMail',
					              {    
                                                          'mailingId': parseInt(mailId),    
                                                          'campaign':{						                         
							                 'k_id': parseInt(campaignId) 
                                                                   } 
					              }
				    )
			);
				};
				break;
                        case 'SE':
                        case 'SSE':
                        case '4WSE':
                        case 'CSE':    
                            document.getElementById('button_testmail_selbst').style.display = '';
				document.getElementById('button_testmail_selbst').onclick = function() {
					setRequestK(
				                  buildCampaignRequestUrlWidthParameter(
					               'index',
					                'SendTestMail',
					              {    
                                                          'mailingId': mailId,    
                                                          'campaign':{						                         
							                 'k_id': parseInt(campaignId) 
                                                                   } 
					              }
				    )
			);
				};
				break;        
		}
    }
}

function processButtonTestmail(deliverySystem, mailId, mandant, campaignId) {
	document.getElementById('button_testmail').style.display = 'none';

	if (mailId.length > 0) {
		switch (deliverySystem) {
			case 'BM':
                        case 'M':
                        case '4W18':
                        case '4WMS':
                        case 'S18':
                        case 'SB':
                        case 'SS':
                        case '4WB':
                        case '4WBL':
                        case 'SBL':    
                        case 'SSE':
                        case '4WSE': 
                        case 'SRES':
                        case '4WE':    
                        case 'LWB':
                        case 'BL19':
                        case 'BLB':
                        case 'FM19':
                        case 'FMB':
                        case 'BLE':
                        case 'FME':
                        case 'BLBL':
                        case 'FMBL':
                        case 'PM': 
                        case 'CEOO':
                        case 'CEOOBL':    
				document.getElementById('button_testmail').style.display = '';
				document.getElementById('button_testmail').onclick = function() {
					setRequest_default(
						'Module/Campaigns/View/Campaign/campaignTestmail.php?campaign[k_id]=' + parseInt(campaignId),
						'testmail_frame'
					);
				};
				break;
                        case 'SE':
                        case 'SSE':
                        case '4WSE':
                        case 'CSE':    
				document.getElementById('button_testmail').style.display = '';
				document.getElementById('button_testmail').onclick = function() {
					setRequest_default(
						'Module/Campaigns/View/Campaign/campaignTestmail.php?campaign[k_id]=' + parseInt(campaignId),
						'testmail_frame'
					);
				};
				break;        
                                
		}
    }
}
function processButtonUpdate(campaignId) {
	document.getElementById('button_update').style.display = '';
	document.getElementById('button_update').onclick = function() {
		document.getElementById('yui-gen28-button').style.display = 'none';
		document.getElementById('update_frame').innerHTML = "<img src='img/ajax-loader.gif' />";
		
		processAjaxRequest(
			'AjaxRequests/ajax.php',
			'actionMethod=campaign' 
				+ '&actionType=updateCampaignDataWidthDeliverySystem' 
				+ '&campaign[kid]=' + parseInt(campaignId),
			'',
			function (contentResult) {
				document.getElementById('yui-gen28-button').style.display = 'block';
				document.getElementById('update_frame').innerHTML = contentResult;
			}
		);
	};
}

function HighLightTR(origColor, kid, h) {
}

function loadNew() {
    YAHOO.util.Event.onDOMReady(function() {
        var inTxt = YAHOO.util.Dom.get('in'), 
            outTxt = YAHOO.util.Dom.get('out'), 
            inDate, 
            outDate, 
            interval
        ;
        
        inTxt.value = '';
        outTxt.value = '';
        
        var cal = new YAHOO.example.calendar.IntervalCalendar('cal1Container', {
            START_WEEKDAY: 1,
            pages: 3
        });
        
        cal.cfg.setProperty('WEEKDAYS_SHORT', ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']);
        cal.cfg.setProperty('MONTHS_LONG', ['Januar', 'Februar', "M\u00E4rz", 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']);
        
        cal.selectEvent.subscribe(function() {
            interval = this.getInterval();

            if (interval.length === 2) {
                inDate = interval[0];
                inTxt.value = inDate.getFullYear() + '-' + (inDate.getMonth() + 1) + '-' + inDate.getDate();

                if (interval[0].getTime() !== interval[1].getTime()) {
                    outDate = interval[1];
                    outTxt.value = outDate.getFullYear() + '-' +(outDate.getMonth() + 1) + '-' + outDate.getDate();
                }
            }
        }, cal, true);
        
        cal.deselectEvent.subscribe(function() {
            this._intervalOnDeselect();
        }, cal, true);
        
        cal.render();
    });
    
    setRequest('kampagne/k_main.php?new=2', 'kSearch');
}

function doIt(evt, mailtyp, mailid, k_name, kid, hauptmail, status, agentur, kamp_name, k_datum, k_zeit, gebucht, vorgabe_o, vorgabe_k, bl, v_system, bearbeiter, tkp, ap, note, ou, o, ku, k, versendet, site, u_rechte, mandant) {
    getXY(evt, 'k');
	
	var oldRow, selectedRow;
    
    document.getElementById('k_new').innerHTML = "<div style='height:450px'></div>";
    
    document.nv_calc.reset();
    
    document.getElementById('calc_gebucht').innerHTML = Trenner(gebucht);
    document.getElementById('calc_versendet').innerHTML = Trenner(versendet);
    
	// processDeleteCampaign
    processDeleteCampaign(parseInt(u_rechte), parseInt(kid), parseInt(status), mailid, v_system, hauptmail, k_name);
    
    document.getElementById('button_versender_details').onclick = function() {
        setRequest_default(
			'Module/Campaigns/View/Campaign/getVersenderinfo.php?campaign[k_id]=' + parseInt(kid),
			'versenderinfo_div'
		);
        
        YAHOO.example.container.container_versenderinfo.show();
    };
	
	// processButtonCampaignSync
    processButtonCampaignSync(v_system, site, parseInt(kid));
    
    document.getElementById('status_aendern').style.display = '';
    document.getElementById('status_aendern').onclick = function() {
		setRequest_ajax(
			'Module/Campaigns/View/Campaign/changeStatus.php?campaign[k_id]=' + parseInt(kid),
			'status_new'
		);
    };
    
    document.getElementById('date_aendern').style.display = '';
    document.getElementById('date_aendern').onclick = function() {
        setRequest_ajax(
            'Module/Campaigns/View/Campaign/changeDate.php?campaign[k_id]=' + parseInt(kid),
            'date_new'
        );
    };

    document.getElementById('button_details').onclick = function() {
        setRequest_bottom('kampagne/bottom_details.php?kid=' + parseInt(kid) + '&hauptmail=' + hauptmail);
    };
    
	// processButtonTestmail
	processButtonTestmail(v_system, mailid, mandant, parseInt(kid));
	
	// processButtonTestmailSelbst
	processButtonTestmailSelbst(v_system, mailid, mandant, parseInt(kid));
    
    if (mailid !== '') {
        processButtonUpdate(parseInt(kid));
    } else {
		document.getElementById('button_update').style.display = 'none';
	}
    
	document.getElementById('button_prognose').style.display = 'none';
	document.getElementById('button_createInVs').style.display = 'none';
	document.getElementById('button_report_kontext').style.display = 'none';
	document.getElementById('special').style.display = 'none';
	
    if (hauptmail === 'HV') {
		if (site === 'main') {
            document.getElementById('button_add_k').style.display = '';
            document.getElementById('button_copy').style.display = '';
        }
		
		if (status === 5 
			|| (
				status > 19 
				&& status !== 22
			)
		) {
            document.getElementById('button_report_kontext').style.display = '';
            document.getElementById('button_report_kontext').onclick = function() {
                document.getElementById('rep_ext').innerHTML = "<span style='display:none' id='" + parseInt(kid) + "_st'></span>";
                
                setRequest_default('kampagne/report/rep_details.php?kid=' + parseInt(kid) + '&campaign[oldStatus]=' + parseInt(status), 'rep_details_content');
            };
        }
        
        if (status < 20 
			&& status !== 10
		) {
            document.getElementById('button_prognose').style.display = '';
            document.getElementById('button_prognose').onclick = function() {
				setRequest_default('kampagne/prognose.php?kid=' + parseInt(kid), 'prognosecontent');
				
                YAHOO.example.container.container_prognose.show();
            };

        }
		
		if (status < 20 
			&& status !== 5
		) {
            document.getElementById('button_createInVs').style.display = '';
            document.getElementById('button_createInVs').onclick = function() {
                setRequest_default('kampagne/create_in_vs.php?kid=' + parseInt(kid), 'k_vs');
            };
		}
        
        document.getElementById('button_edit_k').style.display = '';
		document.getElementById('button_edit_k').onclick = function() {
            document.getElementById('k_hd').innerHTML = 'Kampagne bearbeiten';
			
			setRequestK(
				buildCampaignRequestUrlWidthParameter(
					'index',
					'edit',
					{
						'mailing': 'HV',
						'campaign': {
							'k_id': parseInt(kid) 
						}
					}
				)
			);
        };
		YAHOO.util.Event.addListener('button_edit_k', 'click', YAHOO.example.container.dialog1.show, YAHOO.example.container.dialog1, true);
		
		document.getElementById('button_edit_nv').style.display = 'none';
        
        document.getElementById('button_copy').onclick = function() {
            document.getElementById('k_hd').innerHTML = 'Kampagne kopieren';
            
			setRequestK(
				buildCampaignRequestUrlWidthParameter(
					'index',
					'copy',
					{
						'mailing': 'HV',
						'campaign': {
							'k_id': parseInt(kid) 
						}
					}
				)
			);
        };
		YAHOO.util.Event.addListener('button_copy', 'click', YAHOO.example.container.dialog1.show, YAHOO.example.container.dialog1, true);
        
		document.getElementById('button_empfehlung').style.display = '';
        document.getElementById('button_empfehlung').onclick = function() {
            setRequest_default('kampagne/nv_calc.php?kid=' + parseInt(kid), 'nv_calc_content');
        };
        
        document.getElementById('button_add_k').onclick = function() {
            document.getElementById('k_hd').innerHTML = 'Nachversand / Split hinzuf&uuml;gen';
            
			setRequestK(
				buildCampaignRequestUrlWidthParameter(
					'index',
					'addNv',
					{
						'mailing': 'NV',
						'campaign': {
							'k_id': parseInt(kid) 
						}
					}
				)
			);
        };
        
		document.getElementById('special').style.display = '';
    }
    
    if (hauptmail === 'NV') {
		var hvCampaignId = parseInt(document.getElementById('snv' + kid).getAttribute('data-hvCampaignId'));
		var currentCampaignId = parseInt(document.getElementById('snv' + kid).getAttribute('data-currentCampaignId'));
		
		document.getElementById('button_edit_k').style.display = 'none';
		
		document.getElementById('button_edit_nv').style.display = '';
        document.getElementById('button_edit_nv').onclick = function() {
            document.getElementById('k_hd').innerHTML = 'Kampagne bearbeiten';
            
			setRequestK(
				buildCampaignRequestUrlWidthParameter(
					'index',
					'edit',
					{
						'mailing': 'NV',
						'campaign': {
							'k_id': parseInt(kid) 
						}
					}
				)
			);
        };
        YAHOO.util.Event.addListener('button_edit_nv', 'click', YAHOO.example.container.dialog1.show, YAHOO.example.container.dialog1, true);
		
		document.getElementById('button_copy').style.display = 'none';
		document.getElementById('button_empfehlung').style.display = 'none';
		
		document.getElementById('button_add_k').style.display = '';
		document.getElementById('button_add_k').onclick = function() {
            document.getElementById('k_hd').innerHTML = 'Nachversand / Split hinzuf&uuml;gen';
            
			setRequestK(
				buildCampaignRequestUrlWidthParameter(
					'index',
					'addNv',
					{
						'mailing': 'NV',
						'hvCampaignId': hvCampaignId,
						'currentCampaignId': currentCampaignId,
						'campaign': {
							'k_id': parseInt(kid) 
						}
					}
				)
			);
        };
		
		//if (v_system === 'M' | v_system === 'SE' ) {
			document.getElementById('special').style.display = '';
		//}
    }
    
    selectedRow = (evt.target || evt.srcElement);
    while (selectedRow.tagName !== 'TR') {
        selectedRow = selectedRow.parentNode;
    }
    
    if (oldRow) { 
        oldRow.style.backgroundColor = '';
        if (oldRow === selectedRow) {
            oldRow = null;
            
            return true;
        }
    }
    
    oldRow = selectedRow;
}


document.getElementById('k_analyse_button').onclick = function() {
    setRequest_k_st('start');
};

if (document.getElementById('Analyse')) {
    document.getElementById('Analyse').onclick = function() {
        setRequest_k_st();
    };
}

document.getElementById('test_adressen_button').onclick = function() {
   
    
};

document.getElementById('neu_k_button').onclick = function() {
    document.getElementById('hd_sublabel').innerHTML = '';
    document.getElementById('k_new').innerHTML = "<div style='height:450px'></div>";
    document.getElementById('k_hd').innerHTML = 'Neue Kampagne anlegen';
    
	setRequestK(
		buildCampaignRequestUrlWidthParameter(
			'index',
			'new',
			{
				'mailing': 'HV'
			}
		)
	);
};

document.getElementById('neu_k_button_planer').onclick = function() {
    document.getElementById('hd_sublabel').innerHTML = '';
    document.getElementById('k_new').innerHTML = "<div style='height:450px'></div>";
    document.getElementById('k_hd').innerHTML = 'Neue Kampagne anlegen';
    
	setRequestK(
		buildCampaignRequestUrlWidthParameter(
			'index',
			'new',
			{
				'mailing': 'HV'
			}
		)
	);
};

document.getElementById('tab_config').onclick = function() {
    setRequestAnzeige('Module/Campaigns/View/Campaign/previewCampaignFields.php');
};

document.getElementById('s_pic').onclick = function() {
    var p = document.getElementById('page').value;
    
    setRequest(p, 'k');
};

document.getElementById('button_neu_cm_kunde').onclick = function() {
    YAHOO.example.container.container_kunde.show();
    
    setRequest_default('kampagne/View/Customers/Customer/createOrEditData.php', 'kunde_content');
};