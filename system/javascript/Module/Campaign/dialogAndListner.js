function validateCustomerData(prefixId) {
	var countryId = parseInt(YAHOO.util.Dom.get(prefixId + 'CountryId').value);
	var customerErrorMessage = '';

	if (YAHOO.util.Dom.get(prefixId + 'Company').value === '' 
		|| !validateStrLengthData(YAHOO.util.Dom.get(prefixId + 'Company').value, 3)
	) {
		customerErrorMessage += "- Bitte Firmennamen angeben\n";
	}

	if (YAHOO.util.Dom.get(prefixId + 'Street').value === '' 
		|| !validateStrLengthData(YAHOO.util.Dom.get(prefixId + 'Street').value, 3)
	) {
		customerErrorMessage += "- Bitte Strasse und Hausnr. angeben\n";
	}

	if (YAHOO.util.Dom.get(prefixId + 'Zip').value === '') {
		customerErrorMessage += "- Bitte Plz angeben\n";
	}

	if (YAHOO.util.Dom.get(prefixId + 'City').value === '' 
		|| !validateStrLengthData(YAHOO.util.Dom.get(prefixId + 'City').value, 3)
	) {
		customerErrorMessage += "- Bitte Ort angeben\n";
	}

	if (countryId === 0
		|| isNaN(countryId)
	) {
		customerErrorMessage += "- Bitte Land angeben\n";
	} else {
		if (countryId !== 54 
			&& countryId !== 41
		) {
			// nur bei außland bei de, oder ch (54, 41) nicht
			if (!validateStrLengthData(YAHOO.util.Dom.get(prefixId + 'Zip').value, 3)) {
				customerErrorMessage += "- Plz ist falsch\n";
			}
			
			if (YAHOO.util.Dom.get(prefixId + 'VatNumber').value === '' 
				|| !validateStrLengthData(YAHOO.util.Dom.get(prefixId + 'VatNumber').value, 3)
			) {
				customerErrorMessage += "- Bitte USt-IdNr. (Vat.Nr.) angeben\n";
			}
		} else if (countryId === 54) {
			// nur bei de
			if (!validateDeZipCode(YAHOO.util.Dom.get(prefixId + 'Zip').value)) {
				customerErrorMessage += "- Plz ist falsch\n";
			}
		}
	}
	
	return customerErrorMessage;
}

function validateContactPersonData(prefixId) {
	var customerPersonErrorMessage = '';

	if (YAHOO.util.Dom.get(prefixId + 'PersonFirstname').value === '' 
		|| !validateStrLengthData(YAHOO.util.Dom.get(prefixId + 'PersonFirstname').value, 3)
	) {
		customerPersonErrorMessage += "- Bitte Vorname angeben\n";
	}

	if (YAHOO.util.Dom.get(prefixId + 'PersonLastname').value === '' 
		|| !validateStrLengthData(YAHOO.util.Dom.get(prefixId + 'PersonLastname').value, 3)
	) {
		customerPersonErrorMessage += "- Bitte Nachname angeben\n";
	}

	if (YAHOO.util.Dom.get(prefixId + 'PersonEmail').value === '') {
		customerPersonErrorMessage += "- Bitte Email angeben\n";
	} else if (!validateEmail(YAHOO.util.Dom.get(prefixId + 'PersonEmail').value)) {
		customerPersonErrorMessage += "- Email Addresse ist falsch\n";
	}

	if (isNaN(parseInt(YAHOO.util.Dom.get(prefixId + 'PersonVertriebler').value))) {
		customerPersonErrorMessage += "- Bitte Vertriebler auswaehlen\n";
	}
	
	return customerPersonErrorMessage;
}

function validateTestmailData() {
    var campaignErrorMessage = '';
    
    if (YAHOO.util.Dom.get('personFirstname').value === '') {
		campaignErrorMessage += "- Vorname fehlt\n";
	}
        
    if (YAHOO.util.Dom.get('personLastname').value === '') {
		campaignErrorMessage += "- Nachname fehlt\n";
	}
        
    if (YAHOO.util.Dom.get('personEmail').value === '') {
		campaignErrorMessage += "- Email fehlt\n";
	}else if (!validateEmail(YAHOO.util.Dom.get('personEmail').value)) {
		campaignErrorMessage += "- Email Addresse ist falsch\n";
	}
        
        return campaignErrorMessage;
}

function validateCampaignData() {
	var campaignErrorMessage = '';
        var sendVolumen = [
                100000,
                150000,
                200000,
                250000,
                300000,
                350000,
                400000,
                500000,
                1000000

        ];
	if (YAHOO.util.Dom.get('agentur').value === '') {
		campaignErrorMessage = "- Agentur fehlt\n";
	}
	
	if (YAHOO.util.Dom.get('ap').value === '') {
		campaignErrorMessage = "- Ansprechpartner fehlt\n";
	}

	if (YAHOO.util.Dom.get('k_name').value === '' 
		|| !validateStrLengthData(YAHOO.util.Dom.get('k_name').value, 3)
	) {
		campaignErrorMessage += "- Kampagnenname fehlt oder zu kurz\n";
	}

	if (YAHOO.util.Dom.get('date').value === '--') {
		campaignErrorMessage += "- Versanddatum fehlt\n";
	}
	
	var campaignStatus = parseInt(YAHOO.util.Dom.get('status').value);
	if (campaignStatus < 20 
		&& campaignStatus !== 5
	) {
		// wenn kampagne noch nicht versendet wurde datum überprüfen
		if (!checkIfDeliveryDateIsValide('date', 'campaign_versandHour', 'campaign_versandMin')) {
			campaignErrorMessage += unescape("- Versanddatum mu%DF in der Zukunft liegen!") + "\n";
		}
		
		if (YAHOO.util.Dom.get('mailingTyp').value === 'NV') {
			if (!checkNvSendCampaignDate(hvCampaignDateObject)) {
				campaignErrorMessage += unescape("Versanddatum der NV mu%DF gr%F6%DFer sein als das HV Versanddatum: " + hvCampaignDateObject.toLocaleString() + " Uhr");
			}
		}
	}

	if (YAHOO.util.Dom.get('campaign_click_profiles_id').value === '') {
		campaignErrorMessage += "- Kampagnen-Klickprofil fehlt\n";
	}
	
	if (YAHOO.util.Dom.get('deliverySystem').value === '') {
		campaignErrorMessage += "- Versandsystem fehlt\n";
	}
        
        if (YAHOO.util.Dom.get('deliverySystemDistributorItems').value === '') {
		campaignErrorMessage += "- Verteiler fehlt\n";
	}
        
        if (YAHOO.util.Dom.get('deliverySystemDistributorDomainItems') === null) {
		campaignErrorMessage += "- Versanddomain fehlt\n";
	}

	if (YAHOO.util.Dom.get('abrechnungsart').value === '') {
		campaignErrorMessage += "- Abrechnungsart fehlt\n";
	}
	
	if (isNaN(parseInt(YAHOO.util.Dom.get('vorgabe_m').value)) 
		|| parseInt(YAHOO.util.Dom.get('vorgabe_m').value) === 0
	) {
		campaignErrorMessage += "- Zu versend. Volumen fehlt bzw. ist zu gering.\n";
	}
	
    if (YAHOO.util.Dom.get('campaign_use_campaign_start_date_0') !== null) {	
		if(YAHOO.util.Dom.get('campaign_use_campaign_start_date_1').checked
		   && parseInt(YAHOO.util.Dom.get('vorgabe_m').value) < 2
		){
			campaignErrorMessage += "- Bei Auto-NV darf nicht die Zielmenge 1 erfasst werden!\nIhr Kampagne würden nur einem Empfänger erreichen\nWir empfehlen die Menge des HV bei der gleichen Zielgruppe.\nAlternativ können Sie eine höhere oder niedrigere Zahl erfassen\n";
		}
    }
    
    if (YAHOO.util.Dom.get('deliverySystem').value === 'M' 
            && YAHOO.util.Dom.get('campaign_use_campaign_start_date_0') !== null
        ){
           if(YAHOO.util.Dom.get('campaign_use_campaign_start_date_1').checked){
                var volumen = YAHOO.util.Dom.get('vorgabe_m').value;
                var intVolumen = volumen.replace(".", ""); 
                  if (sendVolumen.includes(parseInt(intVolumen)) == false){
	         	campaignErrorMessage += unescape("- Bitte Zu versend. Volumen ausw%E4hlen.\n");
                    }
                }
        }	
	return campaignErrorMessage;
}

function createFormPostDataFromFromElements(formElements) {
	var postData = [];
	for (var i = 0; i < formElements.length; i++) {
		var inputElement = formElements[i];
		
		if (inputElement.name.length > 0) {
			switch (inputElement.type) {
				case 'checkbox':
					var checkboxElements = document.getElementsByName(inputElement.name);
					if (checkboxElements.length === 1) {
						var pushDataContent;
						
						if (inputElement.checked) {
							pushDataContent = inputElement.name + '=' + encodeURIComponent(inputElement.value);
						} else {
							pushDataContent = inputElement.name + '=';
						}
						
						postData.push(pushDataContent);
					} else {
						if (inputElement.checked) {
							postData.push(inputElement.name + '=' + encodeURIComponent(inputElement.value));
						}
					}
					break;
					
				case 'radio':
					if (inputElement.checked) {
						postData.push(inputElement.name + '=' + encodeURIComponent(inputElement.value));
					}
					break;
				
				default:
					if (!inputElement.readonly 
						&& !inputElement.disabled
					) {
						postData.push(inputElement.name + '=' + encodeURIComponent(inputElement.value));
					}
					break;
			}
		}
	}
	
	return postData;
}


function initCampaignManager() {
	var handleSubmit_fields = function() {
		YAHOO.util.Dom.get('ul2').innerHTML = '';

		this.submit();
		
		pageReload(
			'kampagne/k_main.php',
			'k'
		);
	};
	
	var handleSubmit_customer = function() {
		var validateResult = validateCustomerData('customer');
		
		if (validateResult.length > 0) {
			alert(validateResult);
			
			return false;
		} else {
			this.submit();
			
			var customerId = parseInt(YAHOO.util.Dom.get('kunde_id').value);
			if (isNaN(customerId) 
				|| customerId === 0
			) {
				setRequest(
					'kampagne/View/Customers/',
					'cm_kunde'
				);
			} else {
				setRequest_default(
					'kampagne/View/Customers/details.php?customer[id]=' + customerId,
					'basic_content'
				);
			}
		}
	};
	
	var handleSubmit_contactPerson = function() {
		var validateResult = validateContactPersonData('contact');
		
		if (validateResult.length > 0) {
			alert(validateResult);
			
			return false;
		} else {
			this.submit();
			
			var kunde_id2 = YAHOO.util.Dom.get('kunde_id2').value;
			setRequest_default(
				'kampagne/View/Customers/details.php?customer[id]=' + parseInt(kunde_id2),
				'basic_content'
			);
		}
	};
	
	var handleSubmit_campaignNewCustomer = function() {
		var validateResult = validateCustomerData('cmCustomer');
		
		if (validateResult.length > 0) {
			alert(validateResult);
			
			return false;
		} else {
			YAHOO.util.Dom.get('kNewCustomerShortCompany').value = YAHOO.util.Dom.get('cmCustomerShortCompany').value;
			YAHOO.util.Dom.get('kNewCustomerStreet').value = YAHOO.util.Dom.get('cmCustomerStreet').value;
			YAHOO.util.Dom.get('kNewCustomerZip').value = YAHOO.util.Dom.get('cmCustomerZip').value;
			YAHOO.util.Dom.get('kNewCustomerCity').value = YAHOO.util.Dom.get('cmCustomerCity').value;
			YAHOO.util.Dom.get('kNewCustomerCountryId').value = YAHOO.util.Dom.get('cmCustomerCountryId').value;
			YAHOO.util.Dom.get('kNewCustomerPhone').value = YAHOO.util.Dom.get('cmCustomerPhone').value;
			YAHOO.util.Dom.get('kNewCustomerFax').value = YAHOO.util.Dom.get('cmCustomerFax').value;
			YAHOO.util.Dom.get('kNewCustomerWebsite').value = YAHOO.util.Dom.get('cmCustomerWebsite').value;
			YAHOO.util.Dom.get('kNewCustomerEmail').value = YAHOO.util.Dom.get('cmCustomerEmail').value;
			YAHOO.util.Dom.get('kNewCustomerCeo').value = YAHOO.util.Dom.get('cmCustomerCeo').value;
			YAHOO.util.Dom.get('kNewCustomerRegistergericht').value = YAHOO.util.Dom.get('cmCustomerRegistergericht').value;
			YAHOO.util.Dom.get('kNewCustomerVatNumber').value = YAHOO.util.Dom.get('cmCustomerVatNumber').value;
			YAHOO.util.Dom.get('kNewCustomerPaymentDeadline').value = YAHOO.util.Dom.get('cmCustomerPaymentDeadline').value;
			YAHOO.util.Dom.get('kNewCustomerDataSelection').value = YAHOO.util.Dom.get('cmCustomerDataSelection').value;
			
			var theSel = document.neu_k_form.agentur;
			var newText = YAHOO.util.Dom.get('cmCustomerCompany').value;
			var newValue = newText;
			
			if (theSel.options[0].value === '') {
				theSel.options[0] = null;
			}
			
			var newEntry = new Option(newText, newValue, false, true);
			theSel.options[theSel.length] = newEntry;
			
			// kunde/agentur neu button ausblenden
			YAHOO.util.Dom.get('k_neuer_kunde').style.visibility = 'hidden';
			
			// ansprechpartner button einblenden
			var apButton = YAHOO.util.Dom.get('apButton');
			if (apButton !== null) {
				apButton.style.visibility = '';
			}
			
			YAHOO.example.container.k_neuer_kunde.hide();
		}
	};
	
	var handleSubmit_campaignNewContactPerson = function() {
		var validateResult = validateContactPersonData('cmContact');
		
		if (validateResult.length > 0) {
			alert(validateResult);
			
			return false;
		} else {
			var contactPersonGender;
			var vertrieberObj = YAHOO.util.Dom.get('cmContactPersonVertriebler');
			
			if (YAHOO.util.Dom.get('cmContactPersonGender_1').checked === true) {
				contactPersonGender = 'Herr';
			} else {
				contactPersonGender = 'Frau';
			}
			
			YAHOO.util.Dom.get('kNewContactPersonGender').value = contactPersonGender;
			YAHOO.util.Dom.get('kNewContactPersonVertriebler').value = vertrieberObj.value;
			YAHOO.util.Dom.get('kNewContactPersonTitle').value = YAHOO.util.Dom.get('cmContactPersonTitle').value;
			YAHOO.util.Dom.get('kNewContactPersonFirstname').value = YAHOO.util.Dom.get('cmContactPersonFirstname').value;
			YAHOO.util.Dom.get('kNewContactPersonLastname').value = YAHOO.util.Dom.get('cmContactPersonLastname').value;
			YAHOO.util.Dom.get('kNewContactPersonEmail').value = YAHOO.util.Dom.get('cmContactPersonEmail').value;
			YAHOO.util.Dom.get('kNewContactPersonPhone').value = YAHOO.util.Dom.get('cmContactPersonPhone').value;
			YAHOO.util.Dom.get('kNewContactPersonFax').value = YAHOO.util.Dom.get('cmContactPersonFax').value;
			YAHOO.util.Dom.get('kNewContactPersonMobil').value = YAHOO.util.Dom.get('cmContactPersonMobil').value;
			YAHOO.util.Dom.get('kNewContactPersonPosition').value = YAHOO.util.Dom.get('cmContactPersonPosition').value;
			
			YAHOO.util.Dom.get('vertriebler_select').innerHTML = vertrieberObj.options[vertrieberObj.selectedIndex].text;
			
			
			var theSel = document.neu_k_form.ap;
			var newText = YAHOO.util.Dom.get('cmContactPersonFirstname').value + ' ' + YAHOO.util.Dom.get('cmContactPersonLastname').value;
			var newValue = newText;
			
			if (theSel.options[0].value === '') {
				theSel.options[0] = null;
			}
			
			var newEntry = new Option(newText, newValue, false, true);
			theSel.options[theSel.length] = newEntry;
			
			// ansprechpartner neu button ausblenden
			YAHOO.util.Dom.get('apButton').style.visibility = 'hidden';
			
			YAHOO.example.container.k_neuer_kontakt.hide();
		}
	};
	
	var handleDelete_contactPerson = function() {
		YAHOO.util.Dom.get('del_ap_id').value = YAHOO.util.Dom.get('ap_id_def').value;

		this.submit();

		YAHOO.util.Dom.get('del_ap_id').value = YAHOO.util.Dom.get('ap_id_def').value = '';

		setRequest_default(
			'kampagne/View/Customers/details.php?customer[id]=' + parseInt(YAHOO.util.Dom.get('kunde_id_def').value),
			'basic_content'
		);
	};
	
	var handleDelete_customer = function() {
		YAHOO.util.Dom.get('del_kunde_id').value = YAHOO.util.Dom.get('kunde_id_def').value;

		this.submit();

		YAHOO.util.Dom.get('del_kunde_id').value = '';
		
		setRequest(
			'kampagne/View/Customers/',
			'cm_kunde'
		);
	};
	
	var handleSubmit_status = function() {
		var p = YAHOO.util.Dom.get('page').value;
		var p_sub = YAHOO.util.Dom.get('page_sub').value;

		this.submit();

		pageReload(p, p_sub);
	};
	
        var handleSubmit_date= function() {
        var p = YAHOO.util.Dom.get('page').value;
        var p_sub = YAHOO.util.Dom.get('page_sub').value;

        this.submit();

        pageReload(p, p_sub);
        };


	var handleSubmit_campaign = function() {
		var validateResult = validateCampaignData();
		if (validateResult.length > 0) {
			alert(validateResult);
			
			return false;
		} else {
			YAHOO.util.Dom.get('abrechnungsart').disabled = '';
			YAHOO.util.Dom.get('campaign_versandMin').disabled = '';
			YAHOO.util.Dom.get('campaign_versandHour').disabled = '';
			YAHOO.util.Dom.get('deliverySystem').disabled = '';
			
			if (YAHOO.util.Dom.get('mailingTyp').value === 'NV') {
				enableCampaign_useCampaignStartDate();
			}
			var reloadUrl;
			var p = YAHOO.util.Dom.get('page').value;
			var subPage = YAHOO.util.Dom.get('page_sub').value;

			if ((p.indexOf('filter')) > 0) {
				reloadUrl = p;
			} else {
				switch (YAHOO.util.Dom.get('campaignAction').value) {
					case 'edit':
					case 'addNv':
						reloadUrl = createCampaignReloadUrl();
						break;

					default:
						// new, copy
						reloadUrl = p;
						break;
				}
			}
			
			var postData = createFormPostDataFromFromElements(this.form.elements);
			processAjaxRequest(
				this.form.action,
				postData.join('&'),
				'',
				function (result) {
					console.log(result);
				}
			);
			
			this.hide();
			
			pageReload(reloadUrl, subPage);
		}
	};
	
	var handleSubmit_editReport = function() {
		this.submit();

		var kid_rep_edit = YAHOO.util.Dom.get('kid_rep_edit').value;
		setRequest_default('kampagne/report/rep_details.php?kid=' + kid_rep_edit, 'rep_details_content');
	};
	
	var handleCancel_campaign = function() {
		this.cancel();

		var edit_kid = YAHOO.util.Dom.get('kid').value;
		setRequestK('kampagne/save.php?action=edit_lock&kid=' + edit_kid);
	};
	
	var handle_rechnungsstellung = function() {
		YAHOO.util.Dom.get('vorlage_buchhaltungmail').innerHTML = CKEDITOR.instances.vorlage_buchhaltungmail_edit.getData();
		YAHOO.util.Dom.get('vorlage_buchhaltungmail_betreff').value = YAHOO.util.Dom.get('vorlage_buchhaltungmail_betreff_edit').value;

		this.cancel();
	};
	
	var handle_zustellreport = function() {
		var page = YAHOO.util.Dom.get('page').value;
		var subPage = YAHOO.util.Dom.get('page_sub').value;
		var e = YAHOO.util.Dom.get('zustellreport_email').value;
		var b = YAHOO.util.Dom.get('vorlage_zustellreport_betreff_edit').value;
		var n = CKEDITOR.instances.vorlage_zustellreport_mail.getData();

		var fehler = '';
		if (e === '') {
			fehler = "- Empfaenger fehlt\n";
		}

		if (b === '') {
			fehler += "- Betreff fehlt\n";
		}

		if (n === '') {
			fehler += "- Mailinhalt fehlt\n";
		}

		if (fehler === '') {
			YAHOO.util.Dom.get('vorlage_zustellreport_mail').value = n;

			this.submit();

			if (YAHOO.util.Dom.get('page_planer') !== null) {
				// TODO: remove, use: setRequest(page, subPage);
				pagePlanerRequest();
			} else {
				setRequest(
					page,
					subPage
				);
			}

			var alertTxt;
			if (YAHOO.util.Dom.get('buchhaltungmail_')) {
				alertTxt = 'Rechnungsstellungsmail';
			} else {
				alertTxt = 'Zustellreport';
			}

			alert(alertTxt + ' versendet');
		} else {
			alert(fehler);
		}
	};
	
	var handleOpen_nv = function() {
		this.cancel();

		var kidp = YAHOO.util.Dom.get('kidP').value;
		var oRatePrognose = YAHOO.util.Dom.get('oRatePrognose').value;
		var kRatePrognose = YAHOO.util.Dom.get('kRatePrognose').value;

		setRequest_default('kampagne/nv_calc.php?kid=' + kidp + '&oRatePrognose=' + oRatePrognose + '&kRatePrognose=' + kRatePrognose, 'nv_calc_content');

		YAHOO.example.container.calc.show();
	};
	
	var handleDelete_campaignSuccess = function(o) {
		var page = YAHOO.util.Dom.get('page').value;
		var subPage = YAHOO.util.Dom.get('page_sub').value;
		var responseJson = YAHOO.lang.JSON.parse(o.responseText);

		alert(responseJson.content.replace('<br />', "\n"));
		if (YAHOO.util.Dom.get('page_planer') !== null) {
			// TODO: remove, use: setRequest(page, subPage);
			pagePlanerRequest();
		} else {
			setRequest(
				page,
				subPage
			);
		}
	};
	var handleDelelete_campaignFailure = function(o) {
		alert('Submission failed: ' + o.status);
	};
	
	var handleCancel_campaignReport = function() {
		var p = YAHOO.util.Dom.get('page').value;
		var p_sub = YAHOO.util.Dom.get('page_sub').value;
		var campaignStatus = parseInt(YAHOO.util.Dom.get('reportCampaignOldStatus').value);
		
		this.cancel();
		
		if (campaignStatus >= 30) {
			processAjaxRequest(
                'AjaxRequests/ajax.php',
                'actionMethod=campaign' 
					+ '&actionType=restoreOldCampaignStatus' 
					+ '&campaign[kid]=' +  parseInt(YAHOO.util.Dom.get('Report_kid').value)
					+ '&campaign[status]=' + campaignStatus
				,
                '',
                function () {}
            );
		}
		
		pageReload(p, p_sub);
	};
	
	var handleSend_testmail = function() {
		var empf = YAHOO.util.Dom.get('testmailRecipient').value;
		if (empf === '') {
			alert("Bitte einen Empfaenger auswaehlen.");
			
			return false;
		} else {
			var cfm = confirm(testmailRecipients);
			if (cfm) {
				
				YAHOO.util.Dom.get('testmailContentTableFrame').outerHTML = getLoaderImage();
				
				processAjaxRequest(
					'AjaxRequests/ajax.php',
					'actionMethod=campaign' 
						+ '&actionType=sendTestmail' 
						+ '&campaign[k_id]=' +  parseInt(YAHOO.util.Dom.get('testmailCampaignId').value)
						+ '&recipient[listId]=' + parseInt(YAHOO.util.Dom.get('testmailTestlistId').value)
						+ '&' + arrayToQueryString(
							empf.split(';'),
							'recipients[]'
						)
					,
					'',
					function (contentResult) {
						if (contentResult.success) {
							YAHOO.example.container.container_testmail.hide();

						} else {
							YAHOO.util.Dom.get('testmail_frame').innerHTML = contentResult.message;
						}
					}
				);
			}
		}
	};

         var handleSync_testmail = function() {
             processAjaxRequest(
					'AjaxRequests/ajax.php',
					'actionMethod=campaign' 
				        + '&actionType=syncTestmail' 
					,
					'',
					function (contentResult) {
						if (contentResult.success) {
							YAHOO.example.container.dialog2.hide();

						} else {
							YAHOO.util.Dom.get('dialog2').innerHTML = contentResult.message;
						}
					}
                                                
				);
                        
             this.hide();
                        YAHOO.util.Dom.get('personFirstname').value ='';
                        YAHOO.util.Dom.get('personLastname').value ='';
                        YAHOO.util.Dom.get('personEmail').value ='';
         }
        var handleCreate_testmail = function() {

	var validateResult = validateTestmailData();
		if (validateResult.length > 0) {
			alert(validateResult);
			
			return false;
		} else {
                    var personGender;
                         if (YAHOO.util.Dom.get('personGender_1').checked === true) {
				personGender = 'Herr';
			} else {
				personGender = 'Frau';
			}
				
				processAjaxRequest(
					'AjaxRequests/ajax.php',
					'actionMethod=campaign' 
				        + '&actionType=createTestmail' 
                                        + '&person[gender]=' + personGender
				        + '&person[vorname]=' + YAHOO.util.Dom.get('personFirstname').value
                                        + '&person[nachname]=' + YAHOO.util.Dom.get('personLastname').value
                                        + '&person[email]=' + YAHOO.util.Dom.get('personEmail').value
					,
					'',
					function (contentResult) {
						if (contentResult.success) {
							YAHOO.example.container.dialog2.hide();

						} else {
							YAHOO.util.Dom.get('dialog2').innerHTML = contentResult.message;
						}
					}
				);
                        this.hide();
                        YAHOO.util.Dom.get('personFirstname').value ='';
                        YAHOO.util.Dom.get('personLastname').value ='';
                        YAHOO.util.Dom.get('personEmail').value ='';
                    }
			
		
	};
        
	YAHOO.example.container.dialog1 = new YAHOO.widget.Dialog('dialog1', {
		width: '500px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: false,
		constraintoviewport: true,
		buttons: [{
                        text: 'speichern',
			handler: handleSubmit_campaign,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_campaign
		}]
	});
	YAHOO.example.container.dialog1.render();
	YAHOO.util.Event.addListener('neu_k_button', 'click', YAHOO.example.container.dialog1.show, YAHOO.example.container.dialog1, true);
	YAHOO.util.Event.addListener('neu_k_button_planer', 'click', YAHOO.example.container.dialog1.show, YAHOO.example.container.dialog1, true);
	YAHOO.util.Event.addListener('button_add_k', 'click', YAHOO.example.container.dialog1.show, YAHOO.example.container.dialog1, true);

	
        YAHOO.example.container.dialog2 = new YAHOO.widget.Dialog('dialog2', {
		width: '500px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: false,
		constraintoviewport: true,
		buttons: [{
                        text: 'anmelden',
			handler: handleCreate_testmail,
			isDefault: true
		}, {
                        text: 'Kontakte in SE aktualisieren',
			handler: handleSync_testmail,
			isDefault: true
		},{
			text: 'abbrechen',
			handler: handleCancel_campaign
		}]
	});
	YAHOO.example.container.dialog2.render();
	YAHOO.util.Event.addListener('test_adressen_button', 'click', YAHOO.example.container.dialog2.show, YAHOO.example.container.dialog2, true);
        
	YAHOO.example.container.container_versenderinfo = new YAHOO.widget.Dialog('container_versenderinfo', {
		width: '500px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		constraintoviewport: true
	});
	YAHOO.example.container.container_versenderinfo.render();

	YAHOO.example.container.k_neuer_kunde = new YAHOO.widget.Dialog('container_k_neuer_kunde', {
		width: '350px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'hinzuf&uuml;gen',
			handler: handleSubmit_campaignNewCustomer,
			isDefault: true
		}, {
			text: 'schliessen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.k_neuer_kunde.render();

	YAHOO.example.container.k_neuer_kontakt = new YAHOO.widget.Dialog('container_k_neuer_kontakt', {
		width: '300px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'hinzuf&uuml;gen',
			handler: handleSubmit_campaignNewContactPerson,
			isDefault: true
		}, {
			text: 'schliessen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.k_neuer_kontakt.render();

	YAHOO.example.container.container_vs_anlegen = new YAHOO.widget.Dialog('container_vs_anlegen', {
		width: '600px',
		height: '420px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'speichern',
			handler: handleSubmit_campaign,
			isDefault: true
		}, {
			text: 'schliessen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_vs_anlegen.render();
	YAHOO.util.Event.addListener('button_createInVs', 'click', YAHOO.example.container.container_vs_anlegen.show, YAHOO.example.container.container_vs_anlegen, true);

	YAHOO.example.container.container_cm_report_kontext = new YAHOO.widget.Dialog('container_cm_report_kontext', {
		width: '800px',
		height: '650px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: false,
		constraintoviewport: true,
		buttons: [{
			text: 'schliessen',
			handler: handleCancel_campaignReport
		}]
	});
	YAHOO.example.container.container_cm_report_kontext.render();
	YAHOO.util.Event.addListener('button_report_kontext', 'click', YAHOO.example.container.container_cm_report_kontext.show, YAHOO.example.container.container_cm_report_kontext, true);

	YAHOO.example.container.container_rep_editieren = new YAHOO.widget.Dialog('container_rep_editieren', {
		width: '450px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'speichern',
			handler: handleSubmit_editReport,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_rep_editieren.render();

	YAHOO.example.container.container_update = new YAHOO.widget.Dialog('container_update', {
		width: '325px',
		height: '100px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'OK',
			handler: handleUpdate,
			isDefault: true
		}]
	});
	YAHOO.example.container.container_update.render();
	YAHOO.util.Event.addListener('upd_all_button', 'click', YAHOO.example.container.container_update.show, YAHOO.example.container.container_update, true);
	YAHOO.util.Event.addListener('upd_all_button_planer', 'click', YAHOO.example.container.container_update.show, YAHOO.example.container.container_update, true);
	YAHOO.util.Event.addListener('button_update', 'click', YAHOO.example.container.container_update.show, YAHOO.example.container.container_update, true);

	YAHOO.example.container.container_info = new YAHOO.widget.Dialog('container_info', {
		width: '700px',
		height: '430px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true
	});
	YAHOO.example.container.container_info.render();
	YAHOO.util.Event.addListener('button_details', 'click', YAHOO.example.container.container_info.show, YAHOO.example.container.container_info, true);

	YAHOO.example.container.container_prognose = new YAHOO.widget.Dialog('container_prognose', {
		width: '625px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'NV Rechner',
			handler: handleOpen_nv,
			isDefault: true
		}, {
			text: 'schliessen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_prognose.render();
	YAHOO.util.Event.addListener('button_prognose2', 'click', YAHOO.example.container.container_prognose.show, YAHOO.example.container.container_prognose, true);

	YAHOO.example.container.container_sync = new YAHOO.widget.Dialog('container_sync', {
		width: '770px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'OK',
			handler: handleUpdate,
			isDefault: true
		}]
	});
	YAHOO.example.container.container_sync.render();
	YAHOO.util.Event.addListener('button_sync_k', 'click', YAHOO.example.container.container_sync.show, YAHOO.example.container.container_sync, true);

	YAHOO.example.container.container_k_rep_vorlage = new YAHOO.widget.Dialog('container_k_rep_vorlage', {
		width: '450px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'speichern',
			handler: handleUpdate,
			isDefault: true
		}]
	});
	YAHOO.example.container.container_k_rep_vorlage.render();
	YAHOO.util.Event.addListener('k_rep_vorlage_button', 'click', YAHOO.example.container.container_k_rep_vorlage.show, YAHOO.example.container.container_k_rep_vorlage, true);

	YAHOO.example.container.container_testmail = new YAHOO.widget.Dialog('container_testmail', {
		width: '800px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'Testmail senden',
			handler: handleSend_testmail,
			isDefault: true
		}]
	});
	YAHOO.example.container.container_testmail.render();
	YAHOO.util.Event.addListener('button_testmail', 'click', YAHOO.example.container.container_testmail.show, YAHOO.example.container.container_testmail, true);
	
	YAHOO.util.Event.addListener('button_testmail_selbst', 'click');

	YAHOO.example.container.container_del = new YAHOO.widget.Dialog('container_del', {
		width: '400px',
		fixedcenter: true,
		visible: false,
		modal: true,
		constraintoviewport: true,
		buttons: [{
			text: 'l&ouml;schen',
			handler: handleSubmit_default,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_del.callback = {
		success: handleDelete_campaignSuccess,
		failure: handleDelelete_campaignFailure
	};
	YAHOO.example.container.container_del.render();
	YAHOO.util.Event.addListener('button_del_k', 'click', YAHOO.example.container.container_del.show, YAHOO.example.container.container_del, true);

	YAHOO.example.container.container_noRightDel = new YAHOO.widget.Dialog('container_noRightDel', {
		width: '400px',
		fixedcenter: true,
		visible: false,
		modal: true,
		constraintoviewport: true,
		buttons: [{
			text: 'ok',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_noRightDel.render();
	YAHOO.util.Event.addListener('button_noRightDel_k', 'click', YAHOO.example.container.container_noRightDel.show, YAHOO.example.container.container_noRightDel, true);

	YAHOO.example.container.container_status = new YAHOO.widget.Dialog('container_status', {
		width: '350px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: '&auml;ndern',
			handler: handleSubmit_status,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_status.render();
	YAHOO.util.Event.addListener('status_aendern', 'click', YAHOO.example.container.container_status.show, YAHOO.example.container.container_status, true);

        YAHOO.example.container.container_date = new YAHOO.widget.Dialog('container_date', {
        width: '350px',
        fixedcenter: true,
        visible: false,
        modal: true,
        close: true,
        constraintoviewport: true,
        buttons: [{
            text: '&auml;ndern',
            handler: handleSubmit_date,
            isDefault: true
        }, {
            text: 'abbrechen',
            handler: handleCancel_default
        }]
        });
        YAHOO.example.container.container_date.render();
        YAHOO.util.Event.addListener('date_aendern', 'click', YAHOO.example.container.container_date.show, YAHOO.example.container.container_date, true);


	YAHOO.example.container.container_tab_felder = new YAHOO.widget.Dialog('container_tab_felder', {
		width: '470px',
		fixedcenter: true,
		visible: false,
		modal: true,
		scroll: true,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: '&Uuml;bernehmen',
			handler: handleSubmit_fields,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_tab_felder.render();
	YAHOO.util.Event.addListener('tab_config', 'click', YAHOO.example.container.container_tab_felder.show, YAHOO.example.container.container_tab_felder, true);

	YAHOO.example.container.calc = new YAHOO.widget.Dialog('container_calc', {
		width: '350px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'schliessen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.calc.render();
	YAHOO.util.Event.addListener('button_empfehlung', 'click', YAHOO.example.container.calc.show, YAHOO.example.container.calc, true);

	YAHOO.example.container.suche = new YAHOO.widget.Dialog('suche', {
		width: '560px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'suchen',
			handler: handleSubmit_status,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.suche.render();
	YAHOO.util.Event.addListener('suchen2', 'click', YAHOO.example.container.suche.show, YAHOO.example.container.suche, true);

	YAHOO.example.container.k_statistik = new YAHOO.widget.Dialog('container_k_statistik', {
		width: '940px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true
	});
	YAHOO.example.container.k_statistik.render();
	YAHOO.util.Event.addListener('k_analyse_button', 'click', YAHOO.example.container.k_statistik.show, YAHOO.example.container.k_statistik, true);
	YAHOO.util.Event.addListener('Analyse', 'click', YAHOO.example.container.k_statistik.show, YAHOO.example.container.k_statistik, true);
	
	YAHOO.example.container.container_rechnungsstellung = new YAHOO.widget.Dialog('container_rechnungsstellung', {
		width: '500px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: '&uuml;bernehmen',
			handler: handle_rechnungsstellung,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_rechnungsstellung.render();

	YAHOO.example.container.container_zustellreport = new YAHOO.widget.Dialog('container_zustellreport', {
		width: '500px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'senden',
			handler: handle_zustellreport,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_zustellreport.render();

	YAHOO.example.container.container_re_stellung = new YAHOO.widget.Dialog('container_re_stellung', {
		width: '500px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'senden',
			handler: handle_zustellreport,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_re_stellung.render();
	
	
	// customers
	YAHOO.example.container.container_kunde = new YAHOO.widget.Dialog('container_kunde', {
		width: '350px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'speichern',
			handler: handleSubmit_customer,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_kunde.render();

	YAHOO.example.container.container_kunde_ap = new YAHOO.widget.Dialog('container_kunde_ap', {
		width: '300px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'speichern',
			handler: handleSubmit_contactPerson,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_kunde_ap.render();

	YAHOO.example.container.container_del_kunde_ap = new YAHOO.widget.Dialog('container_del_kunde_ap', {
		width: '300px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'l&ouml;schen',
			handler: handleDelete_contactPerson,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_del_kunde_ap.render();

	YAHOO.example.container.container_del_kunde = new YAHOO.widget.Dialog('container_del_kunde', {
		width: '300px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'l&ouml;schen',
			handler: handleDelete_customer,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_del_kunde.render();
}

YAHOO.util.Event.addListener(window, 'load', initCampaignManager);