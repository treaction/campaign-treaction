function setRequestDataSt(url) {
	request = createRequest();
    
    // überprüfen, ob Request erzeugt wurde
    if (!request) {
        alert('Kann keine XMLHTTP-Instanz erzeugen');
        
        return false;
    } else {
		request.open('POST', url, true);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send();
		request.onreadystatechange = interpretRequestStData;
	}
}

// Request auswerten
function interpretRequestStData() {
	switch (request.readyState) {
		// wenn der readyState 4 und der request.status 200 ist, dann ist alles korrekt gelaufen
		case 4:
			if (request.status !== 200) {
				document.getElementById('a_status_new').innerHTML = 'Fehler: ' + request.status + ' ' + request.statusText;
			} else {
				var contentData = request.responseText;
				
				// den Inhalt des Requests in das <div> schreiben
				document.getElementById('a_status_new').innerHTML = contentData;
				
				evalScript(contentData);
			}
			break;
			
		default:
			break;
	}
}