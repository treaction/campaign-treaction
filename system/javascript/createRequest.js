function createRequest() {
	// Request erzeugen
	if (window.XMLHttpRequest) {
		return new XMLHttpRequest(); // Mozilla, Safari, Opera
	} else if (window.ActiveXObject) {
		try {
			return new ActiveXObject('Msxml2.XMLHTTP'); // IE 5
		} catch (e) {
			try {
				return new ActiveXObject('Microsoft.XMLHTTP'); // IE 6
			} catch (e) {
				console.log(e);
			}
		}
	}
}

function evalScript(scripts) {
    try {
        if (scripts !== '') {
            var script = '';
            scripts = scripts.replace(/<script[^>]*>([\s\S]*?)<\/script>/gi, function() {
                if (scripts !== null) {
					script += arguments[1] + '\n';
				}
				
                return '';
            });
            
            if (script) {
				(window.execScript) 
					? window.execScript(script) 
					: window.setTimeout(script, 0)
				;
			}
        }
        
        return false;
    } catch(e) {
        alert(e);
    }
}