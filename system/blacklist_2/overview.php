<?php
session_start();
$mandant = $_SESSION['mandant'];
$mandantID = $_SESSION['mID'];
include('../library/chart.php');

if ($_GET['jahr']) {
    $jahr = $_GET['jahr'];
} else {
    $jahr = date("Y");
}

function zahl_format($zahl) {
    $zahl = number_format($zahl, 0, 0, '.');
    return $zahl;
}

function getMonat($m) {

    switch ($m) {
        case 1: return "Jan";
        case 2: return "Feb";
        case 3: return "M&auml;r";
        case 4: return "Apr";
        case 5: return "Mai";
        case 6: return "Jun";
        case 7: return "Jul";
        case 8: return "Aug";
        case 9: return "Sep";
        case 10: return "Okt";
        case 11: return "Nov";
        case 12: return "Dez";
    }
}

function getQuote($brutto, $netto) {
    $rnd = round((($netto / $brutto) * 100), 1);
    return $rnd;
}

function kAB() {
    global $mandantID;
    global $jahr;
    $verbindung = mysql_connect('localhost', 'abmelder', '2X6w5W9a') or die("FEHLER: Keine Verbindung zur Datenbank!");
    mysql_select_db('abmelder_ems', $verbindung) or die("Keine Datenbank!");

    $k_sql = mysql_query("SELECT MONTH(created_at) as Monat, COUNT(*) as Anzahl
						  FROM mandant_" . $mandantID . " 
						  WHERE YEAR(created_at) = '$jahr'
                                                  AND ( `ip` != '' OR `benutzer` > 0 )
					      GROUP BY Monat
						  ORDER BY Monat ASC", $verbindung);

    $data = "<chart formatNumberScale='0' thousandSeparator='.' canvasBorderColor='999999' showValues='0' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF'>";
    while ($kz = mysql_fetch_array($k_sql)) {

        $a = $kz['Monat'];
        $monat = getMonat($a);
        $data .= "<set value='" . $kz['Anzahl'] . "' label='" . $monat . "' color='666666' />";
    }

    $data .= "</chart>";
    return createGridChart::renderGridChart('abm', $data, 'Line', '475', '200');
}

function kABL() {
    global $mandantID;
    global $mandant;
    global $jahr;

    $verbindung = mysql_connect('localhost', 'blackuser', 'ati21coyu09t') or die("Keine Verbindung!");
    mysql_select_db('blacklist_2', $verbindung) or die("Keine Datenbank!");

    $k_sql = mysql_query("SELECT MONTH(Datum) as Monat, COUNT(*) as Anzahl
						  FROM Blacklist_2 
						  WHERE YEAR(Datum) = '$jahr'
						  AND (Firma = '$mandant'
						  OR mandant = '$mandantID')
					      GROUP BY Monat
						  ORDER BY Monat ASC", $verbindung);

    $data = "<chart formatNumberScale='0' thousandSeparator='.' canvasBorderColor='999999' showValues='0' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF'>";
    while ($kz = mysql_fetch_array($k_sql)) {

        $a = $kz['Monat'];
        $monat = getMonat($a);
        $data .= "<set value='" . $kz['Anzahl'] . "' label='" . $monat . "' color='666666' />";
    }

    $data .= "</chart>";
    return createGridChart::renderGridChart('bl', $data, 'Line', '475', '200');
}
?>

<div class="ovHeader">
    &Uuml;bersicht<br />
    <span style="font-size:18px;color:#8e8901">Blacklist &amp; Abmeldungen - <?php print $jahr; ?></span>
</div>

<div style="width:1000px;margin:auto;">
    <div id="kOV">

        <div class="kOV_table" style="float:left;margin-right:20px;margin-top:-1px">
            <table class="ovTable" width="500">
                <tr>
                    <td class="ovBlock">Blacklist</td>
                </tr>
                <tr>
                    <td ><?php print kABL(); ?></td>
                </tr>
                <tr>
                    <td class="ovBlock"></td>
                </tr>
            </table>
        </div>

        <div class="kOV_table">
            <table class="ovTable" width="500">
                <tr>
                    <td class="ovBlock">Abmeldungen (unique &amp; ohne Blacklister)</td>
                </tr>
                <tr>
                    <td ><?php print kAB(); ?></td>
                </tr>
                <tr>
                    <td class="ovBlock"></td>
                </tr>
            </table>
        </div>

    </div>
</div>
<div class="ovFooter"></div>
