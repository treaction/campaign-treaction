<?php
/**
 * skipClient
 * 
 * @param string $clientShortcut
 * @param \ClientManager $clientManager
 * @return void
 */
function skipClient($clientShortcut, \ClientManager $clientManager) {
	try {
		$clientEntity = $clientManager->getClientDataByShortcut($clientShortcut);
		/* @var $clientEntity ClientEntity */
		if (!($clientEntity instanceof \ClientEntity)) {
			throw new \DomainException('no ClientEntity', 1426498758);
		}

		// set newSessionData
		$_SESSION['broadmailImport'] = $clientEntity->getBroadmail_import();
		$_SESSION['client_base_path'] = $clientEntity->getClient_base_path();
		$_SESSION['hasMultipleDistributors'] = $clientEntity->getHas_multiple_distributors();
		
		
		/**
		 * @deprecated
		 */
		$_SESSION['mID'] = $clientEntity->getId();
		$_SESSION['mandant'] = $clientEntity->getAbkz();
		
		// setParentClientSessionData
		\setParentClientSessionData(
			$clientEntity->getId(),
			$clientManager
		);
		
		// session daten aufräumen
		unset(
			$_SESSION['crmDataArray'],
			$_SESSION['campaigns'],
			$_SESSION['datensaetze'],
			$_SESSION['campaign'],
			$_SESSION[\RegistryUtils::$systemKey]
		);
		
		\RegistryUtils::set('clientEntity', $clientEntity);
		
		/**
		 * TODO: removes @deprecated
		 */
		$_SESSION['crmMid'] = $clientEntity->getCrm_hash(); 
		$_SESSION['intoneGroup'] = $clientEntity->getIntone_group();
		
		\header('Location: ' . \BaseUtils::getLocationRedirect($_SESSION['client_base_path']) . 'index.php');
	} catch (\Exception $e) {
		require(DIR_configs . 'exceptions.php');
		
		\header('Location: https://campaign-treaction.de/meta/login.php'); // ProdSystem
	}
	
	exit();
}

/**
 * setParentClientSessionData
 * 
 * @param integer $clientId
 * @param \ClientManager $clientManager
 * @return void
 */
function setParentClientSessionData($clientId, \ClientManager $clientManager) {
	$parentClients = $underClientIds = array();
	if ((boolean) $_SESSION['hasMultipleDistributors']) {
		/**
		 * getParentClientsDataItems
		 */
		$parentClients = $clientManager->getParentClientsDataItems($clientId);
		if (\count($parentClients) > 0) {
			$underClientIds[] = $clientId;
			
			foreach ($parentClients as $clientEntity) {
				/* @var $clientEntity \ClientEntity */
				$underClientIds[] = $clientEntity->getId();
			}
		}
	}
	
	$_SESSION['parentClients'] = $parentClients;
	$_SESSION['underClientIds'] = $underClientIds;
	unset($parentClients, $underClientIds);
}

/**
 * getAndSetClientEntity
 * 
 * @param integer $clientId
 * @param \ClientManager $clientManager
 * @throws \DomainException
 * @return void
 */
function getAndSetClientEntity($clientId, \ClientManager $clientManager) {
	if (!(\RegistryUtils::get('clientEntity') instanceof \ClientEntity)) {
		/**
		 * getClientDataById
		 */
		$clientEntity = $clientManager->getClientDataById($clientId);
		if (!($clientEntity instanceof \ClientEntity)) {
			throw new \DomainException('no ClientEntity', 1427288977);
		}
		\RegistryUtils::set('clientEntity', $clientEntity);
		unset($clientEntity);
	}
}

/**
 * loadModuleJavascriptByUserPackages
 * 
 * @param array $userPackages
 * @return void
 */
function loadModuleJavascriptByUserPackages(array $userPackages) {
	// Campaign Manager
	if (\UserUtils::checkUserPackagesPermission($userPackages, 2)) {
		?>
		<script type="text/javascript" src="javascript/Module/Campaign/dialogAndListner.js"></script>
		<script type="text/javascript" src="javascript/Module/Campaign/campaign.js"></script>
		<script type="text/javascript" src="javascript/Module/Campaign/Requests/display.js"></script>
		<script type="text/javascript" src="javascript/Module/Campaign/Requests/new.js"></script>
		<script type="text/javascript" src="javascript/Module/Campaign/Requests/statistic.js"></script>
		<script type="text/javascript" src="javascript/Module/Campaign/Requests/bottom.js"></script>
		<script type="text/javascript" src="javascript/Module/Campaign/Requests/preview.js"></script>
		<script type="text/javascript" src="javascript/Module/Campaign/Requests/vorlage.js"></script>
		<?php
	}

	// Data Manager
	if (\UserUtils::checkUserPackagesPermission($userPackages, 4)) {
		?>
		<script type="text/javascript" src="javascript/Module/DataManager/dialogAndListner.js"></script>
		<script type="text/javascript" src="javascript/Module/DataManager/import.js"></script>
		<script type="text/javascript" src="javascript/Module/DataManager/supplier.js"></script>
		<script type="text/javascript" src="javascript/Module/DataManager/Requests/statistic.js"></script>
		<script type="text/javascript" src="javascript/Module/DataManager/Requests/status.js"></script>
		<?php
	}

	// Blacklist/Abmelder
	if (\UserUtils::checkUserPackagesPermission($userPackages, 6)) {
		?>
		<script type="text/javascript" src="javascript/Module/BlacklistUnsubscribe/dialogAndListner.js"></script>
		<script type="text/javascript" src="javascript/Module/BlacklistUnsubscribe/Requests/blacklist.js"></script>
		<script type="text/javascript" src="javascript/Module/BlacklistUnsubscribe/blacklist.js"></script>
		<script type="text/javascript" src="javascript/Module/BlacklistUnsubscribe/unsubscribe.js"></script>
		<?php
	}

	// Dais
	if (\UserUtils::checkUserPackagesPermission($userPackages, 7)) {
		?>
		<script type="text/javascript" src="javascript/Module/Dais/Requests/dais.js"></script>
		<?php
	}

	// admin
	if (\UserUtils::checkUserPackagesPermission($userPackages, 999)) {
		?>
		<script type="text/javascript" src="javascript/Module/Admin/dialogAndListner.js"></script>
		<script type="text/javascript" src="javascript/Module/Admin/default.js"></script>
		<?php
	}
}



/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * db_connect
 * 
 * $debugLogManager
 * $clientManager
 */
require_once('loadAndInitEmsAutoloader.php');

$mandant = $_SESSION['mandant'];

require_once('db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */



/**
 * Registry global values
 */
\RegistryUtils::set('basePath', \BaseUtils::getBasePath());
\RegistryUtils::set('beginPath', \BaseUtils::getFirstPath(\RegistryUtils::get('basePath')));
\RegistryUtils::set('dateNow', new \DateTime());


if (isset($_POST['mandant_wechsel']) 
	&& \strlen($_POST['mandant_wechsel']) > 0
) {
	\skipClient(
		\DataFilterUtils::filterData(
			$_POST['mandant_wechsel'],
			\DataFilterUtils::$validateFilterDataArray['string']
		),
		$clientManager
	);
}   
   
// debug
$debugLogManager->logData('POST', $_POST);
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('client', $mandant);

$yearDataArray = array();

for ($i = 2009; $i <= \RegistryUtils::get('dateNow')->format('Y'); $i++) {
    $yearDataArray[] = $i;
}
$debugLogManager->logData('yearDataArray', $yearDataArray);

try {
	$clientChangeForm = '<form action="index.php" method="post" style="display:inline;">' 
		. \HtmlFormUtils::createSelectItem(
			array(
				'name' => 'mandant_wechsel',
				'id' => 'mandant_wechsel',
				'onchange' => 'this.form.submit();'
			),
			\ClientUtils::getClientsSelectItems($_SESSION['mID'])
		) 
		. '</form>'
	;
    
	/**
	 * getAndSetClientEntity
	 * 
	 * nur von altes system auf das neue
	 */
	\getAndSetClientEntity(
		$_SESSION['mID'],
		$clientManager
	);
	/**
	 * getUserDataItemById
	 */

	$userEntity = $clientManager->getUserDataItemById($_SESSION['benutzer_id']);

	if (!($userEntity instanceof \UserEntity)) {
		throw new \DomainException('no UserEntity', 1426155610);
	}
	\RegistryUtils::set('userEntity', $userEntity);
	
	$userPackages = \explode(',', $userEntity->getMenu());
	$debugLogManager->logData('userPackages', $userPackages);
	
	/**
	 * getDeliverySystemDataItemsByClientId
	 */
	$_SESSION['deliverySystemsDataArray'] = $clientManager->getDeliverySystemDataItemsByClientId(\RegistryUtils::get('clientEntity')->getId());
	if (\count($_SESSION['deliverySystemsDataArray']) === 0) {
		throw new \DomainException('no deliverySystemsDataArray', 1426498862);
	}
	
	// initSystemPackages
	require_once(\DIR_userPackages . 'initSystemPackages.php');
	
	
	// Campaign Manager
	if (\UserUtils::checkUserPackagesPermission($userPackages, 2)) {
		require_once(\DIR_userPackages . 'initCampaignPackages.php');
	}
	
	// Data Manager
	if (\UserUtils::checkUserPackagesPermission($userPackages, 4)) {
		require_once(DIR_userPackages . 'initDataManagerPackages.php');
	}
	
	// Blacklist/Abmelder
	if (\UserUtils::checkUserPackagesPermission($userPackages, 6)) {
		require_once(DIR_userPackages . 'initBlacklistUnsubscribePackages.php');
	}
	
	// admin
	if (\UserUtils::checkUserPackagesPermission($userPackages, 999)) {
		require_once(DIR_userPackages . 'initAdminPackages.php');
	}
	
	
	// debug
	#$debugLogManager->logData('SESSION', $_SESSION);
	#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());
} catch (\Exception $e) {
	// debug
	#$debugLogManager->logData('SESSION', $_SESSION);
	#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());
	
	require(DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
        <title>CM - Campaign Manager</title>

        <link rel="stylesheet" type="text/css" href="yui/build/reset-fonts-grids/reset-fonts-grids.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/fonts/fonts-min.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/resize/assets/skins/sam/resize.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/layout/assets/skins/sam/layout.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/fonts/fonts-min.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/container/assets/skins/sam/container.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/button/assets/skins/sam/button.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/menu/assets/skins/sam/menu.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/tabview/assets/skins/sam/tabview.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/autocomplete/assets/skins/sam/autocomplete.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/calendar/assets/skins/sam/calendar.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/treeview/assets/skins/sam/treeview.css" />
        <link rel="stylesheet" type="text/css" href="css/jqcontextmenu.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/datatable/assets/skins/sam/datatable.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/resize/assets/skins/sam/resize.css" />
        <link rel="stylesheet" type="text/css" href="yui/build/accordionview/assets/skins/sam/reset-fonts.css" /> 
        <link rel="stylesheet" type="text/css" href="yui/build/accordionview/assets/skins/sam/accordionview.css" /> 

        <link rel="stylesheet" type="text/css" href="css/tabview.css" />
        <link rel="stylesheet" type="text/css" href="css/button.css" />
        <link rel="stylesheet" type="text/css" href="css/layout.css" />
        <link rel="stylesheet" type="text/css" href="css/2spalten.css" />
        <link rel="stylesheet" type="text/css" href="css/superTables.css" />
        
        <link rel="stylesheet" type="text/css" href="css/umsatz.css" />
        <link rel="stylesheet" type="text/css" href="css/campaign.css" />
        <link rel="stylesheet" type="text/css" href="css/tcm.css" />
		
		<link rel="stylesheet" type="text/css" href="css/newStyle.css" />

        <script type="text/javascript" src="yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
        <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
        <script type="text/javascript" src="yui/build/event/event-min.js"></script>
        <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
        <script type="text/javascript" src="yui/build/element/element-min.js"></script>
        <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
        <script type="text/javascript" src="yui/build/resize/resize-min.js"></script>
        <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
        <script type="text/javascript" src="yui/build/layout/layout-min.js"></script>
        <script type="text/javascript" src="yui/build/datasource/datasource-min.js"></script>
        <script type="text/javascript" src="yui/build/datatable/datatable-min.js"></script>
		<script type="text/javascript" src="yui/build/uploader/uploader-min.js"></script>
        <script type="text/javascript" src="yui/build/button/button-min.js"></script>
        <script type="text/javascript" src="yui/build/container/container-min.js"></script>
        <script type="text/javascript" src="yui/build/menu/menu-min.js"></script>
        <script type="text/javascript" src="yui/build/selector/selector-min.js"></script>
        <script type="text/javascript" src="yui/build/connection/connection-min.js"></script>
        <script type="text/javascript" src="yui/build/calendar/calendar-min.js"></script>
        <script type="text/javascript" src="yui/build/treeview/treeview-min.js"></script>
        <script type="text/javascript" src="yui/build/tabview/tabview-min.js"></script>
        <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
        <script type="text/javascript" src="yui/build/swf/swf-min.js"></script> 
        <script type="text/javascript" src="yui/build/charts/charts-min.js"></script> 
        <script type="text/javascript" src="yui/build/json/json-min.js"></script> 
        <script type="text/javascript" src="yui/build/slider/slider-min.js"></script> 
        <script type="text/javascript" src="yui/build/accordionview/utilities.js"></script>
        <script type="text/javascript" src="yui/build/slider/slider-min.js"></script> 
        <script type="text/javascript" src="yui/build/cookie/cookie-min.js"></script> 
        <script type="text/javascript" src="yui/build/connection/connection-min.js"></script> 
        <script type="text/javascript" src="yui/build/utilities/utilities.js"></script> 
        <script type="text/javascript" src="yui/build/container/container.js"></script>
		
		<script type="text/javascript">
			<?php
				echo \implode(\chr(13), $jsUserPackageDataArray);
			?>
		</script>
		
		<script type="text/javascript" src="javascript/jqcontextmenu.js"></script>
		<script type="text/javascript" src="javascript/createRequest.js"></script>
        <script type="text/javascript" src="javascript/setRequest.js"></script>
        <script type="text/javascript" src="javascript/setRequest_default.js"></script>
		<script type="text/javascript" src="javascript/setRequest_defaultWidthLoader.js"></script>
		<script type="text/javascript" src="javascript/ajaxRequest.js"></script>
        
        <script type="text/javascript" src="javascript/superTables.js"></script>
        
        <script type="text/javascript" src="ckeditor/ckeditor.js"></script>

        <link rel="SHORTCUT ICON" href="favicon.ico" type="image/x-icon;charset=binary">
		<link rel="icon" href="favicon.ico" type="image/x-icon;charset=binary">

        <script type="text/javascript">
            if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)){
                var uBrowser = "Firefox";
            } 

            function checkAnalyse (ziel) {
                var a_selected = document.getElementById('a_selected').value;
	
                if (a_selected === 1) {
                    setRequest_k_st(ziel);
                } else {
                    alert("Bitte wählen Sie mindestens eine Kampage aus.");
                    document.getElementById('analyse_selected').selected = true;
                }
            }

            function Trenner(number) {
                number = '' + number;
                if (number.length > 3) {
                    var mod = number.length % 3;
                    var output = (mod > 0 ? (number.substring(0,mod)) : '');
                    
                    for (var i=0; i < Math.floor(number.length / 3); i++) {
                        if ((mod === 0) && (i === 0)) {
                            output += number.substring(mod+ 3 * i, mod + 3 * i + 3);
                        } else {
                            // hier wird das Trennzeichen festgelegt mit '.'
                            output += '.' + number.substring(mod + 3 * i, mod + 3 * i + 3);
                        }
                    }
                    
                    return (output);
                } else {
                    return number;
                }
            }

            function runde(x) {
                var n = 1;
                var e = Math.pow(10, n);
                var k = (Math.round(x * e) / e).toString();
                
                if (k.indexOf('.') === -1) {
                    k += '.';
                }
                k += e.toString().substring(1);
                
                return k.substring(0, k.indexOf('.') + n+1);
            }

            function handleEnter(inField, e, p, sub_p) {
                var charCode;
			
                if (e && e.which) {
                    charCode = e.which;
                } else if (window.event) {
                    e = window.event;
                    charCode = e.keyCode;
                }
		
                if (charCode === 13) {
                    setRequest(p, sub_p);
                }
            }
			
            function str_replace(search, replace, subject) {
                return subject.split(search).join(replace);
            }

            SimpleContextMenu.setup({'preventDefault':true, 'preventForms':false});
            SimpleContextMenu.attach('container', 'CM1');
            SimpleContextMenu.attach('container2', 'CM2');
            SimpleContextMenu.attach('container3', 'CM_partner');
	    
            function menu_anz(feld, b) {
                if (feld !== '') {
                    vista = (document.getElementById(feld).style.display === 'none') ? '' : 'none';
                    document.getElementById(feld).style.display = vista;
		
                    if (feld === 'anfrage_details') {
                        vista2 = (document.getElementById('v_datum').innerHTML === 'Versanddatum') ? 'Anfrage vom' : 'Versanddatum';
                        document.getElementById('v_datum').innerHTML = vista2;
                    }
                }
            }

            function show_submenu(img) {
                var obj;

                obj = document.getElementById(img);
	
                var plus = new Image();
                plus.src = 'img/menu_down_small.png';
                
                var minus = new Image();
                minus.src = 'img/menu_up_small.png';
	
                obj.src = (obj.src === plus.src) ? minus.src : plus.src;
            }

            function mover(path) {
                document.getElementById(path).src = path + '.png';
            }

            function mout(path) {
                document.getElementById(path).src = path + '-deactive.png';
            }
			
			function showViewById(selectorId) {
				YAHOO.util.Dom.removeClass(selectorId, 'toggleHide');
				YAHOO.util.Dom.addClass(selectorId, 'toggleShow');
			}
			
			function hideViewById(selectorId) {
				YAHOO.util.Dom.removeClass(selectorId, 'toggleShow');
				YAHOO.util.Dom.addClass(selectorId, 'toggleHide');
			}
			
			function resetSelectedValueById(selectorId) {
				YAHOO.util.Dom.get(selectorId).selectedIndex = 0;
			}
			
			function removeSelectItemsById(selectorId) {
				var element = YAHOO.util.Dom.get(selectorId);
				
				for (var i = element.options.length - 1; i >= 0 ; i--) {
					element.remove(i);
				}
			}
			
			function setSelectedValueById(selectorId, content) {
				var element = YAHOO.util.Dom.get(selectorId);
				
				element.add(
					new Option(
						content,
						''
					)
				);
			}
			
			function convertValue2Integer(stringData) {
				stringData = str_replace('.', '', stringData);
				stringData = str_replace(',', '', stringData);
				
				return parseInt(stringData);
			}
			
			function convertValue2Float(valueData) {
				if (valueData !== 0) {
					valueData = str_replace(',', '.', valueData);
				}
				
				return parseFloat(valueData);
			}
			
			function convertFloat2Number(valueData) {
				if (valueData !== 0) {
					valueData = str_replace('.', ',', String(valueData));
				}
				
				return valueData;
			}
			
			function validateEmail(stringData) {
				var emailRegex = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
				
				return emailRegex.test(stringData);
			}
			
			function validateStrLengthData(stringData, validateLenght) {
				var val = stringData.trim();
				
				return (val.length < parseInt(validateLenght) ? false : true);
			}
                        
                        function checkStrLengthData(stringData, maxLenght) {
				var val = stringData.trim();
				
				return (val.length > parseInt(maxLenght) ? false : true);
			}
			
			function validateDeZipCode(stringData) {
				var result = true;
				var zipCode = stringData.trim();
				
				if (isNaN(parseInt(zipCode)) 
					|| zipCode.toString().length !== 5
				) {
					result = false;
				}
				
				return result;
			}
			
			function getCheckedValue(imputObject) {
				var result = '';
				
				for (var i = 0; i < imputObject.length; i++) {
					if (imputObject[i].checked) {
						result = imputObject[i].value;
					}
				}
				
				return result;
			}
			
			function parseCampaignDateByElementsIds(dateElementId, hourElementId, minElementId) {
				var dataDataArray = YAHOO.util.Dom.get(dateElementId).value.split('-');
				
				return new Date(
					parseInt(dataDataArray[0]),
					parseInt(dataDataArray[1] - 1),
					parseInt(dataDataArray[2]),
					parseInt(YAHOO.util.Dom.get(hourElementId).value),
					parseInt(YAHOO.util.Dom.get(minElementId).value)
				);
			}
			
			function checkIfDeliveryDateIsValide(dateElementId, hourElementId, minElementId) {
				var result = true;
				var now = new Date();
				now.setMinutes(now.getMinutes() + 10);
				
				var selectedDeliveryDate = parseCampaignDateByElementsIds(
					dateElementId,
					hourElementId,
					minElementId
				);
				
				if (isNaN(selectedDeliveryDate.getTime()) || 
					selectedDeliveryDate.getTime() < now.getTime()
				) {
					result = false;
				}
				
				return result;
			}
			
			function validateCampaignDeliveryDate(dateElementId, hourElementId, minElementId) {
				if (!checkIfDeliveryDateIsValide(dateElementId, hourElementId, minElementId)) {
					uncheckCampaign_useCampaignStartDate();
					disableCampaign_useCampaignStartDate();
					
					alert(unescape("Versanddatum mu%DF in der Zukunft liegen!"));
					return false;
				} else {
					enableCampaign_useCampaignStartDate();
				}
			}
			
			function checkNvSendCampaignDate(hvCampaignDateObject) {
				var nvCampaignDateObject = parseCampaignDateByElementsIds(
					'date',
					'campaign_versandHour',
					'campaign_versandMin'
				);
				
				var result = true;
				if (nvCampaignDateObject.getTime() <= hvCampaignDateObject.getTime()) {
					result = false;
				}
				
				return result;
			}
			
			function disableCampaign_useCampaignStartDate() {
				if (YAHOO.util.Dom.get('campaign_use_campaign_start_date_0') !== null) {
					YAHOO.util.Dom.get('campaign_use_campaign_start_date_0').disabled = 'disabled';
					YAHOO.util.Dom.get('campaign_use_campaign_start_date_1').disabled = 'disabled';
				}
			}
			
			function enableCampaign_useCampaignStartDate() {
				if (YAHOO.util.Dom.get('campaign_use_campaign_start_date_0') !== null) {
					YAHOO.util.Dom.get('campaign_use_campaign_start_date_0').disabled = '';
					YAHOO.util.Dom.get('campaign_use_campaign_start_date_1').disabled = '';
				}
			}
			
			function uncheckCampaign_useCampaignStartDate() {
				if (YAHOO.util.Dom.get('campaign_use_campaign_start_date_0') !== null) {
					YAHOO.util.Dom.get('campaign_use_campaign_start_date_0').checked = 'checked';
					YAHOO.util.Dom.get('campaign_use_campaign_start_date_1').checked = '';
				}
			}
			
			function arrayToQueryString(dataArray, key) {
				var resultDataArray =[];
				for (var i = 0; i < dataArray.length; i++) {
					resultDataArray.push(key + '=' + dataArray[i]);
				}
				
				return resultDataArray.join('&');
			}
        </script>
            
        <style type="text/css" media="screen">
            #container_update .ft .button-group {
                height: 25px;
            }
			
			.ioLabel {
				float: none;
				display: inline;
				color: #000;
				padding-left: 10px;
			}
        </style>
		
		<link rel="stylesheet" type="text/css" href="css/font-awesome/css/font-awesome.min.css" />
    </head>
    <body class="yui-skin-sam" id="topBody">
        <script type="text/javascript">
            var oldRow = null;
        </script>

        <input type="hidden" id="page" value="" />
        <input type="hidden" id="page_sub" value="" />
        <input type="hidden" id="last_pid" name="last_pid" value="" />
        <input type="hidden" id="error" value="1" />
        <input type="hidden" id="stats_monat" value="" />
        <input type="hidden" id="stats_jahr" value="" />
        <input type="hidden" id="stats_partner" value="" />
        <input type="hidden" id="genart" value="" />
        <input type="hidden" id="impnr" value="" />
        <input type="hidden" id="bottom_height" value="" />
        <input type="hidden" id="selected_row" value="" />
        <input type="hidden" id="selected_row_color" value="" />
        <input type="hidden" id="selected_row_h" value="" />
        <input type="hidden" id="selected_button" value="" />
		
		<!-- @deprecated -->
		<input type="hidden" id="mandant" value="<?php echo \RegistryUtils::get('clientEntity')->getAbkz(); ?>" />
        <input type="hidden" id="userID" value="<?php echo \RegistryUtils::get('userEntity')->getBenutzer_id(); ?>" />

		<?php
			try {
				// Campaign Manager
				if (\UserUtils::checkUserPackagesPermission($userPackages, 2)) {
					include_once(DIR_Module_Campaigns . 'actionsNavi.html');
					
					include_once(DIR_Module_Campaigns . 'dialogForm.php');
				}

				// Data Manager
				if (\UserUtils::checkUserPackagesPermission($userPackages, 4)) {
					include_once(DIR_Module_DataManager . 'actionsNavi.html');
					
					include_once(DIR_Module_DataManager . 'dialogForm.php');
				}

				// Blacklist/Abmelder
				if (\UserUtils::checkUserPackagesPermission($userPackages, 6)) {
					include_once(DIR_Module_BlacklistUnsubscribe . 'dialogForm.php');
				}

				// admin
				if (\UserUtils::checkUserPackagesPermission($userPackages, 999)) {
					include_once(DIR_Module_Admin . 'actionsNavi.html');
					
					include_once(DIR_Module_Admin . 'dialogForm.php');
				}
			} catch (\Exception $e) {
				require_once(DIR_configs . 'exceptions.php');
				
				die($exceptionMessage);
			}
        ?>
		
        <div id="top_sync"></div>
        <div id="top1">
<!--             <table width="100%" cellpadding="0" cellspacing="0" border="0" align="left" bgcolor="#40547A"> -->
                 <table width="100%" cellpadding="0" cellspacing="0" border="0" align="left" bgcolor="#FFFFFF">  
                <tr>
                    <td width="250">
                        <img src="img/treaction_logo.jpg" height="40" style="padding-left:5px" />
                        <div id="loaderimg" style="float:right; padding-top:5px; margin-right:3px;"></div>
                    </td>
                    <td valign="bottom" style="padding:5px 1cm 0.4cm;">
                        <span style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; font-weight:bold;">
                            <?php echo \utf8_encode(\RegistryUtils::get('userEntity')->getVorname() . ' ' . \RegistryUtils::get('userEntity')->getNachname()) . " | " . $clientChangeForm; ?>
                        </span><br />
                        <span style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#000000;">
                            <a href="https://www.campaign-treaction.de/meta/login.php?logout=1" style="color:#000000">Logout</a>
                        </span>
                    </td>
                    <td align="right">
                        <div style="color:#000000;padding:5px;width:340px;">
							<strong>Version: <?php echo version;?></strong><br
                            <strong>Support</strong><br >
                            Email: <a href="mailto:support@treaction.de" style="color:#000000">support@treaction.de</a> | Tel.: +49 (0) 721 909 810 -70
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="top_check" style="background-color:#FFFFFF; font-size:11px; text-align:center;height:35px">
            <img src="img/logo/<?php echo \RegistryUtils::get('clientEntity')->getAbkz(); ?>.gif" style="margin-top:3px;border:0px;" />
        </div>
        <div id="left1" style="background-color:#F2F2F2; height:100%;width:193px">
            <div id="treewrapper">
                <ul id="mymenu">
                    <?php
						try {
							// Campaign Manager
							if (\UserUtils::checkUserPackagesPermission($userPackages, 2)) {
								include_once(DIR_Module_Campaigns . 'modulNavi.php');
							}

							// Data Manager
							if (\UserUtils::checkUserPackagesPermission($userPackages, 4)) {
								include_once(DIR_Module_DataManager . 'modulNavi.php');
							}

							// Blacklist/Abmelder
							if (\UserUtils::checkUserPackagesPermission($userPackages, 6)) {
								include_once(DIR_Module_BlacklistUnsubscribe . 'modulNavi.php');
							}

							// Datenauskunft
							if (\UserUtils::checkUserPackagesPermission($userPackages, 7)) {
								include_once(DIR_Module . 'Dais/modulNavi.php');
							}

							// admin
							if (\UserUtils::checkUserPackagesPermission($userPackages, 999)) {
								include_once(DIR_Module_Admin . 'modulNavi.php');
							}
						} catch (\Exception $e) {
							require_once(DIR_configs . 'exceptions.php');
							
							die($exceptionMessage);
						}
                    ?>
                </ul>
            </div>
        </div>
        <div id="inboxToolbar"></div>
        <div id="standard" style="font-size:12px">
            <div id="top2" style="font-size:11px;background-color:#FFFFFF;height:66px;border-top:1px solid #808080">
				<div style="padding:5px;background-color: #8e8901">
                    <div id="menu_top_img" style="float:left;margin-left:5px;margin-right:5px;"></div>
                    <div id="menu_top_label" style="margin-top:2px;font-size:12px;color:#FFFFFF;font-weight:bold;"></div>
                </div>
                <div style="height:36px;background-image:url(img/menu_bg.png);display:block;border-top:1px solid #808080;width:100%;vertical-align:middle;padding-top:1px;padding-bottom:1px;">
                    <div id="menu_buttons" style="margin:0px;padding:0px;height:30px;">
                        <?php
							try {
								// Campaign Manager
								if (\UserUtils::checkUserPackagesPermission($userPackages, 2)) {
									include_once(DIR_Module_Campaigns . 'subViewBar.php');
								}

								// Data Manager
								if (\UserUtils::checkUserPackagesPermission($userPackages, 4)) {
									include_once(DIR_Module_DataManager . 'subViewBar.php');
								}

								// Blacklist/Abmelder
								if (\UserUtils::checkUserPackagesPermission($userPackages, 6)) {
									include_once(DIR_Module_BlacklistUnsubscribe . 'subViewBar.php');
								}

								// admin
								if (\UserUtils::checkUserPackagesPermission($userPackages, 999)) {
									include_once(DIR_Module_Admin . 'subViewBar.php');
								}
							} catch (\Exception $e) {
								require_once(DIR_configs . 'exceptions.php');
								
								die($exceptionMessage);
							}
						?>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="update_content"></div>
        <div id="alert"></div>
		<div id="bottom"></div>
		
        <script type="text/javascript" src="yui/build/accordionview/accordionview.js"></script>
        
        <script type="text/javascript" src="javascript/main.js"></script>
        <script type="text/javascript" src="javascript/dialog.js"></script>
        <script type="text/javascript" src="javascript/kalender.js"></script>
		
		<?php
			/**
			 * loadModuleJavascriptByUserPackages
			 */
			\loadModuleJavascriptByUserPackages($userPackages);
        ?>

        <script type="text/javascript">
            var menuAcc = new YAHOO.widget.AccordionView('mymenu', {
                collapsible: true,
                expandable: false,
                width: '193px',
                hoverActivated: false,
                hoverTimeout: 0,
                animate: true,
                animationSpeed: '0.2',
                expandItem: 0
            });
			
			if (campaignManagerView) {
				setRequest('kampagne/k_main.php?new=1', 'k');
			} else if (dataManagerView) {
				setRequest('limport/overview.php', 'dm_ov');
			} else if (blacklistUnsubscribeView) {
				setRequest('blacklist/overview.php', '');
			} else if (daisView) {
				setRequest('dais/index2.php', 'dais');
			} else if (adminView) {
				setRequest('admin/user/', 'admin_user');
			}
            
            window.location.href = '#topBody';
			
			setInterval(function() {
				processAjaxRequest(
					'AjaxRequests/ajax.php',
					'actionMethod=system' 
						+ '&actionType=fixSessionTimeout'
					,
					'',
					function (ajaxResult) {
						console.log(ajaxResult);
					}
				);
			}, 1000 * 60 * 10); // alle 10 min
        </script>
    </body>
</html>