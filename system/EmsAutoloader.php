<?php
class EmsAutoloader {
	/**
	 * init
	 * 
	 * @return	void
	 * @deprecated use library EmsAutoloader
	 */
	public static function init() {
		try {
			\spl_autoload_register(
				array(
					__CLASS__, 'loadEntity'
				)
			);
			
			\spl_autoload_register(
				array(
					__CLASS__, 'loadUtils'
				)
			);
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
	
	/**
	 * startAndCheckClientSession
	 * 
	 * return void
	 */
	public static function startAndCheckClientSession() {
		\session_start();
		
		if (\strlen($_SESSION['mandant']) === 0) {
			\header('Location: https://www.campaign-treaction.de/meta/login.php');
			exit();
		}
	}
	
	/**
	 * initErrorReporting
	 * 
	 * @return void
	 */
	public static function initErrorReporting() {
		\error_reporting(null);
		\ini_set('display_errors', false);
		
		if (isset($_SESSION['benutzer_id']) 
			&& (int) $_SESSION['benutzer_id'] === 24
		) {
			\error_reporting(E_ALL);
			\ini_set('display_errors', true);
		}
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              load - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * loadEntity
	 * 
	 * @param string $className
	 * return void
	 */
	protected static function loadEntity($className) {
		if ((\preg_match('/Entity$/ui', $className))) {
			if ((\file_exists(\DIR_Entity . $className . '.php')) === true) {
				require_once(\DIR_Entity . $className . '.php');
			}
		}
	}

	/**
	 * loadUtils
	 * 
	 * @param string $className
	 * return void
	 */
	protected static function loadUtils($className) {
		if ((\preg_match('/Utils$/ui', $className))) {
			if ((\file_exists(\DIR_Utils . $className . '.php')) === true) {
				require_once(\DIR_Utils . $className . '.php');
			}
		}
	}

}