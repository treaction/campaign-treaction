<?php
\header('Content-Type: text/html; charset=utf-8');
#test

/**
 * getAjaxActionPath
 * 
 * @param type $actionType
 * @return string
 * @throws \InvalidArgumentException
 * @throws \BadMethodCallException
 */
function getAjaxActionPath($actionType) {
	$acceptedActionsMethodDataArray = array(
		'campaign' => 'Campaign',
		'client' => 'Client',
		'system' => 'System',
		'customer' => 'Customer',
	);
	
	// actionType
	if (\strlen($actionType) === 0) {
		throw new \InvalidArgumentException('No method given!', 1433239838);
	} elseif (!\array_key_exists($actionType, $acceptedActionsMethodDataArray)) {
		throw new \BadMethodCallException('No valid method given: ' . $actionType, 1433240148);
	}
	
	return __DIR__ . \DIRECTORY_SEPARATOR . 'Actions/' . $acceptedActionsMethodDataArray[$actionType] . \DIRECTORY_SEPARATOR;
}



/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * 
 * $debugLogManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);

if ((boolean) \BaseUtils::isProdSystem()) {
	$debugLogManager->logData('prodSystem', true);
} elseif ((boolean) \BaseUtils::isTestSystem()) {
	$debugLogManager->logData('testSystem', true);
} else {
	$debugLogManager->logData('devSystem', true);
}

$actionMethod = isset($_POST['actionMethod']) ? \DataFilterUtils::filterData($_POST['actionMethod']) : null;
$debugLogManager->logData('actionMethod', $actionMethod);

$actionType = isset($_POST['actionType']) ? \DataFilterUtils::filterData($_POST['actionType']) : null;
$debugLogManager->logData('actionType', $actionType);

$createSelectItem = isset($_POST['createSelectItem']) ? (boolean) \intval($_POST['createSelectItem']) : true;
$debugLogManager->logData('createSelectItem', $createSelectItem);

$disableNotSelectedItem = isset($_POST['disableNotSelectedItem']) ? (boolean) \intval($_POST['disableNotSelectedItem']) : false;
$debugLogManager->logData('disableNotSelectedItem', $disableNotSelectedItem);

$jsonData = null;
$addDefaultItem = false;

try {
	/**
	 * getAjaxActionPath
	 */
	$actionPath = \getAjaxActionPath($actionMethod);
	$debugLogManager->logData('actionPath', $actionPath);
	
	// actionType
	if (\strlen($actionType) === 0) {
		throw new \InvalidArgumentException('No actionType given!', 1424768295);
	}
	
	if ((\file_exists($actionPath . $actionType . '.php')) === true) {
		switch ($actionMethod) {
			case 'campaign':
				$mandant = \RegistryUtils::get('clientEntity')->getAbkz();
				require_once(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

				/**
				 * init
				 * 
				 * $campaignManager
				 * $swiftMailerWebservice
				 */
				require_once(DIR_configsInit . 'initCampaignManager.php');
				/* @var $campaignManager \CampaignManager */

				require_once(DIR_configsInit . 'initSwiftMailerWebservice.php');
				/* @var $swiftMailerWebservice \SwiftMailerWebservice */
				
				// campaignFunctions
				require_once(DIR_functions . 'campaignFunctions.php');
				break;
			
			case 'client':
				/**
				 * init
				 * 
				 * $clientManager
				 */
				require_once(DIR_configsInit . 'initClientManager.php');
			   /* @var $clientManager \ClientManager */
				break;
			
			case 'customer':
				$mandant = \RegistryUtils::get('clientEntity')->getAbkz();
				require_once(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

				/**
				 * init
				 * 
				 * $campaignManager
				 * $swiftMailerWebservice
				 */
				require_once(DIR_configsInit . 'initCampaignManager.php');
				/* @var $campaignManager \CampaignManager */
				break;
		}


		// debug
		$debugLogManager->beginGroup($actionMethod);

		// actionMethod
		require_once($actionPath . $actionType . '.php');

		// debug
		$debugLogManager->endGroup();
	} else {
		throw new \BadMethodCallException('Unknown actionType: ' . $actionType, 1424768303);
	}
} catch (\Exception $e) {
	$jsonData['success'] = false;
	$jsonData['message'] = $e->getMessage();
	
	require(DIR_configs . 'exceptions.php');
}

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());

echo \json_encode($result);