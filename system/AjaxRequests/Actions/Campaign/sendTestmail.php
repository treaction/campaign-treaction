<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */


/**
 * processDeliverySystemWebserviceSendTestmail
 * 
 * @param \CampaignEntity $campaignEntity
 * @param mixed $recipientListId
 * @param array $recipientsDataArray
 * @param \CampaignManager $campaignManager
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @return array
 * @throws \DomainException
 */
function processDeliverySystemWebserviceSendTestmail(\CampaignEntity $campaignEntity, $recipientListId, array $recipientsDataArray, \CampaignManager $campaignManager, \ClientManager $clientManager, \debugLogManager $debugLogManager) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
    /**
     * getDeliverySystemDistributorWidthDeliverySystemEntityById
     * 
     * debug
     */
    $deliverySystemDistributorWidthClientDeliverySystemEntity = $clientManager->getDeliverySystemDistributorWidthDeliverySystemEntityById($campaignEntity->getDsd_id());
	if (!($deliverySystemDistributorWidthClientDeliverySystemEntity instanceof \DeliverySystemDistributorWidthClientDeliverySystemEntity)) {
		throw new \DomainException('invalid DeliverySystemDistributorWidthClientDeliverySystemEntity', 1424766373);
	}
	$debugLogManager->logData('deliverySystemDistributorWidthClientDeliverySystemEntity', $deliverySystemDistributorWidthClientDeliverySystemEntity);
	
	
	/**
	 * DeliverySystemFactory::getAndInitDeliverySystem
	 * 
	 * debug
	 */
	$deliverySystemWebservice = \DeliverySystemFactory::getAndInitDeliverySystem($deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity());
	$debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);
    
    
    /**
     * connect to deliveryWebservice
     */
    $deliverySystemWebservice->login();
	$asps = $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp();
                 switch ($asps) {
                            case '4Wave 2018':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Marketing Sandbox':
                            case 'Stellar 2018':
                            case 'Stellar Basis':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Basis':
                            case '4Wave Black':
                            case 'Stellar Black':
                            case 'Stellar Reserve':
                            case '4Wave Extra':   
			    case 'Lead World Black': 
                            case 'BlueLeads2019':
                            case 'BlueLeads Basis':  
                            case 'fulle.media Basis':
                            case 'fulle.media 2019':  
                            case 'BlueLeads Extra':
                            case 'fulle.media Extra': 
                            case 'BlueLeads Black':
                            case 'fulle.media Black':
                            case 'CEOO':
                            case 'CEOO Black':    
                            case 'Maileon':
                            case 'Mail-In-One':    
                              $asp = 'Maileon';
                                break;
                            case 'Sendeffect':
                            case 'Stellar SE':
                            case '4Wave SE':
                            case 'CEOO SE':    
                              $asp = 'Sendeffect';
                                break;  
                            case 'Promio';
                                $asp = 'Promio';
                                break;
                            case 'Broadmail';
                                $asp = 'Broadmail';
                                break;
                            default:
                             $asp = 'Maileon';
                                break;
                        }

	if ((\file_exists(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'sendTestmail.php')) === true) {
		require_once(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'sendTestmail.php');
	} else {
		$file = __FILE__;
		require_once(DIR_deliverySystems . 'noDeliverySystemImplemented.php');
	}
    
    /**
     * disconnect to deliveryWebservice
     */
    $deliverySystemWebservice->logout();
	
	$debugLogManager->endGroup();
	
	return $blacklistRecipients;
}

/**
 * processBlacklistItemsForTcmGroup
 * 
 * @param array $blacklistRecipients
 * @param \CampaignEntity $campaignEntity
 * @param \CampaignManager $campaignManager
 * @param \ClientManager $clientManager
 * @param \SwiftMailerWebservice $swiftMailerWebservice
 * @param \debugLogManager $debugLogManager
 * @return string
 */
function processBlacklistItemsForTcmGroup(array $blacklistRecipients, \CampaignEntity $campaignEntity, \CampaignManager $campaignManager, \ClientManager $clientManager, \SwiftMailerWebservice $swiftMailerWebservice, \debugLogManager $debugLogManager) {
	if (\count($blacklistRecipients) > 0) {
		/**
		 * getCampaignRecipientUserEntity
		 */
		$campaignRecipientUserEntity = \getCampaignRecipientUserEntity(
			$campaignEntity,
			$clientManager,
			$debugLogManager
		);
		
		$result = \createAndSendEmailToRecipientsDataArray(
			array(
				'sami.jarmoud@treaction.de' => 'TCM - Technik'
			),
			'Testmail Adresse befand sich auf der Blacklist', // subject
			'----------------------------------------------' . \chr(13) 
				. 'Adresse: ' . \implode(';', $blacklistRecipients) . \chr(13) 
				. 'Bearbeiter: ' . $campaignRecipientUserEntity->getName() . ' ' . $campaignRecipientUserEntity->getVorname() . ', ' . $campaignRecipientUserEntity->getEmail() . \chr(13) 
				. 'Mandant: ' . \RegistryUtils::get('clientEntity')->getMandant() . \chr(13) 
				. 'Datum/Uhrzeit: ' . \DateUtils::getCurrentDateTime() . \chr(13) 
				. '----------------------------------------------'
			, // emailBody
			$swiftMailerWebservice,
			$campaignEntity,
			$campaignManager,
			$debugLogManager
		);
		$debugLogManager->logData('createAndSendEmailToRecipientsDataArray', $result);
	} else {
		$result = 'Email/s erfolgreich versendet!';
	}
	
	return $result;
}



$campaignId = isset($_POST['campaign']['k_id']) ? \intval($_POST['campaign']['k_id']) : 0;
$debugLogManager->logData('campaignId', $campaignId);

$recipientListId = isset($_POST['recipient']['listId']) 
	? \DataFilterUtils::filterData(
		$_POST['recipient']['listId'],
		\DataFilterUtils::$validateFilterDataArray['string']
	) 
	: null
;
$debugLogManager->logData('recipientListId', $recipientListId);

$recipientsDataArray = isset($_POST['recipients']) 
	? $_POST['recipients']
	: null
;
$debugLogManager->logData('recipientsDataArray', $recipientsDataArray);

if ($campaignId > 0) {
	/*if (\strlen($recipientListId) === 0) {
		throw new \InvalidArgumentException('no recipientListId', 1424766452);
	}*/
	
	if (\count($recipientsDataArray) === 0) {
		$jsonData['message'] = 'no recipients';
		
		throw new \InvalidArgumentException($jsonData['message'], 1424766465);
	}
	
	require_once(DIR_configsInit . 'initClientManager.php');
	/* @var $clientManager \ClientManager */
	
	// Factory
	require_once(DIR_Factory . 'DeliverySystemFactory.php');
	
	/**
	 * getCampaignAndCustomerDataItemById
	 * 
	 * debug
	 */
	$campaignEntity = $campaignManager->getCampaignAndCustomerDataItemById($campaignId);
	if (!($campaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
		$jsonData['message'] = 'no CampaignWidthCustomerAndContactPersonEntity';
		
		throw new \DomainException($jsonData['message'], 1424766475);
	}
	$debugLogManager->logData('campaignEntity', $campaignEntity);
	
	/**
	 * processDeliverySystemWebserviceSendTestmail
	 */
       
	$blacklistRecipients = \processDeliverySystemWebserviceSendTestmail(
		$campaignEntity,
		$recipientListId,
		$recipientsDataArray,
		$campaignManager,
		$clientManager,
		$debugLogManager
	);
       
	
	/**
	 * addNewLogEntryWidthLogdata
	 * 
	 * debug
	 */
	$idLogItem = \addNewLogEntryWidthLogdata(
		$campaignManager,
		$campaignEntity,
		21,
		array(
			'recipientsDataArray' => $recipientsDataArray,
			'blacklistRecipients' => $blacklistRecipients
		)
	);
	$debugLogManager->logData('idLogItem', $idLogItem);
	
	/**
	 * processBlacklistItemsForTcmGroup
	 */
	$sendEmailResult = \processBlacklistItemsForTcmGroup(
		$blacklistRecipients,
		$campaignEntity,
		$campaignManager,
		$clientManager,
		$swiftMailerWebservice,
		$debugLogManager
	);
	
	$resultData = array(
		'success' => true,
		'message' => $sendEmailResult
	);
} else {
	$jsonData['message'] = 'invalid campaignId: ' . $campaignId;
	
	throw new \InvalidArgumentException($jsonData['message'], 1424766235);
}

$result = array(
	'content' => $resultData,
	'jsonData' => $resultData
);