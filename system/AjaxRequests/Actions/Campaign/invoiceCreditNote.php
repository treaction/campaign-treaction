<?php
/* @var $campaignManager \CampaignManager */
/* @var $swiftMailerWebservice \SwiftMailerWebservice */
/* @var $debugLogManager \debugLogManager */

require_once(DIR_Module_Campaigns . 'Packages' . \DIRECTORY_SEPARATOR . 'Invoice' . \DIRECTORY_SEPARATOR . 'InvoiceCreditNote.php');

$invoiceCreditNote = new \InvoiceCreditNote();
$invoiceCreditNote->setCampaignManager($campaignManager);
$invoiceCreditNote->setDebugLogManager($debugLogManager);
$jsonData = $invoiceCreditNote->main($_POST['invoice']);

$result = array(
	'content' => $jsonData,
	'jsonData' => $jsonData
);