<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */


/**
 * processDeliverySystemWebserviceAndUpdateCampaignData
 * 
 * @param \CampaignEntity $campaignEntity
 * @param \CampaignManager $campaignManager
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @throws \DomainException
 * @return void
 */
function processDeliverySystemWebserviceAndUpdateCampaignData(\CampaignEntity $campaignEntity, \CampaignManager $campaignManager, \ClientManager $clientManager, \debugLogManager $debugLogManager) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getDeliverySystemDistributorWidthDeliverySystemEntityById
	 * 
	 * debug
	 */
	$deliverySystemDistributorWidthClientDeliverySystemEntity = $clientManager->getDeliverySystemDistributorWidthDeliverySystemEntityById($campaignEntity->getDsd_id());
	if (!($deliverySystemDistributorWidthClientDeliverySystemEntity instanceof \DeliverySystemDistributorWidthClientDeliverySystemEntity)) {
		throw new \DomainException('invalid DeliverySystemDistributorWidthClientDeliverySystemEntity', 1424766373);
	}
	$debugLogManager->logData('deliverySystemDistributorWidthClientDeliverySystemEntity', $deliverySystemDistributorWidthClientDeliverySystemEntity);
	
	
	/**
     * DeliverySystemFactory::getAndInitDeliverySystem
	 * 
	 * debug
     */
    $deliverySystemWebservice = \DeliverySystemFactory::getAndInitDeliverySystem($deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity());
    $debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);
	
	
	/**
     * connect to deliveryWebservice
     */
    $deliverySystemWebservice->login();
		$asps = $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp();
                 switch ($asps) {
                            case '4Wave 2018':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Marketing Sandbox':
                            case 'Stellar 2018':
                            case 'Stellar Basis':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Basis':
                            case '4Wave Black':
                            case 'Stellar Black':
                            case 'Stellar Reserve':
                            case '4Wave Extra':
			    case 'Lead World Black':    
                            case 'BlueLeads2019':
                            case 'BlueLeads Basis':    
                            case 'fulle.media Basis':
                            case 'fulle.media 2019':
                            case 'BlueLeads Extra':
                            case 'fulle.media Extra': 
                            case 'BlueLeads Black':
                            case 'fulle.media Black':     
                            case 'CEOO':
                            case 'CEOO Black':    
                            case 'Maileon':
                            case 'Mail-In-One':    
                              $asp = 'Maileon';
                                break;
                            case 'Sendeffect':
                            case 'Stellar SE':
                            case '4Wave SE':
                            case 'CEOO SE':    
                              $asp = 'Sendeffect';
                                break;  
                            case 'Promio';
                                $asp = 'Promio';
                                break;
                            case 'Broadmail';
                                $asp = 'Broadmail';
                                break;
                            default:
                             $asp = 'Maileon';
                                break;
                        }
	if ((\file_exists(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'updateSyncCampaign.php')) === true) {
		$actionId = 16;
		require_once(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'updateSyncCampaign.php');
	} else {
		$file = __FILE__;
		require_once(DIR_deliverySystems . 'noDeliverySystemImplemented.php');
	}
	
	/**
     * disconnect from deliveryWebservice
     */
    $deliverySystemWebservice->logout();
	
	// debug
	$debugLogManager->endGroup();
}



$campaignId = isset($_POST['campaign']['kid']) ? \intval($_POST['campaign']['kid']) : 0;
$debugLogManager->logData('campaignId', $campaignId);


if ($campaignId > 0) {
	/**
	 * getCampaignDataItemById
	 * 
	 * debug
	 */
	$campaignEntity = $campaignManager->getCampaignDataItemById($campaignId);
	/* @var $campaignId \CampaignEntity */
	if (!($campaignEntity instanceof \CampaignEntity)) {
		$jsonData['message'] = 'no CampaignEntity';
		
		throw new \DomainException($jsonData['message'], 1424766384);
	}
	$debugLogManager->logData('campaignEntity', $campaignEntity);

	if (\strlen($campaignEntity->getMail_id()) > 0) {
		require_once(DIR_configsInit . 'initClientManager.php');
		/* @var $clientManager \ClientManager */
		
		// Factory
		require_once(DIR_Factory . 'DeliverySystemFactory.php');

		/**
		 * processDeliverySystemWebserviceAndUpdateCampaignData
		 */
		\processDeliverySystemWebserviceAndUpdateCampaignData(
			$campaignEntity,
			$campaignManager,
			$clientManager,
			$debugLogManager
		);
	}

	$resultData = '<span style="color:#333333;font-size:12px;font-weight:bold;">'
			. '<img src="img/Tango/22/actions/dialog-apply.png" style="margin-bottom:-4px;" /> Aktualisierung abgeschlossen'
		. '</span>'
	;
} else {
	$jsonData['message'] = 'invalid campaignId: ' . $campaignId;
	
	throw new \InvalidArgumentException($jsonData['message'], 1424766235);
}

$result = array(
	'content' => $resultData,
	'jsonData' => $resultData
);