<?php

/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

// Factory
require_once(DIR_Factory . 'DeliverySystemFactory.php');


$mandant = $_SESSION['mandant'];

switch ($mandant) {
    case 'demoClient':
        $maspIdArray = array(
            '11',
            '13'
        );
        $recipientsID = '11';
        $contactFilterId = '40';
        $url ='https://sf14.sendsfx.com/xml.php';
        break;
    
     case 'stellarPerformance':
         $maspIdArray = array(
             '10',
             '12'
        );
        $recipientsID = '9';
        $contactFilterId = '12';
        $url ='https://sf14.sendsfx.com/xml.php';
        break;
    
     case 'blueleads':
       $maspIdArray = array(
           '14',
           '15'
        );
       $recipientsID = '1180';
       $contactFilterId = '44';
       $url ='https://sf21.sendsfx.com/xml.php';
        break;

    default:
        
         $maspIdArray = array(
             '10',
             '12'
        );
         $recipientsID = '9';
        $contactFilterId = '12';
        $url ='https://sf14.sendsfx.com/xml.php';
        
        break;
}

foreach ($maspIdArray as $maspId) {
    // debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
    /**
     * getClientDeliveryEntityByMaspId
     * 
     * debug
     */
    $ClientDeliverySystemWidthDeliverySystemEntity = $clientManager->getClientDeliveryWidthDeliverySystemEntityByMaspId($maspId);
    	
   $debugLogManager->logData('ClientDeliverySystemWidthDeliverySystemEntity', $ClientDeliverySystemWidthDeliverySystemEntity);
   
  /**
    * getClientDeliveryDataByClientIdAndDeliverySystemAsp
    * 
    * gilt nur f�r alte kampagnen, wenn die db aktualisiert wurde kann dieser code entfernt werden
    */
    $clientDeliveryEntity = $clientManager->getClientDeliveryDataByClientIdAndDeliverySystemAsp(
            $ClientDeliverySystemWidthDeliverySystemEntity->getM_id(),
            $ClientDeliverySystemWidthDeliverySystemEntity->getDeliverySystem()->getAsp_abkz()
    );
    if (!($clientDeliveryEntity instanceof \ClientDeliveryEntity)) {
            throw new \DomainException('invalid ClientDeliveryEntity');
    }

     $debugLogManager->logData('clientDeliveryEntity', $clientDeliveryEntity);

     /**
     * DeliverySystemFactory::getAndInitInstance
	 * 
	 * debug
     */
    $deliverySystemWebservice = \DeliverySystemFactory::getAndInitInstance(
		$ClientDeliverySystemWidthDeliverySystemEntity->getDeliverySystem()->getAsp_abkz(),
		$clientDeliveryEntity
	);
    $debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);
    
        
    /**
     * connect to deliveryWebservice
     */
    $deliverySystemWebservice->login();

	if ((\file_exists(DIR_deliverySystems . $ClientDeliverySystemWidthDeliverySystemEntity->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'syncTestmail.php')) === true) {
		require_once(DIR_deliverySystems . $ClientDeliverySystemWidthDeliverySystemEntity->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'syncTestmail.php');
	} else {
		#$file = __FILE__;
		#require_once(DIR_deliverySystems . 'noDeliverySystemImplemented.php');
	}
    
    /**
     * disconnect to deliveryWebservice
     */
    $deliverySystemWebservice->logout();
	
	$debugLogManager->endGroup();
        
}
$result = array(
	'content' => $resultData,
	'jsonData' => $resultData
);

