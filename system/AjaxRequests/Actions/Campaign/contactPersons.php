<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */


$customerId = isset($_POST['customerId']) ? \intval($_POST['customerId']) : 0;
$debugLogManager->logData('customerId', $customerId);

$contactPersonId = isset($_POST['contactPersonId']) ? \intval($_POST['contactPersonId']) : 0;
$debugLogManager->logData('contactPersonId', $contactPersonId);

if ($customerId > 0) {
	/**
	 * getContactPersonDataItemsByQueryParts
	 * 
	 * debug
	 */
	$contactPersonDataArray = $campaignManager->getContactPersonDataItemsByQueryParts(
		array(
			'WHERE' => array(
				'kundeId' => array(
					'sql' => '`kunde_id`',
					'value' => $customerId,
					'comparison' => 'integerEqual'
				)
			)
		),
		\PDO::FETCH_CLASS
	);
	$debugLogManager->logData('contactPersonDataArray', $contactPersonDataArray);

	if (\count($contactPersonDataArray) > 0) {
		$contactPersonOptionItems = '';
		$jsonData = array();

		foreach ($contactPersonDataArray as $contactPersonItemEntity) {
			/* @var $contactPersonItemEntity \ContactPersonEntity */

			$selected = null;
			if ($contactPersonId > 0 
				&& $contactPersonItemEntity->getAp_id() === $contactPersonId
			) {
				$selected = 'selected';
			}

			$contactPersonOptionItems .= \HtmlFormUtils::createOptionItem(
				array(
					'value' => $contactPersonItemEntity->getAp_id(),
					'selected' => $selected,
					'onclick' => 'getVertriebler(' . $contactPersonItemEntity->getVertriebler_id() . ');'
				),
				$contactPersonItemEntity->getVorname() . ' ' . $contactPersonItemEntity->getNachname()
			);

			$jsonData[] = $contactPersonItemEntity;
		}

		$addDefaultItem = true;
	} else {
		$contactPersonOptionItems = '<option value="">-- keine Kontakte --</option>';
	}
} else {
	$jsonData['message'] = 'invalid customerId: ' . $customerId;
	
	throw new \InvalidArgumentException($jsonData['message'], 1424766193);
}

$result = array(
	'content' => \HtmlFormUtils::createSelectItem(
		array(
			'name' => 'campaign[ap]',
			'id' => 'ap',
			'onchange' => 'changeContactPerson(this.value);'
		),
		$contactPersonOptionItems,
		$addDefaultItem
	),
	'jsonData' => $jsonData
);