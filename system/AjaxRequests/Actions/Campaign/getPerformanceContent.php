<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */


$billingType = isset($_POST['billingType']) ? \DataFilterUtils::filterData($_POST['billingType']) : '';
$debugLogManager->logData('billingType', $billingType);

if (\strlen($billingType) > 0) {
	/**
	 * CampaignEntity
	 * 
	 * debug
	 */
	$campaignEntity = new \CampaignEntity();
	$campaignEntity->setAbrechnungsart($billingType);
	$campaignEntity->setPreis(isset($_POST['campaignPrice']) ? \DataFilterUtils::convertDataToFloatvalue($_POST['campaignPrice']) : 0);
	$campaignEntity->setUnit_price(isset($_POST['campaignUnitPrice']) ? \DataFilterUtils::convertDataToFloatvalue($_POST['campaignUnitPrice']) : 0);
	$campaignEntity->setGebucht(isset($_POST['campaignBooked']) ? \intval($_POST['campaignBooked']) : 0);
	$campaignEntity->setLeads(isset($_POST['campaignLeads']) ? \intval($_POST['campaignLeads']) : 0);
	$campaignEntity->setLeads_u(isset($_POST['campaignLeadsUnique']) ? \intval($_POST['campaignLeadsUnique']) : 0);
	$campaignEntity->setPreis2(isset($_POST['campaignPrice2']) ? \DataFilterUtils::convertDataToFloatvalue($_POST['campaignPrice2']) : 0);
        $campaignEntity->setHybrid_preis(isset($_POST['campaignPriceHyprid']) ? \DataFilterUtils::convertDataToFloatvalue($_POST['campaignPriceHyprid']) : 0);
        $campaignEntity->setHybrid_preis2(isset($_POST['campaignPriceHyprid2']) ? \DataFilterUtils::convertDataToFloatvalue($_POST['campaignPriceHyprid2']) : 0);
	$campaignEntity->setPreis_gesamt(isset($_POST['campaignPriceTotal']) ? \DataFilterUtils::convertDataToFloatvalue($_POST['campaignPriceTotal']) : 0);
	$debugLogManager->logData('campaignEntity', $campaignEntity);


	/**
	 * table, row and cell Settings
	 */
	$tableSettings = array(
		'class' => 'neu_k extendedTable',
		'cellpadding' => 0,
		'cellspacing' => 0,
		'border' => 0
	);
	$rowSettings = array();
	$firstCellDataArray = array(
		'align' => 'right',
		'class' => 'info'
	);
	$secondCellDataArray = array(
		'align' => 'left',
	);


	$tableDataArray = \HtmlTableUtils::createTable($tableSettings);
	$tableRowDataArray = \HtmlTableUtils::createTableRow($rowSettings);
	$tableFooterDataArray = \HtmlTableUtils::createTableRow(
		array(
			'class' => 'footerRow'
		)
	);

	$tkpCalculationFormelContent = ' (' . ($campaignEntity->getGebucht() / 1000) . 'k gebucht * ' . \FormatUtils::sumFormat($campaignEntity->getPreis()) . ' &euro;)';

	$tableContent = $tableDataArray['begin'];

	switch ($billingType) {
		case 'TKP':
			$tableContent .=
				$tableRowDataArray['begin']
					. \HtmlTableUtils::createTableCellWidthContent(
						$firstCellDataArray,
						'Einnahmen ' . $campaignEntity->getAbrechnungsart() . ':'
					) . \HtmlTableUtils::createTableCellWidthContent(
							$secondCellDataArray,
							\FormatUtils::sumFormat(FormatUtils::priceCalculationByBillingType($campaignEntity)) . ' &euro;'
								. $tkpCalculationFormelContent
					)
				. $tableRowDataArray['end']
				. $tableRowDataArray['begin']
					. \HtmlTableUtils::createTableCellWidthContent(
						$firstCellDataArray,
						'Pauschale:'
					) . \HtmlTableUtils::createTableCellWidthContent(
						$secondCellDataArray,
						\FormatUtils::sumFormat($campaignEntity->getUnit_price()) . ' &euro;'
					)
				. $tableRowDataArray['end']
			;
			break;

		case 'Hybrid':
			// getTKP Price
			$campaignEntity->setAbrechnungsart('TKP');
			$tkpPrice = \FormatUtils::priceCalculationByBillingType($campaignEntity);

			$campaignEntity->setAbrechnungsart($billingType);

			$tableContent .=
				$tableRowDataArray['begin']
					. \HtmlTableUtils::createTableCellWidthContent(
						$firstCellDataArray,
						'Einnahmen TKP:'
					) . \HtmlTableUtils::createTableCellWidthContent(
						$secondCellDataArray,
						\FormatUtils::sumFormat($tkpPrice) . ' &euro;'
							. $tkpCalculationFormelContent
					)
				. $tableRowDataArray['end']
				. $tableRowDataArray['begin']
					. \HtmlTableUtils::createTableCellWidthContent(
						$firstCellDataArray,
						'Einnahmen CPX:'
					) . \HtmlTableUtils::createTableCellWidthContent(
						$secondCellDataArray,
						\FormatUtils::sumFormat(
							($campaignEntity->getLeads_u() > 0 
								? $campaignEntity->getLeads_u() 
								: $campaignEntity->getLeads()
							) * $campaignEntity->getPreis2()
						) . ' &euro;'
							. ' ('
								. ($campaignEntity->getLeads_u() > 0 
									? $campaignEntity->getLeads_u() 
									: $campaignEntity->getLeads()
								) . ' Leads * ' . $campaignEntity->getPreis2()
							. ' &euro;)'
					)
				. $tableRowDataArray['end']
				. $tableRowDataArray['begin']
					. \HtmlTableUtils::createTableCellWidthContent(
						$firstCellDataArray,
						'Pauschale:'
					) . \HtmlTableUtils::createTableCellWidthContent(
						$secondCellDataArray,
						\FormatUtils::sumFormat($campaignEntity->getUnit_price()) . ' &euro;'
					)
				. $tableRowDataArray['end']
			;
			break;

		case 'Festpreis':
			break;

		default:
			$tableContent .=
				$tableRowDataArray['begin']
					. \HtmlTableUtils::createTableCellWidthContent(
						$firstCellDataArray,
						'Einnahmen ' . $campaignEntity->getAbrechnungsart() . ':'
					) . \HtmlTableUtils::createTableCellWidthContent(
						$secondCellDataArray,
						\FormatUtils::sumFormat(FormatUtils::priceCalculationByBillingType($campaignEntity)) . ' &euro;'
							. ' ('
								. ($campaignEntity->getLeads_u() > 0 
									? $campaignEntity->getLeads_u() 
									: $campaignEntity->getLeads()
								) . ' Leads * ' . $campaignEntity->getPreis()
							. ' &euro;)'
					)
					. $tableRowDataArray['end']
					. $tableRowDataArray['begin']
					. \HtmlTableUtils::createTableCellWidthContent(
						$firstCellDataArray,
						'Pauschale:'
					) . \HtmlTableUtils::createTableCellWidthContent(
						$secondCellDataArray,
						\FormatUtils::sumFormat($campaignEntity->getUnit_price()) . ' &euro;'
					)
					. $tableRowDataArray['end']
			;
			break;
	}

	$tableContent .=
		$tableFooterDataArray['begin']
			. \HtmlTableUtils::createTableCellWidthContent(
				$firstCellDataArray,
				'Einnahmen gesamt:'
			) . \HtmlTableUtils::createTableCellWidthContent(
				$secondCellDataArray,
				'<b>' . \FormatUtils::sumFormat(FormatUtils::totalPriceCalculationByBillingType($campaignEntity)) . ' &euro;' . '</b>'
			)
		. $tableFooterDataArray['end']
	;
	unset($tableDataArray, $tableRowDataArray, $tableFooterDataArray, $campaignEntity);
} else {
	$jsonData['message'] = 'invalid billingType: ' . $billingType;
	
	throw new \InvalidArgumentException($jsonData['message'], 1424766250);
}

$result = array(
	'content' => $tableContent,
	'jsonData' => $jsonData
);