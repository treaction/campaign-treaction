<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */


$customerId = isset($_POST['customerId']) ? \intval($_POST['customerId']) : 0;
$debugLogManager->logData('customerId', $customerId);

if ($customerId > 0) {
	/**
	 * getContactPersonDataItemsByQueryParts
	 * 
	 * debug
	 */
	$contactPersonDataArray = $campaignManager->getContactPersonDataItemsByQueryParts(
		array(
			'WHERE' => array(
				'kundeId' => array(
					'sql' => '`kunde_id`',
					'value' => $customerId,
					'comparison' => 'integerEqual'
				)
			)
		),
		\PDO::FETCH_CLASS
	);
	$debugLogManager->logData('contactPersonDataArray', $contactPersonDataArray);

	if (\count($contactPersonDataArray) > 0) {
		$contactPersonOptionItems = \HtmlFormUtils::createOptionItem(
			array(
				'value' => 0
			),
			'-- Alle Kontakte --'
		);
		$jsonData = array();

		foreach ($contactPersonDataArray as $contactPersonItemEntity) {
			/* @var $contactPersonItemEntity \ContactPersonEntity */

			$contactPersonOptionItems .= \HtmlFormUtils::createOptionItem(
				array(
					'value' => $contactPersonItemEntity->getAp_id()
				),
				$contactPersonItemEntity->getVorname() . ' ' . $contactPersonItemEntity->getNachname()
			);

			$jsonData[] = $contactPersonItemEntity;
		}
	} else {
		$contactPersonOptionItems = '<option value="">-- keine Kontakte --</option>';
	}	
} else {
	$jsonData['message'] = 'invalid customerId: ' . $customerId;
	
	throw new \InvalidArgumentException($jsonData['message'], 1424766193);
}

$result = array(
	'content' => $contactPersonOptionItems,
	'jsonData' => $jsonData
);