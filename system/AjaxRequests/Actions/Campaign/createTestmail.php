<?php

/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

// Factory
require_once(DIR_Factory . 'DeliverySystemFactory.php');

$Anrede = isset($_POST['person']['gender']) 
	? \DataFilterUtils::filterData(
		$_POST['person']['gender'],
		\DataFilterUtils::$validateFilterDataArray['string']
	) 
	: null
;


$Vorname = isset($_POST['person']['vorname']) 
	? \DataFilterUtils::filterData(
		$_POST['person']['vorname'],
		\DataFilterUtils::$validateFilterDataArray['string']
	) 
	: null
;

$Nachname = isset($_POST['person']['nachname']) 
	? \DataFilterUtils::filterData(
		$_POST['person']['nachname'],
		\DataFilterUtils::$validateFilterDataArray['string']
	) 
	: null
;

$Email = isset($_POST['person']['email']) 
	? \DataFilterUtils::filterData(
		$_POST['person']['email'],
		\DataFilterUtils::$validateFilterDataArray['string']
	) 
	: null
;


$mandant = $_SESSION['mandant'];

switch ($mandant) {
    case 'demoClient':
        $maspIdArray = array(
            '11',
            '13'
        );
        $recipientsID = '11';
        $contactFilterId = '40';
        $url ='https://sf14.sendsfx.com/xml.php';
        $customfields = [  1 => $Anrede,
                           2 => $Vorname,
                           3 => $Nachname ];
        break;
    
     case 'stellarPerformance':
         $maspIdArray = array(
             '10',
             '12'
        );
        $recipientsID = '9';
        $contactFilterId = '12';
        $url ='https://sf14.sendsfx.com/xml.php';
        $customfields = [  16 => $Anrede,
                           17 => $Vorname,
                           30 => $Nachname ];
        break;
    
     case 'blueleads':
       $maspIdArray = array(
           '14',
           '15'
        );
       $recipientsID = '1180';
       $contactFilterId = '44';
       $url ='https://sf21.sendsfx.com/xml.php';
       $customfields = [  1193 => $Anrede,
                          1192 => $Vorname,
                          1187 => $Nachname ];
        break;

     case 'leadWorld':
       $maspIdArray = array(
           '28'
        );
        break;   
    
     case 'taglichAngebote':
       $maspIdArray = array(
           '29'
        );

        break; 
     case 'leadMania':
       $maspIdArray = array(
           '33'
        );

        break;

    default:
        
         $maspIdArray = array(
             '10',
             '12'
        );
        break;
}

foreach ($maspIdArray as $maspId) {
    // debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
    /**
     * getClientDeliveryEntityByMaspId
     * 
     * debug
     */
    $ClientDeliverySystemWidthDeliverySystemEntity = $clientManager->getClientDeliveryWidthDeliverySystemEntityByMaspId($maspId);
    	
   $debugLogManager->logData('ClientDeliverySystemWidthDeliverySystemEntity', $ClientDeliverySystemWidthDeliverySystemEntity);
   
  /**
    * getClientDeliveryDataByClientIdAndDeliverySystemAsp
    * 
    * gilt nur f�r alte kampagnen, wenn die db aktualisiert wurde kann dieser code entfernt werden
    */
    $clientDeliveryEntity = $clientManager->getClientDeliveryDataByClientIdAndDeliverySystemAsp(
            $ClientDeliverySystemWidthDeliverySystemEntity->getM_id(),
            $ClientDeliverySystemWidthDeliverySystemEntity->getDeliverySystem()->getAsp_abkz()
    );
    if (!($clientDeliveryEntity instanceof \ClientDeliveryEntity)) {
            throw new \DomainException('invalid ClientDeliveryEntity');
    }

     $debugLogManager->logData('clientDeliveryEntity', $clientDeliveryEntity);

     /**
     * DeliverySystemFactory::getAndInitInstance
	 * 
	 * debug
     */
    $deliverySystemWebservice = \DeliverySystemFactory::getAndInitInstance(
		$ClientDeliverySystemWidthDeliverySystemEntity->getDeliverySystem()->getAsp_abkz(),
		$clientDeliveryEntity
	);
    $debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);
    
        
    /**
     * connect to deliveryWebservice
     */
    $deliverySystemWebservice->login();

	if ((\file_exists(DIR_deliverySystems . $ClientDeliverySystemWidthDeliverySystemEntity->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'createTestmail.php')) === true) {
		require_once(DIR_deliverySystems . $ClientDeliverySystemWidthDeliverySystemEntity->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'createTestmail.php');
	} else {
		#$file = __FILE__;
		#require_once(DIR_deliverySystems . 'noDeliverySystemImplemented.php');
	}
    
    /**
     * disconnect to deliveryWebservice
     */
    $deliverySystemWebservice->logout();
	
	$debugLogManager->endGroup();
        
}
$result = array(
	'content' => $resultData,
	'jsonData' => $resultData
);

