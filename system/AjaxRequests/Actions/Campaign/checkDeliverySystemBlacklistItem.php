<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */


/**
 * processDeliverySystemWebserviceCheckBlacklistRecipient
 * 
 * @param \CampaignEntity $campaignEntity
 * @param string $recipient
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @throws \DomainException
 * @return boolean
 */
function processDeliverySystemWebserviceCheckBlacklistRecipient(\CampaignEntity $campaignEntity, $recipient, \ClientManager $clientManager, \debugLogManager $debugLogManager) {
	$deliverySystemResult = true;
	
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
    /**
     * getDeliverySystemDistributorWidthDeliverySystemEntityById
     * 
     * debug
     */
    $deliverySystemDistributorWidthClientDeliverySystemEntity = $clientManager->getDeliverySystemDistributorWidthDeliverySystemEntityById($campaignEntity->getDsd_id());
	if (!($deliverySystemDistributorWidthClientDeliverySystemEntity instanceof \DeliverySystemDistributorWidthClientDeliverySystemEntity)) {
		throw new \DomainException('invalid DeliverySystemDistributorWidthClientDeliverySystemEntity', 1424766373);
	}
	$debugLogManager->logData('deliverySystemDistributorWidthClientDeliverySystemEntity', $deliverySystemDistributorWidthClientDeliverySystemEntity);
	
	
	/**
	 * DeliverySystemFactory::getAndInitDeliverySystem
	 * 
	 * debug
	 */
	$deliverySystemWebservice = \DeliverySystemFactory::getAndInitDeliverySystem($deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity());
	$debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);
    
    
    /**
     * connect to deliveryWebservice
     */
    $deliverySystemWebservice->login();
	
	if ((\file_exists(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'checkBlacklistRecipient.php')) === true) {
		require_once(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'checkBlacklistRecipient.php');
	} else {
		$file = __FILE__;
		require_once(DIR_deliverySystems . 'noDeliverySystemImplemented.php');
	}
    
    /**
     * disconnect to deliveryWebservice
     */
    $deliverySystemWebservice->logout();
	
	$debugLogManager->endGroup();
	
	return $deliverySystemResult;
}


$campaignId = isset($_POST['campaign']['k_id']) ? \intval($_POST['campaign']['k_id']) : 0;
$debugLogManager->logData('campaignId', $campaignId);

$recipient = isset($_POST['recipient']) 
	? \DataFilterUtils::filterData(
		$_POST['recipient'],
		\DataFilterUtils::$validateFilterDataArray['email']
	) 
	: null
;
$debugLogManager->logData('recipient', $recipient);

if ($campaignId > 0) {
	if (\strlen($recipient) === 0) {
		throw new \InvalidArgumentException('invalid Recipient', 1426068249);
	}
	
	require_once(DIR_configsInit . 'initClientManager.php');
	/* @var $clientManager \ClientManager */
	
	// Factory
	require_once(DIR_Factory . 'DeliverySystemFactory.php');
	
	
	/**
	 * getCampaignAndCustomerDataItemById
	 * 
	 * debug
	 */
	$campaignEntity = $campaignManager->getCampaignAndCustomerDataItemById($campaignId);
	if (!($campaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
		$jsonData['message'] = 'no CampaignWidthCustomerAndContactPersonEntity';
		
		throw new \DomainException($jsonData['message'], 1424766475);
	}
	$debugLogManager->logData('campaignEntity', $campaignEntity);
	
	
	// processDeliverySystemWebserviceCheckBlacklistRecipient
	$deliverySystemResult = \processDeliverySystemWebserviceCheckBlacklistRecipient(
		$campaignEntity,
		$recipient,
		$clientManager,
		$debugLogManager
	);
	
	$resultData = array(
		'success' => true,
		'itemFound' => (boolean) $deliverySystemResult,
		'message' => ''
	);
} else {
	$jsonData['itemFound'] = true; // true bei fehler
	$jsonData['message'] = 'invalid campaignId: ' . $campaignId;
	
	throw new \InvalidArgumentException($jsonData['message'], 1424766235);
}

$result = array(
	'content' => $resultData,
	'jsonData' => $resultData
);