<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */


$customerId = isset($_POST['customer']['kunde_id']) ? \intval($_POST['customer']['kunde_id']) : 0;
$debugLogManager->logData('customerId', $customerId);

if ($customerId > 0) {
   $checkedMonth = $_POST['customer']['monatsrechnung'];
   
    if($checkedMonth === 'true'){
        $invoiceMonth = 1;
    }else{
        $invoiceMonth = 0;
    }
	$dataArray = array(
		'monatsrechnung' => array(
			'value' =>  $invoiceMonth,
			'dataType' => \PDO::PARAM_INT
		),
	);
	
	$campaignManager->updateCustomerDataById(
		$customerId,
		$dataArray
	);
        /**
	 * getContactPersonDataItemsByQueryParts
	 * 
	 * debug
	 */
	$contactPersonDataArray = $campaignManager->getContactPersonDataItemsByQueryParts(
		array(
			'WHERE' => array(
				'customerId' => array(
					'sql' => '`kunde_id`',
					'value' => $customerId,
					'comparison' => 'integerEqual'
				)
			),
			'ORDER_BY' => '`email` ASC'
		),
		\PDO::FETCH_CLASS
	);
        $debugLogManager->logData('contactPersonDataArray', $contactPersonDataArray);
        if(\count($contactPersonDataArray) > 0){
           foreach ($contactPersonDataArray as $contactPersonItemEntity) {
			/* @var $contactPersonItemEntity \ContactPersonEntity */
               $campaignManager->updateCampaignDataInvoiceByCustomerid(
                       $contactPersonItemEntity->getAp_id(),
                       $dataArray
                       );
           }
        }

	$resultData = array(
		'success' => true,
		'message' => ''
	);
        unset($invoiceMonth);
} else {
	$jsonData['message'] = 'invalid customerId: ' . $customerId;
	
	throw new \InvalidArgumentException($jsonData['message'], 1424766193);
}

$result = array(
	'content' => $resultData,
	'jsonData' => $resultData
);