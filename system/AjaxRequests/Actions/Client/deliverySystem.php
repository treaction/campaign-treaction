<?php
/* @var $debugLogManager \debugLogManager */
/* @var $clientManager \ClientManager */


$clientId = isset($_POST['clientId']) ? \intval($_POST['clientId']) : 0;
$debugLogManager->logData('clientId', $clientId);

if ($clientId > 0) {
	/**
	 * getDeliverySystemDataItemsByClientId
	 * 
	 * debug
	 */
	$deliverySystemsDataArray = $clientManager->getDeliverySystemDataItemsByClientId($clientId);
	$debugLogManager->logData('deliverySystemsDataArray', $deliverySystemsDataArray);

	$deliverySystemOptionItems = null;
	if (\count($deliverySystemsDataArray) > 0) {
		foreach ($deliverySystemsDataArray as $deliverySystemItemEntity) {
			/* @var $deliverySystemItemEntity \DeliverySystemEntity */

			$deliverySystemOptionItems .= \HtmlFormUtils::createOptionItem(
				array(
					'value' => $deliverySystemItemEntity->getAsp_abkz()
				),
				$deliverySystemItemEntity->getAsp()
			);
			$jsonData[] = $deliverySystemItemEntity->getAsp_abkz();
		}
	}
} else {
	$jsonData['message'] = 'invalid clientId: ' . $clientId;
	
	throw new \InvalidArgumentException($jsonData['message'], 1426153265);
}

$result = array(
	'content' => \HtmlFormUtils::createSelectItem(
		array(
			'name' => 'campaign[versandsystem]',
			'id' => 'deliverySystem',
			//'onchange' => 'changeTargetGroupByDeliverySystem1(this.value, \'targetGroup\');'
		),
		$deliverySystemOptionItems
	),
	'jsonData' => $jsonData
);
