<?php
/* @var $debugLogManager \debugLogManager */
/* @var $clientManager \ClientManager */

unset($result);
$deliverySystemDistributorId = isset($_POST['deliverySystemDistributor']) ? \DataFilterUtils::filterData($_POST['deliverySystemDistributor']) : '';
$debugLogManager->logData('deliverySystemDistributorId', $deliverySystemDistributorId);

$dsdId = isset($_POST['dsdId']) ? \intval($_POST['dsdId']) : 0;
$debugLogManager->logData('dsdId', $dsdId);

$deliverySystemDistributorDomainContent = '';
if (\strlen($deliverySystemDistributorId) > 0) {
	

	/**
	 * getDomainDeliverySystemsDataArrayByClientId
	 * 
	 * debug
	 */
	$deliveryDomainDataArray = $clientManager->getDomainDeliverySystemsDataArrayByClientId(
		$_SESSION['mID'],
		$deliverySystemDistributorId,
		$_SESSION['underClientIds']
	);
	$debugLogManager->logData('deliveryDomainDataArray', $deliveryDomainDataArray);
	if (\count($deliveryDomainDataArray) > 0) {
		if ($disableNotSelectedItem) {
			if ($dsdId > 0) {
				/**
				 * getDeliverySystemDistributorDataById
				 */
				$deliverySystemDistributorEntity = $clientManager->getDeliverySystemDistributorDataById($dsdId);
				if (!($deliverySystemDistributorEntity instanceof \DeliverySystemDistributorEntity)) {
					$jsonData['message'] = 'no DeliverySystemDistributorEntity';
					
					throw new \DomainException($jsonData['message'], 1426154777);
				}
			} else {
				$jsonData['message'] = 'invalid dsdId!';
				
				throw new \InvalidArgumentException($jsonData['message'], 1426154887);
			}
		} else {
			$deliveryDomainEntity = new \DomainDeliveryEntity();
		}
		$debugLogManager->logData('deliveryDomainEntity', $deliveryDomainEntity);

		/**
		 * CampaignAndOthersUtils::getDeliverySystemDistributorsItems
		 */
		$deliverySystemDistributorDomainContent = $selectItems = \CampaignAndOthersUtils::getDomainDeliverySystemItems(
			$deliveryDomainDataArray,
			$deliveryDomainEntity,
			$disableNotSelectedItem,
			true
		);
		
		if ((boolean) $createSelectItem === true) {
			$deliverySystemDistributorDomainContent = \HtmlFormUtils::createSelectItem(
				array(
					'name' => 'campaign[dasp_id]',
					'class' => 'widthAuto',
					'id' => 'deliverySystemDistributorDomainItems'
				),
				$selectItems
			);
		}
	} else {
		$jsonData = \utf8_encode('<span style="color:red">kein Domain verfuegbar!</span>');
	}
} else {
	$jsonData['message'] = 'invalid domainDeliverySystem: ' . $deliverySystemDistributorId;
	
	throw new \InvalidArgumentException($jsonData['message'], 1426153621);
}

$result = array(
	'content' => $deliverySystemDistributorDomainContent,
	'jsonData' => $jsonData
);