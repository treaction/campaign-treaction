<?php
/* @var $debugLogManager \debugLogManager */
/* @var $clientManager \ClientManager */


$salesId = isset($_POST['salesId']) ? \intval($_POST['salesId']) : 0;
$debugLogManager->logData('salesId', $salesId);

if ($salesId > 0) {
	/**
	 * getUserDataItemById
	 * 
	 * debug
	 */
	$userEntity = $clientManager->getUserDataItemById($salesId);
	if (!($userEntity instanceof \UserEntity)) {
		$jsonData['message'] = 'no UserEntity';
		
		throw new \DomainException($jsonData['message'], 1426155610);
	}
	$debugLogManager->logData('userEntity', $userEntity);

	$salesPersonContent = $userEntity->getVorname() . ' ' . $userEntity->getNachname() . ' '
		. \HtmlFormUtils::createHiddenField(
			'campaign[vertriebler_id]', $userEntity->getBenutzer_id()
		)
	;
	
	$jsonData = $userEntity;
} else {
	$salesPersonContent = '<span style="color:red">- nicht zugewiesen -</span>';
}

$result = array(
	'content' => $salesPersonContent,
	'jsonData' => $jsonData
);