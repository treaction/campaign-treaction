<?php
/* @var $debugLogManager \debugLogManager */
/* @var $clientManager \ClientManager */

unset($result);
$deliverySystemAbkz = isset($_POST['deliverySystem']) ? \DataFilterUtils::filterData($_POST['deliverySystem']) : '';
$debugLogManager->logData('deliverySystemAbkz', $deliverySystemAbkz);

$dsdId = isset($_POST['dsdId']) ? \intval($_POST['dsdId']) : 0;
$debugLogManager->logData('dsdId', $dsdId);

$deliverySystemDistributorContent = '';
if (\strlen($deliverySystemAbkz) > 0) {
	/**
	 * CampaignAndOthersUtils::getDeliverySystemEntityByAspAbkz
	 * 
	 * debug
	 */
	$deliverySystemEntity = \CampaignAndOthersUtils::getDeliverySystemEntityByAspAbkz($deliverySystemAbkz);
	if (!($deliverySystemEntity instanceof \DeliverySystemEntity)) {
		$jsonData['message'] = 'no DeliverySystemEntity: ' . $deliverySystemAbkz;
		
		throw new \DomainException($jsonData['message'], 1426154158);
	}
	$debugLogManager->logData('deliverySystemEntity', $deliverySystemEntity);

	/**
	 * getDeliverySystemDistributorsDataItemsByClientIdAndDeliverySystemId
	 * 
	 * debug
	 */
	$deliverySystemDistributorDataArray = $clientManager->getDeliverySystemDistributorsDataItemsByClientIdAndDeliverySystemId(
		$_SESSION['mID'],
		$deliverySystemEntity->getId(),
		$_SESSION['underClientIds']
	);
	$debugLogManager->logData('deliverySystemDistributorDataArray', $deliverySystemDistributorDataArray);
	
	if (\count($deliverySystemDistributorDataArray) > 0) {
		if ($disableNotSelectedItem) {
			if ($dsdId > 0) {
				/**
				 * getDeliverySystemDistributorDataById
				 */
				$deliverySystemDistributorEntity = $clientManager->getDeliverySystemDistributorDataById($dsdId);
				if (!($deliverySystemDistributorEntity instanceof \DeliverySystemDistributorEntity)) {
					$jsonData['message'] = 'no DeliverySystemDistributorEntity';
					
					throw new \DomainException($jsonData['message'], 1426154777);
				}
			} else {
				$jsonData['message'] = 'invalid dsdId!';
				
				throw new \InvalidArgumentException($jsonData['message'], 1426154887);
			}
		} else {
			$deliverySystemDistributorEntity = new \DeliverySystemDistributorEntity();
		}
		$debugLogManager->logData('deliverySystemDistributorEntity', $deliverySystemDistributorEntity);

		/**
		 * CampaignAndOthersUtils::getDeliverySystemDistributorsItems
		 */
		$deliverySystemDistributorContent = $selectItems = \CampaignAndOthersUtils::getDeliverySystemDistributorsItems(
			$deliverySystemDistributorDataArray,
			$deliverySystemDistributorEntity,
			$disableNotSelectedItem,
			true
		);
		
		if ((boolean) $createSelectItem === true) {
			$deliverySystemDistributorContent = \HtmlFormUtils::createSelectItem(
				array(
					'name' => 'campaign[deliverySystemDistributor]',
					'class' => 'widthAuto',
					'id' => 'deliverySystemDistributorItems',
                                       'onchange' => 'changeDeliverySystemDomain(this.value, \'deliverySystemDistributorDomain\', 0, 0)'
				),
				$selectItems
			);
		}
	} else {
		$jsonData = \utf8_encode('<span style="color:red">kein Verteiler f�r ' . $deliverySystemEntity->getAsp() . ' verf�gbar!</span>');
	}
} else {
	$jsonData['message'] = 'invalid deliverySystem: ' . $deliverySystemAbkz;
	
	throw new \InvalidArgumentException($jsonData['message'], 1426153621);
}

$result = array(
	'content' => $deliverySystemDistributorContent,
	'jsonData' => $jsonData
);