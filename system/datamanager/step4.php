<?php
require_once('autoload.php');

/* @var $debugLogManager debugLogManager */

$dManager = new dataManagerWebservice();

$fieldNamenArr = $_SESSION['fieldNamenArr'];
$fileName = $_SESSION['fileName'];
$anzahl_elemente = (int) $_SESSION['anzahl_elemente'];
$abgleichArr = $_SESSION['impAbgleich'];
$anzahlDatensaetze = (int) $_SESSION['impAnzahl'];
$trennzeichen = $_SESSION['trennzeichen'];

// debug
if (!isset($_GET['d'])) {
    $debugLogManager->logData('fieldNamenArr', $fieldNamenArr);
    $debugLogManager->logData('anzahl_elemente', $anzahl_elemente);
    $debugLogManager->logData('abgleichArr', $abgleichArr);
    $debugLogManager->logData('anzahlDatensaetze', $anzahlDatensaetze);
    $debugLogManager->logData('abgleichTableCnt', $abgleichTableCnt);
    $debugLogManager->logData('abgleichTableArr', $abgleichTableArr);
    #$debugLogManager->logData('SESSION', $_SESSION);
}

$z = 0;
$danzahl = 1;
$ende = $anzahlDS;
$zeileFehlerCnt = 0;
$pWidth = 5;
$Abgleich = '';
$progressBarWidth = 750;

$pid = $_SESSION['impPid'];
$partnerArr = $dManager->getPartnerData(
    $partner_db,
    $felderPartnerDB['Firma'],
    $felderPartnerDB['PID'],
    $pid
);
// debug
if (!isset($_GET['d'])) {
    $debugLogManager->logData('partnerArr', $partnerArr);
}

$fieldNamenCnt = count($fieldNamenArr);
$durchlaeufeCnt = ceil($anzahlDatensaetze / $anzahlDS);

$fileName2 = $fileName . '.csv';
$fcontents = file($uploadFolder . $fileName2);
$elements = sizeof($fcontents);

if (in_array('-de-', $abgleichArr)) {
    $Abgleich .= '
        <tr>
            <td>Bereits erhaltene Adressen (MetaDB)</td>
            <td style="width:150px" id="metaCnt">0</td>
            <td style="width:350px"><span id="metaQ">0</span>%</td>
        </tr>
    ';
} else {
    $Abgleich .= '
        <tr>
            <td>Bereits erhaltene Adressen (MetaDB)</td>
            <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
            <td style="width:350px"></td>
        </tr>
    ';
}

if (in_array('-bl-', $abgleichArr)) {
    $Abgleich .= '
        <tr>
            <td>Globale Blacklist</td><td style="width:150px" id="blCnt">0</td>
            <td style="width:350px"><span id="blQ">0</span>%</td>
        </tr>
    ';
} else {
    $Abgleich .= '
        <tr>
            <td>Globale Blacklist</td>
            <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
            <td style="width:350px"></td>
        </tr>
    ';
}

if (in_array('-dp-', $abgleichArr)) {
    $Abgleich .= '
        <tr>
            <td>Dubletten (postalisch)</td>
            <td style="width:150px" id="dpCnt">0</td>
            <td style="width:350px"><span id="dpQ">0</span>%</td>
        </tr>
    ';
} else {
    $Abgleich .= '
        <tr>
            <td>Dubletten (postalisch)</td>
            <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
            <td style="width:350px"></td>
        </tr>
    ';
}

if (in_array('-bo-', $abgleichArr)) {
    $Abgleich .= '
        <tr>
            <td>Bounces</td>
            <td style="width:150px" id="boCnt">0</td>
            <td style="width:350px"><span id="boQ">0</span>%</td>
        </tr>
    ';
} else {
    $Abgleich .= '
        <tr>
            <td>Bounces</td>
            <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
            <td style="width:350px"></td>
        </tr>
    ';
}

if (in_array('-fe-', $abgleichArr)) {
    $Abgleich .= '
        <tr>
            <td>Fake-Check (Email)</td>
            <td style="width:150px" id="feCnt">0</td>
            <td style="width:350px"><span id="feQ">0</span>%</td>
        </tr>
    ';
} else {
    $Abgleich .= '
        <tr>
            <td>Fake-Check (Email)</td>
            <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
            <td style="width:350px"></td>
        </tr>
    ';
}

if (in_array('-dd-', $abgleichArr)) {
    $Abgleich .= '
        <tr>
            <td>Dubletten innerhalb der Lieferung</td>
            <td style="width:150px" id="ddCnt">0</td>
            <td style="width:350px"><span id="ddQ">0</span>%</td>
        </tr>
    ';
} else {
    $Abgleich .= '
        <tr>
            <td>Dubletten innerhalb der Lieferung</td>
            <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
            <td style="width:350px"></td>
        </tr>
    ';
}

if ($abgleichTableCnt > 0) {
    for ($a = 0; $a < $abgleichTableCnt; $a++) {
        if (in_array($abgleichTableArr[$a]['id'], $abgleichArr)) {
            $Abgleich .= '
                <tr>
                    <td>' . $abgleichTableArr[$a]['info'] . '</td>
                    <td style="width:150px" id="' . $abgleichTableArr[$a]['id'] . 'Cnt">0</td>
                    <td style="width:350px"><span id="' . $abgleichTableArr[$a]['id'] . 'Q">0</span>%</td>
                </tr>
            ';
        } else {
            $Abgleich .= '
                <tr>
                    <td>' . $abgleichTableArr[$a]['info'] . '</td>
                    <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
                    <td style="width:350px"></td>
                </tr>
            ';
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['encoding']; ?>" />
        <title>MaaS - IMPORT - Datenanalyse</title>
        <link rel="stylesheet" type="text/css" href="../css/dataManager.css" />
        <script type="text/javascript" src="../javascript/jquery/js/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="../javascript/jquery/js/jquery-ui-1.8.9.custom.min.js"></script>
    </head>
    <body>
        <table width="100%" height="100%" cellpadding="8" cellspacing="0" border="0" bgcolor="#F2F2F2" style="font-size:11px">
            <tr>
                <td height="70" colspan="2" style="background-color:#FFFFFF; font-weight:bold; font-size:16px; color:#0066FF; border-bottom:1px solid #666666;letter-spacing:0.1em" align="left">DATEI IMPORT  [<?php echo $mandant; ?>]<br /><span style="color:#999999;font-size:12px;font-weight:normal;letter-spacing:normal">Datenanalyse</span></td>
            </tr>

            <tr style="background-color:#666666;border-top:1px solid #666666;">
                <td colspan="2" height="15" style="color:#FFFFFF"><span class="deactive">Dateiauswahl</span> > <span class="deactive">Feldzuordnung</span> > <span class="deactive">Kontrolltabelle</span> > <span class="bold">Datenanalyse</span> > <span class="deactive">Import</span></td>
            </tr>
            <tr style="background-color:#F2F2F2;">
                <td colspan="2" height="15">
                    <div style="padding:5px;border:1px solid #CCCCCC;width:<?php echo $progressBarWidth; ?>px;float:left">
                        <div style="background-color:#CCCCCC;width:<?php echo $progressBarWidth; ?>px;height:2px;">
                            <div id="progressBar" style="background-color:#0066FF;width:5px;height:2px;float:left"></div>		
                        </div>
                        <div style="margin:auto;width:400px;text-align:center;margin-top:5px">
                            Fortschritt: <span id="processedDS">0</span> (<?php echo dataUtil::zahl_format($anzahlDatensaetze); ?>) | <span id="progressBarProzent">0</span>% | Laufzeit: <span id="laufzeit">0</span> min
                        </div>
                    </div>
                </td>
            </tr>
            <tr style="background-color:#F2F2F2">
                <td>
                    <fieldset style="border:solid 1px #666666;padding:.5em;">
                        <legend style="color:#0066FF;font-weight:bold;font-size:12px;">Datenanalyse</legend>
                        <div style="width:750px;overflow:auto;background-color:#FFFFFF;">
                            <table id="importVorschau" width="100%">
                                <tr>
                                    <td>Dateiname</td>
                                    <td colspan="2"><?php echo $_SESSION['fileNameOrig']; ?></td>
                                </tr>
                                <tr>
                                    <td>Lieferant</td>
                                    <td colspan="2"><?php echo ($_SESSION['encoding'] == 'utf-8' ? $partnerArr['Firma'] : \utf8_decode($partnerArr['Firma'])); ?></td>
                                </tr>
                                <tr>
                                    <td>Datens&auml;tze</td>
                                    <td colspan="2"><?php echo dataUtil::zahl_format($anzahlDatensaetze); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="border:0px;background-color:#F2F2F2;"></td>
                                </tr>
                                <tr>
                                    <td class="tdbg">Abgleich</td>
                                    <td class="tdbg">Gefundene Datens&auml;tze</td>
                                    <td class="tdbg">Quote</td>
                                </tr>
                                <?php echo $Abgleich; ?>
                                <tr>
                                    <td>&Uuml;bersprungene Datens&auml;tze</td>
                                    <td class="fehlerCnt"></td>
                                    <td><span id="skipQ">0</span>%</td>
                                </tr>
                                <tr>
                                    <td class="tdbg">Netto</td>
                                    <td class="tdbg" id="nettoCnt"><?php echo dataUtil::zahl_format($anzahlDatensaetze); ?></td>
                                    <td class="tdbg"><span id="nettoQ">100</span>%</td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>
            <tr style="background-color:#F2F2F2">
                <td>
                    <fieldset style="border:solid 1px #666666;padding:.5em;">
                        <legend style="color:#0066FF;font-weight:bold;font-size:12px;">&Uuml;bersprungene Datens&auml;tze (<span class="fehlerCnt">0</span>)</legend>
                        Die Feldanzahl der folgende Datens&auml;tze stimmt nicht mit der Spaltenanzahl &uuml;berein.<br />
                        Der Datensatz enth&auml;lt ein oder mehrere zus&auml;tzliche Trennzeichen. 
                        <div style="width:750px;height:100px;overflow:auto;background-color:#FFFFFF;font-size:10px;border:1px solid #CCCCCC;">
                            <table id="fehlerVorschau" width="100%"></table>
                        </div>
                    </fieldset>
                </td>
            </tr>
            <tr style="background-color:#666666">
                <td id="progress">
                    <input type="button" onclick="location.href='step5.php'" id="impstart" value="Import starten" disabled="disabled"/>
                    <span id="cancelBtn"><input type="button" value="abbrechen" /></span>
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            $('#impstart').click(function() {
                $('#progress').html('<span style="font-weight:bold;color:#FFFFFF;">Der Import wird gestartet...</span>');
            });
            
            <?php
                require_once('include/cancel.js');
            ?>
        </script>
        <?php
        if (!empty($_GET['d'])) {
            $danzahl = intval($_GET['d']);
        }
        
        if (!empty($_GET['z'])) {
            $z = intval($_GET['z']);
        }
        
        if (!empty($_GET['ende'])) {
            $ende = intval($_GET['ende']);
        }
        
        $zeileFehlerArr = isset($_SESSION['zeileFehlerArr']) ? $_SESSION['zeileFehlerArr'] : array();

        if ((int) $danzahl === 1) {
            $_SESSION['zeileFehlerArr'] = '';
            $zeileFehlerArr = array();
            $zeileFehlerCnt = 0;
            
            $tmpTableManager->truncateTmpTable($fileName);
            $tmpTableManager->addAttributeField($fileName);
            
            $domainArr = $tmpTableManager->getBLdomain();
            $_SESSION['blDomain'] = $domainArr;
            
            $fakeEmailArr = $tmpTableManager->getFakeEmail();
            $_SESSION['fakeEmail'] = $fakeEmailArr;
            
            $_SESSION['startZeit'] = time();
        } else {
            $domainArr = $_SESSION['blDomain'];
            $fakeEmailArr = $_SESSION['fakeEmail'];
        }
        
        if ($danzahl <= $durchlaeufeCnt) {
            $startScript = time();

            $fZeile = '';
            $fieldNamenCnt = $fieldNamenCnt + 2;
            $metaCnt = 0;
            $blCnt = 0;
            $boCnt = 0;
            $ddCnt = 0;
            $feCnt = 0;
            $dpCnt = 0;
            $m = 0;
            $zeileFehlerCnt = 0;
            $value = '';

            if ($danzahl === $durchlaeufeCnt) {
                $ende++;
            }
            
            for ($ianzahl = $z; $ianzahl < $ende; $ianzahl++) {
                if ((int) $danzahl === 1 && (int) $ianzahl === 0) {
                    $ianzahl = 1;
                }

                if ($ianzahl < $elements) {
                    $fvalue = '';
                    $line = trim(html_entity_decode($fcontents[$ianzahl]));
                    $line = $line . $trennzeichen . $trennzeichen;
                    $arr = explode($trennzeichen, $line);
                    $arr = str_replace("\"", '', $arr);
                    $arr = str_replace("'", '', $arr);
                    $arr = str_replace('"', '', $arr);
                    $arrCnt = count($arr);

                    $zeileFehler = ($ianzahl + 1);

                    if ($fieldNamenCnt != $arrCnt) {
                        foreach ($arr as $v) {
                            $fvalue .= "'" . $v . "'" . $trennzeichen;
                        }
                        
                        $fvalue = substr($fvalue, 0, -7);
                        $zeileFehlerArr[] = '<b>Zeile ' . $zeileFehler . ':</b> ' . $fvalue . '<br />';
                    } else {
                        $value .= '(';
                        foreach ($arr as $v) {
                            $value .= "'" . $v . "',";
                        }
                        $value = substr($value, 0, -1);
                        $value .= '),';
                    }
                }
            }
            
            $values = substr($value, 0, -1);
            $tmpTableManager->insertAllTmpTable(
                $values,
                $fileName,
                $_SESSION['encoding']
            );

            if (count($zeileFehlerArr) > 0) {
                foreach ($zeileFehlerArr as $zeileF) {
                    $zeileF = str_replace('&', '&amp;', $zeileF);
                    $zeileF = str_replace('"', "'", $zeileF);
                    
                    $fZeile .= '<tr><td>' . $zeileF . '</td></tr>';
                }
                $zeileFehlerCnt = count($zeileFehlerArr);
            }

            $startZ = 0;
            if ($z > 0) {
                $startZ = ($z - 1 - $zeileFehlerCnt);
            }
            $endeZ = ($anzahlDS + 1);
            $limit = $startZ . ',' . $endeZ;

            
            /**
             * WEITERE ABGLEICHE
             */
            if ($abgleichTableCnt > 0) {
                for ($a = 0; $a < $abgleichTableCnt; $a++) {
                    $extTableManager = new extTableManager(
                        $abgleichTableArr[$a]['dbhost'],
                        $abgleichTableArr[$a]['dbname'],
                        $abgleichTableArr[$a]['dbuser'],
                        $abgleichTableArr[$a]['dbpw'],
                        $dbHostEms,
                        $db_name,
                        $db_user,
                        $db_pw
                    );
                    
                    if ((int) $danzahl === 1) {
                        $_SESSION[$abgleichTableArr[$a]['id']] = 0;
                    }

                    if (in_array($abgleichTableArr[$a]['id'], $abgleichArr)) {
                        $$abgleichTableArr[$a]['attribute'] = $_SESSION[$abgleichTableArr[$a]['id']];
                        $xQ = 0;

                        $$abgleichTableArr[$a]['tableName'] = $extTableManager->getEmailExtTable(
                            $fileName,
                            $limit,
                            $abgleichTableArr[$a]['feldname'],
                            $abgleichTableArr[$a]['tableName'],
                            $abgleichTableArr[$a]['attribute']
                        );
                        $$abgleichTableArr[$a]['attribute'] += $$abgleichTableArr[$a]['tableName'];
                        $xQ = dataUtil::getQuote(
                            $anzahlDatensaetze,
                            $$abgleichTableArr[$a]['attribute']
                        );
                        
                        $_SESSION[$abgleichTableArr[$a]['id']] = $$abgleichTableArr[$a]['attribute'];
                        
                        echo '
                            <script type="text/javascript">
                                $("#' . $abgleichTableArr[$a]['id'] . 'Cnt").text("' . dataUtil::zahl_format($$abgleichTableArr[$a]['attribute']) . '");
                                $("#' . $abgleichTableArr[$a]['id'] . 'Q").text("' . $xQ . '");
                            </script>
                        ';
                        
                        $bericht[$abgleichTableArr[$a]['id']] = $$abgleichTableArr[$a]['attribute'];
                        $bericht[$$abgleichTableArr[$a]['attribute']] = $$abgleichTableArr[$a]['attribute'];
                    }
                }
            }

            
            /**
             * METADBABGLEICH
             */
            if (in_array('-de-', $abgleichArr)) {
                $metaArr = $tmpTableManager->getMetaTmpTable(
                    $fileName,
                    $limit,
                    $felderMetaDB,
                    $metaDB
                );
            }
            $metaCnt = $tmpTableManager->cntAttribute(
                $fileName,
                'm'
            ) - $m;
            $metaQ = dataUtil::getQuote(
                $anzahlDatensaetze,
                $metaCnt
            );
            echo '
                <script type="text/javascript">
                    $("#metaCnt").text("' . dataUtil::zahl_format($metaCnt) . '");
                    $("#metaQ").text("' . $metaQ . '");
                </script>
            ';
            $bericht['metaCnt'] = $metaCnt;


            /**
             * BLACKLISTABGLEICH
             */
            if (in_array('-bl-', $abgleichArr)) {
                $blArr = $tmpTableManager->getBLTmpTable(
                    $fileName,
                    $limit,
                    $domainArr
                );
            }
            $blCnt = $tmpTableManager->cntAttribute(
                $fileName,
                'b'
            );
            $blQ = dataUtil::getQuote(
                $anzahlDatensaetze,
                $blCnt
            );
            echo '
                <script type="text/javascript">
                    $("#blCnt").text("' . dataUtil::zahl_format($blCnt) . '");
                    $("#blQ").text("' . $blQ . '");
                </script>
            ';
            $bericht['blCnt'] = $blCnt;


            /**
             * BOUNCEABGLEICH
             */
            if (in_array('-bo-', $abgleichArr)) {
                $boArr = $tmpTableManager->getBouncesTmpTable(
                    $fileName,
                    $limit
                );
            }
            $boCnt = $tmpTableManager->cntAttribute(
                $fileName,
                'h'
            );
            $boQ = dataUtil::getQuote(
                $anzahlDatensaetze,
                $boCnt
            );
            echo '
                <script type="text/javascript">
                    $("#boCnt").text("' . dataUtil::zahl_format($boCnt) . '");
                    $("#boQ").text("' . $boQ . '");
                </script>
            ';
            $bericht['boCnt'] = $boCnt;


            /**
             * FAKEEMAILABGLEICH
             */
            if (in_array('-fe-', $abgleichArr)) {
                $feArr = $tmpTableManager->getFakeEmailTmpTable(
                    $fileName,
                    $limit,
                    $fakeEmailArr
                );
            }
            $feCnt = $tmpTableManager->cntAttribute(
                $fileName,
                'f'
            );
            $feQ = dataUtil::getQuote(
                $anzahlDatensaetze,
                $feCnt
            );
            echo '
                <script type="text/javascript">
                    $("#feCnt").text("' . dataUtil::zahl_format($feCnt) . '");
                    $("#feQ").text("' . $feQ . '");
                </script>
            ';
            $bericht['feCnt'] = $feCnt;


            /**
             * DUBLETTENPOSTLABGLEICH
             */
            if (in_array('-dp-', $abgleichArr)) {
                $dpArr = $tmpTableManager->getDublPostTmpTable(
                    $fileName,
                    $limit,
                    $felderMetaDB,
                    $metaDB
                );
            }
            $dpCnt = $tmpTableManager->cntAttribute(
                $fileName,
                'd'
            );
            $dpQ = dataUtil::getQuote(
                $anzahlDatensaetze,
                $dpCnt
            );
            echo '
                <script type="text/javascript">
                    $("#dpCnt").text("' . dataUtil::zahl_format($dpCnt) . '");
                    $("#dpQ").text("' . $dpQ . '");
                </script>
            ';
            $bericht['dpCnt'] = $dpCnt;


            /**
             * DUBLETTENINDATEILABGLEICH
             */
            if (in_array('-dd-', $abgleichArr) && (int) $danzahl === (int) $durchlaeufeCnt) {
                $ddArr = $tmpTableManager->getDubletteInDateiTmpTable($fileName);
                $ddCnt = $tmpTableManager->cntAttribute(
                    $fileName,
                    'i'
                );
                $ddQ = dataUtil::getQuote(
                    $anzahlDatensaetze,
                    $ddCnt
                );
                echo '
                    <script type="text/javascript">
                        $("#ddCnt").text("' . dataUtil::zahl_format($ddCnt) . '");
                        $("#ddQ").text("' . $ddQ . '");
                    </script>
                ';
                $bericht['ddCnt'] = $ddCnt;
            }

            $z += $anzahlDS;
            $ende += $anzahlDS;

            $pProzent = 0;
            if ($danzahl > 1) {
                $pWidth = ceil(($danzahl / $durchlaeufeCnt) * $progressBarWidth);
                $pProzent = ceil(($danzahl / $durchlaeufeCnt) * 100);
            }

            $danzahl++;
            $_SESSION['zeileFehlerArr'] = $zeileFehlerArr;

            $processedCnt = dataUtil::zahl_format($z);
            if ($danzahl > $durchlaeufeCnt || (int) $durchlaeufeCnt === 1) {
                $processedCnt = dataUtil::zahl_format($anzahlDatensaetze);
                $pWidth = $progressBarWidth;
                $pProzent = 100;
            }

            $nettoCnt = $anzahlDatensaetze - $zeileFehlerCnt - $blCnt - $metaCnt - $boCnt - $feCnt - $dpCnt - $ddCnt - $m;
            $nettoQ = dataUtil::getQuote(
                $anzahlDatensaetze,
                $nettoCnt
            );

            $zeileFehlerQ = dataUtil::getQuote(
                $anzahlDatensaetze,
                $zeileFehlerCnt
            );
            $bericht['zeileFehlerCnt'] = $zeileFehlerCnt;
            $bericht['nettoCnt'] = $nettoCnt;

            $endScript = time();
            $sec = ($endScript - $startScript) * $durchlaeufeCnt;
            if ($sec < 0) {
                $sec = 0;
            }
            $laufzeit = sprintf('%02d:%02d', floor($sec / 60), $sec % 60);

            echo '
                <script type="text/javascript">
                        $("#processedDS").text("' . $processedCnt . '");
                        $(".fehlerCnt").text("' . dataUtil::zahl_format($zeileFehlerCnt) . '");
                        $("#nettoCnt").text("' . dataUtil::zahl_format($nettoCnt) . '");
                        $("#nettoQ").text("' . $nettoQ . '");
                        $("#fehlerVorschau").html("' . $fZeile . '");
                        $("#progressBar").css("width","' . $pWidth . 'px");
                        $("#progressBarProzent").text("' . $pProzent . '");
                        $("#skipQ").text("' . $zeileFehlerQ . '");
                        $("#laufzeit").text("' . $laufzeit . '");
                </script>
            ';

            if ($danzahl > $durchlaeufeCnt || (int) $durchlaeufeCnt === 1) {
                $_SESSION['bericht'] = $bericht;
                unlink('uploads/' . $fileName . '.csv');
                
                $endScript = time();
                $sec = ($endScript - $_SESSION['startZeit']);
                if ($sec < 0) {
                    $sec = 0;
                }
                $laufzeit = sprintf('%02d:%02d', floor($sec / 60), $sec % 60);
                
                echo '
                    <script type="text/javascript">
			$("#laufzeit").text("' . $laufzeit . '");
			$("#progressBar").css("background-color","#A3D119");$("#impstart").removeAttr("disabled");
			alert("Datenanalyse erfolgreich abgeschlossen!\n\nLaufzeit: ' . $laufzeit . 'min");
                    </script>
                ';
            }

            if ($danzahl <= $durchlaeufeCnt) {
                echo '
                    <script type="text/javascript">
                        location.href = "step4.php?d=' . $danzahl . '&z=' . $z . '&ende=' . $ende . '";
                    </script>
                ';
            }
        }
        ?>
    </body>
</html>