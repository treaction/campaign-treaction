<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <title>MaaS - IMPORT - Datenanalyse</title>
        <link rel="stylesheet" type="text/css" href="../css/dataManager.css" />
        <script type="text/javascript" src="../javascript/jquery/js/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="../javascript/jquery/js/jquery-ui-1.8.9.custom.min.js"></script>
    </head>
    <body>
        <table width="100%" height="100%" cellpadding="8" cellspacing="0" border="0" bgcolor="#F2F2F2" style="font-size:11px">
            <tr>
                <td height="70" colspan="2" style="background-color:#FFFFFF; font-weight:bold; font-size:16px; color:#0066FF; border-bottom:1px solid #666666;letter-spacing:0.1em" align="left">DATEI IMPORT<br /><span style="color:#999999;font-size:12px;font-weight:normal;letter-spacing:normal">Datenanalyse</span></td>
            </tr>
            <tr style="background-color:#666666;border-top:1px solid #666666;">
                <td colspan="2" height="15" style="color:#FFFFFF"><span class="deactive">Dateiauswahl</span> > <span class="deactive">Feldzuordnung</span> > <span class="deactive">Kontrolltabelle</span> > <span class="bold">Datenanalyse</span> > <span class="deactive">Import</span></td>
            </tr>
            <tr style="background-color:#F2F2F2;">
                <td colspan="2" height="15">
                    Fortschritt:<br />
                    <div style="padding:5px;border:1px solid #CCCCCC;width:600px;">
                        <div style="background-color:#CCCCCC;width:600px;height:2px;">
                            <div id="progressBar" style="background-color:#0066FF;width:5px;height:2px;"></div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr style="background-color:#F2F2F2">
                <td>
                    <fieldset style="border:solid 1px #666666;padding:.5em;">
                        <legend style="color:#0066FF;font-weight:bold;font-size:12px;">Datenanalyse</legend>
                        <div style="width:750px;height:200px;overflow:auto;background-color:#FFFFFF;">
                            <table id="importVorschau" width="100%">
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>
            <tr style="background-color:#F2F2F2">
                <td>
                    <fieldset style="border:solid 1px #666666;padding:.5em;">
                        <legend style="color:#0066FF;font-weight:bold;font-size:12px;">&Uuml;bersprungene Datens&auml;tze (<span id="fehlerCnt">0</span>)</legend>
                        Die Feldanzahl der folgende Datens&auml;tze stimmt nicht mit der Spaltenanzahl &uuml;berein.<br />
                        M&ouml;gliche Gr&uuml;nde: Htmlentities / ISO-Code, die das Trennzeichen enthalten (z.B. &amp;uuml;) oder ein Trennzeichen zuviel am Ende der Zeile. 
                        <div style="width:750px;height:200px;overflow:auto;background-color:#FFFFFF;">
                            <table id="fehlerVorschau" width="100%">
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>
            <tr style="background-color:#666666">
                <td>
                    <input type="button" onclick="window.back();" value="zur&uuml;ck" style="margin-right:25px"/>
                    <input type="submit" value="Import starten"/>
                    <input type="button" onclick="" value="abbrechen" />
                </td>
            </tr>
        </table>
        <div id="ajax"></div>
        <script>
            $.ajax({
                async: true,
                url: 'dataAnalyse.php',
                success: function(data) {
                    $('#ajax').html(data);
                }
            });;	
        </script>
    </body>
</html>