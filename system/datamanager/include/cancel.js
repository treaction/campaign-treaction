$('#cancelBtn').click(function() {
    stop();

    $('#cancelBtn').html('<span style="font-weight:bold;color:#FFFFFF;margin-left:25px">Vorgang wird abgebrochen, bitte warten...</span>');
    location.href = 'cancel.php';
});

$('#closeExpBtn').click(function() {
    location.href = 'done.php';
});