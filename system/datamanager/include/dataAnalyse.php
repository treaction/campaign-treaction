<?php
require_once('../autoload.php');

/* @var $debugLogManager debugLogManager */

$fieldNamenArr = $_SESSION['fieldNamenArr'];
$fileName = $_SESSION['fileName'];
$anzahl_elemente = $_SESSION['anzahl_elemente'];
$abgleichArr = $_SESSION['impAbgleich'];
$anzahlDatensaetze = $_SESSION['impAnzahl'];
$trennzeichen = $_SESSION['trennzeichen'];

$z = 0;
$zeileFehlerArr = array();
$zeileFehlerCnt = 0;
$pWidth = 5;
$fieldNamenCnt = count($fieldNamenArr);
$tmpTableManager->truncateTmpTable($fileName);
$durchlaeufeCnt = ceil($anzahlDatensaetze / $anzahlDS);

$fileName2 = $fileName . ".csv";
$fcontents = file($uploadFolder . $fileName2);
$elements = sizeof($fcontents);

for ($d = 0; $d < $durchlaeufeCnt; $d++) {
    for ($i = $z; $i < $anzahlDS; $i++) {
        if ($i < $elements) {
            $line = trim($fcontents[$i]);
            $arr = explode($trennzeichen, $line);
            $arr = str_replace("\"", '', $arr);
            $arr = str_replace("'", '', $arr);
            $arr = str_replace('"', '', $arr);
            $values = implode('","', $arr);
            $values = '"' . $values . '"';
            $arrCnt = count($arr);

            $zeileFehler = $i + 1;

            if ($fieldNamenCnt != $arrCnt) {
                $zeileFehlerArr[] = "<b>Zeile " . $zeileFehler . ":</b> " . $values . "<br />";
            } else {
                $tmpTableManager->insertTmpTable(
                    $values,
                    $fileName
                );
            }
        }
    }

    $fZeile = '';
    foreach ($zeileFehlerArr as $zeileF) {
        $zeileF = str_replace('&', '&amp;', $zeileF);
        $zeileF = str_replace('"', "'", $zeileF);
        
        $fZeile .= '<tr><td>' . $zeileF . '</td></tr>';
    }

    $zeileFehlerCnt = count($zeileFehlerArr);
    $z += $anzahlDS;
    $anzahlDS += $anzahlDS;

    if ($d > 0) {
        $pWidth = ceil(($d / $durchlaeufeCnt) * 550);
    }

    print '<script>$("#fehlerCnt").text("' . $zeileFehlerCnt . '");</script>';
    print '<script>$("#fehlerVorschau").html("' . $fZeile . '");</script>';
    print '<script>$("#progressBar").css("width","' . $pWidth . 'px");</script>';
}

$emailArr = $tmpTableManager->getEmailTmpTableData($fileName);
// debug
$debugLogManager->logData('emailArr', $emailArr);
?>