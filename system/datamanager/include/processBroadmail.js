$('#broadmailFieldContent').fadeIn('slow');

$('#submitBroadmail').click(function() {
    $('#updateBroadmailImport').submit();
});

$('#updateBroadmailImport').submit(function(event) {
    event.preventDefault();
    
    $.ajax({
        type: 'post',
        url: $(this).attr('action'),
        data: $(this).serialize(),
        dataType: 'json',
        success: function(resultData) {
            var cssStatus = 'error';
            if (resultData.success === true) {
                $('#updateBroadmailImport').hide('slow');
                cssStatus = 'success';
            }
            
            $('#resultInfo').show('slow').addClass(cssStatus).html(resultData.message);
        }
    })
});