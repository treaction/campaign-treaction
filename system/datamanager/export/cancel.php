<?php
require_once('autoload.php');

unlink('../uploads/' . $_SESSION['temp_datei']);
unset($_SESSION['temp_datei']);

unset(
    $_SESSION['mandantIMP'], $_SESSION['zeichensatz'], $_SESSION['dateiArr'], $_SESSION['anreicherungCnt'], $_SESSION['anzahlDatensaetze'], $_SESSION['impNr']
);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <title>MaaS - EXPORT - Abbruch</title>
        
        <link rel="stylesheet" type="text/css" href="../../css/dataManager.css" />
        <script type="text/javascript" src="../../javascript/jquery/js/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="../../javascript/jquery/js/jquery-ui-1.8.9.custom.min.js"></script>
    </head>
    <body>
        <table width="100%" height="100%" cellpadding="8" cellspacing="0" border="0" bgcolor="#F2F2F2" style="font-size:11px">
            <tr>
                <td height="70" colspan="2" style="background-color:#FFFFFF; font-weight:bold; font-size:16px; color:#0066FF; border-bottom:1px solid #666666;letter-spacing:0.1em" align="left">
                    DATEI EXPORT  [<?php print $mandant; ?>]<br /><span style="color:#999999;font-size:12px;font-weight:normal;letter-spacing:normal">Abbruch</span>
                </td>
            </tr>
            <tr style="background-color:#F2F2F2;">
                <td colspan="2" height="15">
                    Der Vorgang wurde abgebrochen.<br /><br />
                    <input type="button" onclick="window.close();" value="OK" />
                </td>
            </tr>
        </table>
    </body>
</html>