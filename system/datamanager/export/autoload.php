<?php
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');


if (isset($_SESSION['zeichensatz'])) {
    header('Content-Type: text/html; charset=' . $_SESSION['zeichensatz']);
} else {
    header('Content-Type: text/html; charset=ISO-8859-1');
}

if (isset($_SESSION['mandantIMP'])) {
    $mandant = $_SESSION['mandantIMP'];
}

if (strpos($_SERVER['SCRIPT_NAME'], '/datamanager/export/export.php') !== false) {
    $mandant = $_SESSION['mandant'];
    $_SESSION['mandantIMP'] = $mandant;
}


include('../../limport/db_connect.inc.php');
require_once('../../library/dataManagerWebservice.php');
require_once('../class/tmpTableManager.php');
require_once('../class/extTableManager.php');
require_once('../class/enrichmentManager.php');
require_once('../class/dataUtil.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */


// debug
$debugLogManager->logData('mandant', $mandant);

$impOption = '';
$dManager = new dataManagerWebservice();
$tmpTableManager = new tmpTableManager(
    $dbHostEms,
    $db_name,
    $db_user,
    $db_pw
);


$enrichmentManager = new enrichmentManager(
    $anreicherungDBArr,
    $dbHostEms,
    $db_name,
    $db_user,
    $db_pw
);

$anzahlDS = 2500;
$anzahlTmpZeilen = 25;
$progressBarWidth = 750;
$uploadFolder = '../uploads/';
?>