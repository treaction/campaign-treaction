<?php
require_once('autoload.php');

/* @var $debugLogManager debugLogManager */

if ((int) $_SESSION['testimport'] === 1) {
    $metaDB = $metaDB . '_test';
    $import_datei_db = $import_datei_db . '_test';
}

$impNr = $_SESSION['impNr'];
$dateiArr = $_SESSION['dateiArr'];
$abgleichArr = isset($_POST['abgleich']) ? $_POST['abgleich'] : array();
$tmpTblName = 'exp_' . md5($dateiArr['Datei'] . $mandant);

if ($_SESSION['anzahlDatensaetze']) {
    $anzahlDatensaetze = $_SESSION['anzahlDatensaetze'];
    $abgleichArr = $_SESSION['abgleich'];
    $trennzeichen = $_SESSION['trennzeichen'];
    $zeichensatz = $_SESSION['zeichensatz'];
    $datumsformat = $_SESSION['datumsformat'];
    $spaltenname = $_SESSION['spaltenname'];
    $festerwert = $_SESSION['festerwert'];
    $anreicherung = $_SESSION['anreicherung'];
    $expLand = $_SESSION['expLand'];
} else {
    $anzahlDatensaetze = $dateiArr['Brutto'];

    if (count($abgleichArr) > 0) {
        foreach ($abgleichArr as $abgl) {
            switch ($abgl) {
                case 'b':
                    $anzahlDatensaetze -= $dateiArr['Blacklist'];
                    break;

                case 'd':
                    $anzahlDatensaetze -= $dateiArr['Dublette_post'];
                    break;

                case 'i':
                    $anzahlDatensaetze -= $dateiArr['Dublette_indatei'];
                    break;

                case 'h':
                    $anzahlDatensaetze -= $dateiArr['Bounces'];
                    break;

                case 'f':
                    $anzahlDatensaetze -= $dateiArr['Fakeemail'];
                    break;
            }
        }
    }
    
    if (!in_array('m', $abgleichArr)) {
        $anzahlDatensaetze -= $dateiArr['meta'];
    }

    $trennzeichen = isset($_POST['trennzeichen']) ? $_POST['trennzeichen'] : '';
    $abgleichArr = isset($_POST['abgleich']) ? $_POST['abgleich'] : array();
    $zeichensatz = isset($_POST['zeichensatz']) ? $_POST['zeichensatz'] : '';
    $datumsformat = isset($_POST['datumsformat']) ? $_POST['datumsformat'] : '';
    $spaltenname = isset($_POST['spaltenname']) ? $_POST['spaltenname'] : '';
    $anreicherung = isset($_POST['anreicherung']) ? (int) $_POST['anreicherung'] : false;
    $expLand = isset($_POST['expLand']) ? $_POST['expLand'] : '';

    if ($spaltenname != 'Spaltenname' && $spaltenname != '') {
        $festerwert = $_POST['festerwert'];
        $_SESSION['spaltenname'] = $spaltenname;
        $_SESSION['festerwert'] = $festerwert;
    } else {
        $_SESSION['spaltenname'] = '';
        $_SESSION['festerwert'] = '';
    }
    
    $_SESSION['abgleich'] = $abgleichArr;
    $_SESSION['anzahlDatensaetze'] = $anzahlDatensaetze;
    $_SESSION['trennzeichen'] = $trennzeichen;
    $_SESSION['zeichensatz'] = $zeichensatz;
    $_SESSION['datumsformat'] = $datumsformat;
    $_SESSION['anreicherung'] = $anreicherung;
    $_SESSION['expLand'] = $expLand;
    $_SESSION['cleanCountry'] = isset($_POST['clean_country']) ? (int) $_POST['clean_country'] : false;
}

// debug
if (!isset($_GET['d'])) {
    $debugLogManager->logData('POST', $_POST);
    #$debugLogManager->logData('SESSION', $_SESSION);
    $debugLogManager->logData('felderMetaDB', $felderMetaDB);
}

$Abgleich = '';

$metaQ = dataUtil::getQuote(
    $dateiArr['Brutto'],
    $dateiArr['meta']
);
if (in_array('m', $abgleichArr)) {
    $img = '<img src="../../img/Tango/16/actions/dialog-apply.png" />';
} else {
    $img = '<span style="color:#CCCCCC;">&uuml;berspungen</span>';
}
$Abgleich .= '
    <tr>
        <td>Bereits erhaltene Adressen (MetaDB)</td>
        <td style="width:150px">' . dataUtil::zahl_format($dateiArr['meta']) . ' (' . $metaQ . '%)</td>
        <td style="width:500px">' . $img . '</td>
    </tr>
';

$blQ = dataUtil::getQuote(
    $dateiArr['Brutto'],
    $dateiArr['Blacklist']
);
if (in_array('b', $abgleichArr)) {
    $img = '<img src="../../img/Tango/16/actions/dialog-apply.png" />';
} else {
    $img = '<span style="color:#CCCCCC;">&uuml;berspungen</span>';
}
$Abgleich .= '
    <tr>
        <td>Blacklist</td>
        <td style="width:150px">' . dataUtil::zahl_format($dateiArr['Blacklist']) . ' (' . $blQ . '%)</td>
        <td style="width:500px">' . $img . '</td>
    </tr>
';

$dpQ = dataUtil::getQuote(
    $dateiArr['Brutto'],
    $dateiArr['Dublette_post']
);
if (in_array('d', $abgleichArr)) {
    $img = '<img src="../../img/Tango/16/actions/dialog-apply.png" />';
} else {
    $img = '<span style="color:#CCCCCC;">&uuml;berspungen</span>';
}
$Abgleich .= '
    <tr>
        <td>Dubletten (postalisch)</td>
        <td style="width:150px">' . dataUtil::zahl_format($dateiArr['Dublette_post']) . ' (' . $dpQ . '%)</td>
        <td style="width:500px">' . $img . '</td>
    </tr>
';

$boQ = dataUtil::getQuote(
    $dateiArr['Brutto'],
    $dateiArr['Bounces']
);
if (in_array('h', $abgleichArr)) {
    $img = '<img src="../../img/Tango/16/actions/dialog-apply.png" />';
} else {
    $img = '<span style="color:#CCCCCC;">&uuml;berspungen</span>';
}
$Abgleich .= '
    <tr>
        <td>Bounces</td>
        <td style="width:150px">' . dataUtil::zahl_format($dateiArr['Bounces']) . ' (' . $boQ . '%)</td>
        <td style="width:500px">' . $img . '</td>
    </tr>
';

$feQ = dataUtil::getQuote(
    $dateiArr['Brutto'],
    $dateiArr['Fakeemail']
);
if (in_array('f', $abgleichArr)) {
    $img = '<img src="../../img/Tango/16/actions/dialog-apply.png" />';
} else {
    $img = '<span style="color:#CCCCCC;">&uuml;berspungen</span>';
}
$Abgleich .= '
    <tr>
        <td>Fake-Check (Email)</td>
        <td style="width:150px">' . dataUtil::zahl_format($dateiArr['Fakeemail']) . ' (' . $feQ . '%)</td>
        <td style="width:500px">' . $img . '</td>
    </tr>
';

$ddQ = dataUtil::getQuote(
    $dateiArr['Brutto'],
    $dateiArr['Dublette_indatei']
);
if (in_array('i', $abgleichArr)) {
    $img = '<img src="../../img/Tango/16/actions/dialog-apply.png" />';
} else {
    $img = '<span style="color:#CCCCCC;">&uuml;berspungen</span>';
}
$Abgleich .= '
    <tr>
        <td>Dubletten innerhalb der Lieferung</td>
        <td style="width:150px">' . dataUtil::zahl_format($dateiArr['Dublette_indatei']) . ' (' . $ddQ . '%)</td>
        <td style="width:500px">' . $img . '</td>
    </tr>
';

if ((boolean) $anreicherung === true) {
    $fieldEnrichArr = array('ANREDE', 'VORNAME', 'NACHNAME', 'STRASSE', 'PLZ', 'ORT', 'LAND', 'BUNDESLAND', 'GEBURTSDATUM', 'TELEFON');
    
    $anreicherungDisplay = '';
    foreach ($fieldEnrichArr as $anrbez) {
        $anreicherungDisplay .= '
            <tr>
                <td>' . ucfirst(strtolower($anrbez)) . '</td>
                <td style="width:150px" id="' . $anrbez . 'Cnt">0</td>
                <td style="width:500px"><span id="' . $anrbez . 'Q">0</span>%</td>
            </tr>
        ';
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $zeichensatz; ?>" />
        <title>MaaS - EXPORT</title>
        <link rel="stylesheet" type="text/css" href="../../css/dataManager.css" />
        <link type="text/css" href="../../javascript/jquery/css/flick/jquery-ui-1.8.9.custom.css" rel="Stylesheet" />
        
        <script type="text/javascript" src="../../javascript/jquery/js/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="../../javascript/jquery/js/jquery-ui-1.8.9.custom.min.js"></script>
    </head>
    <body>
        <div id='upload_form'>
            <form action="export_step2.php" name="export_optionen" method="post">
                <table width="100%" cellpadding="4" cellspacing="0" border="0" bgcolor="#666666" style="font-size:11px">
                    <tr>
                        <td height="70" colspan="2" style="background-color:#FFFFFF; font-weight:bold; font-size:16px; color:#0066FF; border-bottom:1px solid #666666;letter-spacing:0.1em" align="left">DATEI EXPORT [<?php print $mandant; ?>]<br /><span style="color:#999999;font-size:12px;font-weight:normal;letter-spacing:normal">Export</span></td>
                    </tr>
                    <tr style="background-color:#F2F2F2;">
                        <td colspan="2" height="15">
                            <div style="padding:5px;border:1px solid #CCCCCC;width:<?php print $progressBarWidth; ?>px;float:left">
                                <div style="background-color:#CCCCCC;width:<?php print $progressBarWidth; ?>px;height:2px;">
                                    <div id="progressBar" style="background-color:#0066FF;width:5px;height:2px;float:left"></div>		
                                </div>
                                <div style="margin:auto;width:400px;text-align:center;margin-top:5px">
                                    Fortschritt: <span id="processedDS">0</span> (<?php print dataUtil::zahl_format($anzahlDatensaetze); ?>) | <span id="progressBarProzent">0</span>% | Laufzeit: <span id="laufzeit">0</span> min
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color:#F2F2F2">
                        <td>
                            <fieldset style="border:solid 1px #666666;padding:.5em;">
                                <legend style="color:#0066FF;font-weight:bold;font-size:12px;">Exportbericht</legend>
                                <div style="width:750px;overflow:auto;background-color:#FFFFFF;">
                                    <table id="importVorschau" width="100%">
                                        <tr>
                                            <td>Dateiname</td>
                                            <td colspan="2"><?php echo $dateiArr['Datei']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Lieferant</td>
                                            <td colspan="2"><?php echo ($_SESSION['zeichensatz'] == 'utf-8' ? \utf8_encode($dateiArr['Firma']) : $dateiArr['Firma']); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Datens&auml;tze (geliefert)</td>
                                            <td colspan="2"><?php echo dataUtil::zahl_format($dateiArr['Brutto']); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Datens&auml;tze (Export)</td>
                                            <td colspan="2"><?php echo dataUtil::zahl_format($anzahlDatensaetze); ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="border:0px;background-color:#F2F2F2;height:15px"></td>
                                        </tr>
                                        <tr>
                                            <td class="tdbg">Abgleich</td>
                                            <td class="tdbg">Datens&auml;tze</td>
                                            <td class="tdbg"></td>
                                        </tr>
                                        <?php print $Abgleich; ?>
                                        <tr>
                                            <td class="tdbg">Datens&auml;tze (Export)</td>
                                            <td class="tdbg" id="nettoCnt"><?php echo dataUtil::zahl_format($anzahlDatensaetze); ?></td>
                                            <td class="tdbg"><span id="nettoQ"></span></td>
                                        </tr>
                                        <?php
                                        if ((boolean) $anreicherung === true) {
                                            ?>
                                            <tr>
                                                <td colspan="3" style="border:0px;background-color:#F2F2F2;height:15px"></td>
                                            </tr>
                                            <tr>
                                                <td class="tdbg">Anreicherung</td>
                                                <td class="tdbg">Anzahl</td>
                                                <td class="tdbg"></td>
                                            </tr>
                                            <?php
                                            echo $anreicherungDisplay;
                                        }
                                        ?>
                                    </table>
                                </div>
                            </fieldset>
                        </td>
                    </tr>
                    <tr style="background-color:#666666">
                        <td>
                            <span id="cancelBtn"><input type="button" value="abbrechen" /></span> <input type="button" id="closeExpBtn" value="schliessen" style="display:none;" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <script type="text/javascript">
            <?php
                require_once('../include/cancel.js');
            ?>
        </script>
        
        <?php
        $d = 1;
        $z = 0;
        $ende = $anzahlDS;
        $pWidth = 5;
        $durchlaeufeCnt = ceil($anzahlDatensaetze / $anzahlDS);
        $pProzent = 0;

        if (!empty($_GET['d'])) {
            $d = intval($_GET['d']);
        }
        
        if (!empty($_GET['z'])) {
            $z = intval($_GET['z']);
        }
        
        if (!empty($_GET['ende'])) {
            $ende = intval($_GET['ende']);
        }

        $temp_datei = $tmpTblName . '.txt';
        $filehandle = fopen($uploadFolder . $temp_datei, 'a');

        if ((int) $d === 1) {
            if (file_exists($uploadFolder . $temp_datei)) {
                unlink($uploadFolder . $temp_datei);
            }
            
            $filehandle = fopen($uploadFolder . $temp_datei, 'a');
            $felderMetaDBArr = array();
            foreach ($feldExportName as $feldMeta) {
                $felderMetaDBArr[] = $feldMeta;
                
                if ($export_street_hsnr && $feldMeta == 'STRASSE') {
                    $felderMetaDBArr[] = 'HSNR';
                }
            }

            if ($expLand != '') {
                $felderMetaDBArr[] = 'ANREDE2';
            }

            if ($spaltenname != '' && $spaltenname != 'Spaltenname') {
                $felderMetaDBArr[] = $spaltenname;
            }    

            $tmpTableManager->writeData(
                $filehandle,
                $trennzeichen,
                $felderMetaDBArr
            );
           
            $_SESSION['temp_datei'] = $temp_datei;
            if ((boolean) $anreicherung === true) {
                foreach ($fieldEnrichArr as $anrbez) {
                    $fieldCntArr[$anrbez] = 0;
                }
                $_SESSION['anreicherungCnt'] = $fieldCntArr;
            }
        }

        if ($d <= $durchlaeufeCnt) {
            $startScript = time();

            $limit = $z . ',' . $anzahlDS;
            $expDataArr = $tmpTableManager->getExportData(
                $metaDB,
                $felderMetaDB,
                $herkunft_db,
                $felderHerkunftDB,
                $abgleichArr,
                $_SESSION['impNr'],
                $dateiArr['pid'],
                $limit
            );
            

            // debug
            #$debugLogManager->logData('expDataArr', $expDataArr);
            
            if ((boolean) $anreicherung === true) {
                $expDataArr = $enrichmentManager->enrichDataAll(
                    $expDataArr,
                    $metaDB,
                    $felderMetaDB,
                    $_SESSION['impNr'],
                    $anreicherungDBArr
                );
                // debug
                //$debugLogManager->logData('expDataArr anreicherung', $expDataArr);
            }

            $expDataCnt = count($expDataArr);
            if ($expDataCnt > 0) {
                for ($e = 0; $e < $expDataCnt; $e++) {
                    $dataArr = array();
                    foreach ($felderMetaDB as $key => $feld) {
                        $feldData = $expDataArr[$e][$feld];

                        $feldDataArr['hsnr'] = '';
                        
                        if ($key == 'LAND') {
                            if ($expLand != '') {
                                $tmpExplodeDataArray = explode('_', $expLand);
                                $feldData = current($tmpExplodeDataArray);
                            }
                            
                            if ((boolean) $_SESSION['cleanCountry'] === true && $expLand == '') {
                                $feldData = dataUtil::cleanLand(
                                    trim($feldData),
                                    $expDataArr[$e][$felderMetaDB['EMAIL']],
                                    $expDataArr[$e][$felderMetaDB['PLZ']]
                                );
                            }
                        }
                        
                        if ($feldData != '') {
                            switch ($key) {
                                case'ID':
                                    $feldData = \intval($feldData);                                
                                
                                case 'ANREDE':
                                    $feldData = dataUtil::getAnredeBez($feldData);
                                    $expAnrede = dataUtil::getAnredeBezLand(
                                        $feldData,
                                        $expLand
                                    );
                                    break;
                                
                                case 'VORNAME':
                                case 'NACHNAME':
                                case 'ORT':
                                    $feldData = dataUtil::cleanName($feldData);
                                    break;
                                
                                case 'STRASSE':
                                    $feldDataArr = dataUtil::cleanStrasse(
                                        $feldData,
                                        $export_street_hsnr
                                    );
                                    $feldData = $feldDataArr['strasse'];
                                    break;
                                
                                case 'GEBURTSDATUM':
                                case 'EINTRAGSDATUM':
                                case 'DOI_DATE':
                                    if ($feldData != 0) {
                                        $feldData = dataUtil::konv_date2Eng(
                                            $feldData,
                                            $datumsformat
                                        );
                                    } else {
                                        $feldData = '';
                                    }
                                    break;
                                
                                case 'PLZ':
                                    if (isset($expDataArr[$e][$felderMetaDB['LAND']])) {
                                        $feldData = dataUtil::cleanPLZ(
                                            $feldData,
                                            $expDataArr[$e][$felderMetaDB['LAND']]
                                        );
                                    }
                                    break;
                                
                                case 'IP':
                                case 'DOI_IP':
                                    $feldData = dataUtil::cleanIP($feldData);
                                    break;
                            }
                        }
                        
                        if ($zeichensatz == 'ISO-8859-1') {
                            $dataArr[] = utf8_decode($feldData);
                        } else {
                            $dataArr[] = $feldData;
                        }
                        
                        if ($export_street_hsnr && $key == $felderMetaDB['STRASSE']) {
                            $dataArr[] = $feldDataArr['hsnr'];
                        }
                    }

                    if ($expLand != '') {
                        $dataArr[] = $expAnrede;
                    }
                    
                    if ($spaltenname != '' && isset($festerwert)) {
                        $dataArr[] = $festerwert;
                    }
                        
                    $tmpTableManager->writeData(
                        $filehandle,
                        $trennzeichen,
                        $dataArr
                    );
                    // debug
                    #$debugLogManager->logData('dataArr', $dataArr);
                }
            }

            $endScript = time();
            $sec = ($endScript - $startScript) * ($durchlaeufeCnt - $d);
            if ($sec < 0) {
                $sec = 0;
            }
            $laufzeit = sprintf('%02d:%02d', floor($sec / 60), $sec % 60);

            $z += $anzahlDS;

            if ($d > 1) {
                $pWidth = ceil(($d / $durchlaeufeCnt) * $progressBarWidth);
                $pProzent = ceil(($d / $durchlaeufeCnt) * 100);
            }
            $d++;

            $processedCnt = dataUtil::zahl_format($z);
            if ($d > $durchlaeufeCnt || (int) $durchlaeufeCnt === 1) {
                $processedCnt = dataUtil::zahl_format($anzahlDatensaetze);
                $pWidth = $progressBarWidth;
                $pProzent = 100;
            }

            echo '
                <script>
                        $("#processedDS").text("' . $processedCnt . '");
                        $("#progressBar").css("width", "' . $pWidth . 'px");
                        $("#progressBarProzent").text("' . $pProzent . '");
                        $("#laufzeit").text("' . $laufzeit . '");
                </script>
            ';

            if ((boolean) $anreicherung === true) {
                $jsCnt = '';
                foreach ($fieldEnrichArr as $anrbez) {
                    $jsCnt .= '$("#' . $anrbez . 'Cnt").text("' . dataUtil::zahl_format($fieldCntArr[$anrbez]) . '");';
                    $jsCnt .= '$("#' . $anrbez . 'Q").text("' . dataUtil::getQuote(
                        $dateiArr['Brutto'],
                        $fieldCntArr[$anrbez]) . 
                    '");';
                }
                echo '<script>' . $jsCnt . '</script>';
            }

            if ($d > $durchlaeufeCnt || (int) $durchlaeufeCnt === 1) {
                $tmpTableManager->dropTmpTable($tmpTblName);
                
                fclose($filehandle);

                echo '
                    <script>
			$("#progressBar").css("background-color","#A3D119");
			$("#cancelBtn").html("<span style=\'color:#A3D119;font-weight:bold;\'><img src=\'../../img/Tango/22/actions/dialog-apply.png\' style=\'margin-bottom:-6px\' /> Datenexport abgeschlossen!</span>");
			$("#closeExpBtn").fadeIn(\'slow\');
			location.href = "download.php";
                    </script>
                ';
            }

            if ($d <= $durchlaeufeCnt) {
                echo '<script>location.href = "export_step2.php?d=' . $d . '&z=' . $z . '";</script>';
            }
        }
        ?>
    </body>
</html>