<?php
require_once('autoload.php');

/* @var $debugLogManager debugLogManager */

unset(
    $_SESSION['temp_datei'], $_SESSION['encoding'], $_SESSION['dateiArr'], $_SESSION['anreicherungCnt'], $_SESSION['anzahlDatensaetze']
    , $_SESSION['impNr'], $_SESSION['spaltenname'], $_SESSION['festerwert'], $_SESSION['abgleich'], $_SESSION['trennzeichen']
    , $_SESSION['zeichensatz'], $_SESSION['datumsformat'], $_SESSION['anreicherung'], $_SESSION['expLand'], $_SESSION['cleanCountry']
);

$_SESSION['encoding'] = '';
if ((int) $_SESSION['testimport'] === 1) {
    $metaDB = $metaDB . '_test';
    $import_datei_db = $import_datei_db . '_test';
}

$impNr = intval($_GET['import_nr']);


$dateiArr = $tmpTableManager->getImportDateiData(
    $import_datei_db,
    $impNr,
    $partner_db,
    $felderPartnerDB
);

// debug
$debugLogManager->logData('import_datei_db', $import_datei_db);
$debugLogManager->logData('impNr', $impNr);
$debugLogManager->logData('partner_db', $partner_db);
$debugLogManager->logData('felderPartnerDB', $felderPartnerDB);
$debugLogManager->logData('dateiArr', $dateiArr);

$_SESSION['dateiArr'] = $dateiArr;
$_SESSION['anreicherungCnt'] = array();
$_SESSION['anzahlDatensaetze'] = '';
$_SESSION['impNr'] = $impNr;

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <title>MaaS - EXPORT</title>
        <link rel="stylesheet" type="text/css" href="../../css/dataManager.css" />
        <link type="text/css" href="../../javascript/jquery/css/flick/jquery-ui-1.8.9.custom.css" rel="Stylesheet" />
        <link type="text/css" href="../../javascript/jquery/css/style-checkbox.css" rel="Stylesheet" />
        
        <script type="text/javascript" src="../../javascript/jquery/js/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="../../javascript/jquery/js/jquery-ui-1.8.9.custom.min.js"></script>
        <script type="text/javascript" src="../../javascript/jquery/iphone-style-checkboxes.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $(':checkbox').iphoneStyle({
                    checkedLabel: 'JA',
                    uncheckedLabel: 'NEIN'
                });
                $('.hideTR').hide();
            });
        </script>
    </head>
    <body>
        <div id='upload_form'>
            <form action="export_step2.php" name="export_optionen" id="export_optionen" method="post">
                <table width="100%" cellpadding="4" cellspacing="0" border="0" bgcolor="#666666" style="font-size:11px">
                    <tr>
                        <td height="70" colspan="2" style="background-color:#FFFFFF; font-weight:bold; font-size:16px; color:#0066FF; border-bottom:1px solid #666666;letter-spacing:0.1em" align="left">
                            DATEI EXPORT [<?php print $mandant; ?>]
                            <br /><span style="color:#999999;font-size:12px;font-weight:normal;letter-spacing:normal">Export Optionen</span>
                        </td>
                    </tr>
                    <tr style="background-color:#F2F2F2;">
                        <td width="120" style="color:#0066FF" align="right">Datei</td>
                        <td><?php echo $dateiArr['Datei']; ?></td>
                    </tr>
                    <tr id="lTR" style="background-color:#F2F2F2">
                        <td style="color:#0066FF" align="right">Lieferant</td>
                        <td colspan="2"><?php echo $dateiArr['Firma']; ?></td>
                    </tr>
                    <tr id="dTR" style="background-color:#F2F2F2">
                        <td style="color:#0066FF" align="right">Geliefert am</td>
                        <td><?php echo dataUtil::konv_date($dateiArr['Lieferdatum']); ?></td>
                    </tr>
                    <tr id="dTR" style="background-color:#F2F2F2">
                        <td style="color:#0066FF" align="right">Datens&auml;tze (geliefert)</td>
                        <td><?php echo dataUtil::zahl_format($dateiArr['Brutto']); ?></td>
                    </tr>
                    <tr id="dTR" style="background-color:#F2F2F2">
                        <td style="color:#0066FF" align="right">Datens&auml;tze (Netto)</td>
                        <td><?php echo dataUtil::zahl_format($dateiArr['Netto']); ?></td>
                    </tr>
                    <tr id="dTR" style="background-color:#F2F2F2">
                        <td style="color:#0066FF" align="right">bereits erhaltene Adressen exportieren</td>
                        <td class="abgleich">
                            <table>
                                <tr>
                                    <td><input type="checkbox" name="abgleich[]" value="m" checked="checked"/></td>
                                    <td style="font-size: 10px">(<b><?php echo dataUtil::zahl_format($dateiArr['meta']); ?></b> Datens&auml;tze)</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="dTR" style="background-color:#F2F2F2">
                        <td style="color:#0066FF" align="right">Datenanreicherung</td>
                        <td class="abgleich">
                            <table>
                                <tr>
                                    <td><input type="checkbox" name="anreicherung" value="1" checked="checked" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="dTR" style="background-color:#F2F2F2">
                        <td style="color:#0066FF" align="right">Land Korrigieren</td>
                        <td class="abgleich">
                            <table>
                                <tr>
                                    <td><input type="checkbox" name="clean_country" value="1" checked="checked" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="dTR" style="background-color:#F2F2F2">
                        <td style="color:#0066FF" align="right">Anrede</td>
                        <td>
                            <select name="expLand">
                                <?php
                                    echo HtmlFormUtils::createOptionListItemData(
                                        CampaignAndOthersUtils::$exportCountriesDataArray,
                                        ''
                                    );
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr id="dTR" style="background-color:#F2F2F2">
                        <td></td>
                        <td style="color:#666666">
                            Es wird eine neues Feld "Anrede2" mit der Anrede der jew. Sprache erzeugt,<br />
                            z.B. franz&ouml;sisch: Madame, Monsieur<br />
                            Das Feld "Land" wird mit dem festen Wert des ausgew&auml;hlten Landes gesetzt.
                        </td>
                    </tr>
                    <tr id="dTR" style="background-color:#F2F2F2">
                        <td style="color:#0066FF" align="right">Weitere Optionen</td>
                        <td><input type="button" id="optBtn" style="font-size:11px" value="anzeigen" /> <input type="button" style="font-size:11px" id="anzBtn" value="ausblenden" /></td>
                    </tr>
                    <tr class="hideTR" style="background-color:#F2F2F2;">
                        <td style="color:#0066FF" align="right" valign="top">Abgleiche &amp; Checks</td>
                        <td class="abgleich">
                            <table id="abgleich_dTR">
                                <tr>
                                    <td><input type="checkbox" name="abgleich[]" value="b" checked="checked" /></td>
                                    <td><b>Blacklist</b> (<?php echo dataUtil::zahl_format($dateiArr['Blacklist']); ?>)</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="abgleich[]" value="d" checked="checked" /></td>
                                    <td><b>Dubletten (postalisch)</b> (<?php echo dataUtil::zahl_format($dateiArr['Dublette_post']); ?>)</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="abgleich[]" value="i" checked="checked" /></td>
                                    <td><b>Dubletten (innerhalb der Lieferung)</b> (<?php echo dataUtil::zahl_format($dateiArr['Dublette_indatei']); ?>)</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="abgleich[]" value="h" checked="checked" /></td>
                                    <td><b>Bounces</b> (<?php echo dataUtil::zahl_format($dateiArr['Bounces']); ?>)</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="abgleich[]" value="f" checked="checked" /></td>
                                    <td><b>Fake-Check (Email)</b> (<?php echo dataUtil::zahl_format($dateiArr['Fakeemail']); ?>)</td>
                                </tr>
                                <?php 
                                    echo $impOption;
                                ?>
                            </table>
                        </td>
                    </tr>
                    <tr class="hideTR" style="background-color:#F2F2F2;">
                        <td style="color:#0066FF" align="right" valign="top">Trennzeichen</td>
                        <td class="abgleich">
                            <select name="trennzeichen">
                                <option value=";">[;] Semikolon</option>
                                <option value=",">[,] Komma</option>
                                <option value="t">[t] Tabulator</option>
                            </select>
                        </td>
                    </tr>
                    <tr class="hideTR" style="background-color:#F2F2F2;">
                        <td style="color:#0066FF" align="right" valign="top">Datumsformat</td>
                        <td class="abgleich">
                            <select name="datumsformat">
                                <option value="datetime">Datetime [JJJJ-MM-TT hh:mm:ss]</option>
                                <option value="date">Date [JJJJ-MM-TT]</option>
                            </select>
                        </td>
                    </tr>
                    <tr class="hideTR" style="background-color:#F2F2F2;">
                        <td style="color:#0066FF" align="right" valign="top">Zeichensatz</td>
                        <td class="abgleich">
                            <select name="zeichensatz">
                                <option value="ISO-8859-1">ANSI</option>
                                <option value="utf-8">UTF-8</option>
                            </select>
                        </td>
                    </tr>
                    <tr class="hideTR" style="background-color:#F2F2F2;">
                        <td style="color:#0066FF" align="right" valign="top">Spalte hinzuf&uuml;gen</td>
                        <td class="abgleich">
                            <input type="text" name="spaltenname" value="Spaltenname" /> <input name="festerwert" type="text" value="fester Wert" />
                        </td>
                    </tr>
                    <tr style="background-color:#F2F2F2;">
                        <td colspan="2" height="25"></td>
                    </tr>
                    <tr style="background-color:#666666">
                        <td height="50"></td>
                        <td valign="top">
                            <span id="uplbtn"><input name="button" id="expSubmit" type="button" value="Export starten" /> <input class="expBtn" type="button" onclick="window.close();" value="abbrechen" style="margin-left:15px" /></span>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <script>
            <?php
            if ((boolean) $dateiArr['broadmailImport'] === true) {
                ?>
                $('#abgleich_dTR :checkbox').each(function() {
                    $(this).attr('checked', '');
                })
                
                $('#abgleich_dTR').hide('slow');
                <?php
            }
            ?>
            
            $('#anzBtn').hide();
            
            $('#optBtn').click(function () {
                $('.hideTR').toggle();
                $('#anzBtn').show();
                $('#optBtn').hide();
            });
            
            $('#anzBtn').click(function () {
                $('.hideTR').toggle();
                $('#anzBtn').hide();
                $('#optBtn').show();
            });
            
            $('#expSubmit').click(function () {
                $('#uplbtn').html('<span style="color:#FFFFFF"><b>Starte Export, bitte warten...</b></span>');
                $('#export_optionen').submit();
            });
        </script>
    </body>
</html>