<?php
/* @var $debugLogManager \debugLogManager */
require_once('autoload.php');

if (!empty($_FILES)) {
	$mandant = \base64_decode($_REQUEST['mandant']);
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$fileName = preg_replace('/[^0-9a-zA-Z]/', '', ($mandant . $_FILES['Filedata']['name']));
	$fileName = md5($fileName);

	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';
	$targetFile = str_replace('//', '/', $targetPath) . $fileName . '.csv';

	move_uploaded_file($tempFile, $targetFile);
	chmod($targetFile, 0755);
}


// debug
$debugLogManager->logData('REQUEST', $_REQUEST);
$debugLogManager->logData('uploadFolder', $uploadFolder);

$fileName = preg_replace('/[^0-9a-zA-Z]/', '', ($mandant . $_GET['f']));
$fileName = md5($fileName);
$fileName2 = $fileName . '.csv';
$spaltenNamenArr = array();

$_SESSION['fileName'] = $fileName;
$_SESSION['fileNameOrig'] = $_GET['f'];
$_SESSION['importtyp'] = (int) $_GET['importtyp'];

$email = array('email', 'emailadresse', 'e-mail');
$anrede = array('anrede', 'greeting', 'gender');
$vorname = array('vorname', 'firstname');
$nachname = array('nachname', 'zuname', 'lastname');
$strasse = array('strasse', 'straße', 'street');
$hsnr = array('hsnr', 'hausnr', 'hausnummer', 'housenumber');
$plz = array('plz', 'postalcode');
$ort = array('ort', 'stadt', 'city');
$land = array('land', 'country');
$geburtsdatum = array('geburtsdatum', 'dateofbirth', 'geburtstag');
$telefon = array('telefon', 'tel', 'vorwahl');
$eintragsdatum = array('eintragsdatum', 'timestamp');
$ip = array('ip', 'ipadresse', 'userip');
$herkunft = array('herkunft', 'site', 'www', 'url');
$doi_ip = array('doi_ip', 'doptin_ip');
$doi_date = array('doi_date', 'doptin_date');

$feldZuordnung = '';
$x = 1;

$feld_email = false;
$alleFelderArr = array('email', 'anrede', 'vorname', 'nachname', 'strasse', 'hsnr', 'plz', 'ort', 'land', 'geburtsdatum', 'telefon', 'eintragsdatum', 'ip', 'herkunft', 'doi_ip', 'doi_date');

$spalten = '';
$tr_vorschau = '';

$fcontents = file($uploadFolder . $fileName2);
$elements = sizeof($fcontents);

// debug
$debugLogManager->logData('fcontents', $fcontents);
$debugLogManager->logData('elements', $elements);

$_SESSION['encoding'] = $_GET['encoding'];
$_SESSION['impPid'] = $_GET['p'];
$_SESSION['impDate'] = $_GET['d'];
$_SESSION['impAbgleich'] = isset($_GET['abgleich']) ? $_GET['abgleich'] : array();
$_SESSION['impAnzahl'] = ($elements - 1);

$tmpTableManager->dropTmpTable($fileName);

for ($i = 0; $i < $anzahlTmpZeilen; $i++) {
    if (isset($fcontents[$i]) && strlen($fcontents[$i]) > 0) {
        $line = trim(html_entity_decode($fcontents[$i]));
     
        if ($i == 0) {
            if ((substr_count($line, ';')) > (substr_count($line, ',')) && (substr_count($line, ';')) > (substr_count($line, "\t"))) {
                $trennzeichen = ';';
            }

            if ((substr_count($line, ',')) > (substr_count($line, ';')) && (substr_count($line, ',')) > (substr_count($line, "\t"))) {
                $trennzeichen = ',';
            }

            if ((substr_count($line, "\t")) > (substr_count($line, ';')) && (substr_count($line, "\t")) > (substr_count($line, ','))) {
                $trennzeichen = "\t";
            }

            if ($trennzeichen == '') {
                $trennzeichen = "\n";
            }

            $arr = explode($trennzeichen, $line);
            $anzahl_elemente = sizeof($arr);
            // debug
            $debugLogManager->logData('anzahl_elemente', $anzahl_elemente);
        }

        $arr = explode($trennzeichen, $line);
        $arr = str_replace("'", '', $arr);
        $arr = str_replace('"', '', $arr);
        $_SESSION['trennzeichen'] = $trennzeichen;

        if ($i == 0) {
            for ($j = 0; $j < $anzahl_elemente; $j++) {
                $spalten .= 'unbekannt' . $j . ' VARCHAR(255) NOT NULL,';
            };

            $spalten = trim($spalten, ',');

            // debug
            /*
            $debugLogManager->logData('createTmpTable', true);
            $debugLogManager->logData('spalten', $spalten);
            $debugLogManager->logData('fileName', $fileName);
             * 
             */
            
            $tmpTableManager->createTmpTable(
                $spalten,
                $fileName
            );
        }

        $values = implode('","', $arr);
        $values = '"' . $values . '"';

        $tmpTableManager->insertTmpTable(
            $values,
            $fileName,
            $_SESSION['encoding']
        );
    }
}



$dataArr = $tmpTableManager->getTmpTableData(
    $anzahl_elemente,
    $fileName
);

$dataArrCnt = count($dataArr);
// debug
$debugLogManager->logData('dataArrCnt', $dataArrCnt);
$debugLogManager->logData('dataArr', $dataArr);

for ($i = 0; $i < $dataArrCnt; $i++) {
    $td = '';
    $classVorschau = '';

    for ($j = 0; $j < $anzahl_elemente; $j++) {
        if ($i === 0) {
            $classVorschau = 'tdbg';
            $spaltenNamenArr[] = strtolower(trim($dataArr[$i][$j]));
        }
        
        $td .= '<td class="' . $classVorschau . '">' . $dataArr[$i][$j] . '</td>';
    }

    $tr_vorschau .= '<tr>' . $td . '</tr>';
}
// debug
$debugLogManager->logData('spaltenNamenArr', $spaltenNamenArr);
#$debugLogManager->logData('SESSION', $_SESSION);

$_SESSION['anzahl_elemente'] = $anzahl_elemente;
$_SESSION['spaltenNamenArr'] = $spaltenNamenArr;
$noMatch = false;
$funb = 0;

foreach ($dataArr[0] as $feldName) {
    $feldNameOrig = trim($feldName);
    $feldName = strtolower(trim($feldName));
    
    if (in_array($feldName, $alleFelderArr)) {
        $fColor = 'color:#000000;';
        $fImg = '<img src="../img/Tango/16/actions/dialog-apply.png" style="margin-bottom:-3px" title="Feldzuordnung gefunden" />';
    } else {
        $noMatch = true;
        $fColor = 'color:red;';
        $fImg = '<img src="../img/Tango/16/status/dialog-warning.png" style="margin-bottom:-3px" title="Feldzuordnung wurde nicht automatisch erkannt" />' . 
            '<span style="color:#F8901B">Bitte Feldnamen zuweisen oder -ignorieren-.<span>'
        ;
    }

    $feldZuordnung .= '<tr><td style="font-weight:bold;padding-right:15px;' . $fColor . '">' . $feldNameOrig . '</td><td>==></td><td>' . 
        dataUtil::getFeldzuordnung(
            $feldName,
            $funb,
            $alleFelderArr
        ) . ' ' . 
        $fImg . '</td></tr>'
    ;

    $funb++;
}

if ($noMatch) {
    $feldWarnung = '
        <tr>
            <td colspan="5" style="font-weight:bold">
                <img src="../img/Tango/16/status/dialog-warning.png" style="margin-bottom:-3px" /> <span style="color:#F8901B"> Ein oder mehrere Felder konnten nicht automatisch zugeordnet werden!<span>
            </td>
        </tr>'
    ;
} else {
    $feldWarnung = '
        <tr>
            <td colspan="5" style="font-weight:bold">
                <img src="../img/Tango/16/actions/dialog-apply.png" style="margin-bottom:-3px" /> <span style="color:green"> Alle Felder konnten automatisch zugeordnet werden!<span>
            </td>
        </tr>'
    ;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['encoding']; ?>" />
        <title>MaaS - IMPORT - Feldzuordnung</title>
        <link rel="stylesheet" type="text/css" href="../css/dataManager.css" />
        <script type="text/javascript" src="../javascript/jquery/js/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="../javascript/jquery/js/jquery-ui-1.8.9.custom.min.js"></script>
    </head>
    <body>
        <form id="step3" action="step3.php" method="post">
            <table width="100%" height="100%" cellpadding="8" cellspacing="0" border="0" bgcolor="#F2F2F2" style="font-size:11px">
                <tr>
                    <td height="70" colspan="2" style="background-color:#FFFFFF; font-weight:bold; font-size:16px; color:#0066FF; border-bottom:1px solid #666666;letter-spacing:0.1em" align="left">DATEI IMPORT [<?php echo $mandant; ?>]<br /><span style="color:#999999;font-size:12px;font-weight:normal;letter-spacing:normal">Feldzuordnung</span></td>
                </tr>
                <tr style="background-color:#666666;border-top:1px solid #666666;">
                    <td colspan="2" height="15" style="color:#FFFFFF"><span class="deactive">Dateiauswahl</span> > <span class="bold">Feldzuordnung</span> > <span class="deactive">Kontrolltabelle</span> > <span class="deactive">Datenanalyse</span> > <span class="deactive">Import</span></td>
                </tr>
                <tr style="background-color:#F2F2F2">
                    <td>
                        <fieldset style="border:solid 1px #666666;padding:.5em;">
                            <legend style="color:#0066FF;font-weight:bold;font-size:12px;">Vorschau</legend>
                            <div style="width:750px;height:200px;overflow:auto;background-color:#FFFFFF;">
                                <table id="importVorschau" width="100%">
                                    <?php echo $tr_vorschau; ?>
                                </table>
                            </div>
                        </fieldset>
                    </td>
                </tr>
                <?php echo $feldWarnung; ?>
                <tr style="background-color:#F2F2F2">
                    <td>
                        <fieldset>
                            <legend style="color:#0066FF;font-weight:bold;font-size:12px;">Feldzuordnung</legend>
                            <div style="width:750px;height:200px;overflow:auto;background-color:#FFFFFF;">
                                <table cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td><?php echo $feldZuordnung; ?></td>
                                    </tr>
                                </table>
                            </div>
                        </fieldset>
                    </td>
                </tr>
                <tr style="background-color:#666666">
                    <td id="progress">
                        <input id="submitBtn" type="button" value="weiter"/>
                        <span id="cancelBtn"><input type="button" value="abbrechen" /></span>
                    </td>
                </tr>
            </table>
        </form>
        <script type="text/javascript">
            $('#submitBtn').click(function() {
                $('#progress').html('<span style="font-weight:bold;color:#FFFFFF;">Felder werden zugeordnet, bitte warten...</span>');
                $('#step3').submit();
            });
            
            <?php
                require_once('include/cancel.js');
            ?>
        </script>
    </body>
</html>