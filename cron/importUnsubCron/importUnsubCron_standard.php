<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/vhosts/campaign-treaction.de/httpdocs/';
\define('DIR_Utils', $workPath . 'system/library/Utils/');

require_once($workPath . 'cron/class/UnsubscribeWebservice.php');
require_once($workPath . 'cron/class/ImportUnsubscribeBroadmail.php');
require_once($workPath . 'cron/class/ImportUnsubscribeSendEffect.php');
require_once($workPath . 'cron/class/ImportUnsubscribeMaileon.php');
require_once($workPath . 'cron/class/ImportUnsubscribeMaileonStandard.php');
require_once($workPath . 'cron/class/ImportUnsubscribeSendEffectStandard.php');

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');
require_once(DIR_Utils . 'DebugAndExceptionUtils.php');


$messageDataArray = array();
$messageDataArray[] = 'clientId: ' . $mID . \chr(13);
$messageDataArray[] = 'start: ' . \time() . \chr(13);

$limit = 250;

try {
    $unsubWs = new UnsubscribeWebservice();
    #$m_aspArr = $unsubWs->getMandantAsps($mID);
    #$messageDataArray[] = 'asps: ' . \implode('', $m_aspArr) . \chr(13);


   $apikeylistWave = array(
        '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        'db1757ee-52bf-4179-b936-bebd403ba669',
        '24d7cb81-c936-4f47-9075-c336a38d9ba6',
        'd26f5ba2-8374-4ff3-ba7c-4872b017ee1c',
        'bd50cb4d-a848-4efe-90ae-347d931858a7', // BLE
        'c168469b-8f23-4f41-a17b-2ebe0cdb649f' // BLBL
    );

    $apikeylistStellar = array(
        '532ddc97-a5ce-4dce-88da-be441690d0a5',
        'd682dc3c-2937-4946-b231-3bc63890a8f9',
        '98413824-b3ae-4eca-8af0-cfd67b591b07',
        '5826c152-f702-43df-aceb-697c31639f6f', // FMB
        'bd5d0dc6-2859-4cce-bcfb-2218afad8c9f', //FM19      
        'b9d2bfcf-6250-4366-9bc1-fc43def57146', //FME
        '57f1ae23-26dc-49a4-a339-c5066fc391e1' //FMBL
    );
    
    $apikeylistCeoo = array(
        '95d11791-8682-4a56-9da1-65c938595c02', // CEOO
        '3f705491-d90d-4d36-9c21-22265d2b9d69' // Black
    );
   $result_login = array(
                array(
                   'm_login' => 'http://sf14.sendsfx.com/middleware/api/',
                   'api_user' => 'stellar2',
                   'pw' => '82bc671d9822554e29e070d78b01049ea83fefed',
                   'mid' => 6
                ),
                 array(
                   'm_login' => 'http://sf14.sendsfx.com/middleware/api/',
                   'api_user' => '4wave2',
                   'pw' => '17d6df85da1bedf492da0d181789e500c8c3bb5f',
                   'mid' => 7
                ),
                array(
                   'm_login' => 'http://sf52.sendsfx.com/middleware/api/',
                   'api_user' => 'TreactionCeoo',
                   'pw' => '3c74892d13677f06236b835ad93e08340dd06ce3',
                   'mid' => 14
                )
       
            );
    foreach ($apikeylistWave as $apikeyWave) {
        $impUnsubMLWave = new \ImportUnsubscribeMaileonStandard();
        $rcpIds_mlWave = $impUnsubMLWave->importUnsubscribeAll(
                $apikeyWave
        );
        $messageDataArray[] = 'rcpCount: ' . \count($rcpIds_mlWave) . \chr(13);

        if (count($rcpIds_mlWave) > 0) {
            $mID = 7;
            $ml_processed = $unsubWs->addRecipients(
                    $rcpIds_mlWave, $mID
            );

            $messageDataArray[] = 'ImportUnsubscribeAll: ' . 1 . \chr(13);
            #var_dump($ml_processed);
        }
    }

    foreach ($apikeylistStellar as $apikeyStellar) {

        $impUnsubMLStellar = new \ImportUnsubscribeMaileonStandard();
        $rcpIds_mlStellar = $impUnsubMLStellar->importUnsubscribeAll(
                $apikeyStellar
        );
        $messageDataArray[] = 'rcpCount: ' . \count($rcpIds_mlStellar) . \chr(13);

        if (count($rcpIds_mlStellar) > 0) {
            $mID = 6;
            $ml_processed = $unsubWs->addRecipients(
                    $rcpIds_mlStellar, $mID
            );

            $messageDataArray[] = 'ImportUnsubscribeAll: ' . 1 . \chr(13);
           # var_dump($ml_processed);
        }
    }
   
     foreach ($apikeylistCeoo as $apikeyCeoo) {
        $impUnsubMLCeoo = new \ImportUnsubscribeMaileonStandard();
        $rcpIds_mlCeoo = $impUnsubMLCeoo->importUnsubscribeAll(
                $apikeyCeoo
        );
        $messageDataArray[] = 'rcpCount: ' . \count($rcpIds_mlCeoo) . \chr(13);

        if (count($rcpIds_mlCeoo) > 0) {
            $mID = 14;
            $ml_processed = $unsubWs->addRecipients(
                    $rcpIds_mlCeoo, $mID
            );

            $messageDataArray[] = 'ImportUnsubscribeAll: ' . 1 . \chr(13);
            #var_dump($ml_processed);
        }
    }
    
    foreach ($result_login as $row) {

        $impUnsubSEMandant = new \ImportUnsubscribeSendEffectStandard();
        $rcpIds_seMandant = $impUnsubSEMandant->importUnsubscribeAll(
                $row
                );
        $messageDataArray[] = 'rcpCount: ' . \count($rcpIds_seMandant) . \chr(13);
       
        if (count($rcpIds_seMandant) > 0) {
            $mID = $row['mid'];
            $ml_processed = $unsubWs->addRecipients(
                    $rcpIds_seMandant, $row['mid']
            );

            $messageDataArray[] = 'ImportUnsubscribeAll: ' . 1 . \chr(13);
           # var_dump($ml_processed);
        }
    }
} catch (Exception $ex) {
    \DebugAndExceptionUtils::sendDebugData($ex, __FILE__ . ': ' . $mID);
}

unset($messageDataArray);
