<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/vhosts/campaign-treaction.de/httpdocs/';
\define('DIR_Utils', $workPath . 'system/library/Utils/');

require_once($workPath . 'cron/class/UnsubscribeWebservice.php');
require_once($workPath . 'cron/class/ImportUnsubscribeBroadmail.php');
require_once($workPath . 'cron/class/ImportUnsubscribeSendEffect.php');
require_once($workPath . 'cron/class/ImportUnsubscribeMaileon.php');
require_once($workPath . 'cron/class/ImportUnsubscribePromio.php');

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');
require_once(DIR_Utils . 'DebugAndExceptionUtils.php');


$messageDataArray = array();
$messageDataArray[] = 'clientId: ' . $mID . \chr(13);
$messageDataArray[] = 'start: ' . \time() . \chr(13);

$limit = 250;


try {
    $unsubWs = new UnsubscribeWebservice();
    $m_aspArr = $unsubWs->getMandantAspsImport($mID); 
    $messageDataArray[] = 'asps: ' . \implode('', $m_aspArr) . \chr(13);

    foreach ($m_aspArr as $m_asp) {
        $messageDataArray[] = \chr(13) . 'asp: ' . $m_asp . \chr(13);

        switch ((int) $m_asp) {
            case 1:
                 $impUnsubBM = new \ImportUnsubscribeBroadmail();
                $rcpIds_bm = $impUnsubBM->importUnsubscribeAll(
                        $mID
                );
                $messageDataArray[] = 'rcpCount: ' . \count($rcpIds_bm) . \chr(13);

                if (count($rcpIds_bm) > 0) {

                    $se_processed = $unsubWs->addRecipients(
                            $rcpIds_bm, $mID
                    );

                    $messageDataArray[] = 'ImportUnsubscribeAll: ' . 1 . \chr(13);
                }
                break;

            case 7:
            case 17:
            case 18:
                $impUnsubSE = new \ImportUnsubscribeSendEffect();
                $rcpIds_se = $impUnsubSE->importUnsubscribeAll(
                        $mID
                );
                $messageDataArray[] = 'rcpCount: ' . \count($rcpIds_se) . \chr(13);

                if (count($rcpIds_se) > 0) {

                    $se_processed = $unsubWs->addRecipients(
                            $rcpIds_se, $mID
                    );

                    $messageDataArray[] = 'ImportUnsubscribeAll: ' . 1 . \chr(13);
                }
                break;

            case 8:

                $impUnsubML = new \ImportUnsubscribeMaileon();
                $rcpIds_ml = $impUnsubML->importUnsubscribeAll(
                        $mID
                );
                
                var_dump($rcpIds_ml);
                
                $messageDataArray[] = 'rcpCount: ' . \count($rcpIds_ml) . \chr(13);

                if (count($rcpIds_ml) > 0) {

                    $ml_processed = $unsubWs->addRecipients(
                            $rcpIds_ml, $mID
                    );

                    $messageDataArray[] = 'ImportUnsubscribeAll: ' . 1 . \chr(13);
                }
                break;
               case 22:

                $impUnsubML = new \ImportUnsubscribePromio();
                $rcpIds_ml = $impUnsubML->importUnsubscribeAll(
                        $mID
                );
                  
                $messageDataArray[] = 'rcpCount: ' . \count($rcpIds_ml) . \chr(13);

                if (count($rcpIds_ml) > 0) {

                    $ml_processed = $unsubWs->addRecipients(
                            $rcpIds_ml, $mID
                    );

                    $messageDataArray[] = 'ImportUnsubscribeAll: ' . 1 . \chr(13);
                }
                break;
        }
    }
} catch (Exception $ex) {
    \DebugAndExceptionUtils::sendDebugData($ex, __FILE__ . ': ' . $mID);
}

unset($messageDataArray);
