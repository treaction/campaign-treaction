<?php
\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/vhosts/campaign-treaction.de/httpdocs/';
\define('DIR_Utils', $workPath . 'system/library/Utils/');


require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/BlacklistWebservice_2.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blMaileon_2.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blSendEffect_2.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blBroadmail_2.php');

require_once($workPath . 'cron/packages/'. \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');
require_once(DIR_Utils . 'DebugAndExceptionUtils.php');


$limit = 250;

$messageDataArray = array();
$messageDataArray[] = 'clientId: ' . $mID . \chr(13);
$messageDataArray[] = 'start: ' . \time() . \chr(13);

try {
    
	$blacklistWs = new blacklistWebservice_2();
	$cnt = $blacklistWs->countUnProcessedBL();
        
	$messageDataArray[] = 'count: ' . $cnt . \chr(13);
	if ($cnt >= 0) {
        if($maspId == 0){
            $m_aspArr = $blacklistWs->getMandantAsps($mID);    
        }else {
            $m_aspArr = $blacklistWs->getMandantAspsFromMaspId($maspId);  
        }
        
        $messageDataArray[] = 'asps: ' . \implode('', $m_aspArr) . \chr(13);
		$rcpIds = $blacklistWs->getAll($limit);
		$messageDataArray[] = 'count rcpIds: ' . count($rcpIds) . \chr(13);
		foreach ($m_aspArr as $m_asp) {

			$messageDataArray[] = \chr(13) . 'asp: ' . $m_asp . \chr(13);
            
			switch ((int) $m_asp) {
                                 case 1:
					$blacklistBM = new blacklistBroadmail_2();
					$bm_processed = $blacklistBM->blacklistAll(
						$rcpIds,
						$mID
					);
					
					$messageDataArray[] = 'blacklistAll: ' . $bm_processed . \chr(13);
					break;
                        
                                 case 34: #CEOO
                                 case 35: # CEOO Black    
				
                                     $blacklistML = new blacklistMaileon_2();
					$ml_processed = $blacklistML->blacklistAll(
						$rcpIds,
						$mID,
                                             $maspId
					);
					
					$messageDataArray[] = 'blacklistAll: ' . $k_processed . \chr(13);
					break;   
                                case 36:        

                                      $blacklistSE = new blacklistSendEffect_2();
                                       $se_processed = $blacklistSE->blacklistAll(
                                            $rcpIds,
                                            $mID,
                                            $maspId
                                              );

                                          $messageDataArray[] = 'blacklistAll: ' . $k_processed . \chr(13);
                                            break;                                 
			}
                        
                        
		}
	}
	
	$messageDataArray[] = 'end: ' . \time() . \chr(13);
	//\DebugAndExceptionUtils::sendDebugData($messageDataArray, __FILE__ . ': ' . $mID);
} catch (\Exception $e) {
	\DebugAndExceptionUtils::sendDebugData($e, __FILE__ . ': ' . $mID);
}
unset($messageDataArray);