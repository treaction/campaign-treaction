<?php

class blacklistMaileon_2 {

    private $authentification = NULL;
    private $dbh = null;
    private $dbh2 = null;

    public function __construct() {
        $this->dbh = new PDO('mysql:host=localhost;dbname=blacklist', 'blackuser', 'ati21coyu09t');
        $this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
        $this->dbh->exec("set names utf8");
        $this->dbh2->exec("set names utf8");
    }

    public function blacklistAll($rcpIds, $mID, $maspId = 0) {
        
        $qry_login = ' SELECT `m_login`, `api_user`, `pw`, `verteiler`, `masp_id` '
                . ' FROM `mandant_asp` '
                . ' WHERE `m_id` = :mID '
                .' AND `masp_id` IN ( 50, 51)';
        
        $qry_login2 = ' SELECT `m_login`, `api_user`, `pw`, `verteiler`, `asp_id` '
                . ' FROM `mandant_asp` '
                . ' WHERE `masp_id` = :maspId ';
        #   .' AND `asp_id` = 8 ';
        
        $qry = ' UPDATE `asp_blacklist_2` '
                . ' SET `status` = :status, '
                . ' `processed` = NOW() '
                . ' WHERE `participient_id` = :id '
                . ' AND `mandant_asp_id` = :masp_id ';

        $qryUnprocessed = ' SELECT * '
                . ' FROM `asp_blacklist_2` as `asp` '
                . ' INNER JOIN `Blacklist_2` as `b` ON `asp`.`participient_id` = `b`.`ID` '
                . ' WHERE `asp`.`mandant_asp_id` = :masp_id '
                . ' AND `asp`.`status` = 0 ';

        $qry_error = ' UPDATE `asp_blacklist_2` '
                . ' SET `reply` = :error_message, '
                . ' `processed` = NOW() '
                . ' WHERE `participient_id` = :id '
                . ' AND `mandant_asp_id` = :masp_id ';
    
        $stmt_login = $this->dbh2->prepare($qry_login);
        $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt_login->execute();
        $result_login = $stmt_login->fetchAll();
       
        foreach ($result_login as $row) {
            $masp_id = $row['masp_id'];
            $mandant_id = $row['m_login'];
            $username = $row['api_user'];
            $pw = $row['pw'];
            
            if($maspId == 0){
                $mandantid = $masp_id;
            }else{
                $mandantid = $maspId;
            }
               
            switch ($mandantid) {
                
                 case 50: # CEOO
                    $blId = 39373;
                    $apikey = "95d11791-8682-4a56-9da1-65c938595c02";
                    break; 
                 case 51: # CEOO Black 
                    $blId = 39374;
                    $apikey = "3f705491-d90d-4d36-9c21-22265d2b9d69";
                    break; 
                 
            }
           
            $this->authentification = array(
                "BASE_URI" => "https://api.maileon.com/1.0",
                "API_KEY" => $pw,
                "PROXY_HOST" => "",
                "PROXY_PORT" => "",
                "THROW_EXCEPTION" => true,
                "TIMEOUT" => 1000, // 5 seconds
                "DEBUG" => "false" // NEVER enable on production
            );
 
            $blacklistsService = new \com_maileon_api_blacklists_BlacklistsService($this->authentification);
            $blacklistsService->setDebug(FALSE);          
              /*  if($mandantid == 21){     
               try {
                
                $stmtUnprocessed = $this->dbh->prepare($qryUnprocessed);
                $stmtUnprocessed->bindParam(':masp_id', $mandantid, PDO::PARAM_INT);
                $stmtUnprocessed->execute();
                $resultUnprocessed = $stmtUnprocessed->fetchAll();
                $importName = date("Y-m-d H:i:s") . ' Backlisten Sync';
                
                if (count($resultUnprocessed) > 0) {
                    foreach ($resultUnprocessed as $row) {
                        $emails[] = $row['Email'];
                    }
                    $response = $blacklistsService->addEntriesToBlacklist(
                            $blId, $emails, $importName
                    );
                    if ($response->isSuccess()) {
                        $status = 1;
                    } else {
                        $status = 0;
                    }
                    foreach ($resultUnprocessed as $row) {                      
                        $id = $row['ID'];
                        $stmt = $this->dbh->prepare($qry);
                        $stmt->bindParam(':status', $status, PDO::PARAM_INT);
                        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                        $stmt->bindParam(':masp_id', $mandantid, PDO::PARAM_INT);
                        $stmt->execute();
                    }
                    print $response->getResult();
                }
            } catch (Exception $e) {
                $error_message = $e->getMessage();

                foreach ($rcpIds as $rcpId) {
                    $id = $rcpId['id'];
                    $stmt_err = $this->dbh->prepare($qry_error);
                    $stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
                    $stmt_err->bindParam(':id', $id, PDO::PARAM_INT);
                    $stmt_err->bindParam(':masp_id', $mandantid, PDO::PARAM_INT);
                    $stmt_err->execute();
                }
            }
            $mandantid = 14;
        }*/
            try {
                
                $stmtUnprocessed = $this->dbh->prepare($qryUnprocessed);
                $stmtUnprocessed->bindParam(':masp_id', $mandantid, PDO::PARAM_INT);
                $stmtUnprocessed->execute();
                $resultUnprocessed = $stmtUnprocessed->fetchAll();
                $importName = date("Y-m-d H:i:s") . ' Backlisten Sync';                
                if (count($resultUnprocessed) > 0) {
                    foreach ($resultUnprocessed as $row) {
                        $emails[] = $row['Email'];
                    }
                    $response = $blacklistsService->addEntriesToBlacklist(
                            $blId, $emails, $importName
                    );

                    if ($response->isSuccess()) {
                        $status = 1;
                    } else {
                        $status = 0;
                    }
                    foreach ($resultUnprocessed as $row) {                      
                        $id = $row['ID'];
                        $stmt = $this->dbh->prepare($qry);
                        $stmt->bindParam(':status', $status, PDO::PARAM_INT);
                        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                        $stmt->bindParam(':masp_id', $mandantid, PDO::PARAM_INT);
                        $stmt->execute();
                    }
                    print $response->getResult();
                }
            } catch (Exception $e) {
                $error_message = $e->getMessage();

                foreach ($rcpIds as $rcpId) {
                    $id = $rcpId['id'];
                    $stmt_err = $this->dbh->prepare($qry_error);
                    $stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
                    $stmt_err->bindParam(':id', $id, PDO::PARAM_INT);
                    $stmt_err->bindParam(':masp_id', $mandantid, PDO::PARAM_INT);
                    $stmt_err->execute();
                }
            }
        }

        return true;
    }

}

?>