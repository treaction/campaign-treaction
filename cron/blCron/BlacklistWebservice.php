<?php

class blacklistWebservice {

    private $dbh = null;
    private $dbh2 = null;

    public function __construct() {
        $this->dbh = new PDO('mysql:host=localhost;dbname=blacklist', 'blackuser', 'ati21coyu09t');
        $this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
        $this->dbh_unsub = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
    }

    public function getMandantID() {
        $m_ids_Arr = array();
        $qry = ' SELECT DISTINCT(m_id) '
                . ' FROM `mandant_asp` ';

        $stmt = $this->dbh2->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $m_ids_Arr[] = $row['m_id'];
        }
        return $m_ids_Arr;
    }

    public function getMandantAsps($mID) {
        $m_asp_Arr = array();
        $qry = ' SELECT asp_id '
                . ' FROM `mandant_asp` '
                . ' WHERE `m_id` = :mID '
                . ' AND `active` = 1';
        //. ' ORDER BY \'ASC\' LIMIT 2';

        $stmt = $this->dbh2->prepare($qry);
        $stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $m_asp_Arr[] = $row['asp_id'];
        }
        return $m_asp_Arr;
    }

    public function getMandantAspsFromMaspId($mID) {
        $m_asp_Arr = array();
        $qry = ' SELECT asp_id '
                . ' FROM `mandant_asp` '
                . ' WHERE `masp_id` = :mID '
                . ' AND `active` = 1';
        //. ' ORDER BY \'ASC\' LIMIT 2';

        $stmt = $this->dbh2->prepare($qry);
        $stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $m_asp_Arr[] = $row['asp_id'];
        }
        return $m_asp_Arr;
    }
    
    public function getAllAsps() {
        $m_asp_Arr = array();
        $qry = ' SELECT `masp_id` '
                . ' FROM `mandant_asp` '
                . ' WHERE `active` = 1 AND  `asp_id` IN ( 7, 8 )';

        $stmt = $this->dbh2->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $m_asp_Arr[] = $row['masp_id'];
        }
        return $m_asp_Arr;
    }

    public function countUnProcessed() {
        $qry = 'SELECT COUNT(*)'
                . ' FROM `Blacklist` '
                . ' WHERE `status` = 0 ';

        if ($query = $this->dbh->query($qry)) {
            $result = $query->fetch(PDO::FETCH_NUM);
            return ($result[0]);
        } else {
            return 0;
        }
    }

    public function insert_asp_blacklist_table() {
        $teilnehmer = NULL;

        $qry = 'SELECT `ID`'
                . ' FROM `Blacklist`'
                . ' WHERE `status` = 0 '
                #. ' AND `unsub` = 0 '
                . ' ORDER BY `ID` DESC'
                . ' LIMIT 250'
        ;

        $qry_masp = 'SELECT `masp_id`, `m_id` '
                . ' FROM `mandant_asp` '
                # . ' WHERE `active` = 1 AND  `asp_id` IN ( 7, 8 )';
                #. ' WHERE `active` = 1 AND  `asp_id` IN ( 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 29)';
                . ' WHERE `active` = 1 AND  `asp_id` IN (8)';

        $qry_check = 'SELECT * '
                . ' FROM `asp_blacklist` '
                . ' WHERE `participient_id` = :participient_id '
                . ' AND `mandant_asp_id` = :mandant_asp_id '
                . ' AND `m_id` = :m_id ';

        $stmt_select = $this->dbh->prepare($qry);
        $stmt_select->execute();
        $result_select = $stmt_select->fetchAll();
        

        $stmt_masp = $this->dbh2->prepare($qry_masp);
        $stmt_masp->execute();
        $result_masp = $stmt_masp->fetchAll();
        
        /*
          array_push($result_masp, array(
          'masp_id' => '0',
          'm_id' => '0'
          )
          );
         */
        foreach ($result_select as $row) {
            foreach ($result_masp as $row_masp) {
                $exist = '';
                $teilnehmer = '';

                $stmt_check = $this->dbh->prepare($qry_check);
                $stmt_check->bindParam(':participient_id', $row['ID'], PDO::PARAM_INT);
                $stmt_check->bindParam(':mandant_asp_id', $row_masp['masp_id'], PDO::PARAM_INT);
                $stmt_check->bindParam(':m_id', $row_masp['m_id'], PDO::PARAM_INT);
                $stmt_check->execute();
                $result_check = $stmt_check->fetchAll();
                var_dump("result_check",$result_check);
                foreach ($result_check as $row_check) {
                    $exist = $row_check['participient_id'];
                }
                var_dump("exist",$exist);
                if (!$exist) {
                    $teilnehmer = "(" . $row['ID'] . ", " . $row_masp['masp_id'] . ", " . $row_masp['m_id'] . " )";

                    $qry_asps = 'INSERT INTO `asp_blacklist` '
                            . ' (`participient_id`, `mandant_asp_id`, `m_id`) '
                            . ' VALUES '
                            . $teilnehmer;
                    $stmt_asps = $this->dbh->prepare($qry_asps);
                    $stmt_asps->execute();
                }
            }
        }

        #$teilnehmer = substr($teilnehmer,0,-1);
    }

    public function insertUnsub($rcpIds, $mID) {

        $rcpValues = '';
        foreach ($rcpIds as $rcpEmail) {
            if (strpos($rcpEmail[0], "*@") === false) {
                $rcpValues .= "('" . $rcpEmail[0] . "', NOW(), " . $mID . "),";
            }
        }

        $rcpValues = substr($rcpValues, 0, -1);

        $qry_insert = ' REPLACE INTO mandant_' . $mID
                . ' (`email`,`created_at`,`mandant`) '
                . ' VALUES '
                . $rcpValues;

        $stmt_insert = $this->dbh_unsub->prepare($qry_insert);
        $stmt_insert->execute();
    }

    public function getAll($limit) {
        $data = array();

        $qry = 'SELECT `ID`,`Email`, `Grund` '
                . ' FROM `Blacklist` '
                . ' WHERE `status` = 0 '
                . ' ORDER BY `ID` DESC '
                . ' LIMIT 0,' . $limit;

        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $data[] = array('id' => $row['ID'], 'email' => $row['Email'], 'grund' => $row['Grund']);
        }
        return $data;
    }

    public function getAllUnsub($limit) {
        $data = array();

        $qry = 'SELECT `Email`'
                . ' FROM `Blacklist` '
                . ' WHERE `unsub` = 0 '
                . ' LIMIT 0,' . $limit;

        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $data[] = array($row['0']);
        }
        return $data;
    }

    public function getStatus($id, $asp_id_array) {
        $data = 0;
        # foreach ($asp_id_array as $masp_id) {

        $qry_status = ' SELECT SUM(`status`) as `summe` '
                . ' FROM `asp_blacklist` '
                . ' WHERE `participient_id` = :id ';

        $stmt_status = $this->dbh->prepare($qry_status);
        $stmt_status->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt_status->execute();
        $result = $stmt_status->fetchAll();

        foreach ($result as $row) {
            $data = $row['summe'];
        }
        # }
        return $data;
    }

    public function logProcessed($rcpIds, $asp_id_array) {
        $m_asp_anzahl = count($asp_id_array);

        $qry_upd = ' UPDATE `Blacklist` '
                . ' SET `status` = :status, '
                . ' `processed_at` = NOW() '
                . ' WHERE `ID` = :id ';

        $qry_del = ' DELETE FROM `asp_blacklist` '
                . ' WHERE `participient_id` = :id ';

        foreach ($rcpIds as $rcpId) {
            $statusCnt = 0;
            $id = $rcpId['id'];
            $email = $rcpId['email'];
            $statusCnt = $this->getStatus($id, $asp_id_array);

            # uncomment this after the testing of Taglish angebote.    
          #  if ($statusCnt >= $m_asp_anzahl) {
                $status = 1;
              #  $stmt_del = $this->dbh->prepare($qry_del);
              #  $stmt_del->bindParam(':id', $id, PDO::PARAM_INT);
              #  $stmt_del->execute();
            #} else {
            #    $status = 0;
            #}*/
            $status = 1;
            $stmt_upd = $this->dbh->prepare($qry_upd);
            $stmt_upd->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt_upd->bindParam(':status', $status, PDO::PARAM_INT);
            $stmt_upd->execute();
        }
    }

    public function logUnsub($rcpIds) {

        $qry_upd = ' UPDATE `Blacklist` '
                . ' SET `unsub` = 1 '
                . ' WHERE `Email` = :rcpEmail ';

        foreach ($rcpIds as $rcpId) {
            $rcpEmail = $rcpId['email'];
            $stmt_upd = $this->dbh->prepare($qry_upd);
            $stmt_upd->bindParam(':rcpEmail', $rcpEmail, PDO::PARAM_STR);
            $stmt_upd->execute();
        }
    }

}

?>