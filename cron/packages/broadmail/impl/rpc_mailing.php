<?php
    /**
     * (C)opyright 2002-2009 optivo GmbH, Wallstraße 16, 10179 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface MailingWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Feb 24 16:20:55 CET 2010
     */

    class BroadmailRpcMailingWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcMailingWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'Mailing', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function setProperty( $p1,  $p2,  $p3) {
            return $this->_call('setProperty', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.String', $p3)));
        }

        function getProperty( $p1,  $p2) {
            return $this->_call('getProperty', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getName( $p1) {
            return $this->_call('getName', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function remove( $p1) {
            return $this->_call('remove', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function start( $p1,  $p2) {
            return $this->_call('start', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.util.Date', $p2)));
        }

        function resume( $p1) {
            return $this->_call('resume', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setName( $p1,  $p2) {
            return $this->_call('setName', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function copy( $p1) {
            return $this->_call('copy', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function create( $p1,  $p2,  $p3,  $p4,  $p5,  $p6,  $p7) {
            return $this->_call('create', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.String', $p3), 'p5' => $this->_convert('java.lang.Long[]', $p4), 'p6' => $this->_convert('java.lang.String', $p5), 'p7' => $this->_convert('java.lang.String', $p6), 'p8' => $this->_convert('java.lang.String', $p7)));
        }

        function getContent( $p1,  $p2) {
            return $this->_call('getContent', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getStatus( $p1) {
            return $this->_call('getStatus', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getDescription( $p1) {
            return $this->_call('getDescription', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setDescription( $p1,  $p2) {
            return $this->_call('setDescription', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function setFrom( $p1,  $p2,  $p3) {
            return $this->_call('setFrom', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.String', $p3)));
        }

        function getCount( $p1) {
            return $this->_call('getCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function getHeader( $p1,  $p2) {
            return $this->_call('getHeader', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function setContent( $p1,  $p2,  $p3) {
            return $this->_call('setContent', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.String', $p3)));
        }

        function getMimeType( $p1) {
            return $this->_call('getMimeType', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setMimeType( $p1,  $p2) {
            return $this->_call('setMimeType', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getDataSet( $p1) {
            return $this->_call('getDataSet', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function setHeader( $p1,  $p2,  $p3) {
            return $this->_call('setHeader', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.String', $p3)));
        }

        function getIds( $p1) {
            return $this->_call('getIds', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function getIdsInStatus( $p1,  $p2) {
            return $this->_call('getIdsInStatus', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getScheduleDate( $p1) {
            return $this->_call('getScheduleDate', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getCreatedDate( $p1) {
            return $this->_call('getCreatedDate', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getFromName( $p1) {
            return $this->_call('getFromName', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getFromEmailPrefix( $p1) {
            return $this->_call('getFromEmailPrefix', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setCharset( $p1,  $p2) {
            return $this->_call('setCharset', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getCharset( $p1) {
            return $this->_call('getCharset', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function encodeTrackingLinks( $p1,  $p2,  $p3) {
            return $this->_call('encodeTrackingLinks', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.String', $p3)));
        }

        function decodeTrackingLinks( $p1,  $p2,  $p3) {
            return $this->_call('decodeTrackingLinks', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.String', $p3)));
        }

        function setSubject( $p1,  $p2) {
            return $this->_call('setSubject', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getSubject( $p1) {
            return $this->_call('getSubject', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setVerpEnabled( $p1,  $p2) {
            return $this->_call('setVerpEnabled', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2)));
        }

        function isVerpEnabled( $p1) {
            return $this->_call('isVerpEnabled', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function validateContent( $p1,  $p2,  $p3) {
            return $this->_call('validateContent', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.String', $p3)));
        }

        function setAttachmentIds( $p1,  $p2) {
            return $this->_call('setAttachmentIds', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long[]', $p2)));
        }

        function getAttachmentIds( $p1) {
            return $this->_call('getAttachmentIds', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setRecipientFilterId( $p1,  $p2) {
            return $this->_call('setRecipientFilterId', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2)));
        }

        function setRecipientFilterIds( $p1,  $p2) {
            return $this->_call('setRecipientFilterIds', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long[]', $p2)));
        }

        function getRecipientFilterId( $p1) {
            return $this->_call('getRecipientFilterId', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getRecipientFilterIds( $p1) {
            return $this->_call('getRecipientFilterIds', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setDefaultRecipientFilterEnabled( $p1,  $p2) {
            return $this->_call('setDefaultRecipientFilterEnabled', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2)));
        }

        function isDefaultRecipientFilterEnabled( $p1) {
            return $this->_call('isDefaultRecipientFilterEnabled', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setOpenTrackingEnabled( $p1,  $p2) {
            return $this->_call('setOpenTrackingEnabled', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2)));
        }

        function isOpenTrackingEnabled( $p1) {
            return $this->_call('isOpenTrackingEnabled', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setClickTrackingEnabled( $p1,  $p2,  $p3) {
            return $this->_call('setClickTrackingEnabled', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.Boolean', $p3)));
        }

        function isClickTrackingEnabled( $p1,  $p2) {
            return $this->_call('isClickTrackingEnabled', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getRecipientListIds( $p1) {
            return $this->_call('getRecipientListIds', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setRecipientListIds( $p1,  $p2) {
            return $this->_call('setRecipientListIds', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long[]', $p2)));
        }

        function getMaxRecipients( $p1) {
            return $this->_call('getMaxRecipients', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function isMaxRecipientsRandomOrder( $p1) {
            return $this->_call('isMaxRecipientsRandomOrder', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setMaxRecipients( $p1,  $p2,  $p3) {
            return $this->_call('setMaxRecipients', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Integer', $p2), 'p4' => $this->_convert('java.lang.Boolean', $p3)));
        }

        function setMaxRecipientsPercentage( $p1,  $p2,  $p3) {
            return $this->_call('setMaxRecipientsPercentage', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Float', $p2), 'p4' => $this->_convert('java.lang.Boolean', $p3)));
        }

        function isMaxRecipientsPercentage( $p1) {
            return $this->_call('isMaxRecipientsPercentage', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getMaxMailsPerHour( $p1) {
            return $this->_call('getMaxMailsPerHour', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setMaxMailsPerHour( $p1,  $p2) {
            return $this->_call('setMaxMailsPerHour', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Integer', $p2)));
        }

        function getOverallRecipientCount( $p1) {
            return $this->_call('getOverallRecipientCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getSentRecipientCount( $p1) {
            return $this->_call('getSentRecipientCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getFailedRecipientCount( $p1) {
            return $this->_call('getFailedRecipientCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getPredictedRecipientCount( $p1) {
            return $this->_call('getPredictedRecipientCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function pause( $p1) {
            return $this->_call('pause', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function cancel( $p1) {
            return $this->_call('cancel', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function restart( $p1) {
            return $this->_call('restart', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getSendingStartedDate( $p1) {
            return $this->_call('getSendingStartedDate', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getSendingFinishedDate( $p1) {
            return $this->_call('getSendingFinishedDate', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function sendMail( $p1,  $p2,  $p3) {
            return $this->_call('sendMail', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String', $p3)));
        }

        function sendMails( $p1,  $p2,  $p3) {
            return $this->_call('sendMails', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String[]', $p3)));
        }

        function sendTestMail( $p1,  $p2,  $p3) {
            return $this->_call('sendTestMail', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String', $p3)));
        }

        function sendTestMails( $p1,  $p2,  $p3) {
            return $this->_call('sendTestMails', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String[]', $p3)));
        }

        function sendTestMailToAll( $p1,  $p2) {
            return $this->_call('sendTestMailToAll', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2)));
        }

        function getSendingStatus( $p1,  $p2,  $p3) {
            return $this->_call('getSendingStatus', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String', $p3)));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>
