<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/library/packages/broadmail/broadmail_rpc.php');

class BroadmailUnsubscribe
{
	private $broadmailHandler;
	private $listID;
	
	function __construct($lid = NULL)
	{
		$this->broadmailHandler = new BroadmailRpcFactory(1242928686, 'interactiveone.api', '&V2U$4Lnq');
		
		$this->listID = 1242928688;
		
		if(is_numeric($lid)) {
			$this->listID = $lid;
		}		
	}
	
	function remove($email)
	{
		$unsubMail = array($email);
		
		$rcp   = $this->broadmailHandler->newRecipientWebservice();
		$check = $rcp->removeAll($this->listID, $unsubMail);
		
		if(in_array(true, $check)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	function __destruct()
	{
		if($this->broadmailHandler) {
			$this->broadmailHandler->logout();
		}
	}
}
?>