<?php

/**
 * Description of ImportUnsubscribePromio
 *
 * @author SamiMohamedJarmoud
 */
class ImportUnsubscribePromio {
     
        private $authentification = null; 
	private $dbh = null;
	private $dbh2 = null;

	public function __construct()
	{
		$this->dbh = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
		$this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
	}
        
         private function setCurl($ch,$endpoint, $apiKey, $sendId){
         
          
         
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
           'API-Key:'. $apiKey,
           'Sender-ID:'. $sendId,
            'Content-Type: application/json'
           ));
          
          curl_setopt($ch, CURLOPT_URL, $endpoint);
       }
    
     
        public function importUnsubscribeAll($mID){
            
            $data = array();
            $err = NULL;
		
		$qry_login = ' SELECT `m_login`, `api_user`,`secret_key`,`pw` '
	                    .' FROM `mandant_asp` '
                            .' WHERE `m_id` = :mID '
                            .' AND `asp_id` = 22 ';
                
            $stmt_login = $this->dbh2->prepare($qry_login);
            $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
            $stmt_login->execute();
            $result_login = $stmt_login->fetchAll(); 
            
            $dateNow = new \DateTime('now');
	   # $dateNow->setTime(23, 59, 59);
            $dateFrom = clone $dateNow;
	    $dateFrom->sub(new \DateInterval('P1D'));
	    $fromDateTime = $dateFrom->format('Y-m-d');
 
          foreach($result_login as $row) {
                  $username = $row['api_user'];
                  $clientId = $row['m_login'];
                  $pw = $row['secret_key'];
            } 
            
            $ch = curl_init();
            
             $this->setCurl( $ch,
                           'https://api.promio-connect.com/3.0/api/user/unsubscribe-collection',
                           $pw,
                           (int)$clientId
                    );
          
               try{
                    
                    for($count = 1 ; $count < 10 ; $count++){
                        $data['date'] = $fromDateTime;
                        $data['page'] = (int)$count;
                        $data['pageSize'] = 500;
                        
                        $jsonData = json_encode( $data);
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, $jsonData );
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                   
                       $result = curl_exec($ch);
                       $response = json_decode($result, true);                      
                            foreach($response['items'] as $unsubscriber){
                                    $dataPromio[$unsubscriber['mail']]= array(
                                    'email' => $unsubscriber['mail']
                            );
                          }
                       }     
               } catch (Exception $ex) {
                      \DebugAndExceptionUtils::sendDebugData($ex, __FILE__ . ': ' . $mID);
               }
           return $dataPromio;
        }
}
