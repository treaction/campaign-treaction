<?php

/** Error reporting */
#error_reporting(E_ALL);

/** Include path * */
#ini_set('include_path', ini_get('include_path').';../Classes/');

/** PHPExcel */
require_once 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
require_once 'PHPExcel/Writer/Excel2007.php';


$title = "Standalones " . $jahr;
// Create new PHPExcel object
#echo date('H:i:s') . " Create new PHPExcel object\n";
$objPHPExcel = new PHPExcel();

// Set properties
#echo date('H:i:s') . " Set properties<br />";
$objPHPExcel->getProperties()->setCreator("EMS Excel");
$objPHPExcel->getProperties()->setLastModifiedBy("EMS Excel");
$objPHPExcel->getProperties()->setTitle();
$objPHPExcel->getProperties()->setSubject("");
$objPHPExcel->getProperties()->setDescription("");
$sheet = $objPHPExcel->getActiveSheet();

// Add some data
#echo date('H:i:s') . " Add some data<br />";
$objPHPExcel->setActiveSheetIndex(0);


$i = 1;
foreach ($felderTitleArr as $feldTitleKey => $feldTitle) {
    $objPHPExcel->getActiveSheet()->SetCellValue($feldTitleKey, $feldTitle);
    $i++;
}

$objPHPExcel->getActiveSheet()->setTitle($title);

$cDataCnt = count($cDataArr);
foreach ($cDataArr as $row => $contact) {
    $col = 0;
    foreach ($contact as $field => $value) {
        $sheet->getCellByColumnAndRow($col, $row + 1)->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_STRING);
        $col++;
    }
}
/*
  $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Hello');
  $objPHPExcel->getActiveSheet()->SetCellValue('B2', 'world!');
  $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Hello');
  $objPHPExcel->getActiveSheet()->SetCellValue('D2', 'world!');
 */
// Rename sheet
#echo date('H:i:s') . " Rename sheet<br />";
#$sheet->getActiveSheet()->setTitle($title);
// Save Excel 2007 file
#echo date('H:i:s') . " Write to Excel2007 format<br />";
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
#$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

$objWriter->save('/data/www/vhosts/emsuite.de/httpdocs/sa_backup/silver/' . $fileName . '.xlsx');
$objPHPExcel->Destruct();
// Echo done
#echo date('H:i:s') . " Done writing file.<br /><br />";
