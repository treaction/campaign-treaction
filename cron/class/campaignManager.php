<?php
class campaignManager {
    private $dbh;
    protected $table;
    
    
    
    
    
    /**
     * __construct
     * 
     * @param   string  $dbHostEms
     * @param   string  $db_name
     * @param   string  $db_user
     * @param   string  $db_pw
     * 
     * @return  void
     */
    public function __construct($dbHostEms, $db_name, $db_user, $db_pw) {
        try {
            $this->dbh = new PDO('mysql:host=' . $dbHostEms . ';dbname=' . $db_name, $db_user, $db_pw);
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Could not connect to server.' . chr(13);
            echo 'MESSAGE::' . $e->getMessage() . chr(13);
        }
    }
    
    
    /**
     * getAndProcessCampaignDataArrayByDateAndMode
     * 
     * @param   string  $date
     * @param   string  $mode
     * @param   csvGenerator    $csvGenerator
     * 
     * @return  boolean
     */
    public function getAndProcessCampaignDataArrayByDateAndMode($date, $mode, csvGenerator $csvGenerator) {
        $result = false;
        
        $addWhere = null;
        switch ($mode) {
            case 'future':
                $addWhere = ' AND `datum` >= :date';
                break;
            
            case 'past':
                $addWhere = ' AND `datum` <= :date';
                break;
        }
        
        $qry = 'SELECT `k_id`, `status`, `k_name`, `agentur`, date_format(`datum`, "%d.%m.%Y %H:%i") as `versanddatum`, `zielgruppe`, `versandsystem`, `abrechnungsart`' . 
                ', `leads`, `preis`, `preis_gesamt`, `versendet`, `gebucht`, `openings_all`, `klicks_all`' . 
                ', `blacklist`, `absendername`, `betreff`, `hash`, `notiz`' . 
            ' FROM `' . $this->table . '`' . 
            ' WHERE `k_id` > "1"' . 
                 $addWhere . 
            ' ORDER BY `datum` DESC'
        ;
        try {
            $stmt = $this->dbh->prepare($qry);
            $stmt->bindParam(':date', $date, PDO::PARAM_STR);
            $stmt->execute();
        } catch (PDOException $e) {
            echo 'The statement failed.' . chr(13);
            echo 'MESSAGE::' . $e->getMessage() . chr(13);
        }

        $dbResultDataArray = $stmt->fetchAll();
        
        if (count($dbResultDataArray) > 0) {
            $result = $this->processData(
                $dbResultDataArray,
                $csvGenerator
            );
        }
        
        return $result;
    }
    
    
    /**
     * processData
     * 
     * @param   array   $dbResultDataArray
     * @param   csvGenerator    $csvGenerator
     * 
     * @return  boolean
     */
    protected function processData(array $dbResultDataArray, csvGenerator $csvGenerator) {
        $resultDataArray = array();
        
        foreach ($dbResultDataArray as $row) {
            // TODO:
            if (is_numeric($row['preis'])) {
                $preis = floatval($row['preis']);
                
                if ($row['abrechnungsart'] == 'TKP') {
                    $preisGesamt = ($row['gebucht'] / 1000) * $preis;
                } else {
                    $preisGesamt = $row['leads'] * $preis;
                }
                
                $preis = sprintf('%01.2f Euro', $preis);
                $preisGesamt = sprintf('%01.2f Euro', $preisGesamt);
            } elseif (is_numeric($row['preis_gesamt'])) {
                $preis = '';
                
                $preisGesamt = sprintf('%01.2f Euro', floatval($row['preis_gesamt']));
            } else {
                $preisGesamt = '';
                $preis = '';
            }

            $notiz = '';
            if ($row['notiz']) {
                $notiz = str_replace("\n", '. ', $row['notiz']);
            }
            $dataArray = array(
                'Status' => util::getStatusName($row['status']),
                'Kampagne' => $row['k_name'],
                'Agentur' => $row['agentur'],
                'Versanddatum' => $row['versanddatum'],
                'Zielgruppe' => $row['zielgruppe'],
                'Versandsystem' => util::getASPName($row['versandsystem']),
                'Abrechng.art' => $row['abrechnungsart'],
                'Leads' => $row['leads'],
                'Preis' => $preis,
                'Preis Gesamt' => $preisGesamt,
                'Versendet' => util::zahl_format($row['versendet']),
                'Gebucht' => util::zahl_format($row['gebucht']),
                'Oeffnungen' => util::zahl_format($row['openings_all']),
                'Oeffnungsrate' => util::getQuote(
                    $row['gebucht'],
                    $row['openings_all']
                ) . '%',
                'Klicks' => util::zahl_format($row['klicks_all']),
                'Klickrate' => util::getQuote(
                    $row['gebucht'],
                    $row['klicks_all']
                ) . '%',
                'Blacklist' => $row['blacklist'],
                'Absendername' => $row['absendername'],
                'Betreff' => html_entity_decode($row['betreff']),
                'Hash' => $row['hash'],
                'Notiz' => $notiz
            );
            
            $csvGenerator->addData($dataArray);
            
            $resultDataArray[$row['kid']] = true;
        }
        
        $result = false;
        if (count($dbResultDataArray) === count($resultDataArray)) {
            $result = true;
        }
        unset($dbResultDataArray, $resultDataArray);
        
        return $result;
    }
    
    
    
    /*
     ************************************************************************************
     * 
     * setter and getter - Function
     * 
     ************************************************************************************
     */
    public function setTable($table) {
        $this->table = $table;
    }
}