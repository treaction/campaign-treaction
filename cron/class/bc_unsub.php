<?php
require_once("container/http.php");

class BackclickUnsubscribe extends CURLConnect
{
	public $nl = 1;
	
	function __construct($nl = NULL, $url = "http://bc.itsint1.de/servlet/web.httpapi?")
	{
		parent::__construct($url);
		
		if($nl) {
			if(is_array($nl)) {
				$this->nl = implode(';', $nl);
			}
			else {
				$this->nl = $nl;
			}
		}
	}
	
	function unsubscribe($email) 
	{
		return $this->_delete($email);
	}
	
	private function _delete($mail) 
	{
		
		$pFields = 'EMAIL=' . urlencode($mail). '&NEWSLETTER=' .urlencode($this->nl). '&DELETE=1&MID=0';
		
		curl_setopt($this->curlConn, CURLOPT_POSTFIELDS, $pFields);
		
		$output = curl_exec($this->curlConn);
		
		if(preg_match("/OK2|OK3/", $output)) {
			return true;
		}
		return false;			
	}
}
?>