<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/library/packages/broadmail/broadmail_rpc.php');
require_once('Check.php');
?><?php

class BroadmailConnector
{
	private $bmHandler 	= NULL;
	private $bmListID 	= 1242928902;
	private $bmOptIn 	= 0;

	function __construct()
	{
		$this->bmHandler = new BroadmailRpcFactory(1242928686, 'interactiveone.api', '&V2U$4Lnq');
		//$this->bmHandler->timeout = 5;
		//$this->bmHandler->responseTimeout = 10;
	}

	protected function setList($id)
	{
		$id = intval($id);
		if($id > 0) {
			$this->bmListID = (int) $id;
		}
	}
	
	protected function isBlacklisted($email)
	{
		return $this->bmHandler->newBlacklistWebservice()->isBlacklisted($email);
	}

	protected function isRecipient($email)
	{
		return $this->bmHandler->newRecipientWebservice()->contains($this->bmListID, $email);
	}

	protected function addRecipient($email, &$data, $optIn)
	{
		if(is_array($data)) {
			$attrNames  = array_keys($data);
			$attrValues = array_values($data);
			
			
			
			if(!$optIn) {
				$optIn = $this->bmOptIn;
			}
			
			return $this->bmHandler->newRecipientWebservice()->add($this->bmListID, $optIn, NULL, $email, $attrNames, $attrValues);
		}
		return false;
	}
}
?><?php

class Subscriber extends BroadmailConnector
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function add($optIn, $mail, $data)
	{
		$email = trim($mail);

		if($this->isBlackListed($email)) {
			return (-5);
		}
		elseif($this->isRecipient($email)) {
			return (-10);
		}
		else {
			 $rcpAdded = $this->addRecipient($email, $data, $optIn);
			
			if(is_bool($rcpAdded)) {
				return 'OK';
			}
			else {
				return 'NOK';
			}
		}
	}
}
?>