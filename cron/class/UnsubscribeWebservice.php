<?php
class UnsubscribeWebservice
{
	private $dbh = null;
	private $dbh2 = null;

	public function __construct()
	{
		$this->dbh = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
                
		$this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
	}

	public function add($rcpId, $ip, $mandantID, $benutzerID)
	{
		$qry = 'REPLACE mandant_'.$mandantID
				.' (`email`, `ip`, `processed_at`, `created_at`, `mandant`, `benutzer`)'
				.' VALUES (:rcpId, :ip, 0, NOW(), :mandantID, :benutzerID)';

		try {
			$stmt = $this->dbh->prepare($qry);
			$stmt->bindParam(':rcpId', $rcpId, PDO::PARAM_STR);
			$stmt->bindParam(':ip', $ip, PDO::PARAM_STR);
			$stmt->bindParam(':mandantID', $mandantID, PDO::PARAM_INT);
			$stmt->bindParam(':benutzerID', $benutzerID, PDO::PARAM_INT);
			$stmt->execute();
		}
		catch(PDOExecption $e) {
			print $e->getMessage();
		}
	}

	public function getMandantID()
    {
        $m_ids_Arr = array();
        $qry = ' SELECT DISTINCT(m_id) '
                .' FROM `mandant_asp` ';

        $stmt = $this->dbh2->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach($result as $row) {
             $m_ids_Arr[] = $row['m_id'];
        }
        return $m_ids_Arr;
    }
	
     public function getMandantAspsImport($mID)
    {
        $m_asp_Arr = array();
        /*
         *  . 'AND asp_id in(8,7,22)'
         */
        $where = '';
        if($mID == 6){
            $where = 'AND masp_id in(10,12,37,18,24)';
        }
        if($mID == 7){
            $where = 'AND masp_id in(14,15,38,25)';
        }
        if($mID < 10){
               $qry = ' SELECT asp_id '
                .' FROM `mandant_asp` '
				.' WHERE `m_id` = :mID '
                                . $where
                #. 'AND asp_id <= 8'
				.' AND active = 1 ';
                                #. ' ORDER BY \'ASC\' LIMIT 2';
        }else{
               $qry = ' SELECT asp_id '
                .' FROM `mandant_asp` '
				.' WHERE `m_id` = :mID '
				.' AND active = 1 '
                                . ' ORDER BY \'ASC\' LIMIT 2';;
        }
     

        $stmt = $this->dbh2->prepare($qry);
		$stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach($result as $row) {
             $m_asp_Arr[] = $row['asp_id'];
        }
        return $m_asp_Arr;
    }
    
    public function getMandantAsps($mID)
    {
        $m_asp_Arr = array();
        /*
         *  . 'AND asp_id in(8,7,22)'
         */
      /*  $where = '';
        if($mID == 6){
            $where = 'AND masp_id in(10,12,37,18)';
        }
        if($mID == 7){
            $where = 'AND masp_id in(14,15,38,30)';
        }
        if($mID < 10){
               $qry = ' SELECT asp_id '
                .' FROM `mandant_asp` '
				.' WHERE `m_id` = :mID '
                                . $where
                #. 'AND asp_id <= 8'
				.' AND active = 1 ';
                                #. ' ORDER BY \'ASC\' LIMIT 2';
        }else{
               $qry = ' SELECT asp_id '
                .' FROM `mandant_asp` '
				.' WHERE `m_id` = :mID '
				.' AND active = 1 '
                                . ' ORDER BY \'ASC\' LIMIT 2';
        }
        */
        switch ($mID) {
            case 6:
                $qry = ' SELECT asp_id '
                .' FROM `mandant_asp` '
				.' WHERE `m_id` = :mID '
                                .'AND masp_id in(10,12,37,18)'
				.' AND active = 1 ';
               break;
            case 7:
                $qry = ' SELECT asp_id '
                .' FROM `mandant_asp` '
				.' WHERE `m_id` = :mID '
                                .'AND masp_id in(14,15,38,30)'
				.' AND active = 1 ';
                break;
            case 14:
                $qry = ' SELECT asp_id '
                .' FROM `mandant_asp` '
				.' WHERE `m_id` = :mID '
				.' AND active = 1 ';
                break;

            default:
                $qry = ' SELECT asp_id '
                .' FROM `mandant_asp` '
				.' WHERE `m_id` = :mID '
				.' AND active = 1 '
                                . ' ORDER BY \'ASC\' LIMIT 2';
                break;
        }

        $stmt = $this->dbh2->prepare($qry);
		$stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach($result as $row) {
             $m_asp_Arr[] = $row['asp_id'];
        }
        return $m_asp_Arr;
    }

	public function countUnProcessed($mID)
	{
		$qry = 'SELECT COUNT(*)'
				.' FROM mandant_'.$mID
				.' WHERE `status` = 0';
		
		$stmt = $this->dbh->prepare($qry);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_NUM);
		return ($result[0]);
	}

	public function insert_asp_table($mID,$asp_id_array)
	{
		$data = array();
		$teilnehmer = NULL;

		$mandantID = $mID;

		$qry = 'SELECT `id`'
				.' FROM mandant_'.$mID
				.' WHERE `status` = 0 '
				.' ORDER BY `id` DESC '
				.' LIMIT 0,250';

		$stmt_select = $this->dbh->prepare($qry);
		$stmt_select->execute();
		$result_select = $stmt_select->fetchAll();

		foreach($result_select as $row) {
			$teilnehmer .= "(".$row['id'].", ".$mID."),";
		}

		$teilnehmer = substr($teilnehmer,0,-1);

		foreach ($asp_id_array as $asp_id) {
				$asp_table = "asp_".$asp_id;
				$qry_asps = ' INSERT INTO '.$asp_table
					.' (`teilnehmer_id`, `mid`) '
					.' VALUES '
					.$teilnehmer;
				$stmt_asps = $this->dbh->prepare($qry_asps);
				$stmt_asps->execute();
		}

		#return $asp_bez;
	}

	public function getAllunprocessed($limit, $mID)
	{
		$data = array();

		$qry = 'SELECT `id`,`email` '
				.' FROM mandant_'.$mID
				.' WHERE `status` = 0 '
				.' ORDER BY `id` DESC '
				.' LIMIT 0,'.$limit;
		
		$stmt = $this->dbh->prepare($qry);
		$stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
		$stmt->execute();
		$result = $stmt->fetchAll();
		
		foreach($result as $row) {
			$data[] = array('id' => $row['id'], 'email' => $row['email']);
		}
		return $data;
	}
	
	public function getAll($limit, $mID, $asp)
	{
		$data = array();
		$asp_table = "asp_".$asp;

		$qry = 'SELECT ag.id, ag.email '
				.' FROM mandant_'.$mID.' as ag, '.$asp_table
				.' WHERE '.$asp_table.'.status = 0 '
				.' AND ag.id = '.$asp_table.'.teilnehmer_id '
				.' AND '.$asp_table.'.mid = '.$mID
				.' ORDER BY ag.id DESC '
				.' LIMIT 0,'.$limit;
		
		$stmt = $this->dbh->prepare($qry);
		$stmt->bindParam(':mID', $mID, PDO::PARAM_INT);
		$stmt->execute();
		$result = $stmt->fetchAll();
		
		foreach($result as $row) {
			$data[] = array('id' => $row['id'], 'email' => $row['email']);
		}
		
		return $data;
	}
	
	public function getStatus($id, $asp_id_array, $mid) 
	{

		$data = 0;
		foreach ($asp_id_array as $asp_id) {
				$asp_table = "asp_".$asp_id;
				$qry_status = ' SELECT distinct(`status`) '
							.' FROM ' .$asp_table
							.' WHERE `teilnehmer_id` = :id '
							.' AND `mid` = :mid ';
							
				$stmt_status = $this->dbh->prepare($qry_status);
				$stmt_status->bindParam(':id', $id, PDO::PARAM_INT);
				$stmt_status->bindParam(':mid', $mid, PDO::PARAM_INT);
				$stmt_status->execute();
				$result = $stmt_status->fetchAll();
				foreach($result as $row) {
					$data += $row['status'];
				}
		}
		
		
		return $data;
	}
	
	public function logProcessed($rcpIds, $asp_id_array, $mID)
	{
		$m_asp_anzahl = count($asp_id_array);
		$qry_upd = 'UPDATE mandant_'.$mID
					.' SET `status` = :status, '
					.' `processed_at` = NOW() '
					.' WHERE `id` = :id ';
					
		
		foreach($rcpIds as $rcpId) {
			$id = $rcpId['id'];
			$email = $rcpId['email'];
			$statusSum = $this->getStatus($id,$asp_id_array,$mID);
			if ($statusSum == $m_asp_anzahl) {
				$status = 1;
				foreach($asp_id_array as $aspid) {
					$qry_upd_ok = 'DELETE FROM asp_'.$aspid.' WHERE `teilnehmer_id` = '.$id.' AND mid ='.$mID;
					$stmt_upd_ok = $this->dbh->prepare($qry_upd_ok);
					$stmt_upd_ok->execute();
				}
				
			} else {
				$status = 0;
			}
			
			$stmt_upd = $this->dbh->prepare($qry_upd);
			$stmt_upd->bindParam(':id', $id, PDO::PARAM_INT);
			$stmt_upd->bindParam(':status', $status, PDO::PARAM_INT);
			$stmt_upd->execute();
		}
		
		
				
	}
	
	public function cleanProcessed($asp_id_array, $mID)
	{
		
		foreach($asp_id_array as $aspid) {
			$qry_clean = 'DELETE `a` 
			FROM asp_'.$aspid.' as `a` 
			LEFT JOIN mandant_'.$mID.' as `m` ON `a`.`teilnehmer_id` = `m`.`id` 
			WHERE `a`.`mid` = '.$mID.' 
			AND `m`.`id` IS NULL ';
			$stmt_clean = $this->dbh->prepare($qry_clean);
			$stmt_clean->execute();
		}
				
				
	}

        public function addRecipients($rcpIds, $mID){
            
            foreach($rcpIds as $rcpId){          
                 $email = $rcpId['email'];
                
                 $this->add(
                         $email, 
                         '', 
                         $mID, 
                         0
                         );
                 
              $result[] = $email; 
            }
           return $result; 
        }
}
?>