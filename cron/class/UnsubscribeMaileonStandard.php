<?php

class UnsubscribeMaileonStandard {

    private $authentification = NULL;
    private $dbh = null;
    private $dbh2 = null;

    public function __construct() {
        $this->dbh = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
        $this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
    }

    public function unsubscribeAll($rcpIds, $mID) {
        $data = array();
        $err = NULL;
        $apikeylist = array();
       
        if ((int)$mID == 7) {
             $qry = ' UPDATE `asp_29` '
			  .' SET `status` = :status, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
            $apikeylist = array(
                '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
                'db1757ee-52bf-4179-b936-bebd403ba669',
                '24d7cb81-c936-4f47-9075-c336a38d9ba6',
                'd26f5ba2-8374-4ff3-ba7c-4872b017ee1c',
                'bd50cb4d-a848-4efe-90ae-347d931858a7', // BLE
                 'c168469b-8f23-4f41-a17b-2ebe0cdb649f' // BLBL
            );
        }
        if ((int)$mID == 6) {
             $qry = ' UPDATE `asp_11` '
			  .' SET `status` = :status, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
            $apikeylist = array(
                '532ddc97-a5ce-4dce-88da-be441690d0a5', //18
                'd682dc3c-2937-4946-b231-3bc63890a8f9', //Basic
                '98413824-b3ae-4eca-8af0-cfd67b591b07', // MDM
                '6ca7acac-a78f-4124-a749-f1424c93e75c', //reserve
                '5826c152-f702-43df-aceb-697c31639f6f', // FMB
                'bd5d0dc6-2859-4cce-bcfb-2218afad8c9f', //FM19
                'b9d2bfcf-6250-4366-9bc1-fc43def57146', //FME
                '57f1ae23-26dc-49a4-a339-c5066fc391e1' //FMBL
            );
        }
         if ((int)$mID == 10) {
            $apikeylist = array(
                'b2bf48fc-438d-4e1e-add9-747797d1f990'
            );
        }  
        
          if ((int)$mID == 14) {
               $qry = ' UPDATE `asp_34` '
			  .' SET `status` = :status, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
            $apikeylist = array(
                '95d11791-8682-4a56-9da1-65c938595c02',
                '3f705491-d90d-4d36-9c21-22265d2b9d69'
            );
        }  
        
        foreach ($apikeylist as $apikey) {
            $this->authentification = array(
                "BASE_URI" => "https://api.maileon.com/1.0",
                "API_KEY" => $apikey,
                "PROXY_HOST" => "",
                "PROXY_PORT" => "",
                "THROW_EXCEPTION" => true,
                "TIMEOUT" => 10000, // 5 seconds
                "DEBUG" => "false" // NEVER enable on production
            );
            $contactsService = new \com_maileon_api_contacts_ContactsService($this->authentification);
            $contactsService->setDebug(FALSE);
            try {
                foreach ($rcpIds as $rcpId) {
                    $status = 1;
                    $response = $contactsService->unsubscribeContactByEmail(
                            $rcpId['email'], null, null
                    );
                  
                                        $id = $rcpId['id'];
					$stmt = $this->dbh->prepare($qry);
					$stmt->bindParam(':status', $status, PDO::PARAM_INT);
					$stmt->bindParam(':id', $id, PDO::PARAM_INT);
					$stmt->execute();
                                        if((int)$mID == 14){
                                             $qry2 = ' UPDATE `asp_35` '
                                                .' SET `status` = :status, '
                                                .' `processed` = NOW() '
                                                .' WHERE `teilnehmer_id` = :id ';
                                             
                                                $id = $rcpId['id'];
                                                $stmt = $this->dbh->prepare($qry2);
                                                $stmt->bindParam(':status', $status, PDO::PARAM_INT);
                                                $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                                                $stmt->execute();
                                        }
                }
            } catch (Exception $e) {
                $error_message = $e->getMessage();
            }
        }
        return $data;
    }

}

?>
