<?php

/**
 * Description of ImportUnsubscribeMaileonStandard
 *
 * @author SamiMohamedJarmoud
 */
class ImportUnsubscribeMaileonStandard {
     
        private $authentification = null; 
	private $dbh = null;
	private $dbh2 = null;

	public function __construct()
	{
		$this->dbh = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
		$this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
	}
        
        public function importUnsubscribeAll($apikey){
            
            $data = array();
            $err = NULL;

            
            $dateNow = new \DateTime('now');
	    $dateNow->setTime(23, 59, 59);
            $dateFrom = clone $dateNow;
	    $dateFrom->sub(new \DateInterval('P1D'));
	    $fromDateTime = $dateFrom->format('d-m-Y');
 
  
            $this->authentification = array(
                        "BASE_URI" => "https://api.maileon.com/1.0",
                        "API_KEY" => $apikey,
                        "PROXY_HOST" => "",
                        "PROXY_PORT" => "",
                        "THROW_EXCEPTION" => true,
                        "TIMEOUT" => 100, // 5 seconds
                        "DEBUG" => "false" // NEVER enable on production
                    );
            $unsubcribersWebservice = new \com_maileon_api_reports_ReportsService($this->authentification);
            $unsubcribersWebservice->setDebug(FALSE);
            
               try{

                     $fromDate = strtotime($fromDateTime).'000';
                     $toDate = null;
                     $mailingIds = null;
                     $contactIds = null; 
                     $contactEmails = null;
                    
                    for($count = 1 ; $count < 20 ; $count++){
	              $response = $unsubcribersWebservice->getUnsubscribers(
                                     $fromDate, 
                                     $toDate, 
                                     $mailingIds, 
                                     $contactIds, 
                                     $contactEmails,
                                     null,
                                     null,
                                    false,
                                    $count,
                                    1000
                              );

                            foreach($response->getResult() as $unsubscriber){
                                    $data[]= array(
                                    'email' => $unsubscriber->toString()
                            );
                          }
                       }     
               } catch (Exception $ex) {
                      \DebugAndExceptionUtils::sendDebugData($ex, __FILE__ . ': ' . $mID);
               }
           return $data;
        }
}
