<?php

class UnsubscribeMaileon
{
	private $authentification = NULL;
	private $dbh = null;
	private $dbh2 = null;
	public function __construct()
	{
		$this->dbh = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
		$this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
	}

	public function unsubscribeAll($rcpIds, $mID)
	{
		$data = array();
		$err = NULL;
		
		$qry_login = ' SELECT `m_login`, `api_user`, `pw` '
	          .' FROM `mandant_asp` '
              .' WHERE `m_id` = :mID '
              .' AND `asp_id` = 8 ';
			  
		$qry = ' UPDATE `asp_8` '
			  .' SET `status` = :status, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
			  
		$qry_error = ' UPDATE `asp_8` '
			  .' SET `reply` = :error_message, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
			  
	    $stmt_login = $this->dbh2->prepare($qry_login);
        $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt_login->execute();
        $result_login = $stmt_login->fetchAll();
      foreach($result_login as $row) {
            $username = $row['api_user'];
            $pw = $row['pw'];
        }
         $this->authentification = array(
                        "BASE_URI" => "https://api.maileon.com/1.0",
                        "API_KEY" => $pw,
                        "PROXY_HOST" => "",
                        "PROXY_PORT" => "",
                        "THROW_EXCEPTION" => true,
                        "TIMEOUT" => 100000, // 5 seconds
                        "DEBUG" => "false" // NEVER enable on production
                    );
        
	$contactsService = new \com_maileon_api_contacts_ContactsService($this->authentification);
        $contactsService->setDebug(FALSE);
		try {      
		       foreach($rcpIds as $rcpId)
				{
					$status = 1;
                                        $response = $contactsService->unsubscribeContactByEmail(
                                                $rcpId['email'], 
                                                null,
                                                null
                                                );                                       
                      if (!$response->isSuccess()) {
                            throw  \DebugAndExceptionUtils::sendDebugData($response->getBodyData());
                          }
					$id = $rcpId['id'];
					$stmt = $this->dbh->prepare($qry);
					$stmt->bindParam(':status', $status, PDO::PARAM_INT);
					$stmt->bindParam(':id', $id, PDO::PARAM_INT);
					$stmt->execute();
				}

		} catch (Exception $e) {
			$error_message = $e->getMessage();
                        
			foreach($rcpIds as $rcpId) {
				$id = $rcpId['id'];
				$stmt_err = $this->dbh->prepare($qry_error);
				$stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
				$stmt_err->bindParam(':id', $id, PDO::PARAM_INT);
				$stmt_err->execute();
			}
		}
		
		return $data;
	}

}
?>
