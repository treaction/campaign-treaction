<?php
class updateBroadmailManager {
    protected $bmConnection = null;
    
    
    
    
    
    public function getMailingData($mandant_id, $api_user, $pw, $mailing_id, $urlInternArr) {
        try {
            $this->bmConnection = new BroadmailRpcFactory(
                $mandant_id,
                $api_user,
                $pw
            );
            if ($this->bmConnection->getError()) {
                throw new Exception('Login Fehler oder Server nicht erreichbar');
            } else {
                $mailingDataArr = array();
                
                $MailingWebservice = $this->bmConnection->newMailingWebservice();
                $MailingReportingWebservice = $this->bmConnection->newMailingReportingWebservice();
                $ResponseWebservice = $this->bmConnection->newResponseWebservice();

                $status_bm = $MailingWebservice->getStatus($mailing_id);

                if ($status_bm == 'DONE' || $status_bm == 'CANCELED' || $status_bm == 'SENDING' || $status_bm == 'NEW') {
                    $openings = $MailingReportingWebservice->getOpenCount(
                        $mailing_id,
                        true
                    );
                    $openings_all = $MailingReportingWebservice->getOpenCount(
                        $mailing_id,
                        false
                    );
                    $klicks = $MailingReportingWebservice->getClickCount(
                        $mailing_id,
                        true
                    );
                    $klicks_all = $MailingReportingWebservice->getClickCount(
                        $mailing_id,
                        false
                    );
                    $softbounces = $ResponseWebservice->getMailingResponseCount(
                        'softbounce',
                        $mailing_id
                    );
                    $hardbounces = $ResponseWebservice->getMailingResponseCount(
                        'hardbounce',
                        $mailing_id
                    );

                    if ($urlInternArr) {
                        $getInternClicks = $MailingReportingWebservice->getClickCountByUrl(
                            $mailing_id,
                            $urlInternArr,
                            true
                        );
                        
                        $getInternClicks_all = $MailingReportingWebservice->getClickCountByUrl(
                            $mailing_id,
                            $urlInternArr,
                            false
                        );

                        if (count($getInternClicks) > 0) {
                            $getSumInternClicks = array_sum($getInternClicks);
                            $getSumInternClicks_all = array_sum($getInternClicks_all);

                            $klicks = $klicks - $getSumInternClicks;
                            $klicks_all = $klicks_all - $getSumInternClicks_all;
                        }
                    }

                    if ($status_bm == 'DONE' || $status_bm == 'CANCELED') {
                        $start = $MailingWebservice->getSendingStartedDate($mailing_id);

                        if (strpos($start, "T")) {
                            $date_ = explode('T', $start);
                            $datum = $date_[0];
                            $zeit = substr($date_[1], 0, 5);
                            $date_teile = explode('-', $date_[0]);
                            $zeit_teile = explode(':', $zeit);
                            $newTime = mktime($zeit_teile[0], $zeit_teile[1], 0, $date_teile[1], $date_teile[2], $date_teile[0]);
                            $z = date('I', $newTime);

                            if ($z == 0) {
                                $newTime += 3600;
                            } else {
                                $newTime += 7200;
                            }

                            $start_neu = date('Y-m-d H:i:s', $newTime);
                        }

                        $ende = $MailingWebservice->getSendingFinishedDate($mailing_id);

                        if (strpos($ende, "T")) {
                            $date_ = explode('T', $ende);
                            $datum = $date_[0];
                            $zeit = substr($date_[1], 0, 5);
                            $date_teile = explode('-', $date_[0]);
                            $zeit_teile = explode(':', $zeit);
                            $newTime = mktime($zeit_teile[0], $zeit_teile[1], 0, $date_teile[1], $date_teile[2], $date_teile[0]);
                            $z = date('I', $newTime);

                            if ($z == 0) {
                                $newTime += 3600;
                            } else {
                                $newTime += 7200;
                            }

                            $ende_neu = date('Y-m-d H:i:s', $newTime);
                        }
                    }
                    
                    $versendet = $MailingWebservice->getSentRecipientCount($mailing_id);
                    $versendet = round($versendet, -1);
                    
                    $from = $MailingWebservice->getFromName($mailing_id);
                    $from = htmlspecialchars($from, ENT_QUOTES);
                    
                    $subject = $MailingWebservice->getSubject($mailing_id);
                    $subject = htmlspecialchars($subject, ENT_QUOTES);
                    
                    // cr-neu
                    $unsubscribeCount = $MailingReportingWebservice->getUnsubscribeCount($mailing_id);

                    $mailingDataArr = array(
                        "openings" => $openings,
                        "openings_all" => $openings_all,
                        "klicks" => $klicks,
                        "klicks_all" => $klicks_all,
                        "softbounces" => $softbounces,
                        "hardbounces" => $hardbounces,
                        "start" => $start_neu,
                        "ende" => $ende_neu,
                        "status" => $status_bm,
                        "versendet" => $versendet,
                        "absendername" => $from,
                        "betreff" => $subject,
                        "abmelder" => $unsubscribeCount
                    );
                }

                return $mailingDataArr;
            }
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            
            return $error_message;
        }
    }

    function __destruct() {
        if ($this->bmConnection) {
            $this->bmConnection->logout();
        }
    }
}
?>