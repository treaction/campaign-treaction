<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/vhosts/campaign-treaction.de/httpdocs/';

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');

$config = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => "98413824-b3ae-4eca-8af0-cfd67b591b07",
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 100000000000000000000000000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug = FALSE;
$contactsService = new com_maileon_api_contacts_ContactsService($config);
$contactsService->setDebug($debug);

$contactfiltersService = new com_maileon_api_contactfilters_ContactfiltersService($config);
$contactfiltersService->setDebug($debug);
$contactfiltersService->refreshContactFilterContacts(2298, time()*1000);  

$config2 = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => "5826c152-f702-43df-aceb-697c31639f6f",
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 100000000000000000000000000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug2 = FALSE;
$contactsService2 = new com_maileon_api_contacts_ContactsService($config2);
$contactsService2->setDebug($debug2);

$start = time();
for ($count = 1; $count < 70; $count++) {
    //Gesamtverteiler_Deutschland 4wave
    //Neu_Verteiler_Deutschland Stellar
    $response = $contactsService->getContactsByFilterId(2298, $count, 1000, array('SALUTATION',
        'FIRSTNAME',
        'LASTNAME',
        'ADDRESS',
        'ZIP',
        'CITY',
        'COUNTRY',
        'BIRTHDAY'
            ), array('PartnerID',
        'DeliveryID',
        'Neu_Verteiler_Deutschland',
        'Gesamtverteiler_Oesterreich',
        'Gesamtverteiler_Premium',
        'Gesamtverteiler_Schweiz')
    );

    foreach ($response->getResult() as $contact) {

        $newContact = new com_maileon_api_contacts_Contact();
        $newContact->anonymous = FALSE;
        $newContact->email = $contact->email;
        $newContact->permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$SALUTATION] = $contact->standard_fields['SALUTATION'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$FIRSTNAME] = $contact->standard_fields['FIRSTNAME'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$LASTNAME] = $contact->standard_fields['LASTNAME'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$ADDRESS] = $contact->standard_fields['ADDRESS'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$ZIP] = $contact->standard_fields['ZIP'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$CITY] = $contact->standard_fields['CITY'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$COUNTRY] = $contact->standard_fields['COUNTRY'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$BIRTHDAY] = $contact->standard_fields['BIRTHDAY'];
        $newContact->custom_fields["PartnerID"] = $contact->custom_fields['PartnerID'];
        $newContact->custom_fields["DeliveryID"] = $contact->custom_fields['DeliveryID'];
        $newContact->custom_fields["Neu_Verteiler_Deutschland"] = (int)$contact->custom_fields['Neu_Verteiler_Deutschland'];
        $newContact->custom_fields["Gesamtverteiler_Oesterreich"] = (int)$contact->custom_fields['Gesamtverteiler_Oesterreich'];
        $newContact->custom_fields["Gesamtverteiler_Premium"] = (int)$contact->custom_fields['Gesamtverteiler_Premium'];
        $newContact->custom_fields["Gesamtverteiler_Schweiz"] = (int)$contact->custom_fields['Gesamtverteiler_Schweiz'];

        $contactsService2->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "src", "subscriptionPage", false, false);
    }
}

$end = time();

echo "Mandant: fulle.media Basic Duration: " . ($end - $start) . " seconds";

