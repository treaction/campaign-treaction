<?php
\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/vhosts/campaign-treaction.de/httpdocs/';
\define('DIR_Utils', $workPath . 'system/library/Utils/');

require_once($workPath . 'cron/class/UnsubscribeWebservice.php');
require_once($workPath . 'cron/class/UnsubscribeBackclick.php');
require_once($workPath . 'cron/class/UnsubscribeElaine.php');
require_once($workPath . 'cron/class/UnsubscribeKajomi.php');
require_once($workPath . 'cron/class/UnsubscribeMailsolution.php');
require_once($workPath . 'cron/class/UnsubscribeSendEffect.php');
require_once($workPath . 'cron/class/UnsubscribeMaileon.php');
require_once($workPath . 'cron/class/UnsubscribeMaileonStandard.php');
require_once($workPath . 'cron/class/UnsubscribePromio.php');

require_once($workPath . 'cron/packages/broadmail/broadmail_rpc.php');
require_once($workPath . 'cron/class/ioClass/UnsubscribeBroadmailByAspTableId.php');
require_once($workPath . 'cron/packages/'. \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');

require_once(DIR_Utils . 'DebugAndExceptionUtils.php');


$messageDataArray = array();
$messageDataArray[] = 'clientId: ' . $mID . \chr(13);
$messageDataArray[] = 'start: ' . \time() . \chr(13);

$limit = 1250;

try {
	$unsubWs = new UnsubscribeWebservice();
	$cnt = $unsubWs->countUnProcessed($mID);
	$m_aspArr = $unsubWs->getMandantAsps($mID);

	$messageDataArray[] = 'count: ' . $cnt . \chr(13);
	$messageDataArray[] = 'asps: ' . \implode('', $m_aspArr) . \chr(13);
	if ($cnt > 0) {
		$unsubWs->insert_asp_table(
			$mID,
			$m_aspArr
		);
		$messageDataArray[] = 'insert_asp_table: ' . 1 . \chr(13) . \chr(13);     

		foreach ($m_aspArr as $m_asp) {
			$messageDataArray[] = \chr(13) . 'asp: ' . $m_asp . \chr(13);
			switch ((int) $m_asp) {
				case 1:
					$rcpIds_bm = $unsubWs->getAll(
						$limit,
						$mID,
						$m_asp
					);
					$messageDataArray[] = 'rcpCount: ' . \count($rcpIds_bm) . \chr(13);

					if (\count($rcpIds_bm) > 0) {
						$unsubBM = new \UnsubscribeBroadmailByAspTableId();
						$unsubBM->setAspTable('asp_' . (int) $m_asp);
						$unsubBM->unsubscribeAll(
							$rcpIds_bm,
							$mID
						);
						unset($unsubBM);

						$messageDataArray[] = 'unsubscribeAll: ' . 1 . \chr(13);
					}
					break;

				case 2:
					$rcpIds_bc = $unsubWs->getAll(
						$limit,
						$mID,
						$m_asp
					);
					$messageDataArray[] = 'rcpCount: ' . \count($rcpIds_bc) . \chr(13);

					if (count($rcpIds_bc) > 0) {
						$unsubBC = new UnsubscribeBackclick();
						$bc_processed = $unsubBC->unsubscribeAll(
							$rcpIds_bc,
							$mID
						);

						$messageDataArray[] = 'unsubscribeAll: ' . 1 . \chr(13);
					}
					break;

				case 3:
					$rcpIds_e = $unsubWs->getAll(
						$limit,
						$mID,
						$m_asp
					);
					$messageDataArray[] = 'rcpCount: ' . \count($rcpIds_e) . \chr(13);

					if (count($rcpIds_e) > 0) {
						$unsubElaine = new UnsubscribeElaine();
						$el_processed = $unsubElaine->unsubscribeAll(
							$rcpIds_e,
							$mID
						);

						$messageDataArray[] = 'unsubscribeAll: ' . 1 . \chr(13);
					}
					break;

				case 4:
					$rcpIds_k = $unsubWs->getAll(
						$limit,
						$mID,
						$m_asp
					);
					$messageDataArray[] = 'rcpCount: ' . \count($rcpIds_k) . \chr(13);

					if (count($rcpIds_k) > 0) {
						$unsubKajomi = new UnsubscribeKajomi();
						$k_processed = $unsubKajomi->unsubscribeAll(
							$rcpIds_k,
							$mID
						);

						$messageDataArray[] = 'unsubscribeAll: ' . 1 . \chr(13);
					}
					break;

				case 5:
					$rcpIds_ms = $unsubWs->getAll(
						$limit,
						$mID,
						$m_asp
					);
					$messageDataArray[] = 'rcpCount: ' . \count($rcpIds_ms) . \chr(13);

					if (count($rcpIds_ms) > 0) {
						$unsubMS = new UnsubscribeMailsolution();
						$ms_processed = $unsubMS->unsubscribeAll(
							$rcpIds_ms,
							$mID
						);

						$messageDataArray[] = 'unsubscribeAll: ' . 1 . \chr(13);
					}
					break;

				case 6:
					$rcpIds_bmFake = $unsubWs->getAll(
						$limit,
						$mID,
						$m_asp
					);
					$messageDataArray[] = 'rcpCount: ' . \count($rcpIds_bmFake) . \chr(13);

					if (\count($rcpIds_bmFake) > 0) {
						$unsubBMFake = new \UnsubscribeBroadmailByAspTableId();
						$unsubBMFake->setAspTable('asp_' . (int) $m_asp);

						$unsubBMFake->unsubscribeAll(
							$rcpIds_bmFake,
							$mID
						);
						unset($unsubBMFake);

						$messageDataArray[] = 'unsubscribeAll: ' . 1 . \chr(13);
					}
					break;
                                
				case 7:
                                case 36:
					$rcpIds_se = $unsubWs->getAll(
						$limit,
						$mID,
						$m_asp
					);
					$messageDataArray[] = 'rcpCount: ' . \count($rcpIds_se) . \chr(13);

					if (count($rcpIds_se) > 0) {
						$unsubSE = new \UnsubscribeSendEffect();
						$se_processed = $unsubSE->unsubscribeAll(
							$rcpIds_se,
							$mID
						);

						$messageDataArray[] = 'unsubscribeAll: ' . 1 . \chr(13);
					}
					break;    
                                case 11:  
				case 8:
                                case 29:
                                case 34:
                                case 35:
					$rcpIds_ml = $unsubWs->getAll(
						$limit,
						$mID,
						$m_asp
					);
					$messageDataArray[] = 'rcpCount: ' . \count($rcpIds_ml) . \chr(13);

                                        
                                        if (count($rcpIds_ml) > 0) {
						$unsubMLStandard = new \UnsubscribeMaileonStandard();
						$se_processed = $unsubMLStandard->unsubscribeAll(
							$rcpIds_ml,
							$mID
						);

						$messageDataArray[] = 'unsubscribeAll: ' . 1 . \chr(13);
					}
                                        // stellar und 4wave maielon rausnehmen web-2826
                                        $mIDArray = array(8,9,10,11,12,13);
					if (count($rcpIds_ml) > 0 && (in_array($mID, $mIDArray))) {                                         
						$unsubML = new \UnsubscribeMaileon();
						$se_processed = $unsubML->unsubscribeAll(
							$rcpIds_ml,
							$mID
						);

						$messageDataArray[] = 'unsubscribeAll: ' . 1 . \chr(13);
					}
					break;
                               case 22:
                                     $rcpIds_pm = $unsubWs->getAll(
						$limit,
						$mID,
						$m_asp
					);
					$messageDataArray[] = 'rcpCount: ' . \count($rcpIds_pm) . \chr(13);
					if (count($rcpIds_pm) > 0) {
						$unsubPM = new \UnsubscribePromio();
						$pm_processed = $unsubPM->unsubscribeAll(
							$rcpIds_pm,
							$mID
						);

						$messageDataArray[] = 'unsubscribeAll: ' . 1 . \chr(13);
					}
                                     break;
                                     
			}
		}

		$rcpIds = $unsubWs->getAllunprocessed(
			$limit,
			$mID
		);
                
		$messageDataArray[] = 'getAllunprocessed: ' . \count($rcpIds) . \chr(13);
                
		$unsubWs->logProcessed(
			$rcpIds,
			$m_aspArr,
			$mID
		);
		$messageDataArray[] = 'logProcessed: ' . 1 . \chr(13);
	}

	$unsubWs->cleanProcessed(
		$m_aspArr,
		$mID
	);
	$messageDataArray[] = 'cleanProcessed: ' . 1 . \chr(13);
	$messageDataArray[] = 'end: ' . \time() . \chr(13);
	
	//\DebugAndExceptionUtils::sendDebugData($messageDataArray, __FILE__ . ': ' . $mID);
} catch (Exception $ex) {
	\DebugAndExceptionUtils::sendDebugData($e, __FILE__ . ': ' . $mID);
}
unset($messageDataArray);