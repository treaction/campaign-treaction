<?php

/**
 * getBasePath
 * 
 * @return string
 */
function getBasePath() {
	return \dirname($_SERVER['SCRIPT_NAME']);
}

/**
 * getEmsWorkRootPath
 * 
 * @return string
 */
function getEmsWorkRootPath() {
	$resultDataArray = \array_filter(\explode(\DIRECTORY_SEPARATOR, getBasePath()));
	
	return $_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $resultDataArray[1] . \DIRECTORY_SEPARATOR;
}