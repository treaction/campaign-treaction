<?php
namespace Modules\Customer\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * Description of ContactPersonEntity
 *
 * @author Cristian.Reus
 */
class ContactPersonEntity extends AbstractEntity {
	
	/**
	 * ap_id
	 * 
	 * @var integer
	 */
	protected $ap_id = NULL;
	
	/**
	 * kunde_id
	 * 
	 * @var integer
	 */
	protected $kunde_id;
	
	/**
	 * anrede
	 * 
	 * @var string
	 */
	protected $anrede;
	
	/**
	 * title
	 * 
	 * @var string
	 */
	protected $titel;
	
	/**
	 * vorname
	 * 
	 * @var string
	 */
	protected $vorname;
	
	/**
	 * nachname
	 * 
	 * @var string
	 */
	protected $nachname;
	
	/**
	 * email
	 * 
	 * @var string
	 */
	protected $email;
	
	/**
	 * telefon
	 * 
	 * @var string
	 */
	protected $telefon;
	
	/**
	 * fax
	 * 
	 * @var string
	 */
	protected $fax;
	
	/**
	 * mobil
	 * 
	 * @var string
	 */
	protected $mobil;
	
	/**
	 * position
	 * 
	 * @var string
	 */
	protected $position;
	
	/**
	 * status
	 * 
	 * @var boolean
	 */
	protected $status;
	
	/**
	 * vertriebler_id
	 * 
	 * @var integer
	 */
	protected $vertriebler_id;
	
	
	/**
	 * customerEntity
	 * 
	 * @var \Modules\Customer\Domain\Model\CustomerEntity
	 * @relations
	 */
	protected $customerEntity;
	
	
	
	/**
	 * crm_ap_id
	 * 
	 * @var string
	 * @deprecated
	 */
	protected $crm_ap_id;
	
	
	
	public function __construct($primaryFieldName = 'ap_id') {
		parent::__construct($primaryFieldName);
	}
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter
     *
     *******************************************************************************************/
	public function getAp_id() {
		return (int) $this->ap_id;
	}
	public function setAp_id($ap_id) {
		$this->ap_id = \intval($ap_id);
	}

	public function getKunde_id() {
		return (int) $this->kunde_id;
	}
	public function setKunde_id($kunde_id) {
		$this->kunde_id = \intval($kunde_id);
	}

	public function getAnrede() {
		return $this->anrede;
	}
	public function setAnrede($anrede) {
		$this->anrede = $anrede;
	}

	public function getTitel() {
		return $this->titel;
	}
	public function setTitel($titel) {
		$this->titel = $titel;
	}

	public function getVorname() {
		return $this->vorname;
	}
	public function setVorname($vorname) {
		$this->vorname = $vorname;
	}

	public function getNachname() {
		return $this->nachname;
	}
	public function setNachname($nachname) {
		$this->nachname = $nachname;
	}

	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
	}

	public function getTelefon() {
		return $this->telefon;
	}
	public function setTelefon($telefon) {
		$this->telefon = $telefon;
	}

	public function getFax() {
		return $this->fax;
	}
	public function setFax($fax) {
		$this->fax = $fax;
	}

	public function getMobil() {
		return $this->mobil;
	}
	public function setMobil($mobil) {
		$this->mobil = $mobil;
	}

	public function getPosition() {
		return $this->position;
	}
	public function setPosition($position) {
		$this->position = $position;
	}

	public function getStatus() {
		return (boolean) $this->status;
	}
	public function setStatus($status) {
		if ((int) $status === 1) {
			$status = true;
		} else {
			$status = false;
		}
		
		$this->status = $status;
	}

	public function getVertriebler_id() {
		return (int) $this->vertriebler_id;
	}
	public function setVertriebler_id($vertriebler_id) {
		$this->vertriebler_id = \intval($vertriebler_id);
	}
	
	
	public function getCustomerEntity() {
		return $this->customerEntity;
	}
	public function setCustomerEntity(\Modules\Customer\Domain\Model\CustomerEntity $customerEntity) {
		$this->customerEntity = $customerEntity;
	}
	
	
	
	/**
	 * @deprecated
	 */
	public function getCrm_ap_id() {
		return $this->crm_ap_id;
	}
	public function setCrm_ap_id($crm_ap_id) {
		$this->crm_ap_id = $crm_ap_id;
	}

}
