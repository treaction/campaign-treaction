<?php
header('Content-Type: text/html; charset=UTF-8');
?>
<table class="scrollTable2" id="scrollTable2" cellpadding="0" cellspacing="0" border="0" bgcolor="#F5F5F5">
    <tr>
        <th>ESP</th>
        <th>Gesamtverteiler Deutschland</th>
        <th>Gesamtverteiler Oesterreich</th>
        <th> Gesamtverteiler Schweiz</th>
        <th>Premiumverteiler</th>
        <th>Neu Vereteiler Deutschland</th>
    </tr>
    <tr>
        <td><b>Maileon</b></td>
        <td><b><?php echo \number_format($results['Maileon']['de'], 0, '.' , '.').' aktiven Kontakten'?></b></td>
        <td><b><?php echo \number_format($results['Maileon']['at'], 0, '.' , '.').' aktiven Kontakten'?><b></td>
        <td><b><?php echo \number_format($results['Maileon']['ch'], 0, '.' , '.').' aktiven Kontakten'?></b></td>
        <td><b><?php echo \number_format($results['Maileon']['p'], 0, '.' , '.').' aktiven Kontakten'?></b></td>
        <td><b><?php echo \number_format($results['Maileon']['neude'], 0, '.' , '.').' aktiven Kontakten'?></b></td>
    </tr>
    <tr>
        <td><b>Sendeffect</b></td>
        <td><b><?php echo \number_format($results['Sendeffect']['se-de'], 0, '.' , '.').' aktiven Kontakten'?></b></td>
        <td><b><?php echo \number_format($results['Sendeffect']['se-at'], 0, '.' , '.').' aktiven Kontakten'?><b></td>
        <td><b><?php echo \number_format($results['Sendeffect']['se-ch'], 0, '.' , '.').' aktiven Kontakten'?></b></td>
        <td><b><?php echo \number_format($results['Sendeffect']['se-p'], 0, '.' , '.').' aktiven Kontakten'?></b></td>
         <td><b><?php echo \number_format($results['Sendeffect']['se-neude'], 0, '.' , '.').' aktiven Kontakten'?></b></td>
    </tr>

</table>