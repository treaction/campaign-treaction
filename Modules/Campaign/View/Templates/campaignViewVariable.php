<?php
/* @var $this \Modules\Campaign\Controller\IndexController */

/* @var $customersDataArray \Packages\Core\Persistence\ArrayIterator */
/* @var $campaignEntity \Modules\Campaign\Domain\Model\CampaignEntity */
/* @var $domainDeliveryEntity \Packages\Core\Domain\Model\DomainDeliveryEntity */
/* @var $clientDeliveries \Packages\Core\Persistence\ObjectStorage */
/* @var $campaignDeliverySystemDistributorEntity \Packages\Core\Domain\Model\DeliverySystemDistributorEntity */
/* @var $deliverySystemDistributorDataArray \Packages\Core\Persistence\ArrayIterator */

/* @var $clientEntity \Packages\Core\Domain\Model\ClientEntity */
/* @var $loggedUserEntity \Packages\Core\Domain\Model\UserEntity */
/* @var $dateNow \DateTime */


$mailing = $actionRequestDataArray['mailing'];
if ($this->moduleRequestDataArray['_action'] == 'edit' 
	&& $campaignEntity->getK_id() === $campaignEntity->getNv_id()
	&& $mailing == 'NV' 
) {
	$mailing = 'HV';
}
$menge = isset($actionRequestDataArray['menge']) ? \intval($actionRequestDataArray['menge']) : 0;


$readOnly = NULL;
$disabledCampaignDate = FALSE;
$statusCampaignStartDate = FALSE;
$billingTypeSettings = NULL;
$mengeText = NULL;

$priceClass = 'toggleShow';
$performanceContentClass = $incomeContentClass = 'toggleHide';
$jsNewKampaign = 'false';

if ($mailing == 'NV') {
	if ($menge > 0) {
		$mengeText = '[' . $menge . ']';
	}
	
	$billingTypeSettings = 'disabled="disabled"'; 
}

$zielgruppeDataArray = \explode(', ', $campaignEntity->getZielgruppe());