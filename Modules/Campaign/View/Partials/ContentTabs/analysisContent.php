<?php
/* @var $campaignEntity \Modules\Campaign\Domain\Model\CampaignEntity */
?>
<div id="tab5">
	 <table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="headerColumn">Versanddaten (Menge & Performance):</td>
		</tr>
		<tr>
			<td colspan="2">
				<table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2" class="info"><?php echo $performanceInfoContent; ?></td>
					</tr>
					<tr>
						<td align="right" class="info">Versendete Menge:</td>
						<td align="left">
							<input name="campaign[versendet]" id="versendet" type="text" value="<?php echo $campaignEntity->getVersendet(); ?>" <?php echo $readOnlyPerformance; ?> />
						</td>
					</tr>
					<tr>
						<td align="right" class="info">&Ouml;ffnungen (unique):</td>
						<td align="left">
							<input name="campaign[openings]" id="openings_u" type="text" value="<?php echo $campaignEntity->getOpenings(); ?>" <?php echo $readOnlyPerformance; ?> />
						</td>
					</tr>
					<tr>
						<td align="right" class="info">&Ouml;ffnungen:</td>
						<td align="left">
							<input name="campaign[openings_all]" id="openings" type="text" value="<?php echo $campaignEntity->getOpenings_all(); ?>" <?php echo $readOnlyPerformance; ?> />
						</td>
					</tr>
					<tr>
						<td align="right" class="info">Klicks (unique):</td>
						<td align="left">
							<input name="campaign[klicks]" id="klicks_u" type="text" value="<?php echo $campaignEntity->getKlicks(); ?>" <?php echo $readOnlyPerformance; ?> />
						</td>
					</tr>
					<tr>
						<td align="right" class="info">Klicks:</td>
						<td align="left">
							<input name="campaign[klicks_all]" id="klicks" type="text" value="<?php echo $campaignEntity->getKlicks_all(); ?>" <?php echo $readOnlyPerformance; ?> />
						</td>
					</tr>
					<tr>
						<td align="right" class="info">Abmelder:</td>
						<td align="left">
                                                    <input name="campaign[abmelder]" id="abmelder" type="text" value="<?php echo $campaignEntity->getAbmelder(); ?>" <?php echo $readOnlyPerformance; ?> />
						</td>
					</tr>                                        
					<tr class="<?php echo $leadsContentClass; ?>">
						<td align="right" class="info">Leads:</td>
						<td align="left">
							<input name="campaign[leads]" id="leads" type="text" value="<?php echo $campaignEntity->getLeads(); ?>" onchange="createAndShowNewPerformanceContent('<?php echo $campaignEntity->getAbrechnungsart(); ?>');" />
						</td>
					</tr>
					<tr class="<?php echo $leadsContentClass; ?>">
						<td align="right" class="info">Leads validiert:</td>
						<td align="left">
							<input name="campaign[leads_u]" id="leads_u" type="text" value="<?php echo $campaignEntity->getLeads_u(); ?>" onchange="createAndShowNewPerformanceContent('<?php echo $campaignEntity->getAbrechnungsart(); ?>');" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class="<?php echo $incomeContentClass; ?>">
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr class="<?php echo $incomeContentClass; ?>">
			<td colspan="2" class="headerColumn">Einnahmen:</td>
		</tr>
		<tr class="<?php echo $incomeContentClass; ?>">
			<td colspan="2">
				<div id="volumeAndPerformance">&nbsp;</div>
			</td>
		</tr>
	</table>
</div>