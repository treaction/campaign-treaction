<?php
/* @var $campaignEntity \Modules\Campaign\Domain\Model\CampaignEntity */
?>
<div id="tab4">
	<table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" class="info" valign="top" colspan="2">Infomail "Neues Mailing" an:</td>
		</tr>
		<tr>
			<td align="left" colspan="2">
				<div style="height:179px; overflow:auto; background:#FFF;">
					<?php echo \nl2br($usesCheckboxItems); ?>
				</div>
				<br />Zus&auml;tzliche Info (optional):<br />
				<textarea name="actions[sendNotification][notes]" style="height:40px; width: 98%;"></textarea>
			</td>
		</tr>
	</table>
</div>