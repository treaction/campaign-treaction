<?php
/* @var $loggedUserEntity \Packages\Core\Domain\Model\UserEntity */
?>
<tr>
	<td align="right" class="info">Ansprechpartner:</td>
	<td align="left">
		<div id="ap_select" class="floatLeft">
			<select name="campaign[ap]" id="ap" onchange="changeContactPerson(this.value)">
				<option value="">-- keine Kontakte --</option>
			</select>
		</div>
		<input type="button" id="apButton" onclick="kNewContactPerson(<?php echo $loggedUserEntity->getBenutzer_id(); ?>);" value="Neu" class="floatRight" />
	</td>
</tr>

<tr>
	<td align="right" style ="color:red" class="info">Vertriebler:</td>
	<td align="left">
		<div id="vertriebler_select">
			<?php echo $salesFullname; ?>
		</div>
	</td>
</tr>