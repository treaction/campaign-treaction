<?php
namespace Modules\Campaign\Utility;

use Packages\Core\Utility\GeneralUtility;
use Packages\Core\Utility\FileUtility;

use Modules\Campaign\Utility\CampaignAndCustomerUtility;
use Modules\Campaign\Utility\CampaignAdvertisingsEntityUtility;


/**
 * Description of MailingEntityUtility
 *
 * @author Cristian.Reus
 */
final class MailingEntityUtility {
	
	/**
	 * mailingEntityMapToCampaignEntity

	 * @var array
	 */
	public static $mailingEntityMapToCampaignEntity = array(
		'title' => 'k_name',
		'mimeType' => 'mailtyp',
		'charset' => 'charset',
		'subject' => 'betreff',
		'maxRecipients' => 'vorgabe_m',
	);
	
          /**
            * mailingEntityMapToDeliveryEntity

            * @var array
            */
           public static $mailingEntityMapToDeliveryEntity = array(
               'Online_Version' => 'Online_Version',
               'Unsubscribe_link' => 'Unsubscribe_link',
               'Open_Pixel' => 'Open_Pixel',
               'Anrede' => 'Anrede',
               'Vorname' => 'Vorname',
               'Nachname' => 'Nachname',
               'Email' => 'Email',
                'Contact_formal' => 'Contact_formal',
                'Contact_unformal' => 'Contact_unformal',
                'Contact_standard' => 'Contact_standard',
           );

	
	/**
	 * createNewMailingEntityForDeliverySystem
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @param \Packages\Core\Domain\Model\DeliverySystemDistributorEntity $deliverySystemDistributorEntity
	 * @param string $destinationDir
	 * @return \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity
	 */
	public static function createNewMailingEntityForDeliverySystem(\Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity, \Packages\Core\Domain\Model\DeliverySystemDistributorEntity $deliverySystemDistributorEntity, $destinationDir = '',$domain) {
		$mailingEntity = self::createEmptyMailingEntity();
		$mailingEntity->_memorizeCleanState();
                 
		$mailingEntity->setTitle($campaignEntity->getContactPersonEntity()->getCustomerEntity()->getFirma() . ', ' . $campaignEntity->getK_name() . ' [' . $campaignEntity->getDatum()->format('d.m') . ']');
		$mailingEntity->setMimeType(CampaignAndCustomerUtility::$mimeTypeDataArray[$campaignEntity->getMailtyp()]);
		$mailingEntity->setRecipientListIds(array($deliverySystemDistributorEntity->getDistributor_id()));
		$mailingEntity->setFromEmail($campaignEntity->getSender_email());
		$mailingEntity->setFromName($campaignEntity->getAbsendername());
		$mailingEntity->setCharset($campaignEntity->getCharset());
		$mailingEntity->setSubject($campaignEntity->getBetreff());
		$mailingEntity->setMaxRecipients($campaignEntity->getVorgabe_m());
		$mailingEntity->setScheduleDate($campaignEntity->getDatum());
                $mailingEntity->setDomain($domain);

		self::processMailingContent(
			$campaignEntity,
			$destinationDir,
			$mailingEntity
		);
		
		return $mailingEntity;
	}
        
    /**
     * createNewMailingEntityForNewDeliverySystem
     * 
     * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
     * @param \Packages\Core\Domain\Model\DeliverySystemDistributorEntity $deliverySystemDistributorEntity
     * @param string $HvDestinationDir
         * @param string $NvDestinationDir
         * @param array $newEspEntityArray
         * @param array $oldEspEntityArray
     * @return \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity
     */
    public static function createNewMailingEntityForNewDeliverySystem(\Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity, \Packages\Core\Domain\Model\DeliverySystemDistributorEntity $deliverySystemDistributorEntity,$HvDestinationDir = '', $NvDestinationDir = '',$domain, $newEspEntityArray, $oldEspEntityArray) {
        $mailingEntity = self::createEmptyMailingEntity();
        $mailingEntity->_memorizeCleanState();
                 
        $mailingEntity->setTitle($campaignEntity->getContactPersonEntity()->getCustomerEntity()->getFirma() . ', ' . $campaignEntity->getK_name() . ' [' . $campaignEntity->getDatum()->format('d.m') . ']');
        $mailingEntity->setMimeType(CampaignAndCustomerUtility::$mimeTypeDataArray[$campaignEntity->getMailtyp()]);
        $mailingEntity->setRecipientListIds(array($deliverySystemDistributorEntity->getDistributor_id()));
        $mailingEntity->setFromEmail($campaignEntity->getSender_email());
        $mailingEntity->setFromName($campaignEntity->getAbsendername());
        $mailingEntity->setCharset($campaignEntity->getCharset());
        $mailingEntity->setSubject($campaignEntity->getBetreff());
        $mailingEntity->setMaxRecipients($campaignEntity->getVorgabe_m());
        $mailingEntity->setScheduleDate($campaignEntity->getDatum());
                $mailingEntity->setDomain($domain);

        self::processMailingNewContent(
            $campaignEntity,
            $HvDestinationDir,
                        $NvDestinationDir,
            $mailingEntity,
                        $newEspEntityArray,
                        $oldEspEntityArray
        );
        
        return $mailingEntity;
    }

	
	/**
	 * updateMailingEntityForDeliverySystem
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @param \Packages\Core\Domain\Model\DeliverySystemDistributorEntity $deliverySystemDistributorEntity
	 * @param string $destinationDir
	 * @param boolean $updateCampaignAdvertisingsContent
	 * @return \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity
	 */
	public static function updateMailingEntityForDeliverySystem(\Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity, \Packages\Core\Domain\Model\DeliverySystemDistributorEntity $deliverySystemDistributorEntity, $destinationDir, $updateCampaignAdvertisingsContent = FALSE, $domain) {
		$mailingEntity = self::createEmptyMailingEntity();
		$mailingEntity->_setProperty(
			$mailingEntity->getPrimaryFieldName(),
			$campaignEntity->getMail_id()
		);
		$mailingEntity->_memorizeCleanState();
		
		foreach (self::$mailingEntityMapToCampaignEntity as $key => $item) {
			if ($campaignEntity->_isDirty($item)) {
				switch ($key) {
					case 'mimeType':
						$value = CampaignAndCustomerUtility::$mimeTypeDataArray[$campaignEntity->getMailtyp()];
						break;
					
					case 'title':
						if ($campaignEntity->getK_id() === $campaignEntity->getNv_id()) {
							// nur HV
							$value = $campaignEntity->getContactPersonEntity()->getCustomerEntity()->getFirma() . ', ' . $campaignEntity->getK_name() . ' [' . $campaignEntity->getDatum()->format('d.m') . ']';
						} else {
							$value = $campaignEntity->getContactPersonEntity()->getCustomerEntity()->getFirma() . ', ' . $campaignEntity->getK_name();
						}
						break;
					
					default:
						$value = $campaignEntity->_getProperty($item);
						break;
				}
				
				$mailingEntity->_setProperty(
					$key,
					$value
				);
			}
		}
		
		if ($campaignEntity->_isDirty('sender_email') 
			|| $campaignEntity->_isDirty('absendername')
		) {
			$mailingEntity->setFromEmail($campaignEntity->getSender_email());
			$mailingEntity->setFromName($campaignEntity->getAbsendername());
		}
		
                if ($campaignEntity->_isDirty('dsd_id')) {
			$mailingEntity->setRecipientListIds(array($deliverySystemDistributorEntity->getDistributor_id()));
		}
                
		if ($campaignEntity->_isDirty('dasp_id')) {
			$mailingEntity->setDomain($domain);
		}
		
		if ((boolean) $updateCampaignAdvertisingsContent === TRUE) {
			self::processMailingContent(
				$campaignEntity,
				$destinationDir,
				$mailingEntity
			);
		}
		
		if ($campaignEntity->_isDirty('datum')) {
			$mailingEntity->setScheduleDate($campaignEntity->getDatum());
		}
		
		return $mailingEntity;
	}
	
	
	/**
	 * createEmptyMailingEntity

	 * @return \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity
	 */
	protected static function createEmptyMailingEntity() {
		return GeneralUtility::makeInstance('\\Packages\\Api\\DeliverySystem\\Domain\\Entity\\MailingEntity');
	}
	
	/**
	 * processMailingContent
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @param string $destinationDir
	 * @param \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity $mailingEntity
	 * @return void
	 */
	protected static function processMailingContent(\Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity, $destinationDir, \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity &$mailingEntity) {
		if ($campaignEntity->getCampaignAdvertisings()->count() > 0) {
			$campaignAdvertisings = $campaignEntity->getCampaignAdvertisings();
			
			switch ($mailingEntity->getMimeType()) {
				case 'text/html':
					self::getAndSetHtmlMailingContent(
						$destinationDir,
						$campaignAdvertisings,
						$mailingEntity
					);
					break;
				
				case 'text/plain':
					self::getAndSetTxtMailingContent(
						$destinationDir,
						$campaignAdvertisings,
						$mailingEntity
					);
					break;
				
				default:
					// multipart
					
					// html
					self::getAndSetHtmlMailingContent(
						$destinationDir,
						$campaignAdvertisings,
						$mailingEntity
					);
					
					// txt
					self::getAndSetTxtMailingContent(
						$destinationDir,
						$campaignAdvertisings,
						$mailingEntity
					);
					break;
			}
		}
	}
	
	/**
	 * getAndSetTxtMailingContent
	 * 
	 * @param string $destinationDir
	 * @param \Packages\Core\Persistence\ObjectStorage $campaignAdvertisings
	 * @param \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity $mailingEntity
	 * @return void
	 */
	protected static function getAndSetTxtMailingContent($destinationDir, \Packages\Core\Persistence\ObjectStorage $campaignAdvertisings, \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity &$mailingEntity) {
		$campaignAdvertisingEntity = CampaignAdvertisingsEntityUtility::getLatestCampaignAdvertisingByMimeType(
			$campaignAdvertisings,
			\Modules\Campaign\Utility\CampaignAndCustomerUtility::$mailtypDataArray['t']
		);
		if ($campaignAdvertisingEntity instanceof \Modules\Campaign\Domain\Model\CampaignAdvertisingEntity) {
			$fileContent = FileUtility::readFileIntoString($destinationDir . $campaignAdvertisingEntity->getFile());
			
			$mailingEntity->setTxtContent($fileContent);
			unset($campaignAdvertisingEntity, $fileContent);
		}
	}
	
    /**
     * processMailingNewContent
     * 
     * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
     * @param string $HvDestinationDir
         * @param string $NvDestinationDir
     * @param \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity $mailingEntity
     * @return void
     */
    public static function processMailingNewContent(\Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity, $HvDestinationDir, $NvDestinationDir, \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity &$mailingEntity, $newEspEntityArray, $oldEspEntityArray) {
        if ($campaignEntity->getCampaignAdvertisings()->count() > 0) {
            $campaignAdvertisings = $campaignEntity->getCampaignAdvertisings();
            
            switch ($mailingEntity->getMimeType()) {
                case 'text/html':
                    self::getAndSetNewHtmlMailingContent(
                        $HvDestinationDir,
                                                $NvDestinationDir,
                        $campaignAdvertisings,
                        $mailingEntity,
                                                $newEspEntityArray,
                                                $oldEspEntityArray
                    );
                    break;
                
                case 'text/plain':
                    self::getAndSetNewTxtMailingContent(
                        $HvDestinationDir,
                                                $NvDestinationDir,
                        $campaignAdvertisings,
                        $mailingEntity,
                                                $newEspEntityArray,
                                                $oldEspEntityArray
                    );
                    break;
                
                default:
                    // multipart
                    
                    // html
                    self::getAndSetNewHtmlMailingContent(
                        $HvDestinationDir,
                                                $NvDestinationDir,
                        $campaignAdvertisings,
                        $mailingEntity,
                                                $newEspEntityArray,
                                                $oldEspEntityArray
                    );
                    
                    // txt
                    self::getAndSetNewTxtMailingContent(
                        $HvDestinationDir,
                                                $NvDestinationDir,
                        $campaignAdvertisings,
                        $mailingEntity,
                                                $newEspEntityArray,
                                                $oldEspEntityArray
                    );
                    break;
            }
        }
    }

	/**
	 * getAndSetHtmlMailingContent
	 * 
	 * @param string $destinationDir
	 * @param \Packages\Core\Persistence\ObjectStorage $campaignAdvertisings
	 * @param \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity $mailingEntity
	 * @return void
	 */
	protected static function getAndSetHtmlMailingContent($destinationDir, \Packages\Core\Persistence\ObjectStorage $campaignAdvertisings, \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity &$mailingEntity) {
		$campaignAdvertisingEntity = CampaignAdvertisingsEntityUtility::getLatestCampaignAdvertisingByMimeType(
			$campaignAdvertisings,
			\Modules\Campaign\Utility\CampaignAndCustomerUtility::$mailtypDataArray['h']
		);
		if ($campaignAdvertisingEntity instanceof \Modules\Campaign\Domain\Model\CampaignAdvertisingEntity) {
			$fileContent = FileUtility::readFileIntoString($destinationDir . $campaignAdvertisingEntity->getFile());
			
			$mailingEntity->setHtmlContent($fileContent);
			unset($campaignAdvertisingEntity, $fileContent);
		}
	}
        
          /**
	 * getAndSetNewTxtMailingContent
	 * 
	 * @param string $HvDestinationDir
         * @param string $NvDestinationDir
	 * @param \Packages\Core\Persistence\ObjectStorage $campaignAdvertisings
	 * @param \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity $mailingEntity
         * @param array $newEspEntityArray
         * @param array $oldEspEntityArray
	 * @return void
	 */
	protected static function getAndSetNewTxtMailingContent($HvDestinationDir, $NvDestinationDir, \Packages\Core\Persistence\ObjectStorage $campaignAdvertisings, \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity &$mailingEntity, $newEspEntityArray, $oldEspEntityArray) {
		$campaignAdvertisingEntity = CampaignAdvertisingsEntityUtility::getLatestCampaignAdvertisingByMimeType(
			$campaignAdvertisings,
			\Modules\Campaign\Utility\CampaignAndCustomerUtility::$mailtypDataArray['t']
		);
		if ($campaignAdvertisingEntity instanceof \Modules\Campaign\Domain\Model\CampaignAdvertisingEntity) {
			$fileContent = FileUtility::readFileIntoString($HvDestinationDir . $campaignAdvertisingEntity->getFile());
			
                           $mailingEntityMapToDeliveryArray = MailingEntityUtility::$mailingEntityMapToDeliveryEntity;                       
                                foreach ($mailingEntityMapToDeliveryArray as $paramterToMapping) {                 
                                   $fileContent = str_replace(htmlspecialchars_decode($oldEspEntityArray[(string)$paramterToMapping]), htmlspecialchars_decode($newEspEntityArray[(string)$paramterToMapping]), $fileContent);  
                                     }
                        $mailingEntity->setHtmlContent($fileContent);
                        FileUtility::putFileContent($NvDestinationDir . $campaignAdvertisingEntity->getFile(), $fileContent);
                        
			$mailingEntity->setTxtContent($fileContent);
			unset($campaignAdvertisingEntity, $fileContent);
		}
	}
	
	/**
	 * getAndSetNewHtmlMailingContent
	 * 
	 * @param string $HvDestinationDir
         * @param string $NvDestinationDir
	 * @param \Packages\Core\Persistence\ObjectStorage $campaignAdvertisings
	 * @param \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity $mailingEntity
         * @param array $newEspEntityArray
         * @param array $oldEspEntityArray
	 * @return void
	 */
	public static function getAndSetNewHtmlMailingContent($HvDestinationDir, $NvDestinationDir, \Packages\Core\Persistence\ObjectStorage $campaignAdvertisings, \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity &$mailingEntity, $newEspEntityArray, $oldEspEntityArray) {
		$campaignAdvertisingEntity = CampaignAdvertisingsEntityUtility::getLatestCampaignAdvertisingByMimeType(
			$campaignAdvertisings,
			\Modules\Campaign\Utility\CampaignAndCustomerUtility::$mailtypDataArray['h']
		);
		if ($campaignAdvertisingEntity instanceof \Modules\Campaign\Domain\Model\CampaignAdvertisingEntity) {
			$fileContent = FileUtility::readFileIntoString($HvDestinationDir . $campaignAdvertisingEntity->getFile());
                        
                        $mailingEntityMapToDeliveryArray = MailingEntityUtility::$mailingEntityMapToDeliveryEntity; 
                       
                                foreach ($mailingEntityMapToDeliveryArray as $paramterToMapping) {                 
                                  if(strlen($newEspEntityArray['Open_Pixel']) > 5 
                                          &&
                                          strlen($oldEspEntityArray['Open_Pixel']) < 2
                                          ){
                                      $oldEspEntityArray['Open_Pixel'] = '</head>';
                                      $newEspEntityArray['Open_Pixel'] = htmlspecialchars_decode($newEspEntityArray['Open_Pixel']).'</head>';  
                                     }
                                       $fileContent = str_replace(htmlspecialchars_decode($oldEspEntityArray[(string)$paramterToMapping]), htmlspecialchars_decode($newEspEntityArray[(string)$paramterToMapping]), $fileContent);  
                                     } 
                                    
			$mailingEntity->setHtmlContent($fileContent);

                        FileUtility::putFileContent($NvDestinationDir . $campaignAdvertisingEntity->getFile(), $fileContent);

			unset($campaignAdvertisingEntity, $fileContent);
		}       
	}
        
}
