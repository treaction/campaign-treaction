<?php
namespace Modules\Campaign\Controller;

use Modules\Campaign\Controller\AbstractController;
use Packages\Core\Utility\GeneralUtility;
use Packages\Core\Utility\FileUtility;
use Packages\Core\Utility\Format\OthersUtility;


/**
 * Description of AjaxController
 *
 * @author Cristian.Reus
 */
class AjaxController extends AbstractController {
	
	/**
	 * compressAndDownloadCampaignFilesAction
	 * 
	 * @param array $actionRequestDataArray
	 * @throws \LengthException
	 * @throws \InvalidArgumentException
	 * @throws \DomainException
	 * @return mixed
	 */
	public function compressAndDownloadCampaignFilesAction(array $actionRequestDataArray) {
		$campaignId = isset($actionRequestDataArray['campaign']['k_id']) ? \intval($actionRequestDataArray['campaign']['k_id']) : 0;
		if ($campaignId <= 0) {
			throw new \LengthException('invalid campaign id', 1453883694);
		}
		
		if (!isset($actionRequestDataArray['campaignAdvertising']['download'])) {
			throw new \InvalidArgumentException('no download data selected', 1453884074);
		}
		
		$campaignEntity = $this->repository->findById($campaignId);
		if (!($campaignEntity instanceof \Modules\Campaign\Domain\Model\CampaignEntity)) {
			throw new \DomainException('invalid campaignEntity', 1453883724);
		}
		
		$downloadDir = \DIR_Downloads . $this->clientEntity->getAbkz() . \DIRECTORY_SEPARATOR;
		FileUtility::createDirectory($downloadDir);
		
		$destinationDir = $this->targetDir . $campaignEntity->getK_id() . \DIRECTORY_SEPARATOR;
		
		$filesToCompressDataArray = array();
		foreach ($actionRequestDataArray['campaignAdvertising']['download'] as $filename) {
			$filesToCompressDataArray[$filename] = $destinationDir . $filename;
		}
		
		$archivename = $campaignEntity->getDatum()->format('d-m') . '-' .  OthersUtility::cleanupStringForDownload($campaignEntity->getK_name()) . '.zip';
		$zipArchive = GeneralUtility::makeInstance('\\Packages\\Core\\Archives\\Zip');
		/* @var $zipArchive \Packages\Core\Archives\Zip */
		$zipArchive
			->createArchive($downloadDir . $archivename)
			->addFiles($filesToCompressDataArray)
			->close()
		;
		
		FileUtility::createDownloadHeader(
			$downloadDir . $archivename,
			$archivename,
			TRUE
		);
	}

}