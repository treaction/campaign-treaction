<?php
namespace Modules\Campaign\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * Description of CampaignAdvertisingEntity
 *
 * @author Cristian.Reus
 */
class CampaignAdvertisingEntity extends AbstractEntity {
	
	/**
	 * id
	 * 
	 * @var integer
	 */
	protected $id = NULL;
	
	/**
	 * campaign_id
	 * 
	 * @var integer
	 */
	protected $campaign_id;
	
	/**
	 * mime_type
	 * 
	 * @var string
	 */
	protected $mime_type;
	
	/**
	 * file
	 * 
	 * @var string 
	 */
	protected $file;
	
	/**
	 * crdate
	 * 
	 * @var \DateTime
	 */
	protected $crdate;
	
	/**
	 * user_id
	 * 
	 * @var integer
	 */
	protected $user_id;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter
     *
     *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}

	public function getCampaign_id() {
		return (int) $this->campaign_id;
	}
	public function setCampaign_id($campaign_id) {
		$this->campaign_id = \intval($campaign_id);
	}
	
	public function getMime_type() {
		return $this->mime_type;
	}
	public function setMime_type($mime_type) {
		$this->mime_type = $mime_type;
	}

	public function getFile() {
		return $this->file;
	}
	public function setFile($file) {
		$this->file = $file;
	}

	public function getCrdate() {
		if (!($this->crdate instanceof \DateTime)) {
			$this->setCrdate($this->crdate);
		}
		
		return $this->crdate;
	}
	public function setCrdate($crdate = NULL) {
		$this->createDateTimeFromValueForField(
			'crdate',
			$crdate
		);
	}

	public function getUser_id() {
		return (int) $this->user_id;
	}
	public function setUser_id($user_id) {
		$this->user_id = \intval($user_id);
	}

}
