<?php
namespace Modules\Campaign\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * Description of ClientEntity
 *
 * @author Cristian.Reus
 */
class CampaignEntity extends AbstractEntity {
	
	/**
	 * k_id
	 * 
	 * @var integer
	 */
	protected $k_id = NULL;
	
	/**
	 * mail_id
	 * 
	 * @var string
	 */
	protected $mail_id;
	
	/**
	 * nv_id
	 * 
	 * @var integer
	 */
	protected $nv_id;
	
	/**
	 * agentur_id
	 * 
	 * @var integer
	 * @deprecated
	 */
	protected $agentur_id;
	
	/**
	 * ap_id
	 * 
	 * @var integer
	 */
	protected $ap_id;
	
	/**
	 * datum
	 * 
	 * @var \DateTime
	 */
	protected $datum;
	
	/**
	 * status
	 * 
	 * @var integer
	 */
	protected $status;
	
	/**
	 * bearbeiter
	 * 
	 * @var string
	 */
	protected $bearbeiter;
	
	/**
	 * k_name
	 * 
	 * @var string
	 */
	protected $k_name;
	
	/**
	 * betreff
	 * 
	 * @var string
	 */
	protected $betreff;
	
	/**
	 * absendername
	 * 
	 * @var string
	 */
	protected $absendername;
	
	/**
	 * gebucht
	 * 
	 * @var integer
	 */
	protected $gebucht;
	
	/**
	 * versendet
	 * 
	 * @var integer
	 */
	protected $versendet;
	
	/**
	 * gestartet
	 * 
	 * @var \DateTime
	 */
	protected $gestartet;
	
	/**
	 * beendet
	 * 
	 * @var \DateTime
	 */
	protected $beendet;
	
	/**
	 * openings
	 * 
	 * @var integer
	 */
	protected $openings;
	
	/**
	 * openings_all
	 * 
	 * @var integer
	 */
	protected $openings_all;
	
	/**
	 * vorgabe_o
	 * 
	 * @var float
	 */
	protected $vorgabe_o;
	
	/**
	 * klicks
	 * 
	 * @var integer
	 */
	protected $klicks;
	
	/**
	 * klicks_all
	 * 
	 * @var integer
	 */
	protected $klicks_all;
	
	/**
	 * sbounces
	 * 
	 * @var integer
	 */
	protected $sbounces;
	
	/**
	 * hbounces
	 * 
	 * @var integer
	 */
	protected $hbounces;
	
	/**
	 * vorgabe_k
	 * 
	 * @var float
	 */
	protected $vorgabe_k;
	
	/**
	 * zielgruppe
	 * 
	 * @var string
	 */
	protected $zielgruppe;
	
	/**
	 * blacklist
	 * 
	 * @var string
	 * TODO: auf boolean umstellen
	 */
	protected $blacklist;
	
	/**
	 * mailtyp
	 * 
	 * @var string
	 * TODO: auf string/enum umstellen
	 */
	protected $mailtyp;
	
	/**
	 * notiz
	 * 
	 * @var string
	 */
	protected $notiz;
	
	/**
	 * abrechnungsart
	 * 
	 * @var string
	 */
	protected $abrechnungsart;
	
	/**
	 * preis
	 * 
	 * @var float
	 */
	protected $preis;
	
        /**
	 * hybrid_preis
	 *
	 * @var float
	 */
        protected $hybrid_preis;
        
        /**
	 * hybrid_preis2
	 *
	 * @var float
	 */
        protected $hybrid_preis2;
	/**
	 * preis_gesamt
	 * 
	 * @var float
	 */
	protected $preis_gesamt;
	
	/**
	 * leads
	 * 
	 * @var integer
	 */
	protected $leads;
	
	/**
	 * edit_look
	 * 
	 * @var string
	 */
	protected $edit_lock;
	
	/**
	 * erstreporting
	 * 
	 * @var \DateTime
	 */
	protected $erstreporting = NULL;
	
	/**
	 * endreporting
	 * 
	 * @var \DateTime
	 */
	protected $endreporting = NULL;
	
	/**
	 * preis2
	 * 
	 * @var float
	 */
	protected $preis2;
	
	/**
	 * vorgabe_m
	 * 
	 * @var integer
	 */
	protected $vorgabe_m;
	
	/**
	 * hash
	 * 
	 * @var string
	 */
	protected $hash;
	
	/**
	 * klickrate_extern
	 * 
	 * @var integer
	 */
	protected $klickrate_extern;
	
	/**
	 * openingrate_extern
	 * 
	 * @var integer
	 */
	protected $openingrate_extern;
	
	/**
	 * leads_u
	 * 
	 * @var integer
	 */
	protected $leads_u;
	
	/**
	 * abmelder
	 * 
	 * @var integer
	 */
	protected $abmelder;
	
	/**
	 * preis_gesamt_verify
	 * 
	 * @var integer
	 */
	protected $preis_gesamt_verify;
	
	/**
	 * vertriebler_id
	 * 
	 * @var integer
	 */
	protected $vertriebler_id;
	
	/**
	 * last_update
	 * 
	 * @var \DateTime
	 */
	protected $last_update = NULL;
	
	/**
	 * dsd_id
	 * 
	 * @var integer
	 */
	protected $dsd_id;
	
	/**
	 * unit_price
	 * 
	 * @var float
	 */
	protected $unit_price;
	
	/**
	 * use_campaign_start_date
	 * 
	 * @var boolean
	 */
	protected $use_campaign_start_date = FALSE;
	
	/**
	 * campaign_click_profiles_id
	 * 
	 * @var integer
	 */
	protected $campaign_click_profiles_id = 0;
	
	/**
	 * selection_click_profiles_id
	 * 
	 * @var integer
	 */
	protected $selection_click_profiles_id = 0;
	
	/**
	 * premium_campaign
	 * 
	 * @var boolean
	 */
	protected $premium_campaign = FALSE;
	
	/**
	 * advertising_materials
	 * 
	 * @var integer
	 */
	protected $advertising_materials;
	
	/**
	 * sender_email
	 * 
	 * @var string
	 */
	protected $sender_email;
	
	/**
	 * charset
	 * 
	 * @var string
	 */
	protected $charset;
	
	/**
	 * campaignAdvertisings
	 * 
	 * @var \Packages\Core\Persistence\ObjectStorage<\Modules\Campaign\Domain\Model\CampaignAdvertisingEntity> 
	 * @relations
	 */
	protected $campaignAdvertisings;
	
	/**
	 * contactPersonEntity
	 * 
	 * @var \Modules\Customer\Domain\Model\ContactPersonEntity
	 * @relations 
	 */
	protected $contactPersonEntity = NULL;
	
	/**
	 * broking_volume
	 * 
	 * @var int
	 */
	protected $broking_volume;
	
	/**
	 * broking_price
	 * 
	 * @var float
	 */
	protected $broking_price;
	
	/**
	 * advertisings_comment
	 * 
	 * @var string
	 */
	protected $advertisings_comment;




	/**
	 * versandsystem
	 * 
	 * @var string
	 * @deprecated -> use dsdId
	 */
	protected $versandsystem;
	
	
	
	/**
	 * tkp
	 * 
	 * @var string
	 * @deprecated
	 */
	protected $tkp;
	
	/**
	 * report
	 * 
	 * @var string
	 * @deprecated
	 */
	protected $report;
	
	/**
	 * rechnung
	 * 
	 * @var string
	 * @deprecated
	 */
	protected $rechnung;
	
	/**
	 * agentur
	 * 
	 * @var string
	 * @deprecated
	 */
	protected $agentur;
	
	/**
	 * ap
	 * 
	 * @var string
	 * @deprecated
	 */
	protected $ap;
	
	/**
	 * crm_order_id
	 * 
	 * @var string
	 * @deprecated
	 */
	protected $crm_order_id;
	
	/**
	 * crm_guid
	 * 
	 * @var string
	 * @deprecated
	 */
	protected $crm_guid;
	
	/**
	 * cs_id
	 * 
	 * @var integer
	 * @deprecated
	 */
	protected $cs_id;
	
	/**
	 * anfrage
	 * 
	 * @var string
	 * @deprecated
	 */
	protected $anfrage;
	
		/**
	 * reagierer
	 *
	 * @var string
	 */
	protected $reagierer;
	
	/**
	 * alter_von
	 *
	 * @var string
	 */
	protected $alter_von;
	
	/**
	 * alter_bis
	 *
	 * @var string
	 */
	protected $alter_bis;
	
	/**
	 * plz_bis
	 *
	 * @var string
	 */
	protected $plz_von;
	
	/**
	 * plz_von
	 *
	 * @var string
	 */
	protected $plz_bis;
        
	/**
	 * externaljobid
	 *
	 * @var string
	 */
        protected $externaljobid;
        /**
	 * zg_kommentar
	 *
	 * @var string
	 */
	protected $zg_kommentar;
        /**
	 * dasp_id
	 *
	 * @var integer
	 */
	protected $dasp_id;
        
        
	
	public function __construct($primaryFieldName = 'k_id') {
		parent::__construct($primaryFieldName);
		
		$this->campaignAdvertisings = new \Packages\Core\Persistence\ObjectStorage();
	}
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter
     *
     *******************************************************************************************/
	public function getK_id() {
		return (int) $this->k_id;
	}
	public function setK_id($kId) {
		$this->k_id = \intval($kId);
	}

	public function getMail_id() {
		return $this->mail_id;
	}
	public function setMail_id($mailId) {
		$this->mail_id = $mailId;
	}

	public function getNv_id() {
		return (int) $this->nv_id;
	}
	public function setNv_id($nvId) {
		$this->nv_id = \intval($nvId);
	}

	public function getAgentur_id() {
		return (int) $this->agentur_id;
	}
	public function setAgentur_id($agenturId) {
		$this->agentur_id = \intval($agenturId);
	}

	public function getAp_id() {
		return $this->ap_id;
	}
	public function setAp_id($apId) {
		$this->ap_id = \intval($apId);
	}

	public function getDatum() {
		if (!($this->datum instanceof \DateTime)) {
			$this->setDatum($this->datum);
		}
		
		return $this->datum;
	}
	public function setDatum($datum = NULL) {
		$this->createDateTimeFromValueForField(
			'datum',
			$datum
		);
	}

	public function getStatus() {
		return (int) $this->status;
	}
	public function setStatus($status) {
		$this->status = \intval($status);
	}

	public function getBearbeiter() {
		return $this->bearbeiter;
	}
	public function setBearbeiter($bearbeiter) {
		$this->bearbeiter = $bearbeiter;
	}

	public function getK_name() {
		return $this->k_name;
	}
	public function setK_name($kName) {
		$this->k_name = $kName;
	}

	public function getBetreff() {
		return $this->betreff;
	}
	public function setBetreff($betreff) {
		$this->betreff = $betreff;
	}

	public function getAbsendername() {
		return $this->absendername;
	}
	public function setAbsendername($absendername) {
		$this->absendername = $absendername;
	}

	public function getGebucht() {
		return (int) $this->gebucht;
	}
	public function setGebucht($gebucht) {
		$this->gebucht = \intval($gebucht);
	}

	public function getVersendet() {
		return (int) $this->versendet;
	}
	public function setVersendet($versendet) {
		$this->versendet = \intval($versendet);
	}

	public function getGestartet() {
		if (!($this->gestartet instanceof \DateTime)) {
			$this->setGestartet($this->gestartet);
		}
		
		return $this->gestartet;
	}
	public function setGestartet($gestartet = NULL) {
		$this->createDateTimeFromValueForField(
			'gestartet',
			$gestartet
		);
	}

	public function getBeendet() {
		if (!($this->beendet instanceof \DateTime)) {
			$this->setBeendet($this->beendet);
		}
		
		return $this->beendet;
	}
	public function setBeendet($beendet = NULL) {
		$this->createDateTimeFromValueForField(
			'beendet',
			$beendet
		);
	}

	public function getOpenings() {
		return (int) $this->openings;
	}
	public function setOpenings($openings) {
		$this->openings = \intval($openings);
	}

	public function getOpenings_all() {
		return (int) $this->openings_all;
	}
	public function setOpenings_all($openingsAll) {
		$this->openings_all = \intval($openingsAll);
	}

	public function getVorgabe_o() {
		return (float) $this->vorgabe_o;
	}
	public function setVorgabe_o($vorgabeO) {
		$this->vorgabe_o = \floatval($vorgabeO);
	}

	public function getKlicks() {
		return (int) $this->klicks;
	}
	public function setKlicks($klicks) {
		$this->klicks = \intval($klicks);
	}

	public function getKlicks_all() {
		return (int) $this->klicks_all;
	}
	public function setKlicks_all($klicksAll) {
		$this->klicks_all = \intval($klicksAll);
	}

	public function getSbounces() {
		return (int) $this->sbounces;
	}
	public function setSbounces($sbounces) {
		$this->sbounces = \intval($sbounces);
	}

	public function getHbounces() {
		return (int) $this->hbounces;
	}
	public function setHbounces($hbounces) {
		$this->hbounces = \intval($hbounces);
	}

	public function getVorgabe_k() {
		return (float) $this->vorgabe_k;
	}
	public function setVorgabe_k($vorgabe_k) {
		$this->vorgabe_k = \floatval($vorgabe_k);
	}

	public function getZielgruppe() {
		return $this->zielgruppe;
	}
	public function setZielgruppe($zielgruppe) {
		$this->zielgruppe = $zielgruppe;
	}

	public function getBlacklist() {
		return $this->blacklist;
	}
	public function setBlacklist($blacklist) {
            if(empty($blacklist)){
                $this->blacklist = ' ';
            }else{
                $this->blacklist = $blacklist;
            }
		
	}

	public function getMailtyp() {
		return $this->mailtyp;
	}
	public function setMailtyp($mailtyp) {
		$this->mailtyp = $mailtyp;
	}

	public function getNotiz() {
		return $this->notiz;
	}
	public function setNotiz($notiz) {
		$this->notiz = $notiz;
	}

	public function getAbrechnungsart() {
		return $this->abrechnungsart;
	}
	public function setAbrechnungsart($abrechnungsart) {
		$this->abrechnungsart = $abrechnungsart;
	}

	public function getPreis() {
		return (float) $this->preis;
	}
	public function setPreis($preis) {
		$this->preis = \floatval($preis);
	}

	public function getPreis_gesamt() {
		return (float) $this->preis_gesamt;
	}
	public function setPreis_gesamt($preisGesamt) {
		$this->preis_gesamt = \floatval($preisGesamt);
	}

	public function getLeads() {
		return (int) $this->leads;
	}
	public function setLeads($leads) {
		$this->leads = \intval($leads);
	}

	public function getEdit_lock() {
		return $this->edit_lock;
	}
	public function setEdit_lock($editLock) {
		$this->edit_lock = $editLock;
	}

	public function getErstreporting() {
		if (!($this->erstreporting instanceof \DateTime)) {
			$this->setErstreporting($this->erstreporting);
		}
		
		return $this->erstreporting;
	}
	public function setErstreporting($erstreporting = NULL) {
		$this->createDateTimeFromValueForField(
			'erstreporting',
			$erstreporting
		);
	}

	public function getEndreporting() {
		if (!($this->endreporting instanceof \DateTime)) {
			$this->setEndreporting($this->endreporting);
		}
		
		return $this->endreporting;
	}
	public function setEndreporting($endreporting = NULL) {
		$this->createDateTimeFromValueForField(
			'endreporting',
			$endreporting
		);
	}

	public function getPreis2() {
		return (float) $this->preis2;
	}
	public function setPreis2($preis2) {
		$this->preis2 = \floatval($preis2);
	}

	public function getVorgabe_m() {
		return (int) $this->vorgabe_m;
	}
	public function setVorgabe_m($vorgabeM) {
		$this->vorgabe_m = \intval($vorgabeM);
	}

	public function getHash() {
		return $this->hash;
	}
	public function setHash($hash) {
		$this->hash = $hash;
	}

	public function getKlickrate_extern() {
		return (int) $this->klickrate_extern;
	}
	public function setKlickrate_extern($klickrateExtern) {
		$this->klickrate_extern = \intval($klickrateExtern);
	}

	public function getOpeningrate_extern() {
		return (int) $this->openingrate_extern;
	}
	public function setOpeningrate_extern($openingrateExtern) {
		$this->openingrate_extern = \intval($openingrateExtern);
	}

	public function getLeads_u() {
		return (int) $this->leads_u;
	}
	public function setLeads_u($leadsU) {
		$this->leads_u = \intval($leadsU);
	}

	public function getAbmelder() {
		return (int) $this->abmelder;
	}
	public function setAbmelder($abmelder) {
		$this->abmelder = \intval($abmelder);
	}

	public function getPreis_gesamt_verify() {
		return (int) $this->preis_gesamt_verify;
	}
	public function setPreis_gesamt_verify($preisGesamtVerify) {
		$this->preis_gesamt_verify = \intval($preisGesamtVerify);
	}

	public function getVertriebler_id() {
		return (int) $this->vertriebler_id;
	}
	public function setVertriebler_id($vertrieblerId) {
		$this->vertriebler_id = \intval($vertrieblerId);
	}

	public function getLast_update() {
		if (!($this->last_update instanceof \DateTime)) {
			$this->setLast_update($this->last_update);
		}
		
		return $this->last_update;
	}
	public function setLast_update($lastUpdate = NULL) {
		$this->createDateTimeFromValueForField(
			'last_update',
			$lastUpdate
		);
	}

	public function getDsd_id() {
		return (int) $this->dsd_id;
	}
	public function setDsd_id($dsdId) {
		$this->dsd_id = \intval($dsdId);
	}

	public function getUnit_price() {
		return (float) $this->unit_price;
	}
	public function setUnit_price($unitPrice) {
		$this->unit_price = \floatval($unitPrice);
	}
	
	public function getUse_campaign_start_date() {
		return (boolean) $this->use_campaign_start_date;
	}
	public function setUse_campaign_start_date($useCampaignStartDate) {
		if ((int) $useCampaignStartDate === 1) {
			$useCampaignStartDate = TRUE;
		} else {
			$useCampaignStartDate = FALSE;
		}
		
		$this->use_campaign_start_date = $useCampaignStartDate;
	}
	
	public function getCampaign_click_profiles_id() {
		return (int) $this->campaign_click_profiles_id;
	}
	public function setCampaign_click_profiles_id($campaignClickProfilesId) {
		$this->campaign_click_profiles_id = \intval($campaignClickProfilesId);
	}

	public function getSelection_click_profiles_id() {
		return (int) $this->selection_click_profiles_id;
	}
	public function setSelection_click_profiles_id($selectionClickProfilesId) {
		$this->selection_click_profiles_id = \intval($selectionClickProfilesId);
	}
	
	public function getPremium_campaign() {
		return (boolean) $this->premium_campaign;
	}
	public function setPremium_campaign($premiumCampaign) {
		if ((int) $premiumCampaign === 1) {
			$premiumCampaign = true;
		} else {
			$premiumCampaign = false;
		}
		
		$this->premium_campaign = $premiumCampaign;
	}

	public function getAdvertising_materials() {
		return (int) $this->advertising_materials;
	}
	public function setAdvertising_materials($advertisingMaterials) {
		$this->advertising_materials = \intval($advertisingMaterials);
	}
	
	public function getSender_email() {
		return $this->sender_email;
	}
	public function setSender_email($senderEmail) {
		$this->sender_email = $senderEmail;
	}
	
	public function getCharset() {
		return $this->charset;
	}
	public function setCharset($charset) {
		$this->charset = $charset;
	}
	
	public function getCampaignAdvertisings() {
		return $this->campaignAdvertisings;
	}
	public function setCampaignAdvertisings(\Packages\Core\Persistence\ObjectStorage $campaignAdvertisings) {
		$this->campaignAdvertisings = $campaignAdvertisings;
	}
	public function addCampaignAdvertisings(\Modules\Campaign\Domain\Model\CampaignAdvertisingEntity $campaignAdvertisingEntity) {
		$this->campaignAdvertisings->attach($campaignAdvertisingEntity);
	}
	public function removeCampaignAdvertisings(\Modules\Campaign\Domain\Model\CampaignAdvertisingEntity $campaignAdvertisingEntity) {
		$this->campaignAdvertisings->detach($campaignAdvertisingEntity);
	}
	
	public function getContactPersonEntity() {
		return $this->contactPersonEntity;
	}
	public function setContactPersonEntity(\Modules\Customer\Domain\Model\ContactPersonEntity $contactPersonEntity) {
		$this->contactPersonEntity = $contactPersonEntity;
	}
	
	public function getBroking_volume() {
		return (int) $this->broking_volume;
	}
	public function setBroking_volume($brokingVolume) {
		$this->broking_volume = \intval($brokingVolume);
	}

	public function getBroking_price() {
		return (float) $this->broking_price;
	}
	public function setBroking_price($brokingPrice) {
		$this->broking_price = \floatval($brokingPrice);
	}
	
	public function getAdvertisings_comment() {
		return $this->advertisings_comment;
	}
	public function setAdvertisings_comment($advertisings_comment) {
		$this->advertisings_comment = $advertisings_comment;
	}
    	public function getReagierer() {
		return $this->reagierer;
	}
	public function setReagierer($reagierer){
		$this->reagierer = $reagierer;
	}
	public function getAlterVon() {
		return $this->alter_von;
	}
	public function setAlterVon($alter_von){
		$this->alter_von = $alter_von;
	}
	public function getAlterBis() {
		return $this->alter_bis;
	}
	public function setAlterBis($alter_bis){
		$this->alter_bis = $alter_bis;
	}
	public function getPlzVon() {
		return $this->plz_von;
	}
	public function setPlzVon($plz_von){
		$this->plz_von = $plz_von;
	}
	public function getPlzBis() {
		return $this->plz_bis;
	}
	public function setPlzBis($plz_bis){
		$this->plz_bis = $plz_bis;
	}
        public function getExternal_job_id() {
		return $this->externaljobid;
	}
	public function setExternal_job_id($externaljobid){
		$this->externaljobid = $externaljobid;
	}
	public function getZgKommentar() {
		return $this->zg_kommentar;
	}
	public function setZgKommentar($zg_kommentar){
		$this->zg_kommentar = $zg_kommentar;
	}
	
	
				
	
	/**
	 * @deprecated
	 */
	public function getVersandsystem() {
		return $this->versandsystem;
	}
	public function setVersandsystem($versandsystem) {
		$this->versandsystem = $versandsystem;
	}
	
	public function getTkp() {
		return $this->tkp;
	}
	public function setTkp($tkp) {
		$this->tkp = $tkp;
	}
	
	public function getReport() {
		return $this->report;
	}
	public function setReport($report) {
		$this->report = $report;
	}

	public function getRechnung() {
		return $this->rechnung;
	}
	public function setRechnung($rechnung) {
		$this->rechnung = $rechnung;
	}
	
	public function getAgentur() {
		return $this->agentur;
	}
	public function setAgentur($agentur) {
		$this->agentur = $agentur;
	}
	
	public function getAp() {
		return $this->ap;
	}
	public function setAp($ap) {
		$this->ap = $ap;
	}
	
	public function getCrm_order_id() {
		return $this->crm_order_id;
	}
	public function setCrm_order_id($crm_order_id) {
		$this->crm_order_id = $crm_order_id;
	}
	
	public function getCrm_guid() {
		return $this->crm_guid;
	}
	public function setCrm_guid($crm_guid) {
		$this->crm_guid = $crm_guid;
	}
	
	public function getCs_id() {
		return (int) $this->cs_id;
	}
	public function setCs_id($cs_id) {
		$this->cs_id = \intval($cs_id);
	}
	
	public function getAnfrage() {
		return $this->anfrage;
	}
	public function setAnfrage($anfrage) {
		$this->anfrage = $anfrage;
	}
        public function getHybrid_preis() {
		return (float) $this->hybrid_preis;
	}
	public function setHybrid_preis($hybrid_preis) {
		$this->hybrid_preis = \floatval($hybrid_preis);
	}
        
        public function getHybrid_preis2() {
		return (float) $this->hybrid_preis2;
	}
	public function setHybrid_preis2($hybrid_preis2) {
		$this->hybrid_preis2 = \floatval($hybrid_preis2);
	}
        public function getDasp_id() {
		return (int) $this->dasp_id;
	}
	public function setDasp_id($dasp_id) {
		$this->dasp_id = \intval($dasp_id);
	}
        
}