<?php
namespace Modules\Campaign\Domain\Repository;

use Packages\Core\Persistence\AbstractRepository;
use Packages\Core\Persistence\Generic\Query;


/**
 * Description of CampaignRepository
 *
 * @author Cristian.Reus
 */
class CampaignRepository extends AbstractRepository {
	
	/**
	 * contactPersonTable
	 * 
	 * @var string
	 */
	protected $contactPersonTable;
	
	/**
	 * contactPersonPrimaryField
	 * 
	 * @var string
	 */
	protected $contactPersonPrimaryField;
	
	/**
	 * contactPersonFetchClass
	 * 
	 * @var string
	 */
	protected $contactPersonFetchClass;
	
	
	/**
	 * customerTable
	 * 
	 * @var string
	 */
	protected $customerTable;
	
	/**
	 * customerPrimaryField
	 * 
	 * @var string
	 */
	protected $customerPrimaryField;
	
	/**
	 * customerFetchClass
	 * 
	 * @var string
	 */
	protected $customerFetchClass;
	
	
	/**
	 * campaignAdvertisingTable
	 * 
	 * @var string
	 */
	protected $campaignAdvertisingTable;
	
	/**
	 * campaignAdvertisingPrimaryField
	 * 
	 * @var string
	 */
	protected $campaignAdvertisingPrimaryField;
	
	/**
	 * campaignAdvertisingFetchClass
	 * 
	 * @var string
	 */
	protected $campaignAdvertisingFetchClass;
	
	
	/**
	 * logTable
	 * 
	 * @var string
	 */
	protected $logTable;
	
	/**
	 * logPrimaryField
	 * 
	 * @var string
	 */
	protected $logPrimaryField;
	
	/**
	 * logFetchClass
	 * 
	 * @var string
	 */
	protected $logFetchClass;
	
	
	
	/**
	 * findAll
	 * 
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	public function findAll() {
		$queryPartsDataArray = array(
			'WHERE' => array(
				'_' . $this->primaryField => array(
					'sql' => Query::masketField($this->primaryField),
					'value' => 1,
					'comparison' => Query::$fieldGreaterThan
				),
			)
		);
		
		return $this->query->getRowsByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->fetchClass
		);
	}
	
	/**
	 * findById
	 * 
	 * @param integer $id
	 * @return \Modules\Campaign\Domain\Model\CampaignEntity
	 */
	public function findById($id) {
		$object = parent::findById($id);
		
		if ($object instanceof \Modules\Campaign\Domain\Model\CampaignEntity) {
			/* @var $object \Modules\Campaign\Domain\Model\CampaignEntity */
			$object->_resetCleanProperties();
			
			// get and add campaign_advertising
			$campaignAdvertisings = $this->getCampaignAdvertisingsByCampaignId($object->_getProperty($this->primaryField));
			if ($campaignAdvertisings->count() > 0) {
				foreach ($campaignAdvertisings as $campaignAdvertisingEntity) {
					/* @var $campaignAdvertisingEntity \Modules\Campaign\Domain\Model\CampaignAdvertisingEntity */
					
					$campaignAdvertisingEntity->_memorizeCleanState();
					$object->addCampaignAdvertisings($campaignAdvertisingEntity);
				}
			}
			
			try {
				// get and set contactPersonEntity
				$contactPersonEntity = $this->findContactPersonDataById($object->getAp_id());
				if ($contactPersonEntity instanceof \Modules\Customer\Domain\Model\ContactPersonEntity) {
					$customerEntity = $this->findCustomerDataById($contactPersonEntity->getKunde_id());
					
					$contactPersonEntity->setCustomerEntity($customerEntity);
					
					$object->setContactPersonEntity($contactPersonEntity);
				}
			} catch (\Exception $e) {
				// TODO: email senden, gilt nur f�r �ltere kampagnen
			}
			
			$object->_memorizeCleanState();
		}
		
		return $object;
	}
	
	/**
	 * findAllActiveCustomers
	 * 
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	public function findAllActiveCustomers() {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->customerTable),
			'WHERE' => array(
				'_status' => array(
					'sql' => Query::masketField('status'),
					'value' => 1,
					'comparison' => Query::$fieldEqual
				),
			),
			'ORDER_BY' => '`firma` ASC'
		);
		
		return $this->query->getRowsByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->customerFetchClass
		);
	}
	
	/**
	 * getCampaignAdvertisingsByCampaignId
	 * 
	 * @param integer $campaignId
	 * @return \Modules\Campaign\Domain\Model\CampaignAdvertisingEntity
	 */
	public function findCampaignAdvertisingsByCampaignId($campaignId) {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->campaignAdvertisingTable),
			'WHERE' => array(
				'_campaign_id' => array(
					'sql' => Query::masketField('campaign_id'),
					'value' => $campaignId,
					'comparison' => 'integerEqual'
				),
			),
			'ORDER_BY' => Query::masketField('crdate') . ' DESC'
		);
		
		return $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->campaignAdvertisingFetchClass
		);
	}
        
	/**
	 * getCampaignAdvertisingsByCampaignId
	 * 
	 * @param integer $campaignId
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	public function getCampaignAdvertisingsByCampaignId($campaignId) {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->campaignAdvertisingTable),
			'WHERE' => array(
				'_campaign_id' => array(
					'sql' => Query::masketField('campaign_id'),
					'value' => $campaignId,
					'comparison' => 'integerEqual'
				),
			),
			'ORDER_BY' => Query::masketField('crdate') . ' DESC'
		);
		
		return $this->query->getRowsByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->campaignAdvertisingFetchClass
		);
	}
	
	
	
	/**
	 * findContactPersonDataById
	 * 
	 * @param integer $contactPersonId
	 * @return \Modules\Customer\Domain\Model\ContactPersonEntity
	 */
	public function findContactPersonDataById($contactPersonId) {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->contactPersonTable),
			'WHERE' => array(
				'_' . $this->contactPersonPrimaryField => array(
					'sql' => Query::masketField($this->contactPersonPrimaryField),
					'value' => $contactPersonId,
					'comparison' => 'integerEqual'
				),
			),
		);
		
		return $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->contactPersonFetchClass
		);
	}
	
	
	
	/**
	 * findCustomerDataByCompany
	 * 
	 * @param string $customerName
	 * @return \Modules\Customer\Domain\Model\CustomerEntity
	 */
	public function findCustomerDataByCompany($customerName) {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->customerTable),
			'WHERE' => array(
				'_firma' => array(
					'sql' => Query::masketField('firma'),
					'value' => $customerName,
					'comparison' => Query::$fieldEqual
				),
			),
		);
		
		return $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->customerFetchClass
		);
	}
	
	/**
	 * findCustomerDataById
	 * 
	 * @param integer $customerId
	 * @return \Modules\Customer\Domain\Model\CustomerEntity
	 */
	public function findCustomerDataById($customerId) {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->customerTable),
			'WHERE' => array(
				'_' . $this->customerPrimaryField => array(
					'sql' => Query::masketField($this->customerPrimaryField),
					'value' => $customerId,
					'comparison' => 'integerEqual'
				),
			),
		);
		
		return $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->customerFetchClass
		);
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function setContactPersonTable($contactPersonTable) {
		$this->contactPersonTable = $contactPersonTable;
	}
	
	public function setContactPersonPrimaryField($contactPersonPrimaryField) {
		$this->contactPersonPrimaryField = $contactPersonPrimaryField;
	}
	
	public function setContactPersonFetchClass($contactPersonFetchClass) {
		$this->contactPersonFetchClass = $contactPersonFetchClass;
	}

	
	public function setCustomerTable($customerTable) {
		$this->customerTable = $customerTable;
	}
	
	public function setCustomerPrimaryField($customerPrimaryField) {
		$this->customerPrimaryField = $customerPrimaryField;
	}
	
	public function setCustomerFetchClass($customerFetchClass) {
		$this->customerFetchClass = $customerFetchClass;
	}
	
	
	public function setCampaignAdvertisingTable($campaignAdvertisingTable) {
		$this->campaignAdvertisingTable = $campaignAdvertisingTable;
	}

	public function setCampaignAdvertisingPrimaryField($campaignAdvertisingPrimaryField) {
		$this->campaignAdvertisingPrimaryField = $campaignAdvertisingPrimaryField;
	}

	public function setCampaignAdvertisingFetchClass($campaignAdvertisingFetchClass) {
		$this->campaignAdvertisingFetchClass = $campaignAdvertisingFetchClass;
	}

	
	public function setLogTable($logTable) {
		$this->logTable = $logTable;
	}

	public function setLogPrimaryField($logPrimaryField) {
		$this->logPrimaryField = $logPrimaryField;
	}

	public function setLogFetchClass($logFetchClass) {
		$this->logFetchClass = $logFetchClass;
	}

}
