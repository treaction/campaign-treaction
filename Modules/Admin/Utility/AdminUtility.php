<?php
namespace Modules\Admin\Utility;

/**
 * Description of AdminUtility
 *
 * @author Sami.jarmoud
 */
final class AdminUtility {

    /**
	 * deutschdatum
	 * 
	 * @param type $string
	 * @return string
	 */
	public static function deutschdatum ($string){
	  $string = AdminUtility::numfilter($string);
	  $s2  = substr($string,6,2);
	  $s2 .= '.';
	  $s2 .= substr($string,4,2);
	  $s2 .= '.';
	  $s2 .= substr($string,0,4);
  return $s2;
   }
   /**
    * mysqldatum
    * 
    * @param type $string
    * @return string
    */
    public static function mysqldatum ($string){ 
   	  $string = AdminUtility::numfilter($string);
	  $s2  = substr($string,4,4);
	  $s2 .= '-';
	  $s2 .= substr($string,2,2);
	  $s2 .= '-';
	  $s2 .= substr($string,0,2);
  return $s2;
  }
  
  /**
 * numfilter
 * 
 * @param type $string
 * @return string
 */
    public static  function numfilter ($string){ 
   	  $l = strlen($string);
	  $ct = 0;
	  $out = '';
	  while($ct < $l)
	  { $test = substr($string,$ct,1);
	    if($test >= '0' && $test <= '9')
	    {$out .= $test;}
	    $ct++;
	  }
  return $out;
 }
}
