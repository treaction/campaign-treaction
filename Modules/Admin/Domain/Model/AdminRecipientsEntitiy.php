<?php
namespace Modules\Admin\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;

/**
 * AdminRecipientsEntitiy
 * 
 * @author MohamedSamiJarmoud
 *
 */

class AdminRecipientsEntitiy extends AbstractEntity{
			
	/**
	 * r_id
	 * 
	 * @var integer
	 */
	protected $r_id = NULL;
	
	/**
	 * name
	 * 
	 * @var string
	 */
	protected $name;

	/**
	 * first_name
	 * 
	 * @var string
	 */	
	protected $first_name;
	
	/**
	 * sex
	 * 
	 * @var string
	 */	
	protected $sex;
	
	/**
	 * birthday
	 * 
	 * @var string
	 */	
	protected $birthday;
	
	/**
	 * postal_code
	 * 
	 * @var string
	 */	
	protected $postal_code;
	
	/**
	 * country
	 * 
	 * @var string
	 */	
	protected $country;
	
	/**
	 * street
	 * 
	 * @var string
	 */	
	protected $street;

	/**
	 * house_nr
	 * 
	 * @var string
	 */	
	protected $house_nr;

	/**
	 * city;
	 * 
	 * @var string
	 */	
	protected $city;

	/**
	 * email
	 * 
	 * @var string
	 */	
	protected $email;

	
	/**
	 * active
	 * 
	 * @var boolean
	 */
	protected $active;
	
	public function __construct($primaryFieldName = 'r_id') {
		parent::__construct($primaryFieldName);
	}
	

	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 *******************************************************************************************/

	public function getR_id() {
		return $this->r_id;
	}

	public function getName() {
		return $this->name;
	}

	public function getFirst_name() {
		return $this->first_name;
	}

	public function getSex() {
		return $this->sex;
	}

	public function getBirthday() {
		return $this->birthday;
	}

	public function getPostal_code() {
		return $this->postal_code;
	}

	public function getCountry() {
		return $this->country;
	}

	public function getStreet() {
		return $this->street;
	}

	public function getHouse_nr() {
		return $this->house_nr;
	}

	public function getCity() {
		return $this->city;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getActive() {
		return (boolean) $this->active;
	}

	public function setR_id($r_id) {
		$this->r_id = $r_id;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function setFirst_name($first_name) {
		$this->first_name = $first_name;
	}

	public function setSex($sex) {
		$this->sex = $sex;
	}

	public function setBirthday($birthday) {
		$this->birthday = $birthday;
	}

	public function setPostal_code($postal_code) {
		$this->postal_code = $postal_code;
	}

	public function setCountry($country) {
		$this->country = $country;
	}

	public function setStreet($street) {
		$this->street = $street;
	}

	public function setHouse_nr($house_nr) {
		$this->house_nr = $house_nr;
	}

	public function setCity($city) {
		$this->city = $city;
	}

	public function setEmail($email) {
		$this->email = $email;
	}

	public function setActive($active) {
		if ((int) $active === 1) {
			$active = true;
		} else {
			$active = false;
		}
		
		$this->active = $active;
	}	
}