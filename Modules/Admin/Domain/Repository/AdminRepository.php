<?php
namespace Modules\Admin\Domain\Repository;

use Packages\Core\Persistence\AbstractRepository;
use Packages\Core\Persistence\Generic\Query;

/**
 * Description of AdminRepository
 *
 * @author MohamedSamiJarmoud
 */
class AdminRepository extends AbstractRepository {
	
	/**
	 * mandantTable
	 * 
	 * @var string
	 */
	protected $mandantTable;
	
	/**
	 * mandantPrimaryField
	 * 
	 * @var string
	 */
	protected $mandantPrimaryField;
	
	/**
	 * mandantFetchClass
	 * 
	 * @var string
	 */
	protected $mandantFetchClass;
	
	/**
	 * recipientsTable
	 * 
	 * @var string
	 */
	protected $recipientsTable;
	
	/**
	 * recipientsPrimaryField
	 * 
	 * @var string
	 */
	protected $recipientsPrimaryField;
	
	/**
	 * recipientsFetchClass
	 * 
	 * @var string
	 */
	protected $recipientsFetchClass;	
	/**
	 * logTable
	 * 
	 * @var string
	 */
	protected $logTable;
	
	/**
	 * logPrimaryField
	 * 
	 * @var string
	 */
	protected $logPrimaryField;
	
	/**
	 * logFetchClass
	 * 
	 * @var string
	 */
	protected $logFetchClass;
        
        /**
	 * deliverySystemTable
	 * 
	 * @var string
	 */
	protected $deliverySystemTable;
	
	/**
	 * deliverySystemPrimaryField
	 * 
	 * @var string
	 */
	protected $deliverySystemPrimaryField;
	
	/**
	 * deliverySystemFetchClass
	 * 
	 * @var string
	 */
	protected $deliverySystemFetchClass;
	
	
	/**
	 * findAllRecipients
	 *
	 * @return \Packages\Core\Domain\Model\AdminRecipientsEntitiy
	 */
	public function findAllRecipients() {
		$queryPartsDataArray = array(
				'FROM' => Query::masketField($this->recipientsTable),
				'ORDER_BY' => Query::masketField($this->recipientsPrimaryField) . 'DESC'
		);
	
		return $this->query->getRowsByQueryPartsDataArray(
				$queryPartsDataArray,
				$this->recipientsFetchClass
				);
	}
	
	/**
	 * findRecipientById
	 *
	 * @return \Packages\Core\Domain\Model\AdminRecipientsEntitiy
	 */
	public function findRecipientById($recipientId) {
	    if (!\is_int($recipientId)) {
			throw new \InvalidArgumentException('invalid recipient id.', 1446648223);
		}
		
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->recipientsTable),
			'WHERE' => array(
				'_' . $this->recipientsPrimaryField => array(
					'sql' => Query::masketField($this->recipientsPrimaryField),
					'value' => $recipientId,
					'comparison' => 'integerEqual'
				),
			),			    
			'ORDER_BY' => Query::masketField($this->recipientsPrimaryField) . 'DESC'
		);
	
		return $this->query->getRowByQueryPartsDataArray(
				$queryPartsDataArray,
				$this->recipientsFetchClass
				);
	}
        
        /**
	 * getActiveDeliverySystems
	 * 
	 * @return \Packages\Core\Domain\Model\DeliverySystemEntity
	 */
	/*public function getActiveDeliverySystems() {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->deliverySystemTable),
			'WHERE' => array(
				'_active' => array(
					'sql' => Query::masketField('active'),
					'value' => 1,
					'comparison' => 'integerEqual'
				),
			),
		);
		
		return $this->query->getRowsByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->deliverySystemFetchClass
		);
	}*/
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function setMandantTable($mandantTable) {
		$this->mandantTable = $mandantTable;
	}

	public function setMandantPrimaryField($mandantPrimaryField) {
		$this->mandantPrimaryField = $mandantPrimaryField;
	}

	public function setMandantFetchClass($mandantFetchClass) {
		$this->mandantFetchClass = $mandantFetchClass;
	}
	public function setDeliverySystemTable($deliverySystemTable) {
		$this->deliverySystemTable = $deliverySystemTable;
	}

	public function setDeliverySystemPrimaryField($deliverySystemPrimaryField) {
		$this->deliverySystemPrimaryField = $deliverySystemPrimaryField;
	}

	public function setDeliverySystemFetchClass($deliverySystemFetchClass) {
		$this->deliverySystemFetchClass = $deliverySystemFetchClass;
	}
	
        
        public function setRecipientsTable($recipientsTable) {
		$this->recipientsTable = $recipientsTable;
	}

	public function setRecipientsPrimaryField($recipientsPrimaryField) {
		$this->recipientsPrimaryField = $recipientsPrimaryField;
	}

	public function setRecipientsFetchClass($recipientsFetchClass) {
		$this->recipientsFetchClass = $recipientsFetchClass;
	}
        
	public function setLogTable($logTable) {
		$this->logTable = $logTable;
	}

	public function setLogPrimaryField($logPrimaryField) {
		$this->logPrimaryField = $logPrimaryField;
	}

	public function setLogFetchClass($logFetchClass) {
		$this->logFetchClass = $logFetchClass;
	}

}
