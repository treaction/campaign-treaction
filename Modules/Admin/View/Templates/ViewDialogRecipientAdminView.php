<?php
use Modules\Admin\Utility\AdminUtility;
#\Packages\Core\Utility\DebugUtility::debug($recipient, '$recipient');
$disable = $email = $anrede = $vorname = $nachname = $geburtsdatum = $strasse = $hausnummer = $plz = $ort = $land = '';
$enable = 'checked';

if($recipientId > 0
	){

		$email =  $recipient->getEmail();
		$anrede = $recipient->getSex();
		$vorname = $recipient->getFirst_name();
		$nachname = $recipient->getName();
		$geburtsdatum =  AdminUtility::deutschdatum($recipient->getBirthday());
		$strasse = $recipient->getStreet();
		$hausnummer = $recipient->getHouse_nr();
		$plz = $recipient->getPostal_code();
		$ort = $recipient->getCity();
		$land = $recipient->getCountry();		
		$status = $recipient->getActive();

	if($status == '1'){
		$enable = 'checked';
	}else{
		$disable = 'checked';
	}
}
?>

<input type="hidden" name="recipientID" id ="recipientID" value="<?php echo $recipientId; ?>" />
<div id="tab_recipient" class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
 <div  class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class='selected'><a href="#recipientTable"><em>Allgemein</em></a></li>
    </ul>	
		<div class="yui-content" style="margin:0px;padding:0px;">
			 <div id="recipientTable">
				<table class="neu_k" cellpadding="0" cellspacing="0" border="0" style="font-size:11px;">
			    <tr>
                    <td colspan="2" align="right"><span style="color:red;">*</span><span style="color:#666666;font-size:11px"> = Pflichtfelder</span></td>
                </tr>
				 <tr class="bg2">
                    <td align="right" class="info">E-Mail<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="email" name="email" style="width:250px"  value="<?php echo $email;?>" /></td>
				 </tr>
				 <tr>
                    <td align="right" class="info">Anrede<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="anrede" name="anrede" style="width:50px"  value="<?php echo $anrede;?>" /></td>
				 </tr>
				 <tr class="bg2">
                    <td align="right" class="info">Vorname<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="vorname" name="vorname" style="width:250px"  value="<?php echo $vorname;?>" /></td>
				 </tr>				 
				 <tr>
                    <td align="right" class="info">Nachname<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="nachname" name="nachname" style="width:250px"  value="<?php echo $nachname;?>" /></td>
				 </tr>
				 <tr class="bg2">
                    <td align="right" class="info">Geburtsdatum</td>
                    <td align="left"><input type="text" id="geburtsdatum" name="geburtsdatum" style="width:100px"  value="<?php echo $geburtsdatum;?>" /></td>
				 </tr>
				 <tr>
                    <td align="right" class="info">Strasse</td>
                    <td align="left"><input type="text" id="strasse" name="strasse" style="width:250px"  value="<?php echo $strasse;?>" /></td>
				 </tr>
				 <tr class="bg2">
                    <td align="right" class="info">Hausnummer</td>
                    <td align="left"><input type="text" id="hausnummer" name="hausnummer" style="width:50px"  value="<?php echo $hausnummer;?>" /></td>
				 </tr>
				 <tr>
                    <td align="right" class="info">PLZ</td>
                    <td align="left"><input type="text" id="plz" name="plz" style="width:50px"  value="<?php echo $plz;?>" /></td>
				 </tr>
				 <tr class="bg2">
                    <td align="right" class="info">Ort</td>
                    <td align="left"><input type="text" id="ort" name="ort" style="width:150px"  value="<?php echo $ort;?>" /></td>
				 </tr>				 
				 <tr>
                    <td align="right" class="info">Land</td>
                    <td align="left"><input type="text" id="land" name="land" style="width:150px"  value="<?php echo $land;?>" /></td>
				 </tr>
				 <tr class="bg2">
                    <td align="right" class="info">Status</td>
                    <td align="left"><input type="radio" name="status" value="enable" <?php echo $enable;?>/> aktivieren  &nbsp;&nbsp;<input type="radio" name="status" value="disable" <?php echo $disable;?>/> Deaktivieren</td>
				 </tr>				 
				</table>
			 </div>
		</div>
	</div>
	</div>
<script type="text/javascript">
		 var tabView_recipient = new YAHOO.widget.TabView('tab_recipient');
</script>