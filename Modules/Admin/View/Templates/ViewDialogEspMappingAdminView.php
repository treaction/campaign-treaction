<?php

$disable = $asp_id = $name = '';
$enable = 'checked';
if ($espId > 0
) {
    $online_version = $espMapping->getOnlineVersion();
    $unsubscribe_link = $espMapping->getUnsubscribeLink();
    $open_pixel = $espMapping->getOpenPixel();
    $anrede =  $espMapping->getAnrede();
    $vorname =  $espMapping->getVorname();
    $nachname =  $espMapping->getNachname();
    $email =  $espMapping->getEmail();
    $contact_formal = $espMapping->getContact_formal();
    $contact_unformal = $espMapping->getContact_unformal();
    $contact_standard = $espMapping->getContact_standard();
    $status = $espMapping->getActive();
    $versandsystem = $espMapping->getVersandsystem();
	if($status == '1'){
		$enable = 'checked';
	}else{
		$disable = 'checked';
	}
}
?>
<input type="hidden" name="espID" id ="espID" value="<?php echo $espId; ?>" />
<div id="tab_esp" class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
    <div  class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
        <ul class="yui-nav" style="margin-left:-1px;">
            <li class='selected'><a href="#dsdnTable"><em>Allgemein</em></a></li>
        </ul>	
        <div class="yui-content" style="margin:0px;padding:0px;">
            <div id="dsdnTable">
                <table class="neu_k" cellpadding="0" cellspacing="0" border="0" style="font-size:11px;">
                    <tr>
                        <td colspan="2" align="right"><span style="color:red;">*</span><span style="color:#666666;font-size:11px"> = Pflichtfelder</span></td>
                    </tr>
                    <tr class="bg2">
                        <td align="right" class="info">Versandsystem<span style="color:red;">*</span></td>
                        <td align="left">
                             <select name="deliverySystem" id="deliverySystem" >
				<option value=" ">- Bitte ausw&auml;hlen -</option>
				<?php echo $deliverySystemEspOptionItems; ?>
			</select>                          
                        </td>
                    </tr>
                   
                    <tr class="bg2">
                        <td align="right" class="info">Online Version<span style="color:red;">*</span></td>
                        <td align="left"><input type="text" id="online_version" name="online_version" style="width:250px"  value="<?php echo isset($online_version) ? $online_version : '' ?>" /></td>
                    </tr>
                    <tr class="bg2">
                        <td align="right" class="info">Unsubscribe Link<span style="color:red;">*</span></td>
                        <td align="left"><input type="text" id="unsubscribe_link" name="unsubscribe_link" style="width:250px"  value="<?php echo isset($unsubscribe_link) ? $unsubscribe_link : '' ?>" /></td>
                    </tr>
             
                    <tr class="bg2">
                        <td align="right" class="info">Open Pixel</td>
                        <td align="left"><input type="text" id="open_pixel" name="open_pixel" style="width:250px"  value="<?php echo isset($open_pixel) ? $open_pixel : '' ?>" /></td>
                    </tr>
                     <tr class="bg2">
                        <td align="right" class="info">Anrede<span style="color:red;">*</span></td>
                        <td align="left"><input type="text" id="anrede" name="anrede" style="width:250px"  value="<?php echo isset($anrede) ? $anrede : '' ?>" /></td>
                    </tr>
                     <tr class="bg2">
                        <td align="right" class="info">Vorname<span style="color:red;">*</span></td>
                        <td align="left"><input type="text" id="vorname" name="vorname" style="width:250px"  value="<?php echo isset($vorname) ? $vorname : '' ?>" /></td>
                    </tr>
                     <tr class="bg2">
                        <td align="right" class="info">Nachname<span style="color:red;">*</span></td>
                        <td align="left"><input type="text" id="nachname" name="nachname" style="width:250px"  value="<?php echo isset($nachname) ? $nachname : '' ?>" /></td>
                    </tr>
                     <tr class="bg2">
                        <td align="right" class="info">Email<span style="color:red;">*</span></td>
                        <td align="left"><input type="text" id="email" name="email" style="width:250px"  value="<?php echo isset($email) ? $email : '' ?>" /></td>
                    </tr>
                     <tr class="bg2">
                        <td align="right" class="info">Contact Formal</td>
                        <td align="left"><input type="text" id="contact_formal" name="contact_formal" style="width:250px"  value="<?php echo isset($contact_formal) ? $contact_formal : '' ?>" /></td>
                    </tr>
                     <tr class="bg2">
                        <td align="right" class="info">Contact Unformal</td>
                        <td align="left"><input type="text" id="contact_unformal" name="contact_unformal" style="width:250px"  value="<?php echo isset($contact_unformal) ? $contact_unformal : '' ?>" /></td>
                    </tr>
                     <tr class="bg2">
                        <td align="right" class="info">Contact Standard</td>
                        <td align="left"><input type="text" id="contact_standard" name="contact_standard" style="width:250px"  value="<?php echo isset($contact_standard) ? $contact_standard : '' ?>" /></td>
                    </tr>
                    <tr>
                        <td align="right" class="info">Status<span style="color:red;">*</span></td>
                        <td align="left"><input type="radio" name="status" value="enable" <?php echo $enable; ?>/> aktivieren  &nbsp;&nbsp;<input type="radio" name="status" value="disable" <?php echo $disable; ?>/> Deaktivieren</td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var tabView_esp = new YAHOO.widget.TabView('tab_esp');
   
</script>