

<?php
$j = $dataEsp = '';

foreach($espMappings as $espMapping) {
       if ($j == 3 || !$j) {
            $j = 1;
        }
        
        if ($j == 1) {
            $bg_color = '#FFFFFF';
        } elseif ($j == 2) {
            $bg_color = '#E4E4E4';
        }
        
        $status = $espMapping->getActive()== '1' ? 'aktiv' : 'inaktiv';
				if($status == 'inaktiv'){
			$color = '#F40404';
		}else{
			$color = '';
		}
                
        /* @var $deliverySystemEntity \Packages\Core\Domain\Model\DeliverySystemEntity */    
        $deliverySystemEntity = $this->systemRepository->findDeliverySystemByAbkz($espMapping->getVersandsystem());
        $versandsystem = $deliverySystemEntity->getAsp();  
        
	$dataEsp .= "<tr onclick=\"doIt_esp(event,'" . $espMapping->getId() . "');\" bgcolor='" . $bg_color . "' onMouseOver=\"this.bgColor='#A6CBFF'\" onMouseOut=\"this.bgColor='" . $bg_color . "' \" style='cursor:pointer' >";
        $dataEsp .= "<td style='font-weight:bold;'>" . $espMapping->getId(). "</td>";
        $dataEsp .= "<td style='font-weight:bold;'>" . $versandsystem . "</td>";
        $dataEsp .= "<td style='font-weight:bold;'>" . $espMapping->getOnlineVersion() . "</td>";
        $dataEsp .= "<td style='font-weight:bold;'>" . $espMapping->getUnsubscribeLink() . "</td>";
        $dataEsp .= "<td style='font-weight:bold;'>" . $espMapping->getOpenPixel() . "</td>";
        $dataEsp .= "<td style='font-weight:bold;'>" . $espMapping->getAnrede() . "</td>";
        $dataEsp .= "<td style='font-weight:bold;'>" . $espMapping->getVorname() . "</td>";
        $dataEsp .= "<td style='font-weight:bold;'>" . $espMapping->getNachname() . "</td>";
        $dataEsp .= "<td style='font-weight:bold;'>" . $espMapping->getEmail() . "</td>";
        $dataEsp .= "<td style='font-weight:bold;'>" . $espMapping->getContact_formal() . "</td>";
        $dataEsp .= "<td style='font-weight:bold;'>" . $espMapping->getContact_unformal() . "</td>";
        $dataEsp .= "<td style='font-weight:bold;'>" . $espMapping->getContact_standard() . "</td>";
        $dataEsp .= "<td style='font-weight:bold; color: ". $color ."'>" . $status . "</td>";
        $dataEsp .= "</tr>";
        $j++;
}	

?>
<input type="hidden" name="espID" id ="espID" value="" />
<div id="fakeContainer">
 <table class="scrollTable2" id="scrollTable2" cellpadding="0" cellspacing="0" border="0" bgcolor="#F5F5F5">
        <tr>
            <th>ID</th>
            <th>Versandsystem</th>
            <th>Online Version</th>
            <th>Unsubscribe Link</th>
            <th>Open Pixel</th>
            <th>Anrede</th>
            <th>Vorname</th>
            <th>Nachname</th>
            <th>Email</th>
            <th>Contact Formal</th>
            <th>Contact Unformal</th>
            <th>Contact Standard</th>
            <th>Status</th>
        </tr>
        <?php print $dataEsp; ?>
    </table>
</div>
<script type="text/javascript">
    wi = document.documentElement.clientWidth;
    hi = document.documentElement.clientHeight;
    
    if (uBrowser == 'Firefox') {
        hi = hi-142;
        wi = wi-205;
    } else {
        hi = hi-143;
        wi = wi-205;
    }
    
    document.getElementById("fakeContainer").style.width = wi+"px";
    document.getElementById("fakeContainer").style.height = hi+"px";



    var mySt = new superTable("scrollTable2", {
        cssSkin : "sSky",
        fixedCols : 0,
        headerRows : 1
    });
</script>





