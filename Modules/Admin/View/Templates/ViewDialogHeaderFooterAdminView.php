<?php

$disable = $asp_id = $name = '';
$enable = 'checked';
if ($id > 0
) {
    $header = $headerFooterEntity->getHeader();
    $footer = $headerFooterEntity->getFooter();
    $status = $headerFooterEntity->getActive();
    $versandsystem = $headerFooterEntity->getVersandsystem();
	if($status == '1'){
		$enable = 'checked';
	}else{
		$disable = 'checked';
	}
}
?>
<input type="hidden" name="id" id ="id" value="<?php echo $id; ?>" />
<div id="tab_hf" class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
    <div  class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
        <ul class="yui-nav" style="margin-left:-1px;">
            <li class='selected'><a href="#hfTable"><em>Allgemein</em></a></li>
        </ul>	
        <div class="yui-content" style="margin:0px;padding:0px;">
            <div id="dsdnTable">
                <table class="neu_k" cellpadding="0" cellspacing="0" border="0" style="font-size:11px;">
                    <tr>
                        <td colspan="2" align="right"><span style="color:red;">*</span><span style="color:#666666;font-size:11px"> = Pflichtfelder</span></td>
                    </tr>
                    <tr class="bg2">
                        <td align="right" class="info">Versandsystem<span style="color:red;">*</span></td>
                        <td align="left">
                             <select name="deliverySystem" id="deliverySystem" >
				<option value=" ">- Bitte ausw&auml;hlen -</option>
				<?php echo $deliverySystemEspOptionItems; ?>
			</select>                          
                        </td>
                    </tr>
                   
                    <tr class="bg2">
                        <td align="right" class="info">Header<span style="color:red;">*</span></td>
                        <td align="left">
                            <textarea name="header" id="header" style="height:120px; width: 98%;"><?php echo isset($header) ? $header : ''; ?></textarea>
                        </td>
                    </tr>
                    <tr class="bg2">
                        <td align="right" class="info">Footer<span style="color:red;">*</span></td>
                        <td align="left">
                             <textarea name="footer" id="footer" style="height:120px; width: 98%;"><?php echo isset($footer) ? $footer : ''; ?></textarea>
                        </td>
                    </tr>          
                    <tr>
                        <td align="right" class="info">Status<span style="color:red;">*</span></td>
                        <td align="left"><input type="radio" name="status" value="enable" <?php echo $enable; ?>/> aktivieren  &nbsp;&nbsp;<input type="radio" name="status" value="disable" <?php echo $disable; ?>/> Deaktivieren</td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var tabView_hf = new YAHOO.widget.TabView('tab_hf');
   
</script>