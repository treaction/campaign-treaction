<?php

$disable = $asp_id = $name = '';
$enable = 'checked';
if ($deliverySystemDomainId > 0
) {
    $name = $deliverySystemDomain->getDomain_name();
    $status = $deliverySystemDomain->getActive();

	if($status == '1'){
		$enable = 'checked';
	}else{
		$disable = 'checked';
	}
}
?>
<input type="hidden" name="dsdnID" id ="dsdnID" value="<?php echo $deliverySystemDomainId; ?>" />
<div id="tab_dsdn" class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
    <div  class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
        <ul class="yui-nav" style="margin-left:-1px;">
            <li class='selected'><a href="#dsdnTable"><em>Allgemein</em></a></li>
        </ul>	
        <div class="yui-content" style="margin:0px;padding:0px;">
            <div id="dsdnTable">
                <table class="neu_k" cellpadding="0" cellspacing="0" border="0" style="font-size:11px;">
                    <tr>
                        <td colspan="2" align="right"><span style="color:red;">*</span><span style="color:#666666;font-size:11px"> = Pflichtfelder</span></td>
                    </tr>
                    <tr class="bg2">
                        <td align="right" class="info">Versandsystem<span style="color:red;">*</span></td>
                        <td align="left">
                             <select name="deliverySystem" id="deliverySystem" onchange="changeDeliverySystem(this.value, 'deliverySystemDistributor', 0, 0);">
				<option value="">- Bitte ausw&auml;hlen -</option>
				<?php echo $deliverySystemDomainOptionItems; ?>
			</select>                          
                        </td>
                    </tr>
                     <tr id="deliverySystemDistributorCell">
						<td align="right" class="info">Verteiler:</td>
						<td align="left">
							<div id="deliverySystemDistributorContent"></div>
							<div id="deliverySystemDistributor" class="floatLeft">
								<select name="campaign[deliverySystemDistributor]" id="deliverySystemDistributorItems" class="widthAuto">
									<?php echo $deliverySystemDistributorItem; ?>
                                                                    
								</select>
							</div>
						</td>
		  </tr>
                    <tr class="bg2">
                        <td align="right" class="info">Versanddomain<span style="color:red;">*</span></td>
                        <td align="left"><input type="text" id="domain" name="domain" style="width:250px"  value="<?php echo $name; ?>" /></td>
                    </tr>
                    <tr>
                        <td align="right" class="info">Status<span style="color:red;">*</span></td>
                        <td align="left"><input type="radio" name="status" value="enable" <?php echo $enable; ?>/> aktivieren  &nbsp;&nbsp;<input type="radio" name="status" value="disable" <?php echo $disable; ?>/> Deaktivieren</td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var tabView_dsdn = new YAHOO.widget.TabView('tab_dsdn');
   
    function changeDeliverySystem(deliverySystemValue, inputDistributorId, disableNotSelectedItem, campaignDsdId) {
	hideViewById(inputDistributorId + 'Cell');
	YAHOO.util.Dom.get(inputDistributorId + 'Content').innerHTML = '';

	if (deliverySystemValue !== '') {
		processAjaxRequest(
			'AjaxRequests/ajaxClient.php',
			'actionMethod=deliverySystemDistributor'
				+ '&deliverySystem=' + deliverySystemValue
				+ '&disableNotSelectedItem=' + parseInt(disableNotSelectedItem)
				+ '&dsdId=' + parseInt(campaignDsdId)
			,
			inputDistributorId,
			function(deliverySystemDistributorDataArray) {
				if (typeof deliverySystemDistributorDataArray != 'undefined') {
					showViewById(inputDistributorId + 'Content');
					YAHOO.util.Dom.get(inputDistributorId + 'Content').innerHTML = deliverySystemDistributorDataArray;

					hideViewById(inputDistributorId);
				} else {
					hideViewById(inputDistributorId + 'Content');
					showViewById(inputDistributorId);

					deliverySystemDistributor.disabled = false;
				}

				showViewById(inputDistributorId + 'Cell');
			}
		);
	} else {
		removeSelectItemsById(inputDistributorId + 'Items');

		deliverySystemDistributor.disabled = true;
	}
}
</script>