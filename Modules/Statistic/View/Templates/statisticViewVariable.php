<?php
use Packages\Core\Utility\Html\FormUtility;
use Packages\Core\Utility\SystemHelperUtility;
use Modules\Campaign\Utility\CampaignAndCustomerUtility;
use Modules\Statistic\Utility\StatisticUtility;

$selectedDeliverySystem = isset($actionRequestDataArray ["deliverySystem"]) 
	? (int) $actionRequestDataArray ["deliverySystem"] 
	: 'all'
;
$selectedDeliverySystemDistributor = isset($actionRequestDataArray ["deliverySystemDistributor"]) 
	? (int)$actionRequestDataArray ["deliverySystemDistributor"] 
	: 'all'
;
$selectedClientClickProfile = isset($actionRequestDataArray ["clientClickProfile"])
? (int)$actionRequestDataArray ["clientClickProfile"]
: 'all'
;
$selectedGenderSystem = isset($actionRequestDataArray ["genderSystem"])
? (int)$actionRequestDataArray ["genderSystem"]
: 'beide'
;

//VersandSystem
$deliverySystemOptionItems = SystemHelperUtility::getDeliverySystemsItemsByIdOptionList(
	$clientDeliveries,
	$selectedDeliverySystem
);

//Verteiler
if (isset($actionRequestDataArray ["deliverySystem"])
		&& ($actionRequestDataArray ["deliverySystem"] != 'all')
		) {
	$selectedDeliverySystemDistributorEntity = new \Packages\Core\Domain\Model\DeliverySystemDistributorEntity();
	$campaignDeliverySystemOptions = StatisticUtility::getDeliverySystemDistributorsList(
		$deliverySystemDistributorDataArray,
		$selectedDeliverySystemDistributor
	);
}
//Klickprofil
$campaignSelectionClickProfilesOptions = StatisticUtility::getClientClickProfilesOptionList(
	$clientClickProfiles,
	$selectedClientClickProfile
);
$klickprofil = StatisticUtility::getClientClickProfileByName(
	$clientClickProfiles, 
	$selectedClientClickProfile
);
$shortKlickprofil = StatisticUtility::getClientClickProfileByShortcut(
	$clientClickProfiles,
	$selectedClientClickProfile
);
#\Packages\Core\Utility\DebugUtility::debug($deliverySystemDistributorDataArray, '$deliverySystemDistributorDataArray');
//Geschlecht
$genderSystemOptionItems = FormUtility::createOptionListItemData(
	CampaignAndCustomerUtility::$genderDataArray,
	$selectedGenderSystem 
);
//Datum
date_default_timezone_set("Europe/Berlin");
$timestamp = time();
$datum = date("d.m.Y",$timestamp);

if(isset($actionRequestDataArray['auswertung'])){
	$auswertung = $actionRequestDataArray['auswertung'];
}else{
	$auswertung = false;
}

if(isset($actionRequestDataArray['aktualisieren'])){
	$aktualisieren = $actionRequestDataArray['aktualisieren'];
}else{
	$aktualisieren = false;
}
if(isset($actionRequestDataArray['anzeigen'])){
	if( $actionRequestDataArray['anzeigen'] == true) {
		$anzeigen = $actionRequestDataArray['anzeigen'];
	  }
	  }else{
	  	$anzeigen = false;
	}
if(isset($actionRequestDataArray['vorhanden'])){
	if( $actionRequestDataArray['vorhanden'] == true) {
		$vorhanden = $actionRequestDataArray['vorhanden'];
	 }
	}else{
		$vorhanden = false;
	  }

?>