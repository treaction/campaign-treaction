

<?php
use Modules\Statistic\Utility\StatisticUtility;

require_once 'statisticViewVariable.php';

require_once 'statisticViewTableHeader.php';
?>  
<table class="tcm_table newTableView"  width="950" >
<thead>
  <tr class="headerStyleColumn">
    <th>Nummer</th>
    <th>Titel</th>
    <th>Datum</th>
    <th>Benutzer</th>
    <th>Kriterien der Abfrage </th>
    <th>Status</th>
    <th>Ergebnis</th>
      </tr>
</thead>   
 <?php  
        if($auswertung == true ){  ?>
 <tbody>
   <tr>
    <td>#</td>
    <td>Tempor&auml;r</td>
    <td id="d_v"><?php echo $datum;?></td>
    <td id="b_v"><?php echo $resultAuswertung['benutzer'];?></td>
    <td>
    <table>
    <tr>
        <td>Versandsystem</td>
        <td>Verteiler</td>
        <td>Klickprofil</td>
        <td>Geschlecht</td>
        <td>Reagierer</td>
        <td>Alter von</td>
        <td>Alter bis</td>
        <td>PLZ von</td>
        <td>PLZ bis</td>
     </tr>
     <tr>   
        <td id="ds_v" ><?php echo $resultAuswertung['versandSystem'];?></td>
        <td id="dsd_v"><?php echo $resultAuswertung['verteilerTitle'];?></td>
        <input type="hidden" id="ccps_v" name="ccp_v" value="<?php echo $shortKlickprofil;?>" >
		<input type="hidden" id="ccp_v" name="ccp_v" value="<?php echo $selectedClientClickProfile;?>" >
        <td><?php echo $klickprofil;?></td>
        <td id="gs_v"><?php echo $resultAuswertung['result']['gender'];?></td>
        <?php if (!empty($actionRequestDataArray['reagierer'])){?>
        <td id="r_v"><?php echo $actionRequestDataArray['reagierer'];?></td>
        <?php }else{?>
        <td id="r_v"></td>
        <?php }?>
         <?php if (!empty($actionRequestDataArray['alterVon'])){?>
        <td id="av_v"><?php echo $actionRequestDataArray['alterVon'];?></td>
        <?php }else{?>
          <td id="av_v"></td>
        <?php }?>
         <?php if (!empty($actionRequestDataArray['alterBis'])){?>
        <td id="ab_v"><?php echo $actionRequestDataArray['alterBis'];?></td>
        <?php }else{?>
          <td id="ab_v"></td>
        <?php }?>
         <?php if (!empty($actionRequestDataArray['plzVon'])){?>
        <td id="pv_v"><?php echo $actionRequestDataArray['plzVon'];?></td>
        <?php }else{?>
          <td id="pv_v"></td>
        <?php }?>
         <?php if (!empty($actionRequestDataArray['plzBis'])){?>
        <td id="pb_v"><?php echo $actionRequestDataArray['plzBis'];?></td>
        <?php }else{?>
          <td id="pb_v"></td>
        <?php }?>
      <tr>
       </table>
    </td>
    <td>Erledigt</td>
    <td id="ergebnis"><?php echo StatisticUtility::numberFormat($resultAuswertung['result']['data']);?></td>
    <?php if((int)$resultAuswertung['result']['data'] != 0){?>
     <td><input type="button" onclick ="verwenden()" value="Verwenden"></td>
	 <td><input type="button" onclick ="verwerfen()" value="Verwerfen"></td>
     <?php }?>
  </tr>
 <?php }?>
 
  <?php foreach ($resultAuswertung['statistiken'] as $key => $statistiken){?>

  <tr>
  <td id="id_a<?php echo $key;?>"><?php echo $statistiken->getId();?></td>
  <input type="hidden" id="fn_a<?php echo $key;?>" name="fn_a<?php echo $key;?>" value="<?php echo $statistiken->getFunktionName();?>">
  <td><?php echo $statistiken->getTitel();?></td>
  <td><?php echo StatisticUtility::deutschdatum($statistiken->getDatum());?></td>
  <td><?php echo $statistiken->getBenutzer();?></td>
  <td>
	  <table>
	    <tr>
		    <td>Versandsystem</td>
		    <td>Verteiler</td>
		    <td>Klickprofil</td>
		    <td>Geschlecht</td>
		    <td>Reagierer</td>
		    <td>Alter von</td>
		    <td>Alter bis</td>
		    <td>PLZ von</td>
		    <td>PLZ bis</td>
	    </tr>
	    <tr>
	        <td id="ds_a<?php echo $key;?>"><?php echo $statistiken->getVersandSystem();?></td>
            <td id="dsd_a<?php echo $key;?>"><?php echo $statistiken->getVerteiler();?></td>
	        <td id="ccp_a<?php echo $key;?>"><?php echo $statistiken->getKlickProfil();?></td>
	        <td id="gs_a<?php echo $key;?>"><?php echo $statistiken->getGeschlecht();?></td>
	        <td id="r_a<?php echo $key;?>"><?php echo $statistiken->getReagierer();?></td>  
	        <td id="av_a<?php echo $key;?>"><?php echo $statistiken->getAlterVon();?></td> 
	        <td id="ab_a<?php echo $key;?>"><?php echo $statistiken->getAlterBis();?></td>
	        <td id="pv_a<?php echo $key;?>"><?php echo $statistiken->getPlzVon();?></td>
	        <td id="pb_a<?php echo $key;?>"><?php echo $statistiken->getPlzBis();?></td>
	    <tr>
	  </table>
  </td>
  <td><?php echo $statistiken->getStatus();?></td>
  <td id="ds_v"><?php echo StatisticUtility::numberFormat($statistiken->getErgebnis());?></td>
 <!-- <td>
  <?php #if($statistiken->getIsApply() == 0){?>
  <input type="button" title="Statische Auswertung im Vesandsystem anlegen" onclick ="anlegen<?php #echo $key;?>()" value="im VS Anlegen">
 <?php# }else{?>
    <b>ist angelegt</b>
 <?php #}?>
  </td> -->
    <td>
		<input type="button" onclick ="aktualisieren<?php echo $key;?>()" value="Aktualisieren" title="aktualisieren">
  </td>
  <td>
	  <input type="button" id="update<?php echo $key;?>" value="Editieren" title="Titel editieren">
  </td>
  </tr>
  <?php }?>
</tbody> 
</table>
 <?php foreach ($resultAuswertung['statistiken'] as $key => $statistiken){?>
<div id="dialog<?php echo $key;?>" class="yui-pe-content">
<div class="panel_label">Statistik Manager</div>
    <div class="hd" id="nv_hd">
        <span class="hd_label">Titel bearbeiten</span>
        <span class="hd_sublabel"></span>
    </div>
<div class="bd">
	<div class="info">Titel:</div>
	<input type="text" id="titel<?php echo $key;?>" style="width:400px;" name="titel" value="<?php echo $statistiken->getTitel();?>">
	<input type="hidden" id="id_h<?php echo $key;?>" value="<?php echo $statistiken->getId();?>"/>
</div>
</div>
  <?php } 
  
 require_once ('statisticViewFunctions.php');

		