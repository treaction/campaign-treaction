<?php
namespace Modules\Statistic\Utility;

use Packages\Core\Utility\Html\FormUtility;


/**
 * Description of StatisticUtility
 *
 * @author Cristian.Reus
 */
final class StatisticUtility {
	
	/**
	 * constante
	 */
     const INTONE_DE = 0.48;
	 const INTONE_AT = 0.57;
	 const INTONE_CH = 0.55;
	 const INTONE_PREMIUM_DE = 0.66;
	 const INTONE_PREMIUM_AT = 0.58;
	 const INTONE_PREMIUM_CH = 0.67;
	 
	 const COMPLEAD_DE = 0.57;
	 const COMPLEAD_AT = 0.55;
	 const COMPLEAD_CH = 0.51;
	 const COMPLEAD_PREMIUM = 0.65;
	 
	 const SKYLINE_DE = 0.58;
	 const SKYLINE_AT = 0.58;
	 const SKYLINE_CH = 0.54;
	 const SKYLINE_PREMIUM = 0.80;
	 const STELLAR_DE = 0.78;
     const STELLAR_AT = 0.71;
	 const STELLAR_CH = 0.83;
	 const STELLAR_PREMIUM = 0.37;
	
	
	/**
	 * genderDataArray
	 * 
	 * @var array
	 */
	public static $genderDataArray = array(
		1 => 'nur Herren',
		2 => 'nur Frauen',
		0 => 'beide'
	);
	
	
	
	/**
	 * getClientClickProfilesOptionList
	 * 
	 * @param \Packages\Core\Persistence\ObjectStorage $clientClickProfiles
	 * @param integer $selectedItem
	 * @return string
	 */
	public static function getClientClickProfilesOptionList(\Packages\Core\Persistence\ObjectStorage $clientClickProfiles, $selectedItem) {
		$resultSelectionItems = '';
		
		foreach ($clientClickProfiles as $clientClickProfile) {
			/* @var $clientClickProfile \Packages\Core\Domain\Model\ClientClickProfileEntity */
			
			$selected = '';
			if ((int) $selectedItem === $clientClickProfile->getUid()) {
				$selected = 'selected';
			}
			
			$resultSelectionItems .= FormUtility::createOptionItem(
				array(
					'value' => $clientClickProfile->getUid(),
					'selected' => $selected
				),
				$clientClickProfile->getClickProfileEntity()->getTitle() . ' (' . $clientClickProfile->getClickProfileEntity()->getShortcut() . ')'
			);
		}
		
		return $resultSelectionItems;
	}
	/**
	 * getDeliverySystemDistributorsList
	 *
	 * @param \Packages\Core\Persistence\ArrayIterator $deliverySystemDistributorDataArray
	 * @param integer $selectedItem
	 * @return string
	 */
	public static function getDeliverySystemDistributorsList(\Packages\Core\Persistence\ArrayIterator $deliverySystemDistributorDataArray, $selectedItem) {
		$resultSelectionItems = '';
	
		foreach ($deliverySystemDistributorDataArray as $key => $deliverySystemDistributorEntity) {
				
			$selected = '';
			if ((int) $selectedItem === $deliverySystemDistributorEntity->getId()) {
				$selected = 'selected';
			}
				
			$resultSelectionItems .= FormUtility::createOptionItem(
					array(
							'value' => $deliverySystemDistributorEntity->getId(),
							'selected' => $selected
					),
					$deliverySystemDistributorEntity->getTitle()
					);
		}
	
		return $resultSelectionItems;
	}
	/**
	 * getClientClickProfileByName
	 * 
	 * @param \Packages\Core\Persistence\ObjectStorage $clientClickProfiles
	 * @param type $selectedItem
	 * @return string
	 */
	public static function getClientClickProfileByName(\Packages\Core\Persistence\ObjectStorage $clientClickProfiles,$selectedItem){
		foreach ($clientClickProfiles as $clientClickProfile) {
			if ((int) $selectedItem === $clientClickProfile->getUid()) {
				   return $clientClickProfile->getClickProfileEntity()->getTitle();
			}
		}
	}
	/**
	 * getClientClickProfileByShortcut
	 * 
	 * @param \Packages\Core\Persistence\ObjectStorage $clientClickProfiles
	 * @param type $selectedItem
	 * @return string
	 */
	public static function getClientClickProfileByShortcut(\Packages\Core\Persistence\ObjectStorage $clientClickProfiles,$selectedItem){
		foreach ($clientClickProfiles as $clientClickProfile) {
			if ((int) $selectedItem === $clientClickProfile->getUid()) {
				return $clientClickProfile->getClickProfileEntity()->getShortcut();
			}
		}
	}
	/**
	 * getNettoResult
	 * 
	 * @param integer $deliverySystemDistributor
	 * @param integer $brutto
	 * @return float
	 */
	public static function getNettoResult($deliverySystemDistributor, $brutto){
		switch ($deliverySystemDistributor){
		case 12:
				$netto = self::INTONE_DE * $brutto;
			break;
		case 13:
				$netto = self::INTONE_AT * $brutto;
			break;
		case 14:
				$netto = self::INTONE_CH * $brutto;
			break;
		case 18:
				$netto = self::INTONE_PREMIUM_DE * $brutto;
			break;
		case 19:
				$netto = self::INTONE_PREMIUM_AT * $brutto;
			break;
		case 20:
				$netto = self::INTONE_PREMIUM_CH * $brutto;
			break;
		case 8:
				$netto = self::COMPLEAD_DE * $brutto;
			break;
		case 9:
				$netto = self::COMPLEAD_AT * $brutto;
			break;
		case 10:
				$netto = self::COMPLEAD_CH * $brutto;
			break;
		case 11:
				$netto = self::COMPLEAD_PREMIUM * $brutto;
			break;
		case 5:
				$netto = self::SKYLINE_DE * $brutto;
			break;
		case 6:
				$netto = self::SKYLINE_AT * $brutto;
			break;
		case 7:
				$netto = self::SKYLINE_CH * $brutto;
			break;
		case 21:
				$netto = self::SKYLINE_PREMIUM * $brutto;
		    break;
		case 22:
				$netto = self::STELLAR_DE * $brutto;
			break;
		case 23:
				$netto = self::STELLAR_AT * $brutto;
			break;
		case 24:
				$netto = self::STELLAR_CH * $brutto;
			break;
		case 25:
				$netto = self::STELLAR_PREMIUM * $brutto;
			break;
		default :
			$netto = $brutto;
		}
		
		return $netto;
	}
	/**
	 * getMysqlDatum
	 * 
	 * @return datum
	 */
	public static function getMysqlDatum(){
		date_default_timezone_set("Europe/Berlin");
       $timestamp = time();
       $datum = date("d.m.Y",$timestamp);
	   $sqlDatum = StatisticUtility::mysqldatum($datum);
	   return $sqlDatum;
	}
	/**
	 * deutschdatum
	 * 
	 * @param type $string
	 * @return string
	 */
	public static function deutschdatum ($string){
	  $string = StatisticUtility::numfilter($string);
	  $s2  = substr($string,6,2);
	  $s2 .= '.';
	  $s2 .= substr($string,4,2);
	  $s2 .= '.';
	  $s2 .= substr($string,0,4);
  return $s2;
   }
   /**
    * mysqldatum
    * 
    * @param type $string
    * @return string
    */
    public static function mysqldatum ($string){ 
   	  $string = StatisticUtility::numfilter($string);
	  $s2  = substr($string,4,4);
	  $s2 .= '-';
	  $s2 .= substr($string,2,2);
	  $s2 .= '-';
	  $s2 .= substr($string,0,2);
  return $s2;
}
/**
 * numfilter
 * 
 * @param type $string
 * @return string
 */
    public static  function numfilter ($string){ 
   	  $l = strlen($string);
	  $ct = 0;
	  $out = '';
	  while($ct < $l)
	  { $test = substr($string,$ct,1);
	    if($test >= '0' && $test <= '9')
	    {$out .= $test;}
	    $ct++;
	  }
  return $out;
}
	/**
	 * sumFormat
	 * 
	 * @param float $number
	 * @param integer $decimal
	 * @return numeric
	 */
	public static  function sumFormat($number, $decimal = 2) {
		return \number_format($number, $decimal, ',', '.');
	}
	/**
	 * sumFormatWithDecimal3
	 * 
	 * @param float $number
	 * @param integer $decimal
	 * @return numeric
	 */
	public static  function sumFormatWithDecimal3($number, $decimal = 3) {
		return \number_format($number, $decimal, ',', '.');
	}
	/**
	 * numberFormat
	 * 
	 * @param float $number
	 * @return float
	 */
	public static function numberFormat($number) {
		return \number_format($number, 0, '', '.');
	}
}