<?php
namespace Modules\Campaign\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * Description of LogEntity
 *
 * @author Cristian.Reus
 */
class LogEntity extends AbstractEntity {
	
	/**
	 * logid
	 * 
	 * @var integer
	 */
	protected $logid = NULL;
	
	/**
	 * userid
	 * 
	 * @var integer
	 */
	protected $userid;
	
	/**
	 * kid
	 * 
	 * @var integer
	 */
	protected $action;
	
	/**
	 * logid
	 * 
	 * @var integer
	 */
	protected $kid;
	
	/**
	 * timestamp
	 * 
	 * @var \DateTime
	 */
	protected $timestamp;
	
	/**
	 * status
	 * 
	 * @var integer
	 */
	protected $status;
	
	/**
	 * import_nr
	 * 
	 * @var integer
	 */
	protected $import_nr = 0;
	
	/**
	 * log_data
	 * 
	 * @var string
	 */
	protected $log_data = NULL;
	
	
	/**
	 * nv_id
	 * 
	 * @var integer
	 * @deprecated
	 */
	protected $nv_id = 0;
	
	
	
	public function __construct($primaryFieldName = 'logid') {
		$this->setPrimaryFieldName($primaryFieldName);
	}
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter
     *
     *******************************************************************************************/
	public function getLogid() {
		return (int) $this->logid;
	}
	public function setLogid($logid) {
		$this->logid = \intval($logid);
	}

	public function getUserid() {
		return (int) $this->userid;
	}
	public function setUserid($userid) {
		$this->userid = \intval($userid);
	}

	public function getAction() {
		return (int) $this->action;
	}
	public function setAction($action) {
		$this->action = \intval($action);
	}

	public function getKid() {
		return (int) $this->kid;
	}
	public function setKid($kid) {
		$this->kid = \intval($kid);
	}

	public function getTimestamp() {
		if (!($this->timestamp instanceof \DateTime)) {
			$this->setTimestamp($this->timestamp);
		}
		
		return $this->timestamp;
	}
	public function setTimestamp($timestamp = NULL) {
		$this->createDateTimeFromValueForField(
			'timestamp',
			$timestamp
		);
	}

	public function getStatus() {
		return (int) $this->status;
	}
	public function setStatus($status) {
		$this->status = \intval($status);
	}

	public function getImport_nr() {
		return (int) $this->import_nr;
	}
	public function setImport_nr($import_nr) {
		$this->import_nr = \intval($import_nr);
	}
	
	public function getLog_data() {
		return $this->log_data;
	}
	public function setLog_data($log_data = NULL) {
		$this->log_data = $log_data;
	}
	
	
	
	/**
	 * @deprecated
	 */
	public function getNv_id() {
		return (int) $this->nv_id;
	}
	public function setNv_id($nv_id) {
		$this->nv_id = \intval($nv_id);
	}
}